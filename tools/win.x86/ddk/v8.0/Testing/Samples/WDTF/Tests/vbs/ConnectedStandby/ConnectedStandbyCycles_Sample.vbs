'
' Variable defenition 
'
Dim System
Dim TestMode
Dim TestCycles
Dim CSDuration
Dim DelayBetweenCycles
Dim WDTFSup


'
'  Use default query or one from the command line.
'

TestCycles = 1
CSDuration = 10
DelayBetweenCycles = 5

'
'  Display Usage
'
If WScript.Arguments.Named.Exists("?") Then
    WScript.Echo("")
    WScript.Echo("This script will put the system under test through the specified number of ")
    WScript.Echo("Connected Standby cycles.")
    WScript.Echo("")
    WScript.Echo("Usage: cscript ConnectedStandbyCycles_Sample.vbs [/TestCycles:n] [/CSDuration:n] [/DelayBetweenCycles:n]")
    WScript.Echo("")
    WScript.Echo("  /TestCycles:n           n indicates the number of test cycles.")
    WScript.Echo("")
    WScript.Echo("  /CSDuration:n           n indicates delay time in seconds between CS enter and exit.")
    WScript.Echo("")
    WScript.Echo("  /DelayBetweenCycles:n   n indicates delay time in seconds between CS cycles.")
    WScript.Echo("")
    WScript.Quit(1)
End If


'   Parameter details are as following -
'
'        TestCycles
'            Number of test cycles.
' 
'        CSDuration
'            Delay time in seconds between CS enter and exit.
'
'        DelayBetweenCycles
'            Delay time in seconds between CS cycles.
'

If WScript.Arguments.Named.Exists("TestCycles") Then
    TestCycles = WScript.Arguments.Named("TestCycles")
    If TestCycles = 0 Then
        WScript.Echo("Number of test cycles should be greater than 0. Set to default value of 1.")
        TestCycles = 1
    End If
End If                

If WScript.Arguments.Named.Exists("CSDuration") Then
    CSDuration = WScript.Arguments.Named("CSDuration")
    If CSDuration = 0 Then
        WScript.Echo("Connected Standby duration should be greater than 0. Set to default value of 10 seconds.")
        CSDuration = 10
    End If
End If                

If WScript.Arguments.Named.Exists("DelayBetweenCycles") Then
    DelayBetweenCycles = WScript.Arguments.Named("DelayBetweenCycles")
    If DelayBetweenCycles = 0 Then
        WScript.Echo("Delay between test cycles should be greater than 0. Set to default value of 5 seconds.")
        DelayBetweenCycles = 5
    End If
End If                

'
' Create WDTF object
'

Set WDTF = CreateObject("WDTF2.WDTF")

'
' Set TestModeVerify to TRUE to catch device configuration issues
'

WDTF.Config.TestModeVerify = True

'
' Get WDTFSup
'

Set WDTFSup = WDTF.SystemDepot.ThisSystem.GetInterface("Support")

'
' Get System Action
'

Set System = WDTF.SystemDepot.ThisSystem.GetInterface("System")

'
' Only run on system that supports AoAc
'
If NOT WDTF.SystemDepot.ThisSystem.Eval("PowerStates::AoAc=True") Then

   WDTF.Log.OutputError "This test only works on AoAc-capable system."
   WScript.Quit(50)

End If

WDTF.Log.OutputInfo "TestCycles: " & TestCycles 
WDTF.Log.OutputInfo "CSDuration: " & CSDuration
WDTF.Log.OutputInfo "DelayBetweenCycles: " & DelayBetweenCycles


If TestCycles = 1 Then
    WDTF.Log.StartTestCase "Perform " & TestCycles & " Connected Standby Cycle"
Else 
    WDTF.Log.StartTestCase "Perform " & TestCycles & " Connected Standby Cycles"
End If

Dim i
For i = 1 To TestCycles


   WDTF.Log.OutputInfo "Test Connected Standby Cycle #" & i


   '
   '  Enter and exit Connected Standby
   '

   System.ConnectedStandby(CSDuration * 1000)

   '
   '  Alternatively, enter and exit Connected Standby in two separate calls.
   '  This requires the script host to be exempted with the DAM.
   ' 
   '  System.EnterConnectedStandby
   '  <Perform some tests>
   '  System.ExitConnectedStandby
   '

   '
   '  Wait a bit before continuing
   '

   If (i < TestCycles) Then
      WDTFSup.WaitForSeconds(DelayBetweenCycles)
   End If

Next

WDTF.Log.EndTestCase


