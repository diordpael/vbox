<?xml version="1.0" ?>

<!-- Debugging helpers -->
<!-- error    Set this to true to display detailed error messages for syntax or run-time errors in the script component.-->
<!-- debug    Set this to true to enable debugging. If this debugging is not enabled, you cannot launch the script debugger for a script -->
<?component error="false" debug="false"?>

<package>
    <!-- Test module metadata -->
    <ModuleProperty name="Description" value="This test does device IO testing with processor utilization alternating between HPU and LPU. It accepts the following TAEF runtime parameters: DQ, PingPongPeriod (in minutes), HPU (high processor utilization percentage), LPU (low processor utilization percentage), and TestCycles."/>


    <!-- Define a test class -->
    <component id="Devfund">
        <!-- define and instatiate a logger -->
        <object id="Log" progid="WEX.Logger.Log" />
        <object id="RuntimeParameters" progid="Te.Common.RuntimeParameters" />

        <!-- include a reference to the logger so you could use the constants defined in logger library -->
        <reference guid="e65ef678-a232-42a7-8a36-63108d719f31" version="1.0"/>

        <public>
            <!-- Define a test method with metadata -->
            <method name="Device_IO_With_Varying_ProcUtil">
            </method>
        </public>

        <script language="VBScript">
            <![CDATA[
                Function Device_IO_With_Varying_ProcUtil()

                '
                ' Variable definition 
                '

                Dim WDTF
                Dim Devices
                Dim DeviceQuery

                Dim CpuUtilAction
                Dim WDTFSup

                Dim nLowProcUtil
                Dim nHighProcUtil

                Dim nTestCycles
                Dim nPingPongMinutes
                Dim TestModeVerify
                
                '
                ' TODO: Revisit to set these default values
                '                 

                DeviceQuery = "IsDevice"
                nHighProcUtil = 95
                nLowProcUtil = 80
                nTestCycles = 1
                nPingPongMinutes = 1
                TestModeVerify = true

                
                '   Parameter details are as follows -
                '
                '        DQ
                '            An SDEL query.
                '        HPU
                '            High processor utilization
                '        LPU
                '            Low processor utilization
                '        PingPongPeriod
                '            Ping pong period in minutes
                '        TestCycles
                '            Number of test cycles


                If RuntimeParameters.Contains("DQ") Then
                    DeviceQuery = DeviceQuery & " AND (" & RuntimeParameters.GetValue("DQ") & ")"
                End If

                If RuntimeParameters.Contains("HPU") Then
                    nHighProcUtil = CInt(RuntimeParameters.GetValue("HPU"))
                End If

                If RuntimeParameters.Contains("LPU") Then
                    nLowProcUtil = CInt(RuntimeParameters.GetValue("LPU"))
                End If

                If RuntimeParameters.Contains("PingPongPeriod") Then
                    nPingPongMinutes = CInt(RuntimeParameters.GetValue("PingPongPeriod"))
                End If
                
                If RuntimeParameters.Contains("TestCycles") Then
                    nTestCycles = CInt(RuntimeParameters.GetValue("TestCycles"))
                End If
                
                If RuntimeParameters.Contains("TMV") Then
                    TestModeVerify = RuntimeParameters.GetValue("TMV")
                End If                                    
                
                Log.Comment "DQ: " & DeviceQuery
                Log.Comment "HPU: " & nHighProcUtil 
                Log.Comment "LPU: " & nLowProcUtil
                Log.Comment "PingPongPeriod: " & nPingPongMinutes
                Log.Comment "TestCycles: " & nTestCycles

                Log.Comment("Running Device_IO_With_Varying_ProcUtil()")

                '
                ' Create WDTF object
                '

                Set WDTF = CreateObject("WDTF2.WDTF")
                
                '
                '  This test only works on win7 or later 
                '
 
                if NOT (WDTF.SystemDepot.ThisSystem.Eval("OSMajorVersion=6 AND OSMinorVersion>=1") OR WDTF.SystemDepot.ThisSystem.Eval("OSMajorVersion>6")) then
 
                   WDTF.Log.OutputInfo " This test only works in win7 or later"

                   Exit Function

                end if 

                '
                ' Setting TestModeVerify to TRUE will cause device configuration issues to be flagged as errors
                '

                WDTF.Config.TestModeVerify = TestModeVerify

                '
                ' Get Cpu UtilizationSystem & WDTFSUP Actions
                '

                Set CpuUtilAction = WDTF.SystemDepot.ThisSystem.GetInterface("CpuUtilization")
                Set WDTFSup = WDTF.SystemDepot.ThisSystem.GetInterface("Support")

                '
                ' Query for device 
                '

                set Devices = WDTF.DeviceDepot.Query(DeviceQuery)
                
                                '
                ' End test if there are no devices to test
                '
                
                If Devices.Count = 0 then
                  WDTF.Log.OutputInfo "No devices were found for testing"
                  Exit Function
                End If


                '
                '  Get Simple I/O stress tests for each device & its descendants that we have one.
                '

                set Actions = Devices.GetRelations("below-or-self/","IsDevice").GetInterfacesIfExist("SimpleIOStressEx")


                WDTF.Log.StartTestCase "Perform I/O while cycling the CPU utilization from "  & nLowProcUtil & "% to " & nHighProcUtil & "%" & " every " & nPingPongMinutes & " minute(s) " & nTestCycles & " time(s)"
                      
                '
                ' Start I/O 
                '

                if Actions.Count then 
                  Actions.Start()
                End if


                '
                ' Wait a bit for I/O to get going.
                '

                WDTFSup.WaitForMinutes(1)

                '
                '  Now Ping Pong the CPU utilisation from low to high
                '

                Dim i

                For i = 1 To nTestCycles


                   WDTF.Log.OutputInfo "Cycle " & i

                   CpuUtilAction.SetCpuUtilization nLowProcUtil, 0

                   WDTFSup.WaitForMinutes(nPingPongMinutes)

                   CpuUtilAction.SetCpuUtilization nHighProcUtil, 0

                   WDTFSup.WaitForMinutes(nPingPongMinutes)


                Next

                '
                '   Stop I/O 
                '

                if Actions.Count then
                  Actions.Stop()
                End if


                WDTF.Log.EndTestCase

                End Function
            ]]>
        </script>
    </component>
</package>

