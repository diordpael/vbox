<?xml version="1.0" ?>

<!-- Debugging helpers -->
<!-- error    Set this to true to display detailed error messages for syntax or run-time errors in the script component.-->
<!-- debug    Set this to true to enable debugging. If this debugging is not enabled, you cannot launch the script debugger for a script -->
<?component error="false" debug="false"?>

<package>
    <!-- Test module metadata -->
    <ModuleProperty name="Description" value="This test cycles the system through various sleep states and performs IO and basic PNP (disable/enable) on devices before and after each sleep state cycle."/>
    <ModuleProperty name="Kits.Parameter" value="DQ"/>
    <ModuleProperty name="Kits.Parameter.DQ.Description" value="A WDTF SDEL query that is used to identify the target device(s) - http://go.microsoft.com/fwlink/?LinkId=232678"/>
    <ModuleProperty name="Kits.Drivers" value=""/>
    <ModuleProperty name="Kits.Drivers.ResultFile" value="TestTextLog.log"/>
    <ModuleProperty name="Kits.Parameter" value="TestCycles"/>
    <ModuleProperty name="Kits.Parameter.TestCycles.Description" value="Number of test cycles"/>
    <ModuleProperty name="Kits.Parameter" value="ResumeDelay"/>
    <ModuleProperty name="Kits.Parameter.ResumeDelay.Description" value="Delay time in seconds after each sleep cycle"/>
    <ModuleProperty name="Kits.Parameter" value="IOPeriod"/>
    <ModuleProperty name="Kits.Parameter.IOPeriod.Description" value="IO period in minutes"/>
    <ModuleProperty name="Kits.Category" value="Certification.Device Fundamentals.Sleep"/>
    <ModuleProperty name="Kits.Parameter.DQ.Default" value="INF::OriginalInfFileName='%InfFileName%'"/>
    <ModuleProperty name="Kits.Parameter.TestCycles.Default" value="8"/>    
    <ModuleProperty name="Kits.Parameter.ResumeDelay.Default" value="10"/>
    <ModuleProperty name="Kits.Parameter.IOPeriod.Default" value="1"/>

    <!-- Define a test class -->
    <component id="Devfund">
        <!-- define and instatiate a logger -->
        <object id="Log" progid="WEX.Logger.Log" />
        <object id="RuntimeParameters" progid="Te.Common.RuntimeParameters" />

        <!-- include a reference to the logger so you could use the constants defined in logger library -->
        <reference guid="e65ef678-a232-42a7-8a36-63108d719f31" version="1.0"/>

        <public>
            <!-- Define a test method with metadata -->
            <method name="Sleep_PNP_DisableEnable_With_IO_Before_And_After">
                <TestMethodProperty name="Kits.DisplayName" value="Sleep and PNP (disable and enable) with IO Before and After (Certification)"/>
            </method>
        </public>

        <script language="VBScript">
            <![CDATA[
            
                Function Sleep_PNP_DisableEnable_With_IO_Before_And_After()
                
                    Dim WDTF
                    Dim System
                    Dim SleepState
                    Dim Device
                    Dim Devices
                    Dim SimpleIOCol
                    Dim DeviceQuery
                    Dim TestMode
                    Dim TestCycles
                    Dim ResumeDelay
                    Dim IOPeriod
                    Dim WDTFSup
                    Dim TestModeVerify

                    '
                    ' TODO: Revisit to set these default values
                    '  

                    '
                    ' Logo kit defaults. Any changes to these values must be reviewed.
                    '

                    TestCycles = 8
                    ResumeDelay = 10
                    IOPeriod = 1
                    DeviceQuery = "IsDevice"
                    TestModeVerify = true
                    
                    '   Parameter details are as follows -
                    '
                    '        DQ
                    '            An SDEL query.
                    '         
                    '        TestCycles
                    '            Number of test cycles.
                    ' 
                    '        IOPeriod
                    '            IO period in minutes.
                    ' 
                    '        ResumeDelay
                    '            Delay time in seconds after each sleep cycle.
                    ' 
                    
                    If RuntimeParameters.Contains("DQ") Then
                        DeviceQuery = DeviceQuery & " AND (" & RuntimeParameters.GetValue("DQ") & ")"
                    End If
                    
                    If RuntimeParameters.Contains("TestCycles") Then
                        TestCycles = CInt(RuntimeParameters.GetValue("TestCycles"))
                    End If

                    If RuntimeParameters.Contains("IOPeriod") Then
                        IOPeriod = CLng(RuntimeParameters.GetValue("IOPeriod"))
                    End If
                    
                    If RuntimeParameters.Contains("ResumeDelay") Then
                        ResumeDelay = CInt(RuntimeParameters.GetValue("ResumeDelay"))
                    End If
                    
                    If RuntimeParameters.Contains("TMV") Then
                        TestModeVerify = RuntimeParameters.GetValue("TMV")
                    End If                    

                    Log.Comment "DQ: " & DeviceQuery
                    Log.Comment "TestCycles: " & TestCycles 
                    Log.Comment "IOPeriod: " & IOPeriod
                    Log.Comment "ResumeDelay: " & ResumeDelay
                    
                    Log.Comment("Running Sleep_PNP_DisableEnable_With_IO_Before_And_After()")

                    '
                    ' Create WDTF object
                    '

                    Set WDTF = CreateObject("WDTF2.WDTF")
                    
                    '
                    ' Setting TestModeVerify to TRUE will cause device configuration issues to be flagged as errors
                    '

                    WDTF.Config.TestModeVerify = TestModeVerify

                    Set WDTFSup = WDTF.SystemDepot.ThisSystem.GetInterface("Support")

                    '
                    ' Get System Action
                    '

                    Set System = WDTF.SystemDepot.ThisSystem.GetInterface("System")

                    '
                    ' Query for devices
                    '

                    set Devices = WDTF.DeviceDepot.Query(DeviceQuery)
                    
                    '
                    ' End test if there are no devices to test
                    '
                    
                    If Devices.Count = 0 then
                      WDTF.Log.OutputInfo "No devices were found for testing"
                      Exit Function
                    End If                

                    '
                    '  Get Simple I/O stress tests for each device & its descendants that we have one.
                    '

                    set SimpleIOCol = Devices.GetRelations("below-or-self/","IsDevice").GetInterfacesIfExist("SimpleIOStressEx")


                    WDTF.Log.StartTestCase "Perform " & TestCycles & " Sleep Cycles"


                    Dim i
                    For i = 1 To TestCycles


                       WDTF.Log.OutputInfo "Test Sleep Cycle #" & i
       

                       '
                       ' Loop through all sleep states once
                       '
        
                       SleepState =  System.GetFirstSleepState 

                       do while SleepState 

       
                          '
                          ' Test devices for a bit
                          '
                          if SimpleIoCol.Count then
                            SimpleIoCol.Start()
                            WDTFSup.WaitForMinutes(IOPeriod)
                            SimpleIoCol.Stop()
                          End if
     
       
                          '
                          '  Put system to sleep 
                          '


                          System.Sleep SleepState

       
                          WDTFSup.WaitForSeconds(ResumeDelay)


                          '
                          ' Test devices for a bit
                          '
                          
                          if SimpleIoCol.Count then
                            SimpleIoCol.Start()
                            WDTFSup.WaitForMinutes(IOPeriod)
                            SimpleIoCol.Stop()
                          End if                        

                          '
                          '  Disable / Enable each device 
                          '
          
                          Dim PNPAction

                          For Each Device In Devices

                             IF Device.Eval("IsDisableable=True AND IsStarted=True") then 

                                '
                                '  Get PNP action for Device dir
                                ' 

                                set PNPAction = Device.GetInterface("PNP")

                                '
                                '  Disable / Enable device 
                                ' 

                                PNPAction.DisableDevice()
                                PNPAction.EnableDevice()
                                WDTFSup.WaitForMilliseconds(10000)

                             End if
                          Next


                         SleepState =  System.GetNextSleepState 

                        Loop

                        '
                        ' Test devices for a bit.
                        '

                        if SimpleIoCol.Count then
                          SimpleIoCol.Start()
                          WDTFSup.WaitForMinutes(IOPeriod)
                          SimpleIoCol.Stop()
                        End if


                    Next

                    WDTF.Log.EndTestCase

                    WDTFSup.WaitForMinutes(1)

                End Function
            ]]>
        </script>
    </component>
</package>
