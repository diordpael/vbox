<?xml version="1.0" ?>

<!-- Debugging helpers -->
<!-- error    Set this to true to display detailed error messages for syntax or run-time errors in the script component.-->
<!-- debug    Set this to true to enable debugging. If this debugging is not enabled, you cannot launch the script debugger for a script -->
<?component error="false" debug="false"?>

<package>
    <!-- Test module metadata -->

    <ModuleProperty name="Description" value="This test runs driver fuzzing tests with IO during."/>
    <ModuleProperty name="Kits.Drivers" value=""/>
    <ModuleProperty name="Kits.Category" value="Basic.Device Fundamentals.Penetration.FuzzTests"/>
    <ModuleProperty name="Kits.Drivers.ResultFile" value="TestTextLog.log"/>
    <ModuleProperty name="Kits.Parameter" value="DQ"/>
    <ModuleProperty name="Kits.Parameter.DQ.Description" value="A WDTF SDEL query that is used to identify the target device(s) - http://go.microsoft.com/fwlink/?LinkId=232678"/>
    <ModuleProperty name="Kits.Parameter.DQ.Default" value="INF::OriginalInfFileName='%InfFileName%'"/>
    <ModuleProperty name="Kits.Parameter" value="FuzzTestPeriod"/>
    <ModuleProperty name="Kits.Parameter.FuzzTestPeriod.Description" value="Fuzz test period in minutes"/>
    <ModuleProperty name="Kits.Parameter.FuzzTestPeriod.Default" value="3"/>
    
    <!-- Define a test class -->
    <component id="Devfund">
        <!-- define and instatiate a logger -->
        <object id="Log" progid="WEX.Logger.Log" />
        <object id="RuntimeParameters" progid="Te.Common.RuntimeParameters" />

        <!-- include a reference to the logger so you could use the constants defined in logger library -->
        <reference guid="e65ef678-a232-42a7-8a36-63108d719f31" version="1.0"/>

        <public>
            <!-- Define a test method with metadata -->
            <method name="Fuzz_With_IO_During">
                <TestMethodProperty name="Kits.DisplayName" value="Fuzz with IO during (Basic)"/>
            </method>
        </public>

        <script language="VBScript">
            <![CDATA[
                Function Fuzz_With_IO_During()
                
                    '
                    ' Variable definition 
                    '

                    Dim WDTF
                    Dim FuzzTestColl
                    Dim Device
                    Dim DeviceQuery
                    Dim FuzzTestPeriod                
                    Dim TestModeVerify
                
                    '
                    ' TODO: Revisit to set these default values
                    '                    
                    
                    DeviceQuery = "IsDevice"
                    FuzzTestPeriod = 3
                    TestModeVerify = true

                    '   Parameter details are as follows -
                    '
                    '        DQ
                    '            An SDEL query.
                    '       
                    '        FuzzTestPeriod
                    '            Fuzz test period in minutes.
                    '         

                    If RuntimeParameters.Contains("DQ") Then
                        DeviceQuery = DeviceQuery & " AND (" & RuntimeParameters.GetValue("DQ") & ")"
                    End If
                    
                    If RuntimeParameters.Contains("FuzzTestPeriod") Then
                        FuzzTestPeriod = CInt(RuntimeParameters.GetValue("FuzzTestPeriod"))
                    End If
                    
                    If RuntimeParameters.Contains("TMV") Then
                        TestModeVerify = RuntimeParameters.GetValue("TMV")
                    End If                    
                    
                    Log.Comment "DQ: " & DeviceQuery
                    Log.Comment "FuzzTestPeriod: " & FuzzTestPeriod

                    Log.Comment("Running Fuzz_With_IO_During")

                    '
                    ' Create WDTF object
                    '

                    Set WDTF = CreateObject("WDTF2.WDTF")

                    '
                    ' Setting TestModeVerify to TRUE will cause device configuration issues to be flagged as errors
                    '
                    WDTF.Config.TestModeVerify = TestModeVerify

                    Set WDTFSup = WDTF.SystemDepot.ThisSystem.GetInterface("System")

                    '
                    ' Query for devices 
                    '

                    set Devices = WDTF.DeviceDepot.Query(DeviceQuery)

                    '
                    ' End test if there are no devices to test
                    '
                    
                    If Devices.Count = 0 then
                      WDTF.Log.OutputInfo "No devices were found for testing"
                      Exit Function
                    End If
                    
                    '
                    ' Ensure Driver Verifier is enabled against all drivers of device stack(s) under test
                    '

                    If NOT Devices.Eval("Windows::IsDriverVerifierEnabled") Then
                      WDTF.Log.OutputError "Driver Verifier isn't enabled against all drivers of device stack(s) under test. Enable Driver Verifier and re-run the test."
                      Exit Function
                    End If

                    '
                    '  Start I/O 
                    '
                    set SimpleIoStress  = Devices.GetRelations("below-or-self/","IsDevice").GetInterfacesIfExist("SimpleIOStressEx")

                    if SimpleIoStress.Count then
                      SimpleIoStress.Start()
                    End if



                    '
                    '  Enum Devices
                    '

                    WDTF.Log.OutputInfo "Enum Devices"

                    for each Device in Devices


                        '
                        '  Get FuzzTest Action
                        '

                        if Device.HasInterface("FuzzTest") then

                           Set FuzzTestColl = Device.GetInterface("FuzzTest")

                           WDTF.Log.OutputInfo "Number of tests found:" & FuzzTestColl.Count

                           WDTF.Log.OutputInfo "Enum Tests"

                           for each Fuzztest in FuzzTestColl

                              WDTF.Log.OutputInfo "Test Name:" & Fuzztest.Description

                              Fuzztest.Start() 
                              WDTFSup.WaitForMinutes(FuzzTestPeriod)
                              Fuzztest.Stop() 

                           next

                        end if
                        
                    next

                    WDTF.Log.OutputInfo  "Test Completed"

                    if SimpleIoStress.Count then
                      SimpleIoStress.Stop()
                    End if

                End Function
                ]]>                
        </script>
    </component>
</package>                



