<?xml version="1.0" ?>

<!-- Debugging helpers -->
<!-- error    Set this to true to display detailed error messages for syntax or run-time errors in the script component.-->
<!-- debug    Set this to true to enable debugging. If this debugging is not enabled, you cannot launch the script debugger for a script -->
<?component error="false" debug="false"?>

<package>
    <!-- Test module metadata -->
    <ModuleProperty name="RebootPossible" value="true"/>
    <ModuleProperty name="Description" value="This test runs IO on devices before and after a system reboot."/>
    <ModuleProperty name="Kits.Drivers" value=""/>
    <ModuleProperty name="Kits.Category" value="Basic.Device Fundamentals.Reboot"/>
    <ModuleProperty name="Kits.Drivers.ResultFile" value="TestTextLog.log"/>
    <ModuleProperty name="Kits.Parameter" value="DQ"/>
    <ModuleProperty name="Kits.Parameter.DQ.Description" value="A WDTF SDEL query that is used to identify the target device(s) - http://go.microsoft.com/fwlink/?LinkId=232678"/>
    <ModuleProperty name="Kits.Parameter.DQ.Default" value="INF::OriginalInfFileName='%InfFileName%'"/>
    <ModuleProperty name="Kits.Parameter" value="TestCycles"/>
    <ModuleProperty name="Kits.Parameter.TestCycles.Description" value="Number of test cycles"/>
    <ModuleProperty name="Kits.Parameter.TestCycles.Default" value="1"/>
    <ModuleProperty name="Kits.Parameter" value="IOPeriod"/>
    <ModuleProperty name="Kits.Parameter.IOPeriod.Description" value="IO period in minutes"/>
    <ModuleProperty name="Kits.Parameter.IOPeriod.Default" value="1"/>
    
    <!-- Define a test class -->
    <component id="Devfund">
        <!-- define and instatiate a logger -->
        <object id="Log" progid="WEX.Logger.Log" />
        <object id="RuntimeParameters" progid="Te.Common.RuntimeParameters" />

        <!-- include a reference to the logger so you could use the constants defined in logger library -->
        <reference guid="e65ef678-a232-42a7-8a36-63108d719f31" version="1.0"/>

        <public>
            <!-- Define a test method with metadata -->
            <method name="Reboot_Restart_With_IO_Before_And_After">
                <TestMethodProperty name="Kits.DisplayName" value="Reboot restart with IO before and after (Basic)"/>
            </method>
        </public>

        <script language="VBScript">
            <![CDATA[
                Function Reboot_Restart_With_IO_Before_And_After()
                
                    '
                    ' Variable definition 
                    '

                    Dim WDTF
                    Dim System
                    Dim Devices
                    Dim Actions
                    Dim DeviceQuery
                    Dim WDTFSup
                    Dim IOPeriod
                    Dim TestCycles
                    Dim TestModeVerify
                    
                    '
                    ' TODO: Revisit to set these default values
                    '    
                                        
                    DeviceQuery = "IsDevice"
                    IOPeriod = 1
                    TestCycles = 1
                    TestModeVerify = true
                    
                    '   Parameter details are as follows -
                    '
                    '        DQ
                    '            An SDEL query.
                    '        TestCycles
                    '            Number of test cycles.
                    '        IOPeriod
                    '            IO Period in minutes.
                    
                    If RuntimeParameters.Contains("DQ") Then
                        DeviceQuery = DeviceQuery & " AND (" & RuntimeParameters.GetValue("DQ") & ")"
                    End If

                    If RuntimeParameters.Contains("TestCycles") Then
                        TestCycles = CInt(RuntimeParameters.GetValue("TestCycles"))
                    End If
                    
                    If RuntimeParameters.Contains("IOPeriod") Then
                        IOPeriod = CLng(RuntimeParameters.GetValue("IOPeriod"))
                    End If
                    
                    If RuntimeParameters.Contains("TMV") Then
                        TestModeVerify = RuntimeParameters.GetValue("TMV")
                    End If                                        
                    
                    Log.Comment "DQ: " & DeviceQuery
                    Log.Comment "IOPeriod: " & IOPeriod
                    Log.Comment "TestCycles: " & TestCycles

                    Log.Comment("Running Reboot_Restart_With_IO_Before_And_After")

                    '
                    ' Create WDTF object
                    '

                    Set WDTF = CreateObject("WDTF2.WDTF")
                    
                    '
                    ' Setting TestModeVerify to TRUE will cause device configuration issues to be flagged as errors
                    '

                    WDTF.Config.TestModeVerify = TestModeVerify

                    Set WDTFSup = WDTF.SystemDepot.ThisSystem.GetInterface("Support")

                    '
                    ' Get System Action
                    '

                    Set System = WDTF.SystemDepot.ThisSystem.GetInterface("System")

                    '
                    ' Query for device 
                    '

                    set Devices = WDTF.DeviceDepot.Query(DeviceQuery)

                    '
                    ' End test if there are no devices to test
                    '
                    
                    If Devices.Count = 0 then
                      WDTF.Log.OutputInfo "No devices were found for testing"
                      Exit Function
                    End If

                    '
                    '  Get Simple I/O stress tests for each device & its descendants that we have one.
                    '


                    set Actions = Devices.GetRelations("below-or-self/","IsDevice").GetInterfacesIfExist("SimpleIOStressEx")


                    '
                    ' See if we have restarted 
                    '
        
                    if System.IsRestarted then


                       '
                       '  Test devices after the restart
                       '

                        WDTF.Log.StartTestCase "Test devices after restart # " & System.RestartCount


                       if Actions.Count then
                         Actions.Start()
                         WDTFSup.WaitForMinutes(IOPeriod)
                         Actions.Stop()
                       End if


                       WDTF.Log.EndTestCase

 
                       if TestCycles > System.RestartCount then


                          '
                          ' Reboot and restart 
                          '
      
                          System.RebootRestart()

                       end if


                    else

                       '
                       '  Test devices before the restart
                       '

                       WDTF.Log.StartTestCase "Test devices before the restart"


                       if Actions.Count then
                         Actions.Start()
                         WDTFSup.WaitForMinutes(IOPeriod)
                         Actions.Stop()
                       End if


                       WDTF.Log.EndTestCase

                       '
                       ' Reboot and restart 
                       '
      
                       System.RebootRestart()


                    End If

                End Function
            ]]>
        </script>
    </component>
</package>
