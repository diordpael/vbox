<?xml version="1.0" ?>

<!-- Debugging helpers -->
<!-- error    Set this to true to display detailed error messages for syntax or run-time errors in the script component.-->
<!-- debug    Set this to true to enable debugging. If this debugging is not enabled, you cannot launch the script debugger for a script -->
<?component error="false" debug="false"?>

<package>
    <!-- Test module metadata -->
    <ModuleProperty name="Description" value="This test performs SimpleIO testing on devices in a separate process and terminates the IO process after some time. It accepts the following TAEF runtime parameters: DQ, TestCycles, and IOPeriod (in minutes)."/>
    
    <!-- Define a test class -->
    <component id="Devfund">
        <!-- define and instatiate a logger -->
        <object id="Log" progid="WEX.Logger.Log" />
        <object id="RuntimeParameters" progid="Te.Common.RuntimeParameters" />

        <!-- include a reference to the logger so you could use the constants defined in logger library -->
        <reference guid="e65ef678-a232-42a7-8a36-63108d719f31" version="1.0"/>

        <public>
            <!-- Define a test method with metadata -->
            <method name="SimpleIOStress_TermIoProc">
            </method>
        </public>

        <script language="VBScript">
            <![CDATA[
                Function SimpleIOStress_TermIoProc()                

                    '
                    ' Variable definition 
                    '

                    Dim WDTF
                    Dim Devices
                    Dim Action
                    Dim Actions 
                    Dim DeviceQuery
                    Dim TestCycles
                    Dim WDTFSup
                    Dim IOPeriod
                    Dim TestModeVerify

                    '
                    ' TODO: Revisit to set these default values
                    '                    

                    TestCycles = 10
                    DeviceQuery = "IsDevice"
                    IOPeriod = 1
                    TestModeVerify = true
                    
                    '   Parameter details are as follows -
                    '
                    '        DQ
                    '            An SDEL query.
                    '         
                    '        TestCycles
                    '            Number of test cycles.
                    '         
                    '        IOPeriod
                    '            IO Period in minutes.
                    '         


                    If RuntimeParameters.Contains("DQ") Then
                        DeviceQuery = DeviceQuery & " AND (" & RuntimeParameters.GetValue("DQ") & ")"
                    End If
                    
                    If RuntimeParameters.Contains("TestCycles") Then
                        TestCycles = CInt(RuntimeParameters.GetValue("TestCycles"))
                    End If
                    
                    If RuntimeParameters.Contains("IOPeriod") Then
                        IOPeriod = CLng(RuntimeParameters.GetValue("IOPeriod"))
                    End If
                    
                    If RuntimeParameters.Contains("TMV") Then
                        TestModeVerify = RuntimeParameters.GetValue("TMV")
                    End If                    
                    
                    Log.Comment "DQ: " & DeviceQuery
                    Log.Comment "TestCycles: " & TestCycles
                    Log.Comment "IOPeriod: " & IOPeriod                     
                    
                    Log.Comment("Running SimpleIOStress_TermIoProc()")

                    '
                    ' Create WDTF object
                    '

                    Set WDTF = CreateObject("WDTF2.WDTF")
                    
                    '
                    ' Setting TestModeVerify to TRUE will cause device configuration issues to be flagged as errors
                    '

                    WDTF.Config.TestModeVerify = TestModeVerify

                    Set WDTFSup = WDTF.SystemDepot.ThisSystem.GetInterface("Support")

                    WDTF.Log.OutputInfo " Num of test cycles " & TestCycles & " "

                    '
                    ' Query for device 
                    '

                    set Devices = WDTF.DeviceDepot.Query(DeviceQuery)


                    '
                    ' End test if there are no devices to test
                    '
                    
                    If Devices.Count = 0 then
                      WDTF.Log.OutputInfo "No devices were found for testing"
                      Exit Function
                    End If

                    '
                    '  Get Simple I/O Stress test for device & its descendants 
                    '

                    set Actions = Devices.GetRelations("below-or-self/","IsDevice").GetInterfacesIfExist("SimpleIOStressProc")


                    '
                    '  Start test case 
                    '

                    WDTF.Log.StartTestCase "Perform " & TestCycles & " test cycles Device I/O with terminate process "

                    Dim i

                    For i = 1 To TestCycles

                        WDTF.Log.OutputInfo "Device I/O with terminate process Cycle #" & i


                        '
                        ' Start all I/O tests
                        '
                        
                        if Actions.Count then
                          Actions.StartAsync()
                          Actions.WaitAsyncCompletion()
       
                          '
                          ' Wait a bit for I/O to get going 
                          '

                          WDTFSup.WaitForMinutes(IOPeriod)

                          '
                          '  Terminate the process that does the I/O 
                          '

                          Actions.Terminate()

                          '
                          '  Recreate the process that does the I/O 
                          '
                        End if

                    Next



                    WDTF.Log.EndTestCase

                End Function
            ]]>
        </script>
    </component>
</package>

