<?xml version="1.0" ?>

<!-- Debugging helpers -->
<!-- error    Set this to true to display detailed error messages for syntax or run-time errors in the script component.-->
<!-- debug    Set this to true to enable debugging. If this debugging is not enabled, you cannot launch the script debugger for a script -->
<?component error="false" debug="false"?>

<package>
    <!-- Test module metadata -->
    <ModuleProperty name="Description" value="This test performs critical sleep state transitions on the system and performs IO on devices. It accepts the following TAEF runtime parameters: DQ, TestCycles, IOPeriod (in minutes), and ResumeDelay (in seconds)."/>

    <!-- Define a test class -->
    <component id="Devfund">
        <!-- define and instatiate a logger -->
        <object id="Log" progid="WEX.Logger.Log" />
        <object id="RuntimeParameters" progid="Te.Common.RuntimeParameters" />

        <!-- include a reference to the logger so you could use the constants defined in logger library -->
        <reference guid="e65ef678-a232-42a7-8a36-63108d719f31" version="1.0"/>

        <public>
            <method name="Critical_Sleep_With_IO_During">
            </method>
        </public>

        <script language="VBScript">
            <![CDATA[
                Function Critical_Sleep_With_IO_During()
                
                    '
                    ' Variable definition 
                    '

                    Dim WDTF
                    Dim System
                    Dim SleepState
                    Dim Devices
                    Dim Actions
                    Dim Action
                    Dim DeviceQuery

                    Dim TestCycles
                    Dim ResumeDelay
                    Dim IOPeriod
                    Dim WDTFSup
                    Dim TestModeVerify
                    
                    '
                    ' TODO: Revisit to set these default values
                    '                    
                    TestCycles = 2
                    ResumeDelay = 10
                    IOPeriod = 1
                    DeviceQuery = "IsDevice"
                    TestModeVerify = true
                    
                    '   Parameter details are as follows -
                    '
                    '        DQ
                    '            An SDEL query.
                    '         
                    '        TestCycles
                    '            Number of test cycles.
                    ' 
                    '        IOPeriod
                    '            IO period in minutes.
                    ' 
                    '        ResumeDelay
                    '            Delay time in seconds after each sleep cycle.
                    ' 
                    
                    If RuntimeParameters.Contains("DQ") Then
                        DeviceQuery = DeviceQuery & " AND (" & RuntimeParameters.GetValue("DQ") & ")"
                    End If
                    
                    If RuntimeParameters.Contains("TestCycles") Then
                        TestCycles = CInt(RuntimeParameters.GetValue("TestCycles"))
                    End If

                    If RuntimeParameters.Contains("IOPeriod") Then
                        IOPeriod = CLng(RuntimeParameters.GetValue("IOPeriod"))
                    End If
                    
                    If RuntimeParameters.Contains("ResumeDelay") Then
                        ResumeDelay = CInt(RuntimeParameters.GetValue("ResumeDelay"))
                    End If
                    
                    If RuntimeParameters.Contains("TMV") Then
                        TestModeVerify = RuntimeParameters.GetValue("TMV")
                    End If                    
                    
                    Log.Comment "DQ: " & DeviceQuery
                    Log.Comment "TestCycles: " & TestCycles 
                    Log.Comment "IOPeriod: " & IOPeriod
                    Log.Comment "ResumeDelay: " & ResumeDelay
                    
                    Log.Comment("Running Critical_Sleep_With_IO_During")

                    '
                    ' Create WDTF object
                    '

                    Set WDTF = CreateObject("WDTF2.WDTF")

                    '
                    ' Setting TestModeVerify to TRUE will cause device configuration issues to be flagged as errors
                    '

                    WDTF.Config.TestModeVerify = TestModeVerify

                    Set WDTFSup = WDTF.SystemDepot.ThisSystem.GetInterface("Support")

                    '
                    ' Get System Action
                    '

                    Set System = WDTF.SystemDepot.ThisSystem.GetInterface("System")

                    '
                    ' Set Critical property in System Action
                    '
                    
                    System.Critical = True

                    '
                    ' Query for device 
                    '

                    set Devices = WDTF.DeviceDepot.Query(DeviceQuery)

                    '
                    ' End test if there are no devices to test
                    '
                    
                    If Devices.Count = 0 then
                      WDTF.Log.OutputInfo "No devices were found for testing"
                      Exit Function
                    End If

                    '
                    '  Get Simple I/O stress tests for each device & its descendants that we have one.
                    '

                    set Actions = Devices.GetRelations("below-or-self/","IsDevice").GetInterfacesIfExist("SimpleIOStressEx")


                    WDTF.Log.StartTestCase "Perform " & TestCycles & " Test Sleep Cycle with I/O during"

                    Dim i
                    For i = 1 To TestCycles


                       WDTF.Log.OutputInfo "Test Sleep Cycle #" & i
       

                       '
                       ' Loop through all sleep states once
                       '
        
                       SleepState =  System.GetFirstSleepState 

                       do while SleepState 

       
                          '
                          ' Test devices for a bit before 
                          ' we put the system to sleep
                          '
                        
                          if Actions.Count then
                            WDTF.Log.OutputInfo " Start before/after I/O"
                            Actions.Start()
                            WDTFSup.WaitForMinutes(IOPeriod)
                            Actions.Stop()

                            '
                            ' Start I/O
                            '

                            WDTF.Log.OutputInfo " Start during I/O"
                            Actions.DisableObjectErrorLogging()
                            Actions.Start()
          

                            WDTFSup.WaitForMinutes(IOPeriod)
                          End if

                          '
                          '  Put system to sleep 
                          '

                          System.Sleep SleepState

                          WDTFSup.WaitForSeconds(ResumeDelay)

                          '
                          ' Stop I/O
                          ' 
                          
                          if Actions.Count then
                            Actions.Stop()
                            Actions.EnableObjectErrorLogging()
                            WDTF.Log.OutputInfo " Stop during I/O"
                          End if

                          SleepState =  System.GetNextSleepState 

                        Loop

                        '
                        ' Test devices for a bit.
                        '
                        
                        if Actions.Count then
                          WDTF.Log.OutputInfo " Start after I/O"
                          Actions.Start()
                          WDTFSup.WaitForMinutes(IOPeriod)
                          Actions.Stop()
                        End if

                    Next

                    WDTF.Log.EndTestCase

                End Function
            ]]>
        </script>
    </component>
</package>




