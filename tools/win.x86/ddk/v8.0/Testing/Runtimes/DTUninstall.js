/*
Copyright (c) 2011 Microsoft Corporation.  All Rights Reserved.
Driver Test Service Uninstall Script
*/

var wait = false;

// Get arguments
for (var enumArgs = new Enumerator(WScript.Arguments);
    !enumArgs.atEnd();
    enumArgs.moveNext()) {
    var arg = enumArgs.item();
    if ("WaitForService" == arg) {
        wait = true;
    }
}

if (!wait) {
    // Launch the script again to wait for the service
    WScript.Echo("Start service wait script");
    var shell = WScript.CreateObject("WScript.Shell");
    shell.Exec("cscript.exe " + WScript.ScriptFullName + " WaitForService > %TEMP%\DTUninstall_Wait.log");
}
else {
    // Connect to WMI
    var shell = WScript.CreateObject("WScript.Shell");
    var logPath = shell.ExpandEnvironmentStrings("%TEMP%\\DTUninstall.log");
    var fso = new ActiveXObject("Scripting.FileSystemObject");
    var logFile = fso.CreateTextFile(logPath);
    logFile.WriteLine("Connect to WMI...");
    var wmiConnect = "winmgmts:{impersonationLevel=impersonate, (Shutdown)}!\\\\.\\root\\cimv2";
    var wmiService = GetObject(wmiConnect);
    var done = false;

    // Wait for the dtsvc.exe process to exit
    logFile.WriteLine("Wait for service to be removed");
    while (!done) {
        done = true;
        var service = wmiService.ExecQuery(
            "Select * from Win32_Service Where Name = 'dtsvc'");
        for (var enumService = new Enumerator(service);
             !enumService.atEnd();
             enumService.moveNext()) {
            var dtService = enumService.item();
            if (null != dtService) {
                done = false;
                if ("Stopped" == dtService.State) {
                    // Normally stopping a stopped service gives a '5' error code
                    // if we get a different error code then the service has been
                    // removed and we need to go ahead and reboot the machine
                    var errorId = dtService.StopService();
                    if (5 != errorId) {
                        done = true;
                    }
                }
            }
        }

        WScript.Sleep(200);
    }

    // Delete local WDKRemoteUser
    var localComputer = GetObject("WinNT://.");
    localComputer.Delete("User", "wdkremoteuser");
    logFile.WriteLine("User deleted");

    // Get the OS object
    var osCollection = wmiService.ExecQuery("Select * from Win32_OperatingSystem");

    // Go through each OS object
    for (var enumOs = new Enumerator(osCollection);
         !enumOs.atEnd();
         enumOs.moveNext()) {
        // Force logoff any users
        var os = enumOs.item();
        os.Win32Shutdown(6);
        logFile.WriteLine("Reboot initiated");
    }
    logFile.Close();
}

WScript.Quit(0);
