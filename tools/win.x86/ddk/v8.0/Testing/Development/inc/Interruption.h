//----------------------------------------------------------------------------------------------------------------------
/// \file
/// <summary>API and data types for interacting with TAEF during Reboot scenarios.</summary>
// Copyright (c) Microsoft Corporation.  All Rights Reserved.
//----------------------------------------------------------------------------------------------------------------------
#pragma once
#include "TE.Common.h"

namespace WEX { namespace TestExecution
{
    namespace RebootOption
    {
        enum Option
        {
            Rerun,
            Continue
        };
    }

    namespace Interruption
    {
        void TECOMMON_API Reboot(RebootOption::Option option);
        void TECOMMON_API RebootCustom(RebootOption::Option option);
    }
} /* TestExecution */ } /* namespace WEX */
