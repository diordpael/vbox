/*
	Copyright (c) Microsoft Corporation.  All rights reserved.

	IrqlDispatch.slic

	Description:

	This rule verifies that the following routines are only called at IRQL = DISPATCH_LEVEL:

	* AllocateAdapterChannel
	* FreeAdapterChannel
	* FreeMapRegisters
	* GetScatterGatherList
	* IoAllocateController
	* IoFreeController
	* IoStartNextPacket
	* KeAcquireSpinLockAtDpcLevel
	* KeInsertByKeyDeviceQueue
	* KeInsertDeviceQueue
	* KeReleaseSpinLockFromDpcLevel
	* KeRemoveByKeyDeviceQueue
	* KeRemoveDeviceQueue
	* PutScatterGatherList

*/

#include "ntddk_slic.h"

[
AllocateAdapterChannel,
FreeAdapterChannel,
FreeMapRegisters,
GetScatterGatherList,
IoAllocateController,
KeAcquireSpinLockAtDpcLevel,
IoFreeController,
IoStartNextPacket,
KeInsertByKeyDeviceQueue,
KeInsertDeviceQueue,
KeReleaseSpinLockFromDpcLevel,
KeRemoveByKeyDeviceQueue,
KeRemoveDeviceQueue,
PutScatterGatherList
].entry
{
	if ( sdv_irql_current != DISPATCH_LEVEL ) {
		abort "$fname should only be called at IRQL = DISPATCH_LEVEL.";
	}
}
