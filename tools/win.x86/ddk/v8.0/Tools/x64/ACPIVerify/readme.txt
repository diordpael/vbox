To run the boot tests via USB key:
1. run create_bootable_key.cmd with the following parameters:
    i  ) 'arm' or 'amd64' depending on which version of the WDK you have installed
    ii ) The path to the USB key
    iii) 'log_all' or 'log_error'. The former will print the contents of all ACPI tables it verifies to the log file,
         The latter will only print messages from failed tests.
2. Plug the USB key into your efi machine and turn it on. 
3. Wait for the tests to finish running
4. remove the USB key and reboot
5. Before rerunning the tests, run the script 'reset.cmd'.
6. To process the bootlog into a readable format, run 'FWTestApp.exe /ocl'

To run the boot tests from Windows:
1. Run 'FWTestApp.exe'
2. Reboot
3. Wait for the tests to finish, if the computer does not automatically restart, reboot again
4. Run 'FWTestApp.exe /cl'


