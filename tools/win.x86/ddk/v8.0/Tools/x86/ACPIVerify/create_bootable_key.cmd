@ECHO OFF
set usage="Usage: create_bootable_key [arm | amd64] path [log_all | log_error]"

REM Creating a Newline variable (the two blank lines are required!)
set NLM=^


set NL=^^^%NLM%%NLM%^%NLM%%NLM%

set boot_file_name=

IF "%1"=="arm" set boot_file_name="bootarm.efi"
IF "%1"=="amd64" set boot_file_name="bootmgfw.efi"
IF "%1"=="/?" echo %usage% && Exit /B -1

IF "%boot_file_name%"=="" echo %usage% && Exit /B -1
REM "FOO"
IF NOT "%3"=="log_all" (
    IF NOT "%3"=="log_error" (
        echo Please specify a log level: %NL%%usage% && Exit /B -1
    )
)

REM clear the key
DEL /S /Q %2\*

REM create directory structure
mkdir %2\efi
mkdir %2\efi\boot
mkdir %2\efi\Microsoft
mkdir %2\efi\Microsoft\Boot
mkdir %2\efi\Microsoft\Boot\Fonts

copy Fonts\* %2\efi\Microsoft\Boot\Fonts

copy bootmgfw.efi %2\efi\Microsoft\Boot\%boot_file_name%
copy bootmgfw.efi %2\efi\Boot\%boot_file_name%
copy BCD %2\efi\Microsoft\Boot
copy FWTestApp.exe %2\FWTestApp.exe
copy FWTestAppPreOs.efi %2\

echo @Echo OFF >> %2\reset.cmd
echo DEL PreOsResultFile.dat >> %2\reset.cmd
echo fsutil file createnew PreOsResultFile.dat 5242880 >> %2\reset.cmd

fsutil file createnew %2\PreOsResultFile.dat 5242880

REM Modify the BCD if needed

IF "%3"=="log_all" bcdedit /store %2\efi\Microsoft\Boot\BCD /set {default} custom:25000001 3
IF "%3"=="log_error" bcdedit /store %2\efi\Microsoft\Boot\BCD /set {default} custom:25000001 5

IF "%ErrorLevel%"!==!"0" echo "Need to have administrator access to change the error level."

