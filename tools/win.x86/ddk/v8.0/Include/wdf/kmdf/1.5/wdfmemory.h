/*++

Copyright (c) Microsoft Corporation.  All rights reserved.

_WdfVersionBuild_

Module Name:

    WdfMemory.h

Abstract:

    Contains prototypes for managing memory objects in the driver frameworks.

Author:

Environment:

    kernel mode only

Revision History:

--*/

#ifndef _WDFMEMORY_1_5_H_
#define _WDFMEMORY_1_5_H_



#if (NTDDI_VERSION >= NTDDI_WIN2K)

typedef enum _WDF_MEMORY_DESCRIPTOR_TYPE {
    WdfMemoryDescriptorTypeInvalid = 0,
    WdfMemoryDescriptorTypeBuffer,
    WdfMemoryDescriptorTypeMdl,
    WdfMemoryDescriptorTypeHandle,
} WDF_MEMORY_DESCRIPTOR_TYPE;



typedef struct _WDFMEMORY_OFFSET {
    //
    // Offset into the WDFMEMORY that the operation should start at.
    //
    size_t BufferOffset;

    //
    // Number of bytes that the operation should access.  If 0, the entire
    // length of the WDFMEMORY buffer will be used in the operation or ignored
    // depending on the API.
    //
    size_t BufferLength;

} WDFMEMORY_OFFSET, *PWDFMEMORY_OFFSET;

typedef struct _WDF_MEMORY_DESCRIPTOR {
    WDF_MEMORY_DESCRIPTOR_TYPE Type;

    union {
        struct {
            PVOID Buffer;

            ULONG Length;
        } BufferType;

        struct {
            PMDL Mdl;

            ULONG BufferLength;
        } MdlType;

        struct {
            WDFMEMORY Memory;
            PWDFMEMORY_OFFSET Offsets;
        } HandleType;
    } u;

} WDF_MEMORY_DESCRIPTOR, *PWDF_MEMORY_DESCRIPTOR;

VOID
FORCEINLINE
WDF_MEMORY_DESCRIPTOR_INIT_BUFFER(
    OUT PWDF_MEMORY_DESCRIPTOR Descriptor,
    IN PVOID Buffer,
    IN ULONG BufferLength
    )
{
    RtlZeroMemory(Descriptor, sizeof(WDF_MEMORY_DESCRIPTOR));

    Descriptor->Type = WdfMemoryDescriptorTypeBuffer;
    Descriptor->u.BufferType.Buffer = Buffer;
    Descriptor->u.BufferType.Length = BufferLength;
}

VOID
FORCEINLINE
WDF_MEMORY_DESCRIPTOR_INIT_MDL(
    OUT PWDF_MEMORY_DESCRIPTOR Descriptor,
    IN PMDL Mdl,
    IN ULONG BufferLength
    )
{
    RtlZeroMemory(Descriptor, sizeof(WDF_MEMORY_DESCRIPTOR));

    Descriptor->Type = WdfMemoryDescriptorTypeMdl;
    Descriptor->u.MdlType.Mdl = Mdl;
    Descriptor->u.MdlType.BufferLength = BufferLength;
}

VOID
FORCEINLINE
WDF_MEMORY_DESCRIPTOR_INIT_HANDLE(
    OUT PWDF_MEMORY_DESCRIPTOR Descriptor,
    IN WDFMEMORY Memory,
    IN OPTIONAL PWDFMEMORY_OFFSET Offsets
    )
{
    RtlZeroMemory(Descriptor, sizeof(WDF_MEMORY_DESCRIPTOR));

    Descriptor->Type = WdfMemoryDescriptorTypeHandle;
    Descriptor->u.HandleType.Memory = Memory;
    Descriptor->u.HandleType.Offsets = Offsets;
}

//
// WDF Function: WdfMemoryCreate
//
typedef
WDFAPI
NTSTATUS
(*PFN_WDFMEMORYCREATE)(
    IN PWDF_DRIVER_GLOBALS DriverGlobals,
    PWDF_OBJECT_ATTRIBUTES Attributes,
    IN POOL_TYPE PoolType,
    IN OPTIONAL ULONG PoolTag,
    IN size_t BufferSize,
    WDFMEMORY* Memory,
    PVOID* Buffer
    );

_Must_inspect_result_
NTSTATUS
FORCEINLINE
WdfMemoryCreate(
    _In_opt_
    PWDF_OBJECT_ATTRIBUTES Attributes,
    IN POOL_TYPE PoolType,
    IN OPTIONAL ULONG PoolTag,
    IN size_t BufferSize,
    _Out_
    WDFMEMORY* Memory,
    _Out_opt_
    PVOID* Buffer
    )
{
    return ((PFN_WDFMEMORYCREATE) WdfFunctions[WdfMemoryCreateTableIndex])(WdfDriverGlobals, Attributes, PoolType, PoolTag, BufferSize, Memory, Buffer);
}

//
// WDF Function: WdfMemoryCreatePreallocated
//
typedef
WDFAPI
NTSTATUS
(*PFN_WDFMEMORYCREATEPREALLOCATED)(
    IN PWDF_DRIVER_GLOBALS DriverGlobals,
    PWDF_OBJECT_ATTRIBUTES Attributes,
    PVOID Buffer,
    IN size_t BufferSize,
    WDFMEMORY* Memory
    );

_Must_inspect_result_
NTSTATUS
FORCEINLINE
WdfMemoryCreatePreallocated(
    _In_opt_
    PWDF_OBJECT_ATTRIBUTES Attributes,
    _In_
    PVOID Buffer,
    IN size_t BufferSize,
    _Out_
    WDFMEMORY* Memory
    )
{
    return ((PFN_WDFMEMORYCREATEPREALLOCATED) WdfFunctions[WdfMemoryCreatePreallocatedTableIndex])(WdfDriverGlobals, Attributes, Buffer, BufferSize, Memory);
}

//
// WDF Function: WdfMemoryGetBuffer
//
typedef
WDFAPI
PVOID
(*PFN_WDFMEMORYGETBUFFER)(
    IN PWDF_DRIVER_GLOBALS DriverGlobals,
    WDFMEMORY Memory,
    size_t* BufferSize
    );

PVOID
FORCEINLINE
WdfMemoryGetBuffer(
    _In_
    WDFMEMORY Memory,
    _Out_opt_
    size_t* BufferSize
    )
{
    return ((PFN_WDFMEMORYGETBUFFER) WdfFunctions[WdfMemoryGetBufferTableIndex])(WdfDriverGlobals, Memory, BufferSize);
}

//
// WDF Function: WdfMemoryAssignBuffer
//
typedef
WDFAPI
NTSTATUS
(*PFN_WDFMEMORYASSIGNBUFFER)(
    _In_ PWDF_DRIVER_GLOBALS DriverGlobals,
    _In_ WDFMEMORY Memory,
    _Pre_notnull_ _Pre_writable_byte_size_(BufferSize) PVOID Buffer,
    _In_ size_t BufferSize
    );

_Must_inspect_result_
NTSTATUS
FORCEINLINE
WdfMemoryAssignBuffer(
    _In_
    WDFMEMORY Memory,
    _Pre_notnull_ _Pre_writable_byte_size_(BufferSize)
    PVOID Buffer,
    _In_ size_t BufferSize
    )
{
    return ((PFN_WDFMEMORYASSIGNBUFFER) WdfFunctions[WdfMemoryAssignBufferTableIndex])(WdfDriverGlobals, Memory, Buffer, BufferSize);
}

//
// WDF Function: WdfMemoryCopyToBuffer
//
typedef
WDFAPI
NTSTATUS
(*PFN_WDFMEMORYCOPYTOBUFFER)(
    _In_ PWDF_DRIVER_GLOBALS DriverGlobals,
    _In_ WDFMEMORY SourceMemory,
    _In_ size_t SourceOffset,
    _Out_writes_bytes_(NumBytesToCopyTo) PVOID Buffer,
    _In_ size_t NumBytesToCopyTo
    );

_Must_inspect_result_
NTSTATUS
FORCEINLINE
WdfMemoryCopyToBuffer(
    _In_
    WDFMEMORY SourceMemory,
    _In_ size_t SourceOffset,
    _Out_writes_bytes_( NumBytesToCopyTo )
    PVOID Buffer,
    _In_ size_t NumBytesToCopyTo
    )
{
    return ((PFN_WDFMEMORYCOPYTOBUFFER) WdfFunctions[WdfMemoryCopyToBufferTableIndex])(WdfDriverGlobals, SourceMemory, SourceOffset, Buffer, NumBytesToCopyTo);
}

//
// WDF Function: WdfMemoryCopyFromBuffer
//
typedef
WDFAPI
NTSTATUS
(*PFN_WDFMEMORYCOPYFROMBUFFER)(
    _In_ PWDF_DRIVER_GLOBALS DriverGlobals,
    _In_ WDFMEMORY DestinationMemory,
    _In_ size_t DestinationOffset,
    _In_reads_bytes_(NumBytesToCopyFrom) PVOID Buffer,
    _In_ size_t NumBytesToCopyFrom
    );

_Must_inspect_result_
NTSTATUS
FORCEINLINE
WdfMemoryCopyFromBuffer(
    _In_
    WDFMEMORY DestinationMemory,
    _In_ size_t DestinationOffset,
    _In_reads_bytes_(NumBytesToCopyFrom)
    PVOID Buffer,
    _In_ size_t NumBytesToCopyFrom
    )
{
    return ((PFN_WDFMEMORYCOPYFROMBUFFER) WdfFunctions[WdfMemoryCopyFromBufferTableIndex])(WdfDriverGlobals, DestinationMemory, DestinationOffset, Buffer, NumBytesToCopyFrom);
}

//
// WDF Function: WdfLookasideListCreate
//
typedef
WDFAPI
NTSTATUS
(*PFN_WDFLOOKASIDELISTCREATE)(
    IN PWDF_DRIVER_GLOBALS DriverGlobals,
    PWDF_OBJECT_ATTRIBUTES LookasideAttributes,
    IN size_t BufferSize,
    IN POOL_TYPE PoolType,
    PWDF_OBJECT_ATTRIBUTES MemoryAttributes,
    IN OPTIONAL ULONG PoolTag,
    WDFLOOKASIDE* Lookaside
    );

_Must_inspect_result_
NTSTATUS
FORCEINLINE
WdfLookasideListCreate(
    _In_opt_
    PWDF_OBJECT_ATTRIBUTES LookasideAttributes,
    IN size_t BufferSize,
    IN POOL_TYPE PoolType,
    _In_opt_
    PWDF_OBJECT_ATTRIBUTES MemoryAttributes,
    IN OPTIONAL ULONG PoolTag,
    _Out_
    WDFLOOKASIDE* Lookaside
    )
{
    return ((PFN_WDFLOOKASIDELISTCREATE) WdfFunctions[WdfLookasideListCreateTableIndex])(WdfDriverGlobals, LookasideAttributes, BufferSize, PoolType, MemoryAttributes, PoolTag, Lookaside);
}

//
// WDF Function: WdfMemoryCreateFromLookaside
//
typedef
WDFAPI
NTSTATUS
(*PFN_WDFMEMORYCREATEFROMLOOKASIDE)(
    IN PWDF_DRIVER_GLOBALS DriverGlobals,
    WDFLOOKASIDE Lookaside,
    WDFMEMORY* Memory
    );

_Must_inspect_result_
NTSTATUS
FORCEINLINE
WdfMemoryCreateFromLookaside(
    _In_
    WDFLOOKASIDE Lookaside,
    _Out_
    WDFMEMORY* Memory
    )
{
    return ((PFN_WDFMEMORYCREATEFROMLOOKASIDE) WdfFunctions[WdfMemoryCreateFromLookasideTableIndex])(WdfDriverGlobals, Lookaside, Memory);
}



#endif // (NTDDI_VERSION >= NTDDI_WIN2K)


#endif // _WDFMEMORY_1_5_H_

