/*++

Copyright (c) Microsoft Corporation.  All rights reserved.

_WdfVersionBuild_

Module Name:

    WdfPdo.h

Abstract:

    This is the interface to the PDO WDFDEVICE handle.

Environment:

    kernel mode only

Revision History:

--*/

#ifndef _WDFPDO_1_5_H_
#define _WDFPDO_1_5_H_



#if (NTDDI_VERSION >= NTDDI_WIN2K)



typedef
NTSTATUS
(*PFN_WDF_DEVICE_RESOURCES_QUERY)(
    IN WDFDEVICE Device,
    IN WDFCMRESLIST Resources
    );

typedef
NTSTATUS
(*PFN_WDF_DEVICE_RESOURCE_REQUIREMENTS_QUERY)(
    IN WDFDEVICE Device,
    IN WDFIORESREQLIST IoResourceRequirementsList
    );

typedef
NTSTATUS
(*PFN_WDF_DEVICE_EJECT)(
    IN WDFDEVICE Device
    );

typedef
NTSTATUS
(*PFN_WDF_DEVICE_SET_LOCK)(
    IN WDFDEVICE Device,
    IN BOOLEAN IsLocked
    );

typedef
NTSTATUS
(*PFN_WDF_DEVICE_ENABLE_WAKE_AT_BUS)(
    IN WDFDEVICE Device,
    IN SYSTEM_POWER_STATE PowerState
    );

typedef
VOID
(*PFN_WDF_DEVICE_DISABLE_WAKE_AT_BUS)(
    IN WDFDEVICE Device
    );

typedef struct _WDF_PDO_EVENT_CALLBACKS {
    //
    // The size of this structure in bytes
    //
    ULONG Size;

    //
    // Called in response to IRP_MN_QUERY_RESOURCES
    //
    PFN_WDF_DEVICE_RESOURCES_QUERY EvtDeviceResourcesQuery;

    //
    // Called in response to IRP_MN_QUERY_RESOURCE_REQUIREMENTS
    //
    PFN_WDF_DEVICE_RESOURCE_REQUIREMENTS_QUERY EvtDeviceResourceRequirementsQuery;

    //
    // Called in response to IRP_MN_EJECT
    //
    PFN_WDF_DEVICE_EJECT EvtDeviceEject;

    //
    // Called in response to IRP_MN_SET_LOCK
    //
    PFN_WDF_DEVICE_SET_LOCK EvtDeviceSetLock;

    //
    // Called in response to the power policy owner sending a wait wake to the
    // PDO.  Bus generic arming shoulding occur here.
    //
    PFN_WDF_DEVICE_ENABLE_WAKE_AT_BUS       EvtDeviceEnableWakeAtBus;

    //
    // Called in response to the power policy owner sending a wait wake to the
    // PDO.  Bus generic disarming shoulding occur here.
    //
    PFN_WDF_DEVICE_DISABLE_WAKE_AT_BUS      EvtDeviceDisableWakeAtBus;

} WDF_PDO_EVENT_CALLBACKS, *PWDF_PDO_EVENT_CALLBACKS;

VOID
FORCEINLINE
WDF_PDO_EVENT_CALLBACKS_INIT(
    OUT PWDF_PDO_EVENT_CALLBACKS Callbacks
    )
{
    RtlZeroMemory(Callbacks, sizeof(WDF_PDO_EVENT_CALLBACKS));
    Callbacks->Size = sizeof(WDF_PDO_EVENT_CALLBACKS);
}

//
// WDF Function: WdfPdoInitAllocate
//
typedef
WDFAPI
PWDFDEVICE_INIT
(*PFN_WDFPDOINITALLOCATE)(
    IN PWDF_DRIVER_GLOBALS DriverGlobals,
    WDFDEVICE ParentDevice
    );

_Must_inspect_result_
PWDFDEVICE_INIT
FORCEINLINE
WdfPdoInitAllocate(
    _In_
    WDFDEVICE ParentDevice
    )
{
    return ((PFN_WDFPDOINITALLOCATE) WdfFunctions[WdfPdoInitAllocateTableIndex])(WdfDriverGlobals, ParentDevice);
}

//
// WDF Function: WdfPdoInitSetEventCallbacks
//
typedef
WDFAPI
VOID
(*PFN_WDFPDOINITSETEVENTCALLBACKS)(
    IN PWDF_DRIVER_GLOBALS DriverGlobals,
    PWDFDEVICE_INIT DeviceInit,
    PWDF_PDO_EVENT_CALLBACKS DispatchTable
    );

VOID
FORCEINLINE
WdfPdoInitSetEventCallbacks(
    _In_
    PWDFDEVICE_INIT DeviceInit,
    _In_
    PWDF_PDO_EVENT_CALLBACKS DispatchTable
    )
{
    ((PFN_WDFPDOINITSETEVENTCALLBACKS) WdfFunctions[WdfPdoInitSetEventCallbacksTableIndex])(WdfDriverGlobals, DeviceInit, DispatchTable);
}

//
// WDF Function: WdfPdoInitAssignDeviceID
//
typedef
WDFAPI
NTSTATUS
(*PFN_WDFPDOINITASSIGNDEVICEID)(
    IN PWDF_DRIVER_GLOBALS DriverGlobals,
    PWDFDEVICE_INIT DeviceInit,
    PCUNICODE_STRING DeviceID
    );

_Must_inspect_result_
NTSTATUS
FORCEINLINE
WdfPdoInitAssignDeviceID(
    _In_
    PWDFDEVICE_INIT DeviceInit,
    _In_
    PCUNICODE_STRING DeviceID
    )
{
    return ((PFN_WDFPDOINITASSIGNDEVICEID) WdfFunctions[WdfPdoInitAssignDeviceIDTableIndex])(WdfDriverGlobals, DeviceInit, DeviceID);
}

//
// WDF Function: WdfPdoInitAssignInstanceID
//
typedef
WDFAPI
NTSTATUS
(*PFN_WDFPDOINITASSIGNINSTANCEID)(
    IN PWDF_DRIVER_GLOBALS DriverGlobals,
    PWDFDEVICE_INIT DeviceInit,
    PCUNICODE_STRING InstanceID
    );

_Must_inspect_result_
NTSTATUS
FORCEINLINE
WdfPdoInitAssignInstanceID(
    _In_
    PWDFDEVICE_INIT DeviceInit,
    _In_
    PCUNICODE_STRING InstanceID
    )
{
    return ((PFN_WDFPDOINITASSIGNINSTANCEID) WdfFunctions[WdfPdoInitAssignInstanceIDTableIndex])(WdfDriverGlobals, DeviceInit, InstanceID);
}

//
// WDF Function: WdfPdoInitAddHardwareID
//
typedef
WDFAPI
NTSTATUS
(*PFN_WDFPDOINITADDHARDWAREID)(
    IN PWDF_DRIVER_GLOBALS DriverGlobals,
    PWDFDEVICE_INIT DeviceInit,
    PCUNICODE_STRING HardwareID
    );

_Must_inspect_result_
NTSTATUS
FORCEINLINE
WdfPdoInitAddHardwareID(
    _In_
    PWDFDEVICE_INIT DeviceInit,
    _In_
    PCUNICODE_STRING HardwareID
    )
{
    return ((PFN_WDFPDOINITADDHARDWAREID) WdfFunctions[WdfPdoInitAddHardwareIDTableIndex])(WdfDriverGlobals, DeviceInit, HardwareID);
}

//
// WDF Function: WdfPdoInitAddCompatibleID
//
typedef
WDFAPI
NTSTATUS
(*PFN_WDFPDOINITADDCOMPATIBLEID)(
    IN PWDF_DRIVER_GLOBALS DriverGlobals,
    PWDFDEVICE_INIT DeviceInit,
    PCUNICODE_STRING CompatibleID
    );

_Must_inspect_result_
NTSTATUS
FORCEINLINE
WdfPdoInitAddCompatibleID(
    _In_
    PWDFDEVICE_INIT DeviceInit,
    _In_
    PCUNICODE_STRING CompatibleID
    )
{
    return ((PFN_WDFPDOINITADDCOMPATIBLEID) WdfFunctions[WdfPdoInitAddCompatibleIDTableIndex])(WdfDriverGlobals, DeviceInit, CompatibleID);
}

//
// WDF Function: WdfPdoInitAddDeviceText
//
typedef
WDFAPI
NTSTATUS
(*PFN_WDFPDOINITADDDEVICETEXT)(
    IN PWDF_DRIVER_GLOBALS DriverGlobals,
    PWDFDEVICE_INIT DeviceInit,
    PCUNICODE_STRING DeviceDescription,
    PCUNICODE_STRING DeviceLocation,
    IN LCID LocaleId
    );

_Must_inspect_result_
NTSTATUS
FORCEINLINE
WdfPdoInitAddDeviceText(
    _In_
    PWDFDEVICE_INIT DeviceInit,
    _In_
    PCUNICODE_STRING DeviceDescription,
    _In_
    PCUNICODE_STRING DeviceLocation,
    IN LCID LocaleId
    )
{
    return ((PFN_WDFPDOINITADDDEVICETEXT) WdfFunctions[WdfPdoInitAddDeviceTextTableIndex])(WdfDriverGlobals, DeviceInit, DeviceDescription, DeviceLocation, LocaleId);
}

//
// WDF Function: WdfPdoInitSetDefaultLocale
//
typedef
WDFAPI
VOID
(*PFN_WDFPDOINITSETDEFAULTLOCALE)(
    IN PWDF_DRIVER_GLOBALS DriverGlobals,
    PWDFDEVICE_INIT DeviceInit,
    IN LCID LocaleId
    );

VOID
FORCEINLINE
WdfPdoInitSetDefaultLocale(
    _In_
    PWDFDEVICE_INIT DeviceInit,
    IN LCID LocaleId
    )
{
    ((PFN_WDFPDOINITSETDEFAULTLOCALE) WdfFunctions[WdfPdoInitSetDefaultLocaleTableIndex])(WdfDriverGlobals, DeviceInit, LocaleId);
}

//
// WDF Function: WdfPdoInitAssignRawDevice
//
typedef
WDFAPI
NTSTATUS
(*PFN_WDFPDOINITASSIGNRAWDEVICE)(
    IN PWDF_DRIVER_GLOBALS DriverGlobals,
    PWDFDEVICE_INIT DeviceInit,
    CONST GUID* DeviceClassGuid
    );

_Must_inspect_result_
NTSTATUS
FORCEINLINE
WdfPdoInitAssignRawDevice(
    _In_
    PWDFDEVICE_INIT DeviceInit,
    _In_
    CONST GUID* DeviceClassGuid
    )
{
    return ((PFN_WDFPDOINITASSIGNRAWDEVICE) WdfFunctions[WdfPdoInitAssignRawDeviceTableIndex])(WdfDriverGlobals, DeviceInit, DeviceClassGuid);
}

//
// WDF Function: WdfPdoMarkMissing
//
typedef
WDFAPI
NTSTATUS
(*PFN_WDFPDOMARKMISSING)(
    IN PWDF_DRIVER_GLOBALS DriverGlobals,
    WDFDEVICE Device
    );

_Must_inspect_result_
NTSTATUS
FORCEINLINE
WdfPdoMarkMissing(
    _In_
    WDFDEVICE Device
    )
{
    return ((PFN_WDFPDOMARKMISSING) WdfFunctions[WdfPdoMarkMissingTableIndex])(WdfDriverGlobals, Device);
}

//
// WDF Function: WdfPdoRequestEject
//
typedef
WDFAPI
VOID
(*PFN_WDFPDOREQUESTEJECT)(
    IN PWDF_DRIVER_GLOBALS DriverGlobals,
    WDFDEVICE Device
    );

VOID
FORCEINLINE
WdfPdoRequestEject(
    _In_
    WDFDEVICE Device
    )
{
    ((PFN_WDFPDOREQUESTEJECT) WdfFunctions[WdfPdoRequestEjectTableIndex])(WdfDriverGlobals, Device);
}

//
// WDF Function: WdfPdoGetParent
//
typedef
WDFAPI
WDFDEVICE
(*PFN_WDFPDOGETPARENT)(
    IN PWDF_DRIVER_GLOBALS DriverGlobals,
    WDFDEVICE Device
    );

WDFDEVICE
FORCEINLINE
WdfPdoGetParent(
    _In_
    WDFDEVICE Device
    )
{
    return ((PFN_WDFPDOGETPARENT) WdfFunctions[WdfPdoGetParentTableIndex])(WdfDriverGlobals, Device);
}

//
// WDF Function: WdfPdoRetrieveIdentificationDescription
//
typedef
WDFAPI
NTSTATUS
(*PFN_WDFPDORETRIEVEIDENTIFICATIONDESCRIPTION)(
    IN PWDF_DRIVER_GLOBALS DriverGlobals,
    WDFDEVICE Device,
    PWDF_CHILD_IDENTIFICATION_DESCRIPTION_HEADER IdentificationDescription
    );

_Must_inspect_result_
NTSTATUS
FORCEINLINE
WdfPdoRetrieveIdentificationDescription(
    _In_
    WDFDEVICE Device,
    _Inout_
    PWDF_CHILD_IDENTIFICATION_DESCRIPTION_HEADER IdentificationDescription
    )
{
    return ((PFN_WDFPDORETRIEVEIDENTIFICATIONDESCRIPTION) WdfFunctions[WdfPdoRetrieveIdentificationDescriptionTableIndex])(WdfDriverGlobals, Device, IdentificationDescription);
}

//
// WDF Function: WdfPdoRetrieveAddressDescription
//
typedef
WDFAPI
NTSTATUS
(*PFN_WDFPDORETRIEVEADDRESSDESCRIPTION)(
    IN PWDF_DRIVER_GLOBALS DriverGlobals,
    WDFDEVICE Device,
    PWDF_CHILD_ADDRESS_DESCRIPTION_HEADER AddressDescription
    );

_Must_inspect_result_
NTSTATUS
FORCEINLINE
WdfPdoRetrieveAddressDescription(
    _In_
    WDFDEVICE Device,
    _Inout_
    PWDF_CHILD_ADDRESS_DESCRIPTION_HEADER AddressDescription
    )
{
    return ((PFN_WDFPDORETRIEVEADDRESSDESCRIPTION) WdfFunctions[WdfPdoRetrieveAddressDescriptionTableIndex])(WdfDriverGlobals, Device, AddressDescription);
}

//
// WDF Function: WdfPdoUpdateAddressDescription
//
typedef
WDFAPI
NTSTATUS
(*PFN_WDFPDOUPDATEADDRESSDESCRIPTION)(
    IN PWDF_DRIVER_GLOBALS DriverGlobals,
    WDFDEVICE Device,
    PWDF_CHILD_ADDRESS_DESCRIPTION_HEADER AddressDescription
    );

_Must_inspect_result_
NTSTATUS
FORCEINLINE
WdfPdoUpdateAddressDescription(
    _In_
    WDFDEVICE Device,
    _Inout_
    PWDF_CHILD_ADDRESS_DESCRIPTION_HEADER AddressDescription
    )
{
    return ((PFN_WDFPDOUPDATEADDRESSDESCRIPTION) WdfFunctions[WdfPdoUpdateAddressDescriptionTableIndex])(WdfDriverGlobals, Device, AddressDescription);
}

//
// WDF Function: WdfPdoAddEjectionRelationsPhysicalDevice
//
typedef
WDFAPI
NTSTATUS
(*PFN_WDFPDOADDEJECTIONRELATIONSPHYSICALDEVICE)(
    IN PWDF_DRIVER_GLOBALS DriverGlobals,
    WDFDEVICE Device,
    PDEVICE_OBJECT PhysicalDevice
    );

_Must_inspect_result_
NTSTATUS
FORCEINLINE
WdfPdoAddEjectionRelationsPhysicalDevice(
    _In_
    WDFDEVICE Device,
    _In_
    PDEVICE_OBJECT PhysicalDevice
    )
{
    return ((PFN_WDFPDOADDEJECTIONRELATIONSPHYSICALDEVICE) WdfFunctions[WdfPdoAddEjectionRelationsPhysicalDeviceTableIndex])(WdfDriverGlobals, Device, PhysicalDevice);
}

//
// WDF Function: WdfPdoRemoveEjectionRelationsPhysicalDevice
//
typedef
WDFAPI
VOID
(*PFN_WDFPDOREMOVEEJECTIONRELATIONSPHYSICALDEVICE)(
    IN PWDF_DRIVER_GLOBALS DriverGlobals,
    WDFDEVICE Device,
    PDEVICE_OBJECT PhysicalDevice
    );

VOID
FORCEINLINE
WdfPdoRemoveEjectionRelationsPhysicalDevice(
    _In_
    WDFDEVICE Device,
    _In_
    PDEVICE_OBJECT PhysicalDevice
    )
{
    ((PFN_WDFPDOREMOVEEJECTIONRELATIONSPHYSICALDEVICE) WdfFunctions[WdfPdoRemoveEjectionRelationsPhysicalDeviceTableIndex])(WdfDriverGlobals, Device, PhysicalDevice);
}

//
// WDF Function: WdfPdoClearEjectionRelationsDevices
//
typedef
WDFAPI
VOID
(*PFN_WDFPDOCLEAREJECTIONRELATIONSDEVICES)(
    IN PWDF_DRIVER_GLOBALS DriverGlobals,
    WDFDEVICE Device
    );

VOID
FORCEINLINE
WdfPdoClearEjectionRelationsDevices(
    _In_
    WDFDEVICE Device
    )
{
    ((PFN_WDFPDOCLEAREJECTIONRELATIONSDEVICES) WdfFunctions[WdfPdoClearEjectionRelationsDevicesTableIndex])(WdfDriverGlobals, Device);
}



#endif // (NTDDI_VERSION >= NTDDI_WIN2K)


#endif // _WDFPDO_1_5_H_

