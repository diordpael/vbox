/*++

Copyright (c) Microsoft Corporation.  All rights reserved.

_WdfVersionBuild_

Module Name:

    wdfminiport.h

Abstract:

    Interfaces for WDF usage in a miniport environment

Environment:

    kernel mode only

Revision History:

--*/

#ifndef _WDFMINIPORT_1_5_H_
#define _WDFMINIPORT_1_5_H_



#if (NTDDI_VERSION >= NTDDI_WIN2K)



//
// WDF Function: WdfDeviceMiniportCreate
//
typedef
WDFAPI
NTSTATUS
(*PFN_WDFDEVICEMINIPORTCREATE)(
    IN PWDF_DRIVER_GLOBALS DriverGlobals,
    WDFDRIVER Driver,
    PWDF_OBJECT_ATTRIBUTES Attributes,
    PDEVICE_OBJECT DeviceObject,
    PDEVICE_OBJECT AttachedDeviceObject,
    PDEVICE_OBJECT Pdo,
    WDFDEVICE* Device
    );

_Must_inspect_result_
NTSTATUS
FORCEINLINE
WdfDeviceMiniportCreate(
    _In_
    WDFDRIVER Driver,
    _In_opt_
    PWDF_OBJECT_ATTRIBUTES Attributes,
    _In_
    PDEVICE_OBJECT DeviceObject,
    _In_opt_
    PDEVICE_OBJECT AttachedDeviceObject,
    _In_opt_
    PDEVICE_OBJECT Pdo,
    _Out_
    WDFDEVICE* Device
    )
{
    return ((PFN_WDFDEVICEMINIPORTCREATE) WdfFunctions[WdfDeviceMiniportCreateTableIndex])(WdfDriverGlobals, Driver, Attributes, DeviceObject, AttachedDeviceObject, Pdo, Device);
}

//
// WDF Function: WdfDriverMiniportUnload
//
typedef
WDFAPI
VOID
(*PFN_WDFDRIVERMINIPORTUNLOAD)(
    IN PWDF_DRIVER_GLOBALS DriverGlobals,
    WDFDRIVER Driver
    );

VOID
FORCEINLINE
WdfDriverMiniportUnload(
    _In_
    WDFDRIVER Driver
    )
{
    ((PFN_WDFDRIVERMINIPORTUNLOAD) WdfFunctions[WdfDriverMiniportUnloadTableIndex])(WdfDriverGlobals, Driver);
}



#endif // (NTDDI_VERSION >= NTDDI_WIN2K)


#endif // _WDFMINIPORT_1_5_H_

