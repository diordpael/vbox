/*++

Copyright (c) Microsoft Corporation.  All rights reserved.

_WdfVersionBuild_

Module Name:

    WdfRegistry.h

Abstract:

    This is the interface to registry access.

Environment:

    kernel mode only

Revision History:

--*/

#ifndef _WDFREGISTRY_1_5_H_
#define _WDFREGISTRY_1_5_H_



#if (NTDDI_VERSION >= NTDDI_WIN2K)



//
// WDF Function: WdfRegistryOpenKey
//
typedef
WDFAPI
NTSTATUS
(*PFN_WDFREGISTRYOPENKEY)(
    IN PWDF_DRIVER_GLOBALS DriverGlobals,
    WDFKEY ParentKey,
    PCUNICODE_STRING KeyName,
    IN ACCESS_MASK DesiredAccess,
    PWDF_OBJECT_ATTRIBUTES KeyAttributes,
    WDFKEY* Key
    );

_Must_inspect_result_
NTSTATUS
FORCEINLINE
WdfRegistryOpenKey(
    _In_opt_
    WDFKEY ParentKey,
    _In_
    PCUNICODE_STRING KeyName,
    IN ACCESS_MASK DesiredAccess,
    _In_opt_
    PWDF_OBJECT_ATTRIBUTES KeyAttributes,
    _Out_
    WDFKEY* Key
    )
{
    return ((PFN_WDFREGISTRYOPENKEY) WdfFunctions[WdfRegistryOpenKeyTableIndex])(WdfDriverGlobals, ParentKey, KeyName, DesiredAccess, KeyAttributes, Key);
}

//
// WDF Function: WdfRegistryCreateKey
//
typedef
WDFAPI
NTSTATUS
(*PFN_WDFREGISTRYCREATEKEY)(
    IN PWDF_DRIVER_GLOBALS DriverGlobals,
    WDFKEY ParentKey,
    PCUNICODE_STRING KeyName,
    IN ACCESS_MASK DesiredAccess,
    IN ULONG CreateOptions,
    PULONG CreateDisposition,
    PWDF_OBJECT_ATTRIBUTES KeyAttributes,
    WDFKEY* Key
    );

_Must_inspect_result_
NTSTATUS
FORCEINLINE
WdfRegistryCreateKey(
    _In_opt_
    WDFKEY ParentKey,
    _In_
    PCUNICODE_STRING KeyName,
    IN ACCESS_MASK DesiredAccess,
    IN ULONG CreateOptions,
    _Out_opt_
    PULONG CreateDisposition,
    _In_opt_
    PWDF_OBJECT_ATTRIBUTES KeyAttributes,
    _Out_
    WDFKEY* Key
    )
{
    return ((PFN_WDFREGISTRYCREATEKEY) WdfFunctions[WdfRegistryCreateKeyTableIndex])(WdfDriverGlobals, ParentKey, KeyName, DesiredAccess, CreateOptions, CreateDisposition, KeyAttributes, Key);
}

//
// WDF Function: WdfRegistryClose
//
typedef
WDFAPI
VOID
(*PFN_WDFREGISTRYCLOSE)(
    IN PWDF_DRIVER_GLOBALS DriverGlobals,
    WDFKEY Key
    );

VOID
FORCEINLINE
WdfRegistryClose(
    _In_
    WDFKEY Key
    )
{
    ((PFN_WDFREGISTRYCLOSE) WdfFunctions[WdfRegistryCloseTableIndex])(WdfDriverGlobals, Key);
}

//
// WDF Function: WdfRegistryWdmGetHandle
//
typedef
WDFAPI
HANDLE
(*PFN_WDFREGISTRYWDMGETHANDLE)(
    IN PWDF_DRIVER_GLOBALS DriverGlobals,
    WDFKEY Key
    );

HANDLE
FORCEINLINE
WdfRegistryWdmGetHandle(
    _In_
    WDFKEY Key
    )
{
    return ((PFN_WDFREGISTRYWDMGETHANDLE) WdfFunctions[WdfRegistryWdmGetHandleTableIndex])(WdfDriverGlobals, Key);
}

//
// WDF Function: WdfRegistryRemoveKey
//
typedef
WDFAPI
NTSTATUS
(*PFN_WDFREGISTRYREMOVEKEY)(
    IN PWDF_DRIVER_GLOBALS DriverGlobals,
    WDFKEY Key
    );

_Must_inspect_result_
NTSTATUS
FORCEINLINE
WdfRegistryRemoveKey(
    _In_
    WDFKEY Key
    )
{
    return ((PFN_WDFREGISTRYREMOVEKEY) WdfFunctions[WdfRegistryRemoveKeyTableIndex])(WdfDriverGlobals, Key);
}

//
// WDF Function: WdfRegistryRemoveValue
//
typedef
WDFAPI
NTSTATUS
(*PFN_WDFREGISTRYREMOVEVALUE)(
    IN PWDF_DRIVER_GLOBALS DriverGlobals,
    WDFKEY Key,
    PCUNICODE_STRING ValueName
    );

_Must_inspect_result_
NTSTATUS
FORCEINLINE
WdfRegistryRemoveValue(
    _In_
    WDFKEY Key,
    _In_
    PCUNICODE_STRING ValueName
    )
{
    return ((PFN_WDFREGISTRYREMOVEVALUE) WdfFunctions[WdfRegistryRemoveValueTableIndex])(WdfDriverGlobals, Key, ValueName);
}

//
// WDF Function: WdfRegistryQueryValue
//
typedef
WDFAPI
NTSTATUS
(*PFN_WDFREGISTRYQUERYVALUE)(
    IN PWDF_DRIVER_GLOBALS DriverGlobals,
    WDFKEY Key,
    PCUNICODE_STRING ValueName,
    IN ULONG ValueLength,
    PVOID Value,
    PULONG ValueLengthQueried,
    PULONG ValueType
    );

_Must_inspect_result_
NTSTATUS
FORCEINLINE
WdfRegistryQueryValue(
    _In_
    WDFKEY Key,
    _In_
    PCUNICODE_STRING ValueName,
    IN ULONG ValueLength,
    _Out_writes_bytes_opt_( ValueLength)
    PVOID Value,
    _Out_opt_
    PULONG ValueLengthQueried,
    _Out_opt_
    PULONG ValueType
    )
{
    return ((PFN_WDFREGISTRYQUERYVALUE) WdfFunctions[WdfRegistryQueryValueTableIndex])(WdfDriverGlobals, Key, ValueName, ValueLength, Value, ValueLengthQueried, ValueType);
}

//
// WDF Function: WdfRegistryQueryMemory
//
typedef
WDFAPI
NTSTATUS
(*PFN_WDFREGISTRYQUERYMEMORY)(
    IN PWDF_DRIVER_GLOBALS DriverGlobals,
    WDFKEY Key,
    PCUNICODE_STRING ValueName,
    IN POOL_TYPE PoolType,
    PWDF_OBJECT_ATTRIBUTES MemoryAttributes,
    WDFMEMORY* Memory,
    PULONG ValueType
    );

_Must_inspect_result_
NTSTATUS
FORCEINLINE
WdfRegistryQueryMemory(
    _In_
    WDFKEY Key,
    _In_
    PCUNICODE_STRING ValueName,
    IN POOL_TYPE PoolType,
    _In_opt_
    PWDF_OBJECT_ATTRIBUTES MemoryAttributes,
    _Out_
    WDFMEMORY* Memory,
    _Out_opt_
    PULONG ValueType
    )
{
    return ((PFN_WDFREGISTRYQUERYMEMORY) WdfFunctions[WdfRegistryQueryMemoryTableIndex])(WdfDriverGlobals, Key, ValueName, PoolType, MemoryAttributes, Memory, ValueType);
}

//
// WDF Function: WdfRegistryQueryMultiString
//
typedef
WDFAPI
NTSTATUS
(*PFN_WDFREGISTRYQUERYMULTISTRING)(
    IN PWDF_DRIVER_GLOBALS DriverGlobals,
    WDFKEY Key,
    PCUNICODE_STRING ValueName,
    PWDF_OBJECT_ATTRIBUTES StringsAttributes,
    WDFCOLLECTION Collection
    );

_Must_inspect_result_
NTSTATUS
FORCEINLINE
WdfRegistryQueryMultiString(
    _In_
    WDFKEY Key,
    _In_
    PCUNICODE_STRING ValueName,
    _In_opt_
    PWDF_OBJECT_ATTRIBUTES StringsAttributes,
    _In_
    WDFCOLLECTION Collection
    )
{
    return ((PFN_WDFREGISTRYQUERYMULTISTRING) WdfFunctions[WdfRegistryQueryMultiStringTableIndex])(WdfDriverGlobals, Key, ValueName, StringsAttributes, Collection);
}

//
// WDF Function: WdfRegistryQueryUnicodeString
//
typedef
WDFAPI
NTSTATUS
(*PFN_WDFREGISTRYQUERYUNICODESTRING)(
    IN PWDF_DRIVER_GLOBALS DriverGlobals,
    WDFKEY Key,
    PCUNICODE_STRING ValueName,
    PUSHORT ValueByteLength,
    PUNICODE_STRING Value
    );

_Must_inspect_result_
NTSTATUS
FORCEINLINE
WdfRegistryQueryUnicodeString(
    _In_
    WDFKEY Key,
    _In_
    PCUNICODE_STRING ValueName,
    _Out_opt_
    PUSHORT ValueByteLength,
    _Inout_opt_
    PUNICODE_STRING Value
    )
{
    return ((PFN_WDFREGISTRYQUERYUNICODESTRING) WdfFunctions[WdfRegistryQueryUnicodeStringTableIndex])(WdfDriverGlobals, Key, ValueName, ValueByteLength, Value);
}

//
// WDF Function: WdfRegistryQueryString
//
typedef
WDFAPI
NTSTATUS
(*PFN_WDFREGISTRYQUERYSTRING)(
    IN PWDF_DRIVER_GLOBALS DriverGlobals,
    WDFKEY Key,
    PCUNICODE_STRING ValueName,
    IN WDFSTRING String
    );

_Must_inspect_result_
NTSTATUS
FORCEINLINE
WdfRegistryQueryString(
    _In_
    WDFKEY Key,
    _In_
    PCUNICODE_STRING ValueName,
    IN WDFSTRING String
    )
{
    return ((PFN_WDFREGISTRYQUERYSTRING) WdfFunctions[WdfRegistryQueryStringTableIndex])(WdfDriverGlobals, Key, ValueName, String);
}

//
// WDF Function: WdfRegistryQueryULong
//
typedef
WDFAPI
NTSTATUS
(*PFN_WDFREGISTRYQUERYULONG)(
    IN PWDF_DRIVER_GLOBALS DriverGlobals,
    WDFKEY Key,
    PCUNICODE_STRING ValueName,
    PULONG Value
    );

_Must_inspect_result_
NTSTATUS
FORCEINLINE
WdfRegistryQueryULong(
    _In_
    WDFKEY Key,
    _In_
    PCUNICODE_STRING ValueName,
    _Out_
    PULONG Value
    )
{
    return ((PFN_WDFREGISTRYQUERYULONG) WdfFunctions[WdfRegistryQueryULongTableIndex])(WdfDriverGlobals, Key, ValueName, Value);
}

//
// WDF Function: WdfRegistryAssignValue
//
typedef
WDFAPI
NTSTATUS
(*PFN_WDFREGISTRYASSIGNVALUE)(
    IN PWDF_DRIVER_GLOBALS DriverGlobals,
    WDFKEY Key,
    PCUNICODE_STRING ValueName,
    IN ULONG ValueType,
    IN ULONG ValueLength,
    PVOID Value
    );

_Must_inspect_result_
NTSTATUS
FORCEINLINE
WdfRegistryAssignValue(
    _In_
    WDFKEY Key,
    _In_
    PCUNICODE_STRING ValueName,
    IN ULONG ValueType,
    IN ULONG ValueLength,
    _In_reads_( ValueLength)
    PVOID Value
    )
{
    return ((PFN_WDFREGISTRYASSIGNVALUE) WdfFunctions[WdfRegistryAssignValueTableIndex])(WdfDriverGlobals, Key, ValueName, ValueType, ValueLength, Value);
}

//
// WDF Function: WdfRegistryAssignMemory
//
typedef
WDFAPI
NTSTATUS
(*PFN_WDFREGISTRYASSIGNMEMORY)(
    IN PWDF_DRIVER_GLOBALS DriverGlobals,
    WDFKEY Key,
    PCUNICODE_STRING ValueName,
    IN ULONG ValueType,
    IN WDFMEMORY Memory,
    PWDFMEMORY_OFFSET MemoryOffsets
    );

_Must_inspect_result_
NTSTATUS
FORCEINLINE
WdfRegistryAssignMemory(
    _In_
    WDFKEY Key,
    _In_
    PCUNICODE_STRING ValueName,
    IN ULONG ValueType,
    IN WDFMEMORY Memory,
    _In_opt_
    PWDFMEMORY_OFFSET MemoryOffsets
    )
{
    return ((PFN_WDFREGISTRYASSIGNMEMORY) WdfFunctions[WdfRegistryAssignMemoryTableIndex])(WdfDriverGlobals, Key, ValueName, ValueType, Memory, MemoryOffsets);
}

//
// WDF Function: WdfRegistryAssignMultiString
//
typedef
WDFAPI
NTSTATUS
(*PFN_WDFREGISTRYASSIGNMULTISTRING)(
    IN PWDF_DRIVER_GLOBALS DriverGlobals,
    WDFKEY Key,
    PCUNICODE_STRING ValueName,
    IN WDFCOLLECTION StringsCollection
    );

_Must_inspect_result_
NTSTATUS
FORCEINLINE
WdfRegistryAssignMultiString(
    _In_
    WDFKEY Key,
    _In_
    PCUNICODE_STRING ValueName,
    IN WDFCOLLECTION StringsCollection
    )
{
    return ((PFN_WDFREGISTRYASSIGNMULTISTRING) WdfFunctions[WdfRegistryAssignMultiStringTableIndex])(WdfDriverGlobals, Key, ValueName, StringsCollection);
}

//
// WDF Function: WdfRegistryAssignUnicodeString
//
typedef
WDFAPI
NTSTATUS
(*PFN_WDFREGISTRYASSIGNUNICODESTRING)(
    IN PWDF_DRIVER_GLOBALS DriverGlobals,
    WDFKEY Key,
    PCUNICODE_STRING ValueName,
    PCUNICODE_STRING Value
    );

_Must_inspect_result_
NTSTATUS
FORCEINLINE
WdfRegistryAssignUnicodeString(
    _In_
    WDFKEY Key,
    _In_
    PCUNICODE_STRING ValueName,
    _In_
    PCUNICODE_STRING Value
    )
{
    return ((PFN_WDFREGISTRYASSIGNUNICODESTRING) WdfFunctions[WdfRegistryAssignUnicodeStringTableIndex])(WdfDriverGlobals, Key, ValueName, Value);
}

//
// WDF Function: WdfRegistryAssignString
//
typedef
WDFAPI
NTSTATUS
(*PFN_WDFREGISTRYASSIGNSTRING)(
    IN PWDF_DRIVER_GLOBALS DriverGlobals,
    WDFKEY Key,
    PCUNICODE_STRING ValueName,
    IN WDFSTRING String
    );

_Must_inspect_result_
NTSTATUS
FORCEINLINE
WdfRegistryAssignString(
    _In_
    WDFKEY Key,
    _In_
    PCUNICODE_STRING ValueName,
    IN WDFSTRING String
    )
{
    return ((PFN_WDFREGISTRYASSIGNSTRING) WdfFunctions[WdfRegistryAssignStringTableIndex])(WdfDriverGlobals, Key, ValueName, String);
}

//
// WDF Function: WdfRegistryAssignULong
//
typedef
WDFAPI
NTSTATUS
(*PFN_WDFREGISTRYASSIGNULONG)(
    IN PWDF_DRIVER_GLOBALS DriverGlobals,
    WDFKEY Key,
    PCUNICODE_STRING ValueName,
    IN ULONG Value
    );

_Must_inspect_result_
NTSTATUS
FORCEINLINE
WdfRegistryAssignULong(
    _In_
    WDFKEY Key,
    _In_
    PCUNICODE_STRING ValueName,
    IN ULONG Value
    )
{
    return ((PFN_WDFREGISTRYASSIGNULONG) WdfFunctions[WdfRegistryAssignULongTableIndex])(WdfDriverGlobals, Key, ValueName, Value);
}



#endif // (NTDDI_VERSION >= NTDDI_WIN2K)


#endif // _WDFREGISTRY_1_5_H_

