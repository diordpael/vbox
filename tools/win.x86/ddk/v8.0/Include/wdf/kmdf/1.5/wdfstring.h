/*++

Copyright (c) Microsoft Corporation.  All rights reserved.

_WdfVersionBuild_

Module Name:

    WdfString.h

Abstract:

    This is the DDI for string handles.

Environment:

    kernel mode only

Revision History:

--*/

#ifndef _WDFSTRING_1_5_H_
#define _WDFSTRING_1_5_H_



#if (NTDDI_VERSION >= NTDDI_WIN2K)



//
// WDF Function: WdfStringCreate
//
typedef
WDFAPI
NTSTATUS
(*PFN_WDFSTRINGCREATE)(
    IN PWDF_DRIVER_GLOBALS DriverGlobals,
    PCUNICODE_STRING UnicodeString,
    PWDF_OBJECT_ATTRIBUTES StringAttributes,
    WDFSTRING* String
    );

_Must_inspect_result_
NTSTATUS
FORCEINLINE
WdfStringCreate(
    _In_opt_
    PCUNICODE_STRING UnicodeString,
    _In_opt_
    PWDF_OBJECT_ATTRIBUTES StringAttributes,
    _Out_
    WDFSTRING* String
    )
{
    return ((PFN_WDFSTRINGCREATE) WdfFunctions[WdfStringCreateTableIndex])(WdfDriverGlobals, UnicodeString, StringAttributes, String);
}

//
// WDF Function: WdfStringGetUnicodeString
//
typedef
WDFAPI
VOID
(*PFN_WDFSTRINGGETUNICODESTRING)(
    IN PWDF_DRIVER_GLOBALS DriverGlobals,
    WDFSTRING String,
    PUNICODE_STRING UnicodeString
    );

VOID
FORCEINLINE
WdfStringGetUnicodeString(
    _In_
    WDFSTRING String,
    PUNICODE_STRING UnicodeString
    )
{
    ((PFN_WDFSTRINGGETUNICODESTRING) WdfFunctions[WdfStringGetUnicodeStringTableIndex])(WdfDriverGlobals, String, UnicodeString);
}



#endif // (NTDDI_VERSION >= NTDDI_WIN2K)


#endif // _WDFSTRING_1_5_H_

