// Copyright (c) Microsoft Corporation. All rights reserved.

// Called by PREPROCESS_FUNCTION in VC7\VCProjects\ATLWiz.vsz
function CheckATLProjectName(selProj, selObj)
{
	var strProjectName = wizard.FindSymbol("PROJECT_NAME");
	if (strProjectName.toLowerCase() == "compreg")
	{
		var L_NameNotAllowed_Text = "This project name is not allowed. Please chose another one."
		wizard.ReportError(L_NameNotAllowed_Text);
		return false;
	}
	return true;
}

function OnFinish(selProj, selObj)
{
	try
	{
		var strTemplatePath	= wizard.FindSymbol("TEMPLATES_PATH");
		var strProjectPath	= wizard.FindSymbol("PROJECT_PATH");
		var strProjectName	= wizard.FindSymbol("PROJECT_NAME");

		wizard.AddSymbol("RC_FILE_NAME",CreateSafeRCFileName(strProjectName)+".rc");
		wizard.AddSymbol("SAFE_PROJECT_APPID_NAME", wizard.FindSymbol("SAFE_PROJECT_IDENTIFIER_NAME"));
		wizard.AddSymbol("SAFE_IDL_NAME", CreateASCIIName(wizard.FindSymbol("PROJECT_NAME")));
		selProj = CreateProject(strProjectName, strProjectPath);

		AddConfigurations(selProj, strProjectName);

		SetupFilters(selProj);
		selProj.Object.keyword = "AtlProj";

		AddFilesToProjectWithInfFile(selProj, strProjectName);

		var L_strGenerated_Text = "Generated Files";
		var strIdlName = wizard.FindSymbol("SAFE_IDL_NAME");

		var strMIDLHeader = strProjectPath + "\\" + strIdlName + "_i.h";
		wizard.RenderTemplate(strTemplatePath + "\\root_i.h", strMIDLHeader, true);

		var strMIDL_IFile = strProjectPath + "\\" + strIdlName + "_i.c";
		wizard.RenderTemplate(strTemplatePath + "\\root_i.c", strMIDL_IFile, true);

		var oGeneratedFiles = selProj.Object.AddFilter(L_strGenerated_Text);
		if (oGeneratedFiles)
		{
			oGeneratedFiles.SourceControlFiles = false;
			oGeneratedFiles.AddFile(strMIDLHeader);
			oGeneratedFiles.AddFile(strMIDL_IFile);
		}
		else
		{
			selProj.Object.AddFile(strMIDLHeader);
			selProj.Object.AddFile(strMIDL_IFile);
		}

		if (!wizard.FindSymbol("GENERATE_ATL_DOCUMENT"))
		{
			// Add existing items (links) from MFC project handlers are being added for.
			// Add the document h/cpp, view h.cpp, and cntritem h/cpp (if exist).
			oFSO = new ActiveXObject("Scripting.FileSystemObject");

			var strMFCDocHeader = wizard.FindSymbol("DOCUMENT_HEADER_FILENAME_TRIMMED");
			if (strMFCDocHeader != "")
			{
				var strFile;

				strFile = oFSO.GetAbsolutePathName(strProjectPath + "\\" + strMFCDocHeader);
				if (oFSO.FileExists(strFile) && selProj.Object.CanAddFile(strMFCDocHeader))
				{
					selProj.Object.AddFile(strMFCDocHeader);
				}

				var strMFCDocImpl = strMFCDocHeader.substring(0, strMFCDocHeader.length - 1) + "cpp";
				strFile = oFSO.GetAbsolutePathName(strProjectPath + "\\" + strMFCDocImpl);
				if (oFSO.FileExists(strFile) && selProj.Object.CanAddFile(strMFCDocImpl))
				{
					selProj.Object.AddFile(strMFCDocImpl);
				}

				var strMFCViewHeader = wizard.FindSymbol("VIEW_HEADER_FILENAME_TRIMMED");
				if (strMFCViewHeader != "")
				{
					strFile = oFSO.GetAbsolutePathName(strProjectPath + "\\" + strMFCViewHeader);
					if (oFSO.FileExists(strFile) && selProj.Object.CanAddFile(strMFCViewHeader))
					{
						selProj.Object.AddFile(strMFCViewHeader);
					}

					var strMFCViewImpl = strMFCViewHeader.substring(0, strMFCViewHeader.length - 1) + "cpp";
					strFile = oFSO.GetAbsolutePathName(strProjectPath + "\\" + strMFCViewImpl);
					if (oFSO.FileExists(strFile) && selProj.Object.CanAddFile(strMFCViewImpl))
					{
						selProj.Object.AddFile(strMFCViewImpl);
					}
				}

				var nLength = strMFCDocHeader.length;
				var nEndIndex = nLength - 1;
				while (nEndIndex > 0 && (strMFCDocHeader.charAt(nEndIndex) != '\\'))
				{
					nEndIndex--;
				}

				var strMFCCntrHeader = strMFCDocHeader.substring(0, nEndIndex + 1) + "CntrItem.h";
				strFile = oFSO.GetAbsolutePathName(strProjectPath + "\\" + strMFCCntrHeader);
				if (oFSO.FileExists(strFile) && selProj.Object.CanAddFile(strMFCCntrHeader))
				{
					selProj.Object.AddFile(strMFCCntrHeader);

					var strMFCCntrImpl = strMFCDocHeader.substring(0, nEndIndex + 1) + "CntrItem.cpp";
					strFile = oFSO.GetAbsolutePathName(strProjectPath + "\\" + strMFCCntrImpl);
					if (oFSO.FileExists(strFile) && selProj.Object.CanAddFile(strMFCCntrImpl))
					{
						selProj.Object.AddFile(strMFCCntrImpl);
					}
				}
			}
		}

		// Set special flags on the MIDL-generated .c file
		var projfile = selProj.Object.Files(strIdlName + "_i.c");
		if (projfile != null) {
			projfile = projfile.Object;
			SetFileProperties(projfile, strIdlName + "_i.c");
		}

		SetPchSettings(selProj, strProjectName);

		selProj.Object.Save();

		var bMergeProxy = wizard.FindSymbol("MERGE_PROXY_STUB");
		if (!bMergeProxy)
		{
			var strDefFile = strProjectPath + "\\" + strProjectName + "ps.def";
			var str_PFile;
			var str_IFile;

			str_PFile = strProjectPath + "\\" + strIdlName + "_p.c";
			str_IFile = strProjectPath + "\\" + strIdlName + "_i.c";

			strProjectName += "PS";
			wizard.AddSymbol("CLOSE_SOLUTION", false);
			var oPSProj = CreateProject(strProjectName, strProjectPath);

			SetPSConfigurations(oPSProj, selProj);

			var strSrcFilter = wizard.FindSymbol("SOURCE_FILTER");
			var L_Source_Text = "Source Files";
			var group = oPSProj.Object.AddFilter(L_Source_Text);
			group.Filter = strSrcFilter;

			oPSProj.Object.keyword = "AtlPSProj";

			wizard.RenderTemplate(strTemplatePath + "\\rootps.def", strDefFile);
			oPSProj.Object.AddFile(strDefFile);
			var oGeneratedFiles = oPSProj.Object.AddFilter(L_strGenerated_Text);
			if (oGeneratedFiles)
			{
				oGeneratedFiles.SourceControlFiles = false;
				oGeneratedFiles.AddFile(str_IFile);
				oGeneratedFiles.AddFile(str_PFile);
				oGeneratedFiles.AddFile(strProjectPath + "\\dlldata.c");
			}
			else
			{
				oPSProj.Object.AddFile(str_IFile);
				oPSProj.Object.AddFile(str_PFile);
				oPSProj.Object.AddFile(strProjectPath + "\\dlldata.c");
			}
			// Set special flags on the MIDL-generated .c file
			var projfile = selProj.Object.Files(strIdlName + "_p.c");
			if (projfile != null) {
				projfile = projfile.Object;
				SetFileProperties(projfile, strIdlName + "_p.c");
			}
			projfile = selProj.Object.Files(strIdlName + "_c.c");
			if (projfile != null) {
				projfile = projfile.Object;
				SetFileProperties(projfile, strIdlName + "_c.c");
			}
			projfile = selProj.Object.Files("dlldata.c");
			if (projfile != null) {
				projfile = projfile.Object;
				SetFileProperties(projfile, "dlldata.c");
			}

			oPSProj.Object.Save();
		}

		// expand main project node, highlight it
		//
		strProjectName	= wizard.FindSymbol("PROJECT_NAME");
		var oHier = wizard.dte.Windows.Item(vsWindowKindSolutionExplorer).Object;
		var oHISolution = oHier.UIHierarchyItems(1);
		var oHIProjMain;
		for (nHI=1; nHI<=oHISolution.UIHierarchyItems.Count; nHI++)
		{
			if ( oHISolution.UIHierarchyItems(nHI).name == strProjectName )
			{
				oHIProjMain = oHISolution.UIHierarchyItems(nHI);
				break;
			}
		}
		if (oHIProjMain)
		{
			oHIProjMain.UIHierarchyItems.Expanded = true;
			oHIProjMain.Select(vsUISelectionTypeSelect);
		}
	}
	catch(e)
	{
		if (e.description.length != 0)
			SetErrorInfo(e);
		return e.number
	}
}

function SetFileProperties(projfile, strName)
{
	if (strName == "dllmain.cpp" || strName.substr(strName.length-2,strName.length) == ".c") {
		var Configs = projfile.Object.FileConfigurations;
		for(var i=1;i<=Configs.Count;++i) {
			var Config = Configs(i);
			var CLTool = Config.Tool;
			CLTool.CompileAsManaged = 0; // Force no /CLR
			CLTool.UsePrecompiledHeader  = 0; // No pre-compiled headers
		}
	}
}

function GetTargetName(strName, strProjectName, strResPath, strHelpPath)
{
	try
	{
		var strTarget = strName;

		if (strName.substr(0, 4) == "root")
		{
			if (strName == "root.idl")
			{
				var strProjectName = wizard.FindSymbol("SAFE_IDL_NAME");
				strTarget = strProjectName + ".idl";
			}
			else if (strName == "root.rc")
			{
				strTarget = wizard.FindSymbol("RC_FILE_NAME");
			}
			else
			{
				strTarget = strProjectName + strName.substr(4);
			}

			return strTarget;
		}

		switch (strName)
		{
			case "readme.txt":
				strTarget = "ReadMe.txt";
				break;
			case "resource.h":
				strTarget = "Resource.h";
				break;
			case "document.cpp":
				strTarget = wizard.FindSymbol("DOCUMENT_IMPL_FILENAME_TRIMMED");
				break;
			case "document.h":
				strTarget = wizard.FindSymbol("DOCUMENT_HEADER_FILENAME_TRIMMED");
				break;
			case "Preview.h":
				strTarget = "PreviewHandler.h";
				break;
			case "Preview.rgs":
				strTarget = "PreviewHandler.rgs";
				break;
			case "Thumbnail.h":
				strTarget = "ThumbnailHandler.h";
				break;
			case "Thumbnail.rgs":
				strTarget = "ThumbnailHandler.rgs";
				break;
			case "Search.h":
				strTarget = "FilterHandler.h";
				break;
			case "Search.rgs":
				strTarget = "FilterHandler.rgs";
				break;
			default:
				break;
		}

		return strTarget;
	}
	catch(e)
	{
		throw e;
	}
}

function SetPSConfigurations(oProj, oMainProj)
{
	try
	{
		oConfigs = oProj.Object.Configurations;
		bSupportComPlus = wizard.FindSymbol("SUPPORT_COMPLUS");

		for (var nCntr = 1; nCntr <= oConfigs.Count; nCntr++)
		{
			var oConfig = oConfigs(nCntr);
			var bDebug = false;
			if (-1 != oConfig.Name.indexOf("Debug"))
				bDebug = true;

			oConfig.ConfigurationType = typeDynamicLibrary;
			oConfig.CharacterSet = charSetUNICODE;
			var oCLTool = oConfig.Tools("VCCLCompilerTool");

			var strDefines = oCLTool.PreprocessorDefinitions;
			if (strDefines != "") strDefines += ";";
			strDefines += GetPlatformDefine(oConfig);
			strDefines += "REGISTER_PROXY_DLL";
			if (bDebug)
			{
				strDefines += ";_DEBUG";
			}
			else
			{
				strDefines += ";NDEBUG";
				oCLTool.Optimization = optimizeMaxSpeed;
			}
			oCLTool.PreprocessorDefinitions = strDefines;

			var oLinkTool = oConfig.Tools("VCLinkerTool");
			oLinkTool.AdditionalDependencies = "kernel32.lib rpcns4.lib rpcrt4.lib oleaut32.lib uuid.lib";

			if (bSupportComPlus)
				oLinkTool.AdditionalDependencies += " ole32.lib advapi32.lib comsvcs.lib";

			oLinkTool.ModuleDefinitionFile = oProj.Name + ".def";

			if (!bDebug)
			{
				oLinkTool.EnableCOMDATFolding = optFolding;
				oLinkTool.OptimizeReferences = optReferences;
			}

			oLinkTool.RegisterOutput = true;

			var oPreBuildTool = oConfig.Tools("VCPreBuildEventTool");
			var strCommand = "if exist dlldata.c goto :END\r\n";
			var L_Echo1_Text = "echo Error: MIDL will not generate DLLDATA.C unless you have at least 1 interface in the main project.\r\n";
			strCommand += L_Echo1_Text;
			strCommand += "Exit 1\r\n";
			strCommand += ":END\r\n";
			oPreBuildTool.CommandLine = strCommand;
			var L_Echo2_Text = "Checking for required files";
			oPreBuildTool.Description = L_Echo2_Text;
		}

		// exclude from Solution build
		var oSolBuild = dte.Solution.SolutionBuild;
		var oSolConfigs = oSolBuild.SolutionConfigurations;
		for (var nCntr = 1; nCntr <= oSolConfigs.Count; nCntr++)
		{
			var oSolContexts = oSolConfigs(nCntr).SolutionContexts;
			for (var nCntr2 = 1; nCntr2 <= oSolContexts.Count; nCntr2++)
			{
				var oSolContext = oSolContexts(nCntr2);
				if (oSolContext.ProjectName == oProj.UniqueName)
					oSolContext.ShouldBuild = false;
			}
		}

		// add main project to build dependency list
	    oSolBuild.BuildDependencies(oProj.UniqueName).AddProject(oMainProj.UniqueName);
	}
	catch(e)
	{
		throw e;
	}
}

var nNumConfigs = 2;

var astrConfigName = new Array();
astrConfigName[0] = "Debug";
astrConfigName[1] = "Release";

var astrConfigDir = new Array();
astrConfigDir[0] = "Debug";
astrConfigDir[1] = "Release";

var astrDefines = new Array();
astrDefines[0] = "_WINDOWS;_DEBUG";
astrDefines[1] = "_WINDOWS;NDEBUG";

var anCRT = new Array();
anCRT[0] = rtMultiThreadedDebugDLL;
anCRT[1] = rtMultiThreadedDLL;

function AddConfigurations(proj, strProjectName)
{
	try
	{
		var nCntr;
		for (nCntr = 0; nCntr < nNumConfigs; nCntr++)
		{
			var strIdlName = wizard.FindSymbol("SAFE_IDL_NAME");
			// check if Debug
			var bDebug = false;
			if (-1 != astrConfigName[nCntr].search("Debug"))
				bDebug = true;

			var config = proj.Object.Configurations(astrConfigName[nCntr]);

			// add configuration if it doesn't exist
			if (!config)
			{
				proj.Object.AddConfiguration(astrConfigName[nCntr]);
				config = proj.Object.Configurations(astrConfigName[nCntr]);
			}

			// set configuration type
			var bAppTypeDLL = wizard.FindSymbol("DLL_APP");
			if (bAppTypeDLL)
				config.ConfigurationType = typeDynamicLibrary;

			config.UseOfATL = useATLDynamic;
			config.CharacterSet = charSetUNICODE;

			// Compiler settings
			var CLTool = config.Tools("VCCLCompilerTool");
			CLTool.UsePrecompiledHeader = pchUseUsingSpecific;
			CLTool.WarningLevel = WarningLevel_3;
			if (bDebug)
			{
				CLTool.Optimization = optimizeDisabled;
			}
			else
			{
				CLTool.Optimization = optimizeMaxSpeed;
			}

			var bMFC = wizard.FindSymbol("SUPPORT_MFC");
			var bMergeProxy = wizard.FindSymbol("MERGE_PROXY_STUB");
			var bSupportComPlus	= wizard.FindSymbol("SUPPORT_COMPLUS");
			var bSupportComponentRegistrar = wizard.FindSymbol("SUPPORT_COMPONENT_REGISTRAR");

			var strDefines = CLTool.PreprocessorDefinitions;
			if (strDefines != "") strDefines += ";";
			strDefines += GetPlatformDefine(config);
			strDefines += astrDefines[nCntr];
			if (bAppTypeDLL)
				strDefines += ";_USRDLL";
			if (bMFC)
				config.UseOfMFC = useMfcDynamic;
			if (bMergeProxy && bSupportComponentRegistrar)
				strDefines += ";_MERGE_PROXYSTUB";
			CLTool.PreprocessorDefinitions = strDefines;

			// MIDL settings
			var MidlTool = config.Tools("VCMidlTool");
			MidlTool.MkTypLibCompatible = false;
			if (IsPlatformWin32(config))
				MidlTool.TargetEnvironment = midlTargetWin32;

			if (bDebug)
				MidlTool.PreprocessorDefinitions = "_DEBUG";
			else
				MidlTool.PreprocessorDefinitions = "NDEBUG";

			MidlTool.HeaderFileName = strIdlName + "_i.h";
			MidlTool.InterfaceIdentifierFileName = strIdlName + "_i.c";
			MidlTool.ProxyFileName = strIdlName + "_p.c";
			MidlTool.GenerateStublessProxies = true;
			MidlTool.TypeLibraryName = "$(IntDir)" + strIdlName + ".tlb";
			MidlTool.DLLDataFileName = "";

			// no /no_robust
			MidlTool.ValidateParameters = true;

			// Resource settings
			var RCTool = config.Tools("VCResourceCompilerTool");
			RCTool.Culture = wizard.FindSymbol("LCID");
			RCTool.AdditionalIncludeDirectories = "$(IntDir)";
			if (bDebug)
				RCTool.PreprocessorDefinitions = "_DEBUG";
			else
				RCTool.PreprocessorDefinitions = "NDEBUG";

			// Linker settings
			var LinkTool = config.Tools("VCLinkerTool");
			LinkTool.SubSystem = subSystemWindows;
			LinkTool.IgnoreImportLibrary = true;

			if (bAppTypeDLL)
			{
				var strDefFile = ".\\" + strProjectName + ".def";
				LinkTool.ModuleDefinitionFile = strDefFile;
			}
			if (bSupportComPlus)
				LinkTool.AdditionalDependencies += " comsvcs.lib";

			LinkTool.GenerateDebugInformation = true;
			if (bDebug)
				LinkTool.LinkIncremental = linkIncrementalYes;
			else
			{
				LinkTool.LinkIncremental = linkIncrementalNo;
				LinkTool.EnableCOMDATFolding = optFolding;
				LinkTool.OptimizeReferences = optReferences;
			}


			if (bAppTypeDLL)
			{
				LinkTool.RegisterOutput = true;

				if (wizard.FindSymbol("SEARCH_HANDLER"))
				{
					var PostBuildTool = config.Tools("VCPostBuildEventTool");
					var L_SettingPermissions_Text = "Setting permissions on handler DLL (to enable load by SearchFilterHost.exe)...";
					PostBuildTool.Description = L_SettingPermissions_Text;
					var L_CommandLine_Text = "cacls.exe \"$(TargetPath)\" /G users:r /E"; // only the "users" group name needs to be localized
					PostBuildTool.CommandLine = L_CommandLine_Text;
				}
			}
			else
			{
				var PostBuildTool = config.Tools("VCPostBuildEventTool");
				var L_PerformingRegistration2_Text = "Performing registration";
				PostBuildTool.Description = L_PerformingRegistration2_Text;
				PostBuildTool.CommandLine = "\"$(TargetPath)\" /RegServer";
			}

		}
	}
	catch(e)
	{
		throw e;
	}
}

function SetPchSettings(proj, strProjectName)
{
	try
	{
		var files = proj.Object.Files;
		var fStdafx = files("StdAfx.cpp");

		var nCntr;
		for (nCntr = 0; nCntr < nNumConfigs; nCntr++)
		{
			var config = fStdafx.FileConfigurations(astrConfigName[nCntr]);
			config.Tool.UsePrecompiledHeader = pchCreateUsingSpecific;

			var strIdlName = wizard.FindSymbol("SAFE_IDL_NAME");
			var fProject_i = files(strIdlName + "_i.c");
			config = fProject_i.FileConfigurations(astrConfigName[nCntr]);
			config.Tool.UsePrecompiledHeader = pchNone;

			if (wizard.FindSymbol("MERGE_PROXY_STUB"))
			{
				file = files("xdlldata.c");
				config = file.FileConfigurations(astrConfigName[nCntr]);
				config.Tool.UsePrecompiledHeader = pchNone;
			}
		}
	}
	catch(e)
	{
		throw e;
	}
}

// SIG // Begin signature block
// SIG // MIIXOgYJKoZIhvcNAQcCoIIXKzCCFycCAQExCzAJBgUr
// SIG // DgMCGgUAMGcGCisGAQQBgjcCAQSgWTBXMDIGCisGAQQB
// SIG // gjcCAR4wJAIBAQQQEODJBs441BGiowAQS9NQkAIBAAIB
// SIG // AAIBAAIBAAIBADAhMAkGBSsOAwIaBQAEFBMfL7UE2B2b
// SIG // CzjAf0VEztXOR/JyoIISMTCCBGAwggNMoAMCAQICCi6r
// SIG // EdxQ/1ydy8AwCQYFKw4DAh0FADBwMSswKQYDVQQLEyJD
// SIG // b3B5cmlnaHQgKGMpIDE5OTcgTWljcm9zb2Z0IENvcnAu
// SIG // MR4wHAYDVQQLExVNaWNyb3NvZnQgQ29ycG9yYXRpb24x
// SIG // ITAfBgNVBAMTGE1pY3Jvc29mdCBSb290IEF1dGhvcml0
// SIG // eTAeFw0wNzA4MjIyMjMxMDJaFw0xMjA4MjUwNzAwMDBa
// SIG // MHkxCzAJBgNVBAYTAlVTMRMwEQYDVQQIEwpXYXNoaW5n
// SIG // dG9uMRAwDgYDVQQHEwdSZWRtb25kMR4wHAYDVQQKExVN
// SIG // aWNyb3NvZnQgQ29ycG9yYXRpb24xIzAhBgNVBAMTGk1p
// SIG // Y3Jvc29mdCBDb2RlIFNpZ25pbmcgUENBMIIBIjANBgkq
// SIG // hkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAt3l91l2zRTmo
// SIG // NKwx2vklNUl3wPsfnsdFce/RRujUjMNrTFJi9JkCw03Y
// SIG // SWwvJD5lv84jtwtIt3913UW9qo8OUMUlK/Kg5w0jH9FB
// SIG // JPpimc8ZRaWTSh+ZzbMvIsNKLXxv2RUeO4w5EDndvSn0
// SIG // ZjstATL//idIprVsAYec+7qyY3+C+VyggYSFjrDyuJSj
// SIG // zzimUIUXJ4dO3TD2AD30xvk9gb6G7Ww5py409rQurwp9
// SIG // YpF4ZpyYcw2Gr/LE8yC5TxKNY8ss2TJFGe67SpY7UFMY
// SIG // zmZReaqth8hWPp+CUIhuBbE1wXskvVJmPZlOzCt+M26E
// SIG // RwbRntBKhgJuhgCkwIffUwIDAQABo4H6MIH3MBMGA1Ud
// SIG // JQQMMAoGCCsGAQUFBwMDMIGiBgNVHQEEgZowgZeAEFvQ
// SIG // cO9pcp4jUX4Usk2O/8uhcjBwMSswKQYDVQQLEyJDb3B5
// SIG // cmlnaHQgKGMpIDE5OTcgTWljcm9zb2Z0IENvcnAuMR4w
// SIG // HAYDVQQLExVNaWNyb3NvZnQgQ29ycG9yYXRpb24xITAf
// SIG // BgNVBAMTGE1pY3Jvc29mdCBSb290IEF1dGhvcml0eYIP
// SIG // AMEAizw8iBHRPvZj7N9AMA8GA1UdEwEB/wQFMAMBAf8w
// SIG // HQYDVR0OBBYEFMwdznYAcFuv8drETppRRC6jRGPwMAsG
// SIG // A1UdDwQEAwIBhjAJBgUrDgMCHQUAA4IBAQB7q65+Siby
// SIG // zrxOdKJYJ3QqdbOG/atMlHgATenK6xjcacUOonzzAkPG
// SIG // yofM+FPMwp+9Vm/wY0SpRADulsia1Ry4C58ZDZTX2h6t
// SIG // KX3v7aZzrI/eOY49mGq8OG3SiK8j/d/p1mkJkYi9/uEA
// SIG // uzTz93z5EBIuBesplpNCayhxtziP4AcNyV1ozb2AQWtm
// SIG // qLu3u440yvIDEHx69dLgQt97/uHhrP7239UNs3DWkuNP
// SIG // tjiifC3UPds0C2I3Ap+BaiOJ9lxjj7BauznXYIxVhBoz
// SIG // 9TuYoIIMol+Lsyy3oaXLq9ogtr8wGYUgFA0qvFL0QeBe
// SIG // MOOSKGmHwXDi86erzoBCcnYOMIIEejCCA2KgAwIBAgIK
// SIG // YQHPPgAAAAAADzANBgkqhkiG9w0BAQUFADB5MQswCQYD
// SIG // VQQGEwJVUzETMBEGA1UECBMKV2FzaGluZ3RvbjEQMA4G
// SIG // A1UEBxMHUmVkbW9uZDEeMBwGA1UEChMVTWljcm9zb2Z0
// SIG // IENvcnBvcmF0aW9uMSMwIQYDVQQDExpNaWNyb3NvZnQg
// SIG // Q29kZSBTaWduaW5nIFBDQTAeFw0wOTEyMDcyMjQwMjla
// SIG // Fw0xMTAzMDcyMjQwMjlaMIGDMQswCQYDVQQGEwJVUzET
// SIG // MBEGA1UECBMKV2FzaGluZ3RvbjEQMA4GA1UEBxMHUmVk
// SIG // bW9uZDEeMBwGA1UEChMVTWljcm9zb2Z0IENvcnBvcmF0
// SIG // aW9uMQ0wCwYDVQQLEwRNT1BSMR4wHAYDVQQDExVNaWNy
// SIG // b3NvZnQgQ29ycG9yYXRpb24wggEiMA0GCSqGSIb3DQEB
// SIG // AQUAA4IBDwAwggEKAoIBAQC9MIn7RXKoU2ueiU8AI8C+
// SIG // 1B09sVlAOPNzkYIm5pYSAFPZHIIOPM4du733Qo2X1Pw4
// SIG // GuS5+ePs02EDv6DT1nVNXEap7V7w0uJpWxpz6rMcjQTN
// SIG // KUSgZFkvHphdbserGDmCZcSnvKt1iBnqh5cUJrN/Jnak
// SIG // 1Dg5hOOzJtUY+Svp0skWWlQh8peNh4Yp/vRJLOaL+AQ/
// SIG // fc3NlpKGDXED4tD+DEI1/9e4P92ORQp99tdLrVvwdnId
// SIG // dyN9iTXEHF2yUANLR20Hp1WImAaApoGtVE7Ygdb6v0LA
// SIG // Mb5VDZnVU0kSMOvlpYh8XsR6WhSHCLQ3aaDrMiSMCOv5
// SIG // 1BS64PzN6qQVAgMBAAGjgfgwgfUwEwYDVR0lBAwwCgYI
// SIG // KwYBBQUHAwMwHQYDVR0OBBYEFDh4BXPIGzKbX5KGVa+J
// SIG // usaZsXSOMA4GA1UdDwEB/wQEAwIHgDAfBgNVHSMEGDAW
// SIG // gBTMHc52AHBbr/HaxE6aUUQuo0Rj8DBEBgNVHR8EPTA7
// SIG // MDmgN6A1hjNodHRwOi8vY3JsLm1pY3Jvc29mdC5jb20v
// SIG // cGtpL2NybC9wcm9kdWN0cy9DU1BDQS5jcmwwSAYIKwYB
// SIG // BQUHAQEEPDA6MDgGCCsGAQUFBzAChixodHRwOi8vd3d3
// SIG // Lm1pY3Jvc29mdC5jb20vcGtpL2NlcnRzL0NTUENBLmNy
// SIG // dDANBgkqhkiG9w0BAQUFAAOCAQEAKAODqxMN8f4Rb0J2
// SIG // 2EOruMZC+iRlNK51sHEwjpa2g/py5P7NN+c6cJhRIA66
// SIG // cbTJ9NXkiugocHPV7eHCe+7xVjRagILrENdyA+oSTuzd
// SIG // DYx7RE8MYXX9bpwH3c4rWhgNObBg/dr/BKoCo9j6jqO7
// SIG // vcFqVDsxX+QsbsvxTSoc8h52e4avxofWsSrtrMwOwOSf
// SIG // f+jP6IRyVIIYbirInpW0Gh7Bb5PbYqbBS2utye09kuOy
// SIG // L6t6dzlnagB7gp0DEN5jlUkmQt6VIsGHC9AUo1/cczJy
// SIG // Nh7/yCnFJFJPZkjJHR2pxSY5aVBOp+zCBmwuchvxIdpt
// SIG // JEiAgRVAfJ/MdDhKTzCCBJ0wggOFoAMCAQICEGoLmU/A
// SIG // ACWrEdtFH1h6Z6IwDQYJKoZIhvcNAQEFBQAwcDErMCkG
// SIG // A1UECxMiQ29weXJpZ2h0IChjKSAxOTk3IE1pY3Jvc29m
// SIG // dCBDb3JwLjEeMBwGA1UECxMVTWljcm9zb2Z0IENvcnBv
// SIG // cmF0aW9uMSEwHwYDVQQDExhNaWNyb3NvZnQgUm9vdCBB
// SIG // dXRob3JpdHkwHhcNMDYwOTE2MDEwNDQ3WhcNMTkwOTE1
// SIG // MDcwMDAwWjB5MQswCQYDVQQGEwJVUzETMBEGA1UECBMK
// SIG // V2FzaGluZ3RvbjEQMA4GA1UEBxMHUmVkbW9uZDEeMBwG
// SIG // A1UEChMVTWljcm9zb2Z0IENvcnBvcmF0aW9uMSMwIQYD
// SIG // VQQDExpNaWNyb3NvZnQgVGltZXN0YW1waW5nIFBDQTCC
// SIG // ASIwDQYJKoZIhvcNAQEBBQADggEPADCCAQoCggEBANw3
// SIG // bvuvyEJKcRjIzkg+U8D6qxS6LDK7Ek9SyIPtPjPZSTGS
// SIG // KLaRZOAfUIS6wkvRfwX473W+i8eo1a5pcGZ4J2botrfv
// SIG // hbnN7qr9EqQLWSIpL89A2VYEG3a1bWRtSlTb3fHev5+D
// SIG // x4Dff0wCN5T1wJ4IVh5oR83ZwHZcL322JQS0VltqHGP/
// SIG // gHw87tUEJU05d3QHXcJc2IY3LHXJDuoeOQl8dv6dbG56
// SIG // 4Ow+j5eecQ5fKk8YYmAyntKDTisiXGhFi94vhBBQsvm1
// SIG // Go1s7iWbE/jLENeFDvSCdnM2xpV6osxgBuwFsIYzt/iU
// SIG // W4RBhFiFlG6wHyxIzG+cQ+Bq6H8mjmsCAwEAAaOCASgw
// SIG // ggEkMBMGA1UdJQQMMAoGCCsGAQUFBwMIMIGiBgNVHQEE
// SIG // gZowgZeAEFvQcO9pcp4jUX4Usk2O/8uhcjBwMSswKQYD
// SIG // VQQLEyJDb3B5cmlnaHQgKGMpIDE5OTcgTWljcm9zb2Z0
// SIG // IENvcnAuMR4wHAYDVQQLExVNaWNyb3NvZnQgQ29ycG9y
// SIG // YXRpb24xITAfBgNVBAMTGE1pY3Jvc29mdCBSb290IEF1
// SIG // dGhvcml0eYIPAMEAizw8iBHRPvZj7N9AMBAGCSsGAQQB
// SIG // gjcVAQQDAgEAMB0GA1UdDgQWBBRv6E4/l7k0q0uGj7yc
// SIG // 6qw7QUPG0DAZBgkrBgEEAYI3FAIEDB4KAFMAdQBiAEMA
// SIG // QTALBgNVHQ8EBAMCAYYwDwYDVR0TAQH/BAUwAwEB/zAN
// SIG // BgkqhkiG9w0BAQUFAAOCAQEAlE0RMcJ8ULsRjqFhBwEO
// SIG // jHBFje9zVL0/CQUt/7hRU4Uc7TmRt6NWC96Mtjsb0fus
// SIG // p8m3sVEhG28IaX5rA6IiRu1stG18IrhG04TzjQ++B4o2
// SIG // wet+6XBdRZ+S0szO3Y7A4b8qzXzsya4y1Ye5y2PENtEY
// SIG // Ib923juasxtzniGI2LS0ElSM9JzCZUqaKCacYIoPO8cT
// SIG // ZXhIu8+tgzpPsGJY3jDp6Tkd44ny2jmB+RMhjGSAYwYE
// SIG // lvKaAkMve0aIuv8C2WX5St7aA3STswVuDMyd3ChhfEjx
// SIG // F5wRITgCHIesBsWWMrjlQMZTPb2pid7oZjeN9CKWnMyw
// SIG // d1RROtZyRLIj9jCCBKowggOSoAMCAQICCmEGlC0AAAAA
// SIG // AAkwDQYJKoZIhvcNAQEFBQAweTELMAkGA1UEBhMCVVMx
// SIG // EzARBgNVBAgTCldhc2hpbmd0b24xEDAOBgNVBAcTB1Jl
// SIG // ZG1vbmQxHjAcBgNVBAoTFU1pY3Jvc29mdCBDb3Jwb3Jh
// SIG // dGlvbjEjMCEGA1UEAxMaTWljcm9zb2Z0IFRpbWVzdGFt
// SIG // cGluZyBQQ0EwHhcNMDgwNzI1MTkwMjE3WhcNMTMwNzI1
// SIG // MTkxMjE3WjCBszELMAkGA1UEBhMCVVMxEzARBgNVBAgT
// SIG // Cldhc2hpbmd0b24xEDAOBgNVBAcTB1JlZG1vbmQxHjAc
// SIG // BgNVBAoTFU1pY3Jvc29mdCBDb3Jwb3JhdGlvbjENMAsG
// SIG // A1UECxMETU9QUjEnMCUGA1UECxMebkNpcGhlciBEU0Ug
// SIG // RVNOOjdBODItNjg4QS05RjkyMSUwIwYDVQQDExxNaWNy
// SIG // b3NvZnQgVGltZS1TdGFtcCBTZXJ2aWNlMIIBIjANBgkq
// SIG // hkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAlYEKIEIYUXrZ
// SIG // le2b/dyH0fsOjxPqqjcoEnb+TVCrdpcqk0fgqVZpAuWU
// SIG // fk2F239x73UA27tDbPtvrHHwK9F8ks6UF52hxbr5937d
// SIG // YeEtMB6cJi12P+ZGlo6u2Ik32Mzv889bw/xo4PJkj5vo
// SIG // wxL5o76E/NaLzgU9vQF2UCcD+IS3FoaNYL5dKSw8z6X9
// SIG // mFo1HU8WwDjYHmE/PTazVhQVd5U7EPoAsJPiXTerJ7tj
// SIG // LEgUgVXjbOqpK5WNiA5+owCldyQHmCpwA7gqJJCa3sWi
// SIG // Iku/TFkGd1RyQ7A+ZN2ThAhYtv7ph0kJNrOz+DOpfkyi
// SIG // eX8yWSkOnrX14DyeP+xGOwIDAQABo4H4MIH1MB0GA1Ud
// SIG // DgQWBBQolYi/Ajvr2pS6fUYP+sv0fp3/0TAfBgNVHSME
// SIG // GDAWgBRv6E4/l7k0q0uGj7yc6qw7QUPG0DBEBgNVHR8E
// SIG // PTA7MDmgN6A1hjNodHRwOi8vY3JsLm1pY3Jvc29mdC5j
// SIG // b20vcGtpL2NybC9wcm9kdWN0cy90c3BjYS5jcmwwSAYI
// SIG // KwYBBQUHAQEEPDA6MDgGCCsGAQUFBzAChixodHRwOi8v
// SIG // d3d3Lm1pY3Jvc29mdC5jb20vcGtpL2NlcnRzL3RzcGNh
// SIG // LmNydDATBgNVHSUEDDAKBggrBgEFBQcDCDAOBgNVHQ8B
// SIG // Af8EBAMCBsAwDQYJKoZIhvcNAQEFBQADggEBAADurPzi
// SIG // 0ohmyinjWrnNAIJ+F1zFJFkSu6j3a9eH/o3LtXYfGyL2
// SIG // 9+HKtLlBARo3rUg3lnD6zDOnKIy4C7Z0Eyi3s3XhKgni
// SIG // i0/fmD+XtzQSgeoQ3R3cumTPTlA7TIr9Gd0lrtWWh+pL
// SIG // xOXw+UEXXQHrV4h9dnrlb/6HIKyTnIyav18aoBUwJOCi
// SIG // fmGRHSkpw0mQOkODie7e1YPdTyw1O+dBQQGqAAwL8tZJ
// SIG // G85CjXuw8y2NXSnhvo1/kRV2tGD7FCeqbxJjQihYOoo7
// SIG // i0Dkt8XMklccRlZrj8uSTVYFAMr4MEBFTt8ZiL31EPDd
// SIG // Gt8oHrRR8nfgJuO7CYES3B460EUxggR1MIIEcQIBATCB
// SIG // hzB5MQswCQYDVQQGEwJVUzETMBEGA1UECBMKV2FzaGlu
// SIG // Z3RvbjEQMA4GA1UEBxMHUmVkbW9uZDEeMBwGA1UEChMV
// SIG // TWljcm9zb2Z0IENvcnBvcmF0aW9uMSMwIQYDVQQDExpN
// SIG // aWNyb3NvZnQgQ29kZSBTaWduaW5nIFBDQQIKYQHPPgAA
// SIG // AAAADzAJBgUrDgMCGgUAoIGgMBkGCSqGSIb3DQEJAzEM
// SIG // BgorBgEEAYI3AgEEMBwGCisGAQQBgjcCAQsxDjAMBgor
// SIG // BgEEAYI3AgEVMCMGCSqGSIb3DQEJBDEWBBSrrL2bMX6L
// SIG // /XkGTl5CXDV1EuePgDBABgorBgEEAYI3AgEMMTIwMKAW
// SIG // gBQAZABlAGYAYQB1AGwAdAAuAGoAc6EWgBRodHRwOi8v
// SIG // bWljcm9zb2Z0LmNvbTANBgkqhkiG9w0BAQEFAASCAQBQ
// SIG // NQpuj2gaEOvaCkhr8WdQX2Ks0puZkSmaHvXqMpFDV2oy
// SIG // N/8lg8xv1IXTqMcJYgCCsIiWzApHZoyQTrHC4B3ymPdM
// SIG // 8qb0jLr8zYE1uLsf0GfkuGqwlQUaPyZr3mFuIapnJ/8+
// SIG // uQC1y1xT4Zm5pcDnzCgdDge6B2o6kaR4DGV12iBB03n8
// SIG // xOoxV9z3lpB58UPfwMkkzoe1p2h0sfRxo7lZxjKRkris
// SIG // axInBUiuJJeD7rBL6IvvSFuZM0RQfNJv7KTdY6gBk95G
// SIG // Jpw5S+QXy3pxxm00DugYvYGGe64jGbtU4fKNE7f69rNG
// SIG // QqNXpbp0AJF7WMuFB8hbXdJwfGIuQacpoYICHzCCAhsG
// SIG // CSqGSIb3DQEJBjGCAgwwggIIAgEBMIGHMHkxCzAJBgNV
// SIG // BAYTAlVTMRMwEQYDVQQIEwpXYXNoaW5ndG9uMRAwDgYD
// SIG // VQQHEwdSZWRtb25kMR4wHAYDVQQKExVNaWNyb3NvZnQg
// SIG // Q29ycG9yYXRpb24xIzAhBgNVBAMTGk1pY3Jvc29mdCBU
// SIG // aW1lc3RhbXBpbmcgUENBAgphBpQtAAAAAAAJMAcGBSsO
// SIG // AwIaoF0wGAYJKoZIhvcNAQkDMQsGCSqGSIb3DQEHATAc
// SIG // BgkqhkiG9w0BCQUxDxcNMTAwMzE5MTMwMDU4WjAjBgkq
// SIG // hkiG9w0BCQQxFgQU/l3AzvZ8YVkUcxKqS7ZrjEqD1Sww
// SIG // DQYJKoZIhvcNAQEFBQAEggEAS8aBakkDPLOrgMAr8/nE
// SIG // 2XCYBoplNghBBmiXtU/zNgZ0FOUi+ju7OrfrBZ/d+uIW
// SIG // 9/D74ozp7uMDANz4+xjylMLw4p1DTHVI8l9D4/AtmmEq
// SIG // lT4SmCCsASl6/aSbspFuB9rKGe/VJl4TC3iUVLY3+fUI
// SIG // hGHEIEDnIa5qEBp/z64NTeKULiVPHahFeqCgX2yc7bKZ
// SIG // HdUpl5qqef2f1/j/Y2e/fLrCcOuEBpiuqVANUXqHDmlC
// SIG // ID4BHDg1PpxKMOlVoR+PxvghK/HiNCXWia/znTYN9FoH
// SIG // DYY5oUmF3l7YxmBMygUJ9c/ALC3qoDqu44tlaDP5Ef7t
// SIG // kjTDJhfIWPvXZQ==
// SIG // End signature block
