================================================================================
    BIBLIOTH�QUE MICROSOFT FOUNDATION CLASS : Vue d'ensemble du projet 
    [!output PROJECT_NAME]
================================================================================

L'Assistant Application a cr�� cette application [!output PROJECT_NAME] pour 
vous.  Cette application d�crit les principes de base de l'utilisation de 
Microsoft Foundation Classes et vous permet de cr�er votre application.

Ce fichier contient un r�sum� du contenu de chacun des fichiers qui constituent 
votre application [!output PROJECT_NAME].

[!output PROJECT_NAME].vcxproj
    Il s'agit du fichier projet principal pour les projets VC++ g�n�r�s � l'aide 
    d'un Assistant Application. 
    Il contient les informations sur la version de Visual C++ qui a g�n�r� le 
    fichier et des informations sur les plates-formes, configurations et 
    fonctionnalit�s du projet s�lectionn�es avec l'Assistant Application.

[!output PROJECT_NAME].vcxproj.filters
    Il s'agit du fichier de filtres pour les projets VC++ g�n�r�s � l'aide d'un 
    Assistant Application. 
    Il contient des informations sur l'association entre les fichiers de votre 
    projet et les filtres. Cette association est utilis�e dans l'IDE pour 
    afficher le regroupement des fichiers qui ont des extensions similares sous 
    un n�ud sp�cifique (par exemple, les fichiers ".cpp" sont associ�s au 
    filtre "Fichiers sources").

[!output APP_HEADER]
    Il s'agit du fichier d'en-t�te principal de l'application.  Il contient 
    d'autres en-t�tes de projet sp�cifiques (y compris Resource.h) et d�clare 
    la classe d'application [!output APP_CLASS].

[!output APP_IMPL]
    Il s'agit du fichier source principal de l'application qui contient la 
    classe d'application [!output APP_CLASS].

[!output RC_FILE_NAME]
    Il s'agit de la liste de toutes les ressources Microsoft Windows que le 
    programme utilise.  Elle comprend les ic�nes, les bitmaps et les curseurs 
    qui sont stock�s dans le sous-r�pertoire RES. Ce fichier peut �tre modifi� 
    directement dans Microsoft Visual C++. Vos ressources de projet sont dans 
    [!output LCID].

res\[!output PROJECT_NAME].ico
    Il s'agit d'un fichier ic�ne, qui est utilis� comme ic�ne de l'application.  
    Cette ic�ne est incluse par le fichier de ressource principal 
    [!output PROJECT_NAME].rc.

res\[!output RC2_FILE_NAME].rc2
    Ce fichier contient les ressources qui ne sont pas modifi�es par Microsoft  
    Visual C++. Vous devez placer toutes les ressources non modifiables par 
    l'�diteur de ressources dans ce fichier.
[!if CONTAINER_SERVER || FULL_SERVER || MINI_SERVER || AUTOMATION || HAS_SUFFIX]
[!if !HTML_EDITVIEW]

[!output PROJECT_NAME].reg
    Il s'agit d'un exemple de fichier .reg qui montre le type de param�tres 
    d'enregistrement que le framework d�finit pour vous.  Vous pouvez l'utiliser 
    comme fichier .reg
[!if APP_TYPE_DLG]
    pour votre application.
[!else]
    pour votre application ou le supprimer et utiliser  
    l'enregistrement par d�faut RegisterShellFileTypes.
[!endif]

[!if AUTOMATION]
[!output SAFE_IDL_NAME].idl
    Ce fichier contient le code source IDL (Interface Description Language) de  
    la biblioth�que de types de votre application.
[!endif]
[!endif]
[!endif]

[!if !APP_TYPE_DLG]
/////////////////////////////////////////////////////////////////////////////

Pour la fen�tre frame principale :
[!if PROJECT_STYLE_EXPLORER]
    Style de Windows Explorer : Le projet contiendra une interface de type 
    Windows Explorer avec deux frames.
[!else]
    Le projet comprend une interface MFC standard.
[!endif]

[!output MAIN_FRAME_HEADER], [!output MAIN_FRAME_IMPL]
    Ces fichiers contiennent la classe de frame [!output MAIN_FRAME_CLASS] 
    d�riv�e de
[!if APP_TYPE_MDI]
    CMDIFrameWnd et qui contr�le toutes les fonctionnalit�s des frames MDI.
[!else]
    CFrameWnd et qui contr�le toutes les fonctionnalit�s des frames SDI.
[!endif]
[!if PROJECT_STYLE_EXPLORER]

[!output TREE_VIEW_HEADER], [!output TREE_VIEW_IMPL]
    Ces fichiers contiennent la classe de frame gauche [!output TREE_VIEW_CLASS] 
    d�riv�e de CTreeView.
[!endif]

[!if DOCKING_TOOLBAR]
res\Toolbar.bmp
    Ce fichier bitmap sert � cr�er des images en mosa�que pour la barre 
    d'outils.
    La barre d'outils et la barre d'�tat initiales sont construites dans la 
    classe [!output MAIN_FRAME_CLASS]. Modifiez cette image de barre d'outils 
    avec l'�diteur de ressources et actualisez le tableau 
    IDR_MAINFRAME TOOLBAR dans [!output PROJECT_NAME].rc 
pour ajouter 
    les boutons de barre d'outils.
[!if MINI_SERVER || FULL_SERVER || CONTAINER_SERVER]

res\IToolbar.bmp
    Ce fichier bitmap sert � cr�er des images en mosa�que pour la barre d'outils 
    quand votre application serveur est activ�e sur place dans un autre 
    conteneur. Cette barre d'outils est construite dans la classe 
    [!output INPLACE_FRAME_CLASS]. Cette bitmap est semblable � celle qui se 
    trouve dans res\Toolbar.bmp mais ne contient pas les commandes non d�di�es 
    au serveur.

[!endif]
[!endif]
[!if APP_TYPE_MDI]
/////////////////////////////////////////////////////////////////////////////

Pour la fen�tre frame enfant :

ChildFrm.h, ChildFrm.cpp
    Ces fichiers d�finissent et impl�mentent la classe [!output 
CHILD_FRAME_CLASS] 
    qui prend en charge les fen�tres enfants d'une application MDI.

[!endif]
/////////////////////////////////////////////////////////////////////////////

[!if DOCVIEW]
[!if !DB_VIEW_NO_FILE]
L'Assistant Application cr�e un type de document et une vue :

[!output DOC_HEADER], [!output DOC_IMPL] - le document
    Ces fichiers contiennent votre classe [!output DOC_CLASS].  Modifiez ces 
fichiers pour 
    ajouter les donn�es de document sp�ciales et impl�menter l'enregistrement 
    et le chargement des fichiers (via [!output DOC_CLASS]::Serialize).
[!if HAS_SUFFIX && !HTML_EDITVIEW]
    Le document contiendra les cha�nes suivantes :
        Extension de fichier :         [!output FILE_EXTENSION]
        ID du type de fichier :        [!output FILE_TYPE_ID]
        Titre du frame principal :     [!output MAIN_FRAME_CAPTION]
        Nom du type de document :      [!output DOC_TYPE_NAME]
        Nom de filtre :                [!output FILTER_NAME]
        Nom court de nouveau fichier : [!output FILE_NEW_NAME_SHORT]
        Nom long du type de fichier :  [!output FILE_NEW_NAME_LONG]
[!endif]
[!else]
L'Assistant Application cr�e une vue :
[!endif]

[!output VIEW_HEADER], [!output VIEW_IMPL] - la vue du document
    Ces fichiers contiennent votre classe [!output VIEW_CLASS].
[!if !DB_VIEW_NO_FILE]
    Les objets [!output VIEW_CLASS] servent � afficher les objets 
    [!output DOC_CLASS].
[!endif]

[!if APP_TYPE_MDI]
res\[!output PROJECT_NAME]Doc.ico
    Il s'agit d'un fichier ic�ne, qui est utilis� comme ic�ne des fen�tres 
    enfants MDI pour la classe [!output DOC_CLASS].  Cette ic�ne est incluse 
    par le fichier de ressources principal [!output PROJECT_NAME].rc.
[!endif]

[!endif]

[!if DB_VIEW_NO_FILE || DB_VIEW_WITH_FILE]
/////////////////////////////////////////////////////////////////////////////

Prise en charge des bases de donn�es :

[!output ROWSET_HEADER], [!output ROWSET_IMPL]
    Ces fichiers contiennent votre classe [!output ROWSET_CLASS].  Cette 
    classe sert � acc�der � la source de donn�es s�lectionn�e dans 
    l'Assistant.
[!if DB_VIEW_NO_FILE]
    La s�rialisation ne sera pas prise en charge.
[!else]
    La s�rialisation est prise en charge.
[!endif]
[!endif]
[!if CONTAINER || FULL_SERVER || MINI_SERVER || CONTAINER_SERVER]
/////////////////////////////////////////////////////////////////////////////

L'Assistant Application a �galement cr�� des classes sp�cifiques � OLE

[!if CONTAINER || CONTAINER_SERVER]
[!output CONTAINER_ITEM_HEADER], [!output CONTAINER_ITEM_IMPL]
    Ces fichiers contiennent votre classe [!output CONTAINER_ITEM_CLASS]. 
    Cette classe sert � manipuler les objets OLE.  Les objets OLE sont 
    habituellement affich�s par votre classe [!output VIEW_CLASS] et 
    s�rialis�s avec votre classe 
[!output DOC_CLASS].
[!if ACTIVE_DOC_CONTAINER]
    Le programme prend en charge l'int�gration des documents actifs dans son 
    frame.
[!endif]
[!endif]
[!if MINI_SERVER || FULL_SERVER || CONTAINER_SERVER]

[!output SERVER_ITEM_HEADER], [!output SERVER_ITEM_IMPL]
    Ces fichiers contiennent votre classe [!output SERVER_ITEM_CLASS]. Cette 
    classe sert � connecter votre classe [!output DOC_CLASS] au syst�me OLE 
    et, �ventuellement, � fournir des liens vers votre document.
[!if ACTIVE_DOC_SERVER]
    Le projet permet de cr�er et de g�rer des documents actifs.
[!endif]

[!output INPLACE_FRAME_HEADER], [!output INPLACE_FRAME_IMPL]
    Ces fichiers contiennent votre classe [!output INPLACE_FRAME_CLASS]. Cette 
    classe est d�riv�e de COleIPFrameWnd et contr�le toutes les 
    fonctionnalit�s de frame au cours de l'activation sur place.
[!endif]

[!if SUPPORT_COMPOUND_FILES]
    Le projet prend en charge les fichiers compos�s. Le format de fichier 
    compos� permet de stocker un document qui contient un ou plusieurs objets 
    Automation dans un fichier, tout en laissant accessible chacun de ces 
    objets.
[!endif]
[!endif]
[!else]

/////////////////////////////////////////////////////////////////////////////

[!if AUTOMATION]
L'Assistant Application cr�e une classe de bo�te de dialogue et une classe 
proxy Automation :
[!else]
L'Assistant Application cr�e une classe de bo�te de dialogue :
[!endif]

[!output DIALOG_HEADER], [!output DIALOG_IMPL] � la bo�te de dialogue
    Ces fichiers contiennent votre classe [!output DIALOG_CLASS].  Cette 
    classe d�finit le comportement de la bo�te de dialogue principale de 
    votre application.  Le mod�le de bo�te de dialogue se trouve dans 
    [!output PROJECT_NAME].rc et peut �tre modifi� dans Microsoft 
    Visual C++.
[!if AUTOMATION]

[!output DIALOG_AUTO_PROXY_HEADER], [!output DIALOG_AUTO_PROXY_IMPL] - l'objet 
Automation
    Ces fichiers contiennent votre classe [!output DIALOG_AUTO_PROXY_CLASS].  
    Cette classe est appel�e classe proxy Automation pour votre bo�te de 
    dialogue, car elle prot�ge les m�thodes et les propri�t�s Automation que 
    les contr�leurs Automation peuvent utiliser pour acc�der � votre bo�te 
    de dialogue.  Ces m�thodes et propri�t�s ne sont pas directement expos�es 
    par la classe de bo�te de dialogue car, dans le cas d'une application 
    MFC modale bas�e sur des bo�tes de dialogue, il est plus simple de s�parer 
    l'objet Automation de l'interface utilisateur.
[!endif]
[!endif]

[!if CONTEXT_HELP]
/////////////////////////////////////////////////////////////////////////////

Prise en charge de l'aide :

[!if HELPSTYLE_HTML]
hlp\[!output SAFE_PROJECT_HELP_FILE_NAME].hhp
    Ce fichier est un fichier projet d'aide. Il contient les donn�es requises 
    pour compiler les fichiers d'aide dans un fichier .chm.

hlp\[!output SAFE_PROJECT_HELP_FILE_NAME].hhc
    Ce fichier r�pertorie le contenu du projet d'aide.

hlp\[!output SAFE_PROJECT_HELP_FILE_NAME].hhk
    Ce fichier contient un index des rubriques d'aide.

hlp\afxcore.htm
    Ce fichier contient les rubriques d'aide standard relatives aux 
    objets de l'�cran et aux commandes MFC standard. Ajoutez � ce fichier vos 
    propres rubriques d'aide.

[!if PRINTING]
hlp\afxprint.htm
    Ce fichier contient les rubriques d'aide relatives aux commandes 
    d'impression.

[!endif]
makehtmlhelp.bat
    Ce fichier est utilis� par le syst�me de g�n�ration pour compiler les 
    fichiers d'aide.

hlp\Images\*.gif
    Il s'agit de fichiers bitmap requis par le fichier des rubriques d'aide 
    standard pour les commandes standard de la biblioth�que Microsoft 
    Foundation Class.

[!else]
hlp\[!output PROJECT_NAME].hpj
    Il s'agit du fichier d'aide du projet utilis� par le compilateur d'aide 
    pour cr�er votre fichier d'aide de l'application.

hlp\*.bmp
    Il s'agit de fichiers bitmap requis par le fichier des rubriques d'aide 
    standard pour les commandes standard de la biblioth�que Microsoft 
    Foundation Class.

hlp\*.rtf
    Ces fichiers contiennent les rubriques d'aide standard pour les objets 
    de l'�cran et commandes MFC standard.
[!endif]
[!endif]

[!if ACTIVEX_CONTROLS || PRINTING || SPLITTER || MAPI || SOCKETS]
/////////////////////////////////////////////////////////////////////////////

Autres fonctionnalit�s :
[!if ACTIVEX_CONTROLS]

Contr�les ActiveX
    L'application comprend la prise en charge des contr�les ActiveX.
[!endif]
[!if PRINTING]

Prise en charge de l'impression et de l'aper�u avant impression
    L'Assistant Application a g�n�r� un code pour g�rer les commandes 
    d'impression, de configuration de l'impression et d'aper�u avant 
    impression en appelant les fonctions membres dans la classe CView, � 
    partir de la biblioth�que MFC.
[!endif]
[!if DB_SUPPORT_HEADER_ONLY && !APP_TYPE_DLG]

Prise en charge des bases de donn�es
    L'Assistant Application a ajout� le niveau de base de la prise en charge 
    des bases de donn�es pour votre programme. 
    Seuls les fichiers n�cessaires sont inclus.
[!endif]
[!if SPLITTER && !APP_TYPE_DLG]

Fen�tre fractionn�e
    L'Assistant Application a ajout� la prise en charge des fen�tre 
    fractionn�es pour les documents de votre application.
[!endif]
[!if MAPI]

Prise en charge de MAPI
    Le projet cr�� contient le code n�cessaire pour cr�er, manipuler, 
    transf�rer et stocker les messages �lectroniques.
[!endif]
[!if SOCKETS]

Windows Sockets
    L'application peut prendre en charge l'�tablissement des communications 
    dans les r�seaux TCP/IP.
[!endif]

[!endif]
/////////////////////////////////////////////////////////////////////////////

Autres fichiers standard :

StdAfx.h, StdAfx.cpp
    Ces fichiers sont utilis�s pour g�n�rer un fichier d'en-t�te pr�compil� 
    (PCH) nomm� [!output PROJECT_NAME].pch et un fichier de type pr�compil� 
    nomm� Stdafx.obj.

Resource.h
    Il s'agit du ficher d'en-t�te standard, qui d�finit les nouveaux ID de 
    ressources.
    Microsoft Visual C++ lit et met � jour ce fichier.

[!if MANIFEST]
[!output PROJECT_NAME].manifest
    Les fichiers manifestes d'application sont utilis�s par Windows XP pour 
    d�crire les d�pendances des applications sur des versions sp�cifiques 
    des assemblys c�te � c�te. Le chargeur utilise ces informations pour 
    charger l'assembly appropri� � partir du cache de l'assembly ou 
    directement � partir de l'application. Le manifeste de l'application peut 
    �tre inclus pour redistribution comme fichier .manifest externe install� 
    dans le m�me dossier que l'ex�cutable de l'application ou �tre inclus 
    dans l'ex�cutable sous la forme d'une ressource. 
[!endif]
/////////////////////////////////////////////////////////////////////////////

Autres remarques :

L'Assistant Application utilise "TODO:" pour indiquer les parties du code 
source o� vous devrez ajouter ou modifier du code.
[!if APP_TYPE_MDI || APP_TYPE_SDI || APP_TYPE_DLG || APP_TYPE_MTLD]

Si votre application utilise les MFC dans une DLL partag�e vous devez 
redistribuer les DLL MFC. Si la langue de votre application n'est pas celle 
du syst�me d'exploitation, vous devez �galement redistribuer le fichier des 
ressources localis�es MFC100XXX.DLL. Pour plus d'informations, consultez la 
section relative � la redistribution des applications Visual C++ dans la 
documentation MSDN.
[!endif]

/////////////////////////////////////////////////////////////////////////////
