﻿========================================================================
    ACTIVE TEMPLATE LIBRARY: [!output PROJECT_NAME] 
                             Información general del proyecto
========================================================================

AppWizard ha creado este proyecto [!output PROJECT_NAME] para que lo utilice 
como punto de partida para escribir su biblioteca de vínculos dinámicos (DLL)

Este archivo contiene un resumen de lo que encontrará en todos los archivos
que constituyen la aplicación DLL .

[!output PROJECT_NAME].vcxproj
    Éste es el archivo de proyecto principal para los proyectos de VC++ 
    generados mediante un Asistente para aplicaciones.
    Contiene información acerca de la versión de Visual C++ con la que se 
    generó el archivo, así como información acerca de las plataformas, 
    configuraciones y características del proyecto seleccionadas en el 
    asistente para aplicaciones.

[!output PROJECT_NAME].vcxproj.filters
    Éste es el archivo de filtros para los proyectos de VC++ generados mediante 
    un asistente para aplicaciones. 
    Contiene información acerca de la asociación entre los archivos de un 
    proyecto y los filtros. Esta asociación se usa en el IDE para mostrar la 
    agrupación de archivos con extensiones similares bajo un nodo específico 
    (por ejemplo, los archivos ".cpp" se asocian con el filtro "Archivos de 
    código fuente").

[!output SAFE_IDL_NAME].idl
    Este archivo contiene definiciones IDL de la biblioteca de tipos, las 
    interfaces y las coclases definidas en el proyecto.
    El compilador MIDL procesará este archivo para generar:
        definiciones de la interfaz de C++ 
             y declaraciones                  ([!output SAFE_IDL_NAME].h)
        de GUID                               ([!output SAFE_IDL_NAME]_i.c)
        Biblioteca de tipos                   ([!output SAFE_IDL_NAME].tlb)
        Código de cálculo de referencias      ([!output SAFE_IDL_NAME]_p.c y 
                                                 dlldata.c)

[!output SAFE_IDL_NAME].h
    Este archivo contiene las definiciones de la interfaz C++ y las 
    declaraciones GUID de los elementos definidos en 
    [!output SAFE_IDL_NAME].idl. MIDL vuelve a generar este archivo 
    durante la compilación.

[!output PROJECT_NAME].cpp
    Este archivo contiene el mapa de objetos y la implementación de las 
    exportaciones de los archivos DLL.

[!output PROJECT_NAME].rc
    Ésta es una lista de todos los recursos de Microsoft Windows que utiliza
    el programa.

[!if DLL_APP] 
[!output PROJECT_NAME].def
    Este archivo de definición de módulo proporciona al vinculador información
    acerca de las exportaciones necesarias para el archivo DLL. Contiene 
    exportaciones para:
        DllGetClassObject
        DllCanUnloadNow
        DllRegisterServer
        DllUnregisterServer
        DllInstall
[!endif] 

/////////////////////////////////////////////////////////////////////////////
Otros archivos estándar:

StdAfx.h, StdAfx.cpp
    Estos archivos se utilizan para generar un archivo de encabezado
    precompilado (PCH) denominado [!output PROJECT_NAME].pch y un archivo de 
    tipos precompilado llamado StdAfx.obj.

Resource.h
    Éste es el archivo de encabezado estándar que define identificadores de 
    recurso.

/////////////////////////////////////////////////////////////////////////////
Otras notas:

	La opción de compatibilidad con MFC genera la biblioteca Microsoft 
        Foundation Class en su aplicación esqueleto, poniendo a su disposición 
        clases, objetos y funciones MFC.
/////////////////////////////////////////////////////////////////////////////
