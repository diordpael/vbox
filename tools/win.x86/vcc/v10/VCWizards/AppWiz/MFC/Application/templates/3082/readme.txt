================================================================================
    BIBLIOTECA MICROSOFT FOUNDATION CLASS: Informaci�n general del 
    proyecto[!output PROJECT_NAME]
================================================================================

El Asistente para aplicaciones ha creado esta aplicaci�n [!output PROJECT_NAME].
Esta aplicaci�n no s�lo muestra los fundamentos del uso de Microsoft Foundation 
Classes sino que tambi�n supone un punto de partida para el desarrollo de la 
propia aplicaci�n.

Este archivo incluye un resumen acerca del contenido de los archivos que
constituyen su aplicaci�n [!output PROJECT_NAME].

[!output PROJECT_NAME].vcxproj
    �ste es el archivo de proyecto principal para los proyectos de VC++ 
    generados mediante un asistente para aplicaciones. 
    Contiene informaci�n acerca de la versi�n de Visual C++ con la que se 
    gener� el archivo, as� como informaci�n acerca de las plataformas, 
    configuraciones y caracter�sticas del proyecto seleccionadas en el
    Asistente para aplicaciones.

[!output PROJECT_NAME].vcxproj.filters
    �ste es el archivo de filtros para los proyectos de VC++ generados mediante 
    un asistente para aplicaciones. 
    Contiene informaci�n acerca de la asociaci�n entre los archivos de un 
    proyecto 
y los filtros. Esta asociaci�n se usa en el IDE para mostrar la 
    agrupaci�n de archivos con extensiones similares bajo un nodo espec�fico 
    (por ejemplo, los archivos ".cpp" se asocian con el filtro "Archivos de 
    c�digo fuente").

[!output APP_HEADER]
    �ste es el archivo de encabezado principal para la aplicaci�n. Incluye otros
    encabezados espec�ficos del proyecto y declara la clase de aplicaci�n
    [!output APP_CLASS].

[!output APP_IMPL]
    �ste es el archivo fuente principal de la aplicaci�n contenido en la clase 
    de la aplicaci�n [!output APP_CLASS].

[!output RC_FILE_NAME]
    �sta es una lista de todos los recursos de Microsoft Windows que utiliza el 
    programa. Incluye los iconos, mapas de bits y cursores almacenados en el 
    subdirectorio RES. Este archivo puede editarse directamente en Microsoft
    Visual C++. Los recursos de su proyecto se encuentran en [!output LCID].

res\[!output PROJECT_NAME].ico
    �ste es un archivo de icono, que se utiliza como el icono de la aplicaci�n.
    Este icono est� incluido en el archivo principal de recursos 
    [!output PROJECT_NAME].rc.

res\[!output RC2_FILE_NAME].rc2
    Este archivo incluye recursos no editados por Microsoft 
    Visual C++. Debe colocar todos los recursos que no se pueden editar en 
    los editores de recursos de este archivo.
[!if CONTAINER_SERVER || FULL_SERVER || MINI_SERVER || AUTOMATION || HAS_SUFFIX]
[!if !HTML_EDITVIEW]

[!output PROJECT_NAME].reg
    �ste es un ejemplo de archivo .reg que le muestra el tipo de valores de 
    registro que establecer� el marco de trabajo. Puede utilizarlo como un 
    archivo .reg
[!if APP_TYPE_DLG]
    que acompa�e a su aplicaci�n.
[!else]
    que acompa�e a su aplicaci�n, o eliminarlo y basarse en el registro 
    predeterminado RegisterShellFileTypes.
[!endif]

[!if AUTOMATION]
[!output SAFE_IDL_NAME].idl
    Este archivo contiene el c�digo fuente del Lenguaje de descripci�n de 
    interfaces para la biblioteca de tipos de su aplicaci�n.
[!endif]
[!endif]
[!endif]

[!if !APP_TYPE_DLG]
/////////////////////////////////////////////////////////////////////////////

Para la ventana de marco principal:
[!if PROJECT_STYLE_EXPLORER]
    Estilo del Explorador de Windows: el proyecto incluir� una interfaz similar 
    a la del Explorador de Windows, con dos marcos.
[!else]
    El proyecto incluye una interfaz MFC est�ndar.
[!endif]

[!output MAIN_FRAME_HEADER], [!output MAIN_FRAME_IMPL]
    Estos archivos contienen la clase de marco [!output MAIN_FRAME_CLASS], que se 
    deriva de
[!if APP_TYPE_MDI]
    CMDIFrameWnd y que controla todas las caracter�sticas del marco MDI.
[!else]
    CFrameWnd y que controla todas las caracter�sticas del marco SDI.
[!endif]
[!if PROJECT_STYLE_EXPLORER]

[!output TREE_VIEW_HEADER], [!output TREE_VIEW_IMPL]
    Estos archivos contienen la clase de marco de la parte izquierda 
    [!output TREE_VIEW_CLASS], que se deriva de CTreeView.
[!endif]

[!if DOCKING_TOOLBAR]
res\Toolbar.bmp
    Este archivo de mapa de bits se utiliza para crear im�genes organizadas en 
    mosaico para la barra de herramientas.
    La barra de herramientas inicial y la barra de estado se crean en la clase 
    [!output MAIN_FRAME_CLASS]. Edite este mapa de bits de barra de herramientas 
    utilizando los editores de recursos, y actualice la matriz 
    IDR_MAINFRAME TOOLBAR en [!output PROJECT_NAME].rc para agregar botones de 
    barra de herramientas.
[!if MINI_SERVER || FULL_SERVER || CONTAINER_SERVER]

res\IToolbar.bmp
    Este archivo de mapa de bits se utiliza para crear im�genes organizadas en 
    mosaico para la barra de herramientas, cuando la aplicaci�n de servidor se 
    activa en el contexto dentro de otra aplicaci�n contenedora. Esta barra de 
    herramientas se crea en la clase [!output INPLACE_FRAME_CLASS]. Este mapa de 
    bits es similar al mapa de bits de res\Toolbar.bmp salvo que se le han 
    quitado muchos comandos no relacionados con el servidor.

[!endif]
[!endif]
[!if APP_TYPE_MDI]
/////////////////////////////////////////////////////////////////////////////

Para la ventana de marco secundaria:

ChildFrm.h, ChildFrm.cpp
    Estos archivos definen e implementan la clase [!output CHILD_FRAME_CLASS], 
    que permite ventanas secundarias en una aplicaci�n MDI.

[!endif]
/////////////////////////////////////////////////////////////////////////////

[!if DOCVIEW]
[!if !DB_VIEW_NO_FILE]
El Asistente para aplicaciones crea un tipo de documento y una sola vista:

[!output DOC_HEADER], [!output DOC_IMPL] � el documento
    Estos archivos contienen su clase [!output DOC_CLASS]. Edite estos archivos 
    para agregar los datos del documento especial y para guardar y cargar el 
    archivo (mediante [!output DOC_CLASS]::Serialize).
[!if HAS_SUFFIX && !HTML_EDITVIEW]
    El documento constar� de las siguientes cadenas:
        Extensi�n del archivo:            [!output FILE_EXTENSION]
        Id. del tipo de archivo:          [!output FILE_TYPE_ID]
        T�tulo del marco principal:       [!output MAIN_FRAME_CAPTION]
        Nombre del tipo de documento:     [!output DOC_TYPE_NAME]
        Nombre del filtro:                [!output FILTER_NAME]
        Nuevo nombre corto del archivo:   [!output FILE_NEW_NAME_SHORT]
        Nombre largo del tipo de archivo: [!output FILE_NEW_NAME_LONG]
[!endif]
[!else]
El Asistente para aplicaciones crea una sola vista:
[!endif]

[!output VIEW_HEADER], [!output VIEW_IMPL] � la vista del documento
    Estos archivos contienen su clase [!output VIEW_CLASS].
[!if !DB_VIEW_NO_FILE]
    Los objetos [!output VIEW_CLASS] se utilizan para ver objetos 
    [!output DOC_CLASS].
[!endif]

[!if APP_TYPE_MDI]
res\[!output PROJECT_NAME]Doc.ico
    �ste es un archivo de icono, que se utiliza como el icono para las ventanas 
    secundarias MDI para la clase [!output DOC_CLASS]. Este icono lo incluye el 
    archivo principal de recursos [!output PROJECT_NAME].rc.
[!endif]

[!endif]

[!if DB_VIEW_NO_FILE || DB_VIEW_WITH_FILE]
/////////////////////////////////////////////////////////////////////////////

Compatibilidad con bases de datos:

[!output ROWSET_HEADER], [!output ROWSET_IMPL]
    Estos archivos contienen su clase [!output ROWSET_CLASS]. Esta clase se 
    utiliza para obtener acceso a la fuente de datos que seleccion� en el 
    Asistente.
[!if DB_VIEW_NO_FILE]
    No se agregar� compatibilidad de serializaci�n.
[!else]
    Se ha agregado compatibilidad de serializaci�n.
[!endif]
[!endif]
[!if CONTAINER || FULL_SERVER || MINI_SERVER || CONTAINER_SERVER]
/////////////////////////////////////////////////////////////////////////////

El Asistente para aplicaciones tambi�n ha creado clases espec�ficas para OLE

[!if CONTAINER || CONTAINER_SERVER]
[!output CONTAINER_ITEM_HEADER], [!output CONTAINER_ITEM_IMPL]
    Estos archivos contienen su clase [!output CONTAINER_ITEM_CLASS]. Esta clase 
    se utiliza para manipular objetos OLE. Los objetos OLE se suelen mostrar por 
    su clase [!output VIEW_CLASS] y serializarse como parte de su clase 
    [!output DOC_CLASS].
[!if ACTIVE_DOC_CONTAINER]
    El programa es compatible con los documentos activos en su marco.
[!endif]
[!endif]
[!if MINI_SERVER || FULL_SERVER || CONTAINER_SERVER]

[!output SERVER_ITEM_HEADER], [!output SERVER_ITEM_IMPL]
    Estos archivos contienen [!output SERVER_ITEM_CLASS]. Esta clase se utiliza 
    para conectar su clase [!output DOC_CLASS] con el sistema OLE y, 
    opcionalmente, proporcionar v�nculos a su documento.
[!if ACTIVE_DOC_SERVER]
    El proyecto permite crear y administrar documentos activos.
[!endif]

[!output INPLACE_FRAME_HEADER], [!output INPLACE_FRAME_IMPL]
    Estos archivos contienen [!output INPLACE_FRAME_CLASS]. Esta clase se deriva 
    de COleIPFrameWnd y controla todas las caracter�sticas del marco durante la 
    activaci�n en contexto.
[!endif]

[!if SUPPORT_COMPOUND_FILES]
    El proyecto es compatible con archivos compuestos. El formato de archivos 
    compuestos almacena un documento que contiene uno o varios objetos de 
    automatizaci�n para un archivo y sigue permitiendo el acceso al mismo a 
    objetos individuales.
[!endif]
[!endif]
[!else]

/////////////////////////////////////////////////////////////////////////////

[!if AUTOMATION]
El Asistente para aplicaciones crea una clase de cuadro de di�logo y una clase 
de proxy de automatizaci�n:
[!else]
El Asistente para aplicaciones crea una sola clase de cuadro de di�logo:
[!endif]

[!output DIALOG_HEADER], [!output DIALOG_IMPL] � el cuadro de di�logo
    Estos archivos contienen su clase [!output DIALOG_CLASS].  Esta clase define 
    el comportamiento del cuadro de di�logo principal de su aplicaci�n. La 
    plantilla del cuadro de di�logo est� en [!output PROJECT_NAME].rc, que puede 
    editarse en Microsoft Visual C++.
[!if AUTOMATION]

[!output DIALOG_AUTO_PROXY_HEADER], [!output DIALOG_AUTO_PROXY_IMPL] � el objeto 
de automatizaci�n
    Estos archivos contienen su clase [!output DIALOG_AUTO_PROXY_CLASS]. Esta 
    clase se denomina clase de proxy de automatizaci�n para su cuadro de di�logo,
    porque se ocupa de exponer los m�todos y las propiedades de automatizaci�n 
    que pueden utilizar los controladores de automatizaci�n para permitir el 
    acceso a su cuadro de di�logo. Estos m�todos y propiedades no se exponen 
    desde la clase de cuadro de di�logo directamente, porque en el caso de una 
    aplicaci�n MFC basada en un cuadro de di�logo modal es m�s claro y f�cil 
    mantener el objeto de automatizaci�n separado de la interfaz de usuario.
[!endif]
[!endif]

[!if CONTEXT_HELP]
/////////////////////////////////////////////////////////////////////////////

Compatibilidad con la Ayuda:

[!if HELPSTYLE_HTML]
hlp\[!output SAFE_PROJECT_HELP_FILE_NAME].hhp
    Este archivo es un archivo de proyecto de ayuda. Contiene los datos 
    necesarios para compilar los archivos de ayuda en un archivo .chm.

hlp\[!output SAFE_PROJECT_HELP_FILE_NAME].hhc
    Este archivo muestra una lista del contenido del proyecto de ayuda.

hlp\[!output SAFE_PROJECT_HELP_FILE_NAME].hhk
    Este archivo contiene un �ndice de los temas de la ayuda.

hlp\afxcore.htm
    Este archivo contiene los temas de la ayuda est�ndar para comandos y objetos 
    de pantalla est�ndar de MFC. Agregue sus propios temas de ayuda a este 
    archivo.

[!if PRINTING]
hlp\afxprint.htm
    Este archivo contiene los temas de ayuda para los comandos de impresi�n.

[!endif]
makehtmlhelp.bat
    El sistema de generaci�n utiliza este archivo para compilar los archivos de 
    ayuda.

hlp\Images\*.gif
    �stos son archivos de mapa de bits que necesitan los temas de archivo de la 
    ayuda est�ndar para los comandos est�ndar de la biblioteca Microsoft 
    Foundation Class.

[!else]
hlp\[!output PROJECT_NAME].hpj
    Este archivo es el archivo de proyecto de ayuda que utiliza el compilador de 
    ayudas para crear el archivo de ayuda de su aplicaci�n.

hlp\*.bmp
    �stos son archivos de mapa de bits que necesitan los temas de archivo de la 
    Ayuda est�ndar para los comandos est�ndar de la biblioteca Microsoft 
    Foundation Class.

hlp\*.rtf
    Estos archivos contienen los temas de la Ayuda est�ndar para comandos y 
    objetos de pantalla est�ndar de MFC.
[!endif]
[!endif]

[!if ACTIVEX_CONTROLS || PRINTING || SPLITTER || MAPI || SOCKETS]
/////////////////////////////////////////////////////////////////////////////

Otras caracter�sticas:
[!if ACTIVEX_CONTROLS]

Controles ActiveX
    La aplicaci�n es compatible con el uso de los controles ActiveX.
[!endif]
[!if PRINTING]

Compatibilidad con la impresi�n y la vista preliminar
    Este Asistente para aplicaciones genera el c�digo necesario para controlar 
    los comandos de impresi�n, la configuraci�n de la impresora y la vista 
    preliminar mediante llamadas a funciones miembro de la clase CView de la 
    biblioteca MFC.
[!endif]
[!if DB_SUPPORT_HEADER_ONLY && !APP_TYPE_DLG]

Compatibilidad con bases de datos
    El Asistente para aplicaciones ha agregado el nivel b�sico de compatibilidad 
    con bases de datos para su programa. 
    S�lo se han incluido los archivos necesarios.
[!endif]
[!if SPLITTER && !APP_TYPE_DLG]

Ventana divisora
    El Asistente para aplicaciones ha agregado compatibilidad con ventanas 
    divisoras para los documentos de su aplicaci�n.
[!endif]
[!if MAPI]

Compatibilidad con MAPI
    El proyecto generado contiene el c�digo necesario para crear, manipular, 
    transferir y almacenar mensajes de correo.
[!endif]
[!if SOCKETS]

Windows Sockets
    La aplicaci�n puede establecer comunicaciones con redes TCP/IP.
[!endif]

[!endif]
/////////////////////////////////////////////////////////////////////////////

Otros archivos est�ndar:

StdAfx.h, StdAfx.cpp
    Estos archivos se utilizan para generar un archivo de encabezado 
    precompilado (PCH) denominado [!output PROJECT_NAME].pch y un archivo de 
    tipos precompilado llamado StdAfx.obj.

Resource.h
    �ste es el archivo de encabezado est�ndar, que define nuevos identificadores 
    de recurso.
    Microsoft Visual C++ lee y actualiza este archivo.

[!if MANIFEST]
[!output PROJECT_NAME].manifest
    Windows XP utiliza los archivos de manifiesto de la aplicaci�n para describir 
    la dependencia de una aplicaci�n en versiones espec�ficas de ensamblados 
    simult�neos. El cargador utiliza esta informaci�n para cargar el ensamblado 
    adecuado desde la cach� de ensamblados o desde el directorio privado de la 
    aplicaci�n. El manifiesto de la aplicaci�n puede incluirse para su 
    redistribuci�n como un archivo .manifest instalado en la misma carpeta que 
    el ejecutable de la aplicaci�n o puede incluirse en el ejecutable en forma 
    de recurso. 
[!endif]
/////////////////////////////////////////////////////////////////////////////

Otras notas:

El Asistente para aplicaciones utiliza "TODO:" para indicar partes del c�digo 
fuente que deber�a agregar o personalizar.
[!if APP_TYPE_MDI || APP_TYPE_SDI || APP_TYPE_DLG || APP_TYPE_MTLD]

Si su aplicaci�n utiliza MFC en un archivo DLL compartido, tendr� que copiar
los archivos DLL MFC. Si el idioma de su aplicaci�n es distinto al idioma actual
del sistema operativo, tambi�n tendr� que copiar los recursos localizados 
correspondientes MFC100XXX.DLL. Para obtener mas informaci�n sobre ambos temas, 
consulte la secci�n que trata sobre la copia de aplicaciones de Visual C++ en la 
documentaci�n de MSDN.
[!endif]

/////////////////////////////////////////////////////////////////////////////
