// Copyright (c) Microsoft Corporation. All rights reserved.

function OnFinish(selProj, selObj)
{
	try
	{
		wizard.AddSymbol("SAFE_PROJECT_HELP_FILE_NAME", CreateSafeName(wizard.FindSymbol("PROJECT_NAME")));
		wizard.AddSymbol("RC2_FILE_NAME", CreateASCIIName(wizard.FindSymbol("PROJECT_NAME")));
		wizard.AddSymbol("RC_FILE_NAME",CreateSafeRCFileName(wizard.FindSymbol("PROJECT_NAME")) + ".rc");

		if(wizard.FindSymbol("AUTOMATION"))
			wizard.AddSymbol("SAFE_IDL_NAME", CreateASCIIName(wizard.FindSymbol("PROJECT_NAME")));
		var bOLEDB = wizard.FindSymbol("OLEDB");
		if (bOLEDB)
		{
			var strRowsetClass = wizard.FindSymbol("ROWSET_CLASS");
			var bAttributed = wizard.FindSymbol("ATTRIBUTED");
			var strCode = wizard.GetConsumerClass(strRowsetClass, bAttributed);
			wizard.AddSymbol("ROWSET_CLASS_CODE", strCode);
		}
		else
		{
			var bODBC = wizard.FindSymbol("ODBC");
			if (bODBC)
			{
				var strRowsetClass = wizard.FindSymbol("ROWSET_CLASS");
				var bSnapshot = wizard.FindSymbol("SNAPSHOT");
				var bBindAllColumns = wizard.FindSymbol("BIND_ALL_COLUMNS");
				var strCodeDecl = wizard.GetODBCConsumerClassDecl(bBindAllColumns, bSnapshot, strRowsetClass);
				var strCodeImpl = wizard.GetODBCConsumerClassImpl();
				wizard.AddSymbol("ROWSET_CLASS_ODBC_DECL", strCodeDecl);
				wizard.AddSymbol("ROWSET_CLASS_ODBC_IMPL", strCodeImpl);
			}
		}

		var strProjectPath = wizard.FindSymbol("PROJECT_PATH");
		var strProjectName = wizard.FindSymbol("PROJECT_NAME");

		selProj = CreateProject(strProjectName, strProjectPath);

		AddCommonConfig(selProj, strProjectName,wizard.FindSymbol("UNICODE"));
		AddSpecificConfig(selProj, strProjectName, strProjectPath);

		SetupFilters(selProj);

		var bContextHelp = wizard.FindSymbol("CONTEXT_HELP");
		var bHTMLHelp = wizard.FindSymbol("HELPSTYLE_HTML");
		var L_strHelpFiles_Text = "Help Files";
		if (bContextHelp)
		{
			if (bHTMLHelp)
			{
				var strHelpFilter = wizard.FindSymbol("HTMLHELP_FILTER");
				var L_strHTMLHelpFiles_Text = "HTML Help Files";
				var group = selProj.Object.AddFilter(L_strHTMLHelpFiles_Text);
				group.Filter = strHelpFilter;

				var strHelpFilter = wizard.FindSymbol("HTMLHELP_TOPICSFILTER");
				var L_strHelpTopics_Text = "HTML Help Topics";
				var group = selProj.Object.AddFilter(L_strHelpTopics_Text);
				group.Filter = strHelpFilter;
			}
			else
			{
				var strHelpFilter = wizard.FindSymbol("HELP_FILTER");
				var group = selProj.Object.AddFilter(L_strHelpFiles_Text);
				group.Filter = strHelpFilter;
			}
		}
		
		var strDocTypeName = wizard.FindSymbol("DOC_TYPE_NAME"); 		
		if (strDocTypeName != "" )
		{
			wizard.AddSymbol("SAFE_DOC_TYPE_NAME", CreateSafeName(strDocTypeName));
		}
		
		selProj.Object.keyword = "MFCProj";

		SetResDlgFont();

		AddFilesToProjectWithInfFile(selProj, strProjectName);
		SetCommonPchSettings(selProj);

		if (bContextHelp && !bHTMLHelp)
			AddHelpImages(selProj, L_strHelpFiles_Text);
		
		selProj.Object.Save();

		if (wizard.FindSymbol("APP_TYPE_DLG"))
		{
			var strDialogId = "IDD_" + wizard.FindSymbol("UPPER_CASE_SAFE_PROJECT_IDENTIFIER_NAME") + "_DIALOG";
			var oResHelper = wizard.ResourceHelper;
			oResHelper.OpenResourceFile(strProjectPath + "\\" + strProjectName + ".rc");
			oResHelper.OpenResourceInEditor("Dialog", strDialogId);
		}
		if (bContextHelp)
			AddHelpBuildSteps(selProj.Object, strProjectName, bHTMLHelp);

		var fUnicode = wizard.FindSymbol("UNICODE");
		fUnicode = (fUnicode == true);
		SetAllConfigCharset(selProj, fUnicode);

		// Create ATL project with preview/search/thumbnail handlers if requested
		if (wizard.FindSymbol("PREVIEW_HANDLER") || wizard.FindSymbol("SEARCH_HANDLER") || wizard.FindSymbol("THUMBNAIL_HANDLER"))
		{
			var strATLProjectName = strProjectName + "Handlers";
			var strATLProjectNameSafe = CreateASCIIName(strATLProjectName);
			wizard.AddSymbol("CLOSE_SOLUTION", false);
			oFSO = new ActiveXObject("Scripting.FileSystemObject");
			strATLProjectPath = oFSO.GetAbsolutePathName(strProjectPath + "\\..\\" + strATLProjectName);
			var strBaseProjectName = wizard.FindSymbol("PROJECT_NAME");
			wizard.AddSymbol("BASE_PROJECT_NAME", strBaseProjectName);
			wizard.AddSymbol("PROJECT_NAME", strATLProjectName);
			wizard.AddSymbol("PROJECT_PATH", strATLProjectPath);
			wizard.AddSymbol("LIB_NAME", strATLProjectName);
			wizard.AddSymbol("UPPER_CASE_SAFE_PROJECT_IDENTIFIER_NAME", (CreateSafeName(strATLProjectName)).toUpperCase());
			wizard.AddSymbol("SAFE_IDL_NAME", strATLProjectNameSafe);
			wizard.AddSymbol("SAFE_ATL_MODULE_NAME", CreateClassName(strATLProjectNameSafe, "Module"));
			wizard.AddSymbol("SAFE_MFC_APP_NAME", CreateClassName(strATLProjectNameSafe, "App"));
			CreateGuids();

			var oATLProj = CreateProject(strATLProjectName, strATLProjectPath);
			SetupFilters(oATLProj);
			oATLProj.Object.keyword = "AtlProj";
			AddFilesToProjectWithNamedInfFile(oATLProj, strATLProjectName, "Handler_Templates.inf");

			SetCommonPchSettings(oATLProj);
			SetATLConfigsType(oATLProj, typeDynamicLibrary);
			SetATLConfigsATL(oATLProj);
			SetAllConfigCharset(oATLProj, true);
			SetATLConfigsToolSettings(oATLProj, strATLProjectName);
			SetATLConfigsMFC(oATLProj);
			SetFileSettings(oATLProj);

			var strMFCDocHeader = "..\\" + strBaseProjectName + "\\" + wizard.FindSymbol("DOC_HEADER");
			if (oATLProj.Object.CanAddFile(strMFCDocHeader))
			{
				oATLProj.Object.AddFile(strMFCDocHeader);
			}
			var strMFCDocImpl = "..\\" + strBaseProjectName + "\\" + wizard.FindSymbol("DOC_IMPL");
			if (oATLProj.Object.CanAddFile(strMFCDocImpl))
			{
				oATLProj.Object.AddFile(strMFCDocImpl);
			}

			if (wizard.FindSymbol("PREVIEW_HANDLER"))
			{
				var strMFCViewHeader = "..\\" + strBaseProjectName + "\\" + wizard.FindSymbol("VIEW_HEADER");
				if (oATLProj.Object.CanAddFile(strMFCViewHeader))
				{
					oATLProj.Object.AddFile(strMFCViewHeader);
				}
				var strMFCViewImpl = "..\\" + strBaseProjectName + "\\" + wizard.FindSymbol("VIEW_IMPL");
				if (oATLProj.Object.CanAddFile(strMFCViewImpl))
				{
					oATLProj.Object.AddFile(strMFCViewImpl);
				}
			}

			var strMFCCntrHeader = "..\\" + strBaseProjectName + "\\CntrItem.h";
			strFile = oFSO.GetAbsolutePathName(strATLProjectPath + "\\" + strMFCCntrHeader);
			if (oFSO.FileExists(strFile) && oATLProj.Object.CanAddFile(strMFCCntrHeader)) {
				oATLProj.Object.AddFile(strMFCCntrHeader);

				var strMFCCntrImpl = "..\\" + strBaseProjectName + "\\CntrItem.cpp";
				strFile = oFSO.GetAbsolutePathName(strATLProjectPath + "\\" + strMFCCntrImpl);
				if (oFSO.FileExists(strFile) && oATLProj.Object.CanAddFile(strMFCCntrImpl)) {
					oATLProj.Object.AddFile(strMFCCntrImpl);
				}
			}

			var strMFCSrvrHeader = "..\\" + strBaseProjectName + "\\SrvrItem.h";
			strFile = oFSO.GetAbsolutePathName(strATLProjectPath + "\\" + strMFCSrvrHeader);
			if (oFSO.FileExists(strFile) && oATLProj.Object.CanAddFile(strMFCSrvrHeader)) {
			    oATLProj.Object.AddFile(strMFCSrvrHeader);

			    var strMFCSrvrImpl = "..\\" + strBaseProjectName + "\\SrvrItem.cpp";
			    strFile = oFSO.GetAbsolutePathName(strATLProjectPath + "\\" + strMFCSrvrImpl);
			    if (oFSO.FileExists(strFile) && oATLProj.Object.CanAddFile(strMFCSrvrImpl)) {
			        oATLProj.Object.AddFile(strMFCSrvrImpl);
			    }
			}

			var L_strGenerated_Text = "Generated Files";
			var strIdlName = wizard.FindSymbol("SAFE_IDL_NAME");
			var oGeneratedFiles = oATLProj.Object.AddFilter(L_strGenerated_Text);
			if (oGeneratedFiles)
			{
				oGeneratedFiles.SourceControlFiles = false;
				var files = oATLProj.Object.Files;
				var file;
				file = files(strIdlName + "_i.c");
				if (file.CanMove(oGeneratedFiles))
					file.Move(oGeneratedFiles);
				file = files(strIdlName + "_i.h");
				if (file.CanMove(oGeneratedFiles))
					file.Move(oGeneratedFiles);
			}

			oATLProj.Object.Save();
		}
	}
	catch(e)
	{
		if (e.description.length != 0)
			SetErrorInfo(e);
		return e.number
	}
}

function CreateGuids()
{
	strGuid = wizard.CreateGuid();
	strVal = wizard.FormatGuid(strGuid, 0);
	wizard.AddSymbol("LIBID_REGISTRY_FORMAT", strVal);
	strGuid = wizard.CreateGuid();
	strVal = wizard.FormatGuid(strGuid, 0);
	wizard.AddSymbol("CLSID_REGISTRY_FORMAT", strVal);
	strGuid = wizard.CreateGuid();
	strVal = wizard.FormatGuid(strGuid, 0);
	wizard.AddSymbol("APPID_REGISTRY_FORMAT", strVal);

	strGuid = wizard.CreateGuid();
	strVal = wizard.FormatGuid(strGuid, 0);
	wizard.AddSymbol("CLSID_PREVIEW", strVal);
	strGuid = wizard.CreateGuid();
	strVal = wizard.FormatGuid(strGuid, 0);
	wizard.AddSymbol("IID_PREVIEW", strVal);

	strGuid = wizard.CreateGuid();
	strVal = wizard.FormatGuid(strGuid, 0);
	wizard.AddSymbol("CLSID_THUMBNAIL", strVal);
	strGuid = wizard.CreateGuid();
	strVal = wizard.FormatGuid(strGuid, 0);
	wizard.AddSymbol("IID_THUMBNAIL", strVal);

	strGuid = wizard.CreateGuid();
	strVal = wizard.FormatGuid(strGuid, 0);
	wizard.AddSymbol("CLSID_SEARCH", strVal);
	strGuid = wizard.CreateGuid();
	strVal = wizard.FormatGuid(strGuid, 0);
	wizard.AddSymbol("IID_SEARCH", strVal);

	strGuid = wizard.CreateGuid();
	strVal = wizard.FormatGuid(strGuid, 0);
	wizard.AddSymbol("CLSID_PERSISTENT_HANDLER", strVal);
}

function SetFileProperties(projfile, strName)
{
	return false;
}

function CreateClassName(strPrefix, strPostfix)
{
	var strCandidate;
	strCandidate = "C" + strPrefix + strPostfix;
	return strCandidate;
}

function SetATLConfigsToolSettings(oProj, strProjectName)
{
	var oConfigs = oProj.Object.Configurations;
	for (var nCntr = 1; nCntr <= oConfigs.Count; nCntr++)
	{
		var config = oConfigs(nCntr);
		var bDebug = false;
		if (-1 != config.Name.indexOf("Debug"))
		{
			bDebug = true;
		}

		// MIDL settings
		var strIdlName = wizard.FindSymbol("SAFE_IDL_NAME");
		var MidlTool = config.Tools("VCMidlTool");
		MidlTool.MkTypLibCompatible = false;
		MidlTool.TargetEnvironment = midlTargetWin32;

		if (bDebug)
			MidlTool.PreprocessorDefinitions = "_DEBUG";
		else
			MidlTool.PreprocessorDefinitions = "NDEBUG";

		MidlTool.HeaderFileName = strIdlName + "_i.h";
		MidlTool.InterfaceIdentifierFileName = strIdlName + "_i.c";
		MidlTool.ProxyFileName = strIdlName + "_p.c";
		MidlTool.GenerateStublessProxies = true;
		MidlTool.TypeLibraryName = "$(IntDir)" + strIdlName + ".tlb";
		MidlTool.DLLDataFileName = "";

		// no /no_robust
		MidlTool.ValidateParameters = true;

		// Compiler settings
		var CLTool = config.Tools("VCCLCompilerTool");
		CLTool.UsePrecompiledHeader = pchUseUsingSpecific;
		CLTool.WarningLevel = WarningLevel_3;
		if (bDebug)
		{
			CLTool.Optimization = optimizeDisabled;
			CLTool.PreprocessorDefinitions = "WIN32;_WINDOWS;_DEBUG;_USRDLL";
		}
		else
		{
			CLTool.Optimization = optimizeMaxSpeed;
			CLTool.PreprocessorDefinitions = "WIN32;_WINDOWS;NDEBUG;_USRDLL";
		}

		// Resource settings
		var RCTool = config.Tools("VCResourceCompilerTool");
		RCTool.Culture = wizard.FindSymbol("LCID");
		RCTool.AdditionalIncludeDirectories = "$(IntDir)";
		if (bDebug)
			RCTool.PreprocessorDefinitions = "_DEBUG";
		else
			RCTool.PreprocessorDefinitions = "NDEBUG";

		// Linker settings
		var LinkTool = config.Tools("VCLinkerTool");
		LinkTool.SubSystem = subSystemWindows;
		LinkTool.IgnoreImportLibrary = true;

		var strDefFile = ".\\" + strProjectName + ".def";
		LinkTool.ModuleDefinitionFile = strDefFile;

		LinkTool.GenerateDebugInformation = true;
		if (bDebug)
		{
			LinkTool.LinkIncremental = linkIncrementalYes;
		}
		else
		{
			LinkTool.LinkIncremental = linkIncrementalNo;
			LinkTool.EnableCOMDATFolding = optFolding;
			LinkTool.OptimizeReferences = optReferences;
		}

		LinkTool.RegisterOutput = true;

		// Post-build step to set permissions on the DLL (if necessary)
		if (wizard.FindSymbol("SEARCH_HANDLER"))
		{
			var PostBuildTool = config.Tools("VCPostBuildEventTool");
			var L_SettingPermissions_Text = "Setting permissions on handler DLL (to enable load by SearchFilterHost.exe)...";
			PostBuildTool.Description = L_SettingPermissions_Text;
			var L_CommandLine_Text = "cacls.exe \"$(TargetPath)\" /G users:r /E"; // only the "users" group name needs to be localized
			PostBuildTool.CommandLine = L_CommandLine_Text;
		}
	}
}

function SetATLConfigsType(oProj, typeConfig)
{
	var oConfigs = oProj.Object.Configurations;
	for (var nCntr = 1; nCntr <= oConfigs.Count; nCntr++)
	{
		var config = oConfigs(nCntr);
		config.ConfigurationType = typeConfig;
	}
}

function SetATLConfigsATL(oProj)
{
	var oConfigs = oProj.Object.Configurations;
	for (var nCntr = 1; nCntr <= oConfigs.Count; nCntr++)
	{
		var config = oConfigs(nCntr);
		config.UseOfATL = useATLDynamic;
	}
}

function SetATLConfigsMFC(oProj)
{
	var bDynamicMFC = wizard.FindSymbol("DYNAMIC_MFC");
	var oConfigs = oProj.Object.Configurations;
	for (var nCntr = 1; nCntr <= oConfigs.Count; nCntr++)
	{
		var config = oConfigs(nCntr);
		var bDebug = false;
		if (-1 != config.Name.indexOf("Debug"))
		{
			bDebug = true;
		}

		var CLTool = config.Tools("VCCLCompilerTool");

		if (bDynamicMFC)
		{
			config.UseOfMFC = useMfcDynamic;
		}
		else
		{
			config.UseOfMFC = useMfcStatic;
		}
	}
}

var nNumConfigs = 2;

var astrConfigName = new Array();
astrConfigName[0] = "Debug";
astrConfigName[1] = "Release";

function SetFileSettings(proj)
{
	try
	{
		var files = proj.Object.Files;

		var nCntr;
		for (nCntr = 0; nCntr < nNumConfigs; nCntr++)
		{
			var file;
			var strIdlName = wizard.FindSymbol("SAFE_IDL_NAME");

			file = files(strIdlName + "_i.c");
			config = file.FileConfigurations(astrConfigName[nCntr]);
			config.Tool.CompileAsManaged = 0; // no /CLR
			config.Tool.UsePrecompiledHeader = pchNone; // no PCH

			file = files("dllmain.cpp");
			config = file.FileConfigurations(astrConfigName[nCntr]);
			config.Tool.CompileAsManaged = 0; // no /CLR
			config.Tool.UsePrecompiledHeader = pchNone; // no PCH

			file = files("xdlldata.c");
			config = file.FileConfigurations(astrConfigName[nCntr]);
			config.Tool.CompileAsManaged = 0; // no /CLR
			config.Tool.UsePrecompiledHeader = pchNone; // no PCH
		}
	}
	catch (e) {
		throw e;
	}
}

function AddSpecificConfig(proj, strProjectName, strProjectPath)
{
	try
	{
		var bAutomation = wizard.FindSymbol("AUTOMATION");
		var bServer = wizard.FindSymbol("MINI_SERVER");
		var bDynamicMFC = wizard.FindSymbol("DYNAMIC_MFC");

		if (!bServer)
		{
			bServer = wizard.FindSymbol("FULL_SERVER");
			if (!bServer)
				bServer = wizard.FindSymbol("CONTAINER_SERVER");
		}
		var oConfigs = proj.Object.Configurations;
		for (var nCntr = 1; nCntr <= oConfigs.Count; nCntr++)
		{
			var config = oConfigs(nCntr);
			var bDebug = false;
			if (-1 != config.Name.indexOf("Debug"))
				bDebug = true;
			config.CharacterSet = charSetUNICODE;

			var CLTool = config.Tools("VCCLCompilerTool");

			if (bDynamicMFC)
			{
				config.UseOfMFC = useMfcDynamic;
			}
			else
			{
				config.UseOfMFC = useMfcStatic;
			}
			var strDefines = CLTool.PreprocessorDefinitions;
			if (strDefines != "") strDefines += ";";
			strDefines += GetPlatformDefine(config);
			if(bDebug)
			{
				strDefines += "_WINDOWS;_DEBUG";
			}
			else
			{
				strDefines += "_WINDOWS;NDEBUG";
			}
			CLTool.PreprocessorDefinitions= strDefines;

			var LinkTool = config.Tools("VCLinkerTool");
			LinkTool.GenerateDebugInformation = true;

			LinkTool.LinkIncremental = (bDebug ? linkIncrementalYes : linkIncrementalNo);

			var bRibbon = wizard.FindSymbol("RIBBON_TOOLBAR");
			var bTabbedMDI = wizard.FindSymbol("APP_TYPE_TABBED_MDI");
			var bDBSupportHeaderOnly = wizard.FindSymbol("DB_SUPPORT_HEADER_ONLY");

			var bOLEDB = wizard.FindSymbol("OLEDB");
			var bSupportOLEDB = wizard.FindSymbol("DB_SUPPORT_OLEDB");
			if (bOLEDB || (bDBSupportHeaderOnly && bSupportOLEDB))
			{
				LinkTool.AdditionalDependencies = "msdasc.lib";
			}

			var bODBC = wizard.FindSymbol("ODBC");
			var bSupportODBC = wizard.FindSymbol("DB_SUPPORT_ODBC");
			if (bODBC || (bDBSupportHeaderOnly && bSupportODBC))
			{
				LinkTool.AdditionalDependencies = "odbc32.lib";
			}

			var MidlTool = config.Tools("VCMidlTool");
			MidlTool.MkTypLibCompatible = false;
			
			// no /no_robust
			MidlTool.ValidateParameters = true;
			MidlTool.PreprocessorDefinitions = (bDebug ? "_DEBUG" : "NDEBUG");

			if (bAutomation)
			{
				var strIdlName = wizard.FindSymbol("SAFE_IDL_NAME");
				MidlTool.TypeLibraryName = "$(IntDir)" + strIdlName + ".tlb";
			}

			var RCTool = config.Tools("VCResourceCompilerTool");
			RCTool.Culture = wizard.FindSymbol("LCID");
			RCTool.PreprocessorDefinitions = (bDebug ? "_DEBUG" : "NDEBUG");
			RCTool.AdditionalIncludeDirectories = "$(IntDir)";

			if (bServer || bAutomation)
			{
				LinkTool.RegisterOutput = true;
			}
		} //for
	}
	catch(e)
	{
		throw e;
	}
}

function GetTargetName(strName, strProjectName, strResPath, strHelpPath)
{
	try
	{
		var strTarget = strName;
		var strSafeProjHelpFileName = wizard.FindSymbol("SAFE_PROJECT_HELP_FILE_NAME");
		var strRC2FileName = wizard.FindSymbol("RC2_FILE_NAME");
		if (strName.substr(0, 4) == "root")
		{
			if (strName == "root.ico" || strName == "root.manifest")
			{
				strTarget = strResPath + "\\" + strProjectName + strName.substr(4);
			}
			else if (strName == "root.rc2")
			{
				strTarget = strResPath + "\\" + strRC2FileName + strName.substr(4);
			}
			else if (strName == "root.hpj" || strName == "root.cnt")
			{
				strTarget = strProjectName + strName.substr(4);
				strTarget = strHelpPath + "\\" + strTarget;
			}
			else if (strName == "root.hhc" || 
				strName == "root.hhk" || 
				strName == "root.hhp")
			{
				strTarget = strSafeProjHelpFileName + strName.substr(4);
				strTarget = strHelpPath + "\\" + strTarget;
			}
			else if (strName == "root.idl")
			{
				var strProjectName = wizard.FindSymbol("SAFE_IDL_NAME");
				strTarget = strProjectName + ".idl";
			}
			else
				strTarget = strProjectName + strName.substr(4);
			return strTarget;
		}
		if (strName.substr(0, 7) == "dlgroot")
		{
			var strExtension = strName.substr(7);

			if (strName == "dlgroot.cnt")
			{
				strTarget = strHelpPath + "\\" + strProjectName + strExtension;
			}
			else if (strName == "dlgroot.hhc")
			{
				strTarget = strHelpPath + "\\" + strSafeProjHelpFileName + strExtension;
			}
			else
			{
				strTarget = strProjectName + strExtension;
			}

			return strTarget;
		}

		switch (strName)
		{
			case "readme.txt":
				strTarget = "ReadMe.txt";
				break;
			case "all.rc":
			case "dlgall.rc":
				strTarget = wizard.FindSymbol("RC_FILE_NAME");
				break;
			case "dlgres.h":
			case "resource.h":
				strTarget = "Resource.h";
				break;
			case "dialog.h":
				strTarget = wizard.FindSymbol("DIALOG_HEADER");
				break;
			case "dialog.cpp":
				strTarget = wizard.FindSymbol("DIALOG_IMPL");
				break;
			case "dlgproxy.h":
				strTarget = wizard.FindSymbol("DIALOG_AUTO_PROXY_HEADER");
				break;
			case "dlgproxy.cpp":
				strTarget = wizard.FindSymbol("DIALOG_AUTO_PROXY_IMPL");
				break;
			case "frame.h":
				strTarget = wizard.FindSymbol("MAIN_FRAME_HEADER");
				break;
			case "frame.cpp":
				strTarget = wizard.FindSymbol("MAIN_FRAME_IMPL");
				break;
			case "childfrm.h":
				strTarget = wizard.FindSymbol("CHILD_FRAME_HEADER");
				break;
			case "childfrm.cpp":
				strTarget = wizard.FindSymbol("CHILD_FRAME_IMPL");
				break;
			case "doc.h":
				strTarget = wizard.FindSymbol("DOC_HEADER");
				break;
			case "doc.cpp":
				strTarget = wizard.FindSymbol("DOC_IMPL");
				break;
			case "view.h":
				strTarget = wizard.FindSymbol("VIEW_HEADER");
				break;
			case "view.cpp":
				strTarget = wizard.FindSymbol("VIEW_IMPL");
				break;
			case "wndview.h":
				strTarget = wizard.FindSymbol("WND_VIEW_HEADER");
				break;
			case "wndview.cpp":
				strTarget = wizard.FindSymbol("WND_VIEW_IMPL");
				break;
			case "treeview.h":
				strTarget = wizard.FindSymbol("TREE_VIEW_HEADER");
				break;
			case "treeview.cpp":
				strTarget = wizard.FindSymbol("TREE_VIEW_IMPL");
				break;
			case "recset.h":
				strTarget = wizard.FindSymbol("ROWSET_HEADER");
				break;
			case "recset.cpp":
				strTarget = wizard.FindSymbol("ROWSET_IMPL");
				break;
			case "srvritem.h":
				strTarget = wizard.FindSymbol("SERVER_ITEM_HEADER");
				break;
			case "srvritem.cpp":
				strTarget = wizard.FindSymbol("SERVER_ITEM_IMPL");
				break;
			case "ipframe.h":
				strTarget = wizard.FindSymbol("INPLACE_FRAME_HEADER");
				break;
			case "ipframe.cpp":
				strTarget = wizard.FindSymbol("INPLACE_FRAME_IMPL");
				break;
			case "cntritem.h":
				strTarget = wizard.FindSymbol("CONTAINER_ITEM_HEADER");
				break;
			case "cntritem.cpp":
				strTarget = wizard.FindSymbol("CONTAINER_ITEM_IMPL");
				break;
			case "viewtree.h":
				strTarget = "ViewTree.h";
				break;
			case "viewtree.cpp":
				strTarget = "ViewTree.cpp";
				break;
			case "classview.h":
				strTarget = "ClassView.h";
				break;
			case "classview.cpp":
				strTarget = "ClassView.cpp";
				break;
			case "fileview.h":
				strTarget = "FileView.h";
				break;
			case "fileview.cpp":
				strTarget = "FileView.cpp";
				break;
			case "outputwnd.h":
				strTarget = "OutputWnd.h";
				break;
			case "outputwnd.cpp":
				strTarget = "OutputWnd.cpp";
				break;
			case "propertieswnd.h":
				strTarget = "PropertiesWnd.h";
				break;
			case "propertieswnd.cpp":
				strTarget = "PropertiesWnd.cpp";
				break;
			case "calendarbar.h":
				strTarget = "CalendarBar.h";
				break;
			case "calendarbar.cpp":
				strTarget = "CalendarBar.cpp";
				break;
			case "userimages.bmp":
				strTarget = "UserImages.bmp";
				break;
			case "doc.ico":
				strTarget = strResPath + "\\" + strProjectName + "Doc.ico";
				break;
			case "file_view.ico":
			case "file_view_hc.ico":
			case "fileview.bmp":
			case "fileview_hc.bmp":
			case "class_view.ico":
			case "class_view_hc.ico":
			case "classview.bmp":
			case "classview_hc.bmp":
			case "output_wnd.ico":
			case "output_wnd_hc.ico":
			case "properties_wnd.ico":
			case "properties_wnd_hc.ico":
			case "properties.bmp":
			case "properties_hc.bmp":
			case "explorer.bmp":
			case "explorer_hc.bmp":
			case "sort.bmp":
			case "sort_hc.bmp":
			case "nav_large.bmp":
			case "nav_large_hc.bmp":
			case "pages.bmp":
			case "pages_hc.bmp":
			case "pages_small.bmp":
			case "pages_small_hc.bmp":
			case "menuimages.bmp":
			case "menuimages_hc.bmp":
			case "info.bmp":
				strTarget = strResPath + "\\" + strTarget;
				break;
			case "afxdlg.rtf":
				strTarget = strHelpPath + "\\" + strTarget;
				break;
			case "afxdlg.htm":
			case "afxcore.htm":
				strTarget = strHelpPath + "\\" + strTarget;
				break;
			case "HIDD_ROOT_DIALOG.htm":
			case "afxo0001.htm":
			case "afx_hidd_changeicon.htm":
			case "afx_hidd_color.htm":
			case "afx_hidd_convert.htm":
			case "afx_hidd_editlinks.htm":
			case "afx_hidd_fileopen.htm":
			case "afx_hidd_filesave.htm":
			case "afx_hidd_find.htm":
			case "afx_hidd_font.htm":
			case "afx_hidd_insertobject.htm":
			case "afx_hidd_newtypedlg.htm":
			case "afx_hidd_pastespecial.htm":
			case "afx_hidd_print.htm":
			case "afx_hidd_printdlg.htm":
			case "afx_hidd_printsetup.htm":
			case "afx_hidd_replace.htm":
			case "afx_hidp_default.htm":
			case "afx_hidw_dockbar_top.htm":
			case "afx_hidw_preview_bar.htm":
			case "afx_hidw_resize_bar.htm":
			case "afx_hidw_status_bar.htm":
			case "afx_hidw_toolbar.htm":
			case "hidr_doc1type.htm":
			case "hid_app_about.htm":
			case "hid_app_exit.htm":
			case "hid_context_help.htm":
			case "hid_edit_clear.htm":
			case "hid_edit_clear_all.htm":
			case "hid_edit_copy.htm":
			case "hid_edit_cut.htm":
			case "hid_edit_find.htm":
			case "hid_edit_paste.htm":
			case "hid_edit_paste_link.htm":
			case "hid_edit_redo.htm":
			case "hid_edit_repeat.htm":
			case "hid_edit_replace.htm":
			case "hid_edit_undo.htm":
			case "hid_file_close.htm":
			case "hid_file_mru_file1.htm":
			case "hid_file_new.htm":
			case "hid_file_open.htm":
			case "hid_file_page_setup.htm":
			case "hid_file_print.htm":
			case "hid_file_print_preview.htm":
			case "hid_file_print_setup.htm":
			case "hid_file_save.htm":
			case "hid_file_save_as.htm":
			case "hid_file_save_copy_as.htm":
			case "hid_file_send_mail.htm":
			case "hid_file_update.htm":
			case "hid_help_index.htm":
			case "hid_help_using.htm":
			case "hid_ht_caption.htm":
			case "hid_ht_nowhere.htm":
			case "hid_next_pane.htm":
			case "hid_ole_edit_links.htm":
			case "hid_ole_insert_new.htm":
			case "hid_ole_verb_1.htm":
			case "hid_prev_pane.htm":
			case "hid_record_first.htm":
			case "hid_record_last.htm":
			case "hid_record_next.htm":
			case "hid_record_prev.htm":
			case "hid_sc_close.htm":
			case "hid_sc_maximize.htm":
			case "hid_sc_minimize.htm":
			case "hid_sc_move.htm":
			case "hid_sc_nextwindow.htm":
			case "hid_sc_prevwindow.htm":
			case "hid_sc_restore.htm":
			case "hid_sc_size.htm":
			case "hid_sc_tasklist.htm":
			case "hid_view_ruler.htm":
			case "hid_view_status_bar.htm":
			case "hid_view_toolbar.htm":
			case "hid_window_all.htm":
			case "hid_window_arrange.htm":
			case "hid_window_cascade.htm":
			case "hid_window_new.htm":
			case "hid_window_split.htm":
			case "hid_window_tile.htm":
			case "hid_window_tile_horz.htm":
			case "hid_window_tile_vert.htm":
			case "main_index.htm":
			case "menu_edit.htm":
			case "menu_file.htm":
			case "menu_help.htm":
			case "menu_record.htm":
			case "menu_view.htm":
			case "menu_window.htm":
			case "scrollbars.htm":
				strTarget = strHelpPath + "\\" + strTarget;
				break;
			case "handler_stdafx.h":
				strTarget = "stdafx.h";
				break;
			case "handler_stdafx.cpp":
				strTarget = "stdafx.cpp";
				break;
			case "handler_dllmain.h":
				strTarget = "dllmain.h";
				break;
			case "handler_dllmain.cpp":
				strTarget = "dllmain.cpp";
				break;
			case "handler_readme.txt":
				strTarget = "ReadMe.txt";
				break;
			case "handler_resource.h":
				strTarget = "Resource.h";
				break;
			case "handler_root.cpp":
				strTarget = strProjectName + ".cpp";
				break;
			case "handler_root.def":
				strTarget = strProjectName + ".def";
				break;
			case "handler_root.idl":
				strTarget = CreateASCIIName(strProjectName) + ".idl";
				break;
			case "handler_root_i.c":
				strTarget = CreateASCIIName(strProjectName) + "_i.c";
				break;
			case "handler_root_i.h":
				strTarget = CreateASCIIName(strProjectName) + "_i.h";
				break;
			case "handler_root.rc":
				strTarget = strProjectName + ".rc";
				break;
			case "handler_root.rgs":
				strTarget = strProjectName + ".rgs";
				break;
			case "handler_preview.h":
				strTarget = "PreviewHandler.h";
				break;
			case "handler_preview.rgs":
				strTarget = "PreviewHandler.rgs";
				break;
			case "handler_search.h":
				strTarget = "FilterHandler.h";
				break;
			case "handler_search.rgs":
				strTarget = "FilterHandler.rgs";
				break;
			case "handler_thumbnail.h":
				strTarget = "ThumbnailHandler.h";
				break;
			case "handler_thumbnail.rgs":
				strTarget = "ThumbnailHandler.rgs";
				break;
			case "handler_targetver.h":
				strTarget = "targetver.h";
				break;
			case "handler_xdlldata.h":
				strTarget = "xdlldata.h";
				break;
			case "handler_xdlldata.c":
				strTarget = "xdlldata.c";
				break;
			default:
				break;
		}

		var strTemp = GetAdditionalPath(strTarget, strResPath, strHelpPath);


		strTarget = strTemp;
		return strTarget; 
	}
	catch(e)
	{
		throw e;
	}
}

function GetAdditionalPath(strName, strResPath, strHelpPath)
{
	try
	{
		var strFullName = strName;
		switch(strName)
		{
			case "afxcore.rtf":
			case "afxprint.rtf":
			case "afxolecl.rtf":
			case "afxolesv.rtf":
			case "afxdb.rtf":
				strFullName = strHelpPath + "\\" + strName;
				break;
			case "Bullet.gif":
				strFullName = strHelpPath + "\\Images\\" + strName;
				break;
			case "buttons.bmp":
			case "filelarge.bmp":
			case "filesmall.bmp":
			case "main.bmp":
			case "writelarge.bmp":
			case "writesmall.bmp":
			case "ribbon.mfcribbon-ms":
				strFullName = strResPath + "\\" + strName;
				break;
			case "tbdh_.bmp":
			case "tbd__.bmp":
			case "tbrh_.bmp":
			case "tbr__.bmp":
			case "tbah_.bmp":
			case "tba__.bmp":
			case "tbedh.bmp":
			case "tbed_.bmp":
			case "tbeah.bmp":
			case "tbea_.bmp":
			case "tbndm.bmp":
			case "tbndmh.bmp":
			case "tbnds.bmp":
			case "tbndsh.bmp":
				strFullName = strResPath + "\\Toolbar.bmp";
				break;
			case "tbah_256.bmp":
			case "tba__256.bmp":
			case "tbdh_256.bmp":
			case "tbd__256.bmp":
			case "tbeah_256.bmp":
			case "tbea_256.bmp":
			case "tbedh_256.bmp":
			case "tbed_256.bmp":
			case "tbrh_256.bmp":
			case "tbr__256.bmp":
			case "tbndmh256.bmp":
			case "tbndm256.bmp":
			case "tbndsh256.bmp":
			case "tbnds256.bmp":
				strFullName = strResPath + "\\Toolbar256.bmp";
				break;
			case "tba_i.bmp":
			case "tbrhi.bmp":
			case "tbr_i.bmp":
			case "tbahi.bmp":
				strFullName = strResPath + "\\IToolbar.bmp";
				break;
			case "tba_i256.bmp":
			case "tbrhi256.bmp":
			case "tbr_i256.bmp":
			case "tbahi256.bmp":
				strFullName = strResPath + "\\IToolbar256.bmp";
				break;
			case "bullet.bmp":
				strFullName = "";  // Help image files - will be added to the project in AddHelpImages
				break;
			default:
				break;
		}
		return strFullName;
	}
	catch(e)
	{
		throw e;
	}
}

function AddHelpBuildSteps(projObj, strProjectName, bHTMLHelp)
{
	try
	{
		var fileExt;
		var outFileExt;
		var fileTool1 = "";
		var fileTool2 = "";
		var extraMakehmSwitch = "";
		var extraMakehmSwitch2 = "";
		var strCodeTool1 = new Array();
		var strCodeTool2 = new Array();
		var strHmFileName;
		var strHmFileNameNoQuotes;
		var strHmHeaderComment;
		var strSafeProjHelpFileName = wizard.FindSymbol("SAFE_PROJECT_HELP_FILE_NAME");

		if(bHTMLHelp)
		{
			fileExt = ".hhp";
			outFileExt = ".chm";
			strHmFileName = "\"hlp\\HTMLDefines.h\"";
			strHmFileNameNoQuotes = "hlp\\HTMLDefines.h";
			extraMakehmSwitch = "/h ";
			extraMakehmSwitch2 = "/a afxhh.h "
			var L_strCodeFragment1_Text = "Generated Help Map file.  Used by ";
			strHmHeaderComment = L_strCodeFragment1_Text + strSafeProjHelpFileName + ".HHP.";

			// put together the command line to apply to the .hhp file
			strCodeTool1[0] = "start /wait hhc \"hlp\\" + strSafeProjHelpFileName + ".hhp\"";
			strCodeTool1[1] = "if not exist \"hlp\\" + strSafeProjHelpFileName + ".chm\" goto :HelpError";
			strCodeTool1[2] = "copy \"hlp\\" + strSafeProjHelpFileName + ".chm\" \"$(OutDir)" + strProjectName + ".chm\"";
			strCodeTool1[3] = "goto :HelpDone";
			strCodeTool1[4] = ":HelpError";
			var strLine1 = "echo hlp\\" + strSafeProjHelpFileName + ".hhp(1) : error:";
			var L_strCodeFragment2_Text = "Problem encountered creating help file";
			strLine1 += L_strCodeFragment2_Text;
			strCodeTool1[5] = strLine1;
			strCodeTool1[6] = "echo.";
			strCodeTool1[7] = ":HelpDone";
			strCodeTool1[8] = "echo.";
			var idx;
			for (idx=0; idx<9; idx++)
				fileTool1 += strCodeTool1[idx] + "\r\n";
		}
		else
		{
			fileExt = ".hpj";
			outFileExt = ".hlp";
			strHmFileName = "\"hlp\\$(ProjectName).hm\"";
			strHmFileNameNoQuotes = "hlp\\$(ProjectName).hm";
			var L_strCodeFragment3_Text = "Generated Help Map file.  Used by $(ProjectName).hpj.";
			strHmHeaderComment = L_strCodeFragment3_Text;

			// put together the command line to apply to the .hpj file
			strCodeTool1[0]  = "start /wait hcw /C /E /M \"hlp\\$(ProjectName).hpj\"";
			strCodeTool1[1]  = "if errorlevel 1 goto :HelpError";
			strCodeTool1[2]  = "if not exist \"hlp\\$(ProjectName).hlp\" goto :HelpError";
			strCodeTool1[3]  = "if not exist \"hlp\\$(ProjectName).cnt\" goto :HelpError";
			strCodeTool1[4]  = "echo.";
			strCodeTool1[5]  = "copy \"hlp\\$(ProjectName).hlp\" \"$(OutDir)\"";
			strCodeTool1[6]  = "copy \"hlp\\$(ProjectName).cnt\" \"$(OutDir)\"";
			strCodeTool1[7]  = "goto :HelpDone";
			strCodeTool1[8]  = ":HelpError";
			var strLine1 = "echo hlp\\$(ProjectName).hpj(1) : error: ";
			var L_strWinCodeToolFragment1_Text = "Problem encountered creating help file";
			strLine1 += L_strWinCodeToolFragment1_Text;
			strCodeTool1[9]  = strLine1;
			strCodeTool1[10] = ":HelpDone";
			strCodeTool1[11] = "echo.";
			var idx;
			for (idx=0; idx<12; idx++)
				fileTool1 += strCodeTool1[idx] + "\r\n";
		}

		// put together the command line to apply to the resource.h file
		var strLine = "echo // ";
		strLine += strHmHeaderComment;
		strLine += " > " + strHmFileName;
		strCodeTool2[0]  = strLine;
		strCodeTool2[1]  = "echo. > " + strHmFileName;
		strLine = "echo // ";
		var L_strCodeFragment4_Text = "Commands (ID_* and IDM_*)";
		strLine += L_strCodeFragment4_Text;
		strLine += " >> " + strHmFileName;
		strCodeTool2[2]  = strLine;
		strCodeTool2[3]  = "makehm " + extraMakehmSwitch + "ID_,HID_,0x10000 IDM_,HIDM_,0x10000 \"%(FullPath)\" >> " + strHmFileName;
		strCodeTool2[4]  = "echo. >> " + strHmFileName;
		strLine = "echo // ";
		var L_strCodeFragment5_Text = "Prompts (IDP_*)";
		strLine += L_strCodeFragment5_Text;
		strLine += " >> " + strHmFileName;
		strCodeTool2[5]  = strLine;
		strCodeTool2[6]  = "makehm " + extraMakehmSwitch + "IDP_,HIDP_,0x30000 \"%(FullPath)\" >> " + strHmFileName;
		strCodeTool2[7]  = "echo. >> " + strHmFileName;
		strLine = "echo // ";
		var L_strCodeFragment6_Text = "Resources (IDR_*)";
		strLine += L_strCodeFragment6_Text;
		strLine += " >> " + strHmFileName;
		strCodeTool2[8]  = strLine;
		strCodeTool2[9]  = "makehm " + extraMakehmSwitch + "IDR_,HIDR_,0x20000 \"%(FullPath)\" >> " + strHmFileName;
		strCodeTool2[10] = "echo. >> " + strHmFileName;
		strLine = "echo // ";
		var L_strCodeFragment7_Text = "Dialogs (IDD_*)";
		strLine += L_strCodeFragment7_Text;
		strLine += " >> " + strHmFileName;
		strCodeTool2[11] = strLine;
		strCodeTool2[12] = "makehm " + extraMakehmSwitch + "IDD_,HIDD_,0x20000 \"%(FullPath)\" >> " + strHmFileName;
		strCodeTool2[13] = "echo. >> " + strHmFileName;
		strLine = "echo // ";
		var L_strCodeFragment8_Text = "Frame Controls (IDW_*)";
		strLine += L_strCodeFragment8_Text;
		strLine += " >> " + strHmFileName;
		strCodeTool2[14] = strLine;
		strCodeTool2[15] = "makehm " + extraMakehmSwitch + extraMakehmSwitch2 + "IDW_,HIDW_,0x50000 \"%(FullPath)\" >> " + strHmFileName;
		for (idx=0; idx<16; idx++)
			fileTool2 += strCodeTool2[idx] + "\r\n";

		var oPch = projObj.Files("StdAfx.cpp");
		if (oPch != null)
		{
			for (var n=1; n<=oPch.FileConfigurations.Count; n++)
			{
				var config = oPch.FileConfigurations.Item(n)
				config.Tool.UsePrecompiledHeader = pchCreateUsingSpecific;
			}
		}
		
		var fileObj1;
		if(bHTMLHelp)
		{
			fileObj1 = projObj.Files(strSafeProjHelpFileName + fileExt);
		}
		else
		{
			fileObj1 = projObj.Files(strProjectName + fileExt);
		}
		var fileObj2 = projObj.Files("resource.h");
			
		if (fileObj1 != null)
		{
			for (var i = 1; i <= fileObj1.FileConfigurations.Count; i++)
			{
				var config = fileObj1.FileConfigurations.Item(i);
				if (config != null)
				{
					var CustomBuildTool = config.Tool;
					if (CustomBuildTool != null)
					{
						CustomBuildTool.Outputs = "$(OutDir)" + strProjectName + outFileExt;
						CustomBuildTool.AdditionalDependencies = strHmFileNameNoQuotes;
						CustomBuildTool.CommandLine = fileTool1;
						L_ToolDesc1_Text = "Making help file...";
						CustomBuildTool.Description = L_ToolDesc1_Text;
					}
				}
			}
		}

		if (fileObj2 != null)
		{
			for (var i = 1; i <= fileObj2.FileConfigurations.Count; i++)
			{
				var config = fileObj2.FileConfigurations.Item(i);
				if (config != null)
				{
					var CustomBuildTool = config.Tool;
					if (CustomBuildTool != null)
					{
						CustomBuildTool.Outputs = strHmFileNameNoQuotes;
						CustomBuildTool.CommandLine = fileTool2;
						L_ToolDesc2_Text = "Generating map file for help compiler...";
						CustomBuildTool.Description = L_ToolDesc2_Text;
					}
				}
			}
		}
	}
	catch(e)
	{
		throw e;
	}
}

function AddHelpImages(selProj, L_strHelpFiles_Text)
{
	try
	{
		var oHelpFolder = selProj.Object.Filters.Item(L_strHelpFiles_Text);
		strHelpPath = wizard.FindSymbol("HELP_PATH");
		oHelpFolder.AddFile(strHelpPath + "\\Images\\bullet.bmp");
	}
	catch(e)
	{
		throw e;
	}
}

// SIG // Begin signature block
// SIG // MIIXOgYJKoZIhvcNAQcCoIIXKzCCFycCAQExCzAJBgUr
// SIG // DgMCGgUAMGcGCisGAQQBgjcCAQSgWTBXMDIGCisGAQQB
// SIG // gjcCAR4wJAIBAQQQEODJBs441BGiowAQS9NQkAIBAAIB
// SIG // AAIBAAIBAAIBADAhMAkGBSsOAwIaBQAEFAzUyJrrzIrT
// SIG // TXf4CiejaYn9Kv+QoIISMTCCBGAwggNMoAMCAQICCi6r
// SIG // EdxQ/1ydy8AwCQYFKw4DAh0FADBwMSswKQYDVQQLEyJD
// SIG // b3B5cmlnaHQgKGMpIDE5OTcgTWljcm9zb2Z0IENvcnAu
// SIG // MR4wHAYDVQQLExVNaWNyb3NvZnQgQ29ycG9yYXRpb24x
// SIG // ITAfBgNVBAMTGE1pY3Jvc29mdCBSb290IEF1dGhvcml0
// SIG // eTAeFw0wNzA4MjIyMjMxMDJaFw0xMjA4MjUwNzAwMDBa
// SIG // MHkxCzAJBgNVBAYTAlVTMRMwEQYDVQQIEwpXYXNoaW5n
// SIG // dG9uMRAwDgYDVQQHEwdSZWRtb25kMR4wHAYDVQQKExVN
// SIG // aWNyb3NvZnQgQ29ycG9yYXRpb24xIzAhBgNVBAMTGk1p
// SIG // Y3Jvc29mdCBDb2RlIFNpZ25pbmcgUENBMIIBIjANBgkq
// SIG // hkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAt3l91l2zRTmo
// SIG // NKwx2vklNUl3wPsfnsdFce/RRujUjMNrTFJi9JkCw03Y
// SIG // SWwvJD5lv84jtwtIt3913UW9qo8OUMUlK/Kg5w0jH9FB
// SIG // JPpimc8ZRaWTSh+ZzbMvIsNKLXxv2RUeO4w5EDndvSn0
// SIG // ZjstATL//idIprVsAYec+7qyY3+C+VyggYSFjrDyuJSj
// SIG // zzimUIUXJ4dO3TD2AD30xvk9gb6G7Ww5py409rQurwp9
// SIG // YpF4ZpyYcw2Gr/LE8yC5TxKNY8ss2TJFGe67SpY7UFMY
// SIG // zmZReaqth8hWPp+CUIhuBbE1wXskvVJmPZlOzCt+M26E
// SIG // RwbRntBKhgJuhgCkwIffUwIDAQABo4H6MIH3MBMGA1Ud
// SIG // JQQMMAoGCCsGAQUFBwMDMIGiBgNVHQEEgZowgZeAEFvQ
// SIG // cO9pcp4jUX4Usk2O/8uhcjBwMSswKQYDVQQLEyJDb3B5
// SIG // cmlnaHQgKGMpIDE5OTcgTWljcm9zb2Z0IENvcnAuMR4w
// SIG // HAYDVQQLExVNaWNyb3NvZnQgQ29ycG9yYXRpb24xITAf
// SIG // BgNVBAMTGE1pY3Jvc29mdCBSb290IEF1dGhvcml0eYIP
// SIG // AMEAizw8iBHRPvZj7N9AMA8GA1UdEwEB/wQFMAMBAf8w
// SIG // HQYDVR0OBBYEFMwdznYAcFuv8drETppRRC6jRGPwMAsG
// SIG // A1UdDwQEAwIBhjAJBgUrDgMCHQUAA4IBAQB7q65+Siby
// SIG // zrxOdKJYJ3QqdbOG/atMlHgATenK6xjcacUOonzzAkPG
// SIG // yofM+FPMwp+9Vm/wY0SpRADulsia1Ry4C58ZDZTX2h6t
// SIG // KX3v7aZzrI/eOY49mGq8OG3SiK8j/d/p1mkJkYi9/uEA
// SIG // uzTz93z5EBIuBesplpNCayhxtziP4AcNyV1ozb2AQWtm
// SIG // qLu3u440yvIDEHx69dLgQt97/uHhrP7239UNs3DWkuNP
// SIG // tjiifC3UPds0C2I3Ap+BaiOJ9lxjj7BauznXYIxVhBoz
// SIG // 9TuYoIIMol+Lsyy3oaXLq9ogtr8wGYUgFA0qvFL0QeBe
// SIG // MOOSKGmHwXDi86erzoBCcnYOMIIEejCCA2KgAwIBAgIK
// SIG // YQHPPgAAAAAADzANBgkqhkiG9w0BAQUFADB5MQswCQYD
// SIG // VQQGEwJVUzETMBEGA1UECBMKV2FzaGluZ3RvbjEQMA4G
// SIG // A1UEBxMHUmVkbW9uZDEeMBwGA1UEChMVTWljcm9zb2Z0
// SIG // IENvcnBvcmF0aW9uMSMwIQYDVQQDExpNaWNyb3NvZnQg
// SIG // Q29kZSBTaWduaW5nIFBDQTAeFw0wOTEyMDcyMjQwMjla
// SIG // Fw0xMTAzMDcyMjQwMjlaMIGDMQswCQYDVQQGEwJVUzET
// SIG // MBEGA1UECBMKV2FzaGluZ3RvbjEQMA4GA1UEBxMHUmVk
// SIG // bW9uZDEeMBwGA1UEChMVTWljcm9zb2Z0IENvcnBvcmF0
// SIG // aW9uMQ0wCwYDVQQLEwRNT1BSMR4wHAYDVQQDExVNaWNy
// SIG // b3NvZnQgQ29ycG9yYXRpb24wggEiMA0GCSqGSIb3DQEB
// SIG // AQUAA4IBDwAwggEKAoIBAQC9MIn7RXKoU2ueiU8AI8C+
// SIG // 1B09sVlAOPNzkYIm5pYSAFPZHIIOPM4du733Qo2X1Pw4
// SIG // GuS5+ePs02EDv6DT1nVNXEap7V7w0uJpWxpz6rMcjQTN
// SIG // KUSgZFkvHphdbserGDmCZcSnvKt1iBnqh5cUJrN/Jnak
// SIG // 1Dg5hOOzJtUY+Svp0skWWlQh8peNh4Yp/vRJLOaL+AQ/
// SIG // fc3NlpKGDXED4tD+DEI1/9e4P92ORQp99tdLrVvwdnId
// SIG // dyN9iTXEHF2yUANLR20Hp1WImAaApoGtVE7Ygdb6v0LA
// SIG // Mb5VDZnVU0kSMOvlpYh8XsR6WhSHCLQ3aaDrMiSMCOv5
// SIG // 1BS64PzN6qQVAgMBAAGjgfgwgfUwEwYDVR0lBAwwCgYI
// SIG // KwYBBQUHAwMwHQYDVR0OBBYEFDh4BXPIGzKbX5KGVa+J
// SIG // usaZsXSOMA4GA1UdDwEB/wQEAwIHgDAfBgNVHSMEGDAW
// SIG // gBTMHc52AHBbr/HaxE6aUUQuo0Rj8DBEBgNVHR8EPTA7
// SIG // MDmgN6A1hjNodHRwOi8vY3JsLm1pY3Jvc29mdC5jb20v
// SIG // cGtpL2NybC9wcm9kdWN0cy9DU1BDQS5jcmwwSAYIKwYB
// SIG // BQUHAQEEPDA6MDgGCCsGAQUFBzAChixodHRwOi8vd3d3
// SIG // Lm1pY3Jvc29mdC5jb20vcGtpL2NlcnRzL0NTUENBLmNy
// SIG // dDANBgkqhkiG9w0BAQUFAAOCAQEAKAODqxMN8f4Rb0J2
// SIG // 2EOruMZC+iRlNK51sHEwjpa2g/py5P7NN+c6cJhRIA66
// SIG // cbTJ9NXkiugocHPV7eHCe+7xVjRagILrENdyA+oSTuzd
// SIG // DYx7RE8MYXX9bpwH3c4rWhgNObBg/dr/BKoCo9j6jqO7
// SIG // vcFqVDsxX+QsbsvxTSoc8h52e4avxofWsSrtrMwOwOSf
// SIG // f+jP6IRyVIIYbirInpW0Gh7Bb5PbYqbBS2utye09kuOy
// SIG // L6t6dzlnagB7gp0DEN5jlUkmQt6VIsGHC9AUo1/cczJy
// SIG // Nh7/yCnFJFJPZkjJHR2pxSY5aVBOp+zCBmwuchvxIdpt
// SIG // JEiAgRVAfJ/MdDhKTzCCBJ0wggOFoAMCAQICEGoLmU/A
// SIG // ACWrEdtFH1h6Z6IwDQYJKoZIhvcNAQEFBQAwcDErMCkG
// SIG // A1UECxMiQ29weXJpZ2h0IChjKSAxOTk3IE1pY3Jvc29m
// SIG // dCBDb3JwLjEeMBwGA1UECxMVTWljcm9zb2Z0IENvcnBv
// SIG // cmF0aW9uMSEwHwYDVQQDExhNaWNyb3NvZnQgUm9vdCBB
// SIG // dXRob3JpdHkwHhcNMDYwOTE2MDEwNDQ3WhcNMTkwOTE1
// SIG // MDcwMDAwWjB5MQswCQYDVQQGEwJVUzETMBEGA1UECBMK
// SIG // V2FzaGluZ3RvbjEQMA4GA1UEBxMHUmVkbW9uZDEeMBwG
// SIG // A1UEChMVTWljcm9zb2Z0IENvcnBvcmF0aW9uMSMwIQYD
// SIG // VQQDExpNaWNyb3NvZnQgVGltZXN0YW1waW5nIFBDQTCC
// SIG // ASIwDQYJKoZIhvcNAQEBBQADggEPADCCAQoCggEBANw3
// SIG // bvuvyEJKcRjIzkg+U8D6qxS6LDK7Ek9SyIPtPjPZSTGS
// SIG // KLaRZOAfUIS6wkvRfwX473W+i8eo1a5pcGZ4J2botrfv
// SIG // hbnN7qr9EqQLWSIpL89A2VYEG3a1bWRtSlTb3fHev5+D
// SIG // x4Dff0wCN5T1wJ4IVh5oR83ZwHZcL322JQS0VltqHGP/
// SIG // gHw87tUEJU05d3QHXcJc2IY3LHXJDuoeOQl8dv6dbG56
// SIG // 4Ow+j5eecQ5fKk8YYmAyntKDTisiXGhFi94vhBBQsvm1
// SIG // Go1s7iWbE/jLENeFDvSCdnM2xpV6osxgBuwFsIYzt/iU
// SIG // W4RBhFiFlG6wHyxIzG+cQ+Bq6H8mjmsCAwEAAaOCASgw
// SIG // ggEkMBMGA1UdJQQMMAoGCCsGAQUFBwMIMIGiBgNVHQEE
// SIG // gZowgZeAEFvQcO9pcp4jUX4Usk2O/8uhcjBwMSswKQYD
// SIG // VQQLEyJDb3B5cmlnaHQgKGMpIDE5OTcgTWljcm9zb2Z0
// SIG // IENvcnAuMR4wHAYDVQQLExVNaWNyb3NvZnQgQ29ycG9y
// SIG // YXRpb24xITAfBgNVBAMTGE1pY3Jvc29mdCBSb290IEF1
// SIG // dGhvcml0eYIPAMEAizw8iBHRPvZj7N9AMBAGCSsGAQQB
// SIG // gjcVAQQDAgEAMB0GA1UdDgQWBBRv6E4/l7k0q0uGj7yc
// SIG // 6qw7QUPG0DAZBgkrBgEEAYI3FAIEDB4KAFMAdQBiAEMA
// SIG // QTALBgNVHQ8EBAMCAYYwDwYDVR0TAQH/BAUwAwEB/zAN
// SIG // BgkqhkiG9w0BAQUFAAOCAQEAlE0RMcJ8ULsRjqFhBwEO
// SIG // jHBFje9zVL0/CQUt/7hRU4Uc7TmRt6NWC96Mtjsb0fus
// SIG // p8m3sVEhG28IaX5rA6IiRu1stG18IrhG04TzjQ++B4o2
// SIG // wet+6XBdRZ+S0szO3Y7A4b8qzXzsya4y1Ye5y2PENtEY
// SIG // Ib923juasxtzniGI2LS0ElSM9JzCZUqaKCacYIoPO8cT
// SIG // ZXhIu8+tgzpPsGJY3jDp6Tkd44ny2jmB+RMhjGSAYwYE
// SIG // lvKaAkMve0aIuv8C2WX5St7aA3STswVuDMyd3ChhfEjx
// SIG // F5wRITgCHIesBsWWMrjlQMZTPb2pid7oZjeN9CKWnMyw
// SIG // d1RROtZyRLIj9jCCBKowggOSoAMCAQICCmEGlC0AAAAA
// SIG // AAkwDQYJKoZIhvcNAQEFBQAweTELMAkGA1UEBhMCVVMx
// SIG // EzARBgNVBAgTCldhc2hpbmd0b24xEDAOBgNVBAcTB1Jl
// SIG // ZG1vbmQxHjAcBgNVBAoTFU1pY3Jvc29mdCBDb3Jwb3Jh
// SIG // dGlvbjEjMCEGA1UEAxMaTWljcm9zb2Z0IFRpbWVzdGFt
// SIG // cGluZyBQQ0EwHhcNMDgwNzI1MTkwMjE3WhcNMTMwNzI1
// SIG // MTkxMjE3WjCBszELMAkGA1UEBhMCVVMxEzARBgNVBAgT
// SIG // Cldhc2hpbmd0b24xEDAOBgNVBAcTB1JlZG1vbmQxHjAc
// SIG // BgNVBAoTFU1pY3Jvc29mdCBDb3Jwb3JhdGlvbjENMAsG
// SIG // A1UECxMETU9QUjEnMCUGA1UECxMebkNpcGhlciBEU0Ug
// SIG // RVNOOjdBODItNjg4QS05RjkyMSUwIwYDVQQDExxNaWNy
// SIG // b3NvZnQgVGltZS1TdGFtcCBTZXJ2aWNlMIIBIjANBgkq
// SIG // hkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAlYEKIEIYUXrZ
// SIG // le2b/dyH0fsOjxPqqjcoEnb+TVCrdpcqk0fgqVZpAuWU
// SIG // fk2F239x73UA27tDbPtvrHHwK9F8ks6UF52hxbr5937d
// SIG // YeEtMB6cJi12P+ZGlo6u2Ik32Mzv889bw/xo4PJkj5vo
// SIG // wxL5o76E/NaLzgU9vQF2UCcD+IS3FoaNYL5dKSw8z6X9
// SIG // mFo1HU8WwDjYHmE/PTazVhQVd5U7EPoAsJPiXTerJ7tj
// SIG // LEgUgVXjbOqpK5WNiA5+owCldyQHmCpwA7gqJJCa3sWi
// SIG // Iku/TFkGd1RyQ7A+ZN2ThAhYtv7ph0kJNrOz+DOpfkyi
// SIG // eX8yWSkOnrX14DyeP+xGOwIDAQABo4H4MIH1MB0GA1Ud
// SIG // DgQWBBQolYi/Ajvr2pS6fUYP+sv0fp3/0TAfBgNVHSME
// SIG // GDAWgBRv6E4/l7k0q0uGj7yc6qw7QUPG0DBEBgNVHR8E
// SIG // PTA7MDmgN6A1hjNodHRwOi8vY3JsLm1pY3Jvc29mdC5j
// SIG // b20vcGtpL2NybC9wcm9kdWN0cy90c3BjYS5jcmwwSAYI
// SIG // KwYBBQUHAQEEPDA6MDgGCCsGAQUFBzAChixodHRwOi8v
// SIG // d3d3Lm1pY3Jvc29mdC5jb20vcGtpL2NlcnRzL3RzcGNh
// SIG // LmNydDATBgNVHSUEDDAKBggrBgEFBQcDCDAOBgNVHQ8B
// SIG // Af8EBAMCBsAwDQYJKoZIhvcNAQEFBQADggEBAADurPzi
// SIG // 0ohmyinjWrnNAIJ+F1zFJFkSu6j3a9eH/o3LtXYfGyL2
// SIG // 9+HKtLlBARo3rUg3lnD6zDOnKIy4C7Z0Eyi3s3XhKgni
// SIG // i0/fmD+XtzQSgeoQ3R3cumTPTlA7TIr9Gd0lrtWWh+pL
// SIG // xOXw+UEXXQHrV4h9dnrlb/6HIKyTnIyav18aoBUwJOCi
// SIG // fmGRHSkpw0mQOkODie7e1YPdTyw1O+dBQQGqAAwL8tZJ
// SIG // G85CjXuw8y2NXSnhvo1/kRV2tGD7FCeqbxJjQihYOoo7
// SIG // i0Dkt8XMklccRlZrj8uSTVYFAMr4MEBFTt8ZiL31EPDd
// SIG // Gt8oHrRR8nfgJuO7CYES3B460EUxggR1MIIEcQIBATCB
// SIG // hzB5MQswCQYDVQQGEwJVUzETMBEGA1UECBMKV2FzaGlu
// SIG // Z3RvbjEQMA4GA1UEBxMHUmVkbW9uZDEeMBwGA1UEChMV
// SIG // TWljcm9zb2Z0IENvcnBvcmF0aW9uMSMwIQYDVQQDExpN
// SIG // aWNyb3NvZnQgQ29kZSBTaWduaW5nIFBDQQIKYQHPPgAA
// SIG // AAAADzAJBgUrDgMCGgUAoIGgMBkGCSqGSIb3DQEJAzEM
// SIG // BgorBgEEAYI3AgEEMBwGCisGAQQBgjcCAQsxDjAMBgor
// SIG // BgEEAYI3AgEVMCMGCSqGSIb3DQEJBDEWBBQpxS4vCSPK
// SIG // dZBDL5ygktTLl8OXIDBABgorBgEEAYI3AgEMMTIwMKAW
// SIG // gBQAZABlAGYAYQB1AGwAdAAuAGoAc6EWgBRodHRwOi8v
// SIG // bWljcm9zb2Z0LmNvbTANBgkqhkiG9w0BAQEFAASCAQC6
// SIG // xf3v9HYQ+oubgnsUYTUOd6x8lNy5XgFgEdBzSju4PvVV
// SIG // QdWphJ9f7hn/52UvRmh6VEMsFVLwXP46RV/ztHxOcV2X
// SIG // H6vi8prJCjLL79bifdMdZbrj8GaWcOJdkR1l+rA6mbLf
// SIG // +EnWKctZb8oeQjOP0qQ16wZYYbDbLKUBR2ywhKHRReu6
// SIG // HtqPtqyDuzKIpCaSILamCTeaPQ8eyx/hXj8gkdPlid5o
// SIG // eFOyPU7yna3sHNNGzcMMLuEmw69tQMSMl5TfVQk7C9Rv
// SIG // f1RtVr3hx0OQXYRXs4YmrP0lwxVHG4Yqp51QNPSkhqRV
// SIG // LpLPtQtJUZlimBCTKIGYrQEptvOzdHm8oYICHzCCAhsG
// SIG // CSqGSIb3DQEJBjGCAgwwggIIAgEBMIGHMHkxCzAJBgNV
// SIG // BAYTAlVTMRMwEQYDVQQIEwpXYXNoaW5ndG9uMRAwDgYD
// SIG // VQQHEwdSZWRtb25kMR4wHAYDVQQKExVNaWNyb3NvZnQg
// SIG // Q29ycG9yYXRpb24xIzAhBgNVBAMTGk1pY3Jvc29mdCBU
// SIG // aW1lc3RhbXBpbmcgUENBAgphBpQtAAAAAAAJMAcGBSsO
// SIG // AwIaoF0wGAYJKoZIhvcNAQkDMQsGCSqGSIb3DQEHATAc
// SIG // BgkqhkiG9w0BCQUxDxcNMTAwMzE5MTMwNDM5WjAjBgkq
// SIG // hkiG9w0BCQQxFgQUaDIdhCA3eBgVUe0Ia85WeasYSgww
// SIG // DQYJKoZIhvcNAQEFBQAEggEAbJMysd5DedTn9niP45Vz
// SIG // wAua8js5MyqqFzjBszpV/Wmt+Vp2I3NbKL13D/ERQ0SE
// SIG // 5INsIvjARllEfQl41bod4kBZbETkqHkCVyDodrgRCeLE
// SIG // hVB7PyePBFjNml2KwhGOwaKvUIxlSrrY+acMVUyvwSao
// SIG // BMzGYyqoEUlNZBXP+0e55fnabZoNGSAE9TIU3aIJ7c3o
// SIG // T4zxU5/z1eq2p4G3KrpY0iuShW6nWhHvW+8vf6MpJYXf
// SIG // e6CHeKDbOlJdbI4+FPiN6tKZdVCmkHW0m9RuHPIrydBw
// SIG // 2H5pmGteAY0fxfRgrT9c7Klhd60AeCOjdZXQyNH45UbP
// SIG // 0S8bRbYVw4gLKg==
// SIG // End signature block
