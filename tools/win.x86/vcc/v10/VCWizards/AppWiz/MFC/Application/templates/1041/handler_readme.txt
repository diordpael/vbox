﻿========================================================================
 ATL (ACTIVE TEMPLATE LIBRARY): [!output PROJECT_NAME] プロジェクトの概要
========================================================================

この [!output PROJECT_NAME] プロジェクトは、ユーザーがダイナミック 
リンク ライブラリ (DLL) を作成するための開始点として使用するために、
AppWizard によって作成されました。

このファイルには、プロジェクトを構成する各ファイルの内容の概略が
記述されています。

[!output PROJECT_NAME].vcxproj
    これは、アプリケーション ウィザードを使用して生成された VC++ 
    プロジェクトのメイン プロジェクト ファイルです。
    このファイルには、ファイルを生成した Visual C++ 
    のバージョンに関する情報と、アプリケーション ウィザードで
    選択されたプラットフォーム、構成、およびプロジェクト機能に関する情報が
    含まれています。

[!output PROJECT_NAME].vcxproj.filters
    これは、アプリケーション ウィザードで生成された VC++ プロジェクトのフィルター 
    ファイルです。
    このファイルには、プロジェクト内のファイルとフィルターとの間の関連付けに関する
    情報が含まれています。この関連付けは、特定のノードで同様の拡張子を持つファイルのグルー
プ化を
    示すために IDE で使用されます (たとえば、".cpp" ファイルは "ソース ファイル" 
    フィルターに関連付けられています)。

[!output SAFE_IDL_NAME].idl
    このファイルには、プロジェクトで定義されるタイプ ライブラリの IDL 定義、
    インターフェイス、およびコクラスが含まれます。
    このファイルは MIDL コンパイラによって処理され、次のものが生成されます。
        C++ インターフェイス定義および GUID 宣言([!output SAFE_IDL_NAME].h)
        GUID 定義([!output SAFE_IDL_NAME]_i.c)
        タイプ ライブラリ([!output SAFE_IDL_NAME].tlb)
        マーシャリング コード([!output SAFE_IDL_NAME]_p.c およびdlldata.c)

[!output SAFE_IDL_NAME].h
    このファイルには、C++ のインターフェイス定義および 
    [!output SAFE_IDL_NAME].idl で定義される項目の GUID 
    宣言が含まれます。コンパイル中に MIDL によって再生成されます。

[!output PROJECT_NAME].cpp
    このファイルには、オブジェクト マップおよび DLL 
    のエクスポートの実装が含まれます。

[!output PROJECT_NAME].rc
    これは、プログラムが使用するすべての Microsoft Windows リソースの
    一覧です。

[!if DLL_APP]
[!output PROJECT_NAME].def
    このモジュール定義ファイルは、DLL に必要なエクスポートに関する情報をリンカーに
    提供します。次のエクスポート情報が含まれています。
        DllGetClassObject
        DllCanUnloadNow
        DllRegisterServer
        DllUnregisterServer
        DllInstall
[!endif]

/////////////////////////////////////////////////////////////////////////////
その他の標準ファイル :

StdAfx.h, StdAfx.cpp
    これらのファイルは、プリコンパイル済みヘッダー (PCH) ファイル 
    [!output PROJECT_NAME].pch とプリコンパイル済み型ファイル StdAfx.obj 
    をビルドするために使用されます。

Resource.h
    これは、リソース ID を定義する標準のヘッダー ファイルです。

/////////////////////////////////////////////////////////////////////////////
その他のメモ :

	MFC サポート オプションにより、MFC (Microsoft Foundation Class) 
        ライブラリがスケルトン アプリケーションにビルドされます。
	MFC のクラス、オブジェクト、および関数を使用できます。
/////////////////////////////////////////////////////////////////////////////
