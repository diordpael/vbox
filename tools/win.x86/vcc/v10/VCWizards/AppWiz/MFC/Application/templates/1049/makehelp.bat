@echo off
[!if HM_NOTE]

REM -- �������� �������� �� �������� ������� �������� OEM � ANSI
REM -- ������ ����� ������, ����������� ����, ����� ��������� ������� � ����������� ����������, ��� ��� ��� ��� ������ �������������� 
REM -- ����� �������� OEM, � �� ANSI, ����� ��������� ��������  
REM -- �������� �����. � ����� ������ � ��������� ������������ ����� �������� ANSI.  
REM -- ���� ����� ������������ ������ � ���� ������, ��� ��� ��� �������� 
REM -- ����� �������� ANSI.

[!endif]
REM -- ������� �������� ���� ������������ �� ���������� Microsoft Visual C++ ����� resource.h
echo // ��������� MAKEHELP.BAT ���� ������������ ��� �������.  ������������ [!output PROJECT_NAME].HPJ. >"hlp\[!output HM_FILE_OEM].hm"
echo. >>"hlp\[!output HM_FILE_OEM].hm"
echo // ������� (ID_* � IDM_*) >>"hlp\[!output HM_FILE_OEM].hm"
makehm ID_,HID_,0x10000 IDM_,HIDM_,0x10000 resource.h >>"hlp\[!output HM_FILE_OEM].hm"
echo. >>"hlp\[!output HM_FILE_OEM].hm"
echo // ��������� (IDP_*) >>"hlp\[!output HM_FILE_OEM].hm"
makehm IDP_,HIDP_,0x30000 resource.h >>"hlp\[!output HM_FILE_OEM].hm"
echo. >>"hlp\[!output HM_FILE_OEM].hm"
echo // ������� (IDR_*) >>"hlp\[!output HM_FILE_OEM].hm"
makehm IDR_,HIDR_,0x20000 resource.h >>"hlp\[!output HM_FILE_OEM].hm"
echo. >>"hlp\[!output HM_FILE_OEM].hm"
echo // ���������� ���� (IDD_*) >>"hlp\[!output HM_FILE_OEM].hm"
makehm IDD_,HIDD_,0x20000 resource.h >>"hlp\[!output HM_FILE_OEM].hm"
echo. >>"hlp\[!output HM_FILE_OEM].hm"
echo // �������� ���������� ������ (IDW_*) >>"hlp\[!output HM_FILE_OEM].hm"
makehm IDW_,HIDW_,0x50000 resource.h >>"hlp\[!output HM_FILE_OEM].hm"
REM -- �������� ������� ��� ������� [!output PROJECT_NAME]

echo ���������� ������ ������� Win32
start /wait hcw /C /E /M "hlp\[!output PROJECT_NAME_OEM].hpj"
if errorlevel 1 goto :Error
if not exist "hlp\[!output PROJECT_NAME_OEM].hlp" goto :Error
if not exist "hlp\[!output PROJECT_NAME_OEM].cnt" goto :Error
echo.
if exist Debug\nul copy "hlp\[!output PROJECT_NAME_OEM].hlp" Debug
if exist Debug\nul copy "hlp\[!output PROJECT_NAME_OEM].cnt" Debug
if exist Release\nul copy "hlp\[!output PROJECT_NAME_OEM].hlp" Release
if exist Release\nul copy "hlp\[!output PROJECT_NAME_OEM].cnt" Release
echo.
goto :done

:Error
echo hlp\[!output PROJECT_NAME].hpj(1) : error: �������� ��� �������� ����� �������

:done
echo.
