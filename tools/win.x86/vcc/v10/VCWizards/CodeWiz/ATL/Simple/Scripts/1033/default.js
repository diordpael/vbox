// Copyright (c) Microsoft Corporation. All rights reserved.
// Script for ATL Simple Object

function OnPrep(selProj, selObj)
{
	var L_WizardDialogTitle_Text = "ATL Simple Object Wizard";
	return PrepCodeWizard(selProj, L_WizardDialogTitle_Text);
}

function OnFinish(selProj, selObj)
{
	var oCM;
	try
	{
		oCM	= selProj.CodeModel;
		var bDevice = IsDeviceProject(selProj);
		wizard.AddSymbol("DEVICE", bDevice);
		AddDeviceSymbols(false);

		var strShortName = wizard.FindSymbol("SHORT_NAME");
		var L_TRANSACTION_Text = "Add ATL Simple Object ";
		oCM.StartTransaction(L_TRANSACTION_Text + strShortName);
		if(!AddATLSupportToProject(selProj))
		{
			oCM.AbortTransaction();
			return;
		}

		var bDLL;
		if (typeDynamicLibrary == selProj.Object.Configurations(1).ConfigurationType)
			bDLL = true;
		else
			bDLL = false;
		wizard.AddSymbol("DLL_APP", bDLL);
		
		// used only for device projects where platforms may not support DCOM.
		wizard.AddSymbol("SUPPORT_DCOM", false);
		wizard.AddSymbol("SUPPORT_NON_DCOM", false);

		var isMFCProject = IsMFCProject(selProj, false);
		wizard.AddSymbol("MFC_SUPPORT", isMFCProject);


		var strProjectName		= wizard.FindSymbol("SAFE_PROJECT_IDENTIFIER_NAME");
		var strProjectPath		= wizard.FindSymbol("PROJECT_PATH");
		var strTemplatePath		= wizard.FindSymbol("TEMPLATES_PATH");
		var strUpperShortName		= CreateASCIIName(strShortName.toUpperCase());
		var strInterfaceName		= wizard.FindSymbol("INTERFACE_NAME");
		var strProjectIdlName   	= strProjectName + ".idl";
		
		wizard.AddSymbol("UPPER_SHORT_NAME", strUpperShortName);
		var strVIProgID			= wizard.FindSymbol("VERSION_INDEPENDENT_PROGID");
		if (strVIProgID == null || strVIProgID == "")
		{
			wizard.AddSymbol("PROGID_VALID", false);
			wizard.AddSymbol("VERSION_INDEPENDENT_PROGID","");
			wizard.AddSymbol("PROGID","");
		}
		else
		{
			wizard.AddSymbol("PROGID_VALID", true);
			wizard.AddSymbol("PROGID", strVIProgID.substr(0,37) + ".1");
		}
		var bConnectionPoint		= wizard.FindSymbol("CONNECTION_POINTS");
 		var strClassName		= wizard.FindSymbol("CLASS_NAME");
		var strHeaderFile		= wizard.FindSymbol("HEADER_FILE");
		var strImplFile			= wizard.FindSymbol("IMPL_FILE");
		var strCoClass			= wizard.FindSymbol("COCLASS");
		var bAttributed			= wizard.FindSymbol("ATTRIBUTED");

		var strProjectRC		= GetProjectFile(selProj, "RC", true, false);

		// Create necessary GUIDS
		CreateGUIDs();

		if (!bAttributed)
		{
			var MidlTool = GetIDLConfig(selProj,true);
			var strMidlHeader = MidlTool.HeaderFileName;
			strMidlHeader = selProj.Object.Configurations(1).Evaluate(strMidlHeader);
			wizard.AddSymbol("MIDL_H_FILENAME",strMidlHeader);

			// Get LibName
			wizard.AddSymbol("LIB_NAME", oCM.IDLLibraries(1).Name);

			// Get LibID
			var oUuid = oCM.IDLLibraries(1).Attributes.Find("uuid");
			if (oUuid)
				wizard.AddSymbol("LIBID_REGISTRY_FORMAT", oUuid.Value);

			// Get typelib version
			var oVersion = oCM.IDLLibraries(1).Attributes.Find("version");
			if (oVersion)
			{
				var aryMajorMinor = oVersion.Value.split('.');
				for (var nCntr=0; nCntr<aryMajorMinor.length; nCntr++)
				{
					if (nCntr == 0)
						wizard.AddSymbol("TYPELIB_VERSION_MAJOR", aryMajorMinor[nCntr]);
					else
						wizard.AddSymbol("TYPELIB_VERSION_MINOR", aryMajorMinor[nCntr]);
				}
			}

			// Get AppID
			var strAppID = wizard.GetAppID();
			if (strAppID.length > 0)
			{
				wizard.AddSymbol("APPID_EXIST", true);
				wizard.AddSymbol("APPID_REGISTRY_FORMAT", strAppID);
			}

			// add RGS file resource
			var strRGSFile = GetUniqueFileName(strProjectPath, CreateASCIIName(strShortName) + ".rgs");
			var strRGSDCOMFile = GetUniqueFileName(strProjectPath, CreateASCIIName(strShortName + "DCOM") + ".rgs");
			var strRGSID = "IDR_" + strUpperShortName;
			var strRGSDCOMID = "IDR_" + strUpperShortName + "DCOM";

			var bDeviceDCOM = ProjectContainsDCOMPlatform(selProj)
			var bDeviceNonDCOM = ProjectContainsNonDCOMPlatform(selProj)
			
			if (!bDevice)
			{
				RenderAddTemplate(wizard, "object.rgs", strRGSFile, false, false);
			}
			else
			{
				if (bDeviceDCOM)
				{
					wizard.AddSymbol("SUPPORT_DCOM", true);
					RenderAddTemplate(wizard, "object.rgs", strRGSDCOMFile, false, false);
					wizard.AddSymbol("SUPPORT_DCOM", false);
				}
				if (bDeviceNonDCOM)
				{
					wizard.AddSymbol("SUPPORT_NON_DCOM", true);
					RenderAddTemplate(wizard, "object.rgs", strRGSFile, false, false);
					wizard.AddSymbol("SUPPORT_NON_DCOM", false);
				} 
			}    

			wizard.AddSymbol("SUPPORT_DCOM", bDeviceDCOM);
			wizard.AddSymbol("SUPPORT_NON_DCOM", bDeviceNonDCOM);

			if (!bDevice)
			{
				var oResHelper = wizard.ResourceHelper;
				oResHelper.OpenResourceFile(strProjectRC);

				var strSymbolValue = oResHelper.AddResource(strRGSID, strProjectPath + strRGSFile, "REGISTRY");
				if (strSymbolValue == null) return;				
				wizard.AddSymbol("RGS_ID", strSymbolValue.split("=").shift());

				oResHelper.CloseResourceFile();
			}
			else
			{
				var completedResourceFiles = new Array();
				var configs = selProj.Object.Configurations;
				AddDeviceSymbols(false);

				var oResHelper = wizard.ResourceHelper;

				for (var nCntr = 1; nCntr <= configs.Count; nCntr++)
				{
					var config = configs.Item(nCntr);
					var strCurrentResource = GetDeviceResourceFileForConfig(config);
					
					if (completedResourceFiles.join(";").indexOf(strCurrentResource) == -1)
					{
						oResHelper.OpenResourceFile(strCurrentResource);
						if (bDeviceNonDCOM)
						{
							var strSymbolValue = oResHelper.AddResource(strRGSID, strProjectPath + strRGSFile, "REGISTRY");
							if (strSymbolValue == null) return;				
							wizard.AddSymbol("RGS_ID", strSymbolValue.split("=").shift());
						}
						if (bDeviceDCOM)
						{
							var strSymbolValue = oResHelper.AddResource(strRGSDCOMID, strProjectPath + strRGSDCOMFile, "REGISTRY");
							if (strSymbolValue == null) return;
							wizard.AddSymbol("RGSDCOM_ID", strSymbolValue.split("=").shift());
						}
						oResHelper.CloseResourceFile();
						completedResourceFiles.push(strCurrentResource);
					}
					
				}
			}

			// Add connection point support
			if (bConnectionPoint)
				RenderAddTemplate(wizard, "connpt.h", "_" + strInterfaceName + "Events_CP.h", selObj, false);

			// Render objco.idl and insert into strProject.idl
			AddCoclassFromFile(oCM, "objco.idl");

			// Render objint.idl and insert into strProject.idl
			AddInterfaceFromFile(oCM, "objint.idl");
			
			// Add required import statements
			AddImportStatements(oCM, selProj, strProjectIdlName);
			
			// Add (generate) ATL-based document definition and implementation if required, 
			// Add #define SHARED_HANDLERS to stdafx.h
			if (wizard.FindSymbol("ADD_HANDLER_SUPPORT"))
			{
				if (wizard.FindSymbol("HANDLER_GEN_ATL_DOC"))
				{
					var strAtlDocHeader = wizard.FindSymbol("HANDLER_ATL_DOC_HEADER_FILE_TRIMMED");
					var strAtlDocImpl = wizard.FindSymbol("HANDLER_ATL_DOC_IMPL_FILE_TRIMMED");

					// Add header
					RenderAddTemplate(wizard, "document.h", strAtlDocHeader, selObj, false);

					// Add CPP
					RenderAddTemplate(wizard, "document.cpp", strAtlDocImpl, selObj, false);
				}
				else if (wizard.FindSymbol("HANDLER_ATL_DOC_CLASS_NAME_SPECIFIED"))
				{
					// user specifies ATL-based doc class name only
					var strAtlDocName = wizard.FindSymbol("HANDLER_ATL_DOC_CLASS_NAME_TRIMMED");
					var elem = oCM.Classes.Find(strAtlDocName);

					// add #include if the class is defined
					if (elem != null)
					{
						var strAtlDocNameFromCM = elem.File.substr(elem.File.lastIndexOf("\\") + 1);
					}
				}
				else
				{
					// Add existing items (links) from MFC project handlers are being added for.
					// Add the document h/cpp, view h.cpp, and cntritem h/cpp (if exist).
					var strMFCDocHeader = wizard.FindSymbol("HANDLER_DOCUMENT_FILE_NAME_TRIMMED");
					if (strMFCDocHeader != "")
					{
						if (selProj.Object.CanAddFile(strMFCDocHeader))
						{
							selProj.Object.AddFile(strMFCDocHeader);
						}

						var strMFCDocImpl = strMFCDocHeader.substring(0, strMFCDocHeader.length - 1) + "cpp";
						if (selProj.Object.CanAddFile(strMFCDocImpl))
						{
							selProj.Object.AddFile(strMFCDocImpl);
						}

						var strMFCViewHeader = wizard.FindSymbol("HANDLER_VIEW_FILE_NAME_TRIMMED");
						if (strMFCViewHeader != "")
						{
							if (selProj.Object.CanAddFile(strMFCViewHeader))
							{
								selProj.Object.AddFile(strMFCViewHeader);
							}

							var strMFCViewImpl = strMFCViewHeader.substring(0, strMFCViewHeader.length - 1) + "cpp";
							if (selProj.Object.CanAddFile(strMFCViewImpl))
							{
								selProj.Object.AddFile(strMFCViewImpl);
							}
						}

						var nLength = strMFCDocHeader.length;
						var nEndIndex = nLength - 1;
						while (nEndIndex > 0 && (strMFCDocHeader.charAt(nEndIndex) != '\\'))
						{
							nEndIndex--;
						}

						var strMFCCntrHeader = strMFCDocHeader.substring(0, nEndIndex + 1) + "CntrItem.h";
						if (selProj.Object.CanAddFile(strMFCCntrHeader))
						{
							selProj.Object.AddFile(strMFCCntrHeader);
						}

						var strMFCCntrImpl = strMFCDocHeader.substring(0, nEndIndex + 1) + "CntrItem.cpp";
						if (selProj.Object.CanAddFile(strMFCCntrImpl))
						{
							selProj.Object.AddFile(strMFCCntrImpl);
						}
					}
				}

				AddDefineSharedHandlersStatement(oCM);
			}

			SetMergeProxySymbol(selProj);
		}

		// Add header
		RenderAddTemplate(wizard, "object.h", strHeaderFile, selObj, true);

		// Add CPP
		RenderAddTemplate(wizard, "object.cpp", strImplFile, selObj, false);

		oCM.CommitTransaction();
				
		var newClass = oCM.Classes.Find(strClassName);
		if(newClass)
			newClass.StartPoint.TryToShow(vsPaneShowTop);		
	}
	catch(e)
	{
		if (oCM)
			oCM.AbortTransaction();

		if (e.description.length != 0)
			SetErrorInfo(e);
		return e.number
	}
}

function CreateGUIDs()
{
	try
	{
		// create CLSID
		var strRawGUID = wizard.CreateGuid();
		var strFormattedGUID = wizard.FormatGuid(strRawGUID, 0);
		wizard.AddSymbol("CLSID_REGISTRY_FORMAT", strFormattedGUID);

		// create interface GUID
		strRawGUID = wizard.CreateGuid();
		strFormattedGUID = wizard.FormatGuid(strRawGUID, 0);
		wizard.AddSymbol("INTERFACE_IID", strFormattedGUID);
		
		// create GUID for Search Persistent Handler registration (used in rgs)
		var bSearchHandler = wizard.FindSymbol("SEARCH_HANDLER");
		if (bSearchHandler)
		{
		    strRawGUID = wizard.CreateGuid();
		    strFormattedGUID = wizard.FormatGuid(strRawGUID, 0);
		    wizard.AddSymbol("CLSID_PERSISTENT_HANDLER", strFormattedGUID);
		}

		// create connection point GUID
		var bConnectionPoint = wizard.FindSymbol("CONNECTION_POINTS");
		if (bConnectionPoint)
		{
			strRawGUID = wizard.CreateGuid();
			strFormattedGUID = wizard.FormatGuid(strRawGUID, 0);
			wizard.AddSymbol("CONNECTION_POINT_IID", strFormattedGUID);
		}
	}
	catch(e)
	{
		throw e;
	}
}

function AddImportStatements(oCM, selProj, strProjectIdlName)
{
    var vcIDLImport;
    
	var bSearchHandler = wizard.FindSymbol("SEARCH_HANDLER");
	if (bSearchHandler)
	{
	    var i = 0;
        for (i = 1; i <= oCM.IDLImports.Count; i++)
        {
            vcIDLImport = oCM.IDLImports.Item(i);
            if (vcIDLImport.Name == "filter.idl")
            {
                return;
            }
        }
        
        oCM.AddIDLImport("\"filter.idl\"", strProjectIdlName, -1);
	}
	
    var bThumbnailProvider = wizard.FindSymbol("THUMBNAIL_PROVIDER_HANDLER");
    if (bThumbnailProvider)
    {
        var i = 0;
        for (i = 1; i <= oCM.IDLImports.Count; i++)
        {
            vcIDLImport = oCM.IDLImports.Item(i);
            if (vcIDLImport.Name == "thumbcache.idl")
            {
                return;
            }
        }
        
        oCM.AddIDLImport("\"thumbcache.idl\"", strProjectIdlName, -1);
    }
    
    var bRichPreviewHandler = wizard.FindSymbol("PREVIEW_HANDLER");
    if (bRichPreviewHandler)
    {
        var i = 0;
        for (i = 1; i <= oCM.IDLImports.Count; i++)
        {
            vcIDLImport = oCM.IDLImports.Item(i);
            if (vcIDLImport.Name == "shobjidl.idl")
            {
                return;
            }
        }
        
        oCM.AddIDLImport("\"shobjidl.idl\"", strProjectIdlName, -1);
    }
}
function AddDefineSharedHandlersStatement(oCM)
{
    if(oCM.Macros.Find("SHARED_HANDLERS") == null)
    {
        oCM.AddMacro("SHARED_HANDLERS", "stdafx.h", "", -1);
    }
}

// SIG // Begin signature block
// SIG // MIIXOgYJKoZIhvcNAQcCoIIXKzCCFycCAQExCzAJBgUr
// SIG // DgMCGgUAMGcGCisGAQQBgjcCAQSgWTBXMDIGCisGAQQB
// SIG // gjcCAR4wJAIBAQQQEODJBs441BGiowAQS9NQkAIBAAIB
// SIG // AAIBAAIBAAIBADAhMAkGBSsOAwIaBQAEFOaT58IHoptA
// SIG // 7jvxV5CCRQYDNm6loIISMTCCBGAwggNMoAMCAQICCi6r
// SIG // EdxQ/1ydy8AwCQYFKw4DAh0FADBwMSswKQYDVQQLEyJD
// SIG // b3B5cmlnaHQgKGMpIDE5OTcgTWljcm9zb2Z0IENvcnAu
// SIG // MR4wHAYDVQQLExVNaWNyb3NvZnQgQ29ycG9yYXRpb24x
// SIG // ITAfBgNVBAMTGE1pY3Jvc29mdCBSb290IEF1dGhvcml0
// SIG // eTAeFw0wNzA4MjIyMjMxMDJaFw0xMjA4MjUwNzAwMDBa
// SIG // MHkxCzAJBgNVBAYTAlVTMRMwEQYDVQQIEwpXYXNoaW5n
// SIG // dG9uMRAwDgYDVQQHEwdSZWRtb25kMR4wHAYDVQQKExVN
// SIG // aWNyb3NvZnQgQ29ycG9yYXRpb24xIzAhBgNVBAMTGk1p
// SIG // Y3Jvc29mdCBDb2RlIFNpZ25pbmcgUENBMIIBIjANBgkq
// SIG // hkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAt3l91l2zRTmo
// SIG // NKwx2vklNUl3wPsfnsdFce/RRujUjMNrTFJi9JkCw03Y
// SIG // SWwvJD5lv84jtwtIt3913UW9qo8OUMUlK/Kg5w0jH9FB
// SIG // JPpimc8ZRaWTSh+ZzbMvIsNKLXxv2RUeO4w5EDndvSn0
// SIG // ZjstATL//idIprVsAYec+7qyY3+C+VyggYSFjrDyuJSj
// SIG // zzimUIUXJ4dO3TD2AD30xvk9gb6G7Ww5py409rQurwp9
// SIG // YpF4ZpyYcw2Gr/LE8yC5TxKNY8ss2TJFGe67SpY7UFMY
// SIG // zmZReaqth8hWPp+CUIhuBbE1wXskvVJmPZlOzCt+M26E
// SIG // RwbRntBKhgJuhgCkwIffUwIDAQABo4H6MIH3MBMGA1Ud
// SIG // JQQMMAoGCCsGAQUFBwMDMIGiBgNVHQEEgZowgZeAEFvQ
// SIG // cO9pcp4jUX4Usk2O/8uhcjBwMSswKQYDVQQLEyJDb3B5
// SIG // cmlnaHQgKGMpIDE5OTcgTWljcm9zb2Z0IENvcnAuMR4w
// SIG // HAYDVQQLExVNaWNyb3NvZnQgQ29ycG9yYXRpb24xITAf
// SIG // BgNVBAMTGE1pY3Jvc29mdCBSb290IEF1dGhvcml0eYIP
// SIG // AMEAizw8iBHRPvZj7N9AMA8GA1UdEwEB/wQFMAMBAf8w
// SIG // HQYDVR0OBBYEFMwdznYAcFuv8drETppRRC6jRGPwMAsG
// SIG // A1UdDwQEAwIBhjAJBgUrDgMCHQUAA4IBAQB7q65+Siby
// SIG // zrxOdKJYJ3QqdbOG/atMlHgATenK6xjcacUOonzzAkPG
// SIG // yofM+FPMwp+9Vm/wY0SpRADulsia1Ry4C58ZDZTX2h6t
// SIG // KX3v7aZzrI/eOY49mGq8OG3SiK8j/d/p1mkJkYi9/uEA
// SIG // uzTz93z5EBIuBesplpNCayhxtziP4AcNyV1ozb2AQWtm
// SIG // qLu3u440yvIDEHx69dLgQt97/uHhrP7239UNs3DWkuNP
// SIG // tjiifC3UPds0C2I3Ap+BaiOJ9lxjj7BauznXYIxVhBoz
// SIG // 9TuYoIIMol+Lsyy3oaXLq9ogtr8wGYUgFA0qvFL0QeBe
// SIG // MOOSKGmHwXDi86erzoBCcnYOMIIEejCCA2KgAwIBAgIK
// SIG // YQHPPgAAAAAADzANBgkqhkiG9w0BAQUFADB5MQswCQYD
// SIG // VQQGEwJVUzETMBEGA1UECBMKV2FzaGluZ3RvbjEQMA4G
// SIG // A1UEBxMHUmVkbW9uZDEeMBwGA1UEChMVTWljcm9zb2Z0
// SIG // IENvcnBvcmF0aW9uMSMwIQYDVQQDExpNaWNyb3NvZnQg
// SIG // Q29kZSBTaWduaW5nIFBDQTAeFw0wOTEyMDcyMjQwMjla
// SIG // Fw0xMTAzMDcyMjQwMjlaMIGDMQswCQYDVQQGEwJVUzET
// SIG // MBEGA1UECBMKV2FzaGluZ3RvbjEQMA4GA1UEBxMHUmVk
// SIG // bW9uZDEeMBwGA1UEChMVTWljcm9zb2Z0IENvcnBvcmF0
// SIG // aW9uMQ0wCwYDVQQLEwRNT1BSMR4wHAYDVQQDExVNaWNy
// SIG // b3NvZnQgQ29ycG9yYXRpb24wggEiMA0GCSqGSIb3DQEB
// SIG // AQUAA4IBDwAwggEKAoIBAQC9MIn7RXKoU2ueiU8AI8C+
// SIG // 1B09sVlAOPNzkYIm5pYSAFPZHIIOPM4du733Qo2X1Pw4
// SIG // GuS5+ePs02EDv6DT1nVNXEap7V7w0uJpWxpz6rMcjQTN
// SIG // KUSgZFkvHphdbserGDmCZcSnvKt1iBnqh5cUJrN/Jnak
// SIG // 1Dg5hOOzJtUY+Svp0skWWlQh8peNh4Yp/vRJLOaL+AQ/
// SIG // fc3NlpKGDXED4tD+DEI1/9e4P92ORQp99tdLrVvwdnId
// SIG // dyN9iTXEHF2yUANLR20Hp1WImAaApoGtVE7Ygdb6v0LA
// SIG // Mb5VDZnVU0kSMOvlpYh8XsR6WhSHCLQ3aaDrMiSMCOv5
// SIG // 1BS64PzN6qQVAgMBAAGjgfgwgfUwEwYDVR0lBAwwCgYI
// SIG // KwYBBQUHAwMwHQYDVR0OBBYEFDh4BXPIGzKbX5KGVa+J
// SIG // usaZsXSOMA4GA1UdDwEB/wQEAwIHgDAfBgNVHSMEGDAW
// SIG // gBTMHc52AHBbr/HaxE6aUUQuo0Rj8DBEBgNVHR8EPTA7
// SIG // MDmgN6A1hjNodHRwOi8vY3JsLm1pY3Jvc29mdC5jb20v
// SIG // cGtpL2NybC9wcm9kdWN0cy9DU1BDQS5jcmwwSAYIKwYB
// SIG // BQUHAQEEPDA6MDgGCCsGAQUFBzAChixodHRwOi8vd3d3
// SIG // Lm1pY3Jvc29mdC5jb20vcGtpL2NlcnRzL0NTUENBLmNy
// SIG // dDANBgkqhkiG9w0BAQUFAAOCAQEAKAODqxMN8f4Rb0J2
// SIG // 2EOruMZC+iRlNK51sHEwjpa2g/py5P7NN+c6cJhRIA66
// SIG // cbTJ9NXkiugocHPV7eHCe+7xVjRagILrENdyA+oSTuzd
// SIG // DYx7RE8MYXX9bpwH3c4rWhgNObBg/dr/BKoCo9j6jqO7
// SIG // vcFqVDsxX+QsbsvxTSoc8h52e4avxofWsSrtrMwOwOSf
// SIG // f+jP6IRyVIIYbirInpW0Gh7Bb5PbYqbBS2utye09kuOy
// SIG // L6t6dzlnagB7gp0DEN5jlUkmQt6VIsGHC9AUo1/cczJy
// SIG // Nh7/yCnFJFJPZkjJHR2pxSY5aVBOp+zCBmwuchvxIdpt
// SIG // JEiAgRVAfJ/MdDhKTzCCBJ0wggOFoAMCAQICEGoLmU/A
// SIG // ACWrEdtFH1h6Z6IwDQYJKoZIhvcNAQEFBQAwcDErMCkG
// SIG // A1UECxMiQ29weXJpZ2h0IChjKSAxOTk3IE1pY3Jvc29m
// SIG // dCBDb3JwLjEeMBwGA1UECxMVTWljcm9zb2Z0IENvcnBv
// SIG // cmF0aW9uMSEwHwYDVQQDExhNaWNyb3NvZnQgUm9vdCBB
// SIG // dXRob3JpdHkwHhcNMDYwOTE2MDEwNDQ3WhcNMTkwOTE1
// SIG // MDcwMDAwWjB5MQswCQYDVQQGEwJVUzETMBEGA1UECBMK
// SIG // V2FzaGluZ3RvbjEQMA4GA1UEBxMHUmVkbW9uZDEeMBwG
// SIG // A1UEChMVTWljcm9zb2Z0IENvcnBvcmF0aW9uMSMwIQYD
// SIG // VQQDExpNaWNyb3NvZnQgVGltZXN0YW1waW5nIFBDQTCC
// SIG // ASIwDQYJKoZIhvcNAQEBBQADggEPADCCAQoCggEBANw3
// SIG // bvuvyEJKcRjIzkg+U8D6qxS6LDK7Ek9SyIPtPjPZSTGS
// SIG // KLaRZOAfUIS6wkvRfwX473W+i8eo1a5pcGZ4J2botrfv
// SIG // hbnN7qr9EqQLWSIpL89A2VYEG3a1bWRtSlTb3fHev5+D
// SIG // x4Dff0wCN5T1wJ4IVh5oR83ZwHZcL322JQS0VltqHGP/
// SIG // gHw87tUEJU05d3QHXcJc2IY3LHXJDuoeOQl8dv6dbG56
// SIG // 4Ow+j5eecQ5fKk8YYmAyntKDTisiXGhFi94vhBBQsvm1
// SIG // Go1s7iWbE/jLENeFDvSCdnM2xpV6osxgBuwFsIYzt/iU
// SIG // W4RBhFiFlG6wHyxIzG+cQ+Bq6H8mjmsCAwEAAaOCASgw
// SIG // ggEkMBMGA1UdJQQMMAoGCCsGAQUFBwMIMIGiBgNVHQEE
// SIG // gZowgZeAEFvQcO9pcp4jUX4Usk2O/8uhcjBwMSswKQYD
// SIG // VQQLEyJDb3B5cmlnaHQgKGMpIDE5OTcgTWljcm9zb2Z0
// SIG // IENvcnAuMR4wHAYDVQQLExVNaWNyb3NvZnQgQ29ycG9y
// SIG // YXRpb24xITAfBgNVBAMTGE1pY3Jvc29mdCBSb290IEF1
// SIG // dGhvcml0eYIPAMEAizw8iBHRPvZj7N9AMBAGCSsGAQQB
// SIG // gjcVAQQDAgEAMB0GA1UdDgQWBBRv6E4/l7k0q0uGj7yc
// SIG // 6qw7QUPG0DAZBgkrBgEEAYI3FAIEDB4KAFMAdQBiAEMA
// SIG // QTALBgNVHQ8EBAMCAYYwDwYDVR0TAQH/BAUwAwEB/zAN
// SIG // BgkqhkiG9w0BAQUFAAOCAQEAlE0RMcJ8ULsRjqFhBwEO
// SIG // jHBFje9zVL0/CQUt/7hRU4Uc7TmRt6NWC96Mtjsb0fus
// SIG // p8m3sVEhG28IaX5rA6IiRu1stG18IrhG04TzjQ++B4o2
// SIG // wet+6XBdRZ+S0szO3Y7A4b8qzXzsya4y1Ye5y2PENtEY
// SIG // Ib923juasxtzniGI2LS0ElSM9JzCZUqaKCacYIoPO8cT
// SIG // ZXhIu8+tgzpPsGJY3jDp6Tkd44ny2jmB+RMhjGSAYwYE
// SIG // lvKaAkMve0aIuv8C2WX5St7aA3STswVuDMyd3ChhfEjx
// SIG // F5wRITgCHIesBsWWMrjlQMZTPb2pid7oZjeN9CKWnMyw
// SIG // d1RROtZyRLIj9jCCBKowggOSoAMCAQICCmEFojAAAAAA
// SIG // AAgwDQYJKoZIhvcNAQEFBQAweTELMAkGA1UEBhMCVVMx
// SIG // EzARBgNVBAgTCldhc2hpbmd0b24xEDAOBgNVBAcTB1Jl
// SIG // ZG1vbmQxHjAcBgNVBAoTFU1pY3Jvc29mdCBDb3Jwb3Jh
// SIG // dGlvbjEjMCEGA1UEAxMaTWljcm9zb2Z0IFRpbWVzdGFt
// SIG // cGluZyBQQ0EwHhcNMDgwNzI1MTkwMTE1WhcNMTMwNzI1
// SIG // MTkxMTE1WjCBszELMAkGA1UEBhMCVVMxEzARBgNVBAgT
// SIG // Cldhc2hpbmd0b24xEDAOBgNVBAcTB1JlZG1vbmQxHjAc
// SIG // BgNVBAoTFU1pY3Jvc29mdCBDb3Jwb3JhdGlvbjENMAsG
// SIG // A1UECxMETU9QUjEnMCUGA1UECxMebkNpcGhlciBEU0Ug
// SIG // RVNOOjg1RDMtMzA1Qy01QkNGMSUwIwYDVQQDExxNaWNy
// SIG // b3NvZnQgVGltZS1TdGFtcCBTZXJ2aWNlMIIBIjANBgkq
// SIG // hkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEA8AQtspbAGoFn
// SIG // JbEmYrMTS84wusASOPyBZTQHxDayJGj2BwTAB5f0t/F7
// SIG // HmIsRtlLpFE0t9Ns7Vo7tIOhRz0RCC41a0XmwjyMAmYC
// SIG // qRhp60rtJyzuPHdbpNRwmUtXhBDQry34iR3m6im058+e
// SIG // BmKnclTCO8bPP7jhsFgQbOWl18PCdTe99IXhgego2Bvx
// SIG // 8q7xgqPW1wOinxWE+z36q+G2MsigAmTz5v8aJnEIU4oV
// SIG // AvKDJ3ZJgnGn760yeMbXbBZPImWXYk1GL/8jr4XspnC9
// SIG // A8va2DIFxSuQQLae1SyGbLfLEzJ9jcZ+rhcvMvxmux2w
// SIG // RVX4rfotZ4NnKZOE0lqhIwIDAQABo4H4MIH1MB0GA1Ud
// SIG // DgQWBBTol/b374zx5mnjWWhO95iKet2bLjAfBgNVHSME
// SIG // GDAWgBRv6E4/l7k0q0uGj7yc6qw7QUPG0DBEBgNVHR8E
// SIG // PTA7MDmgN6A1hjNodHRwOi8vY3JsLm1pY3Jvc29mdC5j
// SIG // b20vcGtpL2NybC9wcm9kdWN0cy90c3BjYS5jcmwwSAYI
// SIG // KwYBBQUHAQEEPDA6MDgGCCsGAQUFBzAChixodHRwOi8v
// SIG // d3d3Lm1pY3Jvc29mdC5jb20vcGtpL2NlcnRzL3RzcGNh
// SIG // LmNydDATBgNVHSUEDDAKBggrBgEFBQcDCDAOBgNVHQ8B
// SIG // Af8EBAMCBsAwDQYJKoZIhvcNAQEFBQADggEBAA0/d1+R
// SIG // PL6lNaTbBQWEH1by75mmxwiNL7PNP3HVhnx3H93rF7K9
// SIG // fOP5mfIKRUitFLtpLPI+Z2JU8u5/JxGSOezO2YdOiPdg
// SIG // RyN7JxVACJ+/DTEEgtg1tgycANOLqnhhxbWIQZ0+NtxY
// SIG // pCebOtq9Bl0UprIPTMGOPIvyYpn4Zu3V8xwosDLbyjEJ
// SIG // vPsiaEZM+tNzIucpjiIA+1a/Bq6BoBW6NPkojh9KYgWh
// SIG // ifWBR+kNkQjXWDuPHmsJaanASHxVgj9fADhDnAbMP9gv
// SIG // v09zCT39ul70x+w3wmRhoE3UPXDMW7ATgcHUozEavWTW
// SIG // ltJ6PypbRlMJPM0D+T9ZAMyJU2ExggR1MIIEcQIBATCB
// SIG // hzB5MQswCQYDVQQGEwJVUzETMBEGA1UECBMKV2FzaGlu
// SIG // Z3RvbjEQMA4GA1UEBxMHUmVkbW9uZDEeMBwGA1UEChMV
// SIG // TWljcm9zb2Z0IENvcnBvcmF0aW9uMSMwIQYDVQQDExpN
// SIG // aWNyb3NvZnQgQ29kZSBTaWduaW5nIFBDQQIKYQHPPgAA
// SIG // AAAADzAJBgUrDgMCGgUAoIGgMBkGCSqGSIb3DQEJAzEM
// SIG // BgorBgEEAYI3AgEEMBwGCisGAQQBgjcCAQsxDjAMBgor
// SIG // BgEEAYI3AgEVMCMGCSqGSIb3DQEJBDEWBBSbRbh3ULTE
// SIG // hhtdM2Lcol/NOomv4zBABgorBgEEAYI3AgEMMTIwMKAW
// SIG // gBQAZABlAGYAYQB1AGwAdAAuAGoAc6EWgBRodHRwOi8v
// SIG // bWljcm9zb2Z0LmNvbTANBgkqhkiG9w0BAQEFAASCAQBF
// SIG // mRxrbrIvpsVHtaYMgRcLPaokEWShuqb86oX5c50xSOlW
// SIG // /uYAE2wYXKifBEyjTGK2NZmRB5h6435JuWJQZMHxWoTn
// SIG // 899Buxs3C/M0cjhuxNVrVLfOzJ2qvIfyEJ9N6kBKglWz
// SIG // Z4zISFQ1x04SRk6S/WoCIUYawH9q3EHAJKwf/dEd2+gw
// SIG // /lw5COLGzL5Rs2zaOZKYI9gyoxW6qNHEvs1UMJ5aOdBC
// SIG // LcMHw2DNmTOcKL+TMlY5crXm2Ll3PNrDJ5VvEO8e1Sau
// SIG // ygvCCcyQ8yF4jVInktrA1JWtQKTP7ScJPbGR86UK1JIh
// SIG // RgCwOZ/JH9B86vDXE/EZ1UmjGgtkDWieoYICHzCCAhsG
// SIG // CSqGSIb3DQEJBjGCAgwwggIIAgEBMIGHMHkxCzAJBgNV
// SIG // BAYTAlVTMRMwEQYDVQQIEwpXYXNoaW5ndG9uMRAwDgYD
// SIG // VQQHEwdSZWRtb25kMR4wHAYDVQQKExVNaWNyb3NvZnQg
// SIG // Q29ycG9yYXRpb24xIzAhBgNVBAMTGk1pY3Jvc29mdCBU
// SIG // aW1lc3RhbXBpbmcgUENBAgphBaIwAAAAAAAIMAcGBSsO
// SIG // AwIaoF0wGAYJKoZIhvcNAQkDMQsGCSqGSIb3DQEHATAc
// SIG // BgkqhkiG9w0BCQUxDxcNMTAwMzE5MTMwMjM3WjAjBgkq
// SIG // hkiG9w0BCQQxFgQU8mcMhNpSto/N+D4cXDae/EiwlbMw
// SIG // DQYJKoZIhvcNAQEFBQAEggEA4rxZaDi4IgMYYIyqapQR
// SIG // RCUolW5whAl3ofOd/2rArh6slbhxxuDHiyYDjfasNGr8
// SIG // JcTIuMt4q4yQvUkCs3e4czjyTmW4Gfmq6QQNVCmj0Ihc
// SIG // yQcjgtA8AB0vpqK1uV1Y2tDNEfgfOoV0ixPF78hoZwRG
// SIG // IgsYWsMMs2KfMo/BwMFCGGIOx0Fmxlq2Fq/8scIaPJ04
// SIG // 1BvczInTZ6Dwcx7figealQWsWtSI+WJpH5Y88cw7QkGo
// SIG // 8yhBjc63P6iqsmKvsYHS60V97x7O/c6F9MGO/gu4FYxk
// SIG // StI+u1rvzQ7VZA6A1fiRT66CwQnypseLDzSNdzGiHoSK
// SIG // 3cCXveNRN8Figw==
// SIG // End signature block
