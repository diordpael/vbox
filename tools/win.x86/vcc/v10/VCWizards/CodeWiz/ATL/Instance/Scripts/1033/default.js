// Script for WMI Instance provider wizard

function OnFinish(selProj, selObj)
{

    var oCM = selProj.CodeModel;
    try
    {   
    
        oCM.StartTransaction("Add WMI Instance provider");
    
                
        var bDLL;
        if (typeDynamicLibrary == selProj.Object.Configurations(1).ConfigurationType) //REVIEW: hardcoded configuration
            bDLL = true;
        else
            bDLL = false;
                
        wizard.AddSymbol("DLL_APP", bDLL);
        
        var strProjectName      = wizard.FindSymbol("PROJECT_NAME");
        wizard.AddSymbol("SAFE_IDL_NAME", CreateASCIIName(wizard.FindSymbol("PROJECT_NAME")));
        var strProjectPath      = wizard.FindSymbol("PROJECT_PATH");
        var strTemplatePath     = wizard.FindSymbol("TEMPLATES_PATH");
        var strShortName        = wizard.FindSymbol("SHORT_NAME");
        var strUpperShortName   = CreateASCIIName(strShortName.toUpperCase());
        wizard.AddSymbol("UPPER_SHORT_NAME", strUpperShortName);
                
        var strVIProgID         = wizard.FindSymbol("VERSION_INDEPENDENT_PROGID");
        wizard.AddSymbol("PROGID",  strVIProgID.substr(0,37) + ".1");
        var strClassName        = wizard.FindSymbol("CLASS_NAME");
        var strHeaderFile       = wizard.FindSymbol("HEADER_FILE");
        var strImplFile         = wizard.FindSymbol("IMPL_FILE");
        var strCoClass          = wizard.FindSymbol("COCLASS");
        var bAttributed = wizard.FindSymbol("ATTRIBUTED");
        
        var strProjectCPP       = GetProjectFile(selProj, "CPP", false, false);
        var strProjectRC        = GetProjectFile(selProj, "RC", true, false);
        
        var bClassSpecified = (wizard.FindSymbol("WMICLASSNAME").toString() != "");
        wizard.AddSymbol ("CLASS_SPECIFIED", bClassSpecified);
        if (bClassSpecified)
        {
            //add "dynamic" and "provider" qualifiers, if necessary
            
            var locator = new ActiveXObject("WbemScripting.SWbemLocator");
            var services = locator.ConnectServer("", //local server
                            wizard.FindSymbol("NAMESPACE"));                                                
            var classObj = services.Get(wizard.FindSymbol("WMICLASSNAME"));
            
            AddQualifier (classObj, 
                        "provider", 
                        wizard.FindSymbol("SHORT_NAME"),
                        false,
                        true,
                        true);
                            
            AddQualifier (classObj, 
                        "dynamic", 
                        true,
                        false,
                        true,
                        true);
                                                                                            
        }
        
        // Create necessary GUIDS
        CreateGUIDs();
                
        if (!bAttributed)
        {
            // Get LibName
            wizard.AddSymbol("LIB_NAME", oCM.IDLLibraries(1).Name);
    
            // Get LibID
            wizard.AddSymbol("LIBID_REGISTRY_FORMAT", oCM.IDLLibraries(1).Attributes("uuid").Value);
        
            // Get AppID
            var strAppID = wizard.GetAppID();
            if (strAppID.length > 0)
            {
                wizard.AddSymbol("APPID_EXIST", true);
                wizard.AddSymbol("APPID_REGISTRY_FORMAT", strAppID);
            }
            
            // add RGS file resource
            var strRGSFile = GetUniqueFileName(strProjectPath, CreateASCIIName(strShortName) + ".rgs");
            var strRGSID = "IDR_" + strUpperShortName;
            RenderAddTemplate(wizard, "wmiprov.rgs", strRGSFile, false);
    
            var oResHelper = wizard.ResourceHelper;

            oResHelper.OpenResourceFile(strProjectRC);
            //oResHelper.AddResource(strRGSID, strProjectPath + "\\" + strRGSFile, "REGISTRY");
            oResHelper.AddResource(strRGSID, strProjectPath + strRGSFile, "REGISTRY");
            oResHelper.CloseResourceFile(); 
                                                
            // Render wmiprovco.idl and insert into strProject.idl
            AddCoclassFromFile(oCM, "wmiprovco.idl");       
                        
            SetMergeProxySymbol(selProj);
        
        }

        // Call this function to set the QuerySupportLevelProperty
        SetQuerySupportLevels();
        
        // Add #include "strHeaderFile" to strProjectName.cpp
        if (!DoesIncludeExist(selProj, '"' + strHeaderFile + '"', strProjectCPP))
            oCM.AddInclude('"' + strHeaderFile + '"', strProjectCPP, vsCMAddPositionEnd);


        // Add header
        RenderAddTemplate(wizard, "wmiprov.h", strHeaderFile, selObj);
        
        // Add CPP
        RenderAddTemplate(wizard, "wmiprov.cpp", strImplFile, selObj);
    
        //Add MOF
        AddMOFFromFile("wmiprov.mof", selProj);
        
        //add wmi libraries to linker input
        var nTotal = selProj.Object.Configurations.Count;
        var nCntr;      
        for (nCntr = 1; nCntr <= nTotal; nCntr++)
        {
                    
            var VCLinkTool = selProj.Object.Configurations(nCntr).Tools("VCLinkerTool");
            if (-1 == VCLinkTool.AdditionalDependencies.toUpperCase().search("WBEMUUID.LIB"))
            {
                VCLinkTool.AdditionalDependencies += " wbemuuid.lib";
            }
            if (-1 == VCLinkTool.AdditionalDependencies.toUpperCase().search("WMIUTILS.LIB"))
            {
                VCLinkTool.AdditionalDependencies += " wmiutils.lib";
            }
        }
        
        oCM.CommitTransaction();
                    
    }
    catch(e)
    {
        oCM.AbortTransaction();
        var L_alert1_ErrorMessage = "Error in OnFinish: ";
        wizard.ReportError( L_alert1_ErrorMessage + e.description);
    }
    return;
}// end function OnFinsh


function AddMOFFromFile (strTemplateName, selProj)
{
    // if project.mof doesn't exist in the project already create one
    // append the mof for the new class at the end of the new or existing mof

    try
    {
        var ForReading = 1, ForWriting = 2, ForAppending = 8;
            var TristateUseDefault = -2, TristateUnicode = -1, TristateAnsi = 0;
    
        var fso = new ActiveXObject("Scripting.FileSystemObject");
        var strMOFName = wizard.FindSymbol("PROJECT_NAME") + ".mof";
        
        var strMOFPath = wizard.FindSymbol("PROJECT_PATH") + strMOFName;        
        
        var bFirstRun = !fso.FileExists(strMOFPath);


        if (bFirstRun)
        {
            // create the initial mof file with header and add it to sources "folder" in project
        
            var folder;         
            //add MOF filter to "Source Files"                  
            for( var n = 1 ; n <= selProj.Object.Filters.Count ; n++)
            {
                folder = selProj.Object.Filters.Item(n);
                if( folder.Filter.match( "cpp" ) != null  )
                {
                    folder.Filter = folder.Filter + ";mof"
                    break;
                }
            }
                        
            RenderAddTemplate(wizard, "header.mof", strMOFName, false);

            // Close the opened .mof file, so that there is no warning message
            // for opened modified file
            CloseProjFile(strMOFName,selProj);
        }
        
        // now the header is in the strMOFName file within the project
        // or from the past the header is there with one or more classes
        // build the information for new mof for the new class and append

        
        if (wizard.FindSymbol("CLASS_SPECIFIED") == true)
        {
            var strEscapedNS = BuildEscapedNS();    
            wizard.AddSymbol("ESCAPED_NAMESPACE", strEscapedNS);    
        }
        
        RenderAddTemplate(wizard, strTemplateName, strMOFName, false);    
        
        if(bFirstRun)
        {      
            AddFileToProject(strMOFName,selProj.ProjectItems ,true)                 

            //set custom build step for the MOF file in each configuration
            
            var files = selProj.Object.Files;
            var Moffile = files(strMOFName);

            var nTotal = Moffile.FileConfigurations.Count;          
            var nCntr;      
            for (nCntr = 1; nCntr <= nTotal; nCntr++)
            {               
                var customBuildTool = Moffile.FileConfigurations(nCntr).Tool;
                                
                customBuildTool.CommandLine = "mofcomp \"%(RootDir)%(Directory)\\%(Filename)\".mof\n\recho mofcomptrace > \"$(OutDir)\\%(FileName)\".trace";
                
                customBuildTool.Outputs  =  "\"$(OutDir)\\%(Filename)\".trace";    
                
                var L_alert10_ErrorMessage = "Compiling MOF file";
                customBuildTool.Description = L_alert10_ErrorMessage;                                   
            }           
        }                               
        // To Close the opened .mof file, so that there is no warning message
        // for opened modified file
        CloseProjFile(strMOFName, selProj);

    }
    catch(e)
    {
        var L_alert2_ErrorMessage = "Error in AddMOFFile: ";
        wizard.ReportError(L_alert2_ErrorMessage + e.description);
    }
    return;
}// end function AddMOFFromFile

// helpers

function ProjFileOpen(strFileName, selProj)
{
    // strFileName does not contain path - just simple name
    var bFileOpened = false;
    // Check if MOF file is already opened
    var activeDoc;
    var nDocs;
    for(nDocs = 1 ; nDocs <= selProj.DTE.Documents.Count ; nDocs++)
    {
        activeDoc =selProj.DTE.Documents.Item(nDocs);
        if(activeDoc.Name == strFileName)
        {
            bFileOpened = true;
            break;
        }
    }
    return bFileOpened;
}

function CloseProjFile(strFileName, selProj)
{
    // close an open project file by name
    // strFileName does not contain path - just simple name
    var activeDoc;
    var nDocs;
    for(nDocs = 1 ; nDocs <= selProj.DTE.Documents.Count ; nDocs++)
    {
        activeDoc =selProj.DTE.Documents.Item(nDocs);
        if(activeDoc.Name == strFileName)
        {
            activeDoc.Close();
            break;
        }
    }
    return;
}

function BuildEscapedNS()
{
    //create ESCAPED_NAMESPACE symbol that would have 2 backslashes in
    //complex namespace names

    var strEscapedNS = "";
    var strNS = wizard.FindSymbol("NAMESPACE");
    var arPieces = strNS.split("\\");               

    if (arPieces.length == 0)
    {
        strEscapedNS = strNS;
    }
    else 
    {           
        for (var i = 0; i < arPieces.length - 1; i++)
        {
            strEscapedNS += arPieces[i] + "\\\\";
        }                                       
        strEscapedNS += arPieces[arPieces.length - 1];          
    }
    return strEscapedNS;
}

function CreateGUIDs()
{
    try
    {
        // create CLSID
        var strRawGUID = wizard.CreateGuid();
        var strFormattedGUID = wizard.FormatGuid(strRawGUID, 0);
        wizard.AddSymbol("CLSID_REGISTRY_FORMAT", strFormattedGUID);

        
    }
    catch(e)
    {
        var L_alert3_ErrorMessage = "Error in CreateGUIDs(): ";
        wizard.ReportError(L_alert3_ErrorMessage + e.description);
    }
}

function AddQualifier (classObj, 
                            qualName, 
                            qualValue,
                            bToSubClasses,
                            bToInstances,
                            bOverridable)
{
    try 
    {   
        var theQual;
        try
        {
            theQual = classObj.Qualifiers_(qualName);
            theQual.Value = qualValue;
        }
        catch (e)
        {
            //if (e.number == wbemErrNotFound)  //qualifier not found
            //{
            if (e.number == -2147217406)  //qualifier not found
            {
                theQual = classObj.Qualifiers_.Add(qualName, 
                                                    qualValue, 
                                                    bToSubClasses,
                                                    bToInstances,
                                                    bOverridable);
            }
            else
            {
                throw(e);   //re-throw
            }
        }
        
        //commit changes to WMI
        classObj.Put_();
    }
    catch (e)
    {
        var L_alert4_ErrorMessage = "Error in AddQualifier(): ";
        wizard.ReportError (L_alert4_ErrorMessage + e.description);
    }

}


function IsWMIOk (selProj, selObj)
{
    //uses WMI Registry provider to determine WMI version
        
    try 
    {
        if (!CanAddATLClass(selProj, selObj))
        {
            return false;
        }
        try 
        {
            var locator = new ActiveXObject("WbemScripting.SWbemLocator");
        
            var services = locator.ConnectServer("", //local server
                                        "root\\default");                                               
        }
        catch (e)
        {
            var L_alert5_ErrorMessage = "Could not connect to WMI. Please verify that WMI is installed on your machine";
            throw(L_alert5_ErrorMessage);
        }
                
        var classObj = services.Get("StdRegProv");
                
        var methObject = classObj.Methods_.Item("GetExpandedStringValue");
        var inParmsInstance = methObject.InParameters.SpawnInstance_();
                
        inParmsInstance.Properties_.Add("sSubKeyName", 8);
    
        inParmsInstance.Properties_("sSubKeyName").Value = 
            "SOFTWARE\\Microsoft\\WBEM";
    

        inParmsInstance.Properties_.Add("sValueName", 8);
    
        inParmsInstance.Properties_("sValueName").Value = "Build";


        var outParms = classObj.ExecMethod_("GetExpandedStringValue",
                                            inParmsInstance);
                                            
        if (outParms.Properties_("ReturnValue").Value != 0)
        {
            var L_alert6_ErrorMessage = "Could not determine WMI build number";
            throw (L_alert6_ErrorMessage);
        }

        if (outParms.Properties_("sValue").Value >= "1085")
        {
            return true;
        }
        else 
        {   
            var L_alert7_ErrorMessage = "Version of WMI installed on your machine is ";
            var L_alert8_ErrorMessage = ". Version 1085 or higher is required to run this wizard.";
            throw (L_alert7_ErrorMessage + outParms.Properties_("sValue").Value +
                    L_alert8_ErrorMessage);
        }
    }

    catch (e)
    {
        var L_alert9_ErrorMessage = "Cannot add WMI Instance provider object to your project due to the following error:\n\r";
        wizard.ReportError (L_alert9_ErrorMessage + e);
        return false;
    }

    
}

function SetQuerySupportLevels()
{
    var strQuerySupportLevels="";
    var bIsFirst = true;
    
    if(wizard.FindSymbol("QUERY_ASSOCIATORS").toString().toUpperCase() == "TRUE")
    {
        strQuerySupportLevels += "\"WQL:Associators\"";
        bIsFirst = false;
    }

    if(wizard.FindSymbol("QUERY_CUSTOM").toString().toUpperCase() == "TRUE")
    {
        if(bIsFirst == false)
        {
            strQuerySupportLevels += ",";
        }
        strQuerySupportLevels += "\"WQL:V1ProviderDefined\"";
        bIsFirst = false;
    }

    if(wizard.FindSymbol("QUERY_UNARY").toString().toUpperCase() == "TRUE")
    {
        if(bIsFirst == false)
        {
            strQuerySupportLevels += ",";
        }
        strQuerySupportLevels += "\"WQL:UnarySelect\"";
        bIsFirst = false;
    }

    if(wizard.FindSymbol("QUERY_REFERENCES").toString().toUpperCase() == "TRUE")
    {
        if(bIsFirst == false)
        {
            strQuerySupportLevels += ",";
        }
        strQuerySupportLevels += "\"WQL:References\"";
        bIsFirst = false;
    }
    
    if(bIsFirst == false)
    {
        wizard.AddSymbol("QUERYSUPPORTS_LEVEL",strQuerySupportLevels);
        wizard.AddSymbol("SUPPORTS_QUERY",true);
    }

    
}


// SIG // Begin signature block
// SIG // MIIXOgYJKoZIhvcNAQcCoIIXKzCCFycCAQExCzAJBgUr
// SIG // DgMCGgUAMGcGCisGAQQBgjcCAQSgWTBXMDIGCisGAQQB
// SIG // gjcCAR4wJAIBAQQQEODJBs441BGiowAQS9NQkAIBAAIB
// SIG // AAIBAAIBAAIBADAhMAkGBSsOAwIaBQAEFEJLKuBspDp2
// SIG // iNqR3hymPAhBcPH7oIISMTCCBGAwggNMoAMCAQICCi6r
// SIG // EdxQ/1ydy8AwCQYFKw4DAh0FADBwMSswKQYDVQQLEyJD
// SIG // b3B5cmlnaHQgKGMpIDE5OTcgTWljcm9zb2Z0IENvcnAu
// SIG // MR4wHAYDVQQLExVNaWNyb3NvZnQgQ29ycG9yYXRpb24x
// SIG // ITAfBgNVBAMTGE1pY3Jvc29mdCBSb290IEF1dGhvcml0
// SIG // eTAeFw0wNzA4MjIyMjMxMDJaFw0xMjA4MjUwNzAwMDBa
// SIG // MHkxCzAJBgNVBAYTAlVTMRMwEQYDVQQIEwpXYXNoaW5n
// SIG // dG9uMRAwDgYDVQQHEwdSZWRtb25kMR4wHAYDVQQKExVN
// SIG // aWNyb3NvZnQgQ29ycG9yYXRpb24xIzAhBgNVBAMTGk1p
// SIG // Y3Jvc29mdCBDb2RlIFNpZ25pbmcgUENBMIIBIjANBgkq
// SIG // hkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAt3l91l2zRTmo
// SIG // NKwx2vklNUl3wPsfnsdFce/RRujUjMNrTFJi9JkCw03Y
// SIG // SWwvJD5lv84jtwtIt3913UW9qo8OUMUlK/Kg5w0jH9FB
// SIG // JPpimc8ZRaWTSh+ZzbMvIsNKLXxv2RUeO4w5EDndvSn0
// SIG // ZjstATL//idIprVsAYec+7qyY3+C+VyggYSFjrDyuJSj
// SIG // zzimUIUXJ4dO3TD2AD30xvk9gb6G7Ww5py409rQurwp9
// SIG // YpF4ZpyYcw2Gr/LE8yC5TxKNY8ss2TJFGe67SpY7UFMY
// SIG // zmZReaqth8hWPp+CUIhuBbE1wXskvVJmPZlOzCt+M26E
// SIG // RwbRntBKhgJuhgCkwIffUwIDAQABo4H6MIH3MBMGA1Ud
// SIG // JQQMMAoGCCsGAQUFBwMDMIGiBgNVHQEEgZowgZeAEFvQ
// SIG // cO9pcp4jUX4Usk2O/8uhcjBwMSswKQYDVQQLEyJDb3B5
// SIG // cmlnaHQgKGMpIDE5OTcgTWljcm9zb2Z0IENvcnAuMR4w
// SIG // HAYDVQQLExVNaWNyb3NvZnQgQ29ycG9yYXRpb24xITAf
// SIG // BgNVBAMTGE1pY3Jvc29mdCBSb290IEF1dGhvcml0eYIP
// SIG // AMEAizw8iBHRPvZj7N9AMA8GA1UdEwEB/wQFMAMBAf8w
// SIG // HQYDVR0OBBYEFMwdznYAcFuv8drETppRRC6jRGPwMAsG
// SIG // A1UdDwQEAwIBhjAJBgUrDgMCHQUAA4IBAQB7q65+Siby
// SIG // zrxOdKJYJ3QqdbOG/atMlHgATenK6xjcacUOonzzAkPG
// SIG // yofM+FPMwp+9Vm/wY0SpRADulsia1Ry4C58ZDZTX2h6t
// SIG // KX3v7aZzrI/eOY49mGq8OG3SiK8j/d/p1mkJkYi9/uEA
// SIG // uzTz93z5EBIuBesplpNCayhxtziP4AcNyV1ozb2AQWtm
// SIG // qLu3u440yvIDEHx69dLgQt97/uHhrP7239UNs3DWkuNP
// SIG // tjiifC3UPds0C2I3Ap+BaiOJ9lxjj7BauznXYIxVhBoz
// SIG // 9TuYoIIMol+Lsyy3oaXLq9ogtr8wGYUgFA0qvFL0QeBe
// SIG // MOOSKGmHwXDi86erzoBCcnYOMIIEejCCA2KgAwIBAgIK
// SIG // YQHPPgAAAAAADzANBgkqhkiG9w0BAQUFADB5MQswCQYD
// SIG // VQQGEwJVUzETMBEGA1UECBMKV2FzaGluZ3RvbjEQMA4G
// SIG // A1UEBxMHUmVkbW9uZDEeMBwGA1UEChMVTWljcm9zb2Z0
// SIG // IENvcnBvcmF0aW9uMSMwIQYDVQQDExpNaWNyb3NvZnQg
// SIG // Q29kZSBTaWduaW5nIFBDQTAeFw0wOTEyMDcyMjQwMjla
// SIG // Fw0xMTAzMDcyMjQwMjlaMIGDMQswCQYDVQQGEwJVUzET
// SIG // MBEGA1UECBMKV2FzaGluZ3RvbjEQMA4GA1UEBxMHUmVk
// SIG // bW9uZDEeMBwGA1UEChMVTWljcm9zb2Z0IENvcnBvcmF0
// SIG // aW9uMQ0wCwYDVQQLEwRNT1BSMR4wHAYDVQQDExVNaWNy
// SIG // b3NvZnQgQ29ycG9yYXRpb24wggEiMA0GCSqGSIb3DQEB
// SIG // AQUAA4IBDwAwggEKAoIBAQC9MIn7RXKoU2ueiU8AI8C+
// SIG // 1B09sVlAOPNzkYIm5pYSAFPZHIIOPM4du733Qo2X1Pw4
// SIG // GuS5+ePs02EDv6DT1nVNXEap7V7w0uJpWxpz6rMcjQTN
// SIG // KUSgZFkvHphdbserGDmCZcSnvKt1iBnqh5cUJrN/Jnak
// SIG // 1Dg5hOOzJtUY+Svp0skWWlQh8peNh4Yp/vRJLOaL+AQ/
// SIG // fc3NlpKGDXED4tD+DEI1/9e4P92ORQp99tdLrVvwdnId
// SIG // dyN9iTXEHF2yUANLR20Hp1WImAaApoGtVE7Ygdb6v0LA
// SIG // Mb5VDZnVU0kSMOvlpYh8XsR6WhSHCLQ3aaDrMiSMCOv5
// SIG // 1BS64PzN6qQVAgMBAAGjgfgwgfUwEwYDVR0lBAwwCgYI
// SIG // KwYBBQUHAwMwHQYDVR0OBBYEFDh4BXPIGzKbX5KGVa+J
// SIG // usaZsXSOMA4GA1UdDwEB/wQEAwIHgDAfBgNVHSMEGDAW
// SIG // gBTMHc52AHBbr/HaxE6aUUQuo0Rj8DBEBgNVHR8EPTA7
// SIG // MDmgN6A1hjNodHRwOi8vY3JsLm1pY3Jvc29mdC5jb20v
// SIG // cGtpL2NybC9wcm9kdWN0cy9DU1BDQS5jcmwwSAYIKwYB
// SIG // BQUHAQEEPDA6MDgGCCsGAQUFBzAChixodHRwOi8vd3d3
// SIG // Lm1pY3Jvc29mdC5jb20vcGtpL2NlcnRzL0NTUENBLmNy
// SIG // dDANBgkqhkiG9w0BAQUFAAOCAQEAKAODqxMN8f4Rb0J2
// SIG // 2EOruMZC+iRlNK51sHEwjpa2g/py5P7NN+c6cJhRIA66
// SIG // cbTJ9NXkiugocHPV7eHCe+7xVjRagILrENdyA+oSTuzd
// SIG // DYx7RE8MYXX9bpwH3c4rWhgNObBg/dr/BKoCo9j6jqO7
// SIG // vcFqVDsxX+QsbsvxTSoc8h52e4avxofWsSrtrMwOwOSf
// SIG // f+jP6IRyVIIYbirInpW0Gh7Bb5PbYqbBS2utye09kuOy
// SIG // L6t6dzlnagB7gp0DEN5jlUkmQt6VIsGHC9AUo1/cczJy
// SIG // Nh7/yCnFJFJPZkjJHR2pxSY5aVBOp+zCBmwuchvxIdpt
// SIG // JEiAgRVAfJ/MdDhKTzCCBJ0wggOFoAMCAQICEGoLmU/A
// SIG // ACWrEdtFH1h6Z6IwDQYJKoZIhvcNAQEFBQAwcDErMCkG
// SIG // A1UECxMiQ29weXJpZ2h0IChjKSAxOTk3IE1pY3Jvc29m
// SIG // dCBDb3JwLjEeMBwGA1UECxMVTWljcm9zb2Z0IENvcnBv
// SIG // cmF0aW9uMSEwHwYDVQQDExhNaWNyb3NvZnQgUm9vdCBB
// SIG // dXRob3JpdHkwHhcNMDYwOTE2MDEwNDQ3WhcNMTkwOTE1
// SIG // MDcwMDAwWjB5MQswCQYDVQQGEwJVUzETMBEGA1UECBMK
// SIG // V2FzaGluZ3RvbjEQMA4GA1UEBxMHUmVkbW9uZDEeMBwG
// SIG // A1UEChMVTWljcm9zb2Z0IENvcnBvcmF0aW9uMSMwIQYD
// SIG // VQQDExpNaWNyb3NvZnQgVGltZXN0YW1waW5nIFBDQTCC
// SIG // ASIwDQYJKoZIhvcNAQEBBQADggEPADCCAQoCggEBANw3
// SIG // bvuvyEJKcRjIzkg+U8D6qxS6LDK7Ek9SyIPtPjPZSTGS
// SIG // KLaRZOAfUIS6wkvRfwX473W+i8eo1a5pcGZ4J2botrfv
// SIG // hbnN7qr9EqQLWSIpL89A2VYEG3a1bWRtSlTb3fHev5+D
// SIG // x4Dff0wCN5T1wJ4IVh5oR83ZwHZcL322JQS0VltqHGP/
// SIG // gHw87tUEJU05d3QHXcJc2IY3LHXJDuoeOQl8dv6dbG56
// SIG // 4Ow+j5eecQ5fKk8YYmAyntKDTisiXGhFi94vhBBQsvm1
// SIG // Go1s7iWbE/jLENeFDvSCdnM2xpV6osxgBuwFsIYzt/iU
// SIG // W4RBhFiFlG6wHyxIzG+cQ+Bq6H8mjmsCAwEAAaOCASgw
// SIG // ggEkMBMGA1UdJQQMMAoGCCsGAQUFBwMIMIGiBgNVHQEE
// SIG // gZowgZeAEFvQcO9pcp4jUX4Usk2O/8uhcjBwMSswKQYD
// SIG // VQQLEyJDb3B5cmlnaHQgKGMpIDE5OTcgTWljcm9zb2Z0
// SIG // IENvcnAuMR4wHAYDVQQLExVNaWNyb3NvZnQgQ29ycG9y
// SIG // YXRpb24xITAfBgNVBAMTGE1pY3Jvc29mdCBSb290IEF1
// SIG // dGhvcml0eYIPAMEAizw8iBHRPvZj7N9AMBAGCSsGAQQB
// SIG // gjcVAQQDAgEAMB0GA1UdDgQWBBRv6E4/l7k0q0uGj7yc
// SIG // 6qw7QUPG0DAZBgkrBgEEAYI3FAIEDB4KAFMAdQBiAEMA
// SIG // QTALBgNVHQ8EBAMCAYYwDwYDVR0TAQH/BAUwAwEB/zAN
// SIG // BgkqhkiG9w0BAQUFAAOCAQEAlE0RMcJ8ULsRjqFhBwEO
// SIG // jHBFje9zVL0/CQUt/7hRU4Uc7TmRt6NWC96Mtjsb0fus
// SIG // p8m3sVEhG28IaX5rA6IiRu1stG18IrhG04TzjQ++B4o2
// SIG // wet+6XBdRZ+S0szO3Y7A4b8qzXzsya4y1Ye5y2PENtEY
// SIG // Ib923juasxtzniGI2LS0ElSM9JzCZUqaKCacYIoPO8cT
// SIG // ZXhIu8+tgzpPsGJY3jDp6Tkd44ny2jmB+RMhjGSAYwYE
// SIG // lvKaAkMve0aIuv8C2WX5St7aA3STswVuDMyd3ChhfEjx
// SIG // F5wRITgCHIesBsWWMrjlQMZTPb2pid7oZjeN9CKWnMyw
// SIG // d1RROtZyRLIj9jCCBKowggOSoAMCAQICCmEGlC0AAAAA
// SIG // AAkwDQYJKoZIhvcNAQEFBQAweTELMAkGA1UEBhMCVVMx
// SIG // EzARBgNVBAgTCldhc2hpbmd0b24xEDAOBgNVBAcTB1Jl
// SIG // ZG1vbmQxHjAcBgNVBAoTFU1pY3Jvc29mdCBDb3Jwb3Jh
// SIG // dGlvbjEjMCEGA1UEAxMaTWljcm9zb2Z0IFRpbWVzdGFt
// SIG // cGluZyBQQ0EwHhcNMDgwNzI1MTkwMjE3WhcNMTMwNzI1
// SIG // MTkxMjE3WjCBszELMAkGA1UEBhMCVVMxEzARBgNVBAgT
// SIG // Cldhc2hpbmd0b24xEDAOBgNVBAcTB1JlZG1vbmQxHjAc
// SIG // BgNVBAoTFU1pY3Jvc29mdCBDb3Jwb3JhdGlvbjENMAsG
// SIG // A1UECxMETU9QUjEnMCUGA1UECxMebkNpcGhlciBEU0Ug
// SIG // RVNOOjdBODItNjg4QS05RjkyMSUwIwYDVQQDExxNaWNy
// SIG // b3NvZnQgVGltZS1TdGFtcCBTZXJ2aWNlMIIBIjANBgkq
// SIG // hkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAlYEKIEIYUXrZ
// SIG // le2b/dyH0fsOjxPqqjcoEnb+TVCrdpcqk0fgqVZpAuWU
// SIG // fk2F239x73UA27tDbPtvrHHwK9F8ks6UF52hxbr5937d
// SIG // YeEtMB6cJi12P+ZGlo6u2Ik32Mzv889bw/xo4PJkj5vo
// SIG // wxL5o76E/NaLzgU9vQF2UCcD+IS3FoaNYL5dKSw8z6X9
// SIG // mFo1HU8WwDjYHmE/PTazVhQVd5U7EPoAsJPiXTerJ7tj
// SIG // LEgUgVXjbOqpK5WNiA5+owCldyQHmCpwA7gqJJCa3sWi
// SIG // Iku/TFkGd1RyQ7A+ZN2ThAhYtv7ph0kJNrOz+DOpfkyi
// SIG // eX8yWSkOnrX14DyeP+xGOwIDAQABo4H4MIH1MB0GA1Ud
// SIG // DgQWBBQolYi/Ajvr2pS6fUYP+sv0fp3/0TAfBgNVHSME
// SIG // GDAWgBRv6E4/l7k0q0uGj7yc6qw7QUPG0DBEBgNVHR8E
// SIG // PTA7MDmgN6A1hjNodHRwOi8vY3JsLm1pY3Jvc29mdC5j
// SIG // b20vcGtpL2NybC9wcm9kdWN0cy90c3BjYS5jcmwwSAYI
// SIG // KwYBBQUHAQEEPDA6MDgGCCsGAQUFBzAChixodHRwOi8v
// SIG // d3d3Lm1pY3Jvc29mdC5jb20vcGtpL2NlcnRzL3RzcGNh
// SIG // LmNydDATBgNVHSUEDDAKBggrBgEFBQcDCDAOBgNVHQ8B
// SIG // Af8EBAMCBsAwDQYJKoZIhvcNAQEFBQADggEBAADurPzi
// SIG // 0ohmyinjWrnNAIJ+F1zFJFkSu6j3a9eH/o3LtXYfGyL2
// SIG // 9+HKtLlBARo3rUg3lnD6zDOnKIy4C7Z0Eyi3s3XhKgni
// SIG // i0/fmD+XtzQSgeoQ3R3cumTPTlA7TIr9Gd0lrtWWh+pL
// SIG // xOXw+UEXXQHrV4h9dnrlb/6HIKyTnIyav18aoBUwJOCi
// SIG // fmGRHSkpw0mQOkODie7e1YPdTyw1O+dBQQGqAAwL8tZJ
// SIG // G85CjXuw8y2NXSnhvo1/kRV2tGD7FCeqbxJjQihYOoo7
// SIG // i0Dkt8XMklccRlZrj8uSTVYFAMr4MEBFTt8ZiL31EPDd
// SIG // Gt8oHrRR8nfgJuO7CYES3B460EUxggR1MIIEcQIBATCB
// SIG // hzB5MQswCQYDVQQGEwJVUzETMBEGA1UECBMKV2FzaGlu
// SIG // Z3RvbjEQMA4GA1UEBxMHUmVkbW9uZDEeMBwGA1UEChMV
// SIG // TWljcm9zb2Z0IENvcnBvcmF0aW9uMSMwIQYDVQQDExpN
// SIG // aWNyb3NvZnQgQ29kZSBTaWduaW5nIFBDQQIKYQHPPgAA
// SIG // AAAADzAJBgUrDgMCGgUAoIGgMBkGCSqGSIb3DQEJAzEM
// SIG // BgorBgEEAYI3AgEEMBwGCisGAQQBgjcCAQsxDjAMBgor
// SIG // BgEEAYI3AgEVMCMGCSqGSIb3DQEJBDEWBBSrNfoA3JOt
// SIG // cLjjjQq8itdYUkl9yTBABgorBgEEAYI3AgEMMTIwMKAW
// SIG // gBQAZABlAGYAYQB1AGwAdAAuAGoAc6EWgBRodHRwOi8v
// SIG // bWljcm9zb2Z0LmNvbTANBgkqhkiG9w0BAQEFAASCAQCR
// SIG // 1Sb8e79koOwv/BqiuWYXQLI0YnGYCteSMX6hjxKwGgE0
// SIG // v/pUNhkyGDlYhaw1vBmz5jZLcR+opsTfHvDmgL+ti0so
// SIG // IkA0eCv7AWd9VkbRvRN2Kz8VcaDaybXmANlPiVROM7jl
// SIG // 5tS3R8doM/G82Uagm+WQNacx6oenJvGkAzzyCj/xjlxL
// SIG // JOWmry5waqDDgF0HPO2w4RKCNRfNrBeXB7x9MINY7z9m
// SIG // RGINdOXOy9w9cUMtOafqDoj1P8ma+j1ly2jdlwsiW15r
// SIG // 4tOR8PQmtrtY8xO7P5N8/n7iqVp4M4dnuc0lap5QrFyP
// SIG // OZ9WOU0ud5TDr5N4ALbX1CFCsxCuLUpBoYICHzCCAhsG
// SIG // CSqGSIb3DQEJBjGCAgwwggIIAgEBMIGHMHkxCzAJBgNV
// SIG // BAYTAlVTMRMwEQYDVQQIEwpXYXNoaW5ndG9uMRAwDgYD
// SIG // VQQHEwdSZWRtb25kMR4wHAYDVQQKExVNaWNyb3NvZnQg
// SIG // Q29ycG9yYXRpb24xIzAhBgNVBAMTGk1pY3Jvc29mdCBU
// SIG // aW1lc3RhbXBpbmcgUENBAgphBpQtAAAAAAAJMAcGBSsO
// SIG // AwIaoF0wGAYJKoZIhvcNAQkDMQsGCSqGSIb3DQEHATAc
// SIG // BgkqhkiG9w0BCQUxDxcNMTAwMzE5MTMwODE2WjAjBgkq
// SIG // hkiG9w0BCQQxFgQULSxAYefck/JHuyLO6hKF/UByMg8w
// SIG // DQYJKoZIhvcNAQEFBQAEggEAjPwzYf5bnmtqCtxK59Sd
// SIG // Bb0CqeHTDHMLlolTJ5NS6dm+KaZDhyGBB8XVqTaFdiYP
// SIG // i1Lf848E34YJy96n0PsJUrbWgQW6NDTc/MsOO6PPoVO0
// SIG // 93e1465UfUOp7wdtTwoJ54Fo6fMLrMvUTkWLBbcxOoYh
// SIG // lo7FH9Xlp7YNfc8dO4VYbTxFBeElmVqC+kiPPWvtJaRl
// SIG // QlGjq7eDIHZ8R2c+IaD5G9gKtCXDnHxUKMvdlbH7LhFG
// SIG // nzIFbqzabrF6gq+MJ85FMDZkRE5DDIW31I8kGhs/PtHx
// SIG // Q6ZJVKLc1c9RFBB44Do+t58X5uz1lbEDI1qWKzPrS4Zj
// SIG // M838i/VTUaFb0Q==
// SIG // End signature block
