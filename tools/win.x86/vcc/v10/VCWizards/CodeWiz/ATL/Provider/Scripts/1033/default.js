// Copyright (c) Microsoft Corporation. All rights reserved.
// Script for ATL Provider

function OnPrep(selProj, selObj)
{
	var L_WizardDialogTitle_Text = "ATL OLE DB Provider Wizard";
	return PrepCodeWizard(selProj, L_WizardDialogTitle_Text);
}

function OnFinish(selProj, selObj)
{
	var oCM;
	try
	{		
		oCM	= selProj.CodeModel;

		var strShortName = wizard.FindSymbol("SHORT_NAME");
		var L_TransactionName_Text = "Add ATL Provider ";
		oCM.StartTransaction(L_TransactionName_Text + strShortName);
		if(!AddATLSupportToProject(selProj))
		{
			oCM.AbortTransaction();
			return;
		}

		var strProjectPath		= wizard.FindSymbol("PROJECT_PATH");
		var strTemplatePath		= wizard.FindSymbol("TEMPLATES_PATH");
		var strUpperShortName	= CreateASCIIName(strShortName.toUpperCase());
		wizard.AddSymbol("UPPER_SHORT_NAME", strUpperShortName);
		wizard.AddSymbol("TYPE_NAME", strShortName + " Class");
		var strVIProgID			= wizard.FindSymbol("VERSION_INDEPENDENT_PROGID");
		if (strVIProgID == null || strVIProgID == "")
		{
			wizard.AddSymbol("PROGID_VALID", false);
			wizard.AddSymbol("VERSION_INDEPENDENT_PROGID","");
			wizard.AddSymbol("PROGID","");
		}
		else
		{
			wizard.AddSymbol("PROGID_VALID", true);
			wizard.AddSymbol("PROGID", strVIProgID + "." + wizard.FindSymbol("VERSION"));
		}
		var strCoClass			= wizard.FindSymbol("COCLASS");
		var bAttributed			= wizard.FindSymbol("ATTRIBUTED");

		var strDataSourceHeader	= wizard.FindSymbol("DATASOURCE_HEADER");
		var strSessionHeader	= wizard.FindSymbol("SESSION_HEADER");
		var strRowsetHeader		= wizard.FindSymbol("ROWSET_HEADER");
		var strRowsetImpl		= wizard.FindSymbol("ROWSET_IMPL");
		var strDataSourceClass	= wizard.FindSymbol("DATASOURCE_CLASS");

		var strProjectRC		= GetProjectFile(selProj, "RC", true, false);
		var strRGSFile;

		// Create necessary GUIDS
		CreateGUIDs();

		var oResHelper = wizard.ResourceHelper;
		oResHelper.OpenResourceFile(strProjectRC);

		// add string resources
		var L_RCTEXT1_TEXT =  "Active Sessions";
		var strSymbolValue = oResHelper.AddResource("IDS_DBPROP_ACTIVESESSIONS", L_RCTEXT1_TEXT, "STRING");
		// In theory we should check after each addition. In practive however this check guards agains SCC chekout cancelations.
		// This will either fire in the first attempt or will not fire, so furhter checks are unnecessary.
		if (strSymbolValue == null) return;
		var L_RCTEXT2_TEXT =  "Asynchable Commit";
		oResHelper.AddResource("IDS_DBPROP_ASYNCTXNCOMMIT", L_RCTEXT2_TEXT, "STRING");
		var L_RCTEXT3_TEXT =  "Pass By Ref Accessors";
		oResHelper.AddResource("IDS_DBPROP_BYREFACCESSORS", L_RCTEXT3_TEXT, "STRING");
		var L_RCTEXT4_TEXT =  "Catalog Location";
		oResHelper.AddResource("IDS_DBPROP_CATALOGLOCATION", L_RCTEXT4_TEXT, "STRING");
		var L_RCTEXT5_TEXT =  "Catalog Term";
		oResHelper.AddResource("IDS_DBPROP_CATALOGTERM", L_RCTEXT5_TEXT, "STRING");
		var L_RCTEXT6_TEXT =  "Catalog Usage";
		oResHelper.AddResource("IDS_DBPROP_CATALOGUSAGE", L_RCTEXT6_TEXT, "STRING");
		var L_RCTEXT7_TEXT =  "Column Definition";
		oResHelper.AddResource("IDS_DBPROP_COLUMNDEFINITION", L_RCTEXT7_TEXT, "STRING");
		var L_RCTEXT8_TEXT =  "NULL Concatenation Behavior";
		oResHelper.AddResource("IDS_DBPROP_CONCATNULLBEHAVIOR", L_RCTEXT8_TEXT, "STRING");
		var L_RCTEXT9_TEXT =  "Data Source Name";
		oResHelper.AddResource("IDS_DBPROP_DATASOURCENAME", L_RCTEXT9_TEXT, "STRING");
		var L_RCTEXT10_TEXT =  "Read-Only Data Source";
		oResHelper.AddResource("IDS_DBPROP_DATASOURCEREADONLY", L_RCTEXT10_TEXT, "STRING");
		var L_RCTEXT11_TEXT =  "DBMS Name";
		oResHelper.AddResource("IDS_DBPROP_DBMSNAME", L_RCTEXT11_TEXT, "STRING");
		var L_RCTEXT12_TEXT =  "DBMS Version";
		oResHelper.AddResource("IDS_DBPROP_DBMSVER", L_RCTEXT12_TEXT, "STRING");
		var L_RCTEXT13_TEXT =  "Procedure Term";
		oResHelper.AddResource("IDS_DBPROP_PROCEDURETERM", L_RCTEXT13_TEXT, "STRING");
		var L_RCTEXT14_TEXT =  "OLE DB Version";
		oResHelper.AddResource("IDS_DBPROP_PROVIDEROLEDBVER", L_RCTEXT14_TEXT, "STRING");
		var L_RCTEXT15_TEXT =  "Provider Name";
		oResHelper.AddResource("IDS_DBPROP_PROVIDERNAME", L_RCTEXT15_TEXT, "STRING");
		var L_RCTEXT16_TEXT =  "Provider Version";
		oResHelper.AddResource("IDS_DBPROP_PROVIDERVER", L_RCTEXT16_TEXT, "STRING");
		var L_RCTEXT17_TEXT =  "Quoted Identifier Sensitivity";
		oResHelper.AddResource("IDS_DBPROP_QUOTEDIDENTIFIERCASE", L_RCTEXT17_TEXT, "STRING");
		var L_RCTEXT18_TEXT =  "Schema Term";
		oResHelper.AddResource("IDS_DBPROP_SCHEMATERM", L_RCTEXT18_TEXT, "STRING");
		var L_RCTEXT19_TEXT =  "Schema Usage";
		oResHelper.AddResource("IDS_DBPROP_SCHEMAUSAGE", L_RCTEXT19_TEXT, "STRING");
		var L_RCTEXT20_TEXT =  "SQL Support";
		oResHelper.AddResource("IDS_DBPROP_SQLSUPPORT", L_RCTEXT20_TEXT, "STRING");
		var L_RCTEXT21_TEXT =  "Structured Storage";
		oResHelper.AddResource("IDS_DBPROP_STRUCTUREDSTORAGE", L_RCTEXT21_TEXT, "STRING");
		var L_RCTEXT22_TEXT =  "Subquery Support";
		oResHelper.AddResource("IDS_DBPROP_SUBQUERIES", L_RCTEXT22_TEXT, "STRING");
		var L_RCTEXT23_TEXT =  "Isolation Levels";
		oResHelper.AddResource("IDS_DBPROP_SUPPORTEDTXNISOLEVELS", L_RCTEXT23_TEXT, "STRING");
		var L_RCTEXT24_TEXT =  "Isolation Retention";
		oResHelper.AddResource("IDS_DBPROP_SUPPORTEDTXNISORETAIN", L_RCTEXT24_TEXT, "STRING");
		var L_RCTEXT25_TEXT =     "Table Term";
		oResHelper.AddResource("IDS_DBPROP_TABLETERM", L_RCTEXT25_TEXT, "STRING");
		var L_RCTEXT26_TEXT =      "User Name";
		oResHelper.AddResource("IDS_DBPROP_USERNAME", L_RCTEXT26_TEXT, "STRING");
		var L_RCTEXT27_TEXT =  "Transaction DDL";
		oResHelper.AddResource("IDS_DBPROP_SUPPORTEDTXNDDL", L_RCTEXT27_TEXT, "STRING");
		var L_RCTEXT28_TEXT =  "Asynchable Abort";
		oResHelper.AddResource("IDS_DBPROP_ASYNCTXNABORT", L_RCTEXT28_TEXT, "STRING");
		var L_RCTEXT29_TEXT =  "Data Source Object Threading Model";
		oResHelper.AddResource("IDS_DBPROP_DSOTHREADMODEL", L_RCTEXT29_TEXT, "STRING");
		var L_RCTEXT30_TEXT =  "Multiple Parameter Sets";
		oResHelper.AddResource("IDS_DBPROP_MULTIPLEPARAMSETS", L_RCTEXT30_TEXT, "STRING");
		var L_RCTEXT31_TEXT =  "Output Parameter Availability";
		oResHelper.AddResource("IDS_DBPROP_OUTPUTPARAMETERAVAILABILITY", L_RCTEXT31_TEXT, "STRING");
		var L_RCTEXT32_TEXT =  "Persistent ID Type";
		oResHelper.AddResource("IDS_DBPROP_PERSISTENTIDTYPE", L_RCTEXT32_TEXT, "STRING");
		var L_RCTEXT33_TEXT =  "Column Set Notification";
		oResHelper.AddResource("IDS_DBPROP_NOTIFYCOLUMNSET", L_RCTEXT33_TEXT, "STRING");
		var L_RCTEXT34_TEXT =  "Row Delete Notification";
		oResHelper.AddResource("IDS_DBPROP_NOTIFYROWDELETE", L_RCTEXT34_TEXT, "STRING");
		var L_RCTEXT35_TEXT =  "Row First Change Notification";
		oResHelper.AddResource("IDS_DBPROP_NOTIFYROWFIRSTCHANGE", L_RCTEXT35_TEXT, "STRING");
		var L_RCTEXT36_TEXT =  "Row Insert Notification";
		oResHelper.AddResource("IDS_DBPROP_NOTIFYROWINSERT", L_RCTEXT36_TEXT, "STRING");
		var L_RCTEXT37_TEXT =  "Row Resynchronization Notification";
		oResHelper.AddResource("IDS_DBPROP_NOTIFYROWRESYNCH", L_RCTEXT37_TEXT, "STRING");
		var L_RCTEXT38_TEXT =  "Rowset Release Notification";
		oResHelper.AddResource("IDS_DBPROP_NOTIFYROWSETRELEASE", L_RCTEXT38_TEXT, "STRING");
		var L_RCTEXT39_TEXT =  "Rowset Fetch Position Change Notification";
		oResHelper.AddResource("IDS_DBPROP_NOTIFYROWSETFETCHPOSITIONCHANGE", L_RCTEXT39_TEXT, "STRING");
		var L_RCTEXT40_TEXT =  "Row Undo Change Notification";
		oResHelper.AddResource("IDS_DBPROP_NOTIFYROWUNDOCHANGE", L_RCTEXT40_TEXT, "STRING");
		var L_RCTEXT41_TEXT =  "Row Undo Delete Notification";
		oResHelper.AddResource("IDS_DBPROP_NOTIFYROWUNDODELETE", L_RCTEXT41_TEXT, "STRING");
		var L_RCTEXT42_TEXT =       "GROUP BY Support";
		oResHelper.AddResource("IDS_DBPROP_GROUPBY", L_RCTEXT42_TEXT, "STRING");
		var L_RCTEXT43_TEXT =  "Heterogeneous Table Support";
		oResHelper.AddResource("IDS_DBPROP_HETEROGENEOUSTABLES", L_RCTEXT43_TEXT, "STRING");
		var L_RCTEXT44_TEXT =  "Identifier Case Sensitivity";
		oResHelper.AddResource("IDS_DBPROP_IDENTIFIERCASE", L_RCTEXT44_TEXT, "STRING");
		var L_RCTEXT45_TEXT =     "Lock Modes";
		oResHelper.AddResource("IDS_DBPROP_LOCKMODES", L_RCTEXT45_TEXT, "STRING");
		var L_RCTEXT46_TEXT =  "Maximum Index Size";
		oResHelper.AddResource("IDS_DBPROP_MAXINDEXSIZE", L_RCTEXT46_TEXT, "STRING");
		var L_RCTEXT47_TEXT =    "Maximum Row Size";
		oResHelper.AddResource("IDS_DBPROP_MAXROWSIZE", L_RCTEXT47_TEXT, "STRING");
		var L_RCTEXT48_TEXT =  "Maximum Row Size Includes BLOB";
		oResHelper.AddResource("IDS_DBPROP_MAXROWSIZEINCLUDESBLOB", L_RCTEXT48_TEXT, "STRING");
		var L_RCTEXT49_TEXT =  "Maximum Tables in SELECT";
		oResHelper.AddResource("IDS_DBPROP_MAXTABLESINSELECT", L_RCTEXT49_TEXT, "STRING");
		var L_RCTEXT50_TEXT =  "Multiple Storage Objects";
		oResHelper.AddResource("IDS_DBPROP_MULTIPLESTORAGEOBJECTS", L_RCTEXT50_TEXT, "STRING");
		var L_RCTEXT51_TEXT =  "Multi-Table Update";
		oResHelper.AddResource("IDS_DBPROP_MULTITABLEUPDATE", L_RCTEXT51_TEXT, "STRING");
		var L_RCTEXT52_TEXT =  "Notification Phases";
		oResHelper.AddResource("IDS_DBPROP_NOTIFICATIONPHASES", L_RCTEXT52_TEXT, "STRING");
		var L_RCTEXT53_TEXT =  "NULL Collation Order";
		oResHelper.AddResource("IDS_DBPROP_NULLCOLLATION", L_RCTEXT53_TEXT, "STRING");
		var L_RCTEXT54_TEXT =    "OLE Object Support";
		oResHelper.AddResource("IDS_DBPROP_OLEOBJECTS", L_RCTEXT54_TEXT, "STRING");
		var L_RCTEXT55_TEXT =  "ORDER BY Columns in Select List";
		oResHelper.AddResource("IDS_DBPROP_ORDERBYCOLUMNSINSELECT", L_RCTEXT55_TEXT, "STRING");
		var L_RCTEXT56_TEXT =  "Prepare Commit Behavior";
		oResHelper.AddResource("IDS_DBPROP_PREPARECOMMITBEHAVIOR", L_RCTEXT56_TEXT, "STRING");
		var L_RCTEXT57_TEXT =  "Prepare Abort Behavior";
		oResHelper.AddResource("IDS_DBPROP_PREPAREABORTBEHAVIOR", L_RCTEXT57_TEXT, "STRING");
		var L_RCTEXT58_TEXT =  "Row Undo Insert Notification";
		oResHelper.AddResource("IDS_DBPROP_NOTIFYROWUNDOINSERT", L_RCTEXT58_TEXT, "STRING");
		var L_RCTEXT59_TEXT =  "Row Update Notification";
		oResHelper.AddResource("IDS_DBPROP_NOTIFYROWUPDATE", L_RCTEXT59_TEXT, "STRING");
		var L_RCTEXT60_TEXT =  "Rowset Conversions on Command";
		oResHelper.AddResource("IDS_DBPROP_ROWSETCONVERSIONSONCOMMAND", L_RCTEXT60_TEXT, "STRING");
		var L_RCTEXT61_TEXT =  "Multiple Results";
		oResHelper.AddResource("IDS_DBPROP_MULTIPLERESULTS", L_RCTEXT61_TEXT, "STRING");
		var L_RCTEXT62_TEXT =  "ISequentialStream";
		oResHelper.AddResource("IDS_DBPROP_ISequentialStream", L_RCTEXT62_TEXT, "STRING");
		var L_RCTEXT63_TEXT =  "Preserve on Abort";
		oResHelper.AddResource("IDS_DBPROP_ABORTPRESERVE", L_RCTEXT63_TEXT, "STRING");
		var L_RCTEXT64_TEXT =  "Blocking Storage Objects";
		oResHelper.AddResource("IDS_DBPROP_BLOCKINGSTORAGEOBJECTS", L_RCTEXT64_TEXT, "STRING");
		var L_RCTEXT65_TEXT =  "IRowsetScroll";
		oResHelper.AddResource("IDS_DBPROP_IRowsetScroll", L_RCTEXT65_TEXT, "STRING");
		var L_RCTEXT66_TEXT =  "IRowsetUpdate";
		oResHelper.AddResource("IDS_DBPROP_IRowsetUpdate", L_RCTEXT66_TEXT, "STRING");
		var L_RCTEXT67_TEXT =  "ISupportErrorInfo";
		oResHelper.AddResource("IDS_DBPROP_ISupportErrorInfo", L_RCTEXT67_TEXT, "STRING");
		var L_RCTEXT68_TEXT =  "Change Inserted Rows";
		oResHelper.AddResource("IDS_DBPROP_CHANGEINSERTEDROWS", L_RCTEXT68_TEXT, "STRING");
		var L_RCTEXT69_TEXT =  "Return Pending Inserts";
		oResHelper.AddResource("IDS_DBPROP_RETURNPENDINGINSERTS", L_RCTEXT69_TEXT, "STRING");
		var L_RCTEXT70_TEXT =  "IConvertType";
		oResHelper.AddResource("IDS_DBPROP_IConvertType", L_RCTEXT70_TEXT, "STRING");
		var L_RCTEXT71_TEXT =  "Cache Authentication";
		oResHelper.AddResource("IDS_DBPROP_AUTH_CACHE_AUTHINFO", L_RCTEXT71_TEXT, "STRING");
		var L_RCTEXT72_TEXT =  "Encrypt Password";
		oResHelper.AddResource("IDS_DBPROP_AUTH_ENCRYPT_PASSWORD", L_RCTEXT72_TEXT, "STRING");
		var L_RCTEXT73_TEXT =  "Integrated Security";
		oResHelper.AddResource("IDS_DBPROP_AUTH_INTEGRATED", L_RCTEXT73_TEXT, "STRING");
		var L_RCTEXT74_TEXT =  "Mask Password";
		oResHelper.AddResource("IDS_DBPROP_AUTH_MASK_PASSWORD", L_RCTEXT74_TEXT, "STRING");
		var L_RCTEXT75_TEXT =  "Password";
		oResHelper.AddResource("IDS_DBPROP_AUTH_PASSWORD", L_RCTEXT75_TEXT, "STRING");
		var L_RCTEXT76_TEXT =  "Persist Encrypted";
		oResHelper.AddResource("IDS_DBPROP_AUTH_PERSIST_ENCRYPTED", L_RCTEXT76_TEXT, "STRING");
		var L_RCTEXT77_TEXT =  "Persist Security Info";
		oResHelper.AddResource("IDS_DBPROP_AUTH_PERSIST_SENSITIVE_AUTHINFO", L_RCTEXT77_TEXT, "STRING");
		var L_RCTEXT78_TEXT =   "User ID";
		oResHelper.AddResource("IDS_DBPROP_AUTH_USERID", L_RCTEXT78_TEXT, "STRING");
		var L_RCTEXT79_TEXT =  "Data Source";
		oResHelper.AddResource("IDS_DBPROP_INIT_DATASOURCE", L_RCTEXT79_TEXT, "STRING");
		var L_RCTEXT80_TEXT =     "Window Handle";
		oResHelper.AddResource("IDS_DBPROP_INIT_HWND", L_RCTEXT80_TEXT, "STRING");
		var L_RCTEXT81_TEXT =  "Impersonation Level";
		oResHelper.AddResource("IDS_DBPROP_INIT_IMPERSONATION_LEVEL", L_RCTEXT81_TEXT, "STRING");
		var L_RCTEXT82_TEXT =  "Location";
		oResHelper.AddResource("IDS_DBPROP_INIT_LOCATION", L_RCTEXT82_TEXT, "STRING");
		var L_RCTEXT83_TEXT =     "Mode";
		oResHelper.AddResource("IDS_DBPROP_INIT_MODE", L_RCTEXT83_TEXT, "STRING");
		var L_RCTEXT84_TEXT =   "Prompt";
		oResHelper.AddResource("IDS_DBPROP_INIT_PROMPT", L_RCTEXT84_TEXT, "STRING");
		var L_RCTEXT85_TEXT =  "Protection Level";
		oResHelper.AddResource("IDS_DBPROP_INIT_PROTECTION_LEVEL", L_RCTEXT85_TEXT, "STRING");
		var L_RCTEXT86_TEXT =  "Connect Timeout";
		oResHelper.AddResource("IDS_DBPROP_INIT_TIMEOUT", L_RCTEXT86_TEXT, "STRING");
		var L_RCTEXT87_TEXT =     "Locale Identifier";
		oResHelper.AddResource("IDS_DBPROP_INIT_LCID", L_RCTEXT87_TEXT, "STRING");
		var L_RCTEXT88_TEXT =  "Extended Properties";
		oResHelper.AddResource("IDS_DBPROP_INIT_PROVIDERSTRING", L_RCTEXT88_TEXT, "STRING");
		var L_RCTEXT89_TEXT =  "Autocommit Isolation Levels";
		oResHelper.AddResource("IDS_DBPROP_SESS_AUTOCOMMITISOLEVELS", L_RCTEXT89_TEXT, "STRING");
		var L_RCTEXT90_TEXT =  "Server Cursor";
		oResHelper.AddResource("IDS_DBPROP_SERVERCURSOR", L_RCTEXT90_TEXT, "STRING");
		var L_RCTEXT91_TEXT =  "Objects Transacted";
		oResHelper.AddResource("IDS_DBPROP_TRANSACTEDOBJECT", L_RCTEXT91_TEXT, "STRING");
		var L_RCTEXT92_TEXT =  "Updatability";
		oResHelper.AddResource("IDS_DBPROP_UPDATABILITY", L_RCTEXT92_TEXT, "STRING");
		var L_RCTEXT93_TEXT =  "Strong Row Identity";
		oResHelper.AddResource("IDS_DBPROP_STRONGIDENTITY", L_RCTEXT93_TEXT, "STRING");
		var L_RCTEXT94_TEXT =     "IAccessor";
		oResHelper.AddResource("IDS_DBPROP_IAccessor", L_RCTEXT94_TEXT, "STRING");
		var L_RCTEXT95_TEXT =  "IColumnsInfo";
		oResHelper.AddResource("IDS_DBPROP_IColumnsInfo", L_RCTEXT95_TEXT, "STRING");
		var L_RCTEXT96_TEXT =  "IColumnsRowset";
		oResHelper.AddResource("IDS_DBPROP_IColumnsRowset", L_RCTEXT96_TEXT, "STRING");
		var L_RCTEXT97_TEXT =  "IConnectionPointContainer";
		oResHelper.AddResource("IDS_DBPROP_IConnectionPointContainer", L_RCTEXT97_TEXT, "STRING");
		var L_RCTEXT98_TEXT =  "IProvideMoniker";
		oResHelper.AddResource("IDS_DBPROP_IProvideMoniker", L_RCTEXT98_TEXT, "STRING");
		var L_RCTEXT99_TEXT =       "IRowset";
		oResHelper.AddResource("IDS_DBPROP_IRowset", L_RCTEXT99_TEXT, "STRING");
		var L_RCTEXT100_TEXT =  "IRowsetChange";
		oResHelper.AddResource("IDS_DBPROP_IRowsetChange", L_RCTEXT100_TEXT, "STRING");
		var L_RCTEXT101_TEXT =  "IRowsetIdentity";
		oResHelper.AddResource("IDS_DBPROP_IRowsetIdentity", L_RCTEXT101_TEXT, "STRING");
		var L_RCTEXT102_TEXT =   "IRowsetInfo";
		oResHelper.AddResource("IDS_DBPROP_IRowsetInfo", L_RCTEXT102_TEXT, "STRING");
		var L_RCTEXT103_TEXT =  "IRowsetLocate";
		oResHelper.AddResource("IDS_DBPROP_IRowsetLocate", L_RCTEXT103_TEXT, "STRING");
		var L_RCTEXT104_TEXT =  "IRowsetResynch";
		oResHelper.AddResource("IDS_DBPROP_IRowsetResynch", L_RCTEXT104_TEXT, "STRING");
		var L_RCTEXT105_TEXT =     "Use Bookmarks";
		oResHelper.AddResource("IDS_DBPROP_BOOKMARKS", L_RCTEXT105_TEXT, "STRING");
		var L_RCTEXT106_TEXT =  "Skip Deleted Bookmarks";
		oResHelper.AddResource("IDS_DBPROP_BOOKMARKSKIPPED", L_RCTEXT106_TEXT, "STRING");
		var L_RCTEXT107_TEXT =  "Bookmark Type";
		oResHelper.AddResource("IDS_DBPROP_BOOKMARKTYPE", L_RCTEXT107_TEXT, "STRING");
		var L_RCTEXT108_TEXT =  "Fetch Backwards";
		oResHelper.AddResource("IDS_DBPROP_CANFETCHBACKWARDS", L_RCTEXT108_TEXT, "STRING");
		var L_RCTEXT109_TEXT =   "Hold Rows";
		oResHelper.AddResource("IDS_DBPROP_CANHOLDROWS", L_RCTEXT109_TEXT, "STRING");
		var L_RCTEXT110_TEXT =    "Append-Only Rowset";
		oResHelper.AddResource("IDS_DBPROP_APPENDONLY", L_RCTEXT110_TEXT, "STRING");
		var L_RCTEXT111_TEXT =  "Scroll Backwards";
		oResHelper.AddResource("IDS_DBPROP_CANSCROLLBACKWARDS", L_RCTEXT111_TEXT, "STRING");
		var L_RCTEXT112_TEXT =  "Column Privileges";
		oResHelper.AddResource("IDS_DBPROP_COLUMNRESTRICT", L_RCTEXT112_TEXT, "STRING");
		var L_RCTEXT113_TEXT =  "Command Time Out";
		oResHelper.AddResource("IDS_DBPROP_COMMANDTIMEOUT", L_RCTEXT113_TEXT, "STRING");
		var L_RCTEXT114_TEXT =  "Preserve on Commit";
		oResHelper.AddResource("IDS_DBPROP_COMMITPRESERVE", L_RCTEXT114_TEXT, "STRING");
		var L_RCTEXT115_TEXT =  "Delay Storage Object Updates";
		oResHelper.AddResource("IDS_DBPROP_DELAYSTORAGEOBJECTS", L_RCTEXT115_TEXT, "STRING");
		var L_RCTEXT116_TEXT =  "Immobile Rows";
		oResHelper.AddResource("IDS_DBPROP_IMMOBILEROWS", L_RCTEXT116_TEXT, "STRING");
		var L_RCTEXT117_TEXT =  "Literal Bookmarks";
		oResHelper.AddResource("IDS_DBPROP_LITERALBOOKMARKS", L_RCTEXT117_TEXT, "STRING");
		var L_RCTEXT118_TEXT =  "Literal Row Identity";
		oResHelper.AddResource("IDS_DBPROP_LITERALIDENTITY", L_RCTEXT118_TEXT, "STRING");
		var L_RCTEXT119_TEXT =   "Maximum Open Rows";
		oResHelper.AddResource("IDS_DBPROP_MAXOPENROWS", L_RCTEXT119_TEXT, "STRING");
		var L_RCTEXT120_TEXT =  "Maximum Pending Rows";
		oResHelper.AddResource("IDS_DBPROP_MAXPENDINGROWS", L_RCTEXT120_TEXT, "STRING");
		var L_RCTEXT121_TEXT =       "Maximum Rows";
		oResHelper.AddResource("IDS_DBPROP_MAXROWS", L_RCTEXT121_TEXT, "STRING");
		var L_RCTEXT122_TEXT =   "Others' Inserts Visible";
		oResHelper.AddResource("IDS_DBPROP_OTHERINSERT", L_RCTEXT122_TEXT, "STRING");
		var L_RCTEXT123_TEXT =  "Others' Changes Visible";
		oResHelper.AddResource("IDS_DBPROP_OTHERUPDATEDELETE", L_RCTEXT123_TEXT, "STRING");
		var L_RCTEXT124_TEXT =     "Own Inserts Visible";
		oResHelper.AddResource("IDS_DBPROP_OWNINSERT", L_RCTEXT124_TEXT, "STRING");
		var L_RCTEXT125_TEXT =  "Own Changes Visible";
		oResHelper.AddResource("IDS_DBPROP_OWNUPDATEDELETE", L_RCTEXT125_TEXT, "STRING");
		var L_RCTEXT126_TEXT =  "Quick Restart";
		oResHelper.AddResource("IDS_DBPROP_QUICKRESTART", L_RCTEXT126_TEXT, "STRING");
		var L_RCTEXT127_TEXT =  "Reentrant Events";
		oResHelper.AddResource("IDS_DBPROP_REENTRANTEVENTS", L_RCTEXT127_TEXT, "STRING");
		var L_RCTEXT128_TEXT =  "Remove Deleted Rows";
		oResHelper.AddResource("IDS_DBPROP_REMOVEDELETED", L_RCTEXT128_TEXT, "STRING");
		var L_RCTEXT129_TEXT =  "Report Multiple Changes";
		oResHelper.AddResource("IDS_DBPROP_REPORTMULTIPLECHANGES", L_RCTEXT129_TEXT, "STRING");
		var L_RCTEXT130_TEXT =   "Row Privileges";
		oResHelper.AddResource("IDS_DBPROP_ROWRESTRICT", L_RCTEXT130_TEXT, "STRING");
		var L_RCTEXT131_TEXT =  "Row Threading Model";
		oResHelper.AddResource("IDS_DBPROP_ROWTHREADMODEL", L_RCTEXT131_TEXT, "STRING");
		var L_RCTEXT132_TEXT =  "Bookmarks Ordered";
		oResHelper.AddResource("IDS_DBPROP_ORDEREDBOOKMARKS", L_RCTEXT132_TEXT, "STRING");
		var L_RCTEXT133_TEXT =  "Notification Granularity";
		oResHelper.AddResource("IDS_DBPROP_NOTIFICATIONGRANULARITY", L_RCTEXT133_TEXT, "STRING");
		// 1.5 properties
		var L_RCTEXT134_TEXT =  "Filter Operations";
		oResHelper.AddResource("IDS_DBPROP_FILTERCOMPAREOPS", L_RCTEXT134_TEXT, "STRING");
		var L_RCTEXT135_TEXT =  "Find Operations";
		oResHelper.AddResource("IDS_DBPROP_FINDCOMPAREOPS", L_RCTEXT135_TEXT, "STRING");
		var L_RCTEXT136_TEXT =  "IChapteredRowset";
		oResHelper.AddResource("IDS_DBPROP_IChapteredRowset", L_RCTEXT136_TEXT, "STRING");
		var L_RCTEXT137_TEXT =  "IDBAsynchStatus";
		oResHelper.AddResource("IDS_DBPROP_IDBAsynchStatus", L_RCTEXT137_TEXT, "STRING");
		var L_RCTEXT138_TEXT =  "IRowsetFind";
		oResHelper.AddResource("IDS_DBPROP_IRowsetFind", L_RCTEXT138_TEXT, "STRING");
		var L_RCTEXT139_TEXT =  "IRowsetView";
		oResHelper.AddResource("IDS_DBPROP_IRowsetView", L_RCTEXT139_TEXT, "STRING");
		var L_RCTEXT140_TEXT =  "IViewChapter";
		oResHelper.AddResource("IDS_DBPROP_IViewChapter", L_RCTEXT140_TEXT, "STRING");
		var L_RCTEXT141_TEXT =  "IViewFilter";
		oResHelper.AddResource("IDS_DBPROP_IViewFilter", L_RCTEXT141_TEXT, "STRING");
		var L_RCTEXT142_TEXT =  "IViewRowset";
		oResHelper.AddResource("IDS_DBPROP_IViewRowset", L_RCTEXT142_TEXT, "STRING");
		var L_RCTEXT143_TEXT =  "IViewSort";
		oResHelper.AddResource("IDS_DBPROP_IViewSort", L_RCTEXT143_TEXT, "STRING");
		var L_RCTEXT144_TEXT =  "Asynchronous Processing";
		oResHelper.AddResource("IDS_DBPROP_INIT_ASYNCH", L_RCTEXT144_TEXT, "STRING");
		var L_RCTEXT145_TEXT =  "Maximum Open Chapters";
		oResHelper.AddResource("IDS_DBPROP_MAXOPENCHAPTERS", L_RCTEXT145_TEXT, "STRING");
		var L_RCTEXT146_TEXT =  "Maximum OR Conditions";
		oResHelper.AddResource("IDS_DBPROP_MAXORSINFILTER", L_RCTEXT146_TEXT, "STRING");
		var L_RCTEXT147_TEXT =  "Maximum Sort Columns";
		oResHelper.AddResource("IDS_DBPROP_MAXSORTCOLUMNS", L_RCTEXT147_TEXT, "STRING");
		var L_RCTEXT148_TEXT =  "Asynchronous Rowset Processing";
		oResHelper.AddResource("IDS_DBPROP_ROWSET_ASYNCH", L_RCTEXT148_TEXT, "STRING");
		var L_RCTEXT149_TEXT =  "Sort on Index";
		oResHelper.AddResource("IDS_DBPROP_SORTONINDEX", L_RCTEXT149_TEXT, "STRING");
		// 2.0 properties
		var L_RCTEXT150_TEXT =  "IMultipleResults";
		oResHelper.AddResource("IDS_DBPROP_IMultipleResults", L_RCTEXT150_TEXT, "STRING");
		var L_RCTEXT151_TEXT =  "Data Source Type";
		oResHelper.AddResource("IDS_DBPROP_DATASOURCE_TYPE", L_RCTEXT151_TEXT, "STRING");
//MDPROP
		var L_RCTEXT152_TEXT =  "Number of axes in the dataset";
		oResHelper.AddResource("IDS_MDPROP_AXES", L_RCTEXT152_TEXT, "STRING");
		var L_RCTEXT153_TEXT =  "Flattening Support";
		oResHelper.AddResource("IDS_MDPROP_FLATTENING_SUPPORT", L_RCTEXT153_TEXT, "STRING");
		var L_RCTEXT154_TEXT =  "Support for query joining multiple cubes";
		oResHelper.AddResource("IDS_MDPROP_MDX_JOINCUBES", L_RCTEXT154_TEXT, "STRING");
		var L_RCTEXT155_TEXT =  "Support for named levels";
		oResHelper.AddResource("IDS_MDPROP_NAMED_LEVELS", L_RCTEXT155_TEXT, "STRING");
		//
		var L_RCTEXT156_TEXT =  "RANGEROWSET";
		oResHelper.AddResource("IDS_MDPROP_RANGEROWSET", L_RCTEXT156_TEXT, "STRING");
		var L_RCTEXT157_TEXT =  "The capabilities in the WHERE clause of an MDX statement";
		oResHelper.AddResource("IDS_MDPROP_MDX_SLICER", L_RCTEXT157_TEXT, "STRING");
		//
		var L_RCTEXT158_TEXT =  "MDX_CUBEQUALIFICATION";
		oResHelper.AddResource("IDS_MDPROP_MDX_CUBEQUALIFICATION", L_RCTEXT158_TEXT, "STRING");
		var L_RCTEXT159_TEXT =  "Support for outer reference in an MDX statement";
		oResHelper.AddResource("IDS_MDPROP_MDX_OUTERREFERENCE", L_RCTEXT159_TEXT, "STRING");
		var L_RCTEXT160_TEXT =  "Support for querying by property values in an MDX statement";
		oResHelper.AddResource("IDS_MDPROP_MDX_QUERYBYPROPERTY", L_RCTEXT160_TEXT, "STRING");
		var L_RCTEXT161_TEXT =  "Support for MDX case statements";
		oResHelper.AddResource("IDS_MDPROP_MDX_CASESUPPORT", L_RCTEXT161_TEXT, "STRING");
		var L_RCTEXT162_TEXT =  "Support for string comparison operators other than equals and not-equals operators";
		oResHelper.AddResource("IDS_MDPROP_MDX_STRING_COMPOP", L_RCTEXT162_TEXT, "STRING");
		var L_RCTEXT163_TEXT =  "Support for various <desc flag> values in the DESCENDANTS function";
		oResHelper.AddResource("IDS_MDPROP_MDX_DESCFLAGS", L_RCTEXT163_TEXT, "STRING");
		var L_RCTEXT164_TEXT =  "Support for various set functions";
		oResHelper.AddResource("IDS_MDPROP_MDX_SET_FUNCTIONS", L_RCTEXT164_TEXT, "STRING");
		var L_RCTEXT165_TEXT =  "Support for various member functions";
		oResHelper.AddResource("IDS_MDPROP_MDX_MEMBER_FUNCTIONS", L_RCTEXT165_TEXT, "STRING");
		var L_RCTEXT166_TEXT =  "Support for various numeric functions";
		oResHelper.AddResource("IDS_MDPROP_MDX_NUMERIC_FUNCTIONS", L_RCTEXT166_TEXT, "STRING");
		var L_RCTEXT167_TEXT =  "Support for creation of named sets and calculated members";
		oResHelper.AddResource("IDS_MDPROP_MDX_FORMULAS", L_RCTEXT167_TEXT, "STRING");
		var L_RCTEXT168_TEXT =  "Support for updating aggregated cells";
		oResHelper.AddResource("IDS_MDPROP_AGGREGATECELL_UPDATE", L_RCTEXT168_TEXT, "STRING");
		//
		var L_RCTEXT169_TEXT =  "MDX_AGGREGATECELL_UPDATE";
		oResHelper.AddResource("IDS_MDPROP_MDX_AGGREGATECELL_UPDATE", L_RCTEXT169_TEXT, "STRING");
		var L_RCTEXT170_TEXT =  "Provider's ability to qualify a cube name";
		oResHelper.AddResource("IDS_MDPROP_MDX_OBJQUALIFICATION", L_RCTEXT170_TEXT, "STRING");
		var L_RCTEXT171_TEXT =  "The capabilities in the <numeric_value_expression> argument of set functions";
		oResHelper.AddResource("IDS_MDPROP_MDX_NONMEASURE_EXPRESSONS", L_RCTEXT171_TEXT, "STRING");
// DBPROP
		var L_RCTEXT172_TEXT =  "Access Order";
		oResHelper.AddResource("IDS_DBPROP_ACCESSORDER", L_RCTEXT172_TEXT, "STRING");
		var L_RCTEXT173_TEXT =  "Bookmark Information";
		oResHelper.AddResource("IDS_DBPROP_BOOKMARKINFO", L_RCTEXT173_TEXT, "STRING");
		var L_RCTEXT174_TEXT =  "Initial Catalog";
		oResHelper.AddResource("IDS_DBPROP_INIT_CATALOG", L_RCTEXT174_TEXT, "STRING");
		var L_RCTEXT175_TEXT =  "Bulk Operations";
		oResHelper.AddResource("IDS_DBPROP_ROW_BULKOPS", L_RCTEXT175_TEXT, "STRING");
		var L_RCTEXT176_TEXT =  "Provider Friendly Name";
		oResHelper.AddResource("IDS_DBPROP_PROVIDERFRIENDLYNAME", L_RCTEXT176_TEXT, "STRING");
		var L_RCTEXT177_TEXT =  "Lock Mode";
		oResHelper.AddResource("IDS_DBPROP_LOCKMODE", L_RCTEXT177_TEXT, "STRING");
		var L_RCTEXT178_TEXT =  "Multiple Connections";
		oResHelper.AddResource("IDS_DBPROP_MULTIPLECONNECTIONS", L_RCTEXT178_TEXT, "STRING");
		var L_RCTEXT179_TEXT =  "Unique Rows";
		oResHelper.AddResource("IDS_DBPROP_UNIQUEROWS", L_RCTEXT179_TEXT, "STRING");
		var L_RCTEXT180_TEXT =  "Server Data on Insert";
		oResHelper.AddResource("IDS_DBPROP_SERVERDATAONINSERT", L_RCTEXT180_TEXT, "STRING");
		//
		var L_RCTEXT181_TEXT =  "STORAGEFLAGS";
		oResHelper.AddResource("IDS_DBPROP_STORAGEFLAGS", L_RCTEXT181_TEXT, "STRING");
		var L_RCTEXT182_TEXT =  "Connection Status";
		oResHelper.AddResource("IDS_DBPROP_CONNECTIONSTATUS", L_RCTEXT182_TEXT, "STRING");
		var L_RCTEXT183_TEXT =  "Alter Column Support";
		oResHelper.AddResource("IDS_DBPROP_ALTERCOLUMN", L_RCTEXT183_TEXT, "STRING");
		var L_RCTEXT184_TEXT =  "Column LCID";
		oResHelper.AddResource("IDS_DBPROP_COLUMNLCID", L_RCTEXT184_TEXT, "STRING");
		var L_RCTEXT185_TEXT =  "Reset Datasource";
		oResHelper.AddResource("IDS_DBPROP_RESETDATASOURCE", L_RCTEXT185_TEXT, "STRING");
		var L_RCTEXT186_TEXT =  "OLE DB Services";
		oResHelper.AddResource("IDS_DBPROP_INIT_OLEDBSERVICES", L_RCTEXT186_TEXT, "STRING");
		var L_RCTEXT187_TEXT =  "IRowsetRefresh";
		oResHelper.AddResource("IDS_DBPROP_IRowsetRefresh", L_RCTEXT187_TEXT, "STRING");
		var L_RCTEXT188_TEXT =  "Server Name";
		oResHelper.AddResource("IDS_DBPROP_SERVERNAME", L_RCTEXT188_TEXT, "STRING");
		var L_RCTEXT189_TEXT =  "IParentRowset";
		oResHelper.AddResource("IDS_DBPROP_IParentRowset", L_RCTEXT189_TEXT, "STRING");
		var L_RCTEXT190_TEXT =  "Hidden Column Count";
		oResHelper.AddResource("IDS_DBPROP_HIDDENCOLUMNS", L_RCTEXT190_TEXT, "STRING");
		var L_RCTEXT191_TEXT =  "Provider Owned Memory";
		oResHelper.AddResource("IDS_DBPROP_PROVIDERMEMORY", L_RCTEXT191_TEXT, "STRING");
		var L_RCTEXT192_TEXT =  "Client Cursor";
		oResHelper.AddResource("IDS_DBPROP_CLIENTCURSOR", L_RCTEXT192_TEXT, "STRING");
		// 2.1 properties
		var L_RCTEXT193_TEXT =  "Trustee User Name";
		oResHelper.AddResource("IDS_DBPROP_TRUSTEE_USERNAME", L_RCTEXT193_TEXT, "STRING");
		var L_RCTEXT194_TEXT =  "Authentication String";
		oResHelper.AddResource("IDS_DBPROP_TRUSTEE_AUTHENTICATION", L_RCTEXT194_TEXT, "STRING");
		var L_RCTEXT195_TEXT =  "New Authentication String";
		oResHelper.AddResource("IDS_DBPROP_TRUSTEE_NEWAUTHENTICATION", L_RCTEXT195_TEXT, "STRING");
		var L_RCTEXT196_TEXT =  "IRow";
		oResHelper.AddResource("IDS_DBPROP_IRow", L_RCTEXT196_TEXT, "STRING");
		var L_RCTEXT197_TEXT =  "IRowChange";
		oResHelper.AddResource("IDS_DBPROP_IRowChange", L_RCTEXT197_TEXT, "STRING");
		var L_RCTEXT198_TEXT =  "IRowSchemaChange";
		oResHelper.AddResource("IDS_DBPROP_IRowSchemaChange", L_RCTEXT198_TEXT, "STRING");
		var L_RCTEXT199_TEXT =  "IGetRow";
		oResHelper.AddResource("IDS_DBPROP_IGetRow", L_RCTEXT199_TEXT, "STRING");
		var L_RCTEXT200_TEXT =  "IScopedOperations";
		oResHelper.AddResource("IDS_DBPROP_IScopedOperations", L_RCTEXT200_TEXT, "STRING");
		var L_RCTEXT201_TEXT =  "IBindResource";
		oResHelper.AddResource("IDS_DBPROP_IBindResource", L_RCTEXT201_TEXT, "STRING");
		var L_RCTEXT202_TEXT =  "ICreateRow";
		oResHelper.AddResource("IDS_DBPROP_ICreateRow", L_RCTEXT202_TEXT, "STRING");
		var L_RCTEXT203_TEXT =  "Bind Flags";
		oResHelper.AddResource("IDS_DBPROP_INIT_BINDFLAGS", L_RCTEXT203_TEXT, "STRING");
		var L_RCTEXT204_TEXT =  "Lock Owner";
		oResHelper.AddResource("IDS_DBPROP_INIT_LOCKOWNER", L_RCTEXT204_TEXT, "STRING");
		var L_RCTEXT205_TEXT =  "URL Generation";
		oResHelper.AddResource("IDS_DBPROP_GENERATEURL", L_RCTEXT205_TEXT, "STRING");
		//
		var L_RCTEXT206_TEXT =  "IDBBinderProperties";
		oResHelper.AddResource("IDS_DBPROP_IDBBinderProperties", L_RCTEXT206_TEXT, "STRING");
		var L_RCTEXT207_TEXT =  "IColumnsInfo2";
		oResHelper.AddResource("IDS_DBPROP_IColumnsInfo2", L_RCTEXT207_TEXT, "STRING");
		//
		var L_RCTEXT208_TEXT =  "IRegisterProvider";
		oResHelper.AddResource("IDS_DBPROP_IRegisterProvider", L_RCTEXT208_TEXT, "STRING");
		var L_RCTEXT209_TEXT =  "IGetSession";
		oResHelper.AddResource("IDS_DBPROP_IGetSession", L_RCTEXT209_TEXT, "STRING");
		var L_RCTEXT210_TEXT =  "IGetSourceRow";
		oResHelper.AddResource("IDS_DBPROP_IGetSourceRow", L_RCTEXT210_TEXT, "STRING");
		var L_RCTEXT211_TEXT =  "IRowsetCurrentIndex";
		oResHelper.AddResource("IDS_DBPROP_IRowsetCurrentIndex", L_RCTEXT211_TEXT, "STRING");
		var L_RCTEXT212_TEXT =  "Open Rowset Support";
		oResHelper.AddResource("IDS_DBPROP_OPENROWSETSUPPORT", L_RCTEXT212_TEXT, "STRING");
		var L_RCTEXT213_TEXT =  "Is Long";
		oResHelper.AddResource("IDS_DBPROP_COL_ISLONG", L_RCTEXT213_TEXT, "STRING");
		// 2.5 properties
		var L_RCTEXT214_TEXT =  "Seed";
		oResHelper.AddResource("IDS_DBPROP_COL_SEED", L_RCTEXT214_TEXT, "STRING");
		var L_RCTEXT215_TEXT =  "Increment";
		oResHelper.AddResource("IDS_DBPROP_COL_INCREMENT", L_RCTEXT215_TEXT, "STRING");
		var L_RCTEXT216_TEXT =  "General Timeout";
		oResHelper.AddResource("IDS_DBPROP_INIT_GENERALTIMEOUT", L_RCTEXT216_TEXT, "STRING");
		var L_RCTEXT217_TEXT =  "COM Service Support";
		oResHelper.AddResource("IDS_DBPROP_COMSERVICES", L_RCTEXT217_TEXT, "STRING");
		// 2.6 properties
		var L_RCTEXT218_TEXT =  "Output Stream";
		oResHelper.AddResource("IDS_DBPROP_OUTPUTSTREAM", L_RCTEXT218_TEXT, "STRING");
		var L_RCTEXT219_TEXT =  "Output Encoding";
		oResHelper.AddResource("IDS_DBPROP_OUTPUTENCODING", L_RCTEXT219_TEXT, "STRING");
		var L_RCTEXT220_TEXT =  "Table Statistics Support";
		oResHelper.AddResource("IDS_DBPROP_TABLESTATISTICS", L_RCTEXT220_TEXT, "STRING");
		var L_RCTEXT221_TEXT =  "Skip Row Count Results";
		oResHelper.AddResource("IDS_DBPROP_SKIPROWCOUNTRESULTS", L_RCTEXT221_TEXT, "STRING");
		var L_RCTEXT222_TEXT =  "IRowsetBookmark";
		oResHelper.AddResource("IDS_DBPROP_IRowsetBookmark", L_RCTEXT222_TEXT, "STRING");
//MDPROP
		var L_RCTEXT223_TEXT =  "Defines visibility of precalculated totals";
		oResHelper.AddResource("IDS_MDPROP_VISUALMODE", L_RCTEXT223_TEXT, "STRING");

		if (!bAttributed)
		{
			var MidlTool = GetIDLConfig(selProj,true);
			var strMidlHeader = MidlTool.HeaderFileName;
			strMidlHeader = selProj.Object.Configurations(1).Evaluate(strMidlHeader);
			wizard.AddSymbol("MIDL_H_FILENAME",strMidlHeader);

			// Get LibID
			wizard.AddSymbol("LIBID_REGISTRY_FORMAT", oCM.IDLLibraries(1).Attributes.Find("uuid").Value);

			// Get typelib version
			var oVersion = oCM.IDLLibraries(1).Attributes.Find("version");
			if (oVersion)
			{
				var aryMajorMinor = oVersion.Value.split('.');
				for (var nCntr=0; nCntr<aryMajorMinor.length; nCntr++)
				{
					if (nCntr == 0)
						wizard.AddSymbol("TYPELIB_VERSION_MAJOR", aryMajorMinor[nCntr]);
					else
						wizard.AddSymbol("TYPELIB_VERSION_MINOR", aryMajorMinor[nCntr]);
				}
			}

			// Get AppID
			var strAppID = wizard.GetAppID();
			if (strAppID.length > 0)
			{
				wizard.AddSymbol("APPID_EXIST", true);
				wizard.AddSymbol("APPID_REGISTRY_FORMAT", strAppID);
			}

			// add RGS file resource
			strRGSFile = GetUniqueFileName(strProjectPath, CreateASCIIName(strShortName) + ".rgs");
			var strRGSID = "IDR_" + strUpperShortName;
			RenderAddTemplate(wizard, "provider.rgs", strRGSFile, false, false);
			var strSymbolValue = oResHelper.AddResource(strRGSID, strProjectPath + strRGSFile, "REGISTRY");
			wizard.AddSymbol("RGS_ID", strSymbolValue.split("=").shift());
	
			// Render provco.idl and insert into strProject.idl
			AddCoclassFromFile(oCM, "provco.idl");
		}
		oResHelper.CloseResourceFile();

		// Add #include <atldb.h> to pchFile, if any
		var nTotal = selProj.Object.Configurations.Count;
		var nCntr;
		var pchFile = "";
		for (nCntr = 1; nCntr <= nTotal; nCntr++)
		{
			var VCCLTool = selProj.Object.Configurations(nCntr).Tools("VCCLCompilerTool");
			if(VCCLTool.UsePrecompiledHeader == pchUseUsingSpecific)
			{
				if(pchFile=="")
					pchFile = VCCLTool.PrecompiledHeaderThrough;
			}
			if(pchFile!="")
			{
				if (!DoesIncludeExist(selProj, "<atldb.h>", pchFile))
					oCM.AddInclude("<atldb.h>", pchFile, vsCMAddPositionEnd);
				break;
			}
		}

		// Add header
		var strTemplatePath	= wizard.FindSymbol("TEMPLATES_PATH");
		if ("\\" != strTemplatePath.charAt(strTemplatePath.length-1))
			strTemplatePath += "\\";

		wizard.RenderTemplate(strTemplatePath + "prowset.h", strRowsetHeader);
		wizard.RenderTemplate(strTemplatePath + "psession.h", strSessionHeader);
		wizard.RenderTemplate(strTemplatePath + "pdatasrc.h", strDataSourceHeader);

		AddFileToProject(strRowsetHeader, selObj, true);
		AddFileToProject(strSessionHeader, selObj, false);
		AddFileToProject(strDataSourceHeader, selObj, false);

		// Add CPP
		RenderAddTemplate(wizard, "prowset.cpp", strRowsetImpl, selObj, false);
		
		oCM.CommitTransaction();
	}
	catch(e)
	{
		if (oCM)
			oCM.AbortTransaction();

		if (e.description.length != 0)
			SetErrorInfo(e);
		return e.number
	}
}

function CreateGUIDs()
{
	try
	{
		// create CLSID
		var strRawGUID = wizard.CreateGuid();
		var strFormattedGUID = wizard.FormatGuid(strRawGUID, 0);
		wizard.AddSymbol("CLSID_REGISTRY_FORMAT", strFormattedGUID);

		strRawGUID = wizard.CreateGuid();
		strFormattedGUID = wizard.FormatGuid(strRawGUID, 0);
		wizard.AddSymbol("CLSID_COMMAND_REGISTRY_FORMAT", strFormattedGUID);

		strRawGUID = wizard.CreateGuid();
		strFormattedGUID = wizard.FormatGuid(strRawGUID, 0);
		wizard.AddSymbol("CLSID_SESSION_REGISTRY_FORMAT", strFormattedGUID);
	}
	catch(e)
	{
		throw e;
	}
}

// SIG // Begin signature block
// SIG // MIIXOgYJKoZIhvcNAQcCoIIXKzCCFycCAQExCzAJBgUr
// SIG // DgMCGgUAMGcGCisGAQQBgjcCAQSgWTBXMDIGCisGAQQB
// SIG // gjcCAR4wJAIBAQQQEODJBs441BGiowAQS9NQkAIBAAIB
// SIG // AAIBAAIBAAIBADAhMAkGBSsOAwIaBQAEFIlzndkVW9DO
// SIG // veOGjhdvG/QOYSPToIISMTCCBGAwggNMoAMCAQICCi6r
// SIG // EdxQ/1ydy8AwCQYFKw4DAh0FADBwMSswKQYDVQQLEyJD
// SIG // b3B5cmlnaHQgKGMpIDE5OTcgTWljcm9zb2Z0IENvcnAu
// SIG // MR4wHAYDVQQLExVNaWNyb3NvZnQgQ29ycG9yYXRpb24x
// SIG // ITAfBgNVBAMTGE1pY3Jvc29mdCBSb290IEF1dGhvcml0
// SIG // eTAeFw0wNzA4MjIyMjMxMDJaFw0xMjA4MjUwNzAwMDBa
// SIG // MHkxCzAJBgNVBAYTAlVTMRMwEQYDVQQIEwpXYXNoaW5n
// SIG // dG9uMRAwDgYDVQQHEwdSZWRtb25kMR4wHAYDVQQKExVN
// SIG // aWNyb3NvZnQgQ29ycG9yYXRpb24xIzAhBgNVBAMTGk1p
// SIG // Y3Jvc29mdCBDb2RlIFNpZ25pbmcgUENBMIIBIjANBgkq
// SIG // hkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAt3l91l2zRTmo
// SIG // NKwx2vklNUl3wPsfnsdFce/RRujUjMNrTFJi9JkCw03Y
// SIG // SWwvJD5lv84jtwtIt3913UW9qo8OUMUlK/Kg5w0jH9FB
// SIG // JPpimc8ZRaWTSh+ZzbMvIsNKLXxv2RUeO4w5EDndvSn0
// SIG // ZjstATL//idIprVsAYec+7qyY3+C+VyggYSFjrDyuJSj
// SIG // zzimUIUXJ4dO3TD2AD30xvk9gb6G7Ww5py409rQurwp9
// SIG // YpF4ZpyYcw2Gr/LE8yC5TxKNY8ss2TJFGe67SpY7UFMY
// SIG // zmZReaqth8hWPp+CUIhuBbE1wXskvVJmPZlOzCt+M26E
// SIG // RwbRntBKhgJuhgCkwIffUwIDAQABo4H6MIH3MBMGA1Ud
// SIG // JQQMMAoGCCsGAQUFBwMDMIGiBgNVHQEEgZowgZeAEFvQ
// SIG // cO9pcp4jUX4Usk2O/8uhcjBwMSswKQYDVQQLEyJDb3B5
// SIG // cmlnaHQgKGMpIDE5OTcgTWljcm9zb2Z0IENvcnAuMR4w
// SIG // HAYDVQQLExVNaWNyb3NvZnQgQ29ycG9yYXRpb24xITAf
// SIG // BgNVBAMTGE1pY3Jvc29mdCBSb290IEF1dGhvcml0eYIP
// SIG // AMEAizw8iBHRPvZj7N9AMA8GA1UdEwEB/wQFMAMBAf8w
// SIG // HQYDVR0OBBYEFMwdznYAcFuv8drETppRRC6jRGPwMAsG
// SIG // A1UdDwQEAwIBhjAJBgUrDgMCHQUAA4IBAQB7q65+Siby
// SIG // zrxOdKJYJ3QqdbOG/atMlHgATenK6xjcacUOonzzAkPG
// SIG // yofM+FPMwp+9Vm/wY0SpRADulsia1Ry4C58ZDZTX2h6t
// SIG // KX3v7aZzrI/eOY49mGq8OG3SiK8j/d/p1mkJkYi9/uEA
// SIG // uzTz93z5EBIuBesplpNCayhxtziP4AcNyV1ozb2AQWtm
// SIG // qLu3u440yvIDEHx69dLgQt97/uHhrP7239UNs3DWkuNP
// SIG // tjiifC3UPds0C2I3Ap+BaiOJ9lxjj7BauznXYIxVhBoz
// SIG // 9TuYoIIMol+Lsyy3oaXLq9ogtr8wGYUgFA0qvFL0QeBe
// SIG // MOOSKGmHwXDi86erzoBCcnYOMIIEejCCA2KgAwIBAgIK
// SIG // YQHPPgAAAAAADzANBgkqhkiG9w0BAQUFADB5MQswCQYD
// SIG // VQQGEwJVUzETMBEGA1UECBMKV2FzaGluZ3RvbjEQMA4G
// SIG // A1UEBxMHUmVkbW9uZDEeMBwGA1UEChMVTWljcm9zb2Z0
// SIG // IENvcnBvcmF0aW9uMSMwIQYDVQQDExpNaWNyb3NvZnQg
// SIG // Q29kZSBTaWduaW5nIFBDQTAeFw0wOTEyMDcyMjQwMjla
// SIG // Fw0xMTAzMDcyMjQwMjlaMIGDMQswCQYDVQQGEwJVUzET
// SIG // MBEGA1UECBMKV2FzaGluZ3RvbjEQMA4GA1UEBxMHUmVk
// SIG // bW9uZDEeMBwGA1UEChMVTWljcm9zb2Z0IENvcnBvcmF0
// SIG // aW9uMQ0wCwYDVQQLEwRNT1BSMR4wHAYDVQQDExVNaWNy
// SIG // b3NvZnQgQ29ycG9yYXRpb24wggEiMA0GCSqGSIb3DQEB
// SIG // AQUAA4IBDwAwggEKAoIBAQC9MIn7RXKoU2ueiU8AI8C+
// SIG // 1B09sVlAOPNzkYIm5pYSAFPZHIIOPM4du733Qo2X1Pw4
// SIG // GuS5+ePs02EDv6DT1nVNXEap7V7w0uJpWxpz6rMcjQTN
// SIG // KUSgZFkvHphdbserGDmCZcSnvKt1iBnqh5cUJrN/Jnak
// SIG // 1Dg5hOOzJtUY+Svp0skWWlQh8peNh4Yp/vRJLOaL+AQ/
// SIG // fc3NlpKGDXED4tD+DEI1/9e4P92ORQp99tdLrVvwdnId
// SIG // dyN9iTXEHF2yUANLR20Hp1WImAaApoGtVE7Ygdb6v0LA
// SIG // Mb5VDZnVU0kSMOvlpYh8XsR6WhSHCLQ3aaDrMiSMCOv5
// SIG // 1BS64PzN6qQVAgMBAAGjgfgwgfUwEwYDVR0lBAwwCgYI
// SIG // KwYBBQUHAwMwHQYDVR0OBBYEFDh4BXPIGzKbX5KGVa+J
// SIG // usaZsXSOMA4GA1UdDwEB/wQEAwIHgDAfBgNVHSMEGDAW
// SIG // gBTMHc52AHBbr/HaxE6aUUQuo0Rj8DBEBgNVHR8EPTA7
// SIG // MDmgN6A1hjNodHRwOi8vY3JsLm1pY3Jvc29mdC5jb20v
// SIG // cGtpL2NybC9wcm9kdWN0cy9DU1BDQS5jcmwwSAYIKwYB
// SIG // BQUHAQEEPDA6MDgGCCsGAQUFBzAChixodHRwOi8vd3d3
// SIG // Lm1pY3Jvc29mdC5jb20vcGtpL2NlcnRzL0NTUENBLmNy
// SIG // dDANBgkqhkiG9w0BAQUFAAOCAQEAKAODqxMN8f4Rb0J2
// SIG // 2EOruMZC+iRlNK51sHEwjpa2g/py5P7NN+c6cJhRIA66
// SIG // cbTJ9NXkiugocHPV7eHCe+7xVjRagILrENdyA+oSTuzd
// SIG // DYx7RE8MYXX9bpwH3c4rWhgNObBg/dr/BKoCo9j6jqO7
// SIG // vcFqVDsxX+QsbsvxTSoc8h52e4avxofWsSrtrMwOwOSf
// SIG // f+jP6IRyVIIYbirInpW0Gh7Bb5PbYqbBS2utye09kuOy
// SIG // L6t6dzlnagB7gp0DEN5jlUkmQt6VIsGHC9AUo1/cczJy
// SIG // Nh7/yCnFJFJPZkjJHR2pxSY5aVBOp+zCBmwuchvxIdpt
// SIG // JEiAgRVAfJ/MdDhKTzCCBJ0wggOFoAMCAQICEGoLmU/A
// SIG // ACWrEdtFH1h6Z6IwDQYJKoZIhvcNAQEFBQAwcDErMCkG
// SIG // A1UECxMiQ29weXJpZ2h0IChjKSAxOTk3IE1pY3Jvc29m
// SIG // dCBDb3JwLjEeMBwGA1UECxMVTWljcm9zb2Z0IENvcnBv
// SIG // cmF0aW9uMSEwHwYDVQQDExhNaWNyb3NvZnQgUm9vdCBB
// SIG // dXRob3JpdHkwHhcNMDYwOTE2MDEwNDQ3WhcNMTkwOTE1
// SIG // MDcwMDAwWjB5MQswCQYDVQQGEwJVUzETMBEGA1UECBMK
// SIG // V2FzaGluZ3RvbjEQMA4GA1UEBxMHUmVkbW9uZDEeMBwG
// SIG // A1UEChMVTWljcm9zb2Z0IENvcnBvcmF0aW9uMSMwIQYD
// SIG // VQQDExpNaWNyb3NvZnQgVGltZXN0YW1waW5nIFBDQTCC
// SIG // ASIwDQYJKoZIhvcNAQEBBQADggEPADCCAQoCggEBANw3
// SIG // bvuvyEJKcRjIzkg+U8D6qxS6LDK7Ek9SyIPtPjPZSTGS
// SIG // KLaRZOAfUIS6wkvRfwX473W+i8eo1a5pcGZ4J2botrfv
// SIG // hbnN7qr9EqQLWSIpL89A2VYEG3a1bWRtSlTb3fHev5+D
// SIG // x4Dff0wCN5T1wJ4IVh5oR83ZwHZcL322JQS0VltqHGP/
// SIG // gHw87tUEJU05d3QHXcJc2IY3LHXJDuoeOQl8dv6dbG56
// SIG // 4Ow+j5eecQ5fKk8YYmAyntKDTisiXGhFi94vhBBQsvm1
// SIG // Go1s7iWbE/jLENeFDvSCdnM2xpV6osxgBuwFsIYzt/iU
// SIG // W4RBhFiFlG6wHyxIzG+cQ+Bq6H8mjmsCAwEAAaOCASgw
// SIG // ggEkMBMGA1UdJQQMMAoGCCsGAQUFBwMIMIGiBgNVHQEE
// SIG // gZowgZeAEFvQcO9pcp4jUX4Usk2O/8uhcjBwMSswKQYD
// SIG // VQQLEyJDb3B5cmlnaHQgKGMpIDE5OTcgTWljcm9zb2Z0
// SIG // IENvcnAuMR4wHAYDVQQLExVNaWNyb3NvZnQgQ29ycG9y
// SIG // YXRpb24xITAfBgNVBAMTGE1pY3Jvc29mdCBSb290IEF1
// SIG // dGhvcml0eYIPAMEAizw8iBHRPvZj7N9AMBAGCSsGAQQB
// SIG // gjcVAQQDAgEAMB0GA1UdDgQWBBRv6E4/l7k0q0uGj7yc
// SIG // 6qw7QUPG0DAZBgkrBgEEAYI3FAIEDB4KAFMAdQBiAEMA
// SIG // QTALBgNVHQ8EBAMCAYYwDwYDVR0TAQH/BAUwAwEB/zAN
// SIG // BgkqhkiG9w0BAQUFAAOCAQEAlE0RMcJ8ULsRjqFhBwEO
// SIG // jHBFje9zVL0/CQUt/7hRU4Uc7TmRt6NWC96Mtjsb0fus
// SIG // p8m3sVEhG28IaX5rA6IiRu1stG18IrhG04TzjQ++B4o2
// SIG // wet+6XBdRZ+S0szO3Y7A4b8qzXzsya4y1Ye5y2PENtEY
// SIG // Ib923juasxtzniGI2LS0ElSM9JzCZUqaKCacYIoPO8cT
// SIG // ZXhIu8+tgzpPsGJY3jDp6Tkd44ny2jmB+RMhjGSAYwYE
// SIG // lvKaAkMve0aIuv8C2WX5St7aA3STswVuDMyd3ChhfEjx
// SIG // F5wRITgCHIesBsWWMrjlQMZTPb2pid7oZjeN9CKWnMyw
// SIG // d1RROtZyRLIj9jCCBKowggOSoAMCAQICCmEGlC0AAAAA
// SIG // AAkwDQYJKoZIhvcNAQEFBQAweTELMAkGA1UEBhMCVVMx
// SIG // EzARBgNVBAgTCldhc2hpbmd0b24xEDAOBgNVBAcTB1Jl
// SIG // ZG1vbmQxHjAcBgNVBAoTFU1pY3Jvc29mdCBDb3Jwb3Jh
// SIG // dGlvbjEjMCEGA1UEAxMaTWljcm9zb2Z0IFRpbWVzdGFt
// SIG // cGluZyBQQ0EwHhcNMDgwNzI1MTkwMjE3WhcNMTMwNzI1
// SIG // MTkxMjE3WjCBszELMAkGA1UEBhMCVVMxEzARBgNVBAgT
// SIG // Cldhc2hpbmd0b24xEDAOBgNVBAcTB1JlZG1vbmQxHjAc
// SIG // BgNVBAoTFU1pY3Jvc29mdCBDb3Jwb3JhdGlvbjENMAsG
// SIG // A1UECxMETU9QUjEnMCUGA1UECxMebkNpcGhlciBEU0Ug
// SIG // RVNOOjdBODItNjg4QS05RjkyMSUwIwYDVQQDExxNaWNy
// SIG // b3NvZnQgVGltZS1TdGFtcCBTZXJ2aWNlMIIBIjANBgkq
// SIG // hkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAlYEKIEIYUXrZ
// SIG // le2b/dyH0fsOjxPqqjcoEnb+TVCrdpcqk0fgqVZpAuWU
// SIG // fk2F239x73UA27tDbPtvrHHwK9F8ks6UF52hxbr5937d
// SIG // YeEtMB6cJi12P+ZGlo6u2Ik32Mzv889bw/xo4PJkj5vo
// SIG // wxL5o76E/NaLzgU9vQF2UCcD+IS3FoaNYL5dKSw8z6X9
// SIG // mFo1HU8WwDjYHmE/PTazVhQVd5U7EPoAsJPiXTerJ7tj
// SIG // LEgUgVXjbOqpK5WNiA5+owCldyQHmCpwA7gqJJCa3sWi
// SIG // Iku/TFkGd1RyQ7A+ZN2ThAhYtv7ph0kJNrOz+DOpfkyi
// SIG // eX8yWSkOnrX14DyeP+xGOwIDAQABo4H4MIH1MB0GA1Ud
// SIG // DgQWBBQolYi/Ajvr2pS6fUYP+sv0fp3/0TAfBgNVHSME
// SIG // GDAWgBRv6E4/l7k0q0uGj7yc6qw7QUPG0DBEBgNVHR8E
// SIG // PTA7MDmgN6A1hjNodHRwOi8vY3JsLm1pY3Jvc29mdC5j
// SIG // b20vcGtpL2NybC9wcm9kdWN0cy90c3BjYS5jcmwwSAYI
// SIG // KwYBBQUHAQEEPDA6MDgGCCsGAQUFBzAChixodHRwOi8v
// SIG // d3d3Lm1pY3Jvc29mdC5jb20vcGtpL2NlcnRzL3RzcGNh
// SIG // LmNydDATBgNVHSUEDDAKBggrBgEFBQcDCDAOBgNVHQ8B
// SIG // Af8EBAMCBsAwDQYJKoZIhvcNAQEFBQADggEBAADurPzi
// SIG // 0ohmyinjWrnNAIJ+F1zFJFkSu6j3a9eH/o3LtXYfGyL2
// SIG // 9+HKtLlBARo3rUg3lnD6zDOnKIy4C7Z0Eyi3s3XhKgni
// SIG // i0/fmD+XtzQSgeoQ3R3cumTPTlA7TIr9Gd0lrtWWh+pL
// SIG // xOXw+UEXXQHrV4h9dnrlb/6HIKyTnIyav18aoBUwJOCi
// SIG // fmGRHSkpw0mQOkODie7e1YPdTyw1O+dBQQGqAAwL8tZJ
// SIG // G85CjXuw8y2NXSnhvo1/kRV2tGD7FCeqbxJjQihYOoo7
// SIG // i0Dkt8XMklccRlZrj8uSTVYFAMr4MEBFTt8ZiL31EPDd
// SIG // Gt8oHrRR8nfgJuO7CYES3B460EUxggR1MIIEcQIBATCB
// SIG // hzB5MQswCQYDVQQGEwJVUzETMBEGA1UECBMKV2FzaGlu
// SIG // Z3RvbjEQMA4GA1UEBxMHUmVkbW9uZDEeMBwGA1UEChMV
// SIG // TWljcm9zb2Z0IENvcnBvcmF0aW9uMSMwIQYDVQQDExpN
// SIG // aWNyb3NvZnQgQ29kZSBTaWduaW5nIFBDQQIKYQHPPgAA
// SIG // AAAADzAJBgUrDgMCGgUAoIGgMBkGCSqGSIb3DQEJAzEM
// SIG // BgorBgEEAYI3AgEEMBwGCisGAQQBgjcCAQsxDjAMBgor
// SIG // BgEEAYI3AgEVMCMGCSqGSIb3DQEJBDEWBBRzO6MYyLvy
// SIG // UXSpw805TpYuOO/4ODBABgorBgEEAYI3AgEMMTIwMKAW
// SIG // gBQAZABlAGYAYQB1AGwAdAAuAGoAc6EWgBRodHRwOi8v
// SIG // bWljcm9zb2Z0LmNvbTANBgkqhkiG9w0BAQEFAASCAQAG
// SIG // BFvpmWwdH2yZsAeT9JZpA7jxRpoi1uXTISQsopFYpJfS
// SIG // tDU8iRLEw07lOwfclmu+ubwqq22gHfxIQK86b7ceu8qm
// SIG // VLbeaZTGQD9O0PSv31GQeea3FEn1HRs5UQcVPrfOgxG0
// SIG // 3iUjHL0FC+QGUz7FAzQnIcz4j1tXl/j1kiNNc+nSi6rn
// SIG // JOo1BdBzZzp3C7TehvDhfj///QnYzBdPGwmoyPVTTRts
// SIG // lqZ+mPFTbCoeqTyfNxpYPigHvgXcal3Qcih6brYdGWPa
// SIG // Hp3f/y2aJWE3kIXCSAt4RJ74/taZsorK7MHthP9o2Nld
// SIG // 0kdaCdM21ORiYdGLEQ80WdKHPVUtlIOEoYICHzCCAhsG
// SIG // CSqGSIb3DQEJBjGCAgwwggIIAgEBMIGHMHkxCzAJBgNV
// SIG // BAYTAlVTMRMwEQYDVQQIEwpXYXNoaW5ndG9uMRAwDgYD
// SIG // VQQHEwdSZWRtb25kMR4wHAYDVQQKExVNaWNyb3NvZnQg
// SIG // Q29ycG9yYXRpb24xIzAhBgNVBAMTGk1pY3Jvc29mdCBU
// SIG // aW1lc3RhbXBpbmcgUENBAgphBpQtAAAAAAAJMAcGBSsO
// SIG // AwIaoF0wGAYJKoZIhvcNAQkDMQsGCSqGSIb3DQEHATAc
// SIG // BgkqhkiG9w0BCQUxDxcNMTAwMzE5MTMxMDI3WjAjBgkq
// SIG // hkiG9w0BCQQxFgQUaDYvwzzzgDWoDnhjoKG89kCXZWow
// SIG // DQYJKoZIhvcNAQEFBQAEggEAdiwXHvnIrT3woXz/etMl
// SIG // FQ/rMLIvIeqrhzeUdTE1NGIy0Tn4KV/X1sQ/4NZuyM+9
// SIG // BjKu4xK99g79Rs3vlMmW3kocjTp7sBurkJx4YThAzdl3
// SIG // yW+tZ5PQVuDN0RG8g8Gp8vcJbPnI5XF+Ecc4FFaGPccI
// SIG // Xp/UziJGShAxOoPDKCUEtc8JKWVyaGk7B4kGxfTocvKL
// SIG // /U5gfNB2Loid6HbO+SCkTXITQvws/M/uAc1aSnZFU+HX
// SIG // RP6dT8ldf1+f8NyZWntzQm4HPqu1Zwcsova1IqbANXfQ
// SIG // AoAdYlLD594FKxQmJChhLX0x7KGsQvGK1C77nYonOGru
// SIG // I/0u+sgWTEAqzQ==
// SIG // End signature block
