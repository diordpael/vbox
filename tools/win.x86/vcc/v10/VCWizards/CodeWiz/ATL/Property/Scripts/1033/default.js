// Copyright (c) Microsoft Corporation. All rights reserved.
// Script for Add Property Wizard

var aryParamVT = new Array;
var aryParamTypeNames = new Array;
var aryParamAttribs = new Array;
var bEmbeddedIDL = false;

function OnPrep(selProj, selObj)
{
	var L_WizardDialogTitle_Text = "Add Propery Wizard";
	return PrepCodeWizard(selProj, L_WizardDialogTitle_Text);
}

function OnFinish(selProj, oInterface)
{
	var oCM;
	try
	{
		oCM	= selProj.CodeModel;
		oCM.Synchronize();

		var L_TRANSACTION_Text = "Add Property";
		oCM.StartTransaction(L_TRANSACTION_Text);

		if (typeDynamicLibrary == selProj.Object.Configurations(1).ConfigurationType)
			wizard.AddSymbol("DLL", true);
		else
			wizard.AddSymbol("DLL", false);

		var bMFC = wizard.FindSymbol("MFC_CLASS");

		// IDL changes
		//
		if (oInterface.Language==vsCMLanguageIDL)
		{
			bEmbeddedIDL = false;
		}
		else
		{
			bEmbeddedIDL = true;
		}

		var bIsManaged = oInterface.IsManaged;

		if (bIsManaged)
		{
			AddToManaged(oInterface, true);
		}
		else
		{
			InitParams(bMFC);
			var strAttributes = GetAttributes();

			if (bMFC)
				AddToIDLMFC(oInterface, strAttributes);
			else
			{
				AddToIDL(oInterface, strAttributes);
			}
		}

		// Class changes
		//

		var aryClasses = new Array();
		GetInterfaceClass2(oInterface.Name, oInterface.FullName, oCM.CodeElements, aryClasses, true);
		for (var nIndex = 0; nIndex < aryClasses.length; nIndex++)
		{
			var oClass = aryClasses[nIndex];

			// same code for COleControl-derived class
			if (bMFC && oClass.IsDerivedFrom("CCmdTarget"))
				AddToClassMFC(oClass, selProj);
			// not CCmdTarget-derived
			else if (bIsManaged)
				AddToManaged(oClass, false);
			else
				AddToClass(oClass, selProj);
		}
		oCM.CommitTransaction();
	}
	catch(e)
	{
		if (oCM)
			oCM.AbortTransaction();

		if (e.description.length != 0)
			SetErrorInfo(e);
		return e.number
	}
}

function AddToClass(oClass, oProj)
{
	try
	{
		var strExternalName	= wizard.FindSymbol("EXTERNAL_NAME");
		var strType			= wizard.FindSymbol("TYPE");
		var strReturnType	= wizard.FindSymbol("RETURN_TYPE");
		var bGenerateGet	= wizard.FindSymbol("GENERATE_GET");
		var bGeneratePut	= wizard.FindSymbol("GENERATE_PUT");
		var bPropPutRef		= wizard.FindSymbol("PROPPUTREF");
		var nNumParams		= wizard.FindSymbol("NUM_PARAMETERS");

		// if derived from CComObjectRootEx or interface
		// add prototype and implementation
		var strCPP = oClass.Location(vsCMWhereDefault);
		strCPP = strCPP.substr(strCPP.lastIndexOf("\\")+1);
		strCPP = strCPP.substring(0, strCPP.lastIndexOf(".")+1) + "cpp";
		if (!oProj.Object.Files(strCPP))
			strCPP = "";

		var oGetFunction = false;
		var oPutFunction = false;

		var config = oProj.Object.Configurations(1);
		var MidlTool = config.Tools("VCMidlTool");
		var bDefaultUnsigned = (MidlTool.DefaultCharType==midlCharUnsigned);

		var strParams = "";
		for (nCntr = 0; nCntr < nNumParams; nCntr++)
		{
			if (nCntr > 0)
				strParams += ", ";
			if (aryParamTypeNames[nCntr].substr(0, 10) == "SAFEARRAY(")
			{
				strParams += "SAFEARRAY * " + aryParamTypeNames[nCntr].substr(aryParamTypeNames[nCntr].indexOf(')') + 2);
			}
			else
				strParams += AddUnsignedToChar(aryParamTypeNames[nCntr], bDefaultUnsigned)
		}
		if (strParams.length)
			strParams += ", ";


		strType = AddUnsignedToChar(strType, bDefaultUnsigned);
		strReturnType = AddUnsignedToChar(strReturnType, bDefaultUnsigned);

		if (bGenerateGet)
		{
			oGetFunction = oClass.AddFunction("get_" + strExternalName + "(" + strParams + strType + "* pVal)", vsCMFunctionComMethod, strReturnType, vsCMAddPositionEnd, vsCMAccessPublic, strCPP);
			if (oGetFunction)
				oGetFunction.BodyText = GetFunctionBodyForReturnType(strReturnType);
		}
		if (bGeneratePut)
		{
			if (bPropPutRef)
				oPutFunction = oClass.AddFunction("putref_" + strExternalName + "(" + strParams + strType + " newVal)", vsCMFunctionComMethod, strReturnType, vsCMAddPositionEnd, vsCMAccessPublic, strCPP);
			else
				oPutFunction = oClass.AddFunction("put_" + strExternalName + "(" + strParams + strType + " newVal)", vsCMFunctionComMethod, strReturnType, vsCMAddPositionEnd, vsCMAccessPublic, strCPP);
			if (oPutFunction)
				oPutFunction.BodyText = GetFunctionBodyForReturnType(strReturnType);
		}
	}
	catch(e)
	{
		throw e;
	}
}

function AddToClassMFC(oClass, oProj)
{
	try
	{
		var strExternalName			= wizard.FindSymbol("EXTERNAL_NAME");
		var strType					= wizard.FindSymbol("TYPE");
		var strTypeVT				= wizard.FindSymbol("TYPE_VT");
		var strVariableName			= wizard.FindSymbol("VARIABLE_NAME");
		var strNotificationFunction	= wizard.FindSymbol("NOTIFICATION_FUNCTION");
		var strGetFunction			= wizard.FindSymbol("GET_FUNCTION");
		var strSetFunction			= wizard.FindSymbol("SET_FUNCTION");
		var bStock					= wizard.FindSymbol("STOCK");
		var bMemberVariable			= wizard.FindSymbol("MEMBER_VARIABLE");
		var nNumParams				= wizard.FindSymbol("NUM_PARAMETERS");
		var bDefaultProperty		= wizard.FindSymbol("DEFAULT_PROPERTY");
		var strDispid				= wizard.FindSymbol("DISPID");

		var strClassName = oClass.Name;

		// add to Dispatch map
		var oMap = oClass.Maps.Find("DISPATCH");
		if (bStock)
		{
			oMap.AddEntry(wizard.FindSymbol("STOCK_MACRO"), vsCMAddPositionEnd);
		}
		else
		{
			var config = oProj.Object.Configurations(1);
			var MidlTool = config.Tools("VCMidlTool");
			var bDefaultUnsigned = (MidlTool.DefaultCharType==midlCharUnsigned);

			if (bMemberVariable)
			{
				// add to dispatch map
				var strMapItem;
				if (strNotificationFunction.length)
				{
					// add notification function
					var strCPP = oMap.Location(vsCMWhereDefault);
					strCPP = strCPP.substr(strCPP.lastIndexOf("\\")+1);
					var oFunction = oClass.AddFunction(strNotificationFunction, vsCMFunctionFunction, "void", vsCMAddPositionEnd, vsCMAccessProtected, strCPP);
					oFunction.BodyText = GetFunctionBodyForSet(oClass);
					strMapItem = "DISP_PROPERTY_NOTIFY_ID";
				}
				else
					strMapItem = "DISP_PROPERTY_ID";
				strMapItem += "(" + strClassName;
				strMapItem += ', "' + strExternalName + '"';
				strMapItem += ", dispid" + strExternalName;
				strMapItem += ", " + strVariableName;

				if (strNotificationFunction.length)
					strMapItem += ", " + strNotificationFunction;
				strMapItem += ", " + strTypeVT + ")";

				oMap.AddEntry(strMapItem, vsCMAddPositionEnd);

				// add member variable
				if (strType == "BSTR")
					oClass.AddVariable(strVariableName, "CString", vsCMAddPositionEnd, vsCMAccessProtected);
				else
				{
					strType = AddUnsignedToChar(strType, bDefaultUnsigned);
					oClass.AddVariable(strVariableName, strType, vsCMAddPositionEnd, vsCMAccessProtected);
				}
			}
			// Get/Set functions
			else
			{
				// add to dispatch map
				var strMapItem;
				if (nNumParams)
					strMapItem = "DISP_PROPERTY_PARAM_ID";
				else
					strMapItem = "DISP_PROPERTY_EX_ID";

				strMapItem += "(" + strClassName;
				strMapItem += ', "' + strExternalName + '"';
				strMapItem += ", dispid" + strExternalName;
				if (strGetFunction.length)
					strMapItem += ", " + strGetFunction;
				else
					strMapItem += ", GetNotSupported";
				if (strSetFunction.length)
					strMapItem += ", " + strSetFunction;
				else
					strMapItem += ", SetNotSupported";
				strMapItem += ", " + strTypeVT;

				if (nNumParams)
				{
					strMapItem += ",";
					for (nCntr = 0; nCntr < aryParamVT.length; nCntr++)
					{
							strMapItem += " " + aryParamVT[nCntr];
					}
				}
				strMapItem += ")";
				oMap.AddEntry(strMapItem, vsCMAddPositionEnd);


				// add Get and Set functions
				var strCPP = oMap.Location(vsCMWhereDefault);
				strCPP = strCPP.substr(strCPP.lastIndexOf("\\")+1);
				var oGetFunction = false;
				var oSetFunction = false;

				var strParams = "";
				for (nCntr = 0; nCntr < nNumParams; nCntr++)
				{
					if (nCntr > 0)
						strParams += ", ";
					if (aryParamTypeNames[nCntr].substr(0, 4) == "BSTR" &&
						aryParamTypeNames[nCntr].substr(0, 5) != "BSTR*" &&
						aryParamTypeNames[nCntr].substr(0, 6) != "BSTR *")
					{
						strParams += "LPCTSTR " + aryParamTypeNames[nCntr].substr(5);
					}
					else
						strParams += AddUnsignedToChar(aryParamTypeNames[nCntr], bDefaultUnsigned)
				}

				strType = AddUnsignedToChar(strType, bDefaultUnsigned);
				if (strGetFunction.length)
				{
					oGetFunction = oClass.AddFunction(strGetFunction + "(" + strParams + ")", vsCMFunctionFunction, strType, vsCMAddPositionEnd, vsCMAccessProtected, strCPP);
					if (oGetFunction)
						oGetFunction.BodyText = GetFunctionBodyForReturnType(strType);
				}
				if (strSetFunction.length)
				{
					if (strParams.length)
						strParams += ", ";
					if (strType == "BSTR")
						strType = "LPCTSTR";
					oSetFunction = oClass.AddFunction(strSetFunction + "(" + strParams + strType + " " + GetParamName(strType) + ")", vsCMFunctionFunction, "void", vsCMAddPositionEnd, vsCMAccessProtected, strCPP);
					if (oSetFunction)
						oSetFunction.BodyText = GetFunctionBodyForSet(oClass);
				}
			}

			// add dispidXXX in enum in Class.h
			var oEnum;
			if (oClass.Enums.Count)
				oEnum = oClass.Enums(1);
			else
				oEnum = oClass.AddEnum("", vsCMAddPositionEnd);

			var oEnumMember = oEnum.AddMember("dispid" + strExternalName, vsCMAddPositionEnd);
			oEnumMember.InitExpression = strDispid;

			if (bDefaultProperty)
			{
				var strMapItem = "DISP_DEFVALUE";
				strMapItem += "(" + strClassName;
				strMapItem += ', "' + strExternalName + '")';
				oMap.AddEntry(strMapItem, vsCMAddPositionEnd);
			}
		}
	}
	catch(e)
	{
		throw e;
	}
}

function AddToIDL(oInterface, strAttributes)
{
	try
	{
		var strExternalName		= wizard.FindSymbol("EXTERNAL_NAME");
		var strType				= wizard.FindSymbol("TYPE");
		var strReturnType		= wizard.FindSymbol("RETURN_TYPE");
		var bGenerateGet		= wizard.FindSymbol("GENERATE_GET");
		var bGeneratePut		= wizard.FindSymbol("GENERATE_PUT");
		var bPropPutRef			= wizard.FindSymbol("PROPPUTREF");
		var nNumParams			= wizard.FindSymbol("NUM_PARAMETERS");
		var strInterfaceType	= wizard.FindSymbol("INTERFACE_TYPE");
		var strDispid			= wizard.FindSymbol("DISPID");

		var oGetFunction = false;
		var oPutFunction = false;

		var strParams = "(";

		for (nCntr = 0; nCntr < nNumParams; nCntr++)
		{
			if (nCntr > 0)
				strParams += ", ";
			if (bEmbeddedIDL && aryParamTypeNames[nCntr].substr(0, 10) == "SAFEARRAY(")
			{
				strParams += "["
				if (aryParamAttribs[nCntr].length)
					strParams += aryParamAttribs[nCntr] + ", ";
				strParams += "satype(";
				strParams += aryParamTypeNames[nCntr].substring(10, aryParamTypeNames[nCntr].indexOf(')'));
				strParams += ")] ";
				strParams += "SAFEARRAY * " + aryParamTypeNames[nCntr].substr(aryParamTypeNames[nCntr].indexOf(')') + 2);
			}
			else
			{
				if (aryParamAttribs[nCntr].length)
					strParams += "[" + aryParamAttribs[nCntr] + "] ";
				strParams += aryParamTypeNames[nCntr];
			}
		}

		strParams += ")";

		if (strAttributes.length > 0)
			strAttributes = ', ' + strAttributes;

		if (bGenerateGet)
		{
			var strReturnType1;

			if (strInterfaceType == "dual" || strInterfaceType == "dispinterface")
				strReturnType1 = '	[propget, id(' + strDispid + ')' + strAttributes + '] HRESULT';
			else
				strReturnType1 = '	[propget' + strAttributes + '] ' + strReturnType;

			oGetFunction = oInterface.AddFunction(strExternalName + strParams, vsCMFunctionFunction, strReturnType1, vsCMAddPositionEnd, vsCMAccessDefault);
		}

		if (bGeneratePut)
		{
			var strReturnType1;
			var strPutType = "propput";
			if (bPropPutRef)
				strPutType = "propputref";

			if (strInterfaceType == "dual" || strInterfaceType == "dispinterface")
				strReturnType1 = '	[' + strPutType + ', id(' + strDispid + ')' + strAttributes + '] HRESULT';
			else
				strReturnType1 = '	[' + strPutType + strAttributes + '] ' + strReturnType;

			oPutFunction = oInterface.AddFunction(strExternalName + strParams, vsCMFunctionFunction, strReturnType1, vsCMAddPositionEnd, vsCMAccessDefault);
		}

		if (oGetFunction)
		{
			var strType1 = "[out, retval] " + strType + "*";
			oGetFunction.AddParameter("pVal", strType1, vsCMAddPositionEnd);
		}

		if (oPutFunction)
		{
			var strType1 = "[in] " + strType;
			oPutFunction.AddParameter("newVal", strType1, vsCMAddPositionEnd);
		}
	}
	catch(e)
	{
		throw e;
	}
}


function AddToManaged(oElement, bInterface)
{
	try
	{
		var strExternalName		= wizard.FindSymbol("EXTERNAL_NAME");
		var strType				= wizard.FindSymbol("TYPE");
		var bGenerateGet		= wizard.FindSymbol("GENERATE_GET");
		var bGeneratePut		= wizard.FindSymbol("GENERATE_PUT");

		var oGetFunction = false;
		var oPutFunction = false;
		var Kind = oElement.Kind;

		var strReturnType;

		if (bGenerateGet)
		{
			if (bInterface)
			{
				strReturnType = '__property ' + strType;
			}
			else
			{
				strReturnType = strType;
			}

			oGetFunction = oElement.AddFunction('get_' + strExternalName + '()',
				vsCMFunctionFunction, strReturnType, vsCMAddPositionEnd, vsCMAccessDefault);
		}

		if (bGeneratePut)
		{
			if (bInterface)
			{
				strReturnType = '__property void';
			}
			else
			{
				strReturnType = 'void';
			}

			oPutFunction = oElement.AddFunction('set_' + strExternalName + '(' + strType + ' newVal)',
				vsCMFunctionFunction, strReturnType, vsCMAddPositionEnd, vsCMAccessDefault);
		}
	}
	catch(e)
	{
		throw e;
	}
}


function AddToIDLMFC(oInterface, strAttributes)
{
	try
	{
		var strExternalName		= wizard.FindSymbol("EXTERNAL_NAME");
		var strType				= wizard.FindSymbol("TYPE");
		var bStock				= wizard.FindSymbol("STOCK");
		var bMemberVariable		= wizard.FindSymbol("MEMBER_VARIABLE");
		var nNumParams			= wizard.FindSymbol("NUM_PARAMETERS");
		var bDefaultProperty	= wizard.FindSymbol("DEFAULT_PROPERTY");
		var strDispid			= wizard.FindSymbol("DISPID");

		if (strAttributes.length > 0)
			strAttributes = ', ' + strAttributes;

		if (bStock)
		{
			var strType1 = "	[id(" + wizard.FindSymbol("STOCK_DISPID") + ")" + strAttributes + "] " + strType;
			oInterface.AddVariable(strExternalName, strType1, vsCMAddPositionEnd, vsCMAccessDefault);
			if (bDefaultProperty)
			{
				strType1 = "	[id(0)] " + strType;
				oInterface.AddVariable("_" + strExternalName, strType1, vsCMAddPositionEnd, vsCMAccessDefault);
			}
		}
		else
		{
			if (bMemberVariable)
			{
				var strType1 = "	[id(" + strDispid + ") " + strAttributes + "] " + strType;
				oInterface.AddVariable(strExternalName, strType1, vsCMAddPositionEnd, vsCMAccessDefault);
				if (bDefaultProperty)
				{
					strType1 = "	[id(0)] " + strType;
					oInterface.AddVariable("_" + strExternalName, strType1, vsCMAddPositionEnd, vsCMAccessDefault);
				}
			}
			else
			{
				if (nNumParams)
				{
					var oGetFunction = false;
					var oSetFunction = false;
					var oGetFunction2 = false;
					var oSetFunction2 = false;

					var strParams = "(";
					for (nCntr = 0; nCntr < nNumParams; nCntr++)
					{
						if (nCntr > 0)
							strParams += ", ";
						if (aryParamAttribs[nCntr].length)
							strParams += "[" + aryParamAttribs[nCntr] + "] ";
						strParams += aryParamTypeNames[nCntr];
					}
					strParams += ")";

					var strType1 = "	[id(" + strDispid + "), propget" + strAttributes + "] " + strType;
					oGetFunction = oInterface.AddFunction(strExternalName + strParams, vsCMFunctionFunction, strType1, vsCMAddPositionEnd, vsCMAccessDefault);
					if (bDefaultProperty)
					{
						strType1 = "	[id(0), propget" + strAttributes + "] " + strType;
						oGetFunction2 = oInterface.AddFunction("_" + strExternalName + strParams, vsCMFunctionFunction, strType1, vsCMAddPositionEnd, vsCMAccessDefault);
					}

					var strType1 = "	[id(" + strDispid + "), propput" + strAttributes + "] void";
					oSetFunction = oInterface.AddFunction(strExternalName + strParams, vsCMFunctionFunction, strType1, vsCMAddPositionEnd, vsCMAccessDefault);
					if (bDefaultProperty)
					{
						strType1 = "	[id(0), propput" + strAttributes + "] void";
						oSetFunction2 = oInterface.AddFunction("_" + strExternalName + strParams, vsCMFunctionFunction, strType1, vsCMAddPositionEnd, vsCMAccessDefault);
					}

					if (oSetFunction)
						oSetFunction.AddParameter(GetParamName(strType), strType, vsCMAddPositionEnd);
					if (oSetFunction2)
						oSetFunction2.AddParameter(GetParamName(strType), strType, vsCMAddPositionEnd);
				}
				else
				{
					var strType1 = "	[id(" + strDispid + ")" + strAttributes + "] " + strType;
					oInterface.AddVariable(strExternalName, strType1, vsCMAddPositionEnd, vsCMAccessDefault);
					if (bDefaultProperty)
					{
						strType1 = "	[id(0)" + strAttributes + "] " + strType;
						oInterface.AddVariable("_" + strExternalName, strType1, vsCMAddPositionEnd, vsCMAccessDefault);
					}
				}
			}
		}
	}
	catch(e)
	{
		throw e;
	}
}

function InitParams(bMFC)
{
	try
	{
		var nNumParams = wizard.FindSymbol("NUM_PARAMETERS");
		for (var nCntr = 0; nCntr < nNumParams; nCntr++)
		{
			if (bMFC)
				aryParamVT[nCntr] = wizard.FindSymbol("PARAM_VT_TYPE" + nCntr);

			aryParamTypeNames[nCntr] = wizard.FindSymbol("PARAM_TYPE_NAME" + nCntr);
			if (aryParamTypeNames[nCntr].substr(0, 12) == "OLE_TRISTATE")
				aryParamTypeNames[nCntr] = "enum " + aryParamTypeNames[nCntr];
			aryParamAttribs[nCntr] = wizard.FindSymbol("PARAM_ATTRIB" + nCntr);
		}

		var strType	= wizard.FindSymbol("TYPE");
		if (strType.substr(0, 12) == "OLE_TRISTATE")
		{
			strType = "enum " + strType;
			wizard.AddSymbol("TYPE", strType);
		}

		var strReturnType = wizard.FindSymbol("RETURN_TYPE");
		if (strReturnType.substr(0, 12) == "OLE_TRISTATE")
		{
			strReturnType = "enum " + strReturnType;
			wizard.AddSymbol("RETURN_TYPE", strReturnType);
		}
	}
	catch(e)
	{
		throw e;
	}
}

function GetAttributes()
{
	try
	{
		var strHelpString		= wizard.FindSymbol("HELP_STRING");
		var strHelpContext		= wizard.FindSymbol("HELP_CONTEXT");
		var bBindable			= wizard.FindSymbol("BINDABLE");
		var bDefaultBind		= wizard.FindSymbol("DEFAULT_BIND");
		var bDefaultCollElem	= wizard.FindSymbol("DEFAULT_COLLELEM");
		var bDisplayBind		= wizard.FindSymbol("DISPLAY_BIND");
		var bHidden				= wizard.FindSymbol("HIDDEN");
		var bImmediateBind		= wizard.FindSymbol("IMMEDIATE_BIND");
		var bLocal				= wizard.FindSymbol("LOCAL");
		var bNonBrowsable		= wizard.FindSymbol("NON_BROWSABLE");
		var bRequestEdit		= wizard.FindSymbol("REQUEST_EDIT");
		var bRestricted			= wizard.FindSymbol("RESTRICTED");
		var bSource				= wizard.FindSymbol("SOURCE");

		var strAttributes = "";

		if (strHelpString != "")
			strAttributes += ', helpstring("' + strHelpString + '")';

		if (strHelpContext != "")
			strAttributes += ', helpcontext(' + strHelpContext + ')';

		if (bBindable)
			strAttributes += ", bindable";

		if (bDefaultBind)
			strAttributes += ", defaultbind";

		if (bDefaultCollElem)
			strAttributes += ", defaultcollelem";

		if (bDisplayBind)
			strAttributes += ", displaybind";

		if (bHidden)
			strAttributes += ", hidden";

		if (bImmediateBind)
			strAttributes += ", immediatebind";

		if (bLocal)
			strAttributes += ", local";

		if (bNonBrowsable)
			strAttributes += ", nonbrowsable";

		if (bRequestEdit)
			strAttributes += ", requestedit";

		if (bRestricted)
			strAttributes += ", restricted";

		if (bSource)
			strAttributes += ", source";

		if (strAttributes.length > 1)
			strAttributes = strAttributes.substr(2);

		return strAttributes;
	}
	catch(e)
	{
		throw e;
	}
}

function GetParamName(strType)
{
	try
	{
		var strParamName = "newVal";
		if (-1 != strType.indexOf("**"))
			strParamName = "ppVal";
		else if  (-1 != strType.indexOf("*"))
			strParamName = "pVal";
		return strParamName;
	}
	catch(e)
	{
		throw e;
	}
}

function GetFunctionBodyForSet(oClass)
{
	try
	{
		var strBody = "";
		var bDLL = wizard.FindSymbol("DLL");

		if (bDLL)
			strBody = "AFX_MANAGE_STATE(AfxGetStaticModuleState());\r\n\r\n";
		else
			strBody = "AFX_MANAGE_STATE(AfxGetAppModuleState());\r\n\r\n";

		var L_Comment1_Text = "\57\57 TODO: Add your property handler code here\r\n";
		strBody += L_Comment1_Text;

		if (oClass.IsDerivedFrom("CDocument") || oClass.IsDerivedFrom("COleControl"))
			strBody += "\r\nSetModifiedFlag();\r\n";

		return strBody;
	}
	catch(e)
	{
		throw e;
	}
}

function GetFunctionBodyForReturnType(strReturnType)
{
	try
	{
		var strBody = "";
		var strComment;
		var bMFCProject = wizard.FindSymbol("MFC_PROJECT");
		if (bMFCProject)
		{
			var bDLL = wizard.FindSymbol("DLL");
			if (bDLL)
				strBody = "AFX_MANAGE_STATE(AfxGetStaticModuleState());\r\n\r\n";
			else
				strBody = "AFX_MANAGE_STATE(AfxGetAppModuleState());\r\n\r\n";
		}

		var bMFC = wizard.FindSymbol("MFC_CLASS");
		if (bMFC)
		{
			var L_Comment2_Text = "\57\57 TODO: Add your dispatch handler code here\r\n\r\n";
			strComment = L_Comment2_Text
		}
		else
		{
			var L_Comment3_Text = "\57\57 TODO: Add your implementation code here\r\n\r\n";
			strComment = L_Comment3_Text;
		}

		switch(strReturnType)
		{
			case "IDispatch*":
			case "IFontDisp*":
			case "IPictureDisp*":
			case "IUnknown*":
			case "OLE_HANDLE":
				strBody += strComment;
				strBody += "return NULL;\r\n";
				break;

			case "DATE":
				strBody += strComment;
				strBody += "return (DATE)0;\r\n";
				break;

			case "OLE_COLOR":
				strBody += strComment;
				strBody += "return RGB(0,0,0);\r\n";
				break;

			case "enum OLE_TRISTATE":
				strBody += strComment;
				strBody += "return (OLE_TRISTATE)0;\r\n";
				break;

			case "SCODE":
			case "HRESULT":
				strBody += strComment;
				strBody += "return S_OK;\r\n";
				break;

			case "BSTR":
				if (bMFC)
				{
					strBody += "CString strResult;\r\n\r\n";
					strBody += strComment;
					strBody += "return strResult.AllocSysString();\r\n";
				}
				else
				{
					strBody += strComment;
					strBody += "return NULL;\r\n";
				}
				break;

			case "CY":
				strBody += "CURRENCY cyResult = { 0, 0 };\r\n\r\n";
				strBody += strComment;
				strBody += "return cyResult;\r\n";
				break;

			case "VARIANT":
				strBody += "VARIANT vaResult;\r\n";
				strBody += "VariantInit(&vaResult);\r\n\r\n";
				strBody += strComment;
				strBody += "return vaResult;\r\n";
				break;

			case "VARIANT_BOOL":
				strBody += strComment;
				strBody += "return VARIANT_TRUE;\r\n";
				break;

			case "void":
				strBody += strComment.substr(0, strComment.length-2);
				break;

			default:
				strBody += strComment;
				strBody += "return 0;\r\n";
				break;
		}
		return strBody;
	}
	catch(e)
	{
		throw e;
	}
}

// SIG // Begin signature block
// SIG // MIIXOgYJKoZIhvcNAQcCoIIXKzCCFycCAQExCzAJBgUr
// SIG // DgMCGgUAMGcGCisGAQQBgjcCAQSgWTBXMDIGCisGAQQB
// SIG // gjcCAR4wJAIBAQQQEODJBs441BGiowAQS9NQkAIBAAIB
// SIG // AAIBAAIBAAIBADAhMAkGBSsOAwIaBQAEFFP5wFwmgVSH
// SIG // SobDYVLo8D4ET48zoIISMTCCBGAwggNMoAMCAQICCi6r
// SIG // EdxQ/1ydy8AwCQYFKw4DAh0FADBwMSswKQYDVQQLEyJD
// SIG // b3B5cmlnaHQgKGMpIDE5OTcgTWljcm9zb2Z0IENvcnAu
// SIG // MR4wHAYDVQQLExVNaWNyb3NvZnQgQ29ycG9yYXRpb24x
// SIG // ITAfBgNVBAMTGE1pY3Jvc29mdCBSb290IEF1dGhvcml0
// SIG // eTAeFw0wNzA4MjIyMjMxMDJaFw0xMjA4MjUwNzAwMDBa
// SIG // MHkxCzAJBgNVBAYTAlVTMRMwEQYDVQQIEwpXYXNoaW5n
// SIG // dG9uMRAwDgYDVQQHEwdSZWRtb25kMR4wHAYDVQQKExVN
// SIG // aWNyb3NvZnQgQ29ycG9yYXRpb24xIzAhBgNVBAMTGk1p
// SIG // Y3Jvc29mdCBDb2RlIFNpZ25pbmcgUENBMIIBIjANBgkq
// SIG // hkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAt3l91l2zRTmo
// SIG // NKwx2vklNUl3wPsfnsdFce/RRujUjMNrTFJi9JkCw03Y
// SIG // SWwvJD5lv84jtwtIt3913UW9qo8OUMUlK/Kg5w0jH9FB
// SIG // JPpimc8ZRaWTSh+ZzbMvIsNKLXxv2RUeO4w5EDndvSn0
// SIG // ZjstATL//idIprVsAYec+7qyY3+C+VyggYSFjrDyuJSj
// SIG // zzimUIUXJ4dO3TD2AD30xvk9gb6G7Ww5py409rQurwp9
// SIG // YpF4ZpyYcw2Gr/LE8yC5TxKNY8ss2TJFGe67SpY7UFMY
// SIG // zmZReaqth8hWPp+CUIhuBbE1wXskvVJmPZlOzCt+M26E
// SIG // RwbRntBKhgJuhgCkwIffUwIDAQABo4H6MIH3MBMGA1Ud
// SIG // JQQMMAoGCCsGAQUFBwMDMIGiBgNVHQEEgZowgZeAEFvQ
// SIG // cO9pcp4jUX4Usk2O/8uhcjBwMSswKQYDVQQLEyJDb3B5
// SIG // cmlnaHQgKGMpIDE5OTcgTWljcm9zb2Z0IENvcnAuMR4w
// SIG // HAYDVQQLExVNaWNyb3NvZnQgQ29ycG9yYXRpb24xITAf
// SIG // BgNVBAMTGE1pY3Jvc29mdCBSb290IEF1dGhvcml0eYIP
// SIG // AMEAizw8iBHRPvZj7N9AMA8GA1UdEwEB/wQFMAMBAf8w
// SIG // HQYDVR0OBBYEFMwdznYAcFuv8drETppRRC6jRGPwMAsG
// SIG // A1UdDwQEAwIBhjAJBgUrDgMCHQUAA4IBAQB7q65+Siby
// SIG // zrxOdKJYJ3QqdbOG/atMlHgATenK6xjcacUOonzzAkPG
// SIG // yofM+FPMwp+9Vm/wY0SpRADulsia1Ry4C58ZDZTX2h6t
// SIG // KX3v7aZzrI/eOY49mGq8OG3SiK8j/d/p1mkJkYi9/uEA
// SIG // uzTz93z5EBIuBesplpNCayhxtziP4AcNyV1ozb2AQWtm
// SIG // qLu3u440yvIDEHx69dLgQt97/uHhrP7239UNs3DWkuNP
// SIG // tjiifC3UPds0C2I3Ap+BaiOJ9lxjj7BauznXYIxVhBoz
// SIG // 9TuYoIIMol+Lsyy3oaXLq9ogtr8wGYUgFA0qvFL0QeBe
// SIG // MOOSKGmHwXDi86erzoBCcnYOMIIEejCCA2KgAwIBAgIK
// SIG // YQHPPgAAAAAADzANBgkqhkiG9w0BAQUFADB5MQswCQYD
// SIG // VQQGEwJVUzETMBEGA1UECBMKV2FzaGluZ3RvbjEQMA4G
// SIG // A1UEBxMHUmVkbW9uZDEeMBwGA1UEChMVTWljcm9zb2Z0
// SIG // IENvcnBvcmF0aW9uMSMwIQYDVQQDExpNaWNyb3NvZnQg
// SIG // Q29kZSBTaWduaW5nIFBDQTAeFw0wOTEyMDcyMjQwMjla
// SIG // Fw0xMTAzMDcyMjQwMjlaMIGDMQswCQYDVQQGEwJVUzET
// SIG // MBEGA1UECBMKV2FzaGluZ3RvbjEQMA4GA1UEBxMHUmVk
// SIG // bW9uZDEeMBwGA1UEChMVTWljcm9zb2Z0IENvcnBvcmF0
// SIG // aW9uMQ0wCwYDVQQLEwRNT1BSMR4wHAYDVQQDExVNaWNy
// SIG // b3NvZnQgQ29ycG9yYXRpb24wggEiMA0GCSqGSIb3DQEB
// SIG // AQUAA4IBDwAwggEKAoIBAQC9MIn7RXKoU2ueiU8AI8C+
// SIG // 1B09sVlAOPNzkYIm5pYSAFPZHIIOPM4du733Qo2X1Pw4
// SIG // GuS5+ePs02EDv6DT1nVNXEap7V7w0uJpWxpz6rMcjQTN
// SIG // KUSgZFkvHphdbserGDmCZcSnvKt1iBnqh5cUJrN/Jnak
// SIG // 1Dg5hOOzJtUY+Svp0skWWlQh8peNh4Yp/vRJLOaL+AQ/
// SIG // fc3NlpKGDXED4tD+DEI1/9e4P92ORQp99tdLrVvwdnId
// SIG // dyN9iTXEHF2yUANLR20Hp1WImAaApoGtVE7Ygdb6v0LA
// SIG // Mb5VDZnVU0kSMOvlpYh8XsR6WhSHCLQ3aaDrMiSMCOv5
// SIG // 1BS64PzN6qQVAgMBAAGjgfgwgfUwEwYDVR0lBAwwCgYI
// SIG // KwYBBQUHAwMwHQYDVR0OBBYEFDh4BXPIGzKbX5KGVa+J
// SIG // usaZsXSOMA4GA1UdDwEB/wQEAwIHgDAfBgNVHSMEGDAW
// SIG // gBTMHc52AHBbr/HaxE6aUUQuo0Rj8DBEBgNVHR8EPTA7
// SIG // MDmgN6A1hjNodHRwOi8vY3JsLm1pY3Jvc29mdC5jb20v
// SIG // cGtpL2NybC9wcm9kdWN0cy9DU1BDQS5jcmwwSAYIKwYB
// SIG // BQUHAQEEPDA6MDgGCCsGAQUFBzAChixodHRwOi8vd3d3
// SIG // Lm1pY3Jvc29mdC5jb20vcGtpL2NlcnRzL0NTUENBLmNy
// SIG // dDANBgkqhkiG9w0BAQUFAAOCAQEAKAODqxMN8f4Rb0J2
// SIG // 2EOruMZC+iRlNK51sHEwjpa2g/py5P7NN+c6cJhRIA66
// SIG // cbTJ9NXkiugocHPV7eHCe+7xVjRagILrENdyA+oSTuzd
// SIG // DYx7RE8MYXX9bpwH3c4rWhgNObBg/dr/BKoCo9j6jqO7
// SIG // vcFqVDsxX+QsbsvxTSoc8h52e4avxofWsSrtrMwOwOSf
// SIG // f+jP6IRyVIIYbirInpW0Gh7Bb5PbYqbBS2utye09kuOy
// SIG // L6t6dzlnagB7gp0DEN5jlUkmQt6VIsGHC9AUo1/cczJy
// SIG // Nh7/yCnFJFJPZkjJHR2pxSY5aVBOp+zCBmwuchvxIdpt
// SIG // JEiAgRVAfJ/MdDhKTzCCBJ0wggOFoAMCAQICEGoLmU/A
// SIG // ACWrEdtFH1h6Z6IwDQYJKoZIhvcNAQEFBQAwcDErMCkG
// SIG // A1UECxMiQ29weXJpZ2h0IChjKSAxOTk3IE1pY3Jvc29m
// SIG // dCBDb3JwLjEeMBwGA1UECxMVTWljcm9zb2Z0IENvcnBv
// SIG // cmF0aW9uMSEwHwYDVQQDExhNaWNyb3NvZnQgUm9vdCBB
// SIG // dXRob3JpdHkwHhcNMDYwOTE2MDEwNDQ3WhcNMTkwOTE1
// SIG // MDcwMDAwWjB5MQswCQYDVQQGEwJVUzETMBEGA1UECBMK
// SIG // V2FzaGluZ3RvbjEQMA4GA1UEBxMHUmVkbW9uZDEeMBwG
// SIG // A1UEChMVTWljcm9zb2Z0IENvcnBvcmF0aW9uMSMwIQYD
// SIG // VQQDExpNaWNyb3NvZnQgVGltZXN0YW1waW5nIFBDQTCC
// SIG // ASIwDQYJKoZIhvcNAQEBBQADggEPADCCAQoCggEBANw3
// SIG // bvuvyEJKcRjIzkg+U8D6qxS6LDK7Ek9SyIPtPjPZSTGS
// SIG // KLaRZOAfUIS6wkvRfwX473W+i8eo1a5pcGZ4J2botrfv
// SIG // hbnN7qr9EqQLWSIpL89A2VYEG3a1bWRtSlTb3fHev5+D
// SIG // x4Dff0wCN5T1wJ4IVh5oR83ZwHZcL322JQS0VltqHGP/
// SIG // gHw87tUEJU05d3QHXcJc2IY3LHXJDuoeOQl8dv6dbG56
// SIG // 4Ow+j5eecQ5fKk8YYmAyntKDTisiXGhFi94vhBBQsvm1
// SIG // Go1s7iWbE/jLENeFDvSCdnM2xpV6osxgBuwFsIYzt/iU
// SIG // W4RBhFiFlG6wHyxIzG+cQ+Bq6H8mjmsCAwEAAaOCASgw
// SIG // ggEkMBMGA1UdJQQMMAoGCCsGAQUFBwMIMIGiBgNVHQEE
// SIG // gZowgZeAEFvQcO9pcp4jUX4Usk2O/8uhcjBwMSswKQYD
// SIG // VQQLEyJDb3B5cmlnaHQgKGMpIDE5OTcgTWljcm9zb2Z0
// SIG // IENvcnAuMR4wHAYDVQQLExVNaWNyb3NvZnQgQ29ycG9y
// SIG // YXRpb24xITAfBgNVBAMTGE1pY3Jvc29mdCBSb290IEF1
// SIG // dGhvcml0eYIPAMEAizw8iBHRPvZj7N9AMBAGCSsGAQQB
// SIG // gjcVAQQDAgEAMB0GA1UdDgQWBBRv6E4/l7k0q0uGj7yc
// SIG // 6qw7QUPG0DAZBgkrBgEEAYI3FAIEDB4KAFMAdQBiAEMA
// SIG // QTALBgNVHQ8EBAMCAYYwDwYDVR0TAQH/BAUwAwEB/zAN
// SIG // BgkqhkiG9w0BAQUFAAOCAQEAlE0RMcJ8ULsRjqFhBwEO
// SIG // jHBFje9zVL0/CQUt/7hRU4Uc7TmRt6NWC96Mtjsb0fus
// SIG // p8m3sVEhG28IaX5rA6IiRu1stG18IrhG04TzjQ++B4o2
// SIG // wet+6XBdRZ+S0szO3Y7A4b8qzXzsya4y1Ye5y2PENtEY
// SIG // Ib923juasxtzniGI2LS0ElSM9JzCZUqaKCacYIoPO8cT
// SIG // ZXhIu8+tgzpPsGJY3jDp6Tkd44ny2jmB+RMhjGSAYwYE
// SIG // lvKaAkMve0aIuv8C2WX5St7aA3STswVuDMyd3ChhfEjx
// SIG // F5wRITgCHIesBsWWMrjlQMZTPb2pid7oZjeN9CKWnMyw
// SIG // d1RROtZyRLIj9jCCBKowggOSoAMCAQICCmEGlC0AAAAA
// SIG // AAkwDQYJKoZIhvcNAQEFBQAweTELMAkGA1UEBhMCVVMx
// SIG // EzARBgNVBAgTCldhc2hpbmd0b24xEDAOBgNVBAcTB1Jl
// SIG // ZG1vbmQxHjAcBgNVBAoTFU1pY3Jvc29mdCBDb3Jwb3Jh
// SIG // dGlvbjEjMCEGA1UEAxMaTWljcm9zb2Z0IFRpbWVzdGFt
// SIG // cGluZyBQQ0EwHhcNMDgwNzI1MTkwMjE3WhcNMTMwNzI1
// SIG // MTkxMjE3WjCBszELMAkGA1UEBhMCVVMxEzARBgNVBAgT
// SIG // Cldhc2hpbmd0b24xEDAOBgNVBAcTB1JlZG1vbmQxHjAc
// SIG // BgNVBAoTFU1pY3Jvc29mdCBDb3Jwb3JhdGlvbjENMAsG
// SIG // A1UECxMETU9QUjEnMCUGA1UECxMebkNpcGhlciBEU0Ug
// SIG // RVNOOjdBODItNjg4QS05RjkyMSUwIwYDVQQDExxNaWNy
// SIG // b3NvZnQgVGltZS1TdGFtcCBTZXJ2aWNlMIIBIjANBgkq
// SIG // hkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAlYEKIEIYUXrZ
// SIG // le2b/dyH0fsOjxPqqjcoEnb+TVCrdpcqk0fgqVZpAuWU
// SIG // fk2F239x73UA27tDbPtvrHHwK9F8ks6UF52hxbr5937d
// SIG // YeEtMB6cJi12P+ZGlo6u2Ik32Mzv889bw/xo4PJkj5vo
// SIG // wxL5o76E/NaLzgU9vQF2UCcD+IS3FoaNYL5dKSw8z6X9
// SIG // mFo1HU8WwDjYHmE/PTazVhQVd5U7EPoAsJPiXTerJ7tj
// SIG // LEgUgVXjbOqpK5WNiA5+owCldyQHmCpwA7gqJJCa3sWi
// SIG // Iku/TFkGd1RyQ7A+ZN2ThAhYtv7ph0kJNrOz+DOpfkyi
// SIG // eX8yWSkOnrX14DyeP+xGOwIDAQABo4H4MIH1MB0GA1Ud
// SIG // DgQWBBQolYi/Ajvr2pS6fUYP+sv0fp3/0TAfBgNVHSME
// SIG // GDAWgBRv6E4/l7k0q0uGj7yc6qw7QUPG0DBEBgNVHR8E
// SIG // PTA7MDmgN6A1hjNodHRwOi8vY3JsLm1pY3Jvc29mdC5j
// SIG // b20vcGtpL2NybC9wcm9kdWN0cy90c3BjYS5jcmwwSAYI
// SIG // KwYBBQUHAQEEPDA6MDgGCCsGAQUFBzAChixodHRwOi8v
// SIG // d3d3Lm1pY3Jvc29mdC5jb20vcGtpL2NlcnRzL3RzcGNh
// SIG // LmNydDATBgNVHSUEDDAKBggrBgEFBQcDCDAOBgNVHQ8B
// SIG // Af8EBAMCBsAwDQYJKoZIhvcNAQEFBQADggEBAADurPzi
// SIG // 0ohmyinjWrnNAIJ+F1zFJFkSu6j3a9eH/o3LtXYfGyL2
// SIG // 9+HKtLlBARo3rUg3lnD6zDOnKIy4C7Z0Eyi3s3XhKgni
// SIG // i0/fmD+XtzQSgeoQ3R3cumTPTlA7TIr9Gd0lrtWWh+pL
// SIG // xOXw+UEXXQHrV4h9dnrlb/6HIKyTnIyav18aoBUwJOCi
// SIG // fmGRHSkpw0mQOkODie7e1YPdTyw1O+dBQQGqAAwL8tZJ
// SIG // G85CjXuw8y2NXSnhvo1/kRV2tGD7FCeqbxJjQihYOoo7
// SIG // i0Dkt8XMklccRlZrj8uSTVYFAMr4MEBFTt8ZiL31EPDd
// SIG // Gt8oHrRR8nfgJuO7CYES3B460EUxggR1MIIEcQIBATCB
// SIG // hzB5MQswCQYDVQQGEwJVUzETMBEGA1UECBMKV2FzaGlu
// SIG // Z3RvbjEQMA4GA1UEBxMHUmVkbW9uZDEeMBwGA1UEChMV
// SIG // TWljcm9zb2Z0IENvcnBvcmF0aW9uMSMwIQYDVQQDExpN
// SIG // aWNyb3NvZnQgQ29kZSBTaWduaW5nIFBDQQIKYQHPPgAA
// SIG // AAAADzAJBgUrDgMCGgUAoIGgMBkGCSqGSIb3DQEJAzEM
// SIG // BgorBgEEAYI3AgEEMBwGCisGAQQBgjcCAQsxDjAMBgor
// SIG // BgEEAYI3AgEVMCMGCSqGSIb3DQEJBDEWBBS/7vW58rtp
// SIG // WxOParhtcrAdJm1UkjBABgorBgEEAYI3AgEMMTIwMKAW
// SIG // gBQAZABlAGYAYQB1AGwAdAAuAGoAc6EWgBRodHRwOi8v
// SIG // bWljcm9zb2Z0LmNvbTANBgkqhkiG9w0BAQEFAASCAQCz
// SIG // gywFqsGZIYgyUwKY7e8s9WzuXk/lFPbkKtZEGDIgSvmi
// SIG // vTR7kobvRwh0DuixCNuqWsXxKCtDAyTTROAUkkVbPLE+
// SIG // dnfmLL8C9kVSLn4c0kcWkqHSb7Ky1u3U4aR2WHWrEV8l
// SIG // 0E+LTG37Kz9vFfALt+HQWp1Oa96Gxl0VSyBiz14XY4zG
// SIG // aLTtiL3AoXdnYKbRZYWahOL4/zqqi8sj0aMgfI+VAICi
// SIG // 8uPJ/F1DvbIVP4Wutw65Pp2HoM5kilA/O1uFki6vmBn8
// SIG // LjKkSzmzbT1wwSRRA5EfVTbzQkWUkEXdH+j+69e2ZbyK
// SIG // UOxbYnhjOaiO46hjCK0ghR/tFuScGVchoYICHzCCAhsG
// SIG // CSqGSIb3DQEJBjGCAgwwggIIAgEBMIGHMHkxCzAJBgNV
// SIG // BAYTAlVTMRMwEQYDVQQIEwpXYXNoaW5ndG9uMRAwDgYD
// SIG // VQQHEwdSZWRtb25kMR4wHAYDVQQKExVNaWNyb3NvZnQg
// SIG // Q29ycG9yYXRpb24xIzAhBgNVBAMTGk1pY3Jvc29mdCBU
// SIG // aW1lc3RhbXBpbmcgUENBAgphBpQtAAAAAAAJMAcGBSsO
// SIG // AwIaoF0wGAYJKoZIhvcNAQkDMQsGCSqGSIb3DQEHATAc
// SIG // BgkqhkiG9w0BCQUxDxcNMTAwMzE5MTMwNTI0WjAjBgkq
// SIG // hkiG9w0BCQQxFgQUPtAfntaRI0mthQ8y9jifTyQ0GJEw
// SIG // DQYJKoZIhvcNAQEFBQAEggEARmB95XNubCnN/XFXGoZQ
// SIG // BjEWt0+Ku0kXhW7isjKhJAgY/op6Mh8wIGNgbzihGYjC
// SIG // ll1t4I0osLPd1MQZZFTk/CP6hc3EJcLh3kZCd8US6yGV
// SIG // GZsyOCZOzAkzw9HxHrt92yT35bTefkWyCCJ31u7QKN7i
// SIG // DmJit/hwMjn8uzbL7bUcPvYc9kW8SEhRPz2EYXKPZZdZ
// SIG // wp9Do0hU2No+GkDExhzq/aJ34f9wYqrqSycsuArClulm
// SIG // qicJO2v+INEg6VvWlHByakfC9Rk4ivgUlyG/LVdcDiSC
// SIG // UjQgO4l1+KSde7Bi0eJFxYWm5Zx0wjNlttVJ1de+tl26
// SIG // qawQauud0zpUMQ==
// SIG // End signature block
