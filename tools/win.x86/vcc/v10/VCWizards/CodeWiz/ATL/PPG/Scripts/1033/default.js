// Copyright (c) Microsoft Corporation. All rights reserved.
// Script for ATL Property Page

function OnFinish(selProj, selObj)
{
	var oCM;
	try
	{
		oCM	= selProj.CodeModel;
		var bDevice  = IsDeviceProject(selProj);

		wizard.AddSymbol("DEVICE", bDevice);
		
		// used only for device projects where platforms may not support DCOM.
		wizard.AddSymbol("SUPPORT_DCOM", false);
		wizard.AddSymbol("SUPPORT_NON_DCOM", false);

		SetResDlgFont();

		if (!bAttributed)
		{
			var MidlTool = GetIDLConfig(selProj,true);
			var strMidlHeader = MidlTool.HeaderFileName;
			strMidlHeader = selProj.Object.Configurations(1).Evaluate(strMidlHeader);
			wizard.AddSymbol("MIDL_H_FILENAME",strMidlHeader);
		}

		if (!bDevice)
		{
			var strShortName = wizard.FindSymbol("SHORT_NAME");
			var L_TRANSACTION_Text = "Add ATL Property Page ";
			oCM.StartTransaction(L_TRANSACTION_Text + strShortName);
			if(!AddATLSupportToProject(selProj))
			{
				oCM.AbortTransaction();
				return;
			}

			var bDLL;
			if (typeDynamicLibrary == selProj.Object.Configurations(1).ConfigurationType)
				bDLL = true;
			else
				bDLL = false;
			wizard.AddSymbol("DLL_APP", bDLL);

			var strProjectName		= wizard.FindSymbol("PROJECT_NAME");
			var strProjectPath		= wizard.FindSymbol("PROJECT_PATH");
			var strTemplatePath		= wizard.FindSymbol("TEMPLATES_PATH");
			var strUpperShortName	= CreateASCIIName(strShortName.toUpperCase());
			strUpperShortName		= CreateSafeName(strUpperShortName); 
			wizard.AddSymbol("UPPER_SHORT_NAME", strUpperShortName);
			var strVIProgID			= wizard.FindSymbol("VERSION_INDEPENDENT_PROGID");
			if (strVIProgID == null || strVIProgID == "")
			{
				wizard.AddSymbol("PROGID_VALID", false);
				wizard.AddSymbol("VERSION_INDEPENDENT_PROGID","");
				wizard.AddSymbol("PROGID","");
			}
			else
			{
				wizard.AddSymbol("PROGID_VALID", true);
				wizard.AddSymbol("PROGID", strVIProgID.substr(0,37) + ".1");
			}
			var strClassName		= wizard.FindSymbol("CLASS_NAME");
			var strHeaderFile		= wizard.FindSymbol("HEADER_FILE");
			var strImplFile			= wizard.FindSymbol("IMPL_FILE");
			var strCoClass			= wizard.FindSymbol("COCLASS");
			var bAttributed			= wizard.FindSymbol("ATTRIBUTED");

			var strProjectRC		= GetProjectFile(selProj, "RC", true, false);

			var strProjectH			= GetProjectFile(selProj, "H", false, IsMFCProject(selProj,false));
			wizard.AddSymbol("PROJECT_HEADER_NAME", strProjectH);

			// Create necessary GUIDS
			CreateGUIDs();

			// open resource file
			var oResHelper = wizard.ResourceHelper;
			oResHelper.OpenResourceFile(strProjectRC);

			if (!bAttributed)
			{
				// Get LibName
				wizard.AddSymbol("LIB_NAME", oCM.IDLLibraries(1).Name);

				// Get LibID
				wizard.AddSymbol("LIBID_REGISTRY_FORMAT", oCM.IDLLibraries(1).Attributes.Find("uuid").Value);

				// Get typelib version
				var oVersion = oCM.IDLLibraries(1).Attributes.Find("version");
				if (oVersion)
				{
					var aryMajorMinor = oVersion.Value.split('.');
					for (var nCntr=0; nCntr<aryMajorMinor.length; nCntr++)
					{
						if (nCntr == 0)
							wizard.AddSymbol("TYPELIB_VERSION_MAJOR", aryMajorMinor[nCntr]);
						else
							wizard.AddSymbol("TYPELIB_VERSION_MINOR", aryMajorMinor[nCntr]);
					}
				}

				// Get AppID
				var strAppID = wizard.GetAppID();
				if (strAppID.length > 0)
				{
					wizard.AddSymbol("APPID_EXIST", true);
					wizard.AddSymbol("APPID_REGISTRY_FORMAT", strAppID);
				}

				// Render proppageco.idl and insert into strProject.idl
				AddCoclassFromFile(oCM, "proppageco.idl");

				SetMergeProxySymbol(selProj);
			}

			var strDLGID = "IDD_" + strUpperShortName;			
			var strRCTemplate = strTemplatePath + "\\propdlg.rc";
			var strRCTemplFile = RenderToTemporaryResourceFile(strRCTemplate);
			var strSymbolValue = oResHelper.AddResource(strDLGID, strRCTemplFile, "DIALOG");
			if (strSymbolValue == null) return;				
			wizard.AddSymbol("IDD_DIALOGID", strSymbolValue.split("=").shift());

			var strTitleID = "IDS_TITLE" + strUpperShortName;
			strSymbolValue = oResHelper.AddResource(strTitleID, wizard.FindSymbol("TITLE"), "STRING");
			if (strSymbolValue == null) return;				
			wizard.AddSymbol("IDS_TITLE", strSymbolValue.split("=").shift());

			var strHelpFileID = "IDS_HELPFILE" + strUpperShortName;
			strSymbolValue = oResHelper.AddResource(strHelpFileID, wizard.FindSymbol("HELP_FILE"), "STRING");
			if (strSymbolValue == null) return;				
			wizard.AddSymbol("IDS_HELPFILE", strSymbolValue.split("=").shift());

			var strDocStringID = "IDS_DOCSTRING" + strUpperShortName;
			strSymbolValue = oResHelper.AddResource(strDocStringID, wizard.FindSymbol("DOC_STRING"), "STRING");
			if (strSymbolValue == null) return;				
			wizard.AddSymbol("IDS_DOCSTRING", strSymbolValue.split("=").shift());

			// Add #include <atlcom.h> to stdafx.h
			if (!DoesIncludeExist(selProj, '<atlcom.h>', "stdafx.h"))
				oCM.AddInclude('<atlcom.h>', "stdafx.h", vsCMAddPositionEnd);
			// Add #include <atlctl.h> to stdafx.h
			if (!DoesIncludeExist(selProj, '<atlctl.h>', "stdafx.h"))
				oCM.AddInclude('<atlctl.h>', "stdafx.h", vsCMAddPositionEnd);


			if (!bAttributed)
			{
				// add RGS file resource
				var strRGSFile = GetUniqueFileName(strProjectPath, CreateASCIIName(strShortName) + ".rgs");
				RenderAddTemplate(wizard, "proppage.rgs", strRGSFile, false, false);
				var strRGSID = "IDR_" + strUpperShortName;
				strSymbolValue = oResHelper.AddResource(strRGSID, strProjectPath + strRGSFile, "REGISTRY");
				if (strSymbolValue == null) return;				
				wizard.AddSymbol("RGS_ID", strSymbolValue.split("=").shift());
			}
			// close resource file
			oResHelper.CloseResourceFile();
		}
		else
		{
			var strShortName = wizard.FindSymbol("SHORT_NAME");
			var L_TRANSACTION_Text = "Add ATL Property Page ";
			oCM.StartTransaction(L_TRANSACTION_Text + strShortName);
			if(!AddATLSupportToProject(selProj))
			{
				oCM.AbortTransaction();
				return;
			}

			var bDLL;
			if (typeDynamicLibrary == selProj.Object.Configurations(1).ConfigurationType)
				bDLL = true;
			else
				bDLL = false;
			wizard.AddSymbol("DLL_APP", bDLL);

			var strProjectName		= wizard.FindSymbol("PROJECT_NAME");
			var strProjectPath		= wizard.FindSymbol("PROJECT_PATH");
			var strTemplatePath		= wizard.FindSymbol("TEMPLATES_PATH");
			var strUpperShortName		= strShortName.toUpperCase();
			strUpperShortName		= CreateSafeName(strUpperShortName); 
			wizard.AddSymbol("UPPER_SHORT_NAME", strUpperShortName);
			var strVIProgID			= wizard.FindSymbol("VERSION_INDEPENDENT_PROGID");
			wizard.AddSymbol("PROGID", strVIProgID.substr(0,37) + ".1");
			var strClassName		= wizard.FindSymbol("CLASS_NAME");
			var strHeaderFile		= wizard.FindSymbol("HEADER_FILE");
			var strImplFile			= wizard.FindSymbol("IMPL_FILE");
			var strCoClass			= wizard.FindSymbol("COCLASS");
			var bAttributed			= wizard.FindSymbol("ATTRIBUTED");
			var strProjectH			= GetProjectFile(selProj, "H", false, IsMFCProject(selProj,false));
			wizard.AddSymbol("PROJECT_HEADER_NAME", strProjectH);
			var strRGSFile = GetUniqueFileName(strProjectPath, CreateASCIIName(strShortName) + ".rgs");
			var strRGSID = "IDR_" + strUpperShortName;
			var strRGSDCOMFile = GetUniqueFileName(strProjectPath, CreateASCIIName(strShortName + "DCOM") + ".rgs");;
			var strRGSDCOMID = "IDR_" + strUpperShortName + "DCOM";

			// Create necessary GUIDS
			CreateGUIDs();

			if (!bAttributed)
			{
				// Get LibName
				wizard.AddSymbol("LIB_NAME", oCM.IDLLibraries(1).Name);

				// Get LibID
				wizard.AddSymbol("LIBID_REGISTRY_FORMAT", oCM.IDLLibraries(1).Attributes.Find("uuid").Value);

				// Get AppID
				var strAppID = wizard.GetAppID();
				if (strAppID.length > 0)
				{
					wizard.AddSymbol("APPID_EXIST", true);
					wizard.AddSymbol("APPID_REGISTRY_FORMAT", strAppID);
				}

				// Render proppageco.idl and insert into strProject.idl
				AddCoclassFromFile(oCM, "proppageco.idl");

				SetMergeProxySymbol(selProj);
			}


			// Add #include <atlcom.h> to stdafx.h
			if (!DoesIncludeExist(selProj, '<atlcom.h>', "stdafx.h"))
				oCM.AddInclude('<atlcom.h>', "stdafx.h", vsCMAddPositionEnd);
			// Add #include <atlctl.h> to stdafx.h
			if (!DoesIncludeExist(selProj, '<atlctl.h>', "stdafx.h"))
				oCM.AddInclude('<atlctl.h>', "stdafx.h", vsCMAddPositionEnd);

			var bDeviceDCOM = ProjectContainsDCOMPlatform(selProj)
			var bDeviceNonDCOM = ProjectContainsNonDCOMPlatform(selProj)


			if (!bAttributed)
			{
				// add RGS file resource
				if (bDeviceDCOM)
				{
					wizard.AddSymbol("SUPPORT_DCOM", true);
					RenderAddTemplate(wizard, "proppage.rgs", strRGSDCOMFile, false, false);
					wizard.AddSymbol("SUPPORT_DCOM", false);
				}
				if (bDeviceNonDCOM)
				{
					wizard.AddSymbol("SUPPORT_NON_DCOM", true);
					RenderAddTemplate(wizard, "proppage.rgs", strRGSFile, false, false);
					wizard.AddSymbol("SUPPORT_NON_DCOM", false);
				}
				var strRGSID = "IDR_" + strUpperShortName;
			}

			wizard.AddSymbol("SUPPORT_DCOM", bDeviceDCOM);
			wizard.AddSymbol("SUPPORT_NON_DCOM", bDeviceNonDCOM);

			var strDLGID = "IDD_" + strUpperShortName;			
			var configs = selProj.Object.Configurations;
			var completedResourceFiles = new Array();
			AddDeviceSymbols(false);

			var ProjWiz = new ActiveXObject("ProjWiz.SDProjWiz2.4");
			var oResHelper = wizard.ResourceHelper;

			for (var nCntr = 1; nCntr <= configs.Count; nCntr++)
			{
				var config = configs.Item(nCntr);
				var strCurrentResource = GetDeviceResourceFileForConfig(config);

				if (completedResourceFiles.join(";").indexOf(strCurrentResource) == -1)
				{
					var platformName = config.Platform.Name;
					var symbol = ProjWiz.GetBaseNativePlatformProperty(platformName, "UISymbol");

					// open resource file
					oResHelper.OpenResourceFile(strCurrentResource);

					// before we add the file to the project, render it to a temporary file. Then we 
					// can add the resulting preprocessed file to the current resource file. At which
					// point we delete the temporary file.
					var strRCTemplate = strTemplatePath;
					if (symbol == "POCKETPC2003_UI_MODEL")
					{
						strRCTemplate += "\\propdlgppc.rc";
					}
					else if (symbol == "SMARTPHONE2003_UI_MODEL")
					{
						strRCTemplate += "\\propdlgsp.rc";
					}
					else
					{
						strRCTemplate += "\\propdlgce.rc";
					}

					var strRCTemplFile = RenderToTemporaryResourceFile(strRCTemplate);

					var strCurrentSymbol = GetDeviceSymbolForConfig(config);
					wizard.AddSymbol(strCurrentSymbol, true);
					var strSymbolValue = oResHelper.AddResource(strDLGID, strRCTemplFile, "DIALOG");		
					if (strSymbolValue == null) return;				
					wizard.AddSymbol("IDD_DIALOGID", strSymbolValue.split("=").shift());

					var strTitleID = "IDS_TITLE" + strShortName;
					strSymbolValue = oResHelper.AddResource(strTitleID, wizard.FindSymbol("TITLE"), "STRING");
					if (strSymbolValue == null) return;				
					wizard.AddSymbol("IDS_TITLE", strSymbolValue.split("=").shift());

					var strHelpFileID = "IDS_HELPFILE" + strShortName;
					strSymbolValue = oResHelper.AddResource(strHelpFileID, wizard.FindSymbol("HELP_FILE"), "STRING");
					if (strSymbolValue == null) return;				
					wizard.AddSymbol("IDS_HELPFILE", strSymbolValue.split("=").shift());

					var strDocStringID = "IDS_DOCSTRING" + strShortName;
					strSymbolValue = oResHelper.AddResource(strDocStringID, wizard.FindSymbol("DOC_STRING"), "STRING");
					if (strSymbolValue == null) return;				
					wizard.AddSymbol("IDS_DOCSTRING", strSymbolValue.split("=").shift());
					
					if (!bAttributed)
					{
						if (bDeviceNonDCOM)
						{
							strSymbolValue = oResHelper.AddResource(strRGSID, strProjectPath + strRGSFile, "REGISTRY");
							if (strSymbolValue == null) return;				
							wizard.AddSymbol("RGS_ID", strSymbolValue.split("=").shift());	   
						}
						if (bDeviceDCOM)
						{
							strSymbolValue = oResHelper.AddResource(strRGSDCOMID, strProjectPath + strRGSDCOMFile, "REGISTRY");
							if (strSymbolValue == null) return;				
							wizard.AddSymbol("RGSDCOM_ID", strSymbolValue.split("=").shift());	
						}
					}

					// close resource file
					oResHelper.CloseResourceFile();
					completedResourceFiles.push(strCurrentResource);
				}
			}
		}

		// Add header
		RenderAddTemplate(wizard, "proppage.h", strHeaderFile, selObj, true);

		// Add CPP
		RenderAddTemplate(wizard, "proppage.cpp", strImplFile, selObj, false);

		oCM.CommitTransaction();
					
		var newClass = oCM.Classes.Find(strClassName);
		if(newClass)
			newClass.StartPoint.TryToShow(vsPaneShowTop);		

	}
	catch(e)
	{
		if (oCM)
			oCM.AbortTransaction();

		if (e.description.length != 0)
			SetErrorInfo(e);
		return e.number
	}
}

function CreateGUIDs()
{
	try
	{
		// create CLSID
		var strRawGUID = wizard.CreateGuid();
		var strFormattedGUID = wizard.FormatGuid(strRawGUID, 0);
		wizard.AddSymbol("CLSID_REGISTRY_FORMAT", strFormattedGUID);
	}
	catch(e)
	{
		throw e;
	}
}

// SIG // Begin signature block
// SIG // MIIXOgYJKoZIhvcNAQcCoIIXKzCCFycCAQExCzAJBgUr
// SIG // DgMCGgUAMGcGCisGAQQBgjcCAQSgWTBXMDIGCisGAQQB
// SIG // gjcCAR4wJAIBAQQQEODJBs441BGiowAQS9NQkAIBAAIB
// SIG // AAIBAAIBAAIBADAhMAkGBSsOAwIaBQAEFN1baEpVS7rM
// SIG // gF0Eb7STjEqETZB7oIISMTCCBGAwggNMoAMCAQICCi6r
// SIG // EdxQ/1ydy8AwCQYFKw4DAh0FADBwMSswKQYDVQQLEyJD
// SIG // b3B5cmlnaHQgKGMpIDE5OTcgTWljcm9zb2Z0IENvcnAu
// SIG // MR4wHAYDVQQLExVNaWNyb3NvZnQgQ29ycG9yYXRpb24x
// SIG // ITAfBgNVBAMTGE1pY3Jvc29mdCBSb290IEF1dGhvcml0
// SIG // eTAeFw0wNzA4MjIyMjMxMDJaFw0xMjA4MjUwNzAwMDBa
// SIG // MHkxCzAJBgNVBAYTAlVTMRMwEQYDVQQIEwpXYXNoaW5n
// SIG // dG9uMRAwDgYDVQQHEwdSZWRtb25kMR4wHAYDVQQKExVN
// SIG // aWNyb3NvZnQgQ29ycG9yYXRpb24xIzAhBgNVBAMTGk1p
// SIG // Y3Jvc29mdCBDb2RlIFNpZ25pbmcgUENBMIIBIjANBgkq
// SIG // hkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAt3l91l2zRTmo
// SIG // NKwx2vklNUl3wPsfnsdFce/RRujUjMNrTFJi9JkCw03Y
// SIG // SWwvJD5lv84jtwtIt3913UW9qo8OUMUlK/Kg5w0jH9FB
// SIG // JPpimc8ZRaWTSh+ZzbMvIsNKLXxv2RUeO4w5EDndvSn0
// SIG // ZjstATL//idIprVsAYec+7qyY3+C+VyggYSFjrDyuJSj
// SIG // zzimUIUXJ4dO3TD2AD30xvk9gb6G7Ww5py409rQurwp9
// SIG // YpF4ZpyYcw2Gr/LE8yC5TxKNY8ss2TJFGe67SpY7UFMY
// SIG // zmZReaqth8hWPp+CUIhuBbE1wXskvVJmPZlOzCt+M26E
// SIG // RwbRntBKhgJuhgCkwIffUwIDAQABo4H6MIH3MBMGA1Ud
// SIG // JQQMMAoGCCsGAQUFBwMDMIGiBgNVHQEEgZowgZeAEFvQ
// SIG // cO9pcp4jUX4Usk2O/8uhcjBwMSswKQYDVQQLEyJDb3B5
// SIG // cmlnaHQgKGMpIDE5OTcgTWljcm9zb2Z0IENvcnAuMR4w
// SIG // HAYDVQQLExVNaWNyb3NvZnQgQ29ycG9yYXRpb24xITAf
// SIG // BgNVBAMTGE1pY3Jvc29mdCBSb290IEF1dGhvcml0eYIP
// SIG // AMEAizw8iBHRPvZj7N9AMA8GA1UdEwEB/wQFMAMBAf8w
// SIG // HQYDVR0OBBYEFMwdznYAcFuv8drETppRRC6jRGPwMAsG
// SIG // A1UdDwQEAwIBhjAJBgUrDgMCHQUAA4IBAQB7q65+Siby
// SIG // zrxOdKJYJ3QqdbOG/atMlHgATenK6xjcacUOonzzAkPG
// SIG // yofM+FPMwp+9Vm/wY0SpRADulsia1Ry4C58ZDZTX2h6t
// SIG // KX3v7aZzrI/eOY49mGq8OG3SiK8j/d/p1mkJkYi9/uEA
// SIG // uzTz93z5EBIuBesplpNCayhxtziP4AcNyV1ozb2AQWtm
// SIG // qLu3u440yvIDEHx69dLgQt97/uHhrP7239UNs3DWkuNP
// SIG // tjiifC3UPds0C2I3Ap+BaiOJ9lxjj7BauznXYIxVhBoz
// SIG // 9TuYoIIMol+Lsyy3oaXLq9ogtr8wGYUgFA0qvFL0QeBe
// SIG // MOOSKGmHwXDi86erzoBCcnYOMIIEejCCA2KgAwIBAgIK
// SIG // YQHPPgAAAAAADzANBgkqhkiG9w0BAQUFADB5MQswCQYD
// SIG // VQQGEwJVUzETMBEGA1UECBMKV2FzaGluZ3RvbjEQMA4G
// SIG // A1UEBxMHUmVkbW9uZDEeMBwGA1UEChMVTWljcm9zb2Z0
// SIG // IENvcnBvcmF0aW9uMSMwIQYDVQQDExpNaWNyb3NvZnQg
// SIG // Q29kZSBTaWduaW5nIFBDQTAeFw0wOTEyMDcyMjQwMjla
// SIG // Fw0xMTAzMDcyMjQwMjlaMIGDMQswCQYDVQQGEwJVUzET
// SIG // MBEGA1UECBMKV2FzaGluZ3RvbjEQMA4GA1UEBxMHUmVk
// SIG // bW9uZDEeMBwGA1UEChMVTWljcm9zb2Z0IENvcnBvcmF0
// SIG // aW9uMQ0wCwYDVQQLEwRNT1BSMR4wHAYDVQQDExVNaWNy
// SIG // b3NvZnQgQ29ycG9yYXRpb24wggEiMA0GCSqGSIb3DQEB
// SIG // AQUAA4IBDwAwggEKAoIBAQC9MIn7RXKoU2ueiU8AI8C+
// SIG // 1B09sVlAOPNzkYIm5pYSAFPZHIIOPM4du733Qo2X1Pw4
// SIG // GuS5+ePs02EDv6DT1nVNXEap7V7w0uJpWxpz6rMcjQTN
// SIG // KUSgZFkvHphdbserGDmCZcSnvKt1iBnqh5cUJrN/Jnak
// SIG // 1Dg5hOOzJtUY+Svp0skWWlQh8peNh4Yp/vRJLOaL+AQ/
// SIG // fc3NlpKGDXED4tD+DEI1/9e4P92ORQp99tdLrVvwdnId
// SIG // dyN9iTXEHF2yUANLR20Hp1WImAaApoGtVE7Ygdb6v0LA
// SIG // Mb5VDZnVU0kSMOvlpYh8XsR6WhSHCLQ3aaDrMiSMCOv5
// SIG // 1BS64PzN6qQVAgMBAAGjgfgwgfUwEwYDVR0lBAwwCgYI
// SIG // KwYBBQUHAwMwHQYDVR0OBBYEFDh4BXPIGzKbX5KGVa+J
// SIG // usaZsXSOMA4GA1UdDwEB/wQEAwIHgDAfBgNVHSMEGDAW
// SIG // gBTMHc52AHBbr/HaxE6aUUQuo0Rj8DBEBgNVHR8EPTA7
// SIG // MDmgN6A1hjNodHRwOi8vY3JsLm1pY3Jvc29mdC5jb20v
// SIG // cGtpL2NybC9wcm9kdWN0cy9DU1BDQS5jcmwwSAYIKwYB
// SIG // BQUHAQEEPDA6MDgGCCsGAQUFBzAChixodHRwOi8vd3d3
// SIG // Lm1pY3Jvc29mdC5jb20vcGtpL2NlcnRzL0NTUENBLmNy
// SIG // dDANBgkqhkiG9w0BAQUFAAOCAQEAKAODqxMN8f4Rb0J2
// SIG // 2EOruMZC+iRlNK51sHEwjpa2g/py5P7NN+c6cJhRIA66
// SIG // cbTJ9NXkiugocHPV7eHCe+7xVjRagILrENdyA+oSTuzd
// SIG // DYx7RE8MYXX9bpwH3c4rWhgNObBg/dr/BKoCo9j6jqO7
// SIG // vcFqVDsxX+QsbsvxTSoc8h52e4avxofWsSrtrMwOwOSf
// SIG // f+jP6IRyVIIYbirInpW0Gh7Bb5PbYqbBS2utye09kuOy
// SIG // L6t6dzlnagB7gp0DEN5jlUkmQt6VIsGHC9AUo1/cczJy
// SIG // Nh7/yCnFJFJPZkjJHR2pxSY5aVBOp+zCBmwuchvxIdpt
// SIG // JEiAgRVAfJ/MdDhKTzCCBJ0wggOFoAMCAQICEGoLmU/A
// SIG // ACWrEdtFH1h6Z6IwDQYJKoZIhvcNAQEFBQAwcDErMCkG
// SIG // A1UECxMiQ29weXJpZ2h0IChjKSAxOTk3IE1pY3Jvc29m
// SIG // dCBDb3JwLjEeMBwGA1UECxMVTWljcm9zb2Z0IENvcnBv
// SIG // cmF0aW9uMSEwHwYDVQQDExhNaWNyb3NvZnQgUm9vdCBB
// SIG // dXRob3JpdHkwHhcNMDYwOTE2MDEwNDQ3WhcNMTkwOTE1
// SIG // MDcwMDAwWjB5MQswCQYDVQQGEwJVUzETMBEGA1UECBMK
// SIG // V2FzaGluZ3RvbjEQMA4GA1UEBxMHUmVkbW9uZDEeMBwG
// SIG // A1UEChMVTWljcm9zb2Z0IENvcnBvcmF0aW9uMSMwIQYD
// SIG // VQQDExpNaWNyb3NvZnQgVGltZXN0YW1waW5nIFBDQTCC
// SIG // ASIwDQYJKoZIhvcNAQEBBQADggEPADCCAQoCggEBANw3
// SIG // bvuvyEJKcRjIzkg+U8D6qxS6LDK7Ek9SyIPtPjPZSTGS
// SIG // KLaRZOAfUIS6wkvRfwX473W+i8eo1a5pcGZ4J2botrfv
// SIG // hbnN7qr9EqQLWSIpL89A2VYEG3a1bWRtSlTb3fHev5+D
// SIG // x4Dff0wCN5T1wJ4IVh5oR83ZwHZcL322JQS0VltqHGP/
// SIG // gHw87tUEJU05d3QHXcJc2IY3LHXJDuoeOQl8dv6dbG56
// SIG // 4Ow+j5eecQ5fKk8YYmAyntKDTisiXGhFi94vhBBQsvm1
// SIG // Go1s7iWbE/jLENeFDvSCdnM2xpV6osxgBuwFsIYzt/iU
// SIG // W4RBhFiFlG6wHyxIzG+cQ+Bq6H8mjmsCAwEAAaOCASgw
// SIG // ggEkMBMGA1UdJQQMMAoGCCsGAQUFBwMIMIGiBgNVHQEE
// SIG // gZowgZeAEFvQcO9pcp4jUX4Usk2O/8uhcjBwMSswKQYD
// SIG // VQQLEyJDb3B5cmlnaHQgKGMpIDE5OTcgTWljcm9zb2Z0
// SIG // IENvcnAuMR4wHAYDVQQLExVNaWNyb3NvZnQgQ29ycG9y
// SIG // YXRpb24xITAfBgNVBAMTGE1pY3Jvc29mdCBSb290IEF1
// SIG // dGhvcml0eYIPAMEAizw8iBHRPvZj7N9AMBAGCSsGAQQB
// SIG // gjcVAQQDAgEAMB0GA1UdDgQWBBRv6E4/l7k0q0uGj7yc
// SIG // 6qw7QUPG0DAZBgkrBgEEAYI3FAIEDB4KAFMAdQBiAEMA
// SIG // QTALBgNVHQ8EBAMCAYYwDwYDVR0TAQH/BAUwAwEB/zAN
// SIG // BgkqhkiG9w0BAQUFAAOCAQEAlE0RMcJ8ULsRjqFhBwEO
// SIG // jHBFje9zVL0/CQUt/7hRU4Uc7TmRt6NWC96Mtjsb0fus
// SIG // p8m3sVEhG28IaX5rA6IiRu1stG18IrhG04TzjQ++B4o2
// SIG // wet+6XBdRZ+S0szO3Y7A4b8qzXzsya4y1Ye5y2PENtEY
// SIG // Ib923juasxtzniGI2LS0ElSM9JzCZUqaKCacYIoPO8cT
// SIG // ZXhIu8+tgzpPsGJY3jDp6Tkd44ny2jmB+RMhjGSAYwYE
// SIG // lvKaAkMve0aIuv8C2WX5St7aA3STswVuDMyd3ChhfEjx
// SIG // F5wRITgCHIesBsWWMrjlQMZTPb2pid7oZjeN9CKWnMyw
// SIG // d1RROtZyRLIj9jCCBKowggOSoAMCAQICCmEGlC0AAAAA
// SIG // AAkwDQYJKoZIhvcNAQEFBQAweTELMAkGA1UEBhMCVVMx
// SIG // EzARBgNVBAgTCldhc2hpbmd0b24xEDAOBgNVBAcTB1Jl
// SIG // ZG1vbmQxHjAcBgNVBAoTFU1pY3Jvc29mdCBDb3Jwb3Jh
// SIG // dGlvbjEjMCEGA1UEAxMaTWljcm9zb2Z0IFRpbWVzdGFt
// SIG // cGluZyBQQ0EwHhcNMDgwNzI1MTkwMjE3WhcNMTMwNzI1
// SIG // MTkxMjE3WjCBszELMAkGA1UEBhMCVVMxEzARBgNVBAgT
// SIG // Cldhc2hpbmd0b24xEDAOBgNVBAcTB1JlZG1vbmQxHjAc
// SIG // BgNVBAoTFU1pY3Jvc29mdCBDb3Jwb3JhdGlvbjENMAsG
// SIG // A1UECxMETU9QUjEnMCUGA1UECxMebkNpcGhlciBEU0Ug
// SIG // RVNOOjdBODItNjg4QS05RjkyMSUwIwYDVQQDExxNaWNy
// SIG // b3NvZnQgVGltZS1TdGFtcCBTZXJ2aWNlMIIBIjANBgkq
// SIG // hkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAlYEKIEIYUXrZ
// SIG // le2b/dyH0fsOjxPqqjcoEnb+TVCrdpcqk0fgqVZpAuWU
// SIG // fk2F239x73UA27tDbPtvrHHwK9F8ks6UF52hxbr5937d
// SIG // YeEtMB6cJi12P+ZGlo6u2Ik32Mzv889bw/xo4PJkj5vo
// SIG // wxL5o76E/NaLzgU9vQF2UCcD+IS3FoaNYL5dKSw8z6X9
// SIG // mFo1HU8WwDjYHmE/PTazVhQVd5U7EPoAsJPiXTerJ7tj
// SIG // LEgUgVXjbOqpK5WNiA5+owCldyQHmCpwA7gqJJCa3sWi
// SIG // Iku/TFkGd1RyQ7A+ZN2ThAhYtv7ph0kJNrOz+DOpfkyi
// SIG // eX8yWSkOnrX14DyeP+xGOwIDAQABo4H4MIH1MB0GA1Ud
// SIG // DgQWBBQolYi/Ajvr2pS6fUYP+sv0fp3/0TAfBgNVHSME
// SIG // GDAWgBRv6E4/l7k0q0uGj7yc6qw7QUPG0DBEBgNVHR8E
// SIG // PTA7MDmgN6A1hjNodHRwOi8vY3JsLm1pY3Jvc29mdC5j
// SIG // b20vcGtpL2NybC9wcm9kdWN0cy90c3BjYS5jcmwwSAYI
// SIG // KwYBBQUHAQEEPDA6MDgGCCsGAQUFBzAChixodHRwOi8v
// SIG // d3d3Lm1pY3Jvc29mdC5jb20vcGtpL2NlcnRzL3RzcGNh
// SIG // LmNydDATBgNVHSUEDDAKBggrBgEFBQcDCDAOBgNVHQ8B
// SIG // Af8EBAMCBsAwDQYJKoZIhvcNAQEFBQADggEBAADurPzi
// SIG // 0ohmyinjWrnNAIJ+F1zFJFkSu6j3a9eH/o3LtXYfGyL2
// SIG // 9+HKtLlBARo3rUg3lnD6zDOnKIy4C7Z0Eyi3s3XhKgni
// SIG // i0/fmD+XtzQSgeoQ3R3cumTPTlA7TIr9Gd0lrtWWh+pL
// SIG // xOXw+UEXXQHrV4h9dnrlb/6HIKyTnIyav18aoBUwJOCi
// SIG // fmGRHSkpw0mQOkODie7e1YPdTyw1O+dBQQGqAAwL8tZJ
// SIG // G85CjXuw8y2NXSnhvo1/kRV2tGD7FCeqbxJjQihYOoo7
// SIG // i0Dkt8XMklccRlZrj8uSTVYFAMr4MEBFTt8ZiL31EPDd
// SIG // Gt8oHrRR8nfgJuO7CYES3B460EUxggR1MIIEcQIBATCB
// SIG // hzB5MQswCQYDVQQGEwJVUzETMBEGA1UECBMKV2FzaGlu
// SIG // Z3RvbjEQMA4GA1UEBxMHUmVkbW9uZDEeMBwGA1UEChMV
// SIG // TWljcm9zb2Z0IENvcnBvcmF0aW9uMSMwIQYDVQQDExpN
// SIG // aWNyb3NvZnQgQ29kZSBTaWduaW5nIFBDQQIKYQHPPgAA
// SIG // AAAADzAJBgUrDgMCGgUAoIGgMBkGCSqGSIb3DQEJAzEM
// SIG // BgorBgEEAYI3AgEEMBwGCisGAQQBgjcCAQsxDjAMBgor
// SIG // BgEEAYI3AgEVMCMGCSqGSIb3DQEJBDEWBBTC477r9FW2
// SIG // 9COcEAqDtHlNisfOOTBABgorBgEEAYI3AgEMMTIwMKAW
// SIG // gBQAZABlAGYAYQB1AGwAdAAuAGoAc6EWgBRodHRwOi8v
// SIG // bWljcm9zb2Z0LmNvbTANBgkqhkiG9w0BAQEFAASCAQAG
// SIG // I5YQUXaK3qSjslihgVkN1jJlM283KBe8kOv6RP3bJNFG
// SIG // tMIOcI2GiQGPKbmFNzTooTv9hrluhX8PjUxpzvJuSaV2
// SIG // G7tIN2vXcQDX/u/smAVpWdTBgXxlDAVsIUY1DmckYXHK
// SIG // A5nrkttfr60ry4U7Y2yrhxD/mqVa3TTnoEy+LFy/Tt+x
// SIG // H24NJysj8jDuccFyoVHzY3AlSaTmSavwAl4DioYEik8e
// SIG // LqKKtuJeUr1Or4D3BMFuK2L0P43DOeCECX66n60AV3Pw
// SIG // d/sh7hk+AOi4VrqsoKfoy848umhz6o2l1NpHzvhYtpZe
// SIG // G6ybNRVvGiB8UC1e6Y65E5IWtE3p/g1MoYICHzCCAhsG
// SIG // CSqGSIb3DQEJBjGCAgwwggIIAgEBMIGHMHkxCzAJBgNV
// SIG // BAYTAlVTMRMwEQYDVQQIEwpXYXNoaW5ndG9uMRAwDgYD
// SIG // VQQHEwdSZWRtb25kMR4wHAYDVQQKExVNaWNyb3NvZnQg
// SIG // Q29ycG9yYXRpb24xIzAhBgNVBAMTGk1pY3Jvc29mdCBU
// SIG // aW1lc3RhbXBpbmcgUENBAgphBpQtAAAAAAAJMAcGBSsO
// SIG // AwIaoF0wGAYJKoZIhvcNAQkDMQsGCSqGSIb3DQEHATAc
// SIG // BgkqhkiG9w0BCQUxDxcNMTAwMzE5MTMwMjE4WjAjBgkq
// SIG // hkiG9w0BCQQxFgQU7iwm06SbTXbCFuMRMSlnpS1iSpkw
// SIG // DQYJKoZIhvcNAQEFBQAEggEABB+9QEgZ2WTOl+Q/Dbn0
// SIG // SWG3tNUyblBAN0SICuru9V6A7GqjE+vSEg3MtHwZ+Tar
// SIG // xldXjcUoAm/AxOU/rDJ8dGKXpiHuM/NkfHBnY5WWLUQs
// SIG // t8B3i84SAITtX7vkdgwkDcvryvM2AEm5H7Xul/kZ4u52
// SIG // VZqzrAmGKWYTKVWYBIMdr/omNz1ZREk5Dx0OcanVQW4+
// SIG // XqY+iYQY7hXs3fM3K+1LwSlXrcCMG4z614Npu+GZzy0C
// SIG // G5UCaygqg6R/b2P1moAjPeVSLQbPoiPW+x1WB5luhAux
// SIG // R8SwikH4dhSSZr5Y63GeGeFezBK5yfXkadq4NkcarEwu
// SIG // WKkhUKwTTICuVw==
// SIG // End signature block
