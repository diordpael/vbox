// Copyright (c) Microsoft Corporation. All rights reserved.

function OnPrep(selProj, selObj)
{
	var L_WizardDialogTitle_Text = "Add Member Variable Wizard";
	return PrepCodeWizard(selProj, L_WizardDialogTitle_Text);
}

function OnFinish(selProj, Class)
{
	var oCM;
	try
	{
		oCM	= selProj.CodeModel;
		//Ask user to add AfxRichEditInit2() call and adds TODO comment.
		//This is not part of the transaction since it has to do with adding
		//the resource itself (better done in resource editor).
		RichEditInit(oCM);

		var strName = wizard.FindSymbol("VARIABLE_NAME");
		var L_TransactionName_Text = "Add Variable ";
		oCM.StartTransaction(L_TransactionName_Text + strName);

		var strComment = wizard.FindSymbol("COMMENT");
		var vsAccess = wizard.FindSymbol("ACCESS");
		var strType = wizard.FindSymbol("VARIABLE_TYPE");

		var strClass = wizard.FindSymbol("PARENT_NAME");
		if(Class.Name != strClass)
		{
			var L_WrongContextObjectErr_Text = "Wrong context object in OnFinish";
			throw (L_WrongContextObjectErr_Text);
		}

		var bUpdate = wizard.FindSymbol("UPDATE");
		var newVariable;
		if (!bUpdate)
		{
			newVariable = Class.AddVariable(strName, strType, vsCMAddPositionEnd, vsAccess);

	//		var extender = newVariable.Extender("MFCVariable");
	//		extender.MaxValue = 100;
	//		extender.MinValue = 1;

			if(strComment != "")
				newVariable.Comment=strComment;

			//prepare the initializer expression for some of the known variable types
				var strInit = strName + '(';

			var NamePos, NameLength, NameLengthPos;
			NameLengthPos = new VBArray(wizard.CppParseTypeString(strType));

			NamePos = NameLengthPos.getItem(0);
			NameLength = NameLengthPos.getItem(1);

			var strTypeTail = strType.substr(NamePos+NameLength);
			var strTypeHead = strType.substr(0, NamePos);

			var indexCloseParen = strTypeTail.indexOf(')');
			var indexOpenBra = strTypeTail.indexOf('[');
			var indexTemplate = strTypeHead.indexOf('<');
			if((indexOpenBra !=-1 && (indexCloseParen==-1 || indexOpenBra<indexCloseParen)) ||
				indexTemplate!=-1)
			{
				strInit = ""; //array type or template type
			}
			else if(strTypeHead.indexOf('*')!=-1)
			{
				// assume it's enough, although could check if * is within innermost ()
				strInit += "NULL"; //pointer type
			}
			else if(strType=="bool")
			{
				strInit += "false";
			}
			else if(strType=="BOOL")
				{
				strInit += "FALSE";
			}
			else if(strType.indexOf("char")!=-1
					|| strType.indexOf("double")!=-1
					|| strType.indexOf("float")!=-1
					|| strType.indexOf("int")!=-1
					|| strType.indexOf("long")!=-1
					|| strType.indexOf("short")!=-1)
			{
				strInit += '0';
			}

			// the following are special type cases custom-initialized as VC6.0 class wizard was doing
			else if(strType.indexOf("INT")!=-1
					|| strType.indexOf("UINT")!=-1
					|| strType.indexOf("LONG")!=-1
					|| strType.indexOf("ULONG")!=-1
					|| strType.indexOf("DWORD")!=-1
					|| strType.indexOf("BYTE")!=-1
					|| strType=="CTime")
			{
				strInit += '0';
			}
			else if(strType=="CString")
			{
				strInit += "_T(\"\")";
			}
			else if(strType=="COleDateTime")
			{
				strInit += "COleDateTime::GetCurrentTime()";
			}
			else if(strType=="COleCurrency")
			{
				strInit += "COleCurrency(0, 0)";
			}
			else if(strType == "GUID")
			{
				strInit += "GUID_NULL";
			}
			else
			{
				// unrecognized type
				strInit = "";
			}
			if(strInit.length)
				strInit += ')';

			if(strType == "DECIMAL")
			{
				strInit = "\tZeroMemory(&" + strName + ", sizeof(DECIMAL));\r\n"
			}
			else if(strType == "FILETIME")
			{
				strInit = "\tZeroMemory(&" + strName + ", sizeof(FILETIME));\r\n"
			}

			// unrecognized variable types (when strInit is empty) do not have initializers, only default constructor
			if(strInit.length)
			{
				var oFunctions = Class.Functions;
				for(var cnt=1; cnt<=oFunctions.Count;cnt++)
				{
					var oFunction = oFunctions(cnt);
					if(oFunction.FunctionKind & vsCMFunctionConstructor)
					{
						try
						{
						    if (strType == "DECIMAL" ||
							    strType == "FILETIME")
						    {
							    oFunction.StartPointOf(vsCMPartBody, vsCMWhereDefinition).CreateEditPoint().Insert(strInit);
							    oCM.Synchronize();
						    }
						    else
						    {
							    // add the initializer into the constructor
							    oFunction.AddInitializer(strInit);
						    }
						}
						catch (e)
						{
					                var L_ErrMsg1_Text = "Unable to update class constructor";
					                wizard.ReportError( L_ErrMsg1_Text );
			        		        throw e;
						}
						
						break;
					}
				}
			}

			if (wizard.FindSymbol("CONTROL_VARIABLE"))
			{
				var strControlType = wizard.FindSymbol("CONTROL_TYPE");

				if (!bUpdate)
				{
					var oDDXFunc = Class.Functions.Find("DoDataExchange");
					var bOCX = false;
					if (typeof(oDDXFunc) != "undefined")
					{
						var strFuncBody = "DDX_";
						if (wizard.FindSymbol("VARIABLE_CATEGORY") == "Control")
						{
							strFuncBody += "Control";
						}
						else if (wizard.FindSymbol("VARIABLE_CATEGORY") != "Value")
						{
							bOCX = true;
							strFuncBody += GetControlDDXType(strType);
						}
						else
						{
							strFuncBody += GetDDXType(strControlType, strType);
						}

						strFuncBody += "(pDX, ";
						strFuncBody += wizard.FindSymbol("CONTROL_NAME");
						strFuncBody += ", ";
						if (bOCX)
						{
							strFuncBody += "DISPID(";
							strFuncBody += wizard.FindSymbol("PROP_DISPID");
							strFuncBody += "), ";
						}
						strFuncBody += wizard.FindSymbol("VARIABLE_NAME");
						strFuncBody += ");\r\n";
						try
						{
							var newFuncBody = oDDXFunc.BodyText + strFuncBody;
							oDDXFunc.BodyText = newFuncBody;
						}
						catch (e)
						{
					                var L_ErrMsg1_Text = "Unable to update DoDataExchange method";
					                wizard.ReportError( L_ErrMsg1_Text );
			        		        throw e;
						}
					}
				}

				if (IsActiveXControl(strControlType) && wizard.FindSymbol("CLASS_NAME"))
				{
					var strHeader = wizard.FindSymbol("HEADER_FILE");
					RenderAddTemplate(wizard, "wrapper.h", strHeader, selProj.ProjectItems, false);
					RenderAddTemplate(wizard, "wrapper.cpp", wizard.FindSymbol("IMPL_FILE"), selProj.ProjectItems, false);
					var strFileName = Class.Location(vsCMWhereDefault);
					if (!DoesIncludeExist(selProj,'"' + strHeader + '"', strFileName))
						oCM.AddInclude("\"" + strHeader + "\"", strFileName, vsCMAddPositionEnd);

					if (wizard.FindSymbol("INCLUDE_PICTURE"))
					{
						oCM.AddInclude("\"_Picture.h\"", strHeader, vsCMAddPositionEnd);
						RenderAddTemplate(wizard, "_Picture.h", "_Picture.h", selProj.ProjectItems, false);
					}

					if (wizard.FindSymbol("INCLUDE_FONT"))
					{
						oCM.AddInclude("\"_Font.h\"", strHeader, vsCMAddPositionEnd);
						RenderAddTemplate(wizard, "_Font.h", "_Font.h", selProj.ProjectItems, false);
					}
				}
			}
		}
		else
		{
			newVariable = Class.Variables.Find(strName);
		}
		var extenderName = ExtenderFromType(strType);

		if (extenderName != "")
		{
			var MinValue = wizard.FindSymbol("MIN_VALUE");
			var MaxValue = wizard.FindSymbol("MAX_VALUE");
			var MaxChars = wizard.FindSymbol("MAX_CHARS");
			if ((extenderName == "MFCDialogStringVariable" && MaxChars != "") ||
				(MinValue != "" || MaxValue != ""))
			{
				var L_TRANSACTION_Text = "Add DDV ";

				var extender = newVariable.Extender(extenderName);
				if (extender)
				{
					if (extenderName == "MFCDialogStringVariable")
					{
						extender.MaxChars = MaxChars;
					}
					else
					{
						if (MinValue != "")
							extender.MinValue = MinValue;
						if (MaxValue != "")
							extender.MaxValue = MaxValue;
					}
				}
			}
		}


		try
		{
			IncludeCodeElementDeclaration(selProj, strType, Class.Location(vsCMWhereDefault));
		}
		catch(e)
		{
			//don't display the error in case base class was not found: the warning was already displayed in HTML script
			//var L_ErrMsg1_Text = "Unable to find base class definition: ";
			//wizard.ReportError( L_ErrMsg1_Text + e.description);
		}

		oCM.CommitTransaction();

	}
	catch(e)
	{
		if (oCM)
			oCM.AbortTransaction();

		if (e.description.length != 0)
			SetErrorInfo(e);
		return e.number
	}
}

function RichEditInit(oCM)
{
    try
	{
        //Is it RichEdit class variable?
	    var strCtrlType = wizard.FindSymbol("CONTROL_TYPE");
        var strAfxInitFuncName = "";
        if (strCtrlType == "RichEdit20A")
        {
			strAfxInitFuncName="AfxInitRichEdit2";

		}
		if (strCtrlType == "RICHEDIT")
		{
			strAfxInitFuncName="AfxInitRichEdit";
		}

        if (strAfxInitFuncName == "")
		{
		   return false;
		}

        var oClasses = oCM.Classes;
        var oInitInstance = false;
        for (var nCntr = 1; nCntr <= oClasses.Count; nCntr++)
        {
            oClass = oClasses(nCntr);
            // look for class derived from CWinApp
            if (oClass.IsDerivedFrom("CWinApp") && oClass.Name != "COleControlModule")
            {
                oInitInstance = oClass.Functions.Find("InitInstance");
                if (oInitInstance)
                {
                    //Search  AfxInitRichEdit or AfxInitRichEdit2 and add TODO: comment if not found.
                    var strBody = oInitInstance.BodyText;
                    var nCurPos = strBody.indexOf(strAfxInitFuncName);
                    if (nCurPos == -1)
                    {
                        var L_InfoRichEdit2_Text = "Using Rich Edit control requires a call to ";
	        			L_InfoRichEdit2_Text =  L_InfoRichEdit2_Text + strAfxInitFuncName + "().";
                        wizard.ReportError(L_InfoRichEdit2_Text);
                        var oEditPoint = oInitInstance.StartPointOf(vsCMPartBody, vsCMWhereDefinition).CreateEditPoint();
                        oEditPoint.Insert("//TODO: call " + strAfxInitFuncName + "() to initialize richedit2 library.\n");
                    }
                }
                break;
            }
        }
    }
    catch(e)
    {
        throw e;
    }
}

function IsActiveXControl(strControlType)
{
	try
	{
		// ActiveXContol type string is persisted as its coclass' GUID, it must contain the '{' and '}' chars
		//
		if(strControlType.indexOf("{") != -1 && strControlType.indexOf("}") != -1)
			return true;
		return false;
	}
	catch(e)
	{
		throw e;
	}
}
function GetControlDDXType(strControlType)
{
	try
	{
		var strDDXControlType = "OCInt";

		switch(strControlType.toLowerCase())
		{
			case "float":
			case "double":
				strDDXControlType = "OCFloat";
				break;
			case "BOOL":
				strDDXControlType = "OCBool";
				break;
			case "OLE_COLOR":
				strDDXControlType = "OCColor";
				break;
			case "short":
				strDDXControlType = "OCShort";
				break;
			case "cstring":
				strDDXControlType = "OCText";
				break;
			default:
				break;
		}
		return strDDXControlType;
	}
	catch(e)
	{
		throw e;
	}
}

function GetDDXType(strControlType, strVarType)
{
	try
	{
		var strDDXType = "Text";

		switch(strControlType)
		{
			case "CHECK":
				strDDXType = "Check";
				break;
			case "RADIO":
				strDDXType = "Radio";
				break;
			case "LISTBOX":
				if (strVarType == "CString")
					strDDXType = "LBString";
				else
					strDDXType = "LBIndex";
				break;
			case "COMBOBOX":
			case "ComboBoxEx32":
				if (strVarType == "CString")
					strDDXType = "CBString";
				else
					strDDXType = "CBIndex";
				break;
			case "SCROLLBAR":
				strDDXType = "Scroll";
				break;
			case "SysMonthCal32":
				strDDXType = "MonthCalCtrl";
				break;
			case "SysDateTimePick32":
				strDDXType = "DateTimeCtrl";
				break;
			case "msctls_trackbar32":
				strDDXType = "Slider";
				break;
			case "SysIPAddress32":
				strDDXType = "IPAddress";
				break;
			case "EDIT":
			case "RICHEDIT":
			case "RichEdit20A":
			case "LTEXT":
			case "CTEXT":
			case "RTEXT":
				strDDXType = "Text";
				break;
			default:
				break;
		}
		return strDDXType;
	}
	catch(e)
	{
		throw e;
	}
}

function ExtenderFromType(strVariableType)
{
	try
	{
		var retExtender = "";

		switch(strVariableType)
		{
		case "BYTE" :
		case "CHAR" :
		case "char" :
		case "short" :
		case "SHORT" :
		case "int" :
		case "INT" :
		case "UINT" :
		case "unsigned int" :
		case "unsigned" :
		case "long" :
		case "LONG" :
		case "DWORD" :
		case "float" :
		case "FLOAT" :
		case "double" :
		case "DOUBLE" :
			retExtender =  "MFCDialogNumberVariable";
			break;

		case "CString" :
			retExtender =  "MFCDialogStringVariable";
			break;

		case "COleCurrency" :
			retExtender =  "MFCDialogCurrencyVariable";
			break;

		case "COleDateTime" :
			retExtender =  "MFCDialogDateTimeVariable";
			break;

		}

		return retExtender;
	}
	catch(e)
	{
		throw e;
	}
}

// SIG // Begin signature block
// SIG // MIIXOgYJKoZIhvcNAQcCoIIXKzCCFycCAQExCzAJBgUr
// SIG // DgMCGgUAMGcGCisGAQQBgjcCAQSgWTBXMDIGCisGAQQB
// SIG // gjcCAR4wJAIBAQQQEODJBs441BGiowAQS9NQkAIBAAIB
// SIG // AAIBAAIBAAIBADAhMAkGBSsOAwIaBQAEFBbL2L8nXYQB
// SIG // iog0OUmAZxhYQKEooIISMTCCBGAwggNMoAMCAQICCi6r
// SIG // EdxQ/1ydy8AwCQYFKw4DAh0FADBwMSswKQYDVQQLEyJD
// SIG // b3B5cmlnaHQgKGMpIDE5OTcgTWljcm9zb2Z0IENvcnAu
// SIG // MR4wHAYDVQQLExVNaWNyb3NvZnQgQ29ycG9yYXRpb24x
// SIG // ITAfBgNVBAMTGE1pY3Jvc29mdCBSb290IEF1dGhvcml0
// SIG // eTAeFw0wNzA4MjIyMjMxMDJaFw0xMjA4MjUwNzAwMDBa
// SIG // MHkxCzAJBgNVBAYTAlVTMRMwEQYDVQQIEwpXYXNoaW5n
// SIG // dG9uMRAwDgYDVQQHEwdSZWRtb25kMR4wHAYDVQQKExVN
// SIG // aWNyb3NvZnQgQ29ycG9yYXRpb24xIzAhBgNVBAMTGk1p
// SIG // Y3Jvc29mdCBDb2RlIFNpZ25pbmcgUENBMIIBIjANBgkq
// SIG // hkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAt3l91l2zRTmo
// SIG // NKwx2vklNUl3wPsfnsdFce/RRujUjMNrTFJi9JkCw03Y
// SIG // SWwvJD5lv84jtwtIt3913UW9qo8OUMUlK/Kg5w0jH9FB
// SIG // JPpimc8ZRaWTSh+ZzbMvIsNKLXxv2RUeO4w5EDndvSn0
// SIG // ZjstATL//idIprVsAYec+7qyY3+C+VyggYSFjrDyuJSj
// SIG // zzimUIUXJ4dO3TD2AD30xvk9gb6G7Ww5py409rQurwp9
// SIG // YpF4ZpyYcw2Gr/LE8yC5TxKNY8ss2TJFGe67SpY7UFMY
// SIG // zmZReaqth8hWPp+CUIhuBbE1wXskvVJmPZlOzCt+M26E
// SIG // RwbRntBKhgJuhgCkwIffUwIDAQABo4H6MIH3MBMGA1Ud
// SIG // JQQMMAoGCCsGAQUFBwMDMIGiBgNVHQEEgZowgZeAEFvQ
// SIG // cO9pcp4jUX4Usk2O/8uhcjBwMSswKQYDVQQLEyJDb3B5
// SIG // cmlnaHQgKGMpIDE5OTcgTWljcm9zb2Z0IENvcnAuMR4w
// SIG // HAYDVQQLExVNaWNyb3NvZnQgQ29ycG9yYXRpb24xITAf
// SIG // BgNVBAMTGE1pY3Jvc29mdCBSb290IEF1dGhvcml0eYIP
// SIG // AMEAizw8iBHRPvZj7N9AMA8GA1UdEwEB/wQFMAMBAf8w
// SIG // HQYDVR0OBBYEFMwdznYAcFuv8drETppRRC6jRGPwMAsG
// SIG // A1UdDwQEAwIBhjAJBgUrDgMCHQUAA4IBAQB7q65+Siby
// SIG // zrxOdKJYJ3QqdbOG/atMlHgATenK6xjcacUOonzzAkPG
// SIG // yofM+FPMwp+9Vm/wY0SpRADulsia1Ry4C58ZDZTX2h6t
// SIG // KX3v7aZzrI/eOY49mGq8OG3SiK8j/d/p1mkJkYi9/uEA
// SIG // uzTz93z5EBIuBesplpNCayhxtziP4AcNyV1ozb2AQWtm
// SIG // qLu3u440yvIDEHx69dLgQt97/uHhrP7239UNs3DWkuNP
// SIG // tjiifC3UPds0C2I3Ap+BaiOJ9lxjj7BauznXYIxVhBoz
// SIG // 9TuYoIIMol+Lsyy3oaXLq9ogtr8wGYUgFA0qvFL0QeBe
// SIG // MOOSKGmHwXDi86erzoBCcnYOMIIEejCCA2KgAwIBAgIK
// SIG // YQHPPgAAAAAADzANBgkqhkiG9w0BAQUFADB5MQswCQYD
// SIG // VQQGEwJVUzETMBEGA1UECBMKV2FzaGluZ3RvbjEQMA4G
// SIG // A1UEBxMHUmVkbW9uZDEeMBwGA1UEChMVTWljcm9zb2Z0
// SIG // IENvcnBvcmF0aW9uMSMwIQYDVQQDExpNaWNyb3NvZnQg
// SIG // Q29kZSBTaWduaW5nIFBDQTAeFw0wOTEyMDcyMjQwMjla
// SIG // Fw0xMTAzMDcyMjQwMjlaMIGDMQswCQYDVQQGEwJVUzET
// SIG // MBEGA1UECBMKV2FzaGluZ3RvbjEQMA4GA1UEBxMHUmVk
// SIG // bW9uZDEeMBwGA1UEChMVTWljcm9zb2Z0IENvcnBvcmF0
// SIG // aW9uMQ0wCwYDVQQLEwRNT1BSMR4wHAYDVQQDExVNaWNy
// SIG // b3NvZnQgQ29ycG9yYXRpb24wggEiMA0GCSqGSIb3DQEB
// SIG // AQUAA4IBDwAwggEKAoIBAQC9MIn7RXKoU2ueiU8AI8C+
// SIG // 1B09sVlAOPNzkYIm5pYSAFPZHIIOPM4du733Qo2X1Pw4
// SIG // GuS5+ePs02EDv6DT1nVNXEap7V7w0uJpWxpz6rMcjQTN
// SIG // KUSgZFkvHphdbserGDmCZcSnvKt1iBnqh5cUJrN/Jnak
// SIG // 1Dg5hOOzJtUY+Svp0skWWlQh8peNh4Yp/vRJLOaL+AQ/
// SIG // fc3NlpKGDXED4tD+DEI1/9e4P92ORQp99tdLrVvwdnId
// SIG // dyN9iTXEHF2yUANLR20Hp1WImAaApoGtVE7Ygdb6v0LA
// SIG // Mb5VDZnVU0kSMOvlpYh8XsR6WhSHCLQ3aaDrMiSMCOv5
// SIG // 1BS64PzN6qQVAgMBAAGjgfgwgfUwEwYDVR0lBAwwCgYI
// SIG // KwYBBQUHAwMwHQYDVR0OBBYEFDh4BXPIGzKbX5KGVa+J
// SIG // usaZsXSOMA4GA1UdDwEB/wQEAwIHgDAfBgNVHSMEGDAW
// SIG // gBTMHc52AHBbr/HaxE6aUUQuo0Rj8DBEBgNVHR8EPTA7
// SIG // MDmgN6A1hjNodHRwOi8vY3JsLm1pY3Jvc29mdC5jb20v
// SIG // cGtpL2NybC9wcm9kdWN0cy9DU1BDQS5jcmwwSAYIKwYB
// SIG // BQUHAQEEPDA6MDgGCCsGAQUFBzAChixodHRwOi8vd3d3
// SIG // Lm1pY3Jvc29mdC5jb20vcGtpL2NlcnRzL0NTUENBLmNy
// SIG // dDANBgkqhkiG9w0BAQUFAAOCAQEAKAODqxMN8f4Rb0J2
// SIG // 2EOruMZC+iRlNK51sHEwjpa2g/py5P7NN+c6cJhRIA66
// SIG // cbTJ9NXkiugocHPV7eHCe+7xVjRagILrENdyA+oSTuzd
// SIG // DYx7RE8MYXX9bpwH3c4rWhgNObBg/dr/BKoCo9j6jqO7
// SIG // vcFqVDsxX+QsbsvxTSoc8h52e4avxofWsSrtrMwOwOSf
// SIG // f+jP6IRyVIIYbirInpW0Gh7Bb5PbYqbBS2utye09kuOy
// SIG // L6t6dzlnagB7gp0DEN5jlUkmQt6VIsGHC9AUo1/cczJy
// SIG // Nh7/yCnFJFJPZkjJHR2pxSY5aVBOp+zCBmwuchvxIdpt
// SIG // JEiAgRVAfJ/MdDhKTzCCBJ0wggOFoAMCAQICEGoLmU/A
// SIG // ACWrEdtFH1h6Z6IwDQYJKoZIhvcNAQEFBQAwcDErMCkG
// SIG // A1UECxMiQ29weXJpZ2h0IChjKSAxOTk3IE1pY3Jvc29m
// SIG // dCBDb3JwLjEeMBwGA1UECxMVTWljcm9zb2Z0IENvcnBv
// SIG // cmF0aW9uMSEwHwYDVQQDExhNaWNyb3NvZnQgUm9vdCBB
// SIG // dXRob3JpdHkwHhcNMDYwOTE2MDEwNDQ3WhcNMTkwOTE1
// SIG // MDcwMDAwWjB5MQswCQYDVQQGEwJVUzETMBEGA1UECBMK
// SIG // V2FzaGluZ3RvbjEQMA4GA1UEBxMHUmVkbW9uZDEeMBwG
// SIG // A1UEChMVTWljcm9zb2Z0IENvcnBvcmF0aW9uMSMwIQYD
// SIG // VQQDExpNaWNyb3NvZnQgVGltZXN0YW1waW5nIFBDQTCC
// SIG // ASIwDQYJKoZIhvcNAQEBBQADggEPADCCAQoCggEBANw3
// SIG // bvuvyEJKcRjIzkg+U8D6qxS6LDK7Ek9SyIPtPjPZSTGS
// SIG // KLaRZOAfUIS6wkvRfwX473W+i8eo1a5pcGZ4J2botrfv
// SIG // hbnN7qr9EqQLWSIpL89A2VYEG3a1bWRtSlTb3fHev5+D
// SIG // x4Dff0wCN5T1wJ4IVh5oR83ZwHZcL322JQS0VltqHGP/
// SIG // gHw87tUEJU05d3QHXcJc2IY3LHXJDuoeOQl8dv6dbG56
// SIG // 4Ow+j5eecQ5fKk8YYmAyntKDTisiXGhFi94vhBBQsvm1
// SIG // Go1s7iWbE/jLENeFDvSCdnM2xpV6osxgBuwFsIYzt/iU
// SIG // W4RBhFiFlG6wHyxIzG+cQ+Bq6H8mjmsCAwEAAaOCASgw
// SIG // ggEkMBMGA1UdJQQMMAoGCCsGAQUFBwMIMIGiBgNVHQEE
// SIG // gZowgZeAEFvQcO9pcp4jUX4Usk2O/8uhcjBwMSswKQYD
// SIG // VQQLEyJDb3B5cmlnaHQgKGMpIDE5OTcgTWljcm9zb2Z0
// SIG // IENvcnAuMR4wHAYDVQQLExVNaWNyb3NvZnQgQ29ycG9y
// SIG // YXRpb24xITAfBgNVBAMTGE1pY3Jvc29mdCBSb290IEF1
// SIG // dGhvcml0eYIPAMEAizw8iBHRPvZj7N9AMBAGCSsGAQQB
// SIG // gjcVAQQDAgEAMB0GA1UdDgQWBBRv6E4/l7k0q0uGj7yc
// SIG // 6qw7QUPG0DAZBgkrBgEEAYI3FAIEDB4KAFMAdQBiAEMA
// SIG // QTALBgNVHQ8EBAMCAYYwDwYDVR0TAQH/BAUwAwEB/zAN
// SIG // BgkqhkiG9w0BAQUFAAOCAQEAlE0RMcJ8ULsRjqFhBwEO
// SIG // jHBFje9zVL0/CQUt/7hRU4Uc7TmRt6NWC96Mtjsb0fus
// SIG // p8m3sVEhG28IaX5rA6IiRu1stG18IrhG04TzjQ++B4o2
// SIG // wet+6XBdRZ+S0szO3Y7A4b8qzXzsya4y1Ye5y2PENtEY
// SIG // Ib923juasxtzniGI2LS0ElSM9JzCZUqaKCacYIoPO8cT
// SIG // ZXhIu8+tgzpPsGJY3jDp6Tkd44ny2jmB+RMhjGSAYwYE
// SIG // lvKaAkMve0aIuv8C2WX5St7aA3STswVuDMyd3ChhfEjx
// SIG // F5wRITgCHIesBsWWMrjlQMZTPb2pid7oZjeN9CKWnMyw
// SIG // d1RROtZyRLIj9jCCBKowggOSoAMCAQICCmEFojAAAAAA
// SIG // AAgwDQYJKoZIhvcNAQEFBQAweTELMAkGA1UEBhMCVVMx
// SIG // EzARBgNVBAgTCldhc2hpbmd0b24xEDAOBgNVBAcTB1Jl
// SIG // ZG1vbmQxHjAcBgNVBAoTFU1pY3Jvc29mdCBDb3Jwb3Jh
// SIG // dGlvbjEjMCEGA1UEAxMaTWljcm9zb2Z0IFRpbWVzdGFt
// SIG // cGluZyBQQ0EwHhcNMDgwNzI1MTkwMTE1WhcNMTMwNzI1
// SIG // MTkxMTE1WjCBszELMAkGA1UEBhMCVVMxEzARBgNVBAgT
// SIG // Cldhc2hpbmd0b24xEDAOBgNVBAcTB1JlZG1vbmQxHjAc
// SIG // BgNVBAoTFU1pY3Jvc29mdCBDb3Jwb3JhdGlvbjENMAsG
// SIG // A1UECxMETU9QUjEnMCUGA1UECxMebkNpcGhlciBEU0Ug
// SIG // RVNOOjg1RDMtMzA1Qy01QkNGMSUwIwYDVQQDExxNaWNy
// SIG // b3NvZnQgVGltZS1TdGFtcCBTZXJ2aWNlMIIBIjANBgkq
// SIG // hkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEA8AQtspbAGoFn
// SIG // JbEmYrMTS84wusASOPyBZTQHxDayJGj2BwTAB5f0t/F7
// SIG // HmIsRtlLpFE0t9Ns7Vo7tIOhRz0RCC41a0XmwjyMAmYC
// SIG // qRhp60rtJyzuPHdbpNRwmUtXhBDQry34iR3m6im058+e
// SIG // BmKnclTCO8bPP7jhsFgQbOWl18PCdTe99IXhgego2Bvx
// SIG // 8q7xgqPW1wOinxWE+z36q+G2MsigAmTz5v8aJnEIU4oV
// SIG // AvKDJ3ZJgnGn760yeMbXbBZPImWXYk1GL/8jr4XspnC9
// SIG // A8va2DIFxSuQQLae1SyGbLfLEzJ9jcZ+rhcvMvxmux2w
// SIG // RVX4rfotZ4NnKZOE0lqhIwIDAQABo4H4MIH1MB0GA1Ud
// SIG // DgQWBBTol/b374zx5mnjWWhO95iKet2bLjAfBgNVHSME
// SIG // GDAWgBRv6E4/l7k0q0uGj7yc6qw7QUPG0DBEBgNVHR8E
// SIG // PTA7MDmgN6A1hjNodHRwOi8vY3JsLm1pY3Jvc29mdC5j
// SIG // b20vcGtpL2NybC9wcm9kdWN0cy90c3BjYS5jcmwwSAYI
// SIG // KwYBBQUHAQEEPDA6MDgGCCsGAQUFBzAChixodHRwOi8v
// SIG // d3d3Lm1pY3Jvc29mdC5jb20vcGtpL2NlcnRzL3RzcGNh
// SIG // LmNydDATBgNVHSUEDDAKBggrBgEFBQcDCDAOBgNVHQ8B
// SIG // Af8EBAMCBsAwDQYJKoZIhvcNAQEFBQADggEBAA0/d1+R
// SIG // PL6lNaTbBQWEH1by75mmxwiNL7PNP3HVhnx3H93rF7K9
// SIG // fOP5mfIKRUitFLtpLPI+Z2JU8u5/JxGSOezO2YdOiPdg
// SIG // RyN7JxVACJ+/DTEEgtg1tgycANOLqnhhxbWIQZ0+NtxY
// SIG // pCebOtq9Bl0UprIPTMGOPIvyYpn4Zu3V8xwosDLbyjEJ
// SIG // vPsiaEZM+tNzIucpjiIA+1a/Bq6BoBW6NPkojh9KYgWh
// SIG // ifWBR+kNkQjXWDuPHmsJaanASHxVgj9fADhDnAbMP9gv
// SIG // v09zCT39ul70x+w3wmRhoE3UPXDMW7ATgcHUozEavWTW
// SIG // ltJ6PypbRlMJPM0D+T9ZAMyJU2ExggR1MIIEcQIBATCB
// SIG // hzB5MQswCQYDVQQGEwJVUzETMBEGA1UECBMKV2FzaGlu
// SIG // Z3RvbjEQMA4GA1UEBxMHUmVkbW9uZDEeMBwGA1UEChMV
// SIG // TWljcm9zb2Z0IENvcnBvcmF0aW9uMSMwIQYDVQQDExpN
// SIG // aWNyb3NvZnQgQ29kZSBTaWduaW5nIFBDQQIKYQHPPgAA
// SIG // AAAADzAJBgUrDgMCGgUAoIGgMBkGCSqGSIb3DQEJAzEM
// SIG // BgorBgEEAYI3AgEEMBwGCisGAQQBgjcCAQsxDjAMBgor
// SIG // BgEEAYI3AgEVMCMGCSqGSIb3DQEJBDEWBBR18VCFRRnH
// SIG // Saj2ydzDk1/h44Ls7zBABgorBgEEAYI3AgEMMTIwMKAW
// SIG // gBQAZABlAGYAYQB1AGwAdAAuAGoAc6EWgBRodHRwOi8v
// SIG // bWljcm9zb2Z0LmNvbTANBgkqhkiG9w0BAQEFAASCAQAm
// SIG // JUlpck1LwgozI+HvpZm60Dhw5gji0Uxv8XL2KbxXSG76
// SIG // uPkYGmla7LJw/qh6JC9yswRz1LVQwjxBT7a0zAT4c/Qe
// SIG // NO15YcNOvpwm1XEHKnEk+5Gfwae6JcofIriiJ6gb7lzy
// SIG // tQkQuOdkWU0gdyNvltZSg+3k5vA9wY8r3sJmAawGcN0e
// SIG // twL/pDO4XnQFI+smyuF+g1w1Ayz3RnQoNaS8BFT5Mapc
// SIG // Y3G4PP+jW2QMdWpC31l2UVILgpDYMDz6TjcSH+eq8xCA
// SIG // ASM8ivL6YbrrvaLJePcl61yI+5Ikj0yEqALvVAa95ett
// SIG // fDJ+nAy72y46RGr9sK2hAMLc76OO4XGwoYICHzCCAhsG
// SIG // CSqGSIb3DQEJBjGCAgwwggIIAgEBMIGHMHkxCzAJBgNV
// SIG // BAYTAlVTMRMwEQYDVQQIEwpXYXNoaW5ndG9uMRAwDgYD
// SIG // VQQHEwdSZWRtb25kMR4wHAYDVQQKExVNaWNyb3NvZnQg
// SIG // Q29ycG9yYXRpb24xIzAhBgNVBAMTGk1pY3Jvc29mdCBU
// SIG // aW1lc3RhbXBpbmcgUENBAgphBaIwAAAAAAAIMAcGBSsO
// SIG // AwIaoF0wGAYJKoZIhvcNAQkDMQsGCSqGSIb3DQEHATAc
// SIG // BgkqhkiG9w0BCQUxDxcNMTAwMzE5MTMwNDA3WjAjBgkq
// SIG // hkiG9w0BCQQxFgQUZFSBOHL2sLVPwMDvP8xIU3dzP8Qw
// SIG // DQYJKoZIhvcNAQEFBQAEggEAgvD2elngfTS8YejDyADP
// SIG // Io2dIm8eR/++sH10vwvhlnlU5Tx18GRhkMYtPqbsg9xF
// SIG // HxK9/WMSIgFF6ZBvJx7+NUT4EcjjAqE/KPtF8BjjVvEM
// SIG // OU8S6MCt37pTbKmcBCGPXzJe2v5BkXLikIMFAtrR2d7L
// SIG // Hz8TQeTc4r8wIzhCF4m72/NBPdMyMN8g2DZlpT0Eo85C
// SIG // JNOopQlQne+xz/LWs97GZrCMLQGB4B+ogRWja/u+JF15
// SIG // Kwdzq6Kc+pPlvpM6wkT7awAIDqzUIqloKofnhFd/Pooe
// SIG // tJWI0GwfxrKLM4Yo5XwsIMQbud3wRphc7cL3lmbtC5q8
// SIG // xMPcSGcouMsm9g==
// SIG // End signature block
