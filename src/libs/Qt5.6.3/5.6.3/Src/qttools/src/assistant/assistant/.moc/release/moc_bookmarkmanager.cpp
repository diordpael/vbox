/****************************************************************************
** Meta object code from reading C++ file 'bookmarkmanager.h'
**
** Created by: The Qt Meta Object Compiler version 67 (Qt 5.6.3)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "../../bookmarkmanager.h"
#include <QtCore/qbytearray.h>
#include <QtCore/qmetatype.h>
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'bookmarkmanager.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 67
#error "This file was generated using the moc from 5.6.3. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
struct qt_meta_stringdata_BookmarkManager_t {
    QByteArrayData data[26];
    char stringdata0[341];
};
#define QT_MOC_LITERAL(idx, ofs, len) \
    Q_STATIC_BYTE_ARRAY_DATA_HEADER_INITIALIZER_WITH_OFFSET(len, \
    qptrdiff(offsetof(qt_meta_stringdata_BookmarkManager_t, stringdata0) + ofs \
        - idx * sizeof(QByteArrayData)) \
    )
static const qt_meta_stringdata_BookmarkManager_t qt_meta_stringdata_BookmarkManager = {
    {
QT_MOC_LITERAL(0, 0, 15), // "BookmarkManager"
QT_MOC_LITERAL(1, 16, 13), // "escapePressed"
QT_MOC_LITERAL(2, 30, 0), // ""
QT_MOC_LITERAL(3, 31, 9), // "setSource"
QT_MOC_LITERAL(4, 41, 3), // "url"
QT_MOC_LITERAL(5, 45, 17), // "setSourceInNewTab"
QT_MOC_LITERAL(6, 63, 11), // "addBookmark"
QT_MOC_LITERAL(7, 75, 5), // "title"
QT_MOC_LITERAL(8, 81, 13), // "setupFinished"
QT_MOC_LITERAL(9, 95, 14), // "removeBookmark"
QT_MOC_LITERAL(10, 110, 15), // "manageBookmarks"
QT_MOC_LITERAL(11, 126, 19), // "refreshBookmarkMenu"
QT_MOC_LITERAL(12, 146, 22), // "refreshBookmarkToolBar"
QT_MOC_LITERAL(13, 169, 14), // "renameBookmark"
QT_MOC_LITERAL(14, 184, 5), // "index"
QT_MOC_LITERAL(15, 190, 19), // "setSourceFromAction"
QT_MOC_LITERAL(16, 210, 8), // "QAction*"
QT_MOC_LITERAL(17, 219, 6), // "action"
QT_MOC_LITERAL(18, 226, 18), // "setSourceFromIndex"
QT_MOC_LITERAL(19, 245, 6), // "newTab"
QT_MOC_LITERAL(20, 252, 12), // "focusInEvent"
QT_MOC_LITERAL(21, 265, 25), // "managerWidgetAboutToClose"
QT_MOC_LITERAL(22, 291, 11), // "textChanged"
QT_MOC_LITERAL(23, 303, 4), // "text"
QT_MOC_LITERAL(24, 308, 26), // "customContextMenuRequested"
QT_MOC_LITERAL(25, 335, 5) // "point"

    },
    "BookmarkManager\0escapePressed\0\0setSource\0"
    "url\0setSourceInNewTab\0addBookmark\0"
    "title\0setupFinished\0removeBookmark\0"
    "manageBookmarks\0refreshBookmarkMenu\0"
    "refreshBookmarkToolBar\0renameBookmark\0"
    "index\0setSourceFromAction\0QAction*\0"
    "action\0setSourceFromIndex\0newTab\0"
    "focusInEvent\0managerWidgetAboutToClose\0"
    "textChanged\0text\0customContextMenuRequested\0"
    "point"
};
#undef QT_MOC_LITERAL

static const uint qt_meta_data_BookmarkManager[] = {

 // content:
       7,       // revision
       0,       // classname
       0,    0, // classinfo
      19,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       3,       // signalCount

 // signals: name, argc, parameters, tag, flags
       1,    0,  109,    2, 0x06 /* Public */,
       3,    1,  110,    2, 0x06 /* Public */,
       5,    1,  113,    2, 0x06 /* Public */,

 // slots: name, argc, parameters, tag, flags
       6,    2,  116,    2, 0x0a /* Public */,
       8,    0,  121,    2, 0x08 /* Private */,
       6,    0,  122,    2, 0x08 /* Private */,
       9,    0,  123,    2, 0x08 /* Private */,
      10,    0,  124,    2, 0x08 /* Private */,
      11,    0,  125,    2, 0x08 /* Private */,
      12,    0,  126,    2, 0x08 /* Private */,
      13,    1,  127,    2, 0x08 /* Private */,
      15,    0,  130,    2, 0x08 /* Private */,
      15,    1,  131,    2, 0x08 /* Private */,
      18,    2,  134,    2, 0x08 /* Private */,
      18,    1,  139,    2, 0x28 /* Private | MethodCloned */,
      20,    0,  142,    2, 0x08 /* Private */,
      21,    0,  143,    2, 0x08 /* Private */,
      22,    1,  144,    2, 0x08 /* Private */,
      24,    1,  147,    2, 0x08 /* Private */,

 // signals: parameters
    QMetaType::Void,
    QMetaType::Void, QMetaType::QUrl,    4,
    QMetaType::Void, QMetaType::QUrl,    4,

 // slots: parameters
    QMetaType::Void, QMetaType::QString, QMetaType::QString,    7,    4,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void, QMetaType::QModelIndex,   14,
    QMetaType::Void,
    QMetaType::Void, 0x80000000 | 16,   17,
    QMetaType::Void, QMetaType::QModelIndex, QMetaType::Bool,   14,   19,
    QMetaType::Void, QMetaType::QModelIndex,   14,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void, QMetaType::QString,   23,
    QMetaType::Void, QMetaType::QPoint,   25,

       0        // eod
};

void BookmarkManager::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        BookmarkManager *_t = static_cast<BookmarkManager *>(_o);
        Q_UNUSED(_t)
        switch (_id) {
        case 0: _t->escapePressed(); break;
        case 1: _t->setSource((*reinterpret_cast< const QUrl(*)>(_a[1]))); break;
        case 2: _t->setSourceInNewTab((*reinterpret_cast< const QUrl(*)>(_a[1]))); break;
        case 3: _t->addBookmark((*reinterpret_cast< const QString(*)>(_a[1])),(*reinterpret_cast< const QString(*)>(_a[2]))); break;
        case 4: _t->setupFinished(); break;
        case 5: _t->addBookmark(); break;
        case 6: _t->removeBookmark(); break;
        case 7: _t->manageBookmarks(); break;
        case 8: _t->refreshBookmarkMenu(); break;
        case 9: _t->refreshBookmarkToolBar(); break;
        case 10: _t->renameBookmark((*reinterpret_cast< const QModelIndex(*)>(_a[1]))); break;
        case 11: _t->setSourceFromAction(); break;
        case 12: _t->setSourceFromAction((*reinterpret_cast< QAction*(*)>(_a[1]))); break;
        case 13: _t->setSourceFromIndex((*reinterpret_cast< const QModelIndex(*)>(_a[1])),(*reinterpret_cast< bool(*)>(_a[2]))); break;
        case 14: _t->setSourceFromIndex((*reinterpret_cast< const QModelIndex(*)>(_a[1]))); break;
        case 15: _t->focusInEvent(); break;
        case 16: _t->managerWidgetAboutToClose(); break;
        case 17: _t->textChanged((*reinterpret_cast< const QString(*)>(_a[1]))); break;
        case 18: _t->customContextMenuRequested((*reinterpret_cast< const QPoint(*)>(_a[1]))); break;
        default: ;
        }
    } else if (_c == QMetaObject::IndexOfMethod) {
        int *result = reinterpret_cast<int *>(_a[0]);
        void **func = reinterpret_cast<void **>(_a[1]);
        {
            typedef void (BookmarkManager::*_t)();
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&BookmarkManager::escapePressed)) {
                *result = 0;
                return;
            }
        }
        {
            typedef void (BookmarkManager::*_t)(const QUrl & );
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&BookmarkManager::setSource)) {
                *result = 1;
                return;
            }
        }
        {
            typedef void (BookmarkManager::*_t)(const QUrl & );
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&BookmarkManager::setSourceInNewTab)) {
                *result = 2;
                return;
            }
        }
    }
}

const QMetaObject BookmarkManager::staticMetaObject = {
    { &QObject::staticMetaObject, qt_meta_stringdata_BookmarkManager.data,
      qt_meta_data_BookmarkManager,  qt_static_metacall, Q_NULLPTR, Q_NULLPTR}
};


const QMetaObject *BookmarkManager::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->dynamicMetaObject() : &staticMetaObject;
}

void *BookmarkManager::qt_metacast(const char *_clname)
{
    if (!_clname) return Q_NULLPTR;
    if (!strcmp(_clname, qt_meta_stringdata_BookmarkManager.stringdata0))
        return static_cast<void*>(const_cast< BookmarkManager*>(this));
    return QObject::qt_metacast(_clname);
}

int BookmarkManager::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QObject::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 19)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 19;
    } else if (_c == QMetaObject::RegisterMethodArgumentMetaType) {
        if (_id < 19)
            *reinterpret_cast<int*>(_a[0]) = -1;
        _id -= 19;
    }
    return _id;
}

// SIGNAL 0
void BookmarkManager::escapePressed()
{
    QMetaObject::activate(this, &staticMetaObject, 0, Q_NULLPTR);
}

// SIGNAL 1
void BookmarkManager::setSource(const QUrl & _t1)
{
    void *_a[] = { Q_NULLPTR, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 1, _a);
}

// SIGNAL 2
void BookmarkManager::setSourceInNewTab(const QUrl & _t1)
{
    void *_a[] = { Q_NULLPTR, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 2, _a);
}
struct qt_meta_stringdata_BookmarkManager__BookmarkWidget_t {
    QByteArrayData data[3];
    char stringdata0[46];
};
#define QT_MOC_LITERAL(idx, ofs, len) \
    Q_STATIC_BYTE_ARRAY_DATA_HEADER_INITIALIZER_WITH_OFFSET(len, \
    qptrdiff(offsetof(qt_meta_stringdata_BookmarkManager__BookmarkWidget_t, stringdata0) + ofs \
        - idx * sizeof(QByteArrayData)) \
    )
static const qt_meta_stringdata_BookmarkManager__BookmarkWidget_t qt_meta_stringdata_BookmarkManager__BookmarkWidget = {
    {
QT_MOC_LITERAL(0, 0, 31), // "BookmarkManager::BookmarkWidget"
QT_MOC_LITERAL(1, 32, 12), // "focusInEvent"
QT_MOC_LITERAL(2, 45, 0) // ""

    },
    "BookmarkManager::BookmarkWidget\0"
    "focusInEvent\0"
};
#undef QT_MOC_LITERAL

static const uint qt_meta_data_BookmarkManager__BookmarkWidget[] = {

 // content:
       7,       // revision
       0,       // classname
       0,    0, // classinfo
       1,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       1,       // signalCount

 // signals: name, argc, parameters, tag, flags
       1,    0,   19,    2, 0x06 /* Public */,

 // signals: parameters
    QMetaType::Void,

       0        // eod
};

void BookmarkManager::BookmarkWidget::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        BookmarkWidget *_t = static_cast<BookmarkWidget *>(_o);
        Q_UNUSED(_t)
        switch (_id) {
        case 0: _t->focusInEvent(); break;
        default: ;
        }
    } else if (_c == QMetaObject::IndexOfMethod) {
        int *result = reinterpret_cast<int *>(_a[0]);
        void **func = reinterpret_cast<void **>(_a[1]);
        {
            typedef void (BookmarkWidget::*_t)();
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&BookmarkWidget::focusInEvent)) {
                *result = 0;
                return;
            }
        }
    }
    Q_UNUSED(_a);
}

const QMetaObject BookmarkManager::BookmarkWidget::staticMetaObject = {
    { &QWidget::staticMetaObject, qt_meta_stringdata_BookmarkManager__BookmarkWidget.data,
      qt_meta_data_BookmarkManager__BookmarkWidget,  qt_static_metacall, Q_NULLPTR, Q_NULLPTR}
};


const QMetaObject *BookmarkManager::BookmarkWidget::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->dynamicMetaObject() : &staticMetaObject;
}

void *BookmarkManager::BookmarkWidget::qt_metacast(const char *_clname)
{
    if (!_clname) return Q_NULLPTR;
    if (!strcmp(_clname, qt_meta_stringdata_BookmarkManager__BookmarkWidget.stringdata0))
        return static_cast<void*>(const_cast< BookmarkWidget*>(this));
    return QWidget::qt_metacast(_clname);
}

int BookmarkManager::BookmarkWidget::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QWidget::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 1)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 1;
    } else if (_c == QMetaObject::RegisterMethodArgumentMetaType) {
        if (_id < 1)
            *reinterpret_cast<int*>(_a[0]) = -1;
        _id -= 1;
    }
    return _id;
}

// SIGNAL 0
void BookmarkManager::BookmarkWidget::focusInEvent()
{
    QMetaObject::activate(this, &staticMetaObject, 0, Q_NULLPTR);
}
struct qt_meta_stringdata_BookmarkManager__BookmarkTreeView_t {
    QByteArrayData data[4];
    char stringdata0[57];
};
#define QT_MOC_LITERAL(idx, ofs, len) \
    Q_STATIC_BYTE_ARRAY_DATA_HEADER_INITIALIZER_WITH_OFFSET(len, \
    qptrdiff(offsetof(qt_meta_stringdata_BookmarkManager__BookmarkTreeView_t, stringdata0) + ofs \
        - idx * sizeof(QByteArrayData)) \
    )
static const qt_meta_stringdata_BookmarkManager__BookmarkTreeView_t qt_meta_stringdata_BookmarkManager__BookmarkTreeView = {
    {
QT_MOC_LITERAL(0, 0, 33), // "BookmarkManager::BookmarkTree..."
QT_MOC_LITERAL(1, 34, 15), // "setExpandedData"
QT_MOC_LITERAL(2, 50, 0), // ""
QT_MOC_LITERAL(3, 51, 5) // "index"

    },
    "BookmarkManager::BookmarkTreeView\0"
    "setExpandedData\0\0index"
};
#undef QT_MOC_LITERAL

static const uint qt_meta_data_BookmarkManager__BookmarkTreeView[] = {

 // content:
       7,       // revision
       0,       // classname
       0,    0, // classinfo
       1,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       0,       // signalCount

 // slots: name, argc, parameters, tag, flags
       1,    1,   19,    2, 0x08 /* Private */,

 // slots: parameters
    QMetaType::Void, QMetaType::QModelIndex,    3,

       0        // eod
};

void BookmarkManager::BookmarkTreeView::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        BookmarkTreeView *_t = static_cast<BookmarkTreeView *>(_o);
        Q_UNUSED(_t)
        switch (_id) {
        case 0: _t->setExpandedData((*reinterpret_cast< const QModelIndex(*)>(_a[1]))); break;
        default: ;
        }
    }
}

const QMetaObject BookmarkManager::BookmarkTreeView::staticMetaObject = {
    { &QTreeView::staticMetaObject, qt_meta_stringdata_BookmarkManager__BookmarkTreeView.data,
      qt_meta_data_BookmarkManager__BookmarkTreeView,  qt_static_metacall, Q_NULLPTR, Q_NULLPTR}
};


const QMetaObject *BookmarkManager::BookmarkTreeView::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->dynamicMetaObject() : &staticMetaObject;
}

void *BookmarkManager::BookmarkTreeView::qt_metacast(const char *_clname)
{
    if (!_clname) return Q_NULLPTR;
    if (!strcmp(_clname, qt_meta_stringdata_BookmarkManager__BookmarkTreeView.stringdata0))
        return static_cast<void*>(const_cast< BookmarkTreeView*>(this));
    return QTreeView::qt_metacast(_clname);
}

int BookmarkManager::BookmarkTreeView::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QTreeView::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 1)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 1;
    } else if (_c == QMetaObject::RegisterMethodArgumentMetaType) {
        if (_id < 1)
            *reinterpret_cast<int*>(_a[0]) = -1;
        _id -= 1;
    }
    return _id;
}
QT_END_MOC_NAMESPACE
