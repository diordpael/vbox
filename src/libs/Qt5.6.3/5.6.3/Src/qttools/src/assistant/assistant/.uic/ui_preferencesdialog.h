/********************************************************************************
** Form generated from reading UI file 'preferencesdialog.ui'
**
** Created by: Qt User Interface Compiler version 5.6.3
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_PREFERENCESDIALOG_H
#define UI_PREFERENCESDIALOG_H

#include <QtCore/QVariant>
#include <QtWidgets/QAction>
#include <QtWidgets/QApplication>
#include <QtWidgets/QButtonGroup>
#include <QtWidgets/QCheckBox>
#include <QtWidgets/QComboBox>
#include <QtWidgets/QDialog>
#include <QtWidgets/QDialogButtonBox>
#include <QtWidgets/QGridLayout>
#include <QtWidgets/QGroupBox>
#include <QtWidgets/QHBoxLayout>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QLabel>
#include <QtWidgets/QLineEdit>
#include <QtWidgets/QListWidget>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QSpacerItem>
#include <QtWidgets/QStackedWidget>
#include <QtWidgets/QTabWidget>
#include <QtWidgets/QTreeWidget>
#include <QtWidgets/QVBoxLayout>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_PreferencesDialogClass
{
public:
    QVBoxLayout *vboxLayout;
    QTabWidget *tabWidget;
    QWidget *fontsTab;
    QGridLayout *gridLayout;
    QHBoxLayout *hboxLayout;
    QLabel *fontLabel;
    QComboBox *comboBox;
    QStackedWidget *stackedWidget_2;
    QWidget *page_4;
    QWidget *filtersTab;
    QGridLayout *gridLayout1;
    QLabel *label;
    QLabel *label_2;
    QListWidget *filterWidget;
    QTreeWidget *attributeWidget;
    QPushButton *filterAddButton;
    QPushButton *filterRemoveButton;
    QWidget *docsTab;
    QVBoxLayout *vboxLayout1;
    QLabel *label_3;
    QHBoxLayout *hboxLayout1;
    QListWidget *registeredDocsListWidget;
    QVBoxLayout *vboxLayout2;
    QPushButton *docAddButton;
    QPushButton *docRemoveButton;
    QSpacerItem *spacerItem;
    QWidget *optionsTab;
    QVBoxLayout *verticalLayout_3;
    QGroupBox *groupBox_2;
    QHBoxLayout *horizontalLayout_3;
    QLabel *label_5;
    QComboBox *helpStartComboBox;
    QSpacerItem *horizontalSpacer_3;
    QGroupBox *groupBox;
    QVBoxLayout *verticalLayout;
    QHBoxLayout *horizontalLayout_2;
    QLabel *label_4;
    QLineEdit *homePageLineEdit;
    QHBoxLayout *horizontalLayout;
    QSpacerItem *horizontalSpacer;
    QPushButton *currentPageButton;
    QPushButton *blankPageButton;
    QPushButton *defaultPageButton;
    QGroupBox *groupBox_3;
    QVBoxLayout *verticalLayout_2;
    QCheckBox *showTabs;
    QSpacerItem *verticalSpacer_2;
    QHBoxLayout *hboxLayout2;
    QSpacerItem *spacerItem1;
    QDialogButtonBox *buttonBox;

    void setupUi(QDialog *PreferencesDialogClass)
    {
        if (PreferencesDialogClass->objectName().isEmpty())
            PreferencesDialogClass->setObjectName(QStringLiteral("PreferencesDialogClass"));
        PreferencesDialogClass->resize(375, 275);
        vboxLayout = new QVBoxLayout(PreferencesDialogClass);
        vboxLayout->setSpacing(6);
        vboxLayout->setContentsMargins(11, 11, 11, 11);
        vboxLayout->setObjectName(QStringLiteral("vboxLayout"));
        tabWidget = new QTabWidget(PreferencesDialogClass);
        tabWidget->setObjectName(QStringLiteral("tabWidget"));
        fontsTab = new QWidget();
        fontsTab->setObjectName(QStringLiteral("fontsTab"));
        gridLayout = new QGridLayout(fontsTab);
        gridLayout->setSpacing(6);
        gridLayout->setContentsMargins(11, 11, 11, 11);
        gridLayout->setObjectName(QStringLiteral("gridLayout"));
        hboxLayout = new QHBoxLayout();
        hboxLayout->setSpacing(6);
        hboxLayout->setObjectName(QStringLiteral("hboxLayout"));
        fontLabel = new QLabel(fontsTab);
        fontLabel->setObjectName(QStringLiteral("fontLabel"));
        QSizePolicy sizePolicy(QSizePolicy::Maximum, QSizePolicy::Preferred);
        sizePolicy.setHorizontalStretch(0);
        sizePolicy.setVerticalStretch(0);
        sizePolicy.setHeightForWidth(fontLabel->sizePolicy().hasHeightForWidth());
        fontLabel->setSizePolicy(sizePolicy);

        hboxLayout->addWidget(fontLabel);

        comboBox = new QComboBox(fontsTab);
        comboBox->setObjectName(QStringLiteral("comboBox"));

        hboxLayout->addWidget(comboBox);


        gridLayout->addLayout(hboxLayout, 0, 0, 1, 1);

        stackedWidget_2 = new QStackedWidget(fontsTab);
        stackedWidget_2->setObjectName(QStringLiteral("stackedWidget_2"));
        page_4 = new QWidget();
        page_4->setObjectName(QStringLiteral("page_4"));
        stackedWidget_2->addWidget(page_4);

        gridLayout->addWidget(stackedWidget_2, 1, 0, 1, 1);

        tabWidget->addTab(fontsTab, QString());
        filtersTab = new QWidget();
        filtersTab->setObjectName(QStringLiteral("filtersTab"));
        gridLayout1 = new QGridLayout(filtersTab);
        gridLayout1->setSpacing(6);
        gridLayout1->setContentsMargins(11, 11, 11, 11);
        gridLayout1->setObjectName(QStringLiteral("gridLayout1"));
        label = new QLabel(filtersTab);
        label->setObjectName(QStringLiteral("label"));

        gridLayout1->addWidget(label, 0, 0, 1, 2);

        label_2 = new QLabel(filtersTab);
        label_2->setObjectName(QStringLiteral("label_2"));
        label_2->setFrameShape(QFrame::NoFrame);

        gridLayout1->addWidget(label_2, 0, 2, 1, 1);

        filterWidget = new QListWidget(filtersTab);
        filterWidget->setObjectName(QStringLiteral("filterWidget"));

        gridLayout1->addWidget(filterWidget, 1, 0, 1, 2);

        attributeWidget = new QTreeWidget(filtersTab);
        attributeWidget->setObjectName(QStringLiteral("attributeWidget"));

        gridLayout1->addWidget(attributeWidget, 1, 2, 2, 1);

        filterAddButton = new QPushButton(filtersTab);
        filterAddButton->setObjectName(QStringLiteral("filterAddButton"));

        gridLayout1->addWidget(filterAddButton, 2, 0, 1, 1);

        filterRemoveButton = new QPushButton(filtersTab);
        filterRemoveButton->setObjectName(QStringLiteral("filterRemoveButton"));

        gridLayout1->addWidget(filterRemoveButton, 2, 1, 1, 1);

        tabWidget->addTab(filtersTab, QString());
        docsTab = new QWidget();
        docsTab->setObjectName(QStringLiteral("docsTab"));
        vboxLayout1 = new QVBoxLayout(docsTab);
        vboxLayout1->setSpacing(6);
        vboxLayout1->setContentsMargins(11, 11, 11, 11);
        vboxLayout1->setObjectName(QStringLiteral("vboxLayout1"));
        label_3 = new QLabel(docsTab);
        label_3->setObjectName(QStringLiteral("label_3"));

        vboxLayout1->addWidget(label_3);

        hboxLayout1 = new QHBoxLayout();
        hboxLayout1->setSpacing(6);
        hboxLayout1->setContentsMargins(0, 0, 0, 0);
        hboxLayout1->setObjectName(QStringLiteral("hboxLayout1"));
        registeredDocsListWidget = new QListWidget(docsTab);
        registeredDocsListWidget->setObjectName(QStringLiteral("registeredDocsListWidget"));
        registeredDocsListWidget->setSelectionMode(QAbstractItemView::ExtendedSelection);

        hboxLayout1->addWidget(registeredDocsListWidget);

        vboxLayout2 = new QVBoxLayout();
        vboxLayout2->setSpacing(6);
        vboxLayout2->setContentsMargins(0, 0, 0, 0);
        vboxLayout2->setObjectName(QStringLiteral("vboxLayout2"));
        docAddButton = new QPushButton(docsTab);
        docAddButton->setObjectName(QStringLiteral("docAddButton"));

        vboxLayout2->addWidget(docAddButton);

        docRemoveButton = new QPushButton(docsTab);
        docRemoveButton->setObjectName(QStringLiteral("docRemoveButton"));

        vboxLayout2->addWidget(docRemoveButton);

        spacerItem = new QSpacerItem(20, 40, QSizePolicy::Minimum, QSizePolicy::Expanding);

        vboxLayout2->addItem(spacerItem);


        hboxLayout1->addLayout(vboxLayout2);


        vboxLayout1->addLayout(hboxLayout1);

        tabWidget->addTab(docsTab, QString());
        optionsTab = new QWidget();
        optionsTab->setObjectName(QStringLiteral("optionsTab"));
        verticalLayout_3 = new QVBoxLayout(optionsTab);
        verticalLayout_3->setSpacing(6);
        verticalLayout_3->setContentsMargins(11, 11, 11, 11);
        verticalLayout_3->setObjectName(QStringLiteral("verticalLayout_3"));
        groupBox_2 = new QGroupBox(optionsTab);
        groupBox_2->setObjectName(QStringLiteral("groupBox_2"));
        horizontalLayout_3 = new QHBoxLayout(groupBox_2);
        horizontalLayout_3->setSpacing(6);
        horizontalLayout_3->setContentsMargins(11, 11, 11, 11);
        horizontalLayout_3->setObjectName(QStringLiteral("horizontalLayout_3"));
        label_5 = new QLabel(groupBox_2);
        label_5->setObjectName(QStringLiteral("label_5"));
        QSizePolicy sizePolicy1(QSizePolicy::Preferred, QSizePolicy::Fixed);
        sizePolicy1.setHorizontalStretch(0);
        sizePolicy1.setVerticalStretch(0);
        sizePolicy1.setHeightForWidth(label_5->sizePolicy().hasHeightForWidth());
        label_5->setSizePolicy(sizePolicy1);

        horizontalLayout_3->addWidget(label_5);

        helpStartComboBox = new QComboBox(groupBox_2);
        helpStartComboBox->setObjectName(QStringLiteral("helpStartComboBox"));
        sizePolicy1.setHeightForWidth(helpStartComboBox->sizePolicy().hasHeightForWidth());
        helpStartComboBox->setSizePolicy(sizePolicy1);

        horizontalLayout_3->addWidget(helpStartComboBox);

        horizontalSpacer_3 = new QSpacerItem(54, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout_3->addItem(horizontalSpacer_3);


        verticalLayout_3->addWidget(groupBox_2);

        groupBox = new QGroupBox(optionsTab);
        groupBox->setObjectName(QStringLiteral("groupBox"));
        verticalLayout = new QVBoxLayout(groupBox);
        verticalLayout->setSpacing(6);
        verticalLayout->setContentsMargins(11, 11, 11, 11);
        verticalLayout->setObjectName(QStringLiteral("verticalLayout"));
        horizontalLayout_2 = new QHBoxLayout();
        horizontalLayout_2->setSpacing(6);
        horizontalLayout_2->setObjectName(QStringLiteral("horizontalLayout_2"));
        label_4 = new QLabel(groupBox);
        label_4->setObjectName(QStringLiteral("label_4"));

        horizontalLayout_2->addWidget(label_4);

        homePageLineEdit = new QLineEdit(groupBox);
        homePageLineEdit->setObjectName(QStringLiteral("homePageLineEdit"));

        horizontalLayout_2->addWidget(homePageLineEdit);


        verticalLayout->addLayout(horizontalLayout_2);

        horizontalLayout = new QHBoxLayout();
        horizontalLayout->setSpacing(6);
        horizontalLayout->setObjectName(QStringLiteral("horizontalLayout"));
        horizontalSpacer = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout->addItem(horizontalSpacer);

        currentPageButton = new QPushButton(groupBox);
        currentPageButton->setObjectName(QStringLiteral("currentPageButton"));

        horizontalLayout->addWidget(currentPageButton);

        blankPageButton = new QPushButton(groupBox);
        blankPageButton->setObjectName(QStringLiteral("blankPageButton"));

        horizontalLayout->addWidget(blankPageButton);

        defaultPageButton = new QPushButton(groupBox);
        defaultPageButton->setObjectName(QStringLiteral("defaultPageButton"));

        horizontalLayout->addWidget(defaultPageButton);


        verticalLayout->addLayout(horizontalLayout);


        verticalLayout_3->addWidget(groupBox);

        groupBox_3 = new QGroupBox(optionsTab);
        groupBox_3->setObjectName(QStringLiteral("groupBox_3"));
        verticalLayout_2 = new QVBoxLayout(groupBox_3);
        verticalLayout_2->setSpacing(6);
        verticalLayout_2->setContentsMargins(11, 11, 11, 11);
        verticalLayout_2->setObjectName(QStringLiteral("verticalLayout_2"));
        showTabs = new QCheckBox(groupBox_3);
        showTabs->setObjectName(QStringLiteral("showTabs"));

        verticalLayout_2->addWidget(showTabs);


        verticalLayout_3->addWidget(groupBox_3);

        verticalSpacer_2 = new QSpacerItem(20, 72, QSizePolicy::Minimum, QSizePolicy::Expanding);

        verticalLayout_3->addItem(verticalSpacer_2);

        tabWidget->addTab(optionsTab, QString());

        vboxLayout->addWidget(tabWidget);

        hboxLayout2 = new QHBoxLayout();
        hboxLayout2->setSpacing(6);
        hboxLayout2->setContentsMargins(0, 0, 0, 0);
        hboxLayout2->setObjectName(QStringLiteral("hboxLayout2"));
        spacerItem1 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        hboxLayout2->addItem(spacerItem1);

        buttonBox = new QDialogButtonBox(PreferencesDialogClass);
        buttonBox->setObjectName(QStringLiteral("buttonBox"));
        buttonBox->setOrientation(Qt::Horizontal);
        buttonBox->setStandardButtons(QDialogButtonBox::Cancel|QDialogButtonBox::Ok);

        hboxLayout2->addWidget(buttonBox);


        vboxLayout->addLayout(hboxLayout2);


        retranslateUi(PreferencesDialogClass);
        QObject::connect(comboBox, SIGNAL(currentIndexChanged(int)), stackedWidget_2, SLOT(setCurrentIndex(int)));

        tabWidget->setCurrentIndex(0);
        stackedWidget_2->setCurrentIndex(0);


        QMetaObject::connectSlotsByName(PreferencesDialogClass);
    } // setupUi

    void retranslateUi(QDialog *PreferencesDialogClass)
    {
        PreferencesDialogClass->setWindowTitle(QApplication::translate("PreferencesDialogClass", "Preferences", Q_NULLPTR));
        fontLabel->setText(QApplication::translate("PreferencesDialogClass", "Font settings:", Q_NULLPTR));
        comboBox->clear();
        comboBox->insertItems(0, QStringList()
         << QApplication::translate("PreferencesDialogClass", "Browser", Q_NULLPTR)
         << QApplication::translate("PreferencesDialogClass", "Application", Q_NULLPTR)
        );
        tabWidget->setTabText(tabWidget->indexOf(fontsTab), QApplication::translate("PreferencesDialogClass", "Fonts", Q_NULLPTR));
        label->setText(QApplication::translate("PreferencesDialogClass", "Filter:", Q_NULLPTR));
        label_2->setText(QApplication::translate("PreferencesDialogClass", "Attributes:", Q_NULLPTR));
        QTreeWidgetItem *___qtreewidgetitem = attributeWidget->headerItem();
        ___qtreewidgetitem->setText(0, QApplication::translate("PreferencesDialogClass", "1", Q_NULLPTR));
        filterAddButton->setText(QApplication::translate("PreferencesDialogClass", "Add", Q_NULLPTR));
        filterRemoveButton->setText(QApplication::translate("PreferencesDialogClass", "Remove", Q_NULLPTR));
        tabWidget->setTabText(tabWidget->indexOf(filtersTab), QApplication::translate("PreferencesDialogClass", "Filters", Q_NULLPTR));
        label_3->setText(QApplication::translate("PreferencesDialogClass", "Registered Documentation:", Q_NULLPTR));
        docAddButton->setText(QApplication::translate("PreferencesDialogClass", "Add...", Q_NULLPTR));
        docRemoveButton->setText(QApplication::translate("PreferencesDialogClass", "Remove", Q_NULLPTR));
        tabWidget->setTabText(tabWidget->indexOf(docsTab), QApplication::translate("PreferencesDialogClass", "Documentation", Q_NULLPTR));
        groupBox_2->setTitle(QString());
        label_5->setText(QApplication::translate("PreferencesDialogClass", "On help start:", Q_NULLPTR));
        helpStartComboBox->clear();
        helpStartComboBox->insertItems(0, QStringList()
         << QApplication::translate("PreferencesDialogClass", "Show my home page", Q_NULLPTR)
         << QApplication::translate("PreferencesDialogClass", "Show a blank page", Q_NULLPTR)
         << QApplication::translate("PreferencesDialogClass", "Show my tabs from last session", Q_NULLPTR)
        );
        groupBox->setTitle(QString());
        label_4->setText(QApplication::translate("PreferencesDialogClass", "Homepage", Q_NULLPTR));
        currentPageButton->setText(QApplication::translate("PreferencesDialogClass", "Current Page", Q_NULLPTR));
        blankPageButton->setText(QApplication::translate("PreferencesDialogClass", "Blank Page", Q_NULLPTR));
        defaultPageButton->setText(QApplication::translate("PreferencesDialogClass", "Restore to default", Q_NULLPTR));
        groupBox_3->setTitle(QApplication::translate("PreferencesDialogClass", "Appearance", Q_NULLPTR));
        showTabs->setText(QApplication::translate("PreferencesDialogClass", "Show tabs for each individual page", Q_NULLPTR));
        tabWidget->setTabText(tabWidget->indexOf(optionsTab), QApplication::translate("PreferencesDialogClass", "Options", Q_NULLPTR));
    } // retranslateUi

};

namespace Ui {
    class PreferencesDialogClass: public Ui_PreferencesDialogClass {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_PREFERENCESDIALOG_H
