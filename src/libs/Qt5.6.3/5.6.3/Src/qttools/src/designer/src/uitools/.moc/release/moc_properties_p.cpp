/****************************************************************************
** Meta object code from reading C++ file 'properties_p.h'
**
** Created by: The Qt Meta Object Compiler version 67 (Qt 5.6.3)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "../../../lib/uilib/properties_p.h"
#include <QtCore/qbytearray.h>
#include <QtCore/qmetatype.h>
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'properties_p.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 67
#error "This file was generated using the moc from 5.6.3. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
struct qt_meta_stringdata_QFormInternal__QAbstractFormBuilderGadget_t {
    QByteArrayData data[33];
    char stringdata0[515];
};
#define QT_MOC_LITERAL(idx, ofs, len) \
    Q_STATIC_BYTE_ARRAY_DATA_HEADER_INITIALIZER_WITH_OFFSET(len, \
    qptrdiff(offsetof(qt_meta_stringdata_QFormInternal__QAbstractFormBuilderGadget_t, stringdata0) + ofs \
        - idx * sizeof(QByteArrayData)) \
    )
static const qt_meta_stringdata_QFormInternal__QAbstractFormBuilderGadget_t qt_meta_stringdata_QFormInternal__QAbstractFormBuilderGadget = {
    {
QT_MOC_LITERAL(0, 0, 41), // "QFormInternal::QAbstractFormB..."
QT_MOC_LITERAL(1, 42, 9), // "itemFlags"
QT_MOC_LITERAL(2, 52, 13), // "Qt::ItemFlags"
QT_MOC_LITERAL(3, 66, 10), // "checkState"
QT_MOC_LITERAL(4, 77, 14), // "Qt::CheckState"
QT_MOC_LITERAL(5, 92, 13), // "textAlignment"
QT_MOC_LITERAL(6, 106, 13), // "Qt::Alignment"
QT_MOC_LITERAL(7, 120, 11), // "orientation"
QT_MOC_LITERAL(8, 132, 15), // "Qt::Orientation"
QT_MOC_LITERAL(9, 148, 8), // "sizeType"
QT_MOC_LITERAL(10, 157, 19), // "QSizePolicy::Policy"
QT_MOC_LITERAL(11, 177, 9), // "colorRole"
QT_MOC_LITERAL(12, 187, 19), // "QPalette::ColorRole"
QT_MOC_LITERAL(13, 207, 10), // "colorGroup"
QT_MOC_LITERAL(14, 218, 20), // "QPalette::ColorGroup"
QT_MOC_LITERAL(15, 239, 13), // "styleStrategy"
QT_MOC_LITERAL(16, 253, 20), // "QFont::StyleStrategy"
QT_MOC_LITERAL(17, 274, 11), // "cursorShape"
QT_MOC_LITERAL(18, 286, 15), // "Qt::CursorShape"
QT_MOC_LITERAL(19, 302, 10), // "brushStyle"
QT_MOC_LITERAL(20, 313, 14), // "Qt::BrushStyle"
QT_MOC_LITERAL(21, 328, 11), // "toolBarArea"
QT_MOC_LITERAL(22, 340, 15), // "Qt::ToolBarArea"
QT_MOC_LITERAL(23, 356, 12), // "gradientType"
QT_MOC_LITERAL(24, 369, 15), // "QGradient::Type"
QT_MOC_LITERAL(25, 385, 14), // "gradientSpread"
QT_MOC_LITERAL(26, 400, 17), // "QGradient::Spread"
QT_MOC_LITERAL(27, 418, 18), // "gradientCoordinate"
QT_MOC_LITERAL(28, 437, 25), // "QGradient::CoordinateMode"
QT_MOC_LITERAL(29, 463, 8), // "language"
QT_MOC_LITERAL(30, 472, 17), // "QLocale::Language"
QT_MOC_LITERAL(31, 490, 7), // "country"
QT_MOC_LITERAL(32, 498, 16) // "QLocale::Country"

    },
    "QFormInternal::QAbstractFormBuilderGadget\0"
    "itemFlags\0Qt::ItemFlags\0checkState\0"
    "Qt::CheckState\0textAlignment\0Qt::Alignment\0"
    "orientation\0Qt::Orientation\0sizeType\0"
    "QSizePolicy::Policy\0colorRole\0"
    "QPalette::ColorRole\0colorGroup\0"
    "QPalette::ColorGroup\0styleStrategy\0"
    "QFont::StyleStrategy\0cursorShape\0"
    "Qt::CursorShape\0brushStyle\0Qt::BrushStyle\0"
    "toolBarArea\0Qt::ToolBarArea\0gradientType\0"
    "QGradient::Type\0gradientSpread\0"
    "QGradient::Spread\0gradientCoordinate\0"
    "QGradient::CoordinateMode\0language\0"
    "QLocale::Language\0country\0QLocale::Country"
};
#undef QT_MOC_LITERAL

static const uint qt_meta_data_QFormInternal__QAbstractFormBuilderGadget[] = {

 // content:
       7,       // revision
       0,       // classname
       0,    0, // classinfo
       0,    0, // methods
      16,   14, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       0,       // signalCount

 // properties: name, type, flags
       1, 0x80000000 | 2, 0x00095009,
       3, 0x80000000 | 4, 0x00095009,
       5, 0x80000000 | 6, 0x00095009,
       7, 0x80000000 | 8, 0x00095009,
       9, 0x80000000 | 10, 0x00095009,
      11, 0x80000000 | 12, 0x00095009,
      13, 0x80000000 | 14, 0x00095009,
      15, 0x80000000 | 16, 0x00095009,
      17, 0x80000000 | 18, 0x00095009,
      19, 0x80000000 | 20, 0x00095009,
      21, 0x80000000 | 22, 0x00095009,
      23, 0x80000000 | 24, 0x00095009,
      25, 0x80000000 | 26, 0x00095009,
      27, 0x80000000 | 28, 0x00095009,
      29, 0x80000000 | 30, 0x00095009,
      31, 0x80000000 | 32, 0x00095009,

       0        // eod
};

void QFormInternal::QAbstractFormBuilderGadget::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{

#ifndef QT_NO_PROPERTIES
    if (_c == QMetaObject::ReadProperty) {
        QAbstractFormBuilderGadget *_t = static_cast<QAbstractFormBuilderGadget *>(_o);
        Q_UNUSED(_t)
        void *_v = _a[0];
        switch (_id) {
        case 0: *reinterpret_cast< Qt::ItemFlags*>(_v) = _t->fakeItemFlags(); break;
        case 1: *reinterpret_cast< Qt::CheckState*>(_v) = _t->fakeCheckState(); break;
        case 2: *reinterpret_cast< Qt::Alignment*>(_v) = _t->fakeAlignment(); break;
        case 3: *reinterpret_cast< Qt::Orientation*>(_v) = _t->fakeOrientation(); break;
        case 4: *reinterpret_cast< QSizePolicy::Policy*>(_v) = _t->fakeSizeType(); break;
        case 5: *reinterpret_cast< QPalette::ColorRole*>(_v) = _t->fakeColorRole(); break;
        case 6: *reinterpret_cast< QPalette::ColorGroup*>(_v) = _t->fakeColorGroup(); break;
        case 7: *reinterpret_cast< QFont::StyleStrategy*>(_v) = _t->fakeStyleStrategy(); break;
        case 8: *reinterpret_cast< Qt::CursorShape*>(_v) = _t->fakeCursorShape(); break;
        case 9: *reinterpret_cast< Qt::BrushStyle*>(_v) = _t->fakeBrushStyle(); break;
        case 10: *reinterpret_cast< Qt::ToolBarArea*>(_v) = _t->fakeToolBarArea(); break;
        case 11: *reinterpret_cast< QGradient::Type*>(_v) = _t->fakeGradientType(); break;
        case 12: *reinterpret_cast< QGradient::Spread*>(_v) = _t->fakeGradientSpread(); break;
        case 13: *reinterpret_cast< QGradient::CoordinateMode*>(_v) = _t->fakeGradientCoordinate(); break;
        case 14: *reinterpret_cast< QLocale::Language*>(_v) = _t->fakeLanguage(); break;
        case 15: *reinterpret_cast< QLocale::Country*>(_v) = _t->fakeCountry(); break;
        default: break;
        }
    } else if (_c == QMetaObject::WriteProperty) {
    } else if (_c == QMetaObject::ResetProperty) {
    }
#endif // QT_NO_PROPERTIES
    Q_UNUSED(_o);
    Q_UNUSED(_id);
    Q_UNUSED(_c);
    Q_UNUSED(_a);
}

static const QMetaObject * const qt_meta_extradata_QFormInternal__QAbstractFormBuilderGadget[] = {
        &QSizePolicy::staticMetaObject,
    &QPalette::staticMetaObject,
    &QFont::staticMetaObject,
    &QGradient::staticMetaObject,
    &QLocale::staticMetaObject,
    Q_NULLPTR
};

const QMetaObject QFormInternal::QAbstractFormBuilderGadget::staticMetaObject = {
    { &QWidget::staticMetaObject, qt_meta_stringdata_QFormInternal__QAbstractFormBuilderGadget.data,
      qt_meta_data_QFormInternal__QAbstractFormBuilderGadget,  qt_static_metacall, qt_meta_extradata_QFormInternal__QAbstractFormBuilderGadget, Q_NULLPTR}
};


const QMetaObject *QFormInternal::QAbstractFormBuilderGadget::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->dynamicMetaObject() : &staticMetaObject;
}

void *QFormInternal::QAbstractFormBuilderGadget::qt_metacast(const char *_clname)
{
    if (!_clname) return Q_NULLPTR;
    if (!strcmp(_clname, qt_meta_stringdata_QFormInternal__QAbstractFormBuilderGadget.stringdata0))
        return static_cast<void*>(const_cast< QAbstractFormBuilderGadget*>(this));
    return QWidget::qt_metacast(_clname);
}

int QFormInternal::QAbstractFormBuilderGadget::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QWidget::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    
#ifndef QT_NO_PROPERTIES
   if (_c == QMetaObject::ReadProperty || _c == QMetaObject::WriteProperty
            || _c == QMetaObject::ResetProperty || _c == QMetaObject::RegisterPropertyMetaType) {
        qt_static_metacall(this, _c, _id, _a);
        _id -= 16;
    } else if (_c == QMetaObject::QueryPropertyDesignable) {
        _id -= 16;
    } else if (_c == QMetaObject::QueryPropertyScriptable) {
        _id -= 16;
    } else if (_c == QMetaObject::QueryPropertyStored) {
        _id -= 16;
    } else if (_c == QMetaObject::QueryPropertyEditable) {
        _id -= 16;
    } else if (_c == QMetaObject::QueryPropertyUser) {
        _id -= 16;
    }
#endif // QT_NO_PROPERTIES
    return _id;
}
QT_END_MOC_NAMESPACE
