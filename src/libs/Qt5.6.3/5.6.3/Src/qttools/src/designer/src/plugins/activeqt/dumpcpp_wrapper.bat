@echo off
SetLocal EnableDelayedExpansion
(set PATH=C:\Qt\Qt5.6.3\5.6.3\Src\qtactiveqt\lib;C:\Qt\Qt5.6.3\5.6.3\Src\qtbase\lib;!PATH!)
if defined QT_PLUGIN_PATH (
    set QT_PLUGIN_PATH=C:\Qt\Qt5.6.3\5.6.3\Src\qtbase\plugins;C:\Qt\Qt5.6.3\5.6.3\Src\qtsvg\plugins;C:\Qt\Qt5.6.3\5.6.3\Src\qtimageformats\plugins;!QT_PLUGIN_PATH!
) else (
    set QT_PLUGIN_PATH=C:\Qt\Qt5.6.3\5.6.3\Src\qtbase\plugins;C:\Qt\Qt5.6.3\5.6.3\Src\qtsvg\plugins;C:\Qt\Qt5.6.3\5.6.3\Src\qtimageformats\plugins
)
C:\Qt\Qt5.6.3\5.6.3\Src\qtactiveqt\bin\dumpcpp.exe %*
EndLocal
