/****************************************************************************
** Meta object code from reading C++ file 'default_actionprovider.h'
**
** Created by: The Qt Meta Object Compiler version 67 (Qt 5.6.3)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "../../../formeditor/default_actionprovider.h"
#include <QtCore/qbytearray.h>
#include <QtCore/qmetatype.h>
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'default_actionprovider.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 67
#error "This file was generated using the moc from 5.6.3. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
struct qt_meta_stringdata_qdesigner_internal__QToolBarActionProvider_t {
    QByteArrayData data[1];
    char stringdata0[43];
};
#define QT_MOC_LITERAL(idx, ofs, len) \
    Q_STATIC_BYTE_ARRAY_DATA_HEADER_INITIALIZER_WITH_OFFSET(len, \
    qptrdiff(offsetof(qt_meta_stringdata_qdesigner_internal__QToolBarActionProvider_t, stringdata0) + ofs \
        - idx * sizeof(QByteArrayData)) \
    )
static const qt_meta_stringdata_qdesigner_internal__QToolBarActionProvider_t qt_meta_stringdata_qdesigner_internal__QToolBarActionProvider = {
    {
QT_MOC_LITERAL(0, 0, 42) // "qdesigner_internal::QToolBarA..."

    },
    "qdesigner_internal::QToolBarActionProvider"
};
#undef QT_MOC_LITERAL

static const uint qt_meta_data_qdesigner_internal__QToolBarActionProvider[] = {

 // content:
       7,       // revision
       0,       // classname
       0,    0, // classinfo
       0,    0, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       0,       // signalCount

       0        // eod
};

void qdesigner_internal::QToolBarActionProvider::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    Q_UNUSED(_o);
    Q_UNUSED(_id);
    Q_UNUSED(_c);
    Q_UNUSED(_a);
}

const QMetaObject qdesigner_internal::QToolBarActionProvider::staticMetaObject = {
    { &QObject::staticMetaObject, qt_meta_stringdata_qdesigner_internal__QToolBarActionProvider.data,
      qt_meta_data_qdesigner_internal__QToolBarActionProvider,  qt_static_metacall, Q_NULLPTR, Q_NULLPTR}
};


const QMetaObject *qdesigner_internal::QToolBarActionProvider::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->dynamicMetaObject() : &staticMetaObject;
}

void *qdesigner_internal::QToolBarActionProvider::qt_metacast(const char *_clname)
{
    if (!_clname) return Q_NULLPTR;
    if (!strcmp(_clname, qt_meta_stringdata_qdesigner_internal__QToolBarActionProvider.stringdata0))
        return static_cast<void*>(const_cast< QToolBarActionProvider*>(this));
    if (!strcmp(_clname, "ActionProviderBase"))
        return static_cast< ActionProviderBase*>(const_cast< QToolBarActionProvider*>(this));
    if (!strcmp(_clname, "org.qt-project.Qt.Designer.ActionProvider"))
        return static_cast< QDesignerActionProviderExtension*>(const_cast< QToolBarActionProvider*>(this));
    return QObject::qt_metacast(_clname);
}

int qdesigner_internal::QToolBarActionProvider::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QObject::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    return _id;
}
struct qt_meta_stringdata_qdesigner_internal__QMenuBarActionProvider_t {
    QByteArrayData data[1];
    char stringdata0[43];
};
#define QT_MOC_LITERAL(idx, ofs, len) \
    Q_STATIC_BYTE_ARRAY_DATA_HEADER_INITIALIZER_WITH_OFFSET(len, \
    qptrdiff(offsetof(qt_meta_stringdata_qdesigner_internal__QMenuBarActionProvider_t, stringdata0) + ofs \
        - idx * sizeof(QByteArrayData)) \
    )
static const qt_meta_stringdata_qdesigner_internal__QMenuBarActionProvider_t qt_meta_stringdata_qdesigner_internal__QMenuBarActionProvider = {
    {
QT_MOC_LITERAL(0, 0, 42) // "qdesigner_internal::QMenuBarA..."

    },
    "qdesigner_internal::QMenuBarActionProvider"
};
#undef QT_MOC_LITERAL

static const uint qt_meta_data_qdesigner_internal__QMenuBarActionProvider[] = {

 // content:
       7,       // revision
       0,       // classname
       0,    0, // classinfo
       0,    0, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       0,       // signalCount

       0        // eod
};

void qdesigner_internal::QMenuBarActionProvider::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    Q_UNUSED(_o);
    Q_UNUSED(_id);
    Q_UNUSED(_c);
    Q_UNUSED(_a);
}

const QMetaObject qdesigner_internal::QMenuBarActionProvider::staticMetaObject = {
    { &QObject::staticMetaObject, qt_meta_stringdata_qdesigner_internal__QMenuBarActionProvider.data,
      qt_meta_data_qdesigner_internal__QMenuBarActionProvider,  qt_static_metacall, Q_NULLPTR, Q_NULLPTR}
};


const QMetaObject *qdesigner_internal::QMenuBarActionProvider::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->dynamicMetaObject() : &staticMetaObject;
}

void *qdesigner_internal::QMenuBarActionProvider::qt_metacast(const char *_clname)
{
    if (!_clname) return Q_NULLPTR;
    if (!strcmp(_clname, qt_meta_stringdata_qdesigner_internal__QMenuBarActionProvider.stringdata0))
        return static_cast<void*>(const_cast< QMenuBarActionProvider*>(this));
    if (!strcmp(_clname, "ActionProviderBase"))
        return static_cast< ActionProviderBase*>(const_cast< QMenuBarActionProvider*>(this));
    if (!strcmp(_clname, "org.qt-project.Qt.Designer.ActionProvider"))
        return static_cast< QDesignerActionProviderExtension*>(const_cast< QMenuBarActionProvider*>(this));
    return QObject::qt_metacast(_clname);
}

int qdesigner_internal::QMenuBarActionProvider::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QObject::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    return _id;
}
struct qt_meta_stringdata_qdesigner_internal__QMenuActionProvider_t {
    QByteArrayData data[1];
    char stringdata0[40];
};
#define QT_MOC_LITERAL(idx, ofs, len) \
    Q_STATIC_BYTE_ARRAY_DATA_HEADER_INITIALIZER_WITH_OFFSET(len, \
    qptrdiff(offsetof(qt_meta_stringdata_qdesigner_internal__QMenuActionProvider_t, stringdata0) + ofs \
        - idx * sizeof(QByteArrayData)) \
    )
static const qt_meta_stringdata_qdesigner_internal__QMenuActionProvider_t qt_meta_stringdata_qdesigner_internal__QMenuActionProvider = {
    {
QT_MOC_LITERAL(0, 0, 39) // "qdesigner_internal::QMenuActi..."

    },
    "qdesigner_internal::QMenuActionProvider"
};
#undef QT_MOC_LITERAL

static const uint qt_meta_data_qdesigner_internal__QMenuActionProvider[] = {

 // content:
       7,       // revision
       0,       // classname
       0,    0, // classinfo
       0,    0, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       0,       // signalCount

       0        // eod
};

void qdesigner_internal::QMenuActionProvider::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    Q_UNUSED(_o);
    Q_UNUSED(_id);
    Q_UNUSED(_c);
    Q_UNUSED(_a);
}

const QMetaObject qdesigner_internal::QMenuActionProvider::staticMetaObject = {
    { &QObject::staticMetaObject, qt_meta_stringdata_qdesigner_internal__QMenuActionProvider.data,
      qt_meta_data_qdesigner_internal__QMenuActionProvider,  qt_static_metacall, Q_NULLPTR, Q_NULLPTR}
};


const QMetaObject *qdesigner_internal::QMenuActionProvider::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->dynamicMetaObject() : &staticMetaObject;
}

void *qdesigner_internal::QMenuActionProvider::qt_metacast(const char *_clname)
{
    if (!_clname) return Q_NULLPTR;
    if (!strcmp(_clname, qt_meta_stringdata_qdesigner_internal__QMenuActionProvider.stringdata0))
        return static_cast<void*>(const_cast< QMenuActionProvider*>(this));
    if (!strcmp(_clname, "ActionProviderBase"))
        return static_cast< ActionProviderBase*>(const_cast< QMenuActionProvider*>(this));
    if (!strcmp(_clname, "org.qt-project.Qt.Designer.ActionProvider"))
        return static_cast< QDesignerActionProviderExtension*>(const_cast< QMenuActionProvider*>(this));
    return QObject::qt_metacast(_clname);
}

int qdesigner_internal::QMenuActionProvider::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QObject::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    return _id;
}
QT_END_MOC_NAMESPACE
