/****************************************************************************
** Meta object code from reading C++ file 'qquickgeomapgesturearea_p.h'
**
** Created by: The Qt Meta Object Compiler version 67 (Qt 5.6.3)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "../../qquickgeomapgesturearea_p.h"
#include <QtCore/qbytearray.h>
#include <QtCore/qmetatype.h>
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'qquickgeomapgesturearea_p.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 67
#error "This file was generated using the moc from 5.6.3. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
struct qt_meta_stringdata_QGeoMapPinchEvent_t {
    QByteArrayData data[7];
    char stringdata0[65];
};
#define QT_MOC_LITERAL(idx, ofs, len) \
    Q_STATIC_BYTE_ARRAY_DATA_HEADER_INITIALIZER_WITH_OFFSET(len, \
    qptrdiff(offsetof(qt_meta_stringdata_QGeoMapPinchEvent_t, stringdata0) + ofs \
        - idx * sizeof(QByteArrayData)) \
    )
static const qt_meta_stringdata_QGeoMapPinchEvent_t qt_meta_stringdata_QGeoMapPinchEvent = {
    {
QT_MOC_LITERAL(0, 0, 17), // "QGeoMapPinchEvent"
QT_MOC_LITERAL(1, 18, 6), // "center"
QT_MOC_LITERAL(2, 25, 5), // "angle"
QT_MOC_LITERAL(3, 31, 6), // "point1"
QT_MOC_LITERAL(4, 38, 6), // "point2"
QT_MOC_LITERAL(5, 45, 10), // "pointCount"
QT_MOC_LITERAL(6, 56, 8) // "accepted"

    },
    "QGeoMapPinchEvent\0center\0angle\0point1\0"
    "point2\0pointCount\0accepted"
};
#undef QT_MOC_LITERAL

static const uint qt_meta_data_QGeoMapPinchEvent[] = {

 // content:
       7,       // revision
       0,       // classname
       0,    0, // classinfo
       0,    0, // methods
       6,   14, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       0,       // signalCount

 // properties: name, type, flags
       1, QMetaType::QPointF, 0x00095001,
       2, QMetaType::QReal, 0x00095001,
       3, QMetaType::QPointF, 0x00095001,
       4, QMetaType::QPointF, 0x00095001,
       5, QMetaType::Int, 0x00095001,
       6, QMetaType::Bool, 0x00095103,

       0        // eod
};

void QGeoMapPinchEvent::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{

#ifndef QT_NO_PROPERTIES
    if (_c == QMetaObject::ReadProperty) {
        QGeoMapPinchEvent *_t = static_cast<QGeoMapPinchEvent *>(_o);
        Q_UNUSED(_t)
        void *_v = _a[0];
        switch (_id) {
        case 0: *reinterpret_cast< QPointF*>(_v) = _t->center(); break;
        case 1: *reinterpret_cast< qreal*>(_v) = _t->angle(); break;
        case 2: *reinterpret_cast< QPointF*>(_v) = _t->point1(); break;
        case 3: *reinterpret_cast< QPointF*>(_v) = _t->point2(); break;
        case 4: *reinterpret_cast< int*>(_v) = _t->pointCount(); break;
        case 5: *reinterpret_cast< bool*>(_v) = _t->accepted(); break;
        default: break;
        }
    } else if (_c == QMetaObject::WriteProperty) {
        QGeoMapPinchEvent *_t = static_cast<QGeoMapPinchEvent *>(_o);
        Q_UNUSED(_t)
        void *_v = _a[0];
        switch (_id) {
        case 5: _t->setAccepted(*reinterpret_cast< bool*>(_v)); break;
        default: break;
        }
    } else if (_c == QMetaObject::ResetProperty) {
    }
#endif // QT_NO_PROPERTIES
    Q_UNUSED(_o);
    Q_UNUSED(_id);
    Q_UNUSED(_c);
    Q_UNUSED(_a);
}

const QMetaObject QGeoMapPinchEvent::staticMetaObject = {
    { &QObject::staticMetaObject, qt_meta_stringdata_QGeoMapPinchEvent.data,
      qt_meta_data_QGeoMapPinchEvent,  qt_static_metacall, Q_NULLPTR, Q_NULLPTR}
};


const QMetaObject *QGeoMapPinchEvent::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->dynamicMetaObject() : &staticMetaObject;
}

void *QGeoMapPinchEvent::qt_metacast(const char *_clname)
{
    if (!_clname) return Q_NULLPTR;
    if (!strcmp(_clname, qt_meta_stringdata_QGeoMapPinchEvent.stringdata0))
        return static_cast<void*>(const_cast< QGeoMapPinchEvent*>(this));
    return QObject::qt_metacast(_clname);
}

int QGeoMapPinchEvent::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QObject::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    
#ifndef QT_NO_PROPERTIES
   if (_c == QMetaObject::ReadProperty || _c == QMetaObject::WriteProperty
            || _c == QMetaObject::ResetProperty || _c == QMetaObject::RegisterPropertyMetaType) {
        qt_static_metacall(this, _c, _id, _a);
        _id -= 6;
    } else if (_c == QMetaObject::QueryPropertyDesignable) {
        _id -= 6;
    } else if (_c == QMetaObject::QueryPropertyScriptable) {
        _id -= 6;
    } else if (_c == QMetaObject::QueryPropertyStored) {
        _id -= 6;
    } else if (_c == QMetaObject::QueryPropertyEditable) {
        _id -= 6;
    } else if (_c == QMetaObject::QueryPropertyUser) {
        _id -= 6;
    }
#endif // QT_NO_PROPERTIES
    return _id;
}
struct qt_meta_stringdata_QQuickGeoMapGestureArea_t {
    QByteArrayData data[32];
    char stringdata0[503];
};
#define QT_MOC_LITERAL(idx, ofs, len) \
    Q_STATIC_BYTE_ARRAY_DATA_HEADER_INITIALIZER_WITH_OFFSET(len, \
    qptrdiff(offsetof(qt_meta_stringdata_QQuickGeoMapGestureArea_t, stringdata0) + ofs \
        - idx * sizeof(QByteArrayData)) \
    )
static const qt_meta_stringdata_QQuickGeoMapGestureArea_t qt_meta_stringdata_QQuickGeoMapGestureArea = {
    {
QT_MOC_LITERAL(0, 0, 23), // "QQuickGeoMapGestureArea"
QT_MOC_LITERAL(1, 24, 16), // "panActiveChanged"
QT_MOC_LITERAL(2, 41, 0), // ""
QT_MOC_LITERAL(3, 42, 18), // "pinchActiveChanged"
QT_MOC_LITERAL(4, 61, 14), // "enabledChanged"
QT_MOC_LITERAL(5, 76, 29), // "maximumZoomLevelChangeChanged"
QT_MOC_LITERAL(6, 106, 23), // "acceptedGesturesChanged"
QT_MOC_LITERAL(7, 130, 24), // "flickDecelerationChanged"
QT_MOC_LITERAL(8, 155, 12), // "pinchStarted"
QT_MOC_LITERAL(9, 168, 18), // "QGeoMapPinchEvent*"
QT_MOC_LITERAL(10, 187, 5), // "pinch"
QT_MOC_LITERAL(11, 193, 12), // "pinchUpdated"
QT_MOC_LITERAL(12, 206, 13), // "pinchFinished"
QT_MOC_LITERAL(13, 220, 10), // "panStarted"
QT_MOC_LITERAL(14, 231, 11), // "panFinished"
QT_MOC_LITERAL(15, 243, 12), // "flickStarted"
QT_MOC_LITERAL(16, 256, 13), // "flickFinished"
QT_MOC_LITERAL(17, 270, 22), // "preventStealingChanged"
QT_MOC_LITERAL(18, 293, 27), // "handleFlickAnimationStopped"
QT_MOC_LITERAL(19, 321, 7), // "enabled"
QT_MOC_LITERAL(20, 329, 11), // "pinchActive"
QT_MOC_LITERAL(21, 341, 9), // "panActive"
QT_MOC_LITERAL(22, 351, 16), // "acceptedGestures"
QT_MOC_LITERAL(23, 368, 16), // "AcceptedGestures"
QT_MOC_LITERAL(24, 385, 22), // "maximumZoomLevelChange"
QT_MOC_LITERAL(25, 408, 17), // "flickDeceleration"
QT_MOC_LITERAL(26, 426, 15), // "preventStealing"
QT_MOC_LITERAL(27, 442, 13), // "GeoMapGesture"
QT_MOC_LITERAL(28, 456, 9), // "NoGesture"
QT_MOC_LITERAL(29, 466, 12), // "PinchGesture"
QT_MOC_LITERAL(30, 479, 10), // "PanGesture"
QT_MOC_LITERAL(31, 490, 12) // "FlickGesture"

    },
    "QQuickGeoMapGestureArea\0panActiveChanged\0"
    "\0pinchActiveChanged\0enabledChanged\0"
    "maximumZoomLevelChangeChanged\0"
    "acceptedGesturesChanged\0"
    "flickDecelerationChanged\0pinchStarted\0"
    "QGeoMapPinchEvent*\0pinch\0pinchUpdated\0"
    "pinchFinished\0panStarted\0panFinished\0"
    "flickStarted\0flickFinished\0"
    "preventStealingChanged\0"
    "handleFlickAnimationStopped\0enabled\0"
    "pinchActive\0panActive\0acceptedGestures\0"
    "AcceptedGestures\0maximumZoomLevelChange\0"
    "flickDeceleration\0preventStealing\0"
    "GeoMapGesture\0NoGesture\0PinchGesture\0"
    "PanGesture\0FlickGesture"
};
#undef QT_MOC_LITERAL

static const uint qt_meta_data_QQuickGeoMapGestureArea[] = {

 // content:
       7,       // revision
       0,       // classname
       0,    0, // classinfo
      15,   14, // methods
       7,  110, // properties
       2,  145, // enums/sets
       0,    0, // constructors
       0,       // flags
      14,       // signalCount

 // signals: name, argc, parameters, tag, flags
       1,    0,   89,    2, 0x06 /* Public */,
       3,    0,   90,    2, 0x06 /* Public */,
       4,    0,   91,    2, 0x06 /* Public */,
       5,    0,   92,    2, 0x06 /* Public */,
       6,    0,   93,    2, 0x06 /* Public */,
       7,    0,   94,    2, 0x06 /* Public */,
       8,    1,   95,    2, 0x06 /* Public */,
      11,    1,   98,    2, 0x06 /* Public */,
      12,    1,  101,    2, 0x06 /* Public */,
      13,    0,  104,    2, 0x06 /* Public */,
      14,    0,  105,    2, 0x06 /* Public */,
      15,    0,  106,    2, 0x06 /* Public */,
      16,    0,  107,    2, 0x06 /* Public */,
      17,    0,  108,    2, 0x06 /* Public */,

 // slots: name, argc, parameters, tag, flags
      18,    0,  109,    2, 0x08 /* Private */,

 // signals: parameters
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void, 0x80000000 | 9,   10,
    QMetaType::Void, 0x80000000 | 9,   10,
    QMetaType::Void, 0x80000000 | 9,   10,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,

 // slots: parameters
    QMetaType::Void,

 // properties: name, type, flags
      19, QMetaType::Bool, 0x00495103,
      20, QMetaType::Bool, 0x00495001,
      21, QMetaType::Bool, 0x00495001,
      22, 0x80000000 | 23, 0x0049510b,
      24, QMetaType::QReal, 0x00495103,
      25, QMetaType::QReal, 0x00495103,
      26, QMetaType::Bool, 0x00c95103,

 // properties: notify_signal_id
       2,
       1,
       0,
       4,
       3,
       5,
      13,

 // properties: revision
       0,
       0,
       0,
       0,
       0,
       0,
       1,

 // enums: name, flags, count, data
      27, 0x0,    4,  153,
      23, 0x1,    4,  161,

 // enum data: key, value
      28, uint(QQuickGeoMapGestureArea::NoGesture),
      29, uint(QQuickGeoMapGestureArea::PinchGesture),
      30, uint(QQuickGeoMapGestureArea::PanGesture),
      31, uint(QQuickGeoMapGestureArea::FlickGesture),
      28, uint(QQuickGeoMapGestureArea::NoGesture),
      29, uint(QQuickGeoMapGestureArea::PinchGesture),
      30, uint(QQuickGeoMapGestureArea::PanGesture),
      31, uint(QQuickGeoMapGestureArea::FlickGesture),

       0        // eod
};

void QQuickGeoMapGestureArea::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        QQuickGeoMapGestureArea *_t = static_cast<QQuickGeoMapGestureArea *>(_o);
        Q_UNUSED(_t)
        switch (_id) {
        case 0: _t->panActiveChanged(); break;
        case 1: _t->pinchActiveChanged(); break;
        case 2: _t->enabledChanged(); break;
        case 3: _t->maximumZoomLevelChangeChanged(); break;
        case 4: _t->acceptedGesturesChanged(); break;
        case 5: _t->flickDecelerationChanged(); break;
        case 6: _t->pinchStarted((*reinterpret_cast< QGeoMapPinchEvent*(*)>(_a[1]))); break;
        case 7: _t->pinchUpdated((*reinterpret_cast< QGeoMapPinchEvent*(*)>(_a[1]))); break;
        case 8: _t->pinchFinished((*reinterpret_cast< QGeoMapPinchEvent*(*)>(_a[1]))); break;
        case 9: _t->panStarted(); break;
        case 10: _t->panFinished(); break;
        case 11: _t->flickStarted(); break;
        case 12: _t->flickFinished(); break;
        case 13: _t->preventStealingChanged(); break;
        case 14: _t->handleFlickAnimationStopped(); break;
        default: ;
        }
    } else if (_c == QMetaObject::RegisterMethodArgumentMetaType) {
        switch (_id) {
        default: *reinterpret_cast<int*>(_a[0]) = -1; break;
        case 6:
            switch (*reinterpret_cast<int*>(_a[1])) {
            default: *reinterpret_cast<int*>(_a[0]) = -1; break;
            case 0:
                *reinterpret_cast<int*>(_a[0]) = qRegisterMetaType< QGeoMapPinchEvent* >(); break;
            }
            break;
        case 7:
            switch (*reinterpret_cast<int*>(_a[1])) {
            default: *reinterpret_cast<int*>(_a[0]) = -1; break;
            case 0:
                *reinterpret_cast<int*>(_a[0]) = qRegisterMetaType< QGeoMapPinchEvent* >(); break;
            }
            break;
        case 8:
            switch (*reinterpret_cast<int*>(_a[1])) {
            default: *reinterpret_cast<int*>(_a[0]) = -1; break;
            case 0:
                *reinterpret_cast<int*>(_a[0]) = qRegisterMetaType< QGeoMapPinchEvent* >(); break;
            }
            break;
        }
    } else if (_c == QMetaObject::IndexOfMethod) {
        int *result = reinterpret_cast<int *>(_a[0]);
        void **func = reinterpret_cast<void **>(_a[1]);
        {
            typedef void (QQuickGeoMapGestureArea::*_t)();
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&QQuickGeoMapGestureArea::panActiveChanged)) {
                *result = 0;
                return;
            }
        }
        {
            typedef void (QQuickGeoMapGestureArea::*_t)();
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&QQuickGeoMapGestureArea::pinchActiveChanged)) {
                *result = 1;
                return;
            }
        }
        {
            typedef void (QQuickGeoMapGestureArea::*_t)();
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&QQuickGeoMapGestureArea::enabledChanged)) {
                *result = 2;
                return;
            }
        }
        {
            typedef void (QQuickGeoMapGestureArea::*_t)();
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&QQuickGeoMapGestureArea::maximumZoomLevelChangeChanged)) {
                *result = 3;
                return;
            }
        }
        {
            typedef void (QQuickGeoMapGestureArea::*_t)();
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&QQuickGeoMapGestureArea::acceptedGesturesChanged)) {
                *result = 4;
                return;
            }
        }
        {
            typedef void (QQuickGeoMapGestureArea::*_t)();
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&QQuickGeoMapGestureArea::flickDecelerationChanged)) {
                *result = 5;
                return;
            }
        }
        {
            typedef void (QQuickGeoMapGestureArea::*_t)(QGeoMapPinchEvent * );
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&QQuickGeoMapGestureArea::pinchStarted)) {
                *result = 6;
                return;
            }
        }
        {
            typedef void (QQuickGeoMapGestureArea::*_t)(QGeoMapPinchEvent * );
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&QQuickGeoMapGestureArea::pinchUpdated)) {
                *result = 7;
                return;
            }
        }
        {
            typedef void (QQuickGeoMapGestureArea::*_t)(QGeoMapPinchEvent * );
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&QQuickGeoMapGestureArea::pinchFinished)) {
                *result = 8;
                return;
            }
        }
        {
            typedef void (QQuickGeoMapGestureArea::*_t)();
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&QQuickGeoMapGestureArea::panStarted)) {
                *result = 9;
                return;
            }
        }
        {
            typedef void (QQuickGeoMapGestureArea::*_t)();
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&QQuickGeoMapGestureArea::panFinished)) {
                *result = 10;
                return;
            }
        }
        {
            typedef void (QQuickGeoMapGestureArea::*_t)();
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&QQuickGeoMapGestureArea::flickStarted)) {
                *result = 11;
                return;
            }
        }
        {
            typedef void (QQuickGeoMapGestureArea::*_t)();
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&QQuickGeoMapGestureArea::flickFinished)) {
                *result = 12;
                return;
            }
        }
        {
            typedef void (QQuickGeoMapGestureArea::*_t)();
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&QQuickGeoMapGestureArea::preventStealingChanged)) {
                *result = 13;
                return;
            }
        }
    }
#ifndef QT_NO_PROPERTIES
    else if (_c == QMetaObject::ReadProperty) {
        QQuickGeoMapGestureArea *_t = static_cast<QQuickGeoMapGestureArea *>(_o);
        Q_UNUSED(_t)
        void *_v = _a[0];
        switch (_id) {
        case 0: *reinterpret_cast< bool*>(_v) = _t->enabled(); break;
        case 1: *reinterpret_cast< bool*>(_v) = _t->isPinchActive(); break;
        case 2: *reinterpret_cast< bool*>(_v) = _t->isPanActive(); break;
        case 3: *reinterpret_cast<int*>(_v) = QFlag(_t->acceptedGestures()); break;
        case 4: *reinterpret_cast< qreal*>(_v) = _t->maximumZoomLevelChange(); break;
        case 5: *reinterpret_cast< qreal*>(_v) = _t->flickDeceleration(); break;
        case 6: *reinterpret_cast< bool*>(_v) = _t->preventStealing(); break;
        default: break;
        }
    } else if (_c == QMetaObject::WriteProperty) {
        QQuickGeoMapGestureArea *_t = static_cast<QQuickGeoMapGestureArea *>(_o);
        Q_UNUSED(_t)
        void *_v = _a[0];
        switch (_id) {
        case 0: _t->setEnabled(*reinterpret_cast< bool*>(_v)); break;
        case 3: _t->setAcceptedGestures(QFlag(*reinterpret_cast<int*>(_v))); break;
        case 4: _t->setMaximumZoomLevelChange(*reinterpret_cast< qreal*>(_v)); break;
        case 5: _t->setFlickDeceleration(*reinterpret_cast< qreal*>(_v)); break;
        case 6: _t->setPreventStealing(*reinterpret_cast< bool*>(_v)); break;
        default: break;
        }
    } else if (_c == QMetaObject::ResetProperty) {
    }
#endif // QT_NO_PROPERTIES
}

const QMetaObject QQuickGeoMapGestureArea::staticMetaObject = {
    { &QQuickItem::staticMetaObject, qt_meta_stringdata_QQuickGeoMapGestureArea.data,
      qt_meta_data_QQuickGeoMapGestureArea,  qt_static_metacall, Q_NULLPTR, Q_NULLPTR}
};


const QMetaObject *QQuickGeoMapGestureArea::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->dynamicMetaObject() : &staticMetaObject;
}

void *QQuickGeoMapGestureArea::qt_metacast(const char *_clname)
{
    if (!_clname) return Q_NULLPTR;
    if (!strcmp(_clname, qt_meta_stringdata_QQuickGeoMapGestureArea.stringdata0))
        return static_cast<void*>(const_cast< QQuickGeoMapGestureArea*>(this));
    return QQuickItem::qt_metacast(_clname);
}

int QQuickGeoMapGestureArea::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QQuickItem::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 15)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 15;
    } else if (_c == QMetaObject::RegisterMethodArgumentMetaType) {
        if (_id < 15)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 15;
    }
#ifndef QT_NO_PROPERTIES
   else if (_c == QMetaObject::ReadProperty || _c == QMetaObject::WriteProperty
            || _c == QMetaObject::ResetProperty || _c == QMetaObject::RegisterPropertyMetaType) {
        qt_static_metacall(this, _c, _id, _a);
        _id -= 7;
    } else if (_c == QMetaObject::QueryPropertyDesignable) {
        _id -= 7;
    } else if (_c == QMetaObject::QueryPropertyScriptable) {
        _id -= 7;
    } else if (_c == QMetaObject::QueryPropertyStored) {
        _id -= 7;
    } else if (_c == QMetaObject::QueryPropertyEditable) {
        _id -= 7;
    } else if (_c == QMetaObject::QueryPropertyUser) {
        _id -= 7;
    }
#endif // QT_NO_PROPERTIES
    return _id;
}

// SIGNAL 0
void QQuickGeoMapGestureArea::panActiveChanged()
{
    QMetaObject::activate(this, &staticMetaObject, 0, Q_NULLPTR);
}

// SIGNAL 1
void QQuickGeoMapGestureArea::pinchActiveChanged()
{
    QMetaObject::activate(this, &staticMetaObject, 1, Q_NULLPTR);
}

// SIGNAL 2
void QQuickGeoMapGestureArea::enabledChanged()
{
    QMetaObject::activate(this, &staticMetaObject, 2, Q_NULLPTR);
}

// SIGNAL 3
void QQuickGeoMapGestureArea::maximumZoomLevelChangeChanged()
{
    QMetaObject::activate(this, &staticMetaObject, 3, Q_NULLPTR);
}

// SIGNAL 4
void QQuickGeoMapGestureArea::acceptedGesturesChanged()
{
    QMetaObject::activate(this, &staticMetaObject, 4, Q_NULLPTR);
}

// SIGNAL 5
void QQuickGeoMapGestureArea::flickDecelerationChanged()
{
    QMetaObject::activate(this, &staticMetaObject, 5, Q_NULLPTR);
}

// SIGNAL 6
void QQuickGeoMapGestureArea::pinchStarted(QGeoMapPinchEvent * _t1)
{
    void *_a[] = { Q_NULLPTR, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 6, _a);
}

// SIGNAL 7
void QQuickGeoMapGestureArea::pinchUpdated(QGeoMapPinchEvent * _t1)
{
    void *_a[] = { Q_NULLPTR, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 7, _a);
}

// SIGNAL 8
void QQuickGeoMapGestureArea::pinchFinished(QGeoMapPinchEvent * _t1)
{
    void *_a[] = { Q_NULLPTR, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 8, _a);
}

// SIGNAL 9
void QQuickGeoMapGestureArea::panStarted()
{
    QMetaObject::activate(this, &staticMetaObject, 9, Q_NULLPTR);
}

// SIGNAL 10
void QQuickGeoMapGestureArea::panFinished()
{
    QMetaObject::activate(this, &staticMetaObject, 10, Q_NULLPTR);
}

// SIGNAL 11
void QQuickGeoMapGestureArea::flickStarted()
{
    QMetaObject::activate(this, &staticMetaObject, 11, Q_NULLPTR);
}

// SIGNAL 12
void QQuickGeoMapGestureArea::flickFinished()
{
    QMetaObject::activate(this, &staticMetaObject, 12, Q_NULLPTR);
}

// SIGNAL 13
void QQuickGeoMapGestureArea::preventStealingChanged()
{
    QMetaObject::activate(this, &staticMetaObject, 13, Q_NULLPTR);
}
QT_END_MOC_NAMESPACE
