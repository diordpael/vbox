/****************************************************************************
** Meta object code from reading C++ file 'qdeclarativegeomapitembase_p.h'
**
** Created by: The Qt Meta Object Compiler version 67 (Qt 5.6.3)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "../../qdeclarativegeomapitembase_p.h"
#include <QtCore/qbytearray.h>
#include <QtCore/qmetatype.h>
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'qdeclarativegeomapitembase_p.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 67
#error "This file was generated using the moc from 5.6.3. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
struct qt_meta_stringdata_QDeclarativeGeoMapItemBase_t {
    QByteArrayData data[10];
    char stringdata0[163];
};
#define QT_MOC_LITERAL(idx, ofs, len) \
    Q_STATIC_BYTE_ARRAY_DATA_HEADER_INITIALIZER_WITH_OFFSET(len, \
    qptrdiff(offsetof(qt_meta_stringdata_QDeclarativeGeoMapItemBase_t, stringdata0) + ofs \
        - idx * sizeof(QByteArrayData)) \
    )
static const qt_meta_stringdata_QDeclarativeGeoMapItemBase_t qt_meta_stringdata_QDeclarativeGeoMapItemBase = {
    {
QT_MOC_LITERAL(0, 0, 26), // "QDeclarativeGeoMapItemBase"
QT_MOC_LITERAL(1, 27, 20), // "afterChildrenChanged"
QT_MOC_LITERAL(2, 48, 0), // ""
QT_MOC_LITERAL(3, 49, 20), // "afterViewportChanged"
QT_MOC_LITERAL(4, 70, 26), // "QGeoMapViewportChangeEvent"
QT_MOC_LITERAL(5, 97, 5), // "event"
QT_MOC_LITERAL(6, 103, 15), // "polishAndUpdate"
QT_MOC_LITERAL(7, 119, 21), // "baseCameraDataChanged"
QT_MOC_LITERAL(8, 141, 14), // "QGeoCameraData"
QT_MOC_LITERAL(9, 156, 6) // "camera"

    },
    "QDeclarativeGeoMapItemBase\0"
    "afterChildrenChanged\0\0afterViewportChanged\0"
    "QGeoMapViewportChangeEvent\0event\0"
    "polishAndUpdate\0baseCameraDataChanged\0"
    "QGeoCameraData\0camera"
};
#undef QT_MOC_LITERAL

static const uint qt_meta_data_QDeclarativeGeoMapItemBase[] = {

 // content:
       7,       // revision
       0,       // classname
       0,    0, // classinfo
       4,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       0,       // signalCount

 // slots: name, argc, parameters, tag, flags
       1,    0,   34,    2, 0x09 /* Protected */,
       3,    1,   35,    2, 0x09 /* Protected */,
       6,    0,   38,    2, 0x09 /* Protected */,
       7,    1,   39,    2, 0x08 /* Private */,

 // slots: parameters
    QMetaType::Void,
    QMetaType::Void, 0x80000000 | 4,    5,
    QMetaType::Void,
    QMetaType::Void, 0x80000000 | 8,    9,

       0        // eod
};

void QDeclarativeGeoMapItemBase::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        QDeclarativeGeoMapItemBase *_t = static_cast<QDeclarativeGeoMapItemBase *>(_o);
        Q_UNUSED(_t)
        switch (_id) {
        case 0: _t->afterChildrenChanged(); break;
        case 1: _t->afterViewportChanged((*reinterpret_cast< const QGeoMapViewportChangeEvent(*)>(_a[1]))); break;
        case 2: _t->polishAndUpdate(); break;
        case 3: _t->baseCameraDataChanged((*reinterpret_cast< const QGeoCameraData(*)>(_a[1]))); break;
        default: ;
        }
    } else if (_c == QMetaObject::RegisterMethodArgumentMetaType) {
        switch (_id) {
        default: *reinterpret_cast<int*>(_a[0]) = -1; break;
        case 3:
            switch (*reinterpret_cast<int*>(_a[1])) {
            default: *reinterpret_cast<int*>(_a[0]) = -1; break;
            case 0:
                *reinterpret_cast<int*>(_a[0]) = qRegisterMetaType< QGeoCameraData >(); break;
            }
            break;
        }
    }
}

const QMetaObject QDeclarativeGeoMapItemBase::staticMetaObject = {
    { &QQuickItem::staticMetaObject, qt_meta_stringdata_QDeclarativeGeoMapItemBase.data,
      qt_meta_data_QDeclarativeGeoMapItemBase,  qt_static_metacall, Q_NULLPTR, Q_NULLPTR}
};


const QMetaObject *QDeclarativeGeoMapItemBase::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->dynamicMetaObject() : &staticMetaObject;
}

void *QDeclarativeGeoMapItemBase::qt_metacast(const char *_clname)
{
    if (!_clname) return Q_NULLPTR;
    if (!strcmp(_clname, qt_meta_stringdata_QDeclarativeGeoMapItemBase.stringdata0))
        return static_cast<void*>(const_cast< QDeclarativeGeoMapItemBase*>(this));
    return QQuickItem::qt_metacast(_clname);
}

int QDeclarativeGeoMapItemBase::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QQuickItem::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 4)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 4;
    } else if (_c == QMetaObject::RegisterMethodArgumentMetaType) {
        if (_id < 4)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 4;
    }
    return _id;
}
QT_END_MOC_NAMESPACE
