/****************************************************************************
** Meta object code from reading C++ file 'qdeclarativegeomap_p.h'
**
** Created by: The Qt Meta Object Compiler version 67 (Qt 5.6.3)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "../../qdeclarativegeomap_p.h"
#include <QtCore/qbytearray.h>
#include <QtCore/qmetatype.h>
#include <QtCore/QList>
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'qdeclarativegeomap_p.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 67
#error "This file was generated using the moc from 5.6.3. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
struct qt_meta_stringdata_QDeclarativeGeoMap_t {
    QByteArrayData data[58];
    char stringdata0[882];
};
#define QT_MOC_LITERAL(idx, ofs, len) \
    Q_STATIC_BYTE_ARRAY_DATA_HEADER_INITIALIZER_WITH_OFFSET(len, \
    qptrdiff(offsetof(qt_meta_stringdata_QDeclarativeGeoMap_t, stringdata0) + ofs \
        - idx * sizeof(QByteArrayData)) \
    )
static const qt_meta_stringdata_QDeclarativeGeoMap_t qt_meta_stringdata_QDeclarativeGeoMap = {
    {
QT_MOC_LITERAL(0, 0, 18), // "QDeclarativeGeoMap"
QT_MOC_LITERAL(1, 19, 13), // "pluginChanged"
QT_MOC_LITERAL(2, 33, 0), // ""
QT_MOC_LITERAL(3, 34, 31), // "QDeclarativeGeoServiceProvider*"
QT_MOC_LITERAL(4, 66, 6), // "plugin"
QT_MOC_LITERAL(5, 73, 16), // "zoomLevelChanged"
QT_MOC_LITERAL(6, 90, 9), // "zoomLevel"
QT_MOC_LITERAL(7, 100, 13), // "centerChanged"
QT_MOC_LITERAL(8, 114, 14), // "QGeoCoordinate"
QT_MOC_LITERAL(9, 129, 10), // "coordinate"
QT_MOC_LITERAL(10, 140, 20), // "activeMapTypeChanged"
QT_MOC_LITERAL(11, 161, 24), // "supportedMapTypesChanged"
QT_MOC_LITERAL(12, 186, 23), // "minimumZoomLevelChanged"
QT_MOC_LITERAL(13, 210, 23), // "maximumZoomLevelChanged"
QT_MOC_LITERAL(14, 234, 15), // "mapItemsChanged"
QT_MOC_LITERAL(15, 250, 12), // "errorChanged"
QT_MOC_LITERAL(16, 263, 22), // "copyrightLinkActivated"
QT_MOC_LITERAL(17, 286, 4), // "link"
QT_MOC_LITERAL(18, 291, 12), // "colorChanged"
QT_MOC_LITERAL(19, 304, 5), // "color"
QT_MOC_LITERAL(20, 310, 25), // "mappingManagerInitialized"
QT_MOC_LITERAL(21, 336, 19), // "mapZoomLevelChanged"
QT_MOC_LITERAL(22, 356, 4), // "zoom"
QT_MOC_LITERAL(23, 361, 16), // "mapCenterChanged"
QT_MOC_LITERAL(24, 378, 6), // "center"
QT_MOC_LITERAL(25, 385, 11), // "pluginReady"
QT_MOC_LITERAL(26, 397, 20), // "onMapChildrenChanged"
QT_MOC_LITERAL(27, 418, 26), // "onSupportedMapTypesChanged"
QT_MOC_LITERAL(28, 445, 13), // "removeMapItem"
QT_MOC_LITERAL(29, 459, 27), // "QDeclarativeGeoMapItemBase*"
QT_MOC_LITERAL(30, 487, 4), // "item"
QT_MOC_LITERAL(31, 492, 10), // "addMapItem"
QT_MOC_LITERAL(32, 503, 13), // "clearMapItems"
QT_MOC_LITERAL(33, 517, 12), // "toCoordinate"
QT_MOC_LITERAL(34, 530, 8), // "position"
QT_MOC_LITERAL(35, 539, 14), // "clipToViewPort"
QT_MOC_LITERAL(36, 554, 14), // "fromCoordinate"
QT_MOC_LITERAL(37, 569, 21), // "fitViewportToMapItems"
QT_MOC_LITERAL(38, 591, 3), // "pan"
QT_MOC_LITERAL(39, 595, 2), // "dx"
QT_MOC_LITERAL(40, 598, 2), // "dy"
QT_MOC_LITERAL(41, 601, 12), // "prefetchData"
QT_MOC_LITERAL(42, 614, 9), // "clearData"
QT_MOC_LITERAL(43, 624, 7), // "gesture"
QT_MOC_LITERAL(44, 632, 24), // "QQuickGeoMapGestureArea*"
QT_MOC_LITERAL(45, 657, 16), // "minimumZoomLevel"
QT_MOC_LITERAL(46, 674, 16), // "maximumZoomLevel"
QT_MOC_LITERAL(47, 691, 13), // "activeMapType"
QT_MOC_LITERAL(48, 705, 23), // "QDeclarativeGeoMapType*"
QT_MOC_LITERAL(49, 729, 17), // "supportedMapTypes"
QT_MOC_LITERAL(50, 747, 40), // "QQmlListProperty<QDeclarative..."
QT_MOC_LITERAL(51, 788, 8), // "mapItems"
QT_MOC_LITERAL(52, 797, 15), // "QList<QObject*>"
QT_MOC_LITERAL(53, 813, 5), // "error"
QT_MOC_LITERAL(54, 819, 26), // "QGeoServiceProvider::Error"
QT_MOC_LITERAL(55, 846, 11), // "errorString"
QT_MOC_LITERAL(56, 858, 13), // "visibleRegion"
QT_MOC_LITERAL(57, 872, 9) // "QGeoShape"

    },
    "QDeclarativeGeoMap\0pluginChanged\0\0"
    "QDeclarativeGeoServiceProvider*\0plugin\0"
    "zoomLevelChanged\0zoomLevel\0centerChanged\0"
    "QGeoCoordinate\0coordinate\0"
    "activeMapTypeChanged\0supportedMapTypesChanged\0"
    "minimumZoomLevelChanged\0maximumZoomLevelChanged\0"
    "mapItemsChanged\0errorChanged\0"
    "copyrightLinkActivated\0link\0colorChanged\0"
    "color\0mappingManagerInitialized\0"
    "mapZoomLevelChanged\0zoom\0mapCenterChanged\0"
    "center\0pluginReady\0onMapChildrenChanged\0"
    "onSupportedMapTypesChanged\0removeMapItem\0"
    "QDeclarativeGeoMapItemBase*\0item\0"
    "addMapItem\0clearMapItems\0toCoordinate\0"
    "position\0clipToViewPort\0fromCoordinate\0"
    "fitViewportToMapItems\0pan\0dx\0dy\0"
    "prefetchData\0clearData\0gesture\0"
    "QQuickGeoMapGestureArea*\0minimumZoomLevel\0"
    "maximumZoomLevel\0activeMapType\0"
    "QDeclarativeGeoMapType*\0supportedMapTypes\0"
    "QQmlListProperty<QDeclarativeGeoMapType>\0"
    "mapItems\0QList<QObject*>\0error\0"
    "QGeoServiceProvider::Error\0errorString\0"
    "visibleRegion\0QGeoShape"
};
#undef QT_MOC_LITERAL

static const uint qt_meta_data_QDeclarativeGeoMap[] = {

 // content:
       7,       // revision
       0,       // classname
       0,    0, // classinfo
      28,   14, // methods
      13,  216, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
      11,       // signalCount

 // signals: name, argc, parameters, tag, flags
       1,    1,  154,    2, 0x06 /* Public */,
       5,    1,  157,    2, 0x06 /* Public */,
       7,    1,  160,    2, 0x06 /* Public */,
      10,    0,  163,    2, 0x06 /* Public */,
      11,    0,  164,    2, 0x06 /* Public */,
      12,    0,  165,    2, 0x06 /* Public */,
      13,    0,  166,    2, 0x06 /* Public */,
      14,    0,  167,    2, 0x06 /* Public */,
      15,    0,  168,    2, 0x06 /* Public */,
      16,    1,  169,    2, 0x06 /* Public */,
      18,    1,  172,    2, 0x06 /* Public */,

 // slots: name, argc, parameters, tag, flags
      20,    0,  175,    2, 0x08 /* Private */,
      21,    1,  176,    2, 0x08 /* Private */,
      23,    1,  179,    2, 0x08 /* Private */,
      25,    0,  182,    2, 0x08 /* Private */,
      26,    0,  183,    2, 0x08 /* Private */,
      27,    0,  184,    2, 0x08 /* Private */,

 // methods: name, argc, parameters, tag, flags
      28,    1,  185,    2, 0x02 /* Public */,
      31,    1,  188,    2, 0x02 /* Public */,
      32,    0,  191,    2, 0x02 /* Public */,
      33,    2,  192,    2, 0x02 /* Public */,
      33,    1,  197,    2, 0x22 /* Public | MethodCloned */,
      36,    2,  200,    2, 0x02 /* Public */,
      36,    1,  205,    2, 0x22 /* Public | MethodCloned */,
      37,    0,  208,    2, 0x02 /* Public */,
      38,    2,  209,    2, 0x02 /* Public */,
      41,    0,  214,    2, 0x02 /* Public */,
      42,    0,  215,    2, 0x02 /* Public */,

 // signals: parameters
    QMetaType::Void, 0x80000000 | 3,    4,
    QMetaType::Void, QMetaType::QReal,    6,
    QMetaType::Void, 0x80000000 | 8,    9,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void, QMetaType::QString,   17,
    QMetaType::Void, QMetaType::QColor,   19,

 // slots: parameters
    QMetaType::Void,
    QMetaType::Void, QMetaType::QReal,   22,
    QMetaType::Void, 0x80000000 | 8,   24,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,

 // methods: parameters
    QMetaType::Void, 0x80000000 | 29,   30,
    QMetaType::Void, 0x80000000 | 29,   30,
    QMetaType::Void,
    0x80000000 | 8, QMetaType::QPointF, QMetaType::Bool,   34,   35,
    0x80000000 | 8, QMetaType::QPointF,   34,
    QMetaType::QPointF, 0x80000000 | 8, QMetaType::Bool,    9,   35,
    QMetaType::QPointF, 0x80000000 | 8,    9,
    QMetaType::Void,
    QMetaType::Void, QMetaType::Int, QMetaType::Int,   39,   40,
    QMetaType::Void,
    QMetaType::Void,

 // properties: name, type, flags
      43, 0x80000000 | 44, 0x00095409,
       4, 0x80000000 | 3, 0x0049510b,
      45, QMetaType::QReal, 0x00495103,
      46, QMetaType::QReal, 0x00495103,
       6, QMetaType::QReal, 0x00495103,
      47, 0x80000000 | 48, 0x0049510b,
      49, 0x80000000 | 50, 0x00495009,
      24, 0x80000000 | 8, 0x0049510b,
      51, 0x80000000 | 52, 0x00495009,
      53, 0x80000000 | 54, 0x00495009,
      55, QMetaType::QString, 0x00495001,
      56, 0x80000000 | 57, 0x0009510b,
      19, QMetaType::QColor, 0x00495103,

 // properties: notify_signal_id
       0,
       0,
       5,
       6,
       1,
       3,
       4,
       2,
       7,
       8,
       8,
       0,
      10,

 // enums: name, flags, count, data

 // enum data: key, value

       0        // eod
};

void QDeclarativeGeoMap::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        QDeclarativeGeoMap *_t = static_cast<QDeclarativeGeoMap *>(_o);
        Q_UNUSED(_t)
        switch (_id) {
        case 0: _t->pluginChanged((*reinterpret_cast< QDeclarativeGeoServiceProvider*(*)>(_a[1]))); break;
        case 1: _t->zoomLevelChanged((*reinterpret_cast< qreal(*)>(_a[1]))); break;
        case 2: _t->centerChanged((*reinterpret_cast< const QGeoCoordinate(*)>(_a[1]))); break;
        case 3: _t->activeMapTypeChanged(); break;
        case 4: _t->supportedMapTypesChanged(); break;
        case 5: _t->minimumZoomLevelChanged(); break;
        case 6: _t->maximumZoomLevelChanged(); break;
        case 7: _t->mapItemsChanged(); break;
        case 8: _t->errorChanged(); break;
        case 9: _t->copyrightLinkActivated((*reinterpret_cast< const QString(*)>(_a[1]))); break;
        case 10: _t->colorChanged((*reinterpret_cast< const QColor(*)>(_a[1]))); break;
        case 11: _t->mappingManagerInitialized(); break;
        case 12: _t->mapZoomLevelChanged((*reinterpret_cast< qreal(*)>(_a[1]))); break;
        case 13: _t->mapCenterChanged((*reinterpret_cast< const QGeoCoordinate(*)>(_a[1]))); break;
        case 14: _t->pluginReady(); break;
        case 15: _t->onMapChildrenChanged(); break;
        case 16: _t->onSupportedMapTypesChanged(); break;
        case 17: _t->removeMapItem((*reinterpret_cast< QDeclarativeGeoMapItemBase*(*)>(_a[1]))); break;
        case 18: _t->addMapItem((*reinterpret_cast< QDeclarativeGeoMapItemBase*(*)>(_a[1]))); break;
        case 19: _t->clearMapItems(); break;
        case 20: { QGeoCoordinate _r = _t->toCoordinate((*reinterpret_cast< const QPointF(*)>(_a[1])),(*reinterpret_cast< bool(*)>(_a[2])));
            if (_a[0]) *reinterpret_cast< QGeoCoordinate*>(_a[0]) = _r; }  break;
        case 21: { QGeoCoordinate _r = _t->toCoordinate((*reinterpret_cast< const QPointF(*)>(_a[1])));
            if (_a[0]) *reinterpret_cast< QGeoCoordinate*>(_a[0]) = _r; }  break;
        case 22: { QPointF _r = _t->fromCoordinate((*reinterpret_cast< const QGeoCoordinate(*)>(_a[1])),(*reinterpret_cast< bool(*)>(_a[2])));
            if (_a[0]) *reinterpret_cast< QPointF*>(_a[0]) = _r; }  break;
        case 23: { QPointF _r = _t->fromCoordinate((*reinterpret_cast< const QGeoCoordinate(*)>(_a[1])));
            if (_a[0]) *reinterpret_cast< QPointF*>(_a[0]) = _r; }  break;
        case 24: _t->fitViewportToMapItems(); break;
        case 25: _t->pan((*reinterpret_cast< int(*)>(_a[1])),(*reinterpret_cast< int(*)>(_a[2]))); break;
        case 26: _t->prefetchData(); break;
        case 27: _t->clearData(); break;
        default: ;
        }
    } else if (_c == QMetaObject::RegisterMethodArgumentMetaType) {
        switch (_id) {
        default: *reinterpret_cast<int*>(_a[0]) = -1; break;
        case 2:
            switch (*reinterpret_cast<int*>(_a[1])) {
            default: *reinterpret_cast<int*>(_a[0]) = -1; break;
            case 0:
                *reinterpret_cast<int*>(_a[0]) = qRegisterMetaType< QGeoCoordinate >(); break;
            }
            break;
        case 13:
            switch (*reinterpret_cast<int*>(_a[1])) {
            default: *reinterpret_cast<int*>(_a[0]) = -1; break;
            case 0:
                *reinterpret_cast<int*>(_a[0]) = qRegisterMetaType< QGeoCoordinate >(); break;
            }
            break;
        case 22:
            switch (*reinterpret_cast<int*>(_a[1])) {
            default: *reinterpret_cast<int*>(_a[0]) = -1; break;
            case 0:
                *reinterpret_cast<int*>(_a[0]) = qRegisterMetaType< QGeoCoordinate >(); break;
            }
            break;
        case 23:
            switch (*reinterpret_cast<int*>(_a[1])) {
            default: *reinterpret_cast<int*>(_a[0]) = -1; break;
            case 0:
                *reinterpret_cast<int*>(_a[0]) = qRegisterMetaType< QGeoCoordinate >(); break;
            }
            break;
        }
    } else if (_c == QMetaObject::IndexOfMethod) {
        int *result = reinterpret_cast<int *>(_a[0]);
        void **func = reinterpret_cast<void **>(_a[1]);
        {
            typedef void (QDeclarativeGeoMap::*_t)(QDeclarativeGeoServiceProvider * );
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&QDeclarativeGeoMap::pluginChanged)) {
                *result = 0;
                return;
            }
        }
        {
            typedef void (QDeclarativeGeoMap::*_t)(qreal );
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&QDeclarativeGeoMap::zoomLevelChanged)) {
                *result = 1;
                return;
            }
        }
        {
            typedef void (QDeclarativeGeoMap::*_t)(const QGeoCoordinate & );
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&QDeclarativeGeoMap::centerChanged)) {
                *result = 2;
                return;
            }
        }
        {
            typedef void (QDeclarativeGeoMap::*_t)();
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&QDeclarativeGeoMap::activeMapTypeChanged)) {
                *result = 3;
                return;
            }
        }
        {
            typedef void (QDeclarativeGeoMap::*_t)();
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&QDeclarativeGeoMap::supportedMapTypesChanged)) {
                *result = 4;
                return;
            }
        }
        {
            typedef void (QDeclarativeGeoMap::*_t)();
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&QDeclarativeGeoMap::minimumZoomLevelChanged)) {
                *result = 5;
                return;
            }
        }
        {
            typedef void (QDeclarativeGeoMap::*_t)();
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&QDeclarativeGeoMap::maximumZoomLevelChanged)) {
                *result = 6;
                return;
            }
        }
        {
            typedef void (QDeclarativeGeoMap::*_t)();
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&QDeclarativeGeoMap::mapItemsChanged)) {
                *result = 7;
                return;
            }
        }
        {
            typedef void (QDeclarativeGeoMap::*_t)();
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&QDeclarativeGeoMap::errorChanged)) {
                *result = 8;
                return;
            }
        }
        {
            typedef void (QDeclarativeGeoMap::*_t)(const QString & );
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&QDeclarativeGeoMap::copyrightLinkActivated)) {
                *result = 9;
                return;
            }
        }
        {
            typedef void (QDeclarativeGeoMap::*_t)(const QColor & );
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&QDeclarativeGeoMap::colorChanged)) {
                *result = 10;
                return;
            }
        }
    } else if (_c == QMetaObject::RegisterPropertyMetaType) {
        switch (_id) {
        default: *reinterpret_cast<int*>(_a[0]) = -1; break;
        case 7:
            *reinterpret_cast<int*>(_a[0]) = qRegisterMetaType< QGeoCoordinate >(); break;
        case 11:
            *reinterpret_cast<int*>(_a[0]) = qRegisterMetaType< QGeoShape >(); break;
        case 8:
            *reinterpret_cast<int*>(_a[0]) = qRegisterMetaType< QList<QObject*> >(); break;
        case 0:
            *reinterpret_cast<int*>(_a[0]) = qRegisterMetaType< QQuickGeoMapGestureArea* >(); break;
        }
    }

#ifndef QT_NO_PROPERTIES
    else if (_c == QMetaObject::ReadProperty) {
        QDeclarativeGeoMap *_t = static_cast<QDeclarativeGeoMap *>(_o);
        Q_UNUSED(_t)
        void *_v = _a[0];
        switch (_id) {
        case 0: *reinterpret_cast< QQuickGeoMapGestureArea**>(_v) = _t->gesture(); break;
        case 1: *reinterpret_cast< QDeclarativeGeoServiceProvider**>(_v) = _t->plugin(); break;
        case 2: *reinterpret_cast< qreal*>(_v) = _t->minimumZoomLevel(); break;
        case 3: *reinterpret_cast< qreal*>(_v) = _t->maximumZoomLevel(); break;
        case 4: *reinterpret_cast< qreal*>(_v) = _t->zoomLevel(); break;
        case 5: *reinterpret_cast< QDeclarativeGeoMapType**>(_v) = _t->activeMapType(); break;
        case 6: *reinterpret_cast< QQmlListProperty<QDeclarativeGeoMapType>*>(_v) = _t->supportedMapTypes(); break;
        case 7: *reinterpret_cast< QGeoCoordinate*>(_v) = _t->center(); break;
        case 8: *reinterpret_cast< QList<QObject*>*>(_v) = _t->mapItems(); break;
        case 9: *reinterpret_cast< QGeoServiceProvider::Error*>(_v) = _t->error(); break;
        case 10: *reinterpret_cast< QString*>(_v) = _t->errorString(); break;
        case 11: *reinterpret_cast< QGeoShape*>(_v) = _t->visibleRegion(); break;
        case 12: *reinterpret_cast< QColor*>(_v) = _t->color(); break;
        default: break;
        }
    } else if (_c == QMetaObject::WriteProperty) {
        QDeclarativeGeoMap *_t = static_cast<QDeclarativeGeoMap *>(_o);
        Q_UNUSED(_t)
        void *_v = _a[0];
        switch (_id) {
        case 1: _t->setPlugin(*reinterpret_cast< QDeclarativeGeoServiceProvider**>(_v)); break;
        case 2: _t->setMinimumZoomLevel(*reinterpret_cast< qreal*>(_v)); break;
        case 3: _t->setMaximumZoomLevel(*reinterpret_cast< qreal*>(_v)); break;
        case 4: _t->setZoomLevel(*reinterpret_cast< qreal*>(_v)); break;
        case 5: _t->setActiveMapType(*reinterpret_cast< QDeclarativeGeoMapType**>(_v)); break;
        case 7: _t->setCenter(*reinterpret_cast< QGeoCoordinate*>(_v)); break;
        case 11: _t->setVisibleRegion(*reinterpret_cast< QGeoShape*>(_v)); break;
        case 12: _t->setColor(*reinterpret_cast< QColor*>(_v)); break;
        default: break;
        }
    } else if (_c == QMetaObject::ResetProperty) {
    }
#endif // QT_NO_PROPERTIES
}

static const QMetaObject * const qt_meta_extradata_QDeclarativeGeoMap[] = {
        &QGeoServiceProvider::staticMetaObject,
    Q_NULLPTR
};

const QMetaObject QDeclarativeGeoMap::staticMetaObject = {
    { &QQuickItem::staticMetaObject, qt_meta_stringdata_QDeclarativeGeoMap.data,
      qt_meta_data_QDeclarativeGeoMap,  qt_static_metacall, qt_meta_extradata_QDeclarativeGeoMap, Q_NULLPTR}
};


const QMetaObject *QDeclarativeGeoMap::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->dynamicMetaObject() : &staticMetaObject;
}

void *QDeclarativeGeoMap::qt_metacast(const char *_clname)
{
    if (!_clname) return Q_NULLPTR;
    if (!strcmp(_clname, qt_meta_stringdata_QDeclarativeGeoMap.stringdata0))
        return static_cast<void*>(const_cast< QDeclarativeGeoMap*>(this));
    if (!strcmp(_clname, "org.qt-project.Qt.QQmlParserStatus"))
        return static_cast< QQmlParserStatus*>(const_cast< QDeclarativeGeoMap*>(this));
    return QQuickItem::qt_metacast(_clname);
}

int QDeclarativeGeoMap::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QQuickItem::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 28)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 28;
    } else if (_c == QMetaObject::RegisterMethodArgumentMetaType) {
        if (_id < 28)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 28;
    }
#ifndef QT_NO_PROPERTIES
   else if (_c == QMetaObject::ReadProperty || _c == QMetaObject::WriteProperty
            || _c == QMetaObject::ResetProperty || _c == QMetaObject::RegisterPropertyMetaType) {
        qt_static_metacall(this, _c, _id, _a);
        _id -= 13;
    } else if (_c == QMetaObject::QueryPropertyDesignable) {
        _id -= 13;
    } else if (_c == QMetaObject::QueryPropertyScriptable) {
        _id -= 13;
    } else if (_c == QMetaObject::QueryPropertyStored) {
        _id -= 13;
    } else if (_c == QMetaObject::QueryPropertyEditable) {
        _id -= 13;
    } else if (_c == QMetaObject::QueryPropertyUser) {
        _id -= 13;
    }
#endif // QT_NO_PROPERTIES
    return _id;
}

// SIGNAL 0
void QDeclarativeGeoMap::pluginChanged(QDeclarativeGeoServiceProvider * _t1)
{
    void *_a[] = { Q_NULLPTR, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 0, _a);
}

// SIGNAL 1
void QDeclarativeGeoMap::zoomLevelChanged(qreal _t1)
{
    void *_a[] = { Q_NULLPTR, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 1, _a);
}

// SIGNAL 2
void QDeclarativeGeoMap::centerChanged(const QGeoCoordinate & _t1)
{
    void *_a[] = { Q_NULLPTR, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 2, _a);
}

// SIGNAL 3
void QDeclarativeGeoMap::activeMapTypeChanged()
{
    QMetaObject::activate(this, &staticMetaObject, 3, Q_NULLPTR);
}

// SIGNAL 4
void QDeclarativeGeoMap::supportedMapTypesChanged()
{
    QMetaObject::activate(this, &staticMetaObject, 4, Q_NULLPTR);
}

// SIGNAL 5
void QDeclarativeGeoMap::minimumZoomLevelChanged()
{
    QMetaObject::activate(this, &staticMetaObject, 5, Q_NULLPTR);
}

// SIGNAL 6
void QDeclarativeGeoMap::maximumZoomLevelChanged()
{
    QMetaObject::activate(this, &staticMetaObject, 6, Q_NULLPTR);
}

// SIGNAL 7
void QDeclarativeGeoMap::mapItemsChanged()
{
    QMetaObject::activate(this, &staticMetaObject, 7, Q_NULLPTR);
}

// SIGNAL 8
void QDeclarativeGeoMap::errorChanged()
{
    QMetaObject::activate(this, &staticMetaObject, 8, Q_NULLPTR);
}

// SIGNAL 9
void QDeclarativeGeoMap::copyrightLinkActivated(const QString & _t1)
{
    void *_a[] = { Q_NULLPTR, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 9, _a);
}

// SIGNAL 10
void QDeclarativeGeoMap::colorChanged(const QColor & _t1)
{
    void *_a[] = { Q_NULLPTR, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 10, _a);
}
QT_END_MOC_NAMESPACE
