/****************************************************************************
** Meta object code from reading C++ file 'locationsingleton.h'
**
** Created by: The Qt Meta Object Compiler version 67 (Qt 5.6.3)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "../../locationsingleton.h"
#include <QtCore/qbytearray.h>
#include <QtCore/qmetatype.h>
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'locationsingleton.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 67
#error "This file was generated using the moc from 5.6.3. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
struct qt_meta_stringdata_LocationSingleton_t {
    QByteArrayData data[22];
    char stringdata0[221];
};
#define QT_MOC_LITERAL(idx, ofs, len) \
    Q_STATIC_BYTE_ARRAY_DATA_HEADER_INITIALIZER_WITH_OFFSET(len, \
    qptrdiff(offsetof(qt_meta_stringdata_LocationSingleton_t, stringdata0) + ofs \
        - idx * sizeof(QByteArrayData)) \
    )
static const qt_meta_stringdata_LocationSingleton_t qt_meta_stringdata_LocationSingleton = {
    {
QT_MOC_LITERAL(0, 0, 17), // "LocationSingleton"
QT_MOC_LITERAL(1, 18, 10), // "coordinate"
QT_MOC_LITERAL(2, 29, 14), // "QGeoCoordinate"
QT_MOC_LITERAL(3, 44, 0), // ""
QT_MOC_LITERAL(4, 45, 8), // "latitude"
QT_MOC_LITERAL(5, 54, 9), // "longitude"
QT_MOC_LITERAL(6, 64, 8), // "altitude"
QT_MOC_LITERAL(7, 73, 5), // "shape"
QT_MOC_LITERAL(8, 79, 9), // "QGeoShape"
QT_MOC_LITERAL(9, 89, 9), // "rectangle"
QT_MOC_LITERAL(10, 99, 13), // "QGeoRectangle"
QT_MOC_LITERAL(11, 113, 6), // "center"
QT_MOC_LITERAL(12, 120, 5), // "width"
QT_MOC_LITERAL(13, 126, 6), // "height"
QT_MOC_LITERAL(14, 133, 7), // "topLeft"
QT_MOC_LITERAL(15, 141, 11), // "bottomRight"
QT_MOC_LITERAL(16, 153, 11), // "coordinates"
QT_MOC_LITERAL(17, 165, 6), // "circle"
QT_MOC_LITERAL(18, 172, 10), // "QGeoCircle"
QT_MOC_LITERAL(19, 183, 6), // "radius"
QT_MOC_LITERAL(20, 190, 13), // "shapeToCircle"
QT_MOC_LITERAL(21, 204, 16) // "shapeToRectangle"

    },
    "LocationSingleton\0coordinate\0"
    "QGeoCoordinate\0\0latitude\0longitude\0"
    "altitude\0shape\0QGeoShape\0rectangle\0"
    "QGeoRectangle\0center\0width\0height\0"
    "topLeft\0bottomRight\0coordinates\0circle\0"
    "QGeoCircle\0radius\0shapeToCircle\0"
    "shapeToRectangle"
};
#undef QT_MOC_LITERAL

static const uint qt_meta_data_LocationSingleton[] = {

 // content:
       7,       // revision
       0,       // classname
       0,    0, // classinfo
      13,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       0,       // signalCount

 // methods: name, argc, parameters, tag, flags
       1,    0,   79,    3, 0x02 /* Public */,
       1,    3,   80,    3, 0x02 /* Public */,
       1,    2,   87,    3, 0x22 /* Public | MethodCloned */,
       7,    0,   92,    3, 0x02 /* Public */,
       9,    0,   93,    3, 0x02 /* Public */,
       9,    3,   94,    3, 0x02 /* Public */,
       9,    2,  101,    3, 0x02 /* Public */,
       9,    1,  106,    3, 0x02 /* Public */,
      17,    0,  109,    3, 0x02 /* Public */,
      17,    2,  110,    3, 0x02 /* Public */,
      17,    1,  115,    3, 0x22 /* Public | MethodCloned */,
      20,    1,  118,    3, 0x02 /* Public */,
      21,    1,  121,    3, 0x02 /* Public */,

 // methods: parameters
    0x80000000 | 2,
    0x80000000 | 2, QMetaType::Double, QMetaType::Double, QMetaType::Double,    4,    5,    6,
    0x80000000 | 2, QMetaType::Double, QMetaType::Double,    4,    5,
    0x80000000 | 8,
    0x80000000 | 10,
    0x80000000 | 10, 0x80000000 | 2, QMetaType::Double, QMetaType::Double,   11,   12,   13,
    0x80000000 | 10, 0x80000000 | 2, 0x80000000 | 2,   14,   15,
    0x80000000 | 10, QMetaType::QVariantList,   16,
    0x80000000 | 18,
    0x80000000 | 18, 0x80000000 | 2, QMetaType::QReal,   11,   19,
    0x80000000 | 18, 0x80000000 | 2,   11,
    0x80000000 | 18, 0x80000000 | 8,    7,
    0x80000000 | 10, 0x80000000 | 8,    7,

       0        // eod
};

void LocationSingleton::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        LocationSingleton *_t = static_cast<LocationSingleton *>(_o);
        Q_UNUSED(_t)
        switch (_id) {
        case 0: { QGeoCoordinate _r = _t->coordinate();
            if (_a[0]) *reinterpret_cast< QGeoCoordinate*>(_a[0]) = _r; }  break;
        case 1: { QGeoCoordinate _r = _t->coordinate((*reinterpret_cast< double(*)>(_a[1])),(*reinterpret_cast< double(*)>(_a[2])),(*reinterpret_cast< double(*)>(_a[3])));
            if (_a[0]) *reinterpret_cast< QGeoCoordinate*>(_a[0]) = _r; }  break;
        case 2: { QGeoCoordinate _r = _t->coordinate((*reinterpret_cast< double(*)>(_a[1])),(*reinterpret_cast< double(*)>(_a[2])));
            if (_a[0]) *reinterpret_cast< QGeoCoordinate*>(_a[0]) = _r; }  break;
        case 3: { QGeoShape _r = _t->shape();
            if (_a[0]) *reinterpret_cast< QGeoShape*>(_a[0]) = _r; }  break;
        case 4: { QGeoRectangle _r = _t->rectangle();
            if (_a[0]) *reinterpret_cast< QGeoRectangle*>(_a[0]) = _r; }  break;
        case 5: { QGeoRectangle _r = _t->rectangle((*reinterpret_cast< const QGeoCoordinate(*)>(_a[1])),(*reinterpret_cast< double(*)>(_a[2])),(*reinterpret_cast< double(*)>(_a[3])));
            if (_a[0]) *reinterpret_cast< QGeoRectangle*>(_a[0]) = _r; }  break;
        case 6: { QGeoRectangle _r = _t->rectangle((*reinterpret_cast< const QGeoCoordinate(*)>(_a[1])),(*reinterpret_cast< const QGeoCoordinate(*)>(_a[2])));
            if (_a[0]) *reinterpret_cast< QGeoRectangle*>(_a[0]) = _r; }  break;
        case 7: { QGeoRectangle _r = _t->rectangle((*reinterpret_cast< const QVariantList(*)>(_a[1])));
            if (_a[0]) *reinterpret_cast< QGeoRectangle*>(_a[0]) = _r; }  break;
        case 8: { QGeoCircle _r = _t->circle();
            if (_a[0]) *reinterpret_cast< QGeoCircle*>(_a[0]) = _r; }  break;
        case 9: { QGeoCircle _r = _t->circle((*reinterpret_cast< const QGeoCoordinate(*)>(_a[1])),(*reinterpret_cast< qreal(*)>(_a[2])));
            if (_a[0]) *reinterpret_cast< QGeoCircle*>(_a[0]) = _r; }  break;
        case 10: { QGeoCircle _r = _t->circle((*reinterpret_cast< const QGeoCoordinate(*)>(_a[1])));
            if (_a[0]) *reinterpret_cast< QGeoCircle*>(_a[0]) = _r; }  break;
        case 11: { QGeoCircle _r = _t->shapeToCircle((*reinterpret_cast< const QGeoShape(*)>(_a[1])));
            if (_a[0]) *reinterpret_cast< QGeoCircle*>(_a[0]) = _r; }  break;
        case 12: { QGeoRectangle _r = _t->shapeToRectangle((*reinterpret_cast< const QGeoShape(*)>(_a[1])));
            if (_a[0]) *reinterpret_cast< QGeoRectangle*>(_a[0]) = _r; }  break;
        default: ;
        }
    } else if (_c == QMetaObject::RegisterMethodArgumentMetaType) {
        switch (_id) {
        default: *reinterpret_cast<int*>(_a[0]) = -1; break;
        case 5:
            switch (*reinterpret_cast<int*>(_a[1])) {
            default: *reinterpret_cast<int*>(_a[0]) = -1; break;
            case 0:
                *reinterpret_cast<int*>(_a[0]) = qRegisterMetaType< QGeoCoordinate >(); break;
            }
            break;
        case 6:
            switch (*reinterpret_cast<int*>(_a[1])) {
            default: *reinterpret_cast<int*>(_a[0]) = -1; break;
            case 1:
            case 0:
                *reinterpret_cast<int*>(_a[0]) = qRegisterMetaType< QGeoCoordinate >(); break;
            }
            break;
        case 9:
            switch (*reinterpret_cast<int*>(_a[1])) {
            default: *reinterpret_cast<int*>(_a[0]) = -1; break;
            case 0:
                *reinterpret_cast<int*>(_a[0]) = qRegisterMetaType< QGeoCoordinate >(); break;
            }
            break;
        case 10:
            switch (*reinterpret_cast<int*>(_a[1])) {
            default: *reinterpret_cast<int*>(_a[0]) = -1; break;
            case 0:
                *reinterpret_cast<int*>(_a[0]) = qRegisterMetaType< QGeoCoordinate >(); break;
            }
            break;
        case 11:
            switch (*reinterpret_cast<int*>(_a[1])) {
            default: *reinterpret_cast<int*>(_a[0]) = -1; break;
            case 0:
                *reinterpret_cast<int*>(_a[0]) = qRegisterMetaType< QGeoShape >(); break;
            }
            break;
        case 12:
            switch (*reinterpret_cast<int*>(_a[1])) {
            default: *reinterpret_cast<int*>(_a[0]) = -1; break;
            case 0:
                *reinterpret_cast<int*>(_a[0]) = qRegisterMetaType< QGeoShape >(); break;
            }
            break;
        }
    }
}

const QMetaObject LocationSingleton::staticMetaObject = {
    { &QObject::staticMetaObject, qt_meta_stringdata_LocationSingleton.data,
      qt_meta_data_LocationSingleton,  qt_static_metacall, Q_NULLPTR, Q_NULLPTR}
};


const QMetaObject *LocationSingleton::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->dynamicMetaObject() : &staticMetaObject;
}

void *LocationSingleton::qt_metacast(const char *_clname)
{
    if (!_clname) return Q_NULLPTR;
    if (!strcmp(_clname, qt_meta_stringdata_LocationSingleton.stringdata0))
        return static_cast<void*>(const_cast< LocationSingleton*>(this));
    return QObject::qt_metacast(_clname);
}

int LocationSingleton::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QObject::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 13)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 13;
    } else if (_c == QMetaObject::RegisterMethodArgumentMetaType) {
        if (_id < 13)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 13;
    }
    return _id;
}
QT_END_MOC_NAMESPACE
