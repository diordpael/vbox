/****************************************************************************
** Meta object code from reading C++ file 'qgeorectangle.h'
**
** Created by: The Qt Meta Object Compiler version 67 (Qt 5.6.3)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "../../qgeorectangle.h"
#include <QtCore/qbytearray.h>
#include <QtCore/qmetatype.h>
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'qgeorectangle.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 67
#error "This file was generated using the moc from 5.6.3. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
struct qt_meta_stringdata_QGeoRectangle_t {
    QByteArrayData data[11];
    char stringdata0[99];
};
#define QT_MOC_LITERAL(idx, ofs, len) \
    Q_STATIC_BYTE_ARRAY_DATA_HEADER_INITIALIZER_WITH_OFFSET(len, \
    qptrdiff(offsetof(qt_meta_stringdata_QGeoRectangle_t, stringdata0) + ofs \
        - idx * sizeof(QByteArrayData)) \
    )
static const qt_meta_stringdata_QGeoRectangle_t qt_meta_stringdata_QGeoRectangle = {
    {
QT_MOC_LITERAL(0, 0, 13), // "QGeoRectangle"
QT_MOC_LITERAL(1, 14, 8), // "toString"
QT_MOC_LITERAL(2, 23, 0), // ""
QT_MOC_LITERAL(3, 24, 10), // "bottomLeft"
QT_MOC_LITERAL(4, 35, 14), // "QGeoCoordinate"
QT_MOC_LITERAL(5, 50, 11), // "bottomRight"
QT_MOC_LITERAL(6, 62, 7), // "topLeft"
QT_MOC_LITERAL(7, 70, 8), // "topRight"
QT_MOC_LITERAL(8, 79, 6), // "center"
QT_MOC_LITERAL(9, 86, 6), // "height"
QT_MOC_LITERAL(10, 93, 5) // "width"

    },
    "QGeoRectangle\0toString\0\0bottomLeft\0"
    "QGeoCoordinate\0bottomRight\0topLeft\0"
    "topRight\0center\0height\0width"
};
#undef QT_MOC_LITERAL

static const uint qt_meta_data_QGeoRectangle[] = {

 // content:
       7,       // revision
       0,       // classname
       0,    0, // classinfo
       1,   14, // methods
       7,   20, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       4,       // flags
       0,       // signalCount

 // methods: name, argc, parameters, tag, flags
       1,    0,   19,    2, 0x02 /* Public */,

 // methods: parameters
    QMetaType::QString,

 // properties: name, type, flags
       3, 0x80000000 | 4, 0x0009510b,
       5, 0x80000000 | 4, 0x0009510b,
       6, 0x80000000 | 4, 0x0009510b,
       7, 0x80000000 | 4, 0x0009510b,
       8, 0x80000000 | 4, 0x0009510b,
       9, QMetaType::Double, 0x00095103,
      10, QMetaType::Double, 0x00095103,

       0        // eod
};

void QGeoRectangle::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        QGeoRectangle *_t = reinterpret_cast<QGeoRectangle *>(_o);
        Q_UNUSED(_t)
        switch (_id) {
        case 0: { QString _r = _t->toString();
            if (_a[0]) *reinterpret_cast< QString*>(_a[0]) = _r; }  break;
        default: ;
        }
    } else if (_c == QMetaObject::RegisterPropertyMetaType) {
        switch (_id) {
        default: *reinterpret_cast<int*>(_a[0]) = -1; break;
        case 4:
        case 3:
        case 2:
        case 1:
        case 0:
            *reinterpret_cast<int*>(_a[0]) = qRegisterMetaType< QGeoCoordinate >(); break;
        }
    }

#ifndef QT_NO_PROPERTIES
    else if (_c == QMetaObject::ReadProperty) {
        QGeoRectangle *_t = reinterpret_cast<QGeoRectangle *>(_o);
        Q_UNUSED(_t)
        void *_v = _a[0];
        switch (_id) {
        case 0: *reinterpret_cast< QGeoCoordinate*>(_v) = _t->bottomLeft(); break;
        case 1: *reinterpret_cast< QGeoCoordinate*>(_v) = _t->bottomRight(); break;
        case 2: *reinterpret_cast< QGeoCoordinate*>(_v) = _t->topLeft(); break;
        case 3: *reinterpret_cast< QGeoCoordinate*>(_v) = _t->topRight(); break;
        case 4: *reinterpret_cast< QGeoCoordinate*>(_v) = _t->center(); break;
        case 5: *reinterpret_cast< double*>(_v) = _t->height(); break;
        case 6: *reinterpret_cast< double*>(_v) = _t->width(); break;
        default: break;
        }
    } else if (_c == QMetaObject::WriteProperty) {
        QGeoRectangle *_t = reinterpret_cast<QGeoRectangle *>(_o);
        Q_UNUSED(_t)
        void *_v = _a[0];
        switch (_id) {
        case 0: _t->setBottomLeft(*reinterpret_cast< QGeoCoordinate*>(_v)); break;
        case 1: _t->setBottomRight(*reinterpret_cast< QGeoCoordinate*>(_v)); break;
        case 2: _t->setTopLeft(*reinterpret_cast< QGeoCoordinate*>(_v)); break;
        case 3: _t->setTopRight(*reinterpret_cast< QGeoCoordinate*>(_v)); break;
        case 4: _t->setCenter(*reinterpret_cast< QGeoCoordinate*>(_v)); break;
        case 5: _t->setHeight(*reinterpret_cast< double*>(_v)); break;
        case 6: _t->setWidth(*reinterpret_cast< double*>(_v)); break;
        default: break;
        }
    } else if (_c == QMetaObject::ResetProperty) {
    }
#endif // QT_NO_PROPERTIES
}

const QMetaObject QGeoRectangle::staticMetaObject = {
    { &QGeoShape::staticMetaObject, qt_meta_stringdata_QGeoRectangle.data,
      qt_meta_data_QGeoRectangle,  qt_static_metacall, Q_NULLPTR, Q_NULLPTR}
};

QT_END_MOC_NAMESPACE
