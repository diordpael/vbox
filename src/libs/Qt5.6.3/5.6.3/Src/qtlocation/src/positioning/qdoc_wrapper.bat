@echo off
SetLocal EnableDelayedExpansion
(set QT_VERSION=5.6.3)
(set QT_VER=5.6)
(set QT_VERSION_TAG=563)
(set QT_INSTALL_DOCS=C:/Qt/Qt5.6.3/5.6.3/Src/qtbase/doc)
(set PATH=C:\Qt\Qt5.6.3\5.6.3\Src\qtdeclarative\lib;C:\Qt\Qt5.6.3\5.6.3\Src\qtbase\lib;!PATH!)
if defined QT_PLUGIN_PATH (
    set QT_PLUGIN_PATH=C:\Qt\Qt5.6.3\5.6.3\Src\qtbase\plugins;!QT_PLUGIN_PATH!
) else (
    set QT_PLUGIN_PATH=C:\Qt\Qt5.6.3\5.6.3\Src\qtbase\plugins
)
C:\Qt\Qt5.6.3\5.6.3\Src\qttools\bin\qdoc.exe %*
EndLocal
