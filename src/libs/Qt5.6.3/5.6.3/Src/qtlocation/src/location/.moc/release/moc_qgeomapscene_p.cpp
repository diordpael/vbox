/****************************************************************************
** Meta object code from reading C++ file 'qgeomapscene_p.h'
**
** Created by: The Qt Meta Object Compiler version 67 (Qt 5.6.3)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "../../maps/qgeomapscene_p.h"
#include <QtCore/qbytearray.h>
#include <QtCore/qmetatype.h>
#include <QtCore/QSet>
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'qgeomapscene_p.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 67
#error "This file was generated using the moc from 5.6.3. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
struct qt_meta_stringdata_QGeoMapScene_t {
    QByteArrayData data[5];
    char stringdata0[58];
};
#define QT_MOC_LITERAL(idx, ofs, len) \
    Q_STATIC_BYTE_ARRAY_DATA_HEADER_INITIALIZER_WITH_OFFSET(len, \
    qptrdiff(offsetof(qt_meta_stringdata_QGeoMapScene_t, stringdata0) + ofs \
        - idx * sizeof(QByteArrayData)) \
    )
static const qt_meta_stringdata_QGeoMapScene_t qt_meta_stringdata_QGeoMapScene = {
    {
QT_MOC_LITERAL(0, 0, 12), // "QGeoMapScene"
QT_MOC_LITERAL(1, 13, 15), // "newTilesVisible"
QT_MOC_LITERAL(2, 29, 0), // ""
QT_MOC_LITERAL(3, 30, 18), // "QSet<QGeoTileSpec>"
QT_MOC_LITERAL(4, 49, 8) // "newTiles"

    },
    "QGeoMapScene\0newTilesVisible\0\0"
    "QSet<QGeoTileSpec>\0newTiles"
};
#undef QT_MOC_LITERAL

static const uint qt_meta_data_QGeoMapScene[] = {

 // content:
       7,       // revision
       0,       // classname
       0,    0, // classinfo
       1,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       1,       // signalCount

 // signals: name, argc, parameters, tag, flags
       1,    1,   19,    2, 0x06 /* Public */,

 // signals: parameters
    QMetaType::Void, 0x80000000 | 3,    4,

       0        // eod
};

void QGeoMapScene::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        QGeoMapScene *_t = static_cast<QGeoMapScene *>(_o);
        Q_UNUSED(_t)
        switch (_id) {
        case 0: _t->newTilesVisible((*reinterpret_cast< const QSet<QGeoTileSpec>(*)>(_a[1]))); break;
        default: ;
        }
    } else if (_c == QMetaObject::IndexOfMethod) {
        int *result = reinterpret_cast<int *>(_a[0]);
        void **func = reinterpret_cast<void **>(_a[1]);
        {
            typedef void (QGeoMapScene::*_t)(const QSet<QGeoTileSpec> & );
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&QGeoMapScene::newTilesVisible)) {
                *result = 0;
                return;
            }
        }
    }
}

const QMetaObject QGeoMapScene::staticMetaObject = {
    { &QObject::staticMetaObject, qt_meta_stringdata_QGeoMapScene.data,
      qt_meta_data_QGeoMapScene,  qt_static_metacall, Q_NULLPTR, Q_NULLPTR}
};


const QMetaObject *QGeoMapScene::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->dynamicMetaObject() : &staticMetaObject;
}

void *QGeoMapScene::qt_metacast(const char *_clname)
{
    if (!_clname) return Q_NULLPTR;
    if (!strcmp(_clname, qt_meta_stringdata_QGeoMapScene.stringdata0))
        return static_cast<void*>(const_cast< QGeoMapScene*>(this));
    return QObject::qt_metacast(_clname);
}

int QGeoMapScene::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QObject::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 1)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 1;
    } else if (_c == QMetaObject::RegisterMethodArgumentMetaType) {
        if (_id < 1)
            *reinterpret_cast<int*>(_a[0]) = -1;
        _id -= 1;
    }
    return _id;
}

// SIGNAL 0
void QGeoMapScene::newTilesVisible(const QSet<QGeoTileSpec> & _t1)
{
    void *_a[] = { Q_NULLPTR, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 0, _a);
}
QT_END_MOC_NAMESPACE
