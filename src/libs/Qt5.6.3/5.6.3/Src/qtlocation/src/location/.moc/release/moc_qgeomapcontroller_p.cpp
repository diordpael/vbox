/****************************************************************************
** Meta object code from reading C++ file 'qgeomapcontroller_p.h'
**
** Created by: The Qt Meta Object Compiler version 67 (Qt 5.6.3)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "../../maps/qgeomapcontroller_p.h"
#include <QtCore/qbytearray.h>
#include <QtCore/qmetatype.h>
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'qgeomapcontroller_p.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 67
#error "This file was generated using the moc from 5.6.3. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
struct qt_meta_stringdata_QGeoMapController_t {
    QByteArrayData data[16];
    char stringdata0[173];
};
#define QT_MOC_LITERAL(idx, ofs, len) \
    Q_STATIC_BYTE_ARRAY_DATA_HEADER_INITIALIZER_WITH_OFFSET(len, \
    qptrdiff(offsetof(qt_meta_stringdata_QGeoMapController_t, stringdata0) + ofs \
        - idx * sizeof(QByteArrayData)) \
    )
static const qt_meta_stringdata_QGeoMapController_t qt_meta_stringdata_QGeoMapController = {
    {
QT_MOC_LITERAL(0, 0, 17), // "QGeoMapController"
QT_MOC_LITERAL(1, 18, 13), // "centerChanged"
QT_MOC_LITERAL(2, 32, 0), // ""
QT_MOC_LITERAL(3, 33, 14), // "QGeoCoordinate"
QT_MOC_LITERAL(4, 48, 6), // "center"
QT_MOC_LITERAL(5, 55, 14), // "bearingChanged"
QT_MOC_LITERAL(6, 70, 7), // "bearing"
QT_MOC_LITERAL(7, 78, 11), // "tiltChanged"
QT_MOC_LITERAL(8, 90, 4), // "tilt"
QT_MOC_LITERAL(9, 95, 11), // "rollChanged"
QT_MOC_LITERAL(10, 107, 4), // "roll"
QT_MOC_LITERAL(11, 112, 11), // "zoomChanged"
QT_MOC_LITERAL(12, 124, 4), // "zoom"
QT_MOC_LITERAL(13, 129, 17), // "cameraDataChanged"
QT_MOC_LITERAL(14, 147, 14), // "QGeoCameraData"
QT_MOC_LITERAL(15, 162, 10) // "cameraData"

    },
    "QGeoMapController\0centerChanged\0\0"
    "QGeoCoordinate\0center\0bearingChanged\0"
    "bearing\0tiltChanged\0tilt\0rollChanged\0"
    "roll\0zoomChanged\0zoom\0cameraDataChanged\0"
    "QGeoCameraData\0cameraData"
};
#undef QT_MOC_LITERAL

static const uint qt_meta_data_QGeoMapController[] = {

 // content:
       7,       // revision
       0,       // classname
       0,    0, // classinfo
       6,   14, // methods
       5,   62, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       5,       // signalCount

 // signals: name, argc, parameters, tag, flags
       1,    1,   44,    2, 0x06 /* Public */,
       5,    1,   47,    2, 0x06 /* Public */,
       7,    1,   50,    2, 0x06 /* Public */,
       9,    1,   53,    2, 0x06 /* Public */,
      11,    1,   56,    2, 0x06 /* Public */,

 // slots: name, argc, parameters, tag, flags
      13,    1,   59,    2, 0x08 /* Private */,

 // signals: parameters
    QMetaType::Void, 0x80000000 | 3,    4,
    QMetaType::Void, QMetaType::QReal,    6,
    QMetaType::Void, QMetaType::QReal,    8,
    QMetaType::Void, QMetaType::QReal,   10,
    QMetaType::Void, QMetaType::QReal,   12,

 // slots: parameters
    QMetaType::Void, 0x80000000 | 14,   15,

 // properties: name, type, flags
       4, 0x80000000 | 3, 0x0049510b,
       6, QMetaType::QReal, 0x00495103,
       8, QMetaType::QReal, 0x00495103,
      10, QMetaType::QReal, 0x00495103,
      12, QMetaType::QReal, 0x00495103,

 // properties: notify_signal_id
       0,
       1,
       2,
       3,
       4,

       0        // eod
};

void QGeoMapController::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        QGeoMapController *_t = static_cast<QGeoMapController *>(_o);
        Q_UNUSED(_t)
        switch (_id) {
        case 0: _t->centerChanged((*reinterpret_cast< const QGeoCoordinate(*)>(_a[1]))); break;
        case 1: _t->bearingChanged((*reinterpret_cast< qreal(*)>(_a[1]))); break;
        case 2: _t->tiltChanged((*reinterpret_cast< qreal(*)>(_a[1]))); break;
        case 3: _t->rollChanged((*reinterpret_cast< qreal(*)>(_a[1]))); break;
        case 4: _t->zoomChanged((*reinterpret_cast< qreal(*)>(_a[1]))); break;
        case 5: _t->cameraDataChanged((*reinterpret_cast< const QGeoCameraData(*)>(_a[1]))); break;
        default: ;
        }
    } else if (_c == QMetaObject::RegisterMethodArgumentMetaType) {
        switch (_id) {
        default: *reinterpret_cast<int*>(_a[0]) = -1; break;
        case 0:
            switch (*reinterpret_cast<int*>(_a[1])) {
            default: *reinterpret_cast<int*>(_a[0]) = -1; break;
            case 0:
                *reinterpret_cast<int*>(_a[0]) = qRegisterMetaType< QGeoCoordinate >(); break;
            }
            break;
        case 5:
            switch (*reinterpret_cast<int*>(_a[1])) {
            default: *reinterpret_cast<int*>(_a[0]) = -1; break;
            case 0:
                *reinterpret_cast<int*>(_a[0]) = qRegisterMetaType< QGeoCameraData >(); break;
            }
            break;
        }
    } else if (_c == QMetaObject::IndexOfMethod) {
        int *result = reinterpret_cast<int *>(_a[0]);
        void **func = reinterpret_cast<void **>(_a[1]);
        {
            typedef void (QGeoMapController::*_t)(const QGeoCoordinate & );
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&QGeoMapController::centerChanged)) {
                *result = 0;
                return;
            }
        }
        {
            typedef void (QGeoMapController::*_t)(qreal );
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&QGeoMapController::bearingChanged)) {
                *result = 1;
                return;
            }
        }
        {
            typedef void (QGeoMapController::*_t)(qreal );
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&QGeoMapController::tiltChanged)) {
                *result = 2;
                return;
            }
        }
        {
            typedef void (QGeoMapController::*_t)(qreal );
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&QGeoMapController::rollChanged)) {
                *result = 3;
                return;
            }
        }
        {
            typedef void (QGeoMapController::*_t)(qreal );
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&QGeoMapController::zoomChanged)) {
                *result = 4;
                return;
            }
        }
    } else if (_c == QMetaObject::RegisterPropertyMetaType) {
        switch (_id) {
        default: *reinterpret_cast<int*>(_a[0]) = -1; break;
        case 0:
            *reinterpret_cast<int*>(_a[0]) = qRegisterMetaType< QGeoCoordinate >(); break;
        }
    }

#ifndef QT_NO_PROPERTIES
    else if (_c == QMetaObject::ReadProperty) {
        QGeoMapController *_t = static_cast<QGeoMapController *>(_o);
        Q_UNUSED(_t)
        void *_v = _a[0];
        switch (_id) {
        case 0: *reinterpret_cast< QGeoCoordinate*>(_v) = _t->center(); break;
        case 1: *reinterpret_cast< qreal*>(_v) = _t->bearing(); break;
        case 2: *reinterpret_cast< qreal*>(_v) = _t->tilt(); break;
        case 3: *reinterpret_cast< qreal*>(_v) = _t->roll(); break;
        case 4: *reinterpret_cast< qreal*>(_v) = _t->zoom(); break;
        default: break;
        }
    } else if (_c == QMetaObject::WriteProperty) {
        QGeoMapController *_t = static_cast<QGeoMapController *>(_o);
        Q_UNUSED(_t)
        void *_v = _a[0];
        switch (_id) {
        case 0: _t->setCenter(*reinterpret_cast< QGeoCoordinate*>(_v)); break;
        case 1: _t->setBearing(*reinterpret_cast< qreal*>(_v)); break;
        case 2: _t->setTilt(*reinterpret_cast< qreal*>(_v)); break;
        case 3: _t->setRoll(*reinterpret_cast< qreal*>(_v)); break;
        case 4: _t->setZoom(*reinterpret_cast< qreal*>(_v)); break;
        default: break;
        }
    } else if (_c == QMetaObject::ResetProperty) {
    }
#endif // QT_NO_PROPERTIES
}

const QMetaObject QGeoMapController::staticMetaObject = {
    { &QObject::staticMetaObject, qt_meta_stringdata_QGeoMapController.data,
      qt_meta_data_QGeoMapController,  qt_static_metacall, Q_NULLPTR, Q_NULLPTR}
};


const QMetaObject *QGeoMapController::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->dynamicMetaObject() : &staticMetaObject;
}

void *QGeoMapController::qt_metacast(const char *_clname)
{
    if (!_clname) return Q_NULLPTR;
    if (!strcmp(_clname, qt_meta_stringdata_QGeoMapController.stringdata0))
        return static_cast<void*>(const_cast< QGeoMapController*>(this));
    return QObject::qt_metacast(_clname);
}

int QGeoMapController::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QObject::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 6)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 6;
    } else if (_c == QMetaObject::RegisterMethodArgumentMetaType) {
        if (_id < 6)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 6;
    }
#ifndef QT_NO_PROPERTIES
   else if (_c == QMetaObject::ReadProperty || _c == QMetaObject::WriteProperty
            || _c == QMetaObject::ResetProperty || _c == QMetaObject::RegisterPropertyMetaType) {
        qt_static_metacall(this, _c, _id, _a);
        _id -= 5;
    } else if (_c == QMetaObject::QueryPropertyDesignable) {
        _id -= 5;
    } else if (_c == QMetaObject::QueryPropertyScriptable) {
        _id -= 5;
    } else if (_c == QMetaObject::QueryPropertyStored) {
        _id -= 5;
    } else if (_c == QMetaObject::QueryPropertyEditable) {
        _id -= 5;
    } else if (_c == QMetaObject::QueryPropertyUser) {
        _id -= 5;
    }
#endif // QT_NO_PROPERTIES
    return _id;
}

// SIGNAL 0
void QGeoMapController::centerChanged(const QGeoCoordinate & _t1)
{
    void *_a[] = { Q_NULLPTR, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 0, _a);
}

// SIGNAL 1
void QGeoMapController::bearingChanged(qreal _t1)
{
    void *_a[] = { Q_NULLPTR, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 1, _a);
}

// SIGNAL 2
void QGeoMapController::tiltChanged(qreal _t1)
{
    void *_a[] = { Q_NULLPTR, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 2, _a);
}

// SIGNAL 3
void QGeoMapController::rollChanged(qreal _t1)
{
    void *_a[] = { Q_NULLPTR, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 3, _a);
}

// SIGNAL 4
void QGeoMapController::zoomChanged(qreal _t1)
{
    void *_a[] = { Q_NULLPTR, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 4, _a);
}
QT_END_MOC_NAMESPACE
