/****************************************************************************
** Meta object code from reading C++ file 'qgeotileproviderosm.h'
**
** Created by: The Qt Meta Object Compiler version 67 (Qt 5.6.3)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "../../qgeotileproviderosm.h"
#include <QtCore/qbytearray.h>
#include <QtCore/qmetatype.h>
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'qgeotileproviderosm.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 67
#error "This file was generated using the moc from 5.6.3. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
struct qt_meta_stringdata_QGeoTileProviderOsm_t {
    QByteArrayData data[11];
    char stringdata0[185];
};
#define QT_MOC_LITERAL(idx, ofs, len) \
    Q_STATIC_BYTE_ARRAY_DATA_HEADER_INITIALIZER_WITH_OFFSET(len, \
    qptrdiff(offsetof(qt_meta_stringdata_QGeoTileProviderOsm_t, stringdata0) + ofs \
        - idx * sizeof(QByteArrayData)) \
    )
static const qt_meta_stringdata_QGeoTileProviderOsm_t qt_meta_stringdata_QGeoTileProviderOsm = {
    {
QT_MOC_LITERAL(0, 0, 19), // "QGeoTileProviderOsm"
QT_MOC_LITERAL(1, 20, 18), // "resolutionFinished"
QT_MOC_LITERAL(2, 39, 0), // ""
QT_MOC_LITERAL(3, 40, 26), // "const QGeoTileProviderOsm*"
QT_MOC_LITERAL(4, 67, 8), // "provider"
QT_MOC_LITERAL(5, 76, 15), // "resolutionError"
QT_MOC_LITERAL(6, 92, 27), // "QNetworkReply::NetworkError"
QT_MOC_LITERAL(7, 120, 5), // "error"
QT_MOC_LITERAL(8, 126, 22), // "onNetworkReplyFinished"
QT_MOC_LITERAL(9, 149, 19), // "onNetworkReplyError"
QT_MOC_LITERAL(10, 169, 15) // "resolveProvider"

    },
    "QGeoTileProviderOsm\0resolutionFinished\0"
    "\0const QGeoTileProviderOsm*\0provider\0"
    "resolutionError\0QNetworkReply::NetworkError\0"
    "error\0onNetworkReplyFinished\0"
    "onNetworkReplyError\0resolveProvider"
};
#undef QT_MOC_LITERAL

static const uint qt_meta_data_QGeoTileProviderOsm[] = {

 // content:
       7,       // revision
       0,       // classname
       0,    0, // classinfo
       5,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       2,       // signalCount

 // signals: name, argc, parameters, tag, flags
       1,    1,   39,    2, 0x06 /* Public */,
       5,    2,   42,    2, 0x06 /* Public */,

 // slots: name, argc, parameters, tag, flags
       8,    0,   47,    2, 0x0a /* Public */,
       9,    1,   48,    2, 0x0a /* Public */,
      10,    0,   51,    2, 0x0a /* Public */,

 // signals: parameters
    QMetaType::Void, 0x80000000 | 3,    4,
    QMetaType::Void, 0x80000000 | 3, 0x80000000 | 6,    4,    7,

 // slots: parameters
    QMetaType::Void,
    QMetaType::Void, 0x80000000 | 6,    7,
    QMetaType::Void,

       0        // eod
};

void QGeoTileProviderOsm::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        QGeoTileProviderOsm *_t = static_cast<QGeoTileProviderOsm *>(_o);
        Q_UNUSED(_t)
        switch (_id) {
        case 0: _t->resolutionFinished((*reinterpret_cast< const QGeoTileProviderOsm*(*)>(_a[1]))); break;
        case 1: _t->resolutionError((*reinterpret_cast< const QGeoTileProviderOsm*(*)>(_a[1])),(*reinterpret_cast< QNetworkReply::NetworkError(*)>(_a[2]))); break;
        case 2: _t->onNetworkReplyFinished(); break;
        case 3: _t->onNetworkReplyError((*reinterpret_cast< QNetworkReply::NetworkError(*)>(_a[1]))); break;
        case 4: _t->resolveProvider(); break;
        default: ;
        }
    } else if (_c == QMetaObject::RegisterMethodArgumentMetaType) {
        switch (_id) {
        default: *reinterpret_cast<int*>(_a[0]) = -1; break;
        case 1:
            switch (*reinterpret_cast<int*>(_a[1])) {
            default: *reinterpret_cast<int*>(_a[0]) = -1; break;
            case 1:
                *reinterpret_cast<int*>(_a[0]) = qRegisterMetaType< QNetworkReply::NetworkError >(); break;
            }
            break;
        case 3:
            switch (*reinterpret_cast<int*>(_a[1])) {
            default: *reinterpret_cast<int*>(_a[0]) = -1; break;
            case 0:
                *reinterpret_cast<int*>(_a[0]) = qRegisterMetaType< QNetworkReply::NetworkError >(); break;
            }
            break;
        }
    } else if (_c == QMetaObject::IndexOfMethod) {
        int *result = reinterpret_cast<int *>(_a[0]);
        void **func = reinterpret_cast<void **>(_a[1]);
        {
            typedef void (QGeoTileProviderOsm::*_t)(const QGeoTileProviderOsm * );
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&QGeoTileProviderOsm::resolutionFinished)) {
                *result = 0;
                return;
            }
        }
        {
            typedef void (QGeoTileProviderOsm::*_t)(const QGeoTileProviderOsm * , QNetworkReply::NetworkError );
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&QGeoTileProviderOsm::resolutionError)) {
                *result = 1;
                return;
            }
        }
    }
}

const QMetaObject QGeoTileProviderOsm::staticMetaObject = {
    { &QObject::staticMetaObject, qt_meta_stringdata_QGeoTileProviderOsm.data,
      qt_meta_data_QGeoTileProviderOsm,  qt_static_metacall, Q_NULLPTR, Q_NULLPTR}
};


const QMetaObject *QGeoTileProviderOsm::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->dynamicMetaObject() : &staticMetaObject;
}

void *QGeoTileProviderOsm::qt_metacast(const char *_clname)
{
    if (!_clname) return Q_NULLPTR;
    if (!strcmp(_clname, qt_meta_stringdata_QGeoTileProviderOsm.stringdata0))
        return static_cast<void*>(const_cast< QGeoTileProviderOsm*>(this));
    return QObject::qt_metacast(_clname);
}

int QGeoTileProviderOsm::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QObject::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 5)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 5;
    } else if (_c == QMetaObject::RegisterMethodArgumentMetaType) {
        if (_id < 5)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 5;
    }
    return _id;
}

// SIGNAL 0
void QGeoTileProviderOsm::resolutionFinished(const QGeoTileProviderOsm * _t1)
{
    void *_a[] = { Q_NULLPTR, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 0, _a);
}

// SIGNAL 1
void QGeoTileProviderOsm::resolutionError(const QGeoTileProviderOsm * _t1, QNetworkReply::NetworkError _t2)
{
    void *_a[] = { Q_NULLPTR, const_cast<void*>(reinterpret_cast<const void*>(&_t1)), const_cast<void*>(reinterpret_cast<const void*>(&_t2)) };
    QMetaObject::activate(this, &staticMetaObject, 1, _a);
}
QT_END_MOC_NAMESPACE
