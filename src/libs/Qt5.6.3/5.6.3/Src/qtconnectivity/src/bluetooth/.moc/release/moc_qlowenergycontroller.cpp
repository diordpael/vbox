/****************************************************************************
** Meta object code from reading C++ file 'qlowenergycontroller.h'
**
** Created by: The Qt Meta Object Compiler version 67 (Qt 5.6.3)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "../../qlowenergycontroller.h"
#include <QtCore/qbytearray.h>
#include <QtCore/qmetatype.h>
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'qlowenergycontroller.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 67
#error "This file was generated using the moc from 5.6.3. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
struct qt_meta_stringdata_QLowEnergyController_t {
    QByteArrayData data[31];
    char stringdata0[473];
};
#define QT_MOC_LITERAL(idx, ofs, len) \
    Q_STATIC_BYTE_ARRAY_DATA_HEADER_INITIALIZER_WITH_OFFSET(len, \
    qptrdiff(offsetof(qt_meta_stringdata_QLowEnergyController_t, stringdata0) + ofs \
        - idx * sizeof(QByteArrayData)) \
    )
static const qt_meta_stringdata_QLowEnergyController_t qt_meta_stringdata_QLowEnergyController = {
    {
QT_MOC_LITERAL(0, 0, 20), // "QLowEnergyController"
QT_MOC_LITERAL(1, 21, 9), // "connected"
QT_MOC_LITERAL(2, 31, 0), // ""
QT_MOC_LITERAL(3, 32, 12), // "disconnected"
QT_MOC_LITERAL(4, 45, 12), // "stateChanged"
QT_MOC_LITERAL(5, 58, 37), // "QLowEnergyController::Control..."
QT_MOC_LITERAL(6, 96, 5), // "state"
QT_MOC_LITERAL(7, 102, 5), // "error"
QT_MOC_LITERAL(8, 108, 27), // "QLowEnergyController::Error"
QT_MOC_LITERAL(9, 136, 8), // "newError"
QT_MOC_LITERAL(10, 145, 17), // "serviceDiscovered"
QT_MOC_LITERAL(11, 163, 14), // "QBluetoothUuid"
QT_MOC_LITERAL(12, 178, 10), // "newService"
QT_MOC_LITERAL(13, 189, 17), // "discoveryFinished"
QT_MOC_LITERAL(14, 207, 5), // "Error"
QT_MOC_LITERAL(15, 213, 7), // "NoError"
QT_MOC_LITERAL(16, 221, 12), // "UnknownError"
QT_MOC_LITERAL(17, 234, 24), // "UnknownRemoteDeviceError"
QT_MOC_LITERAL(18, 259, 12), // "NetworkError"
QT_MOC_LITERAL(19, 272, 28), // "InvalidBluetoothAdapterError"
QT_MOC_LITERAL(20, 301, 15), // "ConnectionError"
QT_MOC_LITERAL(21, 317, 15), // "ControllerState"
QT_MOC_LITERAL(22, 333, 16), // "UnconnectedState"
QT_MOC_LITERAL(23, 350, 15), // "ConnectingState"
QT_MOC_LITERAL(24, 366, 14), // "ConnectedState"
QT_MOC_LITERAL(25, 381, 16), // "DiscoveringState"
QT_MOC_LITERAL(26, 398, 15), // "DiscoveredState"
QT_MOC_LITERAL(27, 414, 12), // "ClosingState"
QT_MOC_LITERAL(28, 427, 17), // "RemoteAddressType"
QT_MOC_LITERAL(29, 445, 13), // "PublicAddress"
QT_MOC_LITERAL(30, 459, 13) // "RandomAddress"

    },
    "QLowEnergyController\0connected\0\0"
    "disconnected\0stateChanged\0"
    "QLowEnergyController::ControllerState\0"
    "state\0error\0QLowEnergyController::Error\0"
    "newError\0serviceDiscovered\0QBluetoothUuid\0"
    "newService\0discoveryFinished\0Error\0"
    "NoError\0UnknownError\0UnknownRemoteDeviceError\0"
    "NetworkError\0InvalidBluetoothAdapterError\0"
    "ConnectionError\0ControllerState\0"
    "UnconnectedState\0ConnectingState\0"
    "ConnectedState\0DiscoveringState\0"
    "DiscoveredState\0ClosingState\0"
    "RemoteAddressType\0PublicAddress\0"
    "RandomAddress"
};
#undef QT_MOC_LITERAL

static const uint qt_meta_data_QLowEnergyController[] = {

 // content:
       7,       // revision
       0,       // classname
       0,    0, // classinfo
       6,   14, // methods
       0,    0, // properties
       3,   56, // enums/sets
       0,    0, // constructors
       0,       // flags
       6,       // signalCount

 // signals: name, argc, parameters, tag, flags
       1,    0,   44,    2, 0x06 /* Public */,
       3,    0,   45,    2, 0x06 /* Public */,
       4,    1,   46,    2, 0x06 /* Public */,
       7,    1,   49,    2, 0x06 /* Public */,
      10,    1,   52,    2, 0x06 /* Public */,
      13,    0,   55,    2, 0x06 /* Public */,

 // signals: parameters
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void, 0x80000000 | 5,    6,
    QMetaType::Void, 0x80000000 | 8,    9,
    QMetaType::Void, 0x80000000 | 11,   12,
    QMetaType::Void,

 // enums: name, flags, count, data
      14, 0x0,    6,   68,
      21, 0x0,    6,   80,
      28, 0x0,    2,   92,

 // enum data: key, value
      15, uint(QLowEnergyController::NoError),
      16, uint(QLowEnergyController::UnknownError),
      17, uint(QLowEnergyController::UnknownRemoteDeviceError),
      18, uint(QLowEnergyController::NetworkError),
      19, uint(QLowEnergyController::InvalidBluetoothAdapterError),
      20, uint(QLowEnergyController::ConnectionError),
      22, uint(QLowEnergyController::UnconnectedState),
      23, uint(QLowEnergyController::ConnectingState),
      24, uint(QLowEnergyController::ConnectedState),
      25, uint(QLowEnergyController::DiscoveringState),
      26, uint(QLowEnergyController::DiscoveredState),
      27, uint(QLowEnergyController::ClosingState),
      29, uint(QLowEnergyController::PublicAddress),
      30, uint(QLowEnergyController::RandomAddress),

       0        // eod
};

void QLowEnergyController::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        QLowEnergyController *_t = static_cast<QLowEnergyController *>(_o);
        Q_UNUSED(_t)
        switch (_id) {
        case 0: _t->connected(); break;
        case 1: _t->disconnected(); break;
        case 2: _t->stateChanged((*reinterpret_cast< QLowEnergyController::ControllerState(*)>(_a[1]))); break;
        case 3: _t->error((*reinterpret_cast< QLowEnergyController::Error(*)>(_a[1]))); break;
        case 4: _t->serviceDiscovered((*reinterpret_cast< const QBluetoothUuid(*)>(_a[1]))); break;
        case 5: _t->discoveryFinished(); break;
        default: ;
        }
    } else if (_c == QMetaObject::RegisterMethodArgumentMetaType) {
        switch (_id) {
        default: *reinterpret_cast<int*>(_a[0]) = -1; break;
        case 2:
            switch (*reinterpret_cast<int*>(_a[1])) {
            default: *reinterpret_cast<int*>(_a[0]) = -1; break;
            case 0:
                *reinterpret_cast<int*>(_a[0]) = qRegisterMetaType< QLowEnergyController::ControllerState >(); break;
            }
            break;
        case 3:
            switch (*reinterpret_cast<int*>(_a[1])) {
            default: *reinterpret_cast<int*>(_a[0]) = -1; break;
            case 0:
                *reinterpret_cast<int*>(_a[0]) = qRegisterMetaType< QLowEnergyController::Error >(); break;
            }
            break;
        case 4:
            switch (*reinterpret_cast<int*>(_a[1])) {
            default: *reinterpret_cast<int*>(_a[0]) = -1; break;
            case 0:
                *reinterpret_cast<int*>(_a[0]) = qRegisterMetaType< QBluetoothUuid >(); break;
            }
            break;
        }
    } else if (_c == QMetaObject::IndexOfMethod) {
        int *result = reinterpret_cast<int *>(_a[0]);
        void **func = reinterpret_cast<void **>(_a[1]);
        {
            typedef void (QLowEnergyController::*_t)();
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&QLowEnergyController::connected)) {
                *result = 0;
                return;
            }
        }
        {
            typedef void (QLowEnergyController::*_t)();
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&QLowEnergyController::disconnected)) {
                *result = 1;
                return;
            }
        }
        {
            typedef void (QLowEnergyController::*_t)(QLowEnergyController::ControllerState );
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&QLowEnergyController::stateChanged)) {
                *result = 2;
                return;
            }
        }
        {
            typedef void (QLowEnergyController::*_t)(QLowEnergyController::Error );
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&QLowEnergyController::error)) {
                *result = 3;
                return;
            }
        }
        {
            typedef void (QLowEnergyController::*_t)(const QBluetoothUuid & );
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&QLowEnergyController::serviceDiscovered)) {
                *result = 4;
                return;
            }
        }
        {
            typedef void (QLowEnergyController::*_t)();
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&QLowEnergyController::discoveryFinished)) {
                *result = 5;
                return;
            }
        }
    }
}

const QMetaObject QLowEnergyController::staticMetaObject = {
    { &QObject::staticMetaObject, qt_meta_stringdata_QLowEnergyController.data,
      qt_meta_data_QLowEnergyController,  qt_static_metacall, Q_NULLPTR, Q_NULLPTR}
};


const QMetaObject *QLowEnergyController::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->dynamicMetaObject() : &staticMetaObject;
}

void *QLowEnergyController::qt_metacast(const char *_clname)
{
    if (!_clname) return Q_NULLPTR;
    if (!strcmp(_clname, qt_meta_stringdata_QLowEnergyController.stringdata0))
        return static_cast<void*>(const_cast< QLowEnergyController*>(this));
    return QObject::qt_metacast(_clname);
}

int QLowEnergyController::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QObject::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 6)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 6;
    } else if (_c == QMetaObject::RegisterMethodArgumentMetaType) {
        if (_id < 6)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 6;
    }
    return _id;
}

// SIGNAL 0
void QLowEnergyController::connected()
{
    QMetaObject::activate(this, &staticMetaObject, 0, Q_NULLPTR);
}

// SIGNAL 1
void QLowEnergyController::disconnected()
{
    QMetaObject::activate(this, &staticMetaObject, 1, Q_NULLPTR);
}

// SIGNAL 2
void QLowEnergyController::stateChanged(QLowEnergyController::ControllerState _t1)
{
    void *_a[] = { Q_NULLPTR, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 2, _a);
}

// SIGNAL 3
void QLowEnergyController::error(QLowEnergyController::Error _t1)
{
    void *_a[] = { Q_NULLPTR, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 3, _a);
}

// SIGNAL 4
void QLowEnergyController::serviceDiscovered(const QBluetoothUuid & _t1)
{
    void *_a[] = { Q_NULLPTR, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 4, _a);
}

// SIGNAL 5
void QLowEnergyController::discoveryFinished()
{
    QMetaObject::activate(this, &staticMetaObject, 5, Q_NULLPTR);
}
QT_END_MOC_NAMESPACE
