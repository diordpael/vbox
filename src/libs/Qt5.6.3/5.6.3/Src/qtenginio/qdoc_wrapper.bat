@echo off
SetLocal EnableDelayedExpansion
(set QT_VERSION=1.6.3)
(set QT_VER=1.6)
(set QT_VERSION_TAG=163)
(set QT_INSTALL_DOCS=C:/Qt/Qt5.6.3/5.6.3/Src/qtbase/doc)
C:\Qt\Qt5.6.3\5.6.3\Src\qtbase\bin\qdoc.exe %*
EndLocal
