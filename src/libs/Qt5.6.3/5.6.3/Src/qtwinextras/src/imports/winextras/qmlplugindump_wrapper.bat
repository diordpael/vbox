@echo off
SetLocal EnableDelayedExpansion
(set QML2_IMPORT_PATH=C:\Qt\Qt5.6.3\5.6.3\Src\qtbase\qml;C:\Qt\Qt5.6.3\5.6.3\Src\qtdeclarative\qml;C:\Qt\Qt5.6.3\5.6.3\Src\qtcanvas3d\qml;C:\Qt\Qt5.6.3\5.6.3\Src\qt3d\qml;C:\Qt\Qt5.6.3\5.6.3\Src\qtconnectivity\qml;C:\Qt\Qt5.6.3\5.6.3\Src\qtwebsockets\qml;C:\Qt\Qt5.6.3\5.6.3\Src\qtsensors\qml;C:\Qt\Qt5.6.3\5.6.3\Src\qtmultimedia\qml;C:\Qt\Qt5.6.3\5.6.3\Src\qtgraphicaleffects\qml;C:\Qt\Qt5.6.3\5.6.3\Src\qtquickcontrols\qml;C:\Qt\Qt5.6.3\5.6.3\Src\qtwebchannel\qml;C:\Qt\Qt5.6.3\5.6.3\Src\qtlocation\qml)
(set PATH=C:\Qt\Qt5.6.3\5.6.3\Src\qtdeclarative\lib;C:\Qt\Qt5.6.3\5.6.3\Src\qtbase\lib;!PATH!)
if defined QT_PLUGIN_PATH (
    set QT_PLUGIN_PATH=C:\Qt\Qt5.6.3\5.6.3\Src\qtbase\plugins;C:\Qt\Qt5.6.3\5.6.3\Src\qtdeclarative\plugins;C:\Qt\Qt5.6.3\5.6.3\Src\qtsvg\plugins;C:\Qt\Qt5.6.3\5.6.3\Src\qtimageformats\plugins;!QT_PLUGIN_PATH!
) else (
    set QT_PLUGIN_PATH=C:\Qt\Qt5.6.3\5.6.3\Src\qtbase\plugins;C:\Qt\Qt5.6.3\5.6.3\Src\qtdeclarative\plugins;C:\Qt\Qt5.6.3\5.6.3\Src\qtsvg\plugins;C:\Qt\Qt5.6.3\5.6.3\Src\qtimageformats\plugins
)
C:\Qt\Qt5.6.3\5.6.3\Src\qtdeclarative\bin\qmlplugindump.exe %*
EndLocal
