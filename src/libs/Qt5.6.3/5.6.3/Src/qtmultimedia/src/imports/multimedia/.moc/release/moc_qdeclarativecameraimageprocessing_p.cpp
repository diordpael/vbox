/****************************************************************************
** Meta object code from reading C++ file 'qdeclarativecameraimageprocessing_p.h'
**
** Created by: The Qt Meta Object Compiler version 67 (Qt 5.6.3)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "../../qdeclarativecameraimageprocessing_p.h"
#include <QtCore/qbytearray.h>
#include <QtCore/qmetatype.h>
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'qdeclarativecameraimageprocessing_p.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 67
#error "This file was generated using the moc from 5.6.3. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
struct qt_meta_stringdata_QDeclarativeCameraImageProcessing_t {
    QByteArrayData data[49];
    char stringdata0[892];
};
#define QT_MOC_LITERAL(idx, ofs, len) \
    Q_STATIC_BYTE_ARRAY_DATA_HEADER_INITIALIZER_WITH_OFFSET(len, \
    qptrdiff(offsetof(qt_meta_stringdata_QDeclarativeCameraImageProcessing_t, stringdata0) + ofs \
        - idx * sizeof(QByteArrayData)) \
    )
static const qt_meta_stringdata_QDeclarativeCameraImageProcessing_t qt_meta_stringdata_QDeclarativeCameraImageProcessing = {
    {
QT_MOC_LITERAL(0, 0, 33), // "QDeclarativeCameraImageProces..."
QT_MOC_LITERAL(1, 34, 23), // "whiteBalanceModeChanged"
QT_MOC_LITERAL(2, 58, 0), // ""
QT_MOC_LITERAL(3, 59, 51), // "QDeclarativeCameraImageProces..."
QT_MOC_LITERAL(4, 111, 25), // "manualWhiteBalanceChanged"
QT_MOC_LITERAL(5, 137, 15), // "contrastChanged"
QT_MOC_LITERAL(6, 153, 17), // "saturationChanged"
QT_MOC_LITERAL(7, 171, 22), // "sharpeningLevelChanged"
QT_MOC_LITERAL(8, 194, 21), // "denoisingLevelChanged"
QT_MOC_LITERAL(9, 216, 18), // "colorFilterChanged"
QT_MOC_LITERAL(10, 235, 19), // "setWhiteBalanceMode"
QT_MOC_LITERAL(11, 255, 4), // "mode"
QT_MOC_LITERAL(12, 260, 21), // "setManualWhiteBalance"
QT_MOC_LITERAL(13, 282, 9), // "colorTemp"
QT_MOC_LITERAL(14, 292, 11), // "setContrast"
QT_MOC_LITERAL(15, 304, 5), // "value"
QT_MOC_LITERAL(16, 310, 13), // "setSaturation"
QT_MOC_LITERAL(17, 324, 18), // "setSharpeningLevel"
QT_MOC_LITERAL(18, 343, 17), // "setDenoisingLevel"
QT_MOC_LITERAL(19, 361, 14), // "setColorFilter"
QT_MOC_LITERAL(20, 376, 11), // "ColorFilter"
QT_MOC_LITERAL(21, 388, 11), // "colorFilter"
QT_MOC_LITERAL(22, 400, 16), // "whiteBalanceMode"
QT_MOC_LITERAL(23, 417, 16), // "WhiteBalanceMode"
QT_MOC_LITERAL(24, 434, 18), // "manualWhiteBalance"
QT_MOC_LITERAL(25, 453, 8), // "contrast"
QT_MOC_LITERAL(26, 462, 10), // "saturation"
QT_MOC_LITERAL(27, 473, 15), // "sharpeningLevel"
QT_MOC_LITERAL(28, 489, 14), // "denoisingLevel"
QT_MOC_LITERAL(29, 504, 16), // "WhiteBalanceAuto"
QT_MOC_LITERAL(30, 521, 18), // "WhiteBalanceManual"
QT_MOC_LITERAL(31, 540, 20), // "WhiteBalanceSunlight"
QT_MOC_LITERAL(32, 561, 18), // "WhiteBalanceCloudy"
QT_MOC_LITERAL(33, 580, 17), // "WhiteBalanceShade"
QT_MOC_LITERAL(34, 598, 20), // "WhiteBalanceTungsten"
QT_MOC_LITERAL(35, 619, 23), // "WhiteBalanceFluorescent"
QT_MOC_LITERAL(36, 643, 17), // "WhiteBalanceFlash"
QT_MOC_LITERAL(37, 661, 18), // "WhiteBalanceSunset"
QT_MOC_LITERAL(38, 680, 18), // "WhiteBalanceVendor"
QT_MOC_LITERAL(39, 699, 15), // "ColorFilterNone"
QT_MOC_LITERAL(40, 715, 20), // "ColorFilterGrayscale"
QT_MOC_LITERAL(41, 736, 19), // "ColorFilterNegative"
QT_MOC_LITERAL(42, 756, 19), // "ColorFilterSolarize"
QT_MOC_LITERAL(43, 776, 16), // "ColorFilterSepia"
QT_MOC_LITERAL(44, 793, 20), // "ColorFilterPosterize"
QT_MOC_LITERAL(45, 814, 21), // "ColorFilterWhiteboard"
QT_MOC_LITERAL(46, 836, 21), // "ColorFilterBlackboard"
QT_MOC_LITERAL(47, 858, 15), // "ColorFilterAqua"
QT_MOC_LITERAL(48, 874, 17) // "ColorFilterVendor"

    },
    "QDeclarativeCameraImageProcessing\0"
    "whiteBalanceModeChanged\0\0"
    "QDeclarativeCameraImageProcessing::WhiteBalanceMode\0"
    "manualWhiteBalanceChanged\0contrastChanged\0"
    "saturationChanged\0sharpeningLevelChanged\0"
    "denoisingLevelChanged\0colorFilterChanged\0"
    "setWhiteBalanceMode\0mode\0setManualWhiteBalance\0"
    "colorTemp\0setContrast\0value\0setSaturation\0"
    "setSharpeningLevel\0setDenoisingLevel\0"
    "setColorFilter\0ColorFilter\0colorFilter\0"
    "whiteBalanceMode\0WhiteBalanceMode\0"
    "manualWhiteBalance\0contrast\0saturation\0"
    "sharpeningLevel\0denoisingLevel\0"
    "WhiteBalanceAuto\0WhiteBalanceManual\0"
    "WhiteBalanceSunlight\0WhiteBalanceCloudy\0"
    "WhiteBalanceShade\0WhiteBalanceTungsten\0"
    "WhiteBalanceFluorescent\0WhiteBalanceFlash\0"
    "WhiteBalanceSunset\0WhiteBalanceVendor\0"
    "ColorFilterNone\0ColorFilterGrayscale\0"
    "ColorFilterNegative\0ColorFilterSolarize\0"
    "ColorFilterSepia\0ColorFilterPosterize\0"
    "ColorFilterWhiteboard\0ColorFilterBlackboard\0"
    "ColorFilterAqua\0ColorFilterVendor"
};
#undef QT_MOC_LITERAL

static const uint qt_meta_data_QDeclarativeCameraImageProcessing[] = {

 // content:
       7,       // revision
       0,       // classname
       0,    0, // classinfo
      14,   14, // methods
       7,  124, // properties
       2,  159, // enums/sets
       0,    0, // constructors
       0,       // flags
       7,       // signalCount

 // signals: name, argc, parameters, tag, flags
       1,    1,   84,    2, 0x06 /* Public */,
       4,    1,   87,    2, 0x06 /* Public */,
       5,    1,   90,    2, 0x06 /* Public */,
       6,    1,   93,    2, 0x06 /* Public */,
       7,    1,   96,    2, 0x06 /* Public */,
       8,    1,   99,    2, 0x06 /* Public */,
       9,    0,  102,    2, 0x06 /* Public */,

 // slots: name, argc, parameters, tag, flags
      10,    1,  103,    2, 0x0a /* Public */,
      12,    1,  106,    2, 0x0a /* Public */,
      14,    1,  109,    2, 0x0a /* Public */,
      16,    1,  112,    2, 0x0a /* Public */,
      17,    1,  115,    2, 0x0a /* Public */,
      18,    1,  118,    2, 0x0a /* Public */,
      19,    1,  121,    2, 0x0a /* Public */,

 // signals: parameters
    QMetaType::Void, 0x80000000 | 3,    2,
    QMetaType::Void, QMetaType::QReal,    2,
    QMetaType::Void, QMetaType::QReal,    2,
    QMetaType::Void, QMetaType::QReal,    2,
    QMetaType::Void, QMetaType::QReal,    2,
    QMetaType::Void, QMetaType::QReal,    2,
    QMetaType::Void,

 // slots: parameters
    QMetaType::Void, 0x80000000 | 3,   11,
    QMetaType::Void, QMetaType::QReal,   13,
    QMetaType::Void, QMetaType::QReal,   15,
    QMetaType::Void, QMetaType::QReal,   15,
    QMetaType::Void, QMetaType::QReal,   15,
    QMetaType::Void, QMetaType::QReal,   15,
    QMetaType::Void, 0x80000000 | 20,   21,

 // properties: name, type, flags
      22, 0x80000000 | 23, 0x0049510b,
      24, QMetaType::QReal, 0x00495103,
      25, QMetaType::QReal, 0x00495103,
      26, QMetaType::QReal, 0x00495103,
      27, QMetaType::QReal, 0x00495103,
      28, QMetaType::QReal, 0x00495103,
      21, 0x80000000 | 20, 0x00c9510b,

 // properties: notify_signal_id
       0,
       1,
       2,
       3,
       4,
       5,
       6,

 // properties: revision
       0,
       0,
       0,
       0,
       0,
       0,
       1,

 // enums: name, flags, count, data
      23, 0x0,   10,  167,
      20, 0x0,   10,  187,

 // enum data: key, value
      29, uint(QDeclarativeCameraImageProcessing::WhiteBalanceAuto),
      30, uint(QDeclarativeCameraImageProcessing::WhiteBalanceManual),
      31, uint(QDeclarativeCameraImageProcessing::WhiteBalanceSunlight),
      32, uint(QDeclarativeCameraImageProcessing::WhiteBalanceCloudy),
      33, uint(QDeclarativeCameraImageProcessing::WhiteBalanceShade),
      34, uint(QDeclarativeCameraImageProcessing::WhiteBalanceTungsten),
      35, uint(QDeclarativeCameraImageProcessing::WhiteBalanceFluorescent),
      36, uint(QDeclarativeCameraImageProcessing::WhiteBalanceFlash),
      37, uint(QDeclarativeCameraImageProcessing::WhiteBalanceSunset),
      38, uint(QDeclarativeCameraImageProcessing::WhiteBalanceVendor),
      39, uint(QDeclarativeCameraImageProcessing::ColorFilterNone),
      40, uint(QDeclarativeCameraImageProcessing::ColorFilterGrayscale),
      41, uint(QDeclarativeCameraImageProcessing::ColorFilterNegative),
      42, uint(QDeclarativeCameraImageProcessing::ColorFilterSolarize),
      43, uint(QDeclarativeCameraImageProcessing::ColorFilterSepia),
      44, uint(QDeclarativeCameraImageProcessing::ColorFilterPosterize),
      45, uint(QDeclarativeCameraImageProcessing::ColorFilterWhiteboard),
      46, uint(QDeclarativeCameraImageProcessing::ColorFilterBlackboard),
      47, uint(QDeclarativeCameraImageProcessing::ColorFilterAqua),
      48, uint(QDeclarativeCameraImageProcessing::ColorFilterVendor),

       0        // eod
};

void QDeclarativeCameraImageProcessing::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        QDeclarativeCameraImageProcessing *_t = static_cast<QDeclarativeCameraImageProcessing *>(_o);
        Q_UNUSED(_t)
        switch (_id) {
        case 0: _t->whiteBalanceModeChanged((*reinterpret_cast< QDeclarativeCameraImageProcessing::WhiteBalanceMode(*)>(_a[1]))); break;
        case 1: _t->manualWhiteBalanceChanged((*reinterpret_cast< qreal(*)>(_a[1]))); break;
        case 2: _t->contrastChanged((*reinterpret_cast< qreal(*)>(_a[1]))); break;
        case 3: _t->saturationChanged((*reinterpret_cast< qreal(*)>(_a[1]))); break;
        case 4: _t->sharpeningLevelChanged((*reinterpret_cast< qreal(*)>(_a[1]))); break;
        case 5: _t->denoisingLevelChanged((*reinterpret_cast< qreal(*)>(_a[1]))); break;
        case 6: _t->colorFilterChanged(); break;
        case 7: _t->setWhiteBalanceMode((*reinterpret_cast< QDeclarativeCameraImageProcessing::WhiteBalanceMode(*)>(_a[1]))); break;
        case 8: _t->setManualWhiteBalance((*reinterpret_cast< qreal(*)>(_a[1]))); break;
        case 9: _t->setContrast((*reinterpret_cast< qreal(*)>(_a[1]))); break;
        case 10: _t->setSaturation((*reinterpret_cast< qreal(*)>(_a[1]))); break;
        case 11: _t->setSharpeningLevel((*reinterpret_cast< qreal(*)>(_a[1]))); break;
        case 12: _t->setDenoisingLevel((*reinterpret_cast< qreal(*)>(_a[1]))); break;
        case 13: _t->setColorFilter((*reinterpret_cast< ColorFilter(*)>(_a[1]))); break;
        default: ;
        }
    } else if (_c == QMetaObject::IndexOfMethod) {
        int *result = reinterpret_cast<int *>(_a[0]);
        void **func = reinterpret_cast<void **>(_a[1]);
        {
            typedef void (QDeclarativeCameraImageProcessing::*_t)(QDeclarativeCameraImageProcessing::WhiteBalanceMode ) const;
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&QDeclarativeCameraImageProcessing::whiteBalanceModeChanged)) {
                *result = 0;
                return;
            }
        }
        {
            typedef void (QDeclarativeCameraImageProcessing::*_t)(qreal ) const;
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&QDeclarativeCameraImageProcessing::manualWhiteBalanceChanged)) {
                *result = 1;
                return;
            }
        }
        {
            typedef void (QDeclarativeCameraImageProcessing::*_t)(qreal );
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&QDeclarativeCameraImageProcessing::contrastChanged)) {
                *result = 2;
                return;
            }
        }
        {
            typedef void (QDeclarativeCameraImageProcessing::*_t)(qreal );
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&QDeclarativeCameraImageProcessing::saturationChanged)) {
                *result = 3;
                return;
            }
        }
        {
            typedef void (QDeclarativeCameraImageProcessing::*_t)(qreal );
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&QDeclarativeCameraImageProcessing::sharpeningLevelChanged)) {
                *result = 4;
                return;
            }
        }
        {
            typedef void (QDeclarativeCameraImageProcessing::*_t)(qreal );
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&QDeclarativeCameraImageProcessing::denoisingLevelChanged)) {
                *result = 5;
                return;
            }
        }
        {
            typedef void (QDeclarativeCameraImageProcessing::*_t)();
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&QDeclarativeCameraImageProcessing::colorFilterChanged)) {
                *result = 6;
                return;
            }
        }
    }
#ifndef QT_NO_PROPERTIES
    else if (_c == QMetaObject::ReadProperty) {
        QDeclarativeCameraImageProcessing *_t = static_cast<QDeclarativeCameraImageProcessing *>(_o);
        Q_UNUSED(_t)
        void *_v = _a[0];
        switch (_id) {
        case 0: *reinterpret_cast< WhiteBalanceMode*>(_v) = _t->whiteBalanceMode(); break;
        case 1: *reinterpret_cast< qreal*>(_v) = _t->manualWhiteBalance(); break;
        case 2: *reinterpret_cast< qreal*>(_v) = _t->contrast(); break;
        case 3: *reinterpret_cast< qreal*>(_v) = _t->saturation(); break;
        case 4: *reinterpret_cast< qreal*>(_v) = _t->sharpeningLevel(); break;
        case 5: *reinterpret_cast< qreal*>(_v) = _t->denoisingLevel(); break;
        case 6: *reinterpret_cast< ColorFilter*>(_v) = _t->colorFilter(); break;
        default: break;
        }
    } else if (_c == QMetaObject::WriteProperty) {
        QDeclarativeCameraImageProcessing *_t = static_cast<QDeclarativeCameraImageProcessing *>(_o);
        Q_UNUSED(_t)
        void *_v = _a[0];
        switch (_id) {
        case 0: _t->setWhiteBalanceMode(*reinterpret_cast< WhiteBalanceMode*>(_v)); break;
        case 1: _t->setManualWhiteBalance(*reinterpret_cast< qreal*>(_v)); break;
        case 2: _t->setContrast(*reinterpret_cast< qreal*>(_v)); break;
        case 3: _t->setSaturation(*reinterpret_cast< qreal*>(_v)); break;
        case 4: _t->setSharpeningLevel(*reinterpret_cast< qreal*>(_v)); break;
        case 5: _t->setDenoisingLevel(*reinterpret_cast< qreal*>(_v)); break;
        case 6: _t->setColorFilter(*reinterpret_cast< ColorFilter*>(_v)); break;
        default: break;
        }
    } else if (_c == QMetaObject::ResetProperty) {
    }
#endif // QT_NO_PROPERTIES
}

const QMetaObject QDeclarativeCameraImageProcessing::staticMetaObject = {
    { &QObject::staticMetaObject, qt_meta_stringdata_QDeclarativeCameraImageProcessing.data,
      qt_meta_data_QDeclarativeCameraImageProcessing,  qt_static_metacall, Q_NULLPTR, Q_NULLPTR}
};


const QMetaObject *QDeclarativeCameraImageProcessing::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->dynamicMetaObject() : &staticMetaObject;
}

void *QDeclarativeCameraImageProcessing::qt_metacast(const char *_clname)
{
    if (!_clname) return Q_NULLPTR;
    if (!strcmp(_clname, qt_meta_stringdata_QDeclarativeCameraImageProcessing.stringdata0))
        return static_cast<void*>(const_cast< QDeclarativeCameraImageProcessing*>(this));
    return QObject::qt_metacast(_clname);
}

int QDeclarativeCameraImageProcessing::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QObject::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 14)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 14;
    } else if (_c == QMetaObject::RegisterMethodArgumentMetaType) {
        if (_id < 14)
            *reinterpret_cast<int*>(_a[0]) = -1;
        _id -= 14;
    }
#ifndef QT_NO_PROPERTIES
   else if (_c == QMetaObject::ReadProperty || _c == QMetaObject::WriteProperty
            || _c == QMetaObject::ResetProperty || _c == QMetaObject::RegisterPropertyMetaType) {
        qt_static_metacall(this, _c, _id, _a);
        _id -= 7;
    } else if (_c == QMetaObject::QueryPropertyDesignable) {
        _id -= 7;
    } else if (_c == QMetaObject::QueryPropertyScriptable) {
        _id -= 7;
    } else if (_c == QMetaObject::QueryPropertyStored) {
        _id -= 7;
    } else if (_c == QMetaObject::QueryPropertyEditable) {
        _id -= 7;
    } else if (_c == QMetaObject::QueryPropertyUser) {
        _id -= 7;
    }
#endif // QT_NO_PROPERTIES
    return _id;
}

// SIGNAL 0
void QDeclarativeCameraImageProcessing::whiteBalanceModeChanged(QDeclarativeCameraImageProcessing::WhiteBalanceMode _t1)const
{
    void *_a[] = { Q_NULLPTR, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(const_cast< QDeclarativeCameraImageProcessing *>(this), &staticMetaObject, 0, _a);
}

// SIGNAL 1
void QDeclarativeCameraImageProcessing::manualWhiteBalanceChanged(qreal _t1)const
{
    void *_a[] = { Q_NULLPTR, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(const_cast< QDeclarativeCameraImageProcessing *>(this), &staticMetaObject, 1, _a);
}

// SIGNAL 2
void QDeclarativeCameraImageProcessing::contrastChanged(qreal _t1)
{
    void *_a[] = { Q_NULLPTR, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 2, _a);
}

// SIGNAL 3
void QDeclarativeCameraImageProcessing::saturationChanged(qreal _t1)
{
    void *_a[] = { Q_NULLPTR, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 3, _a);
}

// SIGNAL 4
void QDeclarativeCameraImageProcessing::sharpeningLevelChanged(qreal _t1)
{
    void *_a[] = { Q_NULLPTR, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 4, _a);
}

// SIGNAL 5
void QDeclarativeCameraImageProcessing::denoisingLevelChanged(qreal _t1)
{
    void *_a[] = { Q_NULLPTR, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 5, _a);
}

// SIGNAL 6
void QDeclarativeCameraImageProcessing::colorFilterChanged()
{
    QMetaObject::activate(this, &staticMetaObject, 6, Q_NULLPTR);
}
QT_END_MOC_NAMESPACE
