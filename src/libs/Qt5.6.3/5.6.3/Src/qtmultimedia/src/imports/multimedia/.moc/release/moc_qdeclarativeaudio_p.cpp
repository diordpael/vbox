/****************************************************************************
** Meta object code from reading C++ file 'qdeclarativeaudio_p.h'
**
** Created by: The Qt Meta Object Compiler version 67 (Qt 5.6.3)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "../../qdeclarativeaudio_p.h"
#include <QtCore/qbytearray.h>
#include <QtCore/qmetatype.h>
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'qdeclarativeaudio_p.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 67
#error "This file was generated using the moc from 5.6.3. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
struct qt_meta_stringdata_QDeclarativeAudio_t {
    QByteArrayData data[102];
    char stringdata0[1285];
};
#define QT_MOC_LITERAL(idx, ofs, len) \
    Q_STATIC_BYTE_ARRAY_DATA_HEADER_INITIALIZER_WITH_OFFSET(len, \
    qptrdiff(offsetof(qt_meta_stringdata_QDeclarativeAudio_t, stringdata0) + ofs \
        - idx * sizeof(QByteArrayData)) \
    )
static const qt_meta_stringdata_QDeclarativeAudio_t qt_meta_stringdata_QDeclarativeAudio = {
    {
QT_MOC_LITERAL(0, 0, 17), // "QDeclarativeAudio"
QT_MOC_LITERAL(1, 18, 15), // "playlistChanged"
QT_MOC_LITERAL(2, 34, 0), // ""
QT_MOC_LITERAL(3, 35, 13), // "sourceChanged"
QT_MOC_LITERAL(4, 49, 15), // "autoLoadChanged"
QT_MOC_LITERAL(5, 65, 16), // "loopCountChanged"
QT_MOC_LITERAL(6, 82, 20), // "playbackStateChanged"
QT_MOC_LITERAL(7, 103, 15), // "autoPlayChanged"
QT_MOC_LITERAL(8, 119, 6), // "paused"
QT_MOC_LITERAL(9, 126, 7), // "stopped"
QT_MOC_LITERAL(10, 134, 7), // "playing"
QT_MOC_LITERAL(11, 142, 13), // "statusChanged"
QT_MOC_LITERAL(12, 156, 15), // "durationChanged"
QT_MOC_LITERAL(13, 172, 15), // "positionChanged"
QT_MOC_LITERAL(14, 188, 13), // "volumeChanged"
QT_MOC_LITERAL(15, 202, 12), // "mutedChanged"
QT_MOC_LITERAL(16, 215, 15), // "hasAudioChanged"
QT_MOC_LITERAL(17, 231, 15), // "hasVideoChanged"
QT_MOC_LITERAL(18, 247, 21), // "bufferProgressChanged"
QT_MOC_LITERAL(19, 269, 15), // "seekableChanged"
QT_MOC_LITERAL(20, 285, 19), // "playbackRateChanged"
QT_MOC_LITERAL(21, 305, 16), // "audioRoleChanged"
QT_MOC_LITERAL(22, 322, 19), // "availabilityChanged"
QT_MOC_LITERAL(23, 342, 12), // "Availability"
QT_MOC_LITERAL(24, 355, 12), // "availability"
QT_MOC_LITERAL(25, 368, 12), // "errorChanged"
QT_MOC_LITERAL(26, 381, 5), // "error"
QT_MOC_LITERAL(27, 387, 24), // "QDeclarativeAudio::Error"
QT_MOC_LITERAL(28, 412, 11), // "errorString"
QT_MOC_LITERAL(29, 424, 18), // "mediaObjectChanged"
QT_MOC_LITERAL(30, 443, 4), // "play"
QT_MOC_LITERAL(31, 448, 5), // "pause"
QT_MOC_LITERAL(32, 454, 4), // "stop"
QT_MOC_LITERAL(33, 459, 4), // "seek"
QT_MOC_LITERAL(34, 464, 8), // "position"
QT_MOC_LITERAL(35, 473, 19), // "supportedAudioRoles"
QT_MOC_LITERAL(36, 493, 8), // "QJSValue"
QT_MOC_LITERAL(37, 502, 8), // "_q_error"
QT_MOC_LITERAL(38, 511, 19), // "QMediaPlayer::Error"
QT_MOC_LITERAL(39, 531, 22), // "_q_availabilityChanged"
QT_MOC_LITERAL(40, 554, 31), // "QMultimedia::AvailabilityStatus"
QT_MOC_LITERAL(41, 586, 16), // "_q_statusChanged"
QT_MOC_LITERAL(42, 603, 15), // "_q_mediaChanged"
QT_MOC_LITERAL(43, 619, 13), // "QMediaContent"
QT_MOC_LITERAL(44, 633, 6), // "source"
QT_MOC_LITERAL(45, 640, 8), // "playlist"
QT_MOC_LITERAL(46, 649, 21), // "QDeclarativePlaylist*"
QT_MOC_LITERAL(47, 671, 5), // "loops"
QT_MOC_LITERAL(48, 677, 13), // "playbackState"
QT_MOC_LITERAL(49, 691, 13), // "PlaybackState"
QT_MOC_LITERAL(50, 705, 8), // "autoPlay"
QT_MOC_LITERAL(51, 714, 8), // "autoLoad"
QT_MOC_LITERAL(52, 723, 6), // "status"
QT_MOC_LITERAL(53, 730, 6), // "Status"
QT_MOC_LITERAL(54, 737, 8), // "duration"
QT_MOC_LITERAL(55, 746, 6), // "volume"
QT_MOC_LITERAL(56, 753, 5), // "muted"
QT_MOC_LITERAL(57, 759, 8), // "hasAudio"
QT_MOC_LITERAL(58, 768, 8), // "hasVideo"
QT_MOC_LITERAL(59, 777, 14), // "bufferProgress"
QT_MOC_LITERAL(60, 792, 8), // "seekable"
QT_MOC_LITERAL(61, 801, 12), // "playbackRate"
QT_MOC_LITERAL(62, 814, 5), // "Error"
QT_MOC_LITERAL(63, 820, 8), // "metaData"
QT_MOC_LITERAL(64, 829, 26), // "QDeclarativeMediaMetaData*"
QT_MOC_LITERAL(65, 856, 11), // "mediaObject"
QT_MOC_LITERAL(66, 868, 9), // "audioRole"
QT_MOC_LITERAL(67, 878, 9), // "AudioRole"
QT_MOC_LITERAL(68, 888, 13), // "UnknownStatus"
QT_MOC_LITERAL(69, 902, 7), // "NoMedia"
QT_MOC_LITERAL(70, 910, 7), // "Loading"
QT_MOC_LITERAL(71, 918, 6), // "Loaded"
QT_MOC_LITERAL(72, 925, 7), // "Stalled"
QT_MOC_LITERAL(73, 933, 9), // "Buffering"
QT_MOC_LITERAL(74, 943, 8), // "Buffered"
QT_MOC_LITERAL(75, 952, 10), // "EndOfMedia"
QT_MOC_LITERAL(76, 963, 12), // "InvalidMedia"
QT_MOC_LITERAL(77, 976, 7), // "NoError"
QT_MOC_LITERAL(78, 984, 13), // "ResourceError"
QT_MOC_LITERAL(79, 998, 11), // "FormatError"
QT_MOC_LITERAL(80, 1010, 12), // "NetworkError"
QT_MOC_LITERAL(81, 1023, 12), // "AccessDenied"
QT_MOC_LITERAL(82, 1036, 14), // "ServiceMissing"
QT_MOC_LITERAL(83, 1051, 4), // "Loop"
QT_MOC_LITERAL(84, 1056, 8), // "Infinite"
QT_MOC_LITERAL(85, 1065, 12), // "PlayingState"
QT_MOC_LITERAL(86, 1078, 11), // "PausedState"
QT_MOC_LITERAL(87, 1090, 12), // "StoppedState"
QT_MOC_LITERAL(88, 1103, 9), // "Available"
QT_MOC_LITERAL(89, 1113, 4), // "Busy"
QT_MOC_LITERAL(90, 1118, 11), // "Unavailable"
QT_MOC_LITERAL(91, 1130, 15), // "ResourceMissing"
QT_MOC_LITERAL(92, 1146, 11), // "UnknownRole"
QT_MOC_LITERAL(93, 1158, 17), // "AccessibilityRole"
QT_MOC_LITERAL(94, 1176, 9), // "AlarmRole"
QT_MOC_LITERAL(95, 1186, 8), // "GameRole"
QT_MOC_LITERAL(96, 1195, 9), // "MusicRole"
QT_MOC_LITERAL(97, 1205, 16), // "NotificationRole"
QT_MOC_LITERAL(98, 1222, 12), // "RingtoneRole"
QT_MOC_LITERAL(99, 1235, 16), // "SonificationRole"
QT_MOC_LITERAL(100, 1252, 9), // "VideoRole"
QT_MOC_LITERAL(101, 1262, 22) // "VoiceCommunicationRole"

    },
    "QDeclarativeAudio\0playlistChanged\0\0"
    "sourceChanged\0autoLoadChanged\0"
    "loopCountChanged\0playbackStateChanged\0"
    "autoPlayChanged\0paused\0stopped\0playing\0"
    "statusChanged\0durationChanged\0"
    "positionChanged\0volumeChanged\0"
    "mutedChanged\0hasAudioChanged\0"
    "hasVideoChanged\0bufferProgressChanged\0"
    "seekableChanged\0playbackRateChanged\0"
    "audioRoleChanged\0availabilityChanged\0"
    "Availability\0availability\0errorChanged\0"
    "error\0QDeclarativeAudio::Error\0"
    "errorString\0mediaObjectChanged\0play\0"
    "pause\0stop\0seek\0position\0supportedAudioRoles\0"
    "QJSValue\0_q_error\0QMediaPlayer::Error\0"
    "_q_availabilityChanged\0"
    "QMultimedia::AvailabilityStatus\0"
    "_q_statusChanged\0_q_mediaChanged\0"
    "QMediaContent\0source\0playlist\0"
    "QDeclarativePlaylist*\0loops\0playbackState\0"
    "PlaybackState\0autoPlay\0autoLoad\0status\0"
    "Status\0duration\0volume\0muted\0hasAudio\0"
    "hasVideo\0bufferProgress\0seekable\0"
    "playbackRate\0Error\0metaData\0"
    "QDeclarativeMediaMetaData*\0mediaObject\0"
    "audioRole\0AudioRole\0UnknownStatus\0"
    "NoMedia\0Loading\0Loaded\0Stalled\0Buffering\0"
    "Buffered\0EndOfMedia\0InvalidMedia\0"
    "NoError\0ResourceError\0FormatError\0"
    "NetworkError\0AccessDenied\0ServiceMissing\0"
    "Loop\0Infinite\0PlayingState\0PausedState\0"
    "StoppedState\0Available\0Busy\0Unavailable\0"
    "ResourceMissing\0UnknownRole\0"
    "AccessibilityRole\0AlarmRole\0GameRole\0"
    "MusicRole\0NotificationRole\0RingtoneRole\0"
    "SonificationRole\0VideoRole\0"
    "VoiceCommunicationRole"
};
#undef QT_MOC_LITERAL

static const uint qt_meta_data_QDeclarativeAudio[] = {

 // content:
       7,       // revision
       0,       // classname
       0,    0, // classinfo
      33,   14, // methods
      22,  259, // properties
       6,  369, // enums/sets
       0,    0, // constructors
       0,       // flags
      24,       // signalCount

 // signals: name, argc, parameters, tag, flags
       1,    0,  212,    2, 0x86 /* Public | MethodRevisioned */,
       3,    0,  213,    2, 0x06 /* Public */,
       4,    0,  214,    2, 0x06 /* Public */,
       5,    0,  215,    2, 0x06 /* Public */,
       6,    0,  216,    2, 0x06 /* Public */,
       7,    0,  217,    2, 0x06 /* Public */,
       8,    0,  218,    2, 0x06 /* Public */,
       9,    0,  219,    2, 0x06 /* Public */,
      10,    0,  220,    2, 0x06 /* Public */,
      11,    0,  221,    2, 0x06 /* Public */,
      12,    0,  222,    2, 0x06 /* Public */,
      13,    0,  223,    2, 0x06 /* Public */,
      14,    0,  224,    2, 0x06 /* Public */,
      15,    0,  225,    2, 0x06 /* Public */,
      16,    0,  226,    2, 0x06 /* Public */,
      17,    0,  227,    2, 0x06 /* Public */,
      18,    0,  228,    2, 0x06 /* Public */,
      19,    0,  229,    2, 0x06 /* Public */,
      20,    0,  230,    2, 0x06 /* Public */,
      21,    0,  231,    2, 0x86 /* Public | MethodRevisioned */,
      22,    1,  232,    2, 0x06 /* Public */,
      25,    0,  235,    2, 0x06 /* Public */,
      26,    2,  236,    2, 0x06 /* Public */,
      29,    0,  241,    2, 0x06 /* Public */,

 // slots: name, argc, parameters, tag, flags
      30,    0,  242,    2, 0x0a /* Public */,
      31,    0,  243,    2, 0x0a /* Public */,
      32,    0,  244,    2, 0x0a /* Public */,
      33,    1,  245,    2, 0x0a /* Public */,
      35,    0,  248,    2, 0x8a /* Public | MethodRevisioned */,
      37,    1,  249,    2, 0x08 /* Private */,
      39,    1,  252,    2, 0x08 /* Private */,
      41,    0,  255,    2, 0x08 /* Private */,
      42,    1,  256,    2, 0x08 /* Private */,

 // signals: revision
       1,
       0,
       0,
       0,
       0,
       0,
       0,
       0,
       0,
       0,
       0,
       0,
       0,
       0,
       0,
       0,
       0,
       0,
       0,
       1,
       0,
       0,
       0,
       0,

 // slots: revision
       0,
       0,
       0,
       0,
       1,
       0,
       0,
       0,
       0,

 // signals: parameters
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void, 0x80000000 | 23,   24,
    QMetaType::Void,
    QMetaType::Void, 0x80000000 | 27, QMetaType::QString,   26,   28,
    QMetaType::Void,

 // slots: parameters
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void, QMetaType::Int,   34,
    0x80000000 | 36,
    QMetaType::Void, 0x80000000 | 38,    2,
    QMetaType::Void, 0x80000000 | 40,    2,
    QMetaType::Void,
    QMetaType::Void, 0x80000000 | 43,    2,

 // properties: name, type, flags
      44, QMetaType::QUrl, 0x00495103,
      45, 0x80000000 | 46, 0x00c9510b,
      47, QMetaType::Int, 0x00495003,
      48, 0x80000000 | 49, 0x00495009,
      50, QMetaType::Bool, 0x00495103,
      51, QMetaType::Bool, 0x00495103,
      52, 0x80000000 | 53, 0x00495009,
      54, QMetaType::Int, 0x00495001,
      34, QMetaType::Int, 0x00495001,
      55, QMetaType::QReal, 0x00495103,
      56, QMetaType::Bool, 0x00495103,
      57, QMetaType::Bool, 0x00495001,
      58, QMetaType::Bool, 0x00495001,
      59, QMetaType::QReal, 0x00495001,
      60, QMetaType::Bool, 0x00495001,
      61, QMetaType::QReal, 0x00495103,
      26, 0x80000000 | 62, 0x00495009,
      28, QMetaType::QString, 0x00495001,
      63, 0x80000000 | 64, 0x00095409,
      65, QMetaType::QObjectStar, 0x00490001,
      24, 0x80000000 | 23, 0x00495009,
      66, 0x80000000 | 67, 0x00c9510b,

 // properties: notify_signal_id
       1,
       0,
       3,
       4,
       5,
       2,
       9,
      10,
      11,
      12,
      13,
      14,
      15,
      16,
      17,
      18,
      21,
      21,
       0,
      23,
      20,
      19,

 // properties: revision
       0,
       1,
       0,
       0,
       0,
       0,
       0,
       0,
       0,
       0,
       0,
       0,
       0,
       0,
       0,
       0,
       0,
       0,
       0,
       0,
       0,
       1,

 // enums: name, flags, count, data
      53, 0x0,    9,  393,
      62, 0x0,    6,  411,
      83, 0x0,    1,  423,
      49, 0x0,    3,  425,
      23, 0x0,    4,  431,
      67, 0x0,   10,  439,

 // enum data: key, value
      68, uint(QDeclarativeAudio::UnknownStatus),
      69, uint(QDeclarativeAudio::NoMedia),
      70, uint(QDeclarativeAudio::Loading),
      71, uint(QDeclarativeAudio::Loaded),
      72, uint(QDeclarativeAudio::Stalled),
      73, uint(QDeclarativeAudio::Buffering),
      74, uint(QDeclarativeAudio::Buffered),
      75, uint(QDeclarativeAudio::EndOfMedia),
      76, uint(QDeclarativeAudio::InvalidMedia),
      77, uint(QDeclarativeAudio::NoError),
      78, uint(QDeclarativeAudio::ResourceError),
      79, uint(QDeclarativeAudio::FormatError),
      80, uint(QDeclarativeAudio::NetworkError),
      81, uint(QDeclarativeAudio::AccessDenied),
      82, uint(QDeclarativeAudio::ServiceMissing),
      84, uint(QDeclarativeAudio::Infinite),
      85, uint(QDeclarativeAudio::PlayingState),
      86, uint(QDeclarativeAudio::PausedState),
      87, uint(QDeclarativeAudio::StoppedState),
      88, uint(QDeclarativeAudio::Available),
      89, uint(QDeclarativeAudio::Busy),
      90, uint(QDeclarativeAudio::Unavailable),
      91, uint(QDeclarativeAudio::ResourceMissing),
      92, uint(QDeclarativeAudio::UnknownRole),
      93, uint(QDeclarativeAudio::AccessibilityRole),
      94, uint(QDeclarativeAudio::AlarmRole),
      95, uint(QDeclarativeAudio::GameRole),
      96, uint(QDeclarativeAudio::MusicRole),
      97, uint(QDeclarativeAudio::NotificationRole),
      98, uint(QDeclarativeAudio::RingtoneRole),
      99, uint(QDeclarativeAudio::SonificationRole),
     100, uint(QDeclarativeAudio::VideoRole),
     101, uint(QDeclarativeAudio::VoiceCommunicationRole),

       0        // eod
};

void QDeclarativeAudio::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        QDeclarativeAudio *_t = static_cast<QDeclarativeAudio *>(_o);
        Q_UNUSED(_t)
        switch (_id) {
        case 0: _t->playlistChanged(); break;
        case 1: _t->sourceChanged(); break;
        case 2: _t->autoLoadChanged(); break;
        case 3: _t->loopCountChanged(); break;
        case 4: _t->playbackStateChanged(); break;
        case 5: _t->autoPlayChanged(); break;
        case 6: _t->paused(); break;
        case 7: _t->stopped(); break;
        case 8: _t->playing(); break;
        case 9: _t->statusChanged(); break;
        case 10: _t->durationChanged(); break;
        case 11: _t->positionChanged(); break;
        case 12: _t->volumeChanged(); break;
        case 13: _t->mutedChanged(); break;
        case 14: _t->hasAudioChanged(); break;
        case 15: _t->hasVideoChanged(); break;
        case 16: _t->bufferProgressChanged(); break;
        case 17: _t->seekableChanged(); break;
        case 18: _t->playbackRateChanged(); break;
        case 19: _t->audioRoleChanged(); break;
        case 20: _t->availabilityChanged((*reinterpret_cast< Availability(*)>(_a[1]))); break;
        case 21: _t->errorChanged(); break;
        case 22: _t->error((*reinterpret_cast< QDeclarativeAudio::Error(*)>(_a[1])),(*reinterpret_cast< const QString(*)>(_a[2]))); break;
        case 23: _t->mediaObjectChanged(); break;
        case 24: _t->play(); break;
        case 25: _t->pause(); break;
        case 26: _t->stop(); break;
        case 27: _t->seek((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 28: { QJSValue _r = _t->supportedAudioRoles();
            if (_a[0]) *reinterpret_cast< QJSValue*>(_a[0]) = _r; }  break;
        case 29: _t->_q_error((*reinterpret_cast< QMediaPlayer::Error(*)>(_a[1]))); break;
        case 30: _t->_q_availabilityChanged((*reinterpret_cast< QMultimedia::AvailabilityStatus(*)>(_a[1]))); break;
        case 31: _t->_q_statusChanged(); break;
        case 32: _t->_q_mediaChanged((*reinterpret_cast< const QMediaContent(*)>(_a[1]))); break;
        default: ;
        }
    } else if (_c == QMetaObject::RegisterMethodArgumentMetaType) {
        switch (_id) {
        default: *reinterpret_cast<int*>(_a[0]) = -1; break;
        case 29:
            switch (*reinterpret_cast<int*>(_a[1])) {
            default: *reinterpret_cast<int*>(_a[0]) = -1; break;
            case 0:
                *reinterpret_cast<int*>(_a[0]) = qRegisterMetaType< QMediaPlayer::Error >(); break;
            }
            break;
        case 30:
            switch (*reinterpret_cast<int*>(_a[1])) {
            default: *reinterpret_cast<int*>(_a[0]) = -1; break;
            case 0:
                *reinterpret_cast<int*>(_a[0]) = qRegisterMetaType< QMultimedia::AvailabilityStatus >(); break;
            }
            break;
        case 32:
            switch (*reinterpret_cast<int*>(_a[1])) {
            default: *reinterpret_cast<int*>(_a[0]) = -1; break;
            case 0:
                *reinterpret_cast<int*>(_a[0]) = qRegisterMetaType< QMediaContent >(); break;
            }
            break;
        }
    } else if (_c == QMetaObject::IndexOfMethod) {
        int *result = reinterpret_cast<int *>(_a[0]);
        void **func = reinterpret_cast<void **>(_a[1]);
        {
            typedef void (QDeclarativeAudio::*_t)();
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&QDeclarativeAudio::playlistChanged)) {
                *result = 0;
                return;
            }
        }
        {
            typedef void (QDeclarativeAudio::*_t)();
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&QDeclarativeAudio::sourceChanged)) {
                *result = 1;
                return;
            }
        }
        {
            typedef void (QDeclarativeAudio::*_t)();
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&QDeclarativeAudio::autoLoadChanged)) {
                *result = 2;
                return;
            }
        }
        {
            typedef void (QDeclarativeAudio::*_t)();
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&QDeclarativeAudio::loopCountChanged)) {
                *result = 3;
                return;
            }
        }
        {
            typedef void (QDeclarativeAudio::*_t)();
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&QDeclarativeAudio::playbackStateChanged)) {
                *result = 4;
                return;
            }
        }
        {
            typedef void (QDeclarativeAudio::*_t)();
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&QDeclarativeAudio::autoPlayChanged)) {
                *result = 5;
                return;
            }
        }
        {
            typedef void (QDeclarativeAudio::*_t)();
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&QDeclarativeAudio::paused)) {
                *result = 6;
                return;
            }
        }
        {
            typedef void (QDeclarativeAudio::*_t)();
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&QDeclarativeAudio::stopped)) {
                *result = 7;
                return;
            }
        }
        {
            typedef void (QDeclarativeAudio::*_t)();
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&QDeclarativeAudio::playing)) {
                *result = 8;
                return;
            }
        }
        {
            typedef void (QDeclarativeAudio::*_t)();
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&QDeclarativeAudio::statusChanged)) {
                *result = 9;
                return;
            }
        }
        {
            typedef void (QDeclarativeAudio::*_t)();
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&QDeclarativeAudio::durationChanged)) {
                *result = 10;
                return;
            }
        }
        {
            typedef void (QDeclarativeAudio::*_t)();
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&QDeclarativeAudio::positionChanged)) {
                *result = 11;
                return;
            }
        }
        {
            typedef void (QDeclarativeAudio::*_t)();
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&QDeclarativeAudio::volumeChanged)) {
                *result = 12;
                return;
            }
        }
        {
            typedef void (QDeclarativeAudio::*_t)();
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&QDeclarativeAudio::mutedChanged)) {
                *result = 13;
                return;
            }
        }
        {
            typedef void (QDeclarativeAudio::*_t)();
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&QDeclarativeAudio::hasAudioChanged)) {
                *result = 14;
                return;
            }
        }
        {
            typedef void (QDeclarativeAudio::*_t)();
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&QDeclarativeAudio::hasVideoChanged)) {
                *result = 15;
                return;
            }
        }
        {
            typedef void (QDeclarativeAudio::*_t)();
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&QDeclarativeAudio::bufferProgressChanged)) {
                *result = 16;
                return;
            }
        }
        {
            typedef void (QDeclarativeAudio::*_t)();
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&QDeclarativeAudio::seekableChanged)) {
                *result = 17;
                return;
            }
        }
        {
            typedef void (QDeclarativeAudio::*_t)();
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&QDeclarativeAudio::playbackRateChanged)) {
                *result = 18;
                return;
            }
        }
        {
            typedef void (QDeclarativeAudio::*_t)();
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&QDeclarativeAudio::audioRoleChanged)) {
                *result = 19;
                return;
            }
        }
        {
            typedef void (QDeclarativeAudio::*_t)(Availability );
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&QDeclarativeAudio::availabilityChanged)) {
                *result = 20;
                return;
            }
        }
        {
            typedef void (QDeclarativeAudio::*_t)();
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&QDeclarativeAudio::errorChanged)) {
                *result = 21;
                return;
            }
        }
        {
            typedef void (QDeclarativeAudio::*_t)(QDeclarativeAudio::Error , const QString & );
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&QDeclarativeAudio::error)) {
                *result = 22;
                return;
            }
        }
        {
            typedef void (QDeclarativeAudio::*_t)();
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&QDeclarativeAudio::mediaObjectChanged)) {
                *result = 23;
                return;
            }
        }
    }
#ifndef QT_NO_PROPERTIES
    else if (_c == QMetaObject::ReadProperty) {
        QDeclarativeAudio *_t = static_cast<QDeclarativeAudio *>(_o);
        Q_UNUSED(_t)
        void *_v = _a[0];
        switch (_id) {
        case 0: *reinterpret_cast< QUrl*>(_v) = _t->source(); break;
        case 1: *reinterpret_cast< QDeclarativePlaylist**>(_v) = _t->playlist(); break;
        case 2: *reinterpret_cast< int*>(_v) = _t->loopCount(); break;
        case 3: *reinterpret_cast< PlaybackState*>(_v) = _t->playbackState(); break;
        case 4: *reinterpret_cast< bool*>(_v) = _t->autoPlay(); break;
        case 5: *reinterpret_cast< bool*>(_v) = _t->isAutoLoad(); break;
        case 6: *reinterpret_cast< Status*>(_v) = _t->status(); break;
        case 7: *reinterpret_cast< int*>(_v) = _t->duration(); break;
        case 8: *reinterpret_cast< int*>(_v) = _t->position(); break;
        case 9: *reinterpret_cast< qreal*>(_v) = _t->volume(); break;
        case 10: *reinterpret_cast< bool*>(_v) = _t->isMuted(); break;
        case 11: *reinterpret_cast< bool*>(_v) = _t->hasAudio(); break;
        case 12: *reinterpret_cast< bool*>(_v) = _t->hasVideo(); break;
        case 13: *reinterpret_cast< qreal*>(_v) = _t->bufferProgress(); break;
        case 14: *reinterpret_cast< bool*>(_v) = _t->isSeekable(); break;
        case 15: *reinterpret_cast< qreal*>(_v) = _t->playbackRate(); break;
        case 16: *reinterpret_cast< Error*>(_v) = _t->error(); break;
        case 17: *reinterpret_cast< QString*>(_v) = _t->errorString(); break;
        case 18: *reinterpret_cast< QDeclarativeMediaMetaData**>(_v) = _t->metaData(); break;
        case 19: *reinterpret_cast< QObject**>(_v) = _t->mediaObject(); break;
        case 20: *reinterpret_cast< Availability*>(_v) = _t->availability(); break;
        case 21: *reinterpret_cast< AudioRole*>(_v) = _t->audioRole(); break;
        default: break;
        }
    } else if (_c == QMetaObject::WriteProperty) {
        QDeclarativeAudio *_t = static_cast<QDeclarativeAudio *>(_o);
        Q_UNUSED(_t)
        void *_v = _a[0];
        switch (_id) {
        case 0: _t->setSource(*reinterpret_cast< QUrl*>(_v)); break;
        case 1: _t->setPlaylist(*reinterpret_cast< QDeclarativePlaylist**>(_v)); break;
        case 2: _t->setLoopCount(*reinterpret_cast< int*>(_v)); break;
        case 4: _t->setAutoPlay(*reinterpret_cast< bool*>(_v)); break;
        case 5: _t->setAutoLoad(*reinterpret_cast< bool*>(_v)); break;
        case 9: _t->setVolume(*reinterpret_cast< qreal*>(_v)); break;
        case 10: _t->setMuted(*reinterpret_cast< bool*>(_v)); break;
        case 15: _t->setPlaybackRate(*reinterpret_cast< qreal*>(_v)); break;
        case 21: _t->setAudioRole(*reinterpret_cast< AudioRole*>(_v)); break;
        default: break;
        }
    } else if (_c == QMetaObject::ResetProperty) {
    }
#endif // QT_NO_PROPERTIES
}

const QMetaObject QDeclarativeAudio::staticMetaObject = {
    { &QObject::staticMetaObject, qt_meta_stringdata_QDeclarativeAudio.data,
      qt_meta_data_QDeclarativeAudio,  qt_static_metacall, Q_NULLPTR, Q_NULLPTR}
};


const QMetaObject *QDeclarativeAudio::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->dynamicMetaObject() : &staticMetaObject;
}

void *QDeclarativeAudio::qt_metacast(const char *_clname)
{
    if (!_clname) return Q_NULLPTR;
    if (!strcmp(_clname, qt_meta_stringdata_QDeclarativeAudio.stringdata0))
        return static_cast<void*>(const_cast< QDeclarativeAudio*>(this));
    if (!strcmp(_clname, "QQmlParserStatus"))
        return static_cast< QQmlParserStatus*>(const_cast< QDeclarativeAudio*>(this));
    if (!strcmp(_clname, "org.qt-project.Qt.QQmlParserStatus"))
        return static_cast< QQmlParserStatus*>(const_cast< QDeclarativeAudio*>(this));
    return QObject::qt_metacast(_clname);
}

int QDeclarativeAudio::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QObject::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 33)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 33;
    } else if (_c == QMetaObject::RegisterMethodArgumentMetaType) {
        if (_id < 33)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 33;
    }
#ifndef QT_NO_PROPERTIES
   else if (_c == QMetaObject::ReadProperty || _c == QMetaObject::WriteProperty
            || _c == QMetaObject::ResetProperty || _c == QMetaObject::RegisterPropertyMetaType) {
        qt_static_metacall(this, _c, _id, _a);
        _id -= 22;
    } else if (_c == QMetaObject::QueryPropertyDesignable) {
        _id -= 22;
    } else if (_c == QMetaObject::QueryPropertyScriptable) {
        _id -= 22;
    } else if (_c == QMetaObject::QueryPropertyStored) {
        _id -= 22;
    } else if (_c == QMetaObject::QueryPropertyEditable) {
        _id -= 22;
    } else if (_c == QMetaObject::QueryPropertyUser) {
        _id -= 22;
    }
#endif // QT_NO_PROPERTIES
    return _id;
}

// SIGNAL 0
void QDeclarativeAudio::playlistChanged()
{
    QMetaObject::activate(this, &staticMetaObject, 0, Q_NULLPTR);
}

// SIGNAL 1
void QDeclarativeAudio::sourceChanged()
{
    QMetaObject::activate(this, &staticMetaObject, 1, Q_NULLPTR);
}

// SIGNAL 2
void QDeclarativeAudio::autoLoadChanged()
{
    QMetaObject::activate(this, &staticMetaObject, 2, Q_NULLPTR);
}

// SIGNAL 3
void QDeclarativeAudio::loopCountChanged()
{
    QMetaObject::activate(this, &staticMetaObject, 3, Q_NULLPTR);
}

// SIGNAL 4
void QDeclarativeAudio::playbackStateChanged()
{
    QMetaObject::activate(this, &staticMetaObject, 4, Q_NULLPTR);
}

// SIGNAL 5
void QDeclarativeAudio::autoPlayChanged()
{
    QMetaObject::activate(this, &staticMetaObject, 5, Q_NULLPTR);
}

// SIGNAL 6
void QDeclarativeAudio::paused()
{
    QMetaObject::activate(this, &staticMetaObject, 6, Q_NULLPTR);
}

// SIGNAL 7
void QDeclarativeAudio::stopped()
{
    QMetaObject::activate(this, &staticMetaObject, 7, Q_NULLPTR);
}

// SIGNAL 8
void QDeclarativeAudio::playing()
{
    QMetaObject::activate(this, &staticMetaObject, 8, Q_NULLPTR);
}

// SIGNAL 9
void QDeclarativeAudio::statusChanged()
{
    QMetaObject::activate(this, &staticMetaObject, 9, Q_NULLPTR);
}

// SIGNAL 10
void QDeclarativeAudio::durationChanged()
{
    QMetaObject::activate(this, &staticMetaObject, 10, Q_NULLPTR);
}

// SIGNAL 11
void QDeclarativeAudio::positionChanged()
{
    QMetaObject::activate(this, &staticMetaObject, 11, Q_NULLPTR);
}

// SIGNAL 12
void QDeclarativeAudio::volumeChanged()
{
    QMetaObject::activate(this, &staticMetaObject, 12, Q_NULLPTR);
}

// SIGNAL 13
void QDeclarativeAudio::mutedChanged()
{
    QMetaObject::activate(this, &staticMetaObject, 13, Q_NULLPTR);
}

// SIGNAL 14
void QDeclarativeAudio::hasAudioChanged()
{
    QMetaObject::activate(this, &staticMetaObject, 14, Q_NULLPTR);
}

// SIGNAL 15
void QDeclarativeAudio::hasVideoChanged()
{
    QMetaObject::activate(this, &staticMetaObject, 15, Q_NULLPTR);
}

// SIGNAL 16
void QDeclarativeAudio::bufferProgressChanged()
{
    QMetaObject::activate(this, &staticMetaObject, 16, Q_NULLPTR);
}

// SIGNAL 17
void QDeclarativeAudio::seekableChanged()
{
    QMetaObject::activate(this, &staticMetaObject, 17, Q_NULLPTR);
}

// SIGNAL 18
void QDeclarativeAudio::playbackRateChanged()
{
    QMetaObject::activate(this, &staticMetaObject, 18, Q_NULLPTR);
}

// SIGNAL 19
void QDeclarativeAudio::audioRoleChanged()
{
    QMetaObject::activate(this, &staticMetaObject, 19, Q_NULLPTR);
}

// SIGNAL 20
void QDeclarativeAudio::availabilityChanged(Availability _t1)
{
    void *_a[] = { Q_NULLPTR, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 20, _a);
}

// SIGNAL 21
void QDeclarativeAudio::errorChanged()
{
    QMetaObject::activate(this, &staticMetaObject, 21, Q_NULLPTR);
}

// SIGNAL 22
void QDeclarativeAudio::error(QDeclarativeAudio::Error _t1, const QString & _t2)
{
    void *_a[] = { Q_NULLPTR, const_cast<void*>(reinterpret_cast<const void*>(&_t1)), const_cast<void*>(reinterpret_cast<const void*>(&_t2)) };
    QMetaObject::activate(this, &staticMetaObject, 22, _a);
}

// SIGNAL 23
void QDeclarativeAudio::mediaObjectChanged()
{
    QMetaObject::activate(this, &staticMetaObject, 23, Q_NULLPTR);
}
QT_END_MOC_NAMESPACE
