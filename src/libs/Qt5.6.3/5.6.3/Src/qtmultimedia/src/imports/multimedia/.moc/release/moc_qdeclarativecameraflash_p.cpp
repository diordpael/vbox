/****************************************************************************
** Meta object code from reading C++ file 'qdeclarativecameraflash_p.h'
**
** Created by: The Qt Meta Object Compiler version 67 (Qt 5.6.3)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "../../qdeclarativecameraflash_p.h"
#include <QtCore/qbytearray.h>
#include <QtCore/qmetatype.h>
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'qdeclarativecameraflash_p.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 67
#error "This file was generated using the moc from 5.6.3. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
struct qt_meta_stringdata_QDeclarativeCameraFlash_t {
    QByteArrayData data[19];
    char stringdata0[242];
};
#define QT_MOC_LITERAL(idx, ofs, len) \
    Q_STATIC_BYTE_ARRAY_DATA_HEADER_INITIALIZER_WITH_OFFSET(len, \
    qptrdiff(offsetof(qt_meta_stringdata_QDeclarativeCameraFlash_t, stringdata0) + ofs \
        - idx * sizeof(QByteArrayData)) \
    )
static const qt_meta_stringdata_QDeclarativeCameraFlash_t qt_meta_stringdata_QDeclarativeCameraFlash = {
    {
QT_MOC_LITERAL(0, 0, 23), // "QDeclarativeCameraFlash"
QT_MOC_LITERAL(1, 24, 10), // "flashReady"
QT_MOC_LITERAL(2, 35, 0), // ""
QT_MOC_LITERAL(3, 36, 6), // "status"
QT_MOC_LITERAL(4, 43, 16), // "flashModeChanged"
QT_MOC_LITERAL(5, 60, 9), // "FlashMode"
QT_MOC_LITERAL(6, 70, 12), // "setFlashMode"
QT_MOC_LITERAL(7, 83, 5), // "ready"
QT_MOC_LITERAL(8, 89, 4), // "mode"
QT_MOC_LITERAL(9, 94, 9), // "FlashAuto"
QT_MOC_LITERAL(10, 104, 8), // "FlashOff"
QT_MOC_LITERAL(11, 113, 7), // "FlashOn"
QT_MOC_LITERAL(12, 121, 20), // "FlashRedEyeReduction"
QT_MOC_LITERAL(13, 142, 9), // "FlashFill"
QT_MOC_LITERAL(14, 152, 10), // "FlashTorch"
QT_MOC_LITERAL(15, 163, 15), // "FlashVideoLight"
QT_MOC_LITERAL(16, 179, 25), // "FlashSlowSyncFrontCurtain"
QT_MOC_LITERAL(17, 205, 24), // "FlashSlowSyncRearCurtain"
QT_MOC_LITERAL(18, 230, 11) // "FlashManual"

    },
    "QDeclarativeCameraFlash\0flashReady\0\0"
    "status\0flashModeChanged\0FlashMode\0"
    "setFlashMode\0ready\0mode\0FlashAuto\0"
    "FlashOff\0FlashOn\0FlashRedEyeReduction\0"
    "FlashFill\0FlashTorch\0FlashVideoLight\0"
    "FlashSlowSyncFrontCurtain\0"
    "FlashSlowSyncRearCurtain\0FlashManual"
};
#undef QT_MOC_LITERAL

static const uint qt_meta_data_QDeclarativeCameraFlash[] = {

 // content:
       7,       // revision
       0,       // classname
       0,    0, // classinfo
       3,   14, // methods
       2,   38, // properties
       1,   46, // enums/sets
       0,    0, // constructors
       0,       // flags
       2,       // signalCount

 // signals: name, argc, parameters, tag, flags
       1,    1,   29,    2, 0x06 /* Public */,
       4,    1,   32,    2, 0x06 /* Public */,

 // slots: name, argc, parameters, tag, flags
       6,    1,   35,    2, 0x0a /* Public */,

 // signals: parameters
    QMetaType::Void, QMetaType::Bool,    3,
    QMetaType::Void, 0x80000000 | 5,    2,

 // slots: parameters
    QMetaType::Void, 0x80000000 | 5,    2,

 // properties: name, type, flags
       7, QMetaType::Bool, 0x00495001,
       8, 0x80000000 | 5, 0x0049500b,

 // properties: notify_signal_id
       0,
       1,

 // enums: name, flags, count, data
       5, 0x0,   10,   50,

 // enum data: key, value
       9, uint(QDeclarativeCameraFlash::FlashAuto),
      10, uint(QDeclarativeCameraFlash::FlashOff),
      11, uint(QDeclarativeCameraFlash::FlashOn),
      12, uint(QDeclarativeCameraFlash::FlashRedEyeReduction),
      13, uint(QDeclarativeCameraFlash::FlashFill),
      14, uint(QDeclarativeCameraFlash::FlashTorch),
      15, uint(QDeclarativeCameraFlash::FlashVideoLight),
      16, uint(QDeclarativeCameraFlash::FlashSlowSyncFrontCurtain),
      17, uint(QDeclarativeCameraFlash::FlashSlowSyncRearCurtain),
      18, uint(QDeclarativeCameraFlash::FlashManual),

       0        // eod
};

void QDeclarativeCameraFlash::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        QDeclarativeCameraFlash *_t = static_cast<QDeclarativeCameraFlash *>(_o);
        Q_UNUSED(_t)
        switch (_id) {
        case 0: _t->flashReady((*reinterpret_cast< bool(*)>(_a[1]))); break;
        case 1: _t->flashModeChanged((*reinterpret_cast< FlashMode(*)>(_a[1]))); break;
        case 2: _t->setFlashMode((*reinterpret_cast< FlashMode(*)>(_a[1]))); break;
        default: ;
        }
    } else if (_c == QMetaObject::IndexOfMethod) {
        int *result = reinterpret_cast<int *>(_a[0]);
        void **func = reinterpret_cast<void **>(_a[1]);
        {
            typedef void (QDeclarativeCameraFlash::*_t)(bool );
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&QDeclarativeCameraFlash::flashReady)) {
                *result = 0;
                return;
            }
        }
        {
            typedef void (QDeclarativeCameraFlash::*_t)(FlashMode );
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&QDeclarativeCameraFlash::flashModeChanged)) {
                *result = 1;
                return;
            }
        }
    }
#ifndef QT_NO_PROPERTIES
    else if (_c == QMetaObject::ReadProperty) {
        QDeclarativeCameraFlash *_t = static_cast<QDeclarativeCameraFlash *>(_o);
        Q_UNUSED(_t)
        void *_v = _a[0];
        switch (_id) {
        case 0: *reinterpret_cast< bool*>(_v) = _t->isFlashReady(); break;
        case 1: *reinterpret_cast< FlashMode*>(_v) = _t->flashMode(); break;
        default: break;
        }
    } else if (_c == QMetaObject::WriteProperty) {
        QDeclarativeCameraFlash *_t = static_cast<QDeclarativeCameraFlash *>(_o);
        Q_UNUSED(_t)
        void *_v = _a[0];
        switch (_id) {
        case 1: _t->setFlashMode(*reinterpret_cast< FlashMode*>(_v)); break;
        default: break;
        }
    } else if (_c == QMetaObject::ResetProperty) {
    }
#endif // QT_NO_PROPERTIES
}

const QMetaObject QDeclarativeCameraFlash::staticMetaObject = {
    { &QObject::staticMetaObject, qt_meta_stringdata_QDeclarativeCameraFlash.data,
      qt_meta_data_QDeclarativeCameraFlash,  qt_static_metacall, Q_NULLPTR, Q_NULLPTR}
};


const QMetaObject *QDeclarativeCameraFlash::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->dynamicMetaObject() : &staticMetaObject;
}

void *QDeclarativeCameraFlash::qt_metacast(const char *_clname)
{
    if (!_clname) return Q_NULLPTR;
    if (!strcmp(_clname, qt_meta_stringdata_QDeclarativeCameraFlash.stringdata0))
        return static_cast<void*>(const_cast< QDeclarativeCameraFlash*>(this));
    return QObject::qt_metacast(_clname);
}

int QDeclarativeCameraFlash::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QObject::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 3)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 3;
    } else if (_c == QMetaObject::RegisterMethodArgumentMetaType) {
        if (_id < 3)
            *reinterpret_cast<int*>(_a[0]) = -1;
        _id -= 3;
    }
#ifndef QT_NO_PROPERTIES
   else if (_c == QMetaObject::ReadProperty || _c == QMetaObject::WriteProperty
            || _c == QMetaObject::ResetProperty || _c == QMetaObject::RegisterPropertyMetaType) {
        qt_static_metacall(this, _c, _id, _a);
        _id -= 2;
    } else if (_c == QMetaObject::QueryPropertyDesignable) {
        _id -= 2;
    } else if (_c == QMetaObject::QueryPropertyScriptable) {
        _id -= 2;
    } else if (_c == QMetaObject::QueryPropertyStored) {
        _id -= 2;
    } else if (_c == QMetaObject::QueryPropertyEditable) {
        _id -= 2;
    } else if (_c == QMetaObject::QueryPropertyUser) {
        _id -= 2;
    }
#endif // QT_NO_PROPERTIES
    return _id;
}

// SIGNAL 0
void QDeclarativeCameraFlash::flashReady(bool _t1)
{
    void *_a[] = { Q_NULLPTR, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 0, _a);
}

// SIGNAL 1
void QDeclarativeCameraFlash::flashModeChanged(FlashMode _t1)
{
    void *_a[] = { Q_NULLPTR, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 1, _a);
}
QT_END_MOC_NAMESPACE
