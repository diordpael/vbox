/****************************************************************************
** Meta object code from reading C++ file 'mainwindow.h'
**
** Created by: The Qt Meta Object Compiler version 67 (Qt 5.6.3)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "../../mainwindow.h"
#include <QtCore/qbytearray.h>
#include <QtCore/qmetatype.h>
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'mainwindow.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 67
#error "This file was generated using the moc from 5.6.3. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
struct qt_meta_stringdata_MainWindow_t {
    QByteArrayData data[36];
    char stringdata0[659];
};
#define QT_MOC_LITERAL(idx, ofs, len) \
    Q_STATIC_BYTE_ARRAY_DATA_HEADER_INITIALIZER_WITH_OFFSET(len, \
    qptrdiff(offsetof(qt_meta_stringdata_MainWindow_t, stringdata0) + ofs \
        - idx * sizeof(QByteArrayData)) \
    )
static const qt_meta_stringdata_MainWindow_t qt_meta_stringdata_MainWindow = {
    {
QT_MOC_LITERAL(0, 0, 10), // "MainWindow"
QT_MOC_LITERAL(1, 11, 13), // "appendLogText"
QT_MOC_LITERAL(2, 25, 0), // ""
QT_MOC_LITERAL(3, 26, 26), // "on_actionFileNew_triggered"
QT_MOC_LITERAL(4, 53, 27), // "on_actionFileLoad_triggered"
QT_MOC_LITERAL(5, 81, 27), // "on_actionFileSave_triggered"
QT_MOC_LITERAL(6, 109, 31), // "on_actionContainerSet_triggered"
QT_MOC_LITERAL(7, 141, 33), // "on_actionContainerClear_trigg..."
QT_MOC_LITERAL(8, 175, 38), // "on_actionContainerProperties_..."
QT_MOC_LITERAL(9, 214, 30), // "on_actionControlInfo_triggered"
QT_MOC_LITERAL(10, 245, 39), // "on_actionControlDocumentation..."
QT_MOC_LITERAL(11, 285, 32), // "on_actionControlPixmap_triggered"
QT_MOC_LITERAL(12, 318, 36), // "on_actionControlProperties_tr..."
QT_MOC_LITERAL(13, 355, 33), // "on_actionControlMethods_trigg..."
QT_MOC_LITERAL(14, 389, 23), // "on_VerbMenu_aboutToShow"
QT_MOC_LITERAL(15, 413, 32), // "on_actionScriptingLoad_triggered"
QT_MOC_LITERAL(16, 446, 31), // "on_actionScriptingRun_triggered"
QT_MOC_LITERAL(17, 478, 9), // "updateGUI"
QT_MOC_LITERAL(18, 488, 18), // "logPropertyChanged"
QT_MOC_LITERAL(19, 507, 4), // "prop"
QT_MOC_LITERAL(20, 512, 9), // "logSignal"
QT_MOC_LITERAL(21, 522, 6), // "signal"
QT_MOC_LITERAL(22, 529, 4), // "argc"
QT_MOC_LITERAL(23, 534, 4), // "argv"
QT_MOC_LITERAL(24, 539, 12), // "logException"
QT_MOC_LITERAL(25, 552, 4), // "code"
QT_MOC_LITERAL(26, 557, 6), // "source"
QT_MOC_LITERAL(27, 564, 4), // "desc"
QT_MOC_LITERAL(28, 569, 4), // "help"
QT_MOC_LITERAL(29, 574, 8), // "logMacro"
QT_MOC_LITERAL(30, 583, 11), // "description"
QT_MOC_LITERAL(31, 595, 14), // "sourcePosition"
QT_MOC_LITERAL(32, 610, 10), // "sourceText"
QT_MOC_LITERAL(33, 621, 21), // "on_VerbMenu_triggered"
QT_MOC_LITERAL(34, 643, 8), // "QAction*"
QT_MOC_LITERAL(35, 652, 6) // "action"

    },
    "MainWindow\0appendLogText\0\0"
    "on_actionFileNew_triggered\0"
    "on_actionFileLoad_triggered\0"
    "on_actionFileSave_triggered\0"
    "on_actionContainerSet_triggered\0"
    "on_actionContainerClear_triggered\0"
    "on_actionContainerProperties_triggered\0"
    "on_actionControlInfo_triggered\0"
    "on_actionControlDocumentation_triggered\0"
    "on_actionControlPixmap_triggered\0"
    "on_actionControlProperties_triggered\0"
    "on_actionControlMethods_triggered\0"
    "on_VerbMenu_aboutToShow\0"
    "on_actionScriptingLoad_triggered\0"
    "on_actionScriptingRun_triggered\0"
    "updateGUI\0logPropertyChanged\0prop\0"
    "logSignal\0signal\0argc\0argv\0logException\0"
    "code\0source\0desc\0help\0logMacro\0"
    "description\0sourcePosition\0sourceText\0"
    "on_VerbMenu_triggered\0QAction*\0action"
};
#undef QT_MOC_LITERAL

static const uint qt_meta_data_MainWindow[] = {

 // content:
       7,       // revision
       0,       // classname
       0,    0, // classinfo
      21,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       0,       // signalCount

 // slots: name, argc, parameters, tag, flags
       1,    1,  119,    2, 0x0a /* Public */,
       3,    0,  122,    2, 0x09 /* Protected */,
       4,    0,  123,    2, 0x09 /* Protected */,
       5,    0,  124,    2, 0x09 /* Protected */,
       6,    0,  125,    2, 0x09 /* Protected */,
       7,    0,  126,    2, 0x09 /* Protected */,
       8,    0,  127,    2, 0x09 /* Protected */,
       9,    0,  128,    2, 0x09 /* Protected */,
      10,    0,  129,    2, 0x09 /* Protected */,
      11,    0,  130,    2, 0x09 /* Protected */,
      12,    0,  131,    2, 0x09 /* Protected */,
      13,    0,  132,    2, 0x09 /* Protected */,
      14,    0,  133,    2, 0x09 /* Protected */,
      15,    0,  134,    2, 0x09 /* Protected */,
      16,    0,  135,    2, 0x09 /* Protected */,
      17,    0,  136,    2, 0x08 /* Private */,
      18,    1,  137,    2, 0x08 /* Private */,
      20,    3,  140,    2, 0x08 /* Private */,
      24,    4,  147,    2, 0x08 /* Private */,
      29,    4,  156,    2, 0x08 /* Private */,
      33,    1,  165,    2, 0x08 /* Private */,

 // slots: parameters
    QMetaType::Void, QMetaType::QString,    2,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void, QMetaType::QString,   19,
    QMetaType::Void, QMetaType::QString, QMetaType::Int, QMetaType::VoidStar,   21,   22,   23,
    QMetaType::Void, QMetaType::Int, QMetaType::QString, QMetaType::QString, QMetaType::QString,   25,   26,   27,   28,
    QMetaType::Void, QMetaType::Int, QMetaType::QString, QMetaType::Int, QMetaType::QString,   25,   30,   31,   32,
    QMetaType::Void, 0x80000000 | 34,   35,

       0        // eod
};

void MainWindow::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        MainWindow *_t = static_cast<MainWindow *>(_o);
        Q_UNUSED(_t)
        switch (_id) {
        case 0: _t->appendLogText((*reinterpret_cast< const QString(*)>(_a[1]))); break;
        case 1: _t->on_actionFileNew_triggered(); break;
        case 2: _t->on_actionFileLoad_triggered(); break;
        case 3: _t->on_actionFileSave_triggered(); break;
        case 4: _t->on_actionContainerSet_triggered(); break;
        case 5: _t->on_actionContainerClear_triggered(); break;
        case 6: _t->on_actionContainerProperties_triggered(); break;
        case 7: _t->on_actionControlInfo_triggered(); break;
        case 8: _t->on_actionControlDocumentation_triggered(); break;
        case 9: _t->on_actionControlPixmap_triggered(); break;
        case 10: _t->on_actionControlProperties_triggered(); break;
        case 11: _t->on_actionControlMethods_triggered(); break;
        case 12: _t->on_VerbMenu_aboutToShow(); break;
        case 13: _t->on_actionScriptingLoad_triggered(); break;
        case 14: _t->on_actionScriptingRun_triggered(); break;
        case 15: _t->updateGUI(); break;
        case 16: _t->logPropertyChanged((*reinterpret_cast< const QString(*)>(_a[1]))); break;
        case 17: _t->logSignal((*reinterpret_cast< const QString(*)>(_a[1])),(*reinterpret_cast< int(*)>(_a[2])),(*reinterpret_cast< void*(*)>(_a[3]))); break;
        case 18: _t->logException((*reinterpret_cast< int(*)>(_a[1])),(*reinterpret_cast< const QString(*)>(_a[2])),(*reinterpret_cast< const QString(*)>(_a[3])),(*reinterpret_cast< const QString(*)>(_a[4]))); break;
        case 19: _t->logMacro((*reinterpret_cast< int(*)>(_a[1])),(*reinterpret_cast< const QString(*)>(_a[2])),(*reinterpret_cast< int(*)>(_a[3])),(*reinterpret_cast< const QString(*)>(_a[4]))); break;
        case 20: _t->on_VerbMenu_triggered((*reinterpret_cast< QAction*(*)>(_a[1]))); break;
        default: ;
        }
    }
}

const QMetaObject MainWindow::staticMetaObject = {
    { &QMainWindow::staticMetaObject, qt_meta_stringdata_MainWindow.data,
      qt_meta_data_MainWindow,  qt_static_metacall, Q_NULLPTR, Q_NULLPTR}
};


const QMetaObject *MainWindow::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->dynamicMetaObject() : &staticMetaObject;
}

void *MainWindow::qt_metacast(const char *_clname)
{
    if (!_clname) return Q_NULLPTR;
    if (!strcmp(_clname, qt_meta_stringdata_MainWindow.stringdata0))
        return static_cast<void*>(const_cast< MainWindow*>(this));
    if (!strcmp(_clname, "Ui::MainWindow"))
        return static_cast< Ui::MainWindow*>(const_cast< MainWindow*>(this));
    return QMainWindow::qt_metacast(_clname);
}

int MainWindow::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QMainWindow::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 21)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 21;
    } else if (_c == QMetaObject::RegisterMethodArgumentMetaType) {
        if (_id < 21)
            *reinterpret_cast<int*>(_a[0]) = -1;
        _id -= 21;
    }
    return _id;
}
QT_END_MOC_NAMESPACE
