/*
*********************************************************************
**
** Copyright (C) 2015 The Qt Company Ltd.
** Contact: http://www.qt.io/licensing/
**
** This file is part of the tools applications of the Qt Toolkit.
**
** $QT_BEGIN_LICENSE:LGPL21$
** Commercial License Usage
** Licensees holding valid commercial Qt licenses may use this file in
** accordance with the commercial license agreement provided with the
** Software or, alternatively, in accordance with the terms contained in
** a written agreement between you and The Qt Company. For licensing terms
** and conditions see http://www.qt.io/terms-conditions. For further
** information use the contact form at http://www.qt.io/contact-us.
**
** GNU Lesser General Public License Usage
** Alternatively, this file may be used under the terms of the GNU Lesser
** General Public License version 2.1 or version 3 as published by the Free
** Software Foundation and appearing in the file LICENSE.LGPLv21 and
** LICENSE.LGPLv3 included in the packaging of this file. Please review the
** following information to ensure the GNU Lesser General Public License
** requirements will be met: https://www.gnu.org/licenses/lgpl.html and
** http://www.gnu.org/licenses/old-licenses/lgpl-2.1.html.
**
** As a special exception, The Qt Company gives you certain additional
** rights. These rights are described in The Qt Company LGPL Exception
** version 1.1, included in the file LGPL_EXCEPTION.txt in this package.
**
** $QT_END_LICENSE$
**
*********************************************************************
*/

/********************************************************************************
** Form generated from reading UI file 'controlinfo.ui'
**
** Created by: Qt User Interface Compiler version 5.6.3
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_CONTROLINFO_H
#define UI_CONTROLINFO_H

#include <QtCore/QVariant>
#include <QtWidgets/QAction>
#include <QtWidgets/QApplication>
#include <QtWidgets/QButtonGroup>
#include <QtWidgets/QDialog>
#include <QtWidgets/QHBoxLayout>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QSpacerItem>
#include <QtWidgets/QTreeWidget>
#include <QtWidgets/QVBoxLayout>

QT_BEGIN_NAMESPACE

class Ui_ControlInfo
{
public:
    QVBoxLayout *vboxLayout;
    QTreeWidget *listInfo;
    QHBoxLayout *hboxLayout;
    QSpacerItem *spacerItem;
    QPushButton *buttonClose;

    void setupUi(QDialog *ControlInfo)
    {
        if (ControlInfo->objectName().isEmpty())
            ControlInfo->setObjectName(QStringLiteral("ControlInfo"));
        ControlInfo->resize(600, 480);
        vboxLayout = new QVBoxLayout(ControlInfo);
        vboxLayout->setSpacing(6);
        vboxLayout->setContentsMargins(11, 11, 11, 11);
        vboxLayout->setObjectName(QStringLiteral("vboxLayout"));
        listInfo = new QTreeWidget(ControlInfo);
        listInfo->setObjectName(QStringLiteral("listInfo"));

        vboxLayout->addWidget(listInfo);

        hboxLayout = new QHBoxLayout();
        hboxLayout->setSpacing(6);
        hboxLayout->setContentsMargins(0, 0, 0, 0);
        hboxLayout->setObjectName(QStringLiteral("hboxLayout"));
        spacerItem = new QSpacerItem(1, 1, QSizePolicy::Expanding, QSizePolicy::Minimum);

        hboxLayout->addItem(spacerItem);

        buttonClose = new QPushButton(ControlInfo);
        buttonClose->setObjectName(QStringLiteral("buttonClose"));

        hboxLayout->addWidget(buttonClose);


        vboxLayout->addLayout(hboxLayout);


        retranslateUi(ControlInfo);
        QObject::connect(buttonClose, SIGNAL(clicked()), ControlInfo, SLOT(accept()));

        QMetaObject::connectSlotsByName(ControlInfo);
    } // setupUi

    void retranslateUi(QDialog *ControlInfo)
    {
        ControlInfo->setWindowTitle(QApplication::translate("ControlInfo", "Control Details", Q_NULLPTR));
        QTreeWidgetItem *___qtreewidgetitem = listInfo->headerItem();
        ___qtreewidgetitem->setText(1, QApplication::translate("ControlInfo", "Value", Q_NULLPTR));
        ___qtreewidgetitem->setText(0, QApplication::translate("ControlInfo", "Item", Q_NULLPTR));
        buttonClose->setText(QApplication::translate("ControlInfo", "C&lose", Q_NULLPTR));
    } // retranslateUi

};

namespace Ui {
    class ControlInfo: public Ui_ControlInfo {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_CONTROLINFO_H
