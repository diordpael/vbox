@echo off
SetLocal EnableDelayedExpansion
(set QT_VERSION=5.6.3)
(set QT_VER=5.6)
(set QT_VERSION_TAG=563)
(set QT_INSTALL_DOCS=C:/Qt/Qt5.6.3/5.6.3/Src/qtbase/doc)
C:\Qt\Qt5.6.3\5.6.3\msvc2010_64\bin\qdoc.exe %*
EndLocal
