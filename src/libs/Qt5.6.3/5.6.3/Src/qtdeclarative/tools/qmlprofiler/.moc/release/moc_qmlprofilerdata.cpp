/****************************************************************************
** Meta object code from reading C++ file 'qmlprofilerdata.h'
**
** Created by: The Qt Meta Object Compiler version 67 (Qt 5.6.3)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "../../qmlprofilerdata.h"
#include <QtCore/qbytearray.h>
#include <QtCore/qmetatype.h>
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'qmlprofilerdata.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 67
#error "This file was generated using the moc from 5.6.3. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
struct qt_meta_stringdata_QmlProfilerData_t {
    QByteArrayData data[49];
    char stringdata0[691];
};
#define QT_MOC_LITERAL(idx, ofs, len) \
    Q_STATIC_BYTE_ARRAY_DATA_HEADER_INITIALIZER_WITH_OFFSET(len, \
    qptrdiff(offsetof(qt_meta_stringdata_QmlProfilerData_t, stringdata0) + ofs \
        - idx * sizeof(QByteArrayData)) \
    )
static const qt_meta_stringdata_QmlProfilerData_t qt_meta_stringdata_QmlProfilerData = {
    {
QT_MOC_LITERAL(0, 0, 15), // "QmlProfilerData"
QT_MOC_LITERAL(1, 16, 5), // "error"
QT_MOC_LITERAL(2, 22, 0), // ""
QT_MOC_LITERAL(3, 23, 12), // "stateChanged"
QT_MOC_LITERAL(4, 36, 9), // "dataReady"
QT_MOC_LITERAL(5, 46, 5), // "clear"
QT_MOC_LITERAL(6, 52, 15), // "setTraceEndTime"
QT_MOC_LITERAL(7, 68, 4), // "time"
QT_MOC_LITERAL(8, 73, 17), // "setTraceStartTime"
QT_MOC_LITERAL(9, 91, 11), // "addQmlEvent"
QT_MOC_LITERAL(10, 103, 34), // "QQmlProfilerDefinitions::Rang..."
QT_MOC_LITERAL(11, 138, 4), // "type"
QT_MOC_LITERAL(12, 143, 36), // "QQmlProfilerDefinitions::Bind..."
QT_MOC_LITERAL(13, 180, 11), // "bindingType"
QT_MOC_LITERAL(14, 192, 9), // "startTime"
QT_MOC_LITERAL(15, 202, 8), // "duration"
QT_MOC_LITERAL(16, 211, 4), // "data"
QT_MOC_LITERAL(17, 216, 16), // "QmlEventLocation"
QT_MOC_LITERAL(18, 233, 8), // "location"
QT_MOC_LITERAL(19, 242, 10), // "addV8Event"
QT_MOC_LITERAL(20, 253, 5), // "depth"
QT_MOC_LITERAL(21, 259, 8), // "function"
QT_MOC_LITERAL(22, 268, 8), // "filename"
QT_MOC_LITERAL(23, 277, 10), // "lineNumber"
QT_MOC_LITERAL(24, 288, 9), // "totalTime"
QT_MOC_LITERAL(25, 298, 8), // "selfTime"
QT_MOC_LITERAL(26, 307, 13), // "addFrameEvent"
QT_MOC_LITERAL(27, 321, 9), // "framerate"
QT_MOC_LITERAL(28, 331, 14), // "animationcount"
QT_MOC_LITERAL(29, 346, 8), // "threadId"
QT_MOC_LITERAL(30, 355, 23), // "addSceneGraphFrameEvent"
QT_MOC_LITERAL(31, 379, 44), // "QQmlProfilerDefinitions::Scen..."
QT_MOC_LITERAL(32, 424, 12), // "numericData1"
QT_MOC_LITERAL(33, 437, 12), // "numericData2"
QT_MOC_LITERAL(34, 450, 12), // "numericData3"
QT_MOC_LITERAL(35, 463, 12), // "numericData4"
QT_MOC_LITERAL(36, 476, 12), // "numericData5"
QT_MOC_LITERAL(37, 489, 19), // "addPixmapCacheEvent"
QT_MOC_LITERAL(38, 509, 40), // "QQmlProfilerDefinitions::Pixm..."
QT_MOC_LITERAL(39, 550, 5), // "width"
QT_MOC_LITERAL(40, 556, 6), // "height"
QT_MOC_LITERAL(41, 563, 8), // "refcount"
QT_MOC_LITERAL(42, 572, 14), // "addMemoryEvent"
QT_MOC_LITERAL(43, 587, 35), // "QQmlProfilerDefinitions::Memo..."
QT_MOC_LITERAL(44, 623, 4), // "size"
QT_MOC_LITERAL(45, 628, 13), // "addInputEvent"
QT_MOC_LITERAL(46, 642, 34), // "QQmlProfilerDefinitions::Even..."
QT_MOC_LITERAL(47, 677, 8), // "complete"
QT_MOC_LITERAL(48, 686, 4) // "save"

    },
    "QmlProfilerData\0error\0\0stateChanged\0"
    "dataReady\0clear\0setTraceEndTime\0time\0"
    "setTraceStartTime\0addQmlEvent\0"
    "QQmlProfilerDefinitions::RangeType\0"
    "type\0QQmlProfilerDefinitions::BindingType\0"
    "bindingType\0startTime\0duration\0data\0"
    "QmlEventLocation\0location\0addV8Event\0"
    "depth\0function\0filename\0lineNumber\0"
    "totalTime\0selfTime\0addFrameEvent\0"
    "framerate\0animationcount\0threadId\0"
    "addSceneGraphFrameEvent\0"
    "QQmlProfilerDefinitions::SceneGraphFrameType\0"
    "numericData1\0numericData2\0numericData3\0"
    "numericData4\0numericData5\0addPixmapCacheEvent\0"
    "QQmlProfilerDefinitions::PixmapEventType\0"
    "width\0height\0refcount\0addMemoryEvent\0"
    "QQmlProfilerDefinitions::MemoryType\0"
    "size\0addInputEvent\0"
    "QQmlProfilerDefinitions::EventType\0"
    "complete\0save"
};
#undef QT_MOC_LITERAL

static const uint qt_meta_data_QmlProfilerData[] = {

 // content:
       7,       // revision
       0,       // classname
       0,    0, // classinfo
      15,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       3,       // signalCount

 // signals: name, argc, parameters, tag, flags
       1,    1,   89,    2, 0x06 /* Public */,
       3,    0,   92,    2, 0x06 /* Public */,
       4,    0,   93,    2, 0x06 /* Public */,

 // slots: name, argc, parameters, tag, flags
       5,    0,   94,    2, 0x0a /* Public */,
       6,    1,   95,    2, 0x0a /* Public */,
       8,    1,   98,    2, 0x0a /* Public */,
       9,    6,  101,    2, 0x0a /* Public */,
      19,    6,  114,    2, 0x0a /* Public */,
      26,    4,  127,    2, 0x0a /* Public */,
      30,    7,  136,    2, 0x0a /* Public */,
      37,    6,  151,    2, 0x0a /* Public */,
      42,    3,  164,    2, 0x0a /* Public */,
      45,    2,  171,    2, 0x0a /* Public */,
      47,    0,  176,    2, 0x0a /* Public */,
      48,    1,  177,    2, 0x0a /* Public */,

 // signals: parameters
    QMetaType::Void, QMetaType::QString,    2,
    QMetaType::Void,
    QMetaType::Void,

 // slots: parameters
    QMetaType::Void,
    QMetaType::Void, QMetaType::LongLong,    7,
    QMetaType::Void, QMetaType::LongLong,    7,
    QMetaType::Void, 0x80000000 | 10, 0x80000000 | 12, QMetaType::LongLong, QMetaType::LongLong, QMetaType::QStringList, 0x80000000 | 17,   11,   13,   14,   15,   16,   18,
    QMetaType::Void, QMetaType::Int, QMetaType::QString, QMetaType::QString, QMetaType::Int, QMetaType::Double, QMetaType::Double,   20,   21,   22,   23,   24,   25,
    QMetaType::Void, QMetaType::LongLong, QMetaType::Int, QMetaType::Int, QMetaType::Int,    7,   27,   28,   29,
    QMetaType::Void, 0x80000000 | 31, QMetaType::LongLong, QMetaType::LongLong, QMetaType::LongLong, QMetaType::LongLong, QMetaType::LongLong, QMetaType::LongLong,   11,    7,   32,   33,   34,   35,   36,
    QMetaType::Void, 0x80000000 | 38, QMetaType::LongLong, 0x80000000 | 17, QMetaType::Int, QMetaType::Int, QMetaType::Int,   11,    7,   18,   39,   40,   41,
    QMetaType::Void, 0x80000000 | 43, QMetaType::LongLong, QMetaType::LongLong,   11,    7,   44,
    QMetaType::Void, 0x80000000 | 46, QMetaType::LongLong,   11,    7,
    QMetaType::Void,
    QMetaType::Bool, QMetaType::QString,   22,

       0        // eod
};

void QmlProfilerData::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        QmlProfilerData *_t = static_cast<QmlProfilerData *>(_o);
        Q_UNUSED(_t)
        switch (_id) {
        case 0: _t->error((*reinterpret_cast< QString(*)>(_a[1]))); break;
        case 1: _t->stateChanged(); break;
        case 2: _t->dataReady(); break;
        case 3: _t->clear(); break;
        case 4: _t->setTraceEndTime((*reinterpret_cast< qint64(*)>(_a[1]))); break;
        case 5: _t->setTraceStartTime((*reinterpret_cast< qint64(*)>(_a[1]))); break;
        case 6: _t->addQmlEvent((*reinterpret_cast< QQmlProfilerDefinitions::RangeType(*)>(_a[1])),(*reinterpret_cast< QQmlProfilerDefinitions::BindingType(*)>(_a[2])),(*reinterpret_cast< qint64(*)>(_a[3])),(*reinterpret_cast< qint64(*)>(_a[4])),(*reinterpret_cast< const QStringList(*)>(_a[5])),(*reinterpret_cast< const QmlEventLocation(*)>(_a[6]))); break;
        case 7: _t->addV8Event((*reinterpret_cast< int(*)>(_a[1])),(*reinterpret_cast< const QString(*)>(_a[2])),(*reinterpret_cast< const QString(*)>(_a[3])),(*reinterpret_cast< int(*)>(_a[4])),(*reinterpret_cast< double(*)>(_a[5])),(*reinterpret_cast< double(*)>(_a[6]))); break;
        case 8: _t->addFrameEvent((*reinterpret_cast< qint64(*)>(_a[1])),(*reinterpret_cast< int(*)>(_a[2])),(*reinterpret_cast< int(*)>(_a[3])),(*reinterpret_cast< int(*)>(_a[4]))); break;
        case 9: _t->addSceneGraphFrameEvent((*reinterpret_cast< QQmlProfilerDefinitions::SceneGraphFrameType(*)>(_a[1])),(*reinterpret_cast< qint64(*)>(_a[2])),(*reinterpret_cast< qint64(*)>(_a[3])),(*reinterpret_cast< qint64(*)>(_a[4])),(*reinterpret_cast< qint64(*)>(_a[5])),(*reinterpret_cast< qint64(*)>(_a[6])),(*reinterpret_cast< qint64(*)>(_a[7]))); break;
        case 10: _t->addPixmapCacheEvent((*reinterpret_cast< QQmlProfilerDefinitions::PixmapEventType(*)>(_a[1])),(*reinterpret_cast< qint64(*)>(_a[2])),(*reinterpret_cast< const QmlEventLocation(*)>(_a[3])),(*reinterpret_cast< int(*)>(_a[4])),(*reinterpret_cast< int(*)>(_a[5])),(*reinterpret_cast< int(*)>(_a[6]))); break;
        case 11: _t->addMemoryEvent((*reinterpret_cast< QQmlProfilerDefinitions::MemoryType(*)>(_a[1])),(*reinterpret_cast< qint64(*)>(_a[2])),(*reinterpret_cast< qint64(*)>(_a[3]))); break;
        case 12: _t->addInputEvent((*reinterpret_cast< QQmlProfilerDefinitions::EventType(*)>(_a[1])),(*reinterpret_cast< qint64(*)>(_a[2]))); break;
        case 13: _t->complete(); break;
        case 14: { bool _r = _t->save((*reinterpret_cast< const QString(*)>(_a[1])));
            if (_a[0]) *reinterpret_cast< bool*>(_a[0]) = _r; }  break;
        default: ;
        }
    } else if (_c == QMetaObject::IndexOfMethod) {
        int *result = reinterpret_cast<int *>(_a[0]);
        void **func = reinterpret_cast<void **>(_a[1]);
        {
            typedef void (QmlProfilerData::*_t)(QString );
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&QmlProfilerData::error)) {
                *result = 0;
                return;
            }
        }
        {
            typedef void (QmlProfilerData::*_t)();
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&QmlProfilerData::stateChanged)) {
                *result = 1;
                return;
            }
        }
        {
            typedef void (QmlProfilerData::*_t)();
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&QmlProfilerData::dataReady)) {
                *result = 2;
                return;
            }
        }
    }
}

const QMetaObject QmlProfilerData::staticMetaObject = {
    { &QObject::staticMetaObject, qt_meta_stringdata_QmlProfilerData.data,
      qt_meta_data_QmlProfilerData,  qt_static_metacall, Q_NULLPTR, Q_NULLPTR}
};


const QMetaObject *QmlProfilerData::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->dynamicMetaObject() : &staticMetaObject;
}

void *QmlProfilerData::qt_metacast(const char *_clname)
{
    if (!_clname) return Q_NULLPTR;
    if (!strcmp(_clname, qt_meta_stringdata_QmlProfilerData.stringdata0))
        return static_cast<void*>(const_cast< QmlProfilerData*>(this));
    return QObject::qt_metacast(_clname);
}

int QmlProfilerData::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QObject::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 15)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 15;
    } else if (_c == QMetaObject::RegisterMethodArgumentMetaType) {
        if (_id < 15)
            *reinterpret_cast<int*>(_a[0]) = -1;
        _id -= 15;
    }
    return _id;
}

// SIGNAL 0
void QmlProfilerData::error(QString _t1)
{
    void *_a[] = { Q_NULLPTR, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 0, _a);
}

// SIGNAL 1
void QmlProfilerData::stateChanged()
{
    QMetaObject::activate(this, &staticMetaObject, 1, Q_NULLPTR);
}

// SIGNAL 2
void QmlProfilerData::dataReady()
{
    QMetaObject::activate(this, &staticMetaObject, 2, Q_NULLPTR);
}
QT_END_MOC_NAMESPACE
