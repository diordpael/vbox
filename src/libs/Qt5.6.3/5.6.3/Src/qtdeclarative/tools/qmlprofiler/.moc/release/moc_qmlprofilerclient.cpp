/****************************************************************************
** Meta object code from reading C++ file 'qmlprofilerclient.h'
**
** Created by: The Qt Meta Object Compiler version 67 (Qt 5.6.3)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "../../qmlprofilerclient.h"
#include <QtCore/qbytearray.h>
#include <QtCore/qmetatype.h>
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'qmlprofilerclient.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 67
#error "This file was generated using the moc from 5.6.3. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
struct qt_meta_stringdata_ProfilerClient_t {
    QByteArrayData data[7];
    char stringdata0[66];
};
#define QT_MOC_LITERAL(idx, ofs, len) \
    Q_STATIC_BYTE_ARRAY_DATA_HEADER_INITIALIZER_WITH_OFFSET(len, \
    qptrdiff(offsetof(qt_meta_stringdata_ProfilerClient_t, stringdata0) + ofs \
        - idx * sizeof(QByteArrayData)) \
    )
static const qt_meta_stringdata_ProfilerClient_t qt_meta_stringdata_ProfilerClient = {
    {
QT_MOC_LITERAL(0, 0, 14), // "ProfilerClient"
QT_MOC_LITERAL(1, 15, 8), // "complete"
QT_MOC_LITERAL(2, 24, 0), // ""
QT_MOC_LITERAL(3, 25, 14), // "enabledChanged"
QT_MOC_LITERAL(4, 40, 7), // "cleared"
QT_MOC_LITERAL(5, 48, 9), // "clearData"
QT_MOC_LITERAL(6, 58, 7) // "enabled"

    },
    "ProfilerClient\0complete\0\0enabledChanged\0"
    "cleared\0clearData\0enabled"
};
#undef QT_MOC_LITERAL

static const uint qt_meta_data_ProfilerClient[] = {

 // content:
       7,       // revision
       0,       // classname
       0,    0, // classinfo
       4,   14, // methods
       1,   38, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       3,       // signalCount

 // signals: name, argc, parameters, tag, flags
       1,    0,   34,    2, 0x06 /* Public */,
       3,    0,   35,    2, 0x06 /* Public */,
       4,    0,   36,    2, 0x06 /* Public */,

 // slots: name, argc, parameters, tag, flags
       5,    0,   37,    2, 0x0a /* Public */,

 // signals: parameters
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,

 // slots: parameters
    QMetaType::Void,

 // properties: name, type, flags
       6, QMetaType::Bool, 0x00495001,

 // properties: notify_signal_id
       1,

       0        // eod
};

void ProfilerClient::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        ProfilerClient *_t = static_cast<ProfilerClient *>(_o);
        Q_UNUSED(_t)
        switch (_id) {
        case 0: _t->complete(); break;
        case 1: _t->enabledChanged(); break;
        case 2: _t->cleared(); break;
        case 3: _t->clearData(); break;
        default: ;
        }
    } else if (_c == QMetaObject::IndexOfMethod) {
        int *result = reinterpret_cast<int *>(_a[0]);
        void **func = reinterpret_cast<void **>(_a[1]);
        {
            typedef void (ProfilerClient::*_t)();
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&ProfilerClient::complete)) {
                *result = 0;
                return;
            }
        }
        {
            typedef void (ProfilerClient::*_t)();
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&ProfilerClient::enabledChanged)) {
                *result = 1;
                return;
            }
        }
        {
            typedef void (ProfilerClient::*_t)();
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&ProfilerClient::cleared)) {
                *result = 2;
                return;
            }
        }
    }
#ifndef QT_NO_PROPERTIES
    else if (_c == QMetaObject::ReadProperty) {
        ProfilerClient *_t = static_cast<ProfilerClient *>(_o);
        Q_UNUSED(_t)
        void *_v = _a[0];
        switch (_id) {
        case 0: *reinterpret_cast< bool*>(_v) = _t->isEnabled(); break;
        default: break;
        }
    } else if (_c == QMetaObject::WriteProperty) {
    } else if (_c == QMetaObject::ResetProperty) {
    }
#endif // QT_NO_PROPERTIES
    Q_UNUSED(_a);
}

const QMetaObject ProfilerClient::staticMetaObject = {
    { &QQmlDebugClient::staticMetaObject, qt_meta_stringdata_ProfilerClient.data,
      qt_meta_data_ProfilerClient,  qt_static_metacall, Q_NULLPTR, Q_NULLPTR}
};


const QMetaObject *ProfilerClient::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->dynamicMetaObject() : &staticMetaObject;
}

void *ProfilerClient::qt_metacast(const char *_clname)
{
    if (!_clname) return Q_NULLPTR;
    if (!strcmp(_clname, qt_meta_stringdata_ProfilerClient.stringdata0))
        return static_cast<void*>(const_cast< ProfilerClient*>(this));
    return QQmlDebugClient::qt_metacast(_clname);
}

int ProfilerClient::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QQmlDebugClient::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 4)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 4;
    } else if (_c == QMetaObject::RegisterMethodArgumentMetaType) {
        if (_id < 4)
            *reinterpret_cast<int*>(_a[0]) = -1;
        _id -= 4;
    }
#ifndef QT_NO_PROPERTIES
   else if (_c == QMetaObject::ReadProperty || _c == QMetaObject::WriteProperty
            || _c == QMetaObject::ResetProperty || _c == QMetaObject::RegisterPropertyMetaType) {
        qt_static_metacall(this, _c, _id, _a);
        _id -= 1;
    } else if (_c == QMetaObject::QueryPropertyDesignable) {
        _id -= 1;
    } else if (_c == QMetaObject::QueryPropertyScriptable) {
        _id -= 1;
    } else if (_c == QMetaObject::QueryPropertyStored) {
        _id -= 1;
    } else if (_c == QMetaObject::QueryPropertyEditable) {
        _id -= 1;
    } else if (_c == QMetaObject::QueryPropertyUser) {
        _id -= 1;
    }
#endif // QT_NO_PROPERTIES
    return _id;
}

// SIGNAL 0
void ProfilerClient::complete()
{
    QMetaObject::activate(this, &staticMetaObject, 0, Q_NULLPTR);
}

// SIGNAL 1
void ProfilerClient::enabledChanged()
{
    QMetaObject::activate(this, &staticMetaObject, 1, Q_NULLPTR);
}

// SIGNAL 2
void ProfilerClient::cleared()
{
    QMetaObject::activate(this, &staticMetaObject, 2, Q_NULLPTR);
}
struct qt_meta_stringdata_QmlProfilerClient_t {
    QByteArrayData data[39];
    char stringdata0[578];
};
#define QT_MOC_LITERAL(idx, ofs, len) \
    Q_STATIC_BYTE_ARRAY_DATA_HEADER_INITIALIZER_WITH_OFFSET(len, \
    qptrdiff(offsetof(qt_meta_stringdata_QmlProfilerClient_t, stringdata0) + ofs \
        - idx * sizeof(QByteArrayData)) \
    )
static const qt_meta_stringdata_QmlProfilerClient_t qt_meta_stringdata_QmlProfilerClient = {
    {
QT_MOC_LITERAL(0, 0, 17), // "QmlProfilerClient"
QT_MOC_LITERAL(1, 18, 13), // "traceFinished"
QT_MOC_LITERAL(2, 32, 0), // ""
QT_MOC_LITERAL(3, 33, 4), // "time"
QT_MOC_LITERAL(4, 38, 12), // "traceStarted"
QT_MOC_LITERAL(5, 51, 5), // "range"
QT_MOC_LITERAL(6, 57, 34), // "QQmlProfilerDefinitions::Rang..."
QT_MOC_LITERAL(7, 92, 4), // "type"
QT_MOC_LITERAL(8, 97, 36), // "QQmlProfilerDefinitions::Bind..."
QT_MOC_LITERAL(9, 134, 11), // "bindingType"
QT_MOC_LITERAL(10, 146, 9), // "startTime"
QT_MOC_LITERAL(11, 156, 6), // "length"
QT_MOC_LITERAL(12, 163, 4), // "data"
QT_MOC_LITERAL(13, 168, 16), // "QmlEventLocation"
QT_MOC_LITERAL(14, 185, 8), // "location"
QT_MOC_LITERAL(15, 194, 5), // "frame"
QT_MOC_LITERAL(16, 200, 9), // "frameRate"
QT_MOC_LITERAL(17, 210, 14), // "animationCount"
QT_MOC_LITERAL(18, 225, 8), // "threadId"
QT_MOC_LITERAL(19, 234, 15), // "sceneGraphFrame"
QT_MOC_LITERAL(20, 250, 44), // "QQmlProfilerDefinitions::Scen..."
QT_MOC_LITERAL(21, 295, 12), // "numericData1"
QT_MOC_LITERAL(22, 308, 12), // "numericData2"
QT_MOC_LITERAL(23, 321, 12), // "numericData3"
QT_MOC_LITERAL(24, 334, 12), // "numericData4"
QT_MOC_LITERAL(25, 347, 12), // "numericData5"
QT_MOC_LITERAL(26, 360, 11), // "pixmapCache"
QT_MOC_LITERAL(27, 372, 40), // "QQmlProfilerDefinitions::Pixm..."
QT_MOC_LITERAL(28, 413, 5), // "width"
QT_MOC_LITERAL(29, 419, 6), // "height"
QT_MOC_LITERAL(30, 426, 8), // "refCount"
QT_MOC_LITERAL(31, 435, 16), // "memoryAllocation"
QT_MOC_LITERAL(32, 452, 35), // "QQmlProfilerDefinitions::Memo..."
QT_MOC_LITERAL(33, 488, 6), // "amount"
QT_MOC_LITERAL(34, 495, 10), // "inputEvent"
QT_MOC_LITERAL(35, 506, 34), // "QQmlProfilerDefinitions::Even..."
QT_MOC_LITERAL(36, 541, 9), // "clearData"
QT_MOC_LITERAL(37, 551, 19), // "sendRecordingStatus"
QT_MOC_LITERAL(38, 571, 6) // "record"

    },
    "QmlProfilerClient\0traceFinished\0\0time\0"
    "traceStarted\0range\0"
    "QQmlProfilerDefinitions::RangeType\0"
    "type\0QQmlProfilerDefinitions::BindingType\0"
    "bindingType\0startTime\0length\0data\0"
    "QmlEventLocation\0location\0frame\0"
    "frameRate\0animationCount\0threadId\0"
    "sceneGraphFrame\0"
    "QQmlProfilerDefinitions::SceneGraphFrameType\0"
    "numericData1\0numericData2\0numericData3\0"
    "numericData4\0numericData5\0pixmapCache\0"
    "QQmlProfilerDefinitions::PixmapEventType\0"
    "width\0height\0refCount\0memoryAllocation\0"
    "QQmlProfilerDefinitions::MemoryType\0"
    "amount\0inputEvent\0QQmlProfilerDefinitions::EventType\0"
    "clearData\0sendRecordingStatus\0record"
};
#undef QT_MOC_LITERAL

static const uint qt_meta_data_QmlProfilerClient[] = {

 // content:
       7,       // revision
       0,       // classname
       0,    0, // classinfo
      10,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       8,       // signalCount

 // signals: name, argc, parameters, tag, flags
       1,    1,   64,    2, 0x06 /* Public */,
       4,    1,   67,    2, 0x06 /* Public */,
       5,    6,   70,    2, 0x06 /* Public */,
      15,    4,   83,    2, 0x06 /* Public */,
      19,    7,   92,    2, 0x06 /* Public */,
      26,    6,  107,    2, 0x06 /* Public */,
      31,    3,  120,    2, 0x06 /* Public */,
      34,    2,  127,    2, 0x06 /* Public */,

 // slots: name, argc, parameters, tag, flags
      36,    0,  132,    2, 0x0a /* Public */,
      37,    1,  133,    2, 0x0a /* Public */,

 // signals: parameters
    QMetaType::Void, QMetaType::LongLong,    3,
    QMetaType::Void, QMetaType::LongLong,    3,
    QMetaType::Void, 0x80000000 | 6, 0x80000000 | 8, QMetaType::LongLong, QMetaType::LongLong, QMetaType::QStringList, 0x80000000 | 13,    7,    9,   10,   11,   12,   14,
    QMetaType::Void, QMetaType::LongLong, QMetaType::Int, QMetaType::Int, QMetaType::Int,    3,   16,   17,   18,
    QMetaType::Void, 0x80000000 | 20, QMetaType::LongLong, QMetaType::LongLong, QMetaType::LongLong, QMetaType::LongLong, QMetaType::LongLong, QMetaType::LongLong,    7,    3,   21,   22,   23,   24,   25,
    QMetaType::Void, 0x80000000 | 27, QMetaType::LongLong, 0x80000000 | 13, QMetaType::Int, QMetaType::Int, QMetaType::Int,    2,    3,   14,   28,   29,   30,
    QMetaType::Void, 0x80000000 | 32, QMetaType::LongLong, QMetaType::LongLong,    7,    3,   33,
    QMetaType::Void, 0x80000000 | 35, QMetaType::LongLong,    2,    3,

 // slots: parameters
    QMetaType::Void,
    QMetaType::Void, QMetaType::Bool,   38,

       0        // eod
};

void QmlProfilerClient::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        QmlProfilerClient *_t = static_cast<QmlProfilerClient *>(_o);
        Q_UNUSED(_t)
        switch (_id) {
        case 0: _t->traceFinished((*reinterpret_cast< qint64(*)>(_a[1]))); break;
        case 1: _t->traceStarted((*reinterpret_cast< qint64(*)>(_a[1]))); break;
        case 2: _t->range((*reinterpret_cast< QQmlProfilerDefinitions::RangeType(*)>(_a[1])),(*reinterpret_cast< QQmlProfilerDefinitions::BindingType(*)>(_a[2])),(*reinterpret_cast< qint64(*)>(_a[3])),(*reinterpret_cast< qint64(*)>(_a[4])),(*reinterpret_cast< const QStringList(*)>(_a[5])),(*reinterpret_cast< const QmlEventLocation(*)>(_a[6]))); break;
        case 3: _t->frame((*reinterpret_cast< qint64(*)>(_a[1])),(*reinterpret_cast< int(*)>(_a[2])),(*reinterpret_cast< int(*)>(_a[3])),(*reinterpret_cast< int(*)>(_a[4]))); break;
        case 4: _t->sceneGraphFrame((*reinterpret_cast< QQmlProfilerDefinitions::SceneGraphFrameType(*)>(_a[1])),(*reinterpret_cast< qint64(*)>(_a[2])),(*reinterpret_cast< qint64(*)>(_a[3])),(*reinterpret_cast< qint64(*)>(_a[4])),(*reinterpret_cast< qint64(*)>(_a[5])),(*reinterpret_cast< qint64(*)>(_a[6])),(*reinterpret_cast< qint64(*)>(_a[7]))); break;
        case 5: _t->pixmapCache((*reinterpret_cast< QQmlProfilerDefinitions::PixmapEventType(*)>(_a[1])),(*reinterpret_cast< qint64(*)>(_a[2])),(*reinterpret_cast< const QmlEventLocation(*)>(_a[3])),(*reinterpret_cast< int(*)>(_a[4])),(*reinterpret_cast< int(*)>(_a[5])),(*reinterpret_cast< int(*)>(_a[6]))); break;
        case 6: _t->memoryAllocation((*reinterpret_cast< QQmlProfilerDefinitions::MemoryType(*)>(_a[1])),(*reinterpret_cast< qint64(*)>(_a[2])),(*reinterpret_cast< qint64(*)>(_a[3]))); break;
        case 7: _t->inputEvent((*reinterpret_cast< QQmlProfilerDefinitions::EventType(*)>(_a[1])),(*reinterpret_cast< qint64(*)>(_a[2]))); break;
        case 8: _t->clearData(); break;
        case 9: _t->sendRecordingStatus((*reinterpret_cast< bool(*)>(_a[1]))); break;
        default: ;
        }
    } else if (_c == QMetaObject::IndexOfMethod) {
        int *result = reinterpret_cast<int *>(_a[0]);
        void **func = reinterpret_cast<void **>(_a[1]);
        {
            typedef void (QmlProfilerClient::*_t)(qint64 );
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&QmlProfilerClient::traceFinished)) {
                *result = 0;
                return;
            }
        }
        {
            typedef void (QmlProfilerClient::*_t)(qint64 );
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&QmlProfilerClient::traceStarted)) {
                *result = 1;
                return;
            }
        }
        {
            typedef void (QmlProfilerClient::*_t)(QQmlProfilerDefinitions::RangeType , QQmlProfilerDefinitions::BindingType , qint64 , qint64 , const QStringList & , const QmlEventLocation & );
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&QmlProfilerClient::range)) {
                *result = 2;
                return;
            }
        }
        {
            typedef void (QmlProfilerClient::*_t)(qint64 , int , int , int );
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&QmlProfilerClient::frame)) {
                *result = 3;
                return;
            }
        }
        {
            typedef void (QmlProfilerClient::*_t)(QQmlProfilerDefinitions::SceneGraphFrameType , qint64 , qint64 , qint64 , qint64 , qint64 , qint64 );
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&QmlProfilerClient::sceneGraphFrame)) {
                *result = 4;
                return;
            }
        }
        {
            typedef void (QmlProfilerClient::*_t)(QQmlProfilerDefinitions::PixmapEventType , qint64 , const QmlEventLocation & , int , int , int );
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&QmlProfilerClient::pixmapCache)) {
                *result = 5;
                return;
            }
        }
        {
            typedef void (QmlProfilerClient::*_t)(QQmlProfilerDefinitions::MemoryType , qint64 , qint64 );
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&QmlProfilerClient::memoryAllocation)) {
                *result = 6;
                return;
            }
        }
        {
            typedef void (QmlProfilerClient::*_t)(QQmlProfilerDefinitions::EventType , qint64 );
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&QmlProfilerClient::inputEvent)) {
                *result = 7;
                return;
            }
        }
    }
}

const QMetaObject QmlProfilerClient::staticMetaObject = {
    { &ProfilerClient::staticMetaObject, qt_meta_stringdata_QmlProfilerClient.data,
      qt_meta_data_QmlProfilerClient,  qt_static_metacall, Q_NULLPTR, Q_NULLPTR}
};


const QMetaObject *QmlProfilerClient::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->dynamicMetaObject() : &staticMetaObject;
}

void *QmlProfilerClient::qt_metacast(const char *_clname)
{
    if (!_clname) return Q_NULLPTR;
    if (!strcmp(_clname, qt_meta_stringdata_QmlProfilerClient.stringdata0))
        return static_cast<void*>(const_cast< QmlProfilerClient*>(this));
    return ProfilerClient::qt_metacast(_clname);
}

int QmlProfilerClient::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = ProfilerClient::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 10)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 10;
    } else if (_c == QMetaObject::RegisterMethodArgumentMetaType) {
        if (_id < 10)
            *reinterpret_cast<int*>(_a[0]) = -1;
        _id -= 10;
    }
    return _id;
}

// SIGNAL 0
void QmlProfilerClient::traceFinished(qint64 _t1)
{
    void *_a[] = { Q_NULLPTR, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 0, _a);
}

// SIGNAL 1
void QmlProfilerClient::traceStarted(qint64 _t1)
{
    void *_a[] = { Q_NULLPTR, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 1, _a);
}

// SIGNAL 2
void QmlProfilerClient::range(QQmlProfilerDefinitions::RangeType _t1, QQmlProfilerDefinitions::BindingType _t2, qint64 _t3, qint64 _t4, const QStringList & _t5, const QmlEventLocation & _t6)
{
    void *_a[] = { Q_NULLPTR, const_cast<void*>(reinterpret_cast<const void*>(&_t1)), const_cast<void*>(reinterpret_cast<const void*>(&_t2)), const_cast<void*>(reinterpret_cast<const void*>(&_t3)), const_cast<void*>(reinterpret_cast<const void*>(&_t4)), const_cast<void*>(reinterpret_cast<const void*>(&_t5)), const_cast<void*>(reinterpret_cast<const void*>(&_t6)) };
    QMetaObject::activate(this, &staticMetaObject, 2, _a);
}

// SIGNAL 3
void QmlProfilerClient::frame(qint64 _t1, int _t2, int _t3, int _t4)
{
    void *_a[] = { Q_NULLPTR, const_cast<void*>(reinterpret_cast<const void*>(&_t1)), const_cast<void*>(reinterpret_cast<const void*>(&_t2)), const_cast<void*>(reinterpret_cast<const void*>(&_t3)), const_cast<void*>(reinterpret_cast<const void*>(&_t4)) };
    QMetaObject::activate(this, &staticMetaObject, 3, _a);
}

// SIGNAL 4
void QmlProfilerClient::sceneGraphFrame(QQmlProfilerDefinitions::SceneGraphFrameType _t1, qint64 _t2, qint64 _t3, qint64 _t4, qint64 _t5, qint64 _t6, qint64 _t7)
{
    void *_a[] = { Q_NULLPTR, const_cast<void*>(reinterpret_cast<const void*>(&_t1)), const_cast<void*>(reinterpret_cast<const void*>(&_t2)), const_cast<void*>(reinterpret_cast<const void*>(&_t3)), const_cast<void*>(reinterpret_cast<const void*>(&_t4)), const_cast<void*>(reinterpret_cast<const void*>(&_t5)), const_cast<void*>(reinterpret_cast<const void*>(&_t6)), const_cast<void*>(reinterpret_cast<const void*>(&_t7)) };
    QMetaObject::activate(this, &staticMetaObject, 4, _a);
}

// SIGNAL 5
void QmlProfilerClient::pixmapCache(QQmlProfilerDefinitions::PixmapEventType _t1, qint64 _t2, const QmlEventLocation & _t3, int _t4, int _t5, int _t6)
{
    void *_a[] = { Q_NULLPTR, const_cast<void*>(reinterpret_cast<const void*>(&_t1)), const_cast<void*>(reinterpret_cast<const void*>(&_t2)), const_cast<void*>(reinterpret_cast<const void*>(&_t3)), const_cast<void*>(reinterpret_cast<const void*>(&_t4)), const_cast<void*>(reinterpret_cast<const void*>(&_t5)), const_cast<void*>(reinterpret_cast<const void*>(&_t6)) };
    QMetaObject::activate(this, &staticMetaObject, 5, _a);
}

// SIGNAL 6
void QmlProfilerClient::memoryAllocation(QQmlProfilerDefinitions::MemoryType _t1, qint64 _t2, qint64 _t3)
{
    void *_a[] = { Q_NULLPTR, const_cast<void*>(reinterpret_cast<const void*>(&_t1)), const_cast<void*>(reinterpret_cast<const void*>(&_t2)), const_cast<void*>(reinterpret_cast<const void*>(&_t3)) };
    QMetaObject::activate(this, &staticMetaObject, 6, _a);
}

// SIGNAL 7
void QmlProfilerClient::inputEvent(QQmlProfilerDefinitions::EventType _t1, qint64 _t2)
{
    void *_a[] = { Q_NULLPTR, const_cast<void*>(reinterpret_cast<const void*>(&_t1)), const_cast<void*>(reinterpret_cast<const void*>(&_t2)) };
    QMetaObject::activate(this, &staticMetaObject, 7, _a);
}
struct qt_meta_stringdata_V8ProfilerClient_t {
    QByteArrayData data[11];
    char stringdata0[105];
};
#define QT_MOC_LITERAL(idx, ofs, len) \
    Q_STATIC_BYTE_ARRAY_DATA_HEADER_INITIALIZER_WITH_OFFSET(len, \
    qptrdiff(offsetof(qt_meta_stringdata_V8ProfilerClient_t, stringdata0) + ofs \
        - idx * sizeof(QByteArrayData)) \
    )
static const qt_meta_stringdata_V8ProfilerClient_t qt_meta_stringdata_V8ProfilerClient = {
    {
QT_MOC_LITERAL(0, 0, 16), // "V8ProfilerClient"
QT_MOC_LITERAL(1, 17, 5), // "range"
QT_MOC_LITERAL(2, 23, 0), // ""
QT_MOC_LITERAL(3, 24, 5), // "depth"
QT_MOC_LITERAL(4, 30, 8), // "function"
QT_MOC_LITERAL(5, 39, 8), // "filename"
QT_MOC_LITERAL(6, 48, 10), // "lineNumber"
QT_MOC_LITERAL(7, 59, 9), // "totalTime"
QT_MOC_LITERAL(8, 69, 8), // "selfTime"
QT_MOC_LITERAL(9, 78, 19), // "sendRecordingStatus"
QT_MOC_LITERAL(10, 98, 6) // "record"

    },
    "V8ProfilerClient\0range\0\0depth\0function\0"
    "filename\0lineNumber\0totalTime\0selfTime\0"
    "sendRecordingStatus\0record"
};
#undef QT_MOC_LITERAL

static const uint qt_meta_data_V8ProfilerClient[] = {

 // content:
       7,       // revision
       0,       // classname
       0,    0, // classinfo
       2,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       1,       // signalCount

 // signals: name, argc, parameters, tag, flags
       1,    6,   24,    2, 0x06 /* Public */,

 // slots: name, argc, parameters, tag, flags
       9,    1,   37,    2, 0x0a /* Public */,

 // signals: parameters
    QMetaType::Void, QMetaType::Int, QMetaType::QString, QMetaType::QString, QMetaType::Int, QMetaType::Double, QMetaType::Double,    3,    4,    5,    6,    7,    8,

 // slots: parameters
    QMetaType::Void, QMetaType::Bool,   10,

       0        // eod
};

void V8ProfilerClient::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        V8ProfilerClient *_t = static_cast<V8ProfilerClient *>(_o);
        Q_UNUSED(_t)
        switch (_id) {
        case 0: _t->range((*reinterpret_cast< int(*)>(_a[1])),(*reinterpret_cast< const QString(*)>(_a[2])),(*reinterpret_cast< const QString(*)>(_a[3])),(*reinterpret_cast< int(*)>(_a[4])),(*reinterpret_cast< double(*)>(_a[5])),(*reinterpret_cast< double(*)>(_a[6]))); break;
        case 1: _t->sendRecordingStatus((*reinterpret_cast< bool(*)>(_a[1]))); break;
        default: ;
        }
    } else if (_c == QMetaObject::IndexOfMethod) {
        int *result = reinterpret_cast<int *>(_a[0]);
        void **func = reinterpret_cast<void **>(_a[1]);
        {
            typedef void (V8ProfilerClient::*_t)(int , const QString & , const QString & , int , double , double );
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&V8ProfilerClient::range)) {
                *result = 0;
                return;
            }
        }
    }
}

const QMetaObject V8ProfilerClient::staticMetaObject = {
    { &ProfilerClient::staticMetaObject, qt_meta_stringdata_V8ProfilerClient.data,
      qt_meta_data_V8ProfilerClient,  qt_static_metacall, Q_NULLPTR, Q_NULLPTR}
};


const QMetaObject *V8ProfilerClient::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->dynamicMetaObject() : &staticMetaObject;
}

void *V8ProfilerClient::qt_metacast(const char *_clname)
{
    if (!_clname) return Q_NULLPTR;
    if (!strcmp(_clname, qt_meta_stringdata_V8ProfilerClient.stringdata0))
        return static_cast<void*>(const_cast< V8ProfilerClient*>(this));
    return ProfilerClient::qt_metacast(_clname);
}

int V8ProfilerClient::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = ProfilerClient::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 2)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 2;
    } else if (_c == QMetaObject::RegisterMethodArgumentMetaType) {
        if (_id < 2)
            *reinterpret_cast<int*>(_a[0]) = -1;
        _id -= 2;
    }
    return _id;
}

// SIGNAL 0
void V8ProfilerClient::range(int _t1, const QString & _t2, const QString & _t3, int _t4, double _t5, double _t6)
{
    void *_a[] = { Q_NULLPTR, const_cast<void*>(reinterpret_cast<const void*>(&_t1)), const_cast<void*>(reinterpret_cast<const void*>(&_t2)), const_cast<void*>(reinterpret_cast<const void*>(&_t3)), const_cast<void*>(reinterpret_cast<const void*>(&_t4)), const_cast<void*>(reinterpret_cast<const void*>(&_t5)), const_cast<void*>(reinterpret_cast<const void*>(&_t6)) };
    QMetaObject::activate(this, &staticMetaObject, 0, _a);
}
QT_END_MOC_NAMESPACE
