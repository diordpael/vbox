/****************************************************************************
** Meta object code from reading C++ file 'qmlprofilerapplication.h'
**
** Created by: The Qt Meta Object Compiler version 67 (Qt 5.6.3)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "../../qmlprofilerapplication.h"
#include <QtCore/qbytearray.h>
#include <QtCore/qmetatype.h>
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'qmlprofilerapplication.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 67
#error "This file was generated using the moc from 5.6.3. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
struct qt_meta_stringdata_QmlProfilerApplication_t {
    QByteArrayData data[29];
    char stringdata0[381];
};
#define QT_MOC_LITERAL(idx, ofs, len) \
    Q_STATIC_BYTE_ARRAY_DATA_HEADER_INITIALIZER_WITH_OFFSET(len, \
    qptrdiff(offsetof(qt_meta_stringdata_QmlProfilerApplication_t, stringdata0) + ofs \
        - idx * sizeof(QByteArrayData)) \
    )
static const qt_meta_stringdata_QmlProfilerApplication_t qt_meta_stringdata_QmlProfilerApplication = {
    {
QT_MOC_LITERAL(0, 0, 22), // "QmlProfilerApplication"
QT_MOC_LITERAL(1, 23, 15), // "readyForCommand"
QT_MOC_LITERAL(2, 39, 0), // ""
QT_MOC_LITERAL(3, 40, 11), // "userCommand"
QT_MOC_LITERAL(4, 52, 7), // "command"
QT_MOC_LITERAL(5, 60, 18), // "notifyTraceStarted"
QT_MOC_LITERAL(6, 79, 10), // "outputData"
QT_MOC_LITERAL(7, 90, 3), // "run"
QT_MOC_LITERAL(8, 94, 12), // "tryToConnect"
QT_MOC_LITERAL(9, 107, 9), // "connected"
QT_MOC_LITERAL(10, 117, 22), // "connectionStateChanged"
QT_MOC_LITERAL(11, 140, 28), // "QAbstractSocket::SocketState"
QT_MOC_LITERAL(12, 169, 5), // "state"
QT_MOC_LITERAL(13, 175, 15), // "connectionError"
QT_MOC_LITERAL(14, 191, 28), // "QAbstractSocket::SocketError"
QT_MOC_LITERAL(15, 220, 5), // "error"
QT_MOC_LITERAL(16, 226, 16), // "processHasOutput"
QT_MOC_LITERAL(17, 243, 15), // "processFinished"
QT_MOC_LITERAL(18, 259, 18), // "traceClientEnabled"
QT_MOC_LITERAL(19, 278, 21), // "profilerClientEnabled"
QT_MOC_LITERAL(20, 300, 13), // "traceFinished"
QT_MOC_LITERAL(21, 314, 6), // "prompt"
QT_MOC_LITERAL(22, 321, 4), // "line"
QT_MOC_LITERAL(23, 326, 5), // "ready"
QT_MOC_LITERAL(24, 332, 8), // "logError"
QT_MOC_LITERAL(25, 341, 9), // "logStatus"
QT_MOC_LITERAL(26, 351, 6), // "status"
QT_MOC_LITERAL(27, 358, 11), // "qmlComplete"
QT_MOC_LITERAL(28, 370, 10) // "v8Complete"

    },
    "QmlProfilerApplication\0readyForCommand\0"
    "\0userCommand\0command\0notifyTraceStarted\0"
    "outputData\0run\0tryToConnect\0connected\0"
    "connectionStateChanged\0"
    "QAbstractSocket::SocketState\0state\0"
    "connectionError\0QAbstractSocket::SocketError\0"
    "error\0processHasOutput\0processFinished\0"
    "traceClientEnabled\0profilerClientEnabled\0"
    "traceFinished\0prompt\0line\0ready\0"
    "logError\0logStatus\0status\0qmlComplete\0"
    "v8Complete"
};
#undef QT_MOC_LITERAL

static const uint qt_meta_data_QmlProfilerApplication[] = {

 // content:
       7,       // revision
       0,       // classname
       0,    0, // classinfo
      21,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       1,       // signalCount

 // signals: name, argc, parameters, tag, flags
       1,    0,  119,    2, 0x06 /* Public */,

 // slots: name, argc, parameters, tag, flags
       3,    1,  120,    2, 0x0a /* Public */,
       5,    0,  123,    2, 0x0a /* Public */,
       6,    0,  124,    2, 0x0a /* Public */,
       7,    0,  125,    2, 0x08 /* Private */,
       8,    0,  126,    2, 0x08 /* Private */,
       9,    0,  127,    2, 0x08 /* Private */,
      10,    1,  128,    2, 0x08 /* Private */,
      13,    1,  131,    2, 0x08 /* Private */,
      16,    0,  134,    2, 0x08 /* Private */,
      17,    0,  135,    2, 0x08 /* Private */,
      18,    0,  136,    2, 0x08 /* Private */,
      19,    0,  137,    2, 0x08 /* Private */,
      20,    0,  138,    2, 0x08 /* Private */,
      21,    2,  139,    2, 0x08 /* Private */,
      21,    1,  144,    2, 0x28 /* Private | MethodCloned */,
      21,    0,  147,    2, 0x28 /* Private | MethodCloned */,
      24,    1,  148,    2, 0x08 /* Private */,
      25,    1,  151,    2, 0x08 /* Private */,
      27,    0,  154,    2, 0x08 /* Private */,
      28,    0,  155,    2, 0x08 /* Private */,

 // signals: parameters
    QMetaType::Void,

 // slots: parameters
    QMetaType::Void, QMetaType::QString,    4,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void, 0x80000000 | 11,   12,
    QMetaType::Void, 0x80000000 | 14,   15,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void, QMetaType::QString, QMetaType::Bool,   22,   23,
    QMetaType::Void, QMetaType::QString,   22,
    QMetaType::Void,
    QMetaType::Void, QMetaType::QString,   15,
    QMetaType::Void, QMetaType::QString,   26,
    QMetaType::Void,
    QMetaType::Void,

       0        // eod
};

void QmlProfilerApplication::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        QmlProfilerApplication *_t = static_cast<QmlProfilerApplication *>(_o);
        Q_UNUSED(_t)
        switch (_id) {
        case 0: _t->readyForCommand(); break;
        case 1: _t->userCommand((*reinterpret_cast< const QString(*)>(_a[1]))); break;
        case 2: _t->notifyTraceStarted(); break;
        case 3: _t->outputData(); break;
        case 4: _t->run(); break;
        case 5: _t->tryToConnect(); break;
        case 6: _t->connected(); break;
        case 7: _t->connectionStateChanged((*reinterpret_cast< QAbstractSocket::SocketState(*)>(_a[1]))); break;
        case 8: _t->connectionError((*reinterpret_cast< QAbstractSocket::SocketError(*)>(_a[1]))); break;
        case 9: _t->processHasOutput(); break;
        case 10: _t->processFinished(); break;
        case 11: _t->traceClientEnabled(); break;
        case 12: _t->profilerClientEnabled(); break;
        case 13: _t->traceFinished(); break;
        case 14: _t->prompt((*reinterpret_cast< const QString(*)>(_a[1])),(*reinterpret_cast< bool(*)>(_a[2]))); break;
        case 15: _t->prompt((*reinterpret_cast< const QString(*)>(_a[1]))); break;
        case 16: _t->prompt(); break;
        case 17: _t->logError((*reinterpret_cast< const QString(*)>(_a[1]))); break;
        case 18: _t->logStatus((*reinterpret_cast< const QString(*)>(_a[1]))); break;
        case 19: _t->qmlComplete(); break;
        case 20: _t->v8Complete(); break;
        default: ;
        }
    } else if (_c == QMetaObject::RegisterMethodArgumentMetaType) {
        switch (_id) {
        default: *reinterpret_cast<int*>(_a[0]) = -1; break;
        case 7:
            switch (*reinterpret_cast<int*>(_a[1])) {
            default: *reinterpret_cast<int*>(_a[0]) = -1; break;
            case 0:
                *reinterpret_cast<int*>(_a[0]) = qRegisterMetaType< QAbstractSocket::SocketState >(); break;
            }
            break;
        case 8:
            switch (*reinterpret_cast<int*>(_a[1])) {
            default: *reinterpret_cast<int*>(_a[0]) = -1; break;
            case 0:
                *reinterpret_cast<int*>(_a[0]) = qRegisterMetaType< QAbstractSocket::SocketError >(); break;
            }
            break;
        }
    } else if (_c == QMetaObject::IndexOfMethod) {
        int *result = reinterpret_cast<int *>(_a[0]);
        void **func = reinterpret_cast<void **>(_a[1]);
        {
            typedef void (QmlProfilerApplication::*_t)();
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&QmlProfilerApplication::readyForCommand)) {
                *result = 0;
                return;
            }
        }
    }
}

const QMetaObject QmlProfilerApplication::staticMetaObject = {
    { &QCoreApplication::staticMetaObject, qt_meta_stringdata_QmlProfilerApplication.data,
      qt_meta_data_QmlProfilerApplication,  qt_static_metacall, Q_NULLPTR, Q_NULLPTR}
};


const QMetaObject *QmlProfilerApplication::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->dynamicMetaObject() : &staticMetaObject;
}

void *QmlProfilerApplication::qt_metacast(const char *_clname)
{
    if (!_clname) return Q_NULLPTR;
    if (!strcmp(_clname, qt_meta_stringdata_QmlProfilerApplication.stringdata0))
        return static_cast<void*>(const_cast< QmlProfilerApplication*>(this));
    return QCoreApplication::qt_metacast(_clname);
}

int QmlProfilerApplication::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QCoreApplication::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 21)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 21;
    } else if (_c == QMetaObject::RegisterMethodArgumentMetaType) {
        if (_id < 21)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 21;
    }
    return _id;
}

// SIGNAL 0
void QmlProfilerApplication::readyForCommand()
{
    QMetaObject::activate(this, &staticMetaObject, 0, Q_NULLPTR);
}
QT_END_MOC_NAMESPACE
