/****************************************************************************
** Meta object code from reading C++ file 'qquickcustomparticle_p.h'
**
** Created by: The Qt Meta Object Compiler version 67 (Qt 5.6.3)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "../../qquickcustomparticle_p.h"
#include <QtCore/qbytearray.h>
#include <QtCore/qmetatype.h>
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'qquickcustomparticle_p.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 67
#error "This file was generated using the moc from 5.6.3. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
struct qt_meta_stringdata_QQuickCustomParticle_t {
    QByteArrayData data[10];
    char stringdata0[140];
};
#define QT_MOC_LITERAL(idx, ofs, len) \
    Q_STATIC_BYTE_ARRAY_DATA_HEADER_INITIALIZER_WITH_OFFSET(len, \
    qptrdiff(offsetof(qt_meta_stringdata_QQuickCustomParticle_t, stringdata0) + ofs \
        - idx * sizeof(QByteArrayData)) \
    )
static const qt_meta_stringdata_QQuickCustomParticle_t qt_meta_stringdata_QQuickCustomParticle = {
    {
QT_MOC_LITERAL(0, 0, 20), // "QQuickCustomParticle"
QT_MOC_LITERAL(1, 21, 21), // "fragmentShaderChanged"
QT_MOC_LITERAL(2, 43, 0), // ""
QT_MOC_LITERAL(3, 44, 19), // "vertexShaderChanged"
QT_MOC_LITERAL(4, 64, 15), // "sourceDestroyed"
QT_MOC_LITERAL(5, 80, 6), // "object"
QT_MOC_LITERAL(6, 87, 15), // "propertyChanged"
QT_MOC_LITERAL(7, 103, 8), // "mappedId"
QT_MOC_LITERAL(8, 112, 14), // "fragmentShader"
QT_MOC_LITERAL(9, 127, 12) // "vertexShader"

    },
    "QQuickCustomParticle\0fragmentShaderChanged\0"
    "\0vertexShaderChanged\0sourceDestroyed\0"
    "object\0propertyChanged\0mappedId\0"
    "fragmentShader\0vertexShader"
};
#undef QT_MOC_LITERAL

static const uint qt_meta_data_QQuickCustomParticle[] = {

 // content:
       7,       // revision
       0,       // classname
       0,    0, // classinfo
       4,   14, // methods
       2,   42, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       2,       // signalCount

 // signals: name, argc, parameters, tag, flags
       1,    0,   34,    2, 0x06 /* Public */,
       3,    0,   35,    2, 0x06 /* Public */,

 // slots: name, argc, parameters, tag, flags
       4,    1,   36,    2, 0x08 /* Private */,
       6,    1,   39,    2, 0x08 /* Private */,

 // signals: parameters
    QMetaType::Void,
    QMetaType::Void,

 // slots: parameters
    QMetaType::Void, QMetaType::QObjectStar,    5,
    QMetaType::Void, QMetaType::Int,    7,

 // properties: name, type, flags
       8, QMetaType::QByteArray, 0x00495103,
       9, QMetaType::QByteArray, 0x00495103,

 // properties: notify_signal_id
       0,
       1,

       0        // eod
};

void QQuickCustomParticle::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        QQuickCustomParticle *_t = static_cast<QQuickCustomParticle *>(_o);
        Q_UNUSED(_t)
        switch (_id) {
        case 0: _t->fragmentShaderChanged(); break;
        case 1: _t->vertexShaderChanged(); break;
        case 2: _t->sourceDestroyed((*reinterpret_cast< QObject*(*)>(_a[1]))); break;
        case 3: _t->propertyChanged((*reinterpret_cast< int(*)>(_a[1]))); break;
        default: ;
        }
    } else if (_c == QMetaObject::IndexOfMethod) {
        int *result = reinterpret_cast<int *>(_a[0]);
        void **func = reinterpret_cast<void **>(_a[1]);
        {
            typedef void (QQuickCustomParticle::*_t)();
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&QQuickCustomParticle::fragmentShaderChanged)) {
                *result = 0;
                return;
            }
        }
        {
            typedef void (QQuickCustomParticle::*_t)();
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&QQuickCustomParticle::vertexShaderChanged)) {
                *result = 1;
                return;
            }
        }
    }
#ifndef QT_NO_PROPERTIES
    else if (_c == QMetaObject::ReadProperty) {
        QQuickCustomParticle *_t = static_cast<QQuickCustomParticle *>(_o);
        Q_UNUSED(_t)
        void *_v = _a[0];
        switch (_id) {
        case 0: *reinterpret_cast< QByteArray*>(_v) = _t->fragmentShader(); break;
        case 1: *reinterpret_cast< QByteArray*>(_v) = _t->vertexShader(); break;
        default: break;
        }
    } else if (_c == QMetaObject::WriteProperty) {
        QQuickCustomParticle *_t = static_cast<QQuickCustomParticle *>(_o);
        Q_UNUSED(_t)
        void *_v = _a[0];
        switch (_id) {
        case 0: _t->setFragmentShader(*reinterpret_cast< QByteArray*>(_v)); break;
        case 1: _t->setVertexShader(*reinterpret_cast< QByteArray*>(_v)); break;
        default: break;
        }
    } else if (_c == QMetaObject::ResetProperty) {
    }
#endif // QT_NO_PROPERTIES
}

const QMetaObject QQuickCustomParticle::staticMetaObject = {
    { &QQuickParticlePainter::staticMetaObject, qt_meta_stringdata_QQuickCustomParticle.data,
      qt_meta_data_QQuickCustomParticle,  qt_static_metacall, Q_NULLPTR, Q_NULLPTR}
};


const QMetaObject *QQuickCustomParticle::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->dynamicMetaObject() : &staticMetaObject;
}

void *QQuickCustomParticle::qt_metacast(const char *_clname)
{
    if (!_clname) return Q_NULLPTR;
    if (!strcmp(_clname, qt_meta_stringdata_QQuickCustomParticle.stringdata0))
        return static_cast<void*>(const_cast< QQuickCustomParticle*>(this));
    return QQuickParticlePainter::qt_metacast(_clname);
}

int QQuickCustomParticle::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QQuickParticlePainter::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 4)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 4;
    } else if (_c == QMetaObject::RegisterMethodArgumentMetaType) {
        if (_id < 4)
            *reinterpret_cast<int*>(_a[0]) = -1;
        _id -= 4;
    }
#ifndef QT_NO_PROPERTIES
   else if (_c == QMetaObject::ReadProperty || _c == QMetaObject::WriteProperty
            || _c == QMetaObject::ResetProperty || _c == QMetaObject::RegisterPropertyMetaType) {
        qt_static_metacall(this, _c, _id, _a);
        _id -= 2;
    } else if (_c == QMetaObject::QueryPropertyDesignable) {
        _id -= 2;
    } else if (_c == QMetaObject::QueryPropertyScriptable) {
        _id -= 2;
    } else if (_c == QMetaObject::QueryPropertyStored) {
        _id -= 2;
    } else if (_c == QMetaObject::QueryPropertyEditable) {
        _id -= 2;
    } else if (_c == QMetaObject::QueryPropertyUser) {
        _id -= 2;
    }
#endif // QT_NO_PROPERTIES
    return _id;
}

// SIGNAL 0
void QQuickCustomParticle::fragmentShaderChanged()
{
    QMetaObject::activate(this, &staticMetaObject, 0, Q_NULLPTR);
}

// SIGNAL 1
void QQuickCustomParticle::vertexShaderChanged()
{
    QMetaObject::activate(this, &staticMetaObject, 1, Q_NULLPTR);
}
QT_END_MOC_NAMESPACE
