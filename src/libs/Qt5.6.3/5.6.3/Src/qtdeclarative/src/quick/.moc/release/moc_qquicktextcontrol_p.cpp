/****************************************************************************
** Meta object code from reading C++ file 'qquicktextcontrol_p.h'
**
** Created by: The Qt Meta Object Compiler version 67 (Qt 5.6.3)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "../../items/qquicktextcontrol_p.h"
#include <QtCore/qbytearray.h>
#include <QtCore/qmetatype.h>
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'qquicktextcontrol_p.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 67
#error "This file was generated using the moc from 5.6.3. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
struct qt_meta_stringdata_QQuickTextControl_t {
    QByteArrayData data[40];
    char stringdata0[507];
};
#define QT_MOC_LITERAL(idx, ofs, len) \
    Q_STATIC_BYTE_ARRAY_DATA_HEADER_INITIALIZER_WITH_OFFSET(len, \
    qptrdiff(offsetof(qt_meta_stringdata_QQuickTextControl_t, stringdata0) + ofs \
        - idx * sizeof(QByteArrayData)) \
    )
static const qt_meta_stringdata_QQuickTextControl_t qt_meta_stringdata_QQuickTextControl = {
    {
QT_MOC_LITERAL(0, 0, 17), // "QQuickTextControl"
QT_MOC_LITERAL(1, 18, 11), // "textChanged"
QT_MOC_LITERAL(2, 30, 0), // ""
QT_MOC_LITERAL(3, 31, 14), // "contentsChange"
QT_MOC_LITERAL(4, 46, 4), // "from"
QT_MOC_LITERAL(5, 51, 12), // "charsRemoved"
QT_MOC_LITERAL(6, 64, 10), // "charsAdded"
QT_MOC_LITERAL(7, 75, 13), // "undoAvailable"
QT_MOC_LITERAL(8, 89, 1), // "b"
QT_MOC_LITERAL(9, 91, 13), // "redoAvailable"
QT_MOC_LITERAL(10, 105, 24), // "currentCharFormatChanged"
QT_MOC_LITERAL(11, 130, 15), // "QTextCharFormat"
QT_MOC_LITERAL(12, 146, 6), // "format"
QT_MOC_LITERAL(13, 153, 13), // "copyAvailable"
QT_MOC_LITERAL(14, 167, 16), // "selectionChanged"
QT_MOC_LITERAL(15, 184, 21), // "cursorPositionChanged"
QT_MOC_LITERAL(16, 206, 19), // "updateCursorRequest"
QT_MOC_LITERAL(17, 226, 13), // "updateRequest"
QT_MOC_LITERAL(18, 240, 22), // "cursorRectangleChanged"
QT_MOC_LITERAL(19, 263, 13), // "linkActivated"
QT_MOC_LITERAL(20, 277, 4), // "link"
QT_MOC_LITERAL(21, 282, 11), // "linkHovered"
QT_MOC_LITERAL(22, 294, 12), // "setPlainText"
QT_MOC_LITERAL(23, 307, 4), // "text"
QT_MOC_LITERAL(24, 312, 7), // "setHtml"
QT_MOC_LITERAL(25, 320, 3), // "cut"
QT_MOC_LITERAL(26, 324, 4), // "copy"
QT_MOC_LITERAL(27, 329, 5), // "paste"
QT_MOC_LITERAL(28, 335, 16), // "QClipboard::Mode"
QT_MOC_LITERAL(29, 352, 4), // "mode"
QT_MOC_LITERAL(30, 357, 4), // "undo"
QT_MOC_LITERAL(31, 362, 4), // "redo"
QT_MOC_LITERAL(32, 367, 9), // "selectAll"
QT_MOC_LITERAL(33, 377, 38), // "_q_updateCurrentCharFormatAnd..."
QT_MOC_LITERAL(34, 416, 25), // "_q_updateCursorPosChanged"
QT_MOC_LITERAL(35, 442, 11), // "QTextCursor"
QT_MOC_LITERAL(36, 454, 16), // "inputMethodQuery"
QT_MOC_LITERAL(37, 471, 20), // "Qt::InputMethodQuery"
QT_MOC_LITERAL(38, 492, 5), // "query"
QT_MOC_LITERAL(39, 498, 8) // "argument"

    },
    "QQuickTextControl\0textChanged\0\0"
    "contentsChange\0from\0charsRemoved\0"
    "charsAdded\0undoAvailable\0b\0redoAvailable\0"
    "currentCharFormatChanged\0QTextCharFormat\0"
    "format\0copyAvailable\0selectionChanged\0"
    "cursorPositionChanged\0updateCursorRequest\0"
    "updateRequest\0cursorRectangleChanged\0"
    "linkActivated\0link\0linkHovered\0"
    "setPlainText\0text\0setHtml\0cut\0copy\0"
    "paste\0QClipboard::Mode\0mode\0undo\0redo\0"
    "selectAll\0_q_updateCurrentCharFormatAndSelection\0"
    "_q_updateCursorPosChanged\0QTextCursor\0"
    "inputMethodQuery\0Qt::InputMethodQuery\0"
    "query\0argument"
};
#undef QT_MOC_LITERAL

static const uint qt_meta_data_QQuickTextControl[] = {

 // content:
       7,       // revision
       0,       // classname
       0,    0, // classinfo
      25,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
      13,       // signalCount

 // signals: name, argc, parameters, tag, flags
       1,    0,  139,    2, 0x06 /* Public */,
       3,    3,  140,    2, 0x06 /* Public */,
       7,    1,  147,    2, 0x06 /* Public */,
       9,    1,  150,    2, 0x06 /* Public */,
      10,    1,  153,    2, 0x06 /* Public */,
      13,    1,  156,    2, 0x06 /* Public */,
      14,    0,  159,    2, 0x06 /* Public */,
      15,    0,  160,    2, 0x06 /* Public */,
      16,    0,  161,    2, 0x06 /* Public */,
      17,    0,  162,    2, 0x06 /* Public */,
      18,    0,  163,    2, 0x06 /* Public */,
      19,    1,  164,    2, 0x06 /* Public */,
      21,    1,  167,    2, 0x06 /* Public */,

 // slots: name, argc, parameters, tag, flags
      22,    1,  170,    2, 0x0a /* Public */,
      24,    1,  173,    2, 0x0a /* Public */,
      25,    0,  176,    2, 0x0a /* Public */,
      26,    0,  177,    2, 0x0a /* Public */,
      27,    1,  178,    2, 0x0a /* Public */,
      27,    0,  181,    2, 0x2a /* Public | MethodCloned */,
      30,    0,  182,    2, 0x0a /* Public */,
      31,    0,  183,    2, 0x0a /* Public */,
      32,    0,  184,    2, 0x0a /* Public */,
      33,    0,  185,    2, 0x08 /* Private */,
      34,    1,  186,    2, 0x08 /* Private */,

 // methods: name, argc, parameters, tag, flags
      36,    2,  189,    2, 0x02 /* Public */,

 // signals: parameters
    QMetaType::Void,
    QMetaType::Void, QMetaType::Int, QMetaType::Int, QMetaType::Int,    4,    5,    6,
    QMetaType::Void, QMetaType::Bool,    8,
    QMetaType::Void, QMetaType::Bool,    8,
    QMetaType::Void, 0x80000000 | 11,   12,
    QMetaType::Void, QMetaType::Bool,    8,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void, QMetaType::QString,   20,
    QMetaType::Void, QMetaType::QString,   20,

 // slots: parameters
    QMetaType::Void, QMetaType::QString,   23,
    QMetaType::Void, QMetaType::QString,   23,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void, 0x80000000 | 28,   29,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void, 0x80000000 | 35,    2,

 // methods: parameters
    QMetaType::QVariant, 0x80000000 | 37, QMetaType::QVariant,   38,   39,

       0        // eod
};

void QQuickTextControl::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        QQuickTextControl *_t = static_cast<QQuickTextControl *>(_o);
        Q_UNUSED(_t)
        switch (_id) {
        case 0: _t->textChanged(); break;
        case 1: _t->contentsChange((*reinterpret_cast< int(*)>(_a[1])),(*reinterpret_cast< int(*)>(_a[2])),(*reinterpret_cast< int(*)>(_a[3]))); break;
        case 2: _t->undoAvailable((*reinterpret_cast< bool(*)>(_a[1]))); break;
        case 3: _t->redoAvailable((*reinterpret_cast< bool(*)>(_a[1]))); break;
        case 4: _t->currentCharFormatChanged((*reinterpret_cast< const QTextCharFormat(*)>(_a[1]))); break;
        case 5: _t->copyAvailable((*reinterpret_cast< bool(*)>(_a[1]))); break;
        case 6: _t->selectionChanged(); break;
        case 7: _t->cursorPositionChanged(); break;
        case 8: _t->updateCursorRequest(); break;
        case 9: _t->updateRequest(); break;
        case 10: _t->cursorRectangleChanged(); break;
        case 11: _t->linkActivated((*reinterpret_cast< const QString(*)>(_a[1]))); break;
        case 12: _t->linkHovered((*reinterpret_cast< const QString(*)>(_a[1]))); break;
        case 13: _t->setPlainText((*reinterpret_cast< const QString(*)>(_a[1]))); break;
        case 14: _t->setHtml((*reinterpret_cast< const QString(*)>(_a[1]))); break;
        case 15: _t->cut(); break;
        case 16: _t->copy(); break;
        case 17: _t->paste((*reinterpret_cast< QClipboard::Mode(*)>(_a[1]))); break;
        case 18: _t->paste(); break;
        case 19: _t->undo(); break;
        case 20: _t->redo(); break;
        case 21: _t->selectAll(); break;
        case 22: _t->d_func()->_q_updateCurrentCharFormatAndSelection(); break;
        case 23: _t->d_func()->_q_updateCursorPosChanged((*reinterpret_cast< const QTextCursor(*)>(_a[1]))); break;
        case 24: { QVariant _r = _t->inputMethodQuery((*reinterpret_cast< Qt::InputMethodQuery(*)>(_a[1])),(*reinterpret_cast< QVariant(*)>(_a[2])));
            if (_a[0]) *reinterpret_cast< QVariant*>(_a[0]) = _r; }  break;
        default: ;
        }
    } else if (_c == QMetaObject::IndexOfMethod) {
        int *result = reinterpret_cast<int *>(_a[0]);
        void **func = reinterpret_cast<void **>(_a[1]);
        {
            typedef void (QQuickTextControl::*_t)();
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&QQuickTextControl::textChanged)) {
                *result = 0;
                return;
            }
        }
        {
            typedef void (QQuickTextControl::*_t)(int , int , int );
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&QQuickTextControl::contentsChange)) {
                *result = 1;
                return;
            }
        }
        {
            typedef void (QQuickTextControl::*_t)(bool );
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&QQuickTextControl::undoAvailable)) {
                *result = 2;
                return;
            }
        }
        {
            typedef void (QQuickTextControl::*_t)(bool );
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&QQuickTextControl::redoAvailable)) {
                *result = 3;
                return;
            }
        }
        {
            typedef void (QQuickTextControl::*_t)(const QTextCharFormat & );
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&QQuickTextControl::currentCharFormatChanged)) {
                *result = 4;
                return;
            }
        }
        {
            typedef void (QQuickTextControl::*_t)(bool );
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&QQuickTextControl::copyAvailable)) {
                *result = 5;
                return;
            }
        }
        {
            typedef void (QQuickTextControl::*_t)();
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&QQuickTextControl::selectionChanged)) {
                *result = 6;
                return;
            }
        }
        {
            typedef void (QQuickTextControl::*_t)();
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&QQuickTextControl::cursorPositionChanged)) {
                *result = 7;
                return;
            }
        }
        {
            typedef void (QQuickTextControl::*_t)();
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&QQuickTextControl::updateCursorRequest)) {
                *result = 8;
                return;
            }
        }
        {
            typedef void (QQuickTextControl::*_t)();
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&QQuickTextControl::updateRequest)) {
                *result = 9;
                return;
            }
        }
        {
            typedef void (QQuickTextControl::*_t)();
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&QQuickTextControl::cursorRectangleChanged)) {
                *result = 10;
                return;
            }
        }
        {
            typedef void (QQuickTextControl::*_t)(const QString & );
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&QQuickTextControl::linkActivated)) {
                *result = 11;
                return;
            }
        }
        {
            typedef void (QQuickTextControl::*_t)(const QString & );
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&QQuickTextControl::linkHovered)) {
                *result = 12;
                return;
            }
        }
    }
}

const QMetaObject QQuickTextControl::staticMetaObject = {
    { &QInputControl::staticMetaObject, qt_meta_stringdata_QQuickTextControl.data,
      qt_meta_data_QQuickTextControl,  qt_static_metacall, Q_NULLPTR, Q_NULLPTR}
};


const QMetaObject *QQuickTextControl::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->dynamicMetaObject() : &staticMetaObject;
}

void *QQuickTextControl::qt_metacast(const char *_clname)
{
    if (!_clname) return Q_NULLPTR;
    if (!strcmp(_clname, qt_meta_stringdata_QQuickTextControl.stringdata0))
        return static_cast<void*>(const_cast< QQuickTextControl*>(this));
    return QInputControl::qt_metacast(_clname);
}

int QQuickTextControl::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QInputControl::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 25)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 25;
    } else if (_c == QMetaObject::RegisterMethodArgumentMetaType) {
        if (_id < 25)
            *reinterpret_cast<int*>(_a[0]) = -1;
        _id -= 25;
    }
    return _id;
}

// SIGNAL 0
void QQuickTextControl::textChanged()
{
    QMetaObject::activate(this, &staticMetaObject, 0, Q_NULLPTR);
}

// SIGNAL 1
void QQuickTextControl::contentsChange(int _t1, int _t2, int _t3)
{
    void *_a[] = { Q_NULLPTR, const_cast<void*>(reinterpret_cast<const void*>(&_t1)), const_cast<void*>(reinterpret_cast<const void*>(&_t2)), const_cast<void*>(reinterpret_cast<const void*>(&_t3)) };
    QMetaObject::activate(this, &staticMetaObject, 1, _a);
}

// SIGNAL 2
void QQuickTextControl::undoAvailable(bool _t1)
{
    void *_a[] = { Q_NULLPTR, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 2, _a);
}

// SIGNAL 3
void QQuickTextControl::redoAvailable(bool _t1)
{
    void *_a[] = { Q_NULLPTR, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 3, _a);
}

// SIGNAL 4
void QQuickTextControl::currentCharFormatChanged(const QTextCharFormat & _t1)
{
    void *_a[] = { Q_NULLPTR, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 4, _a);
}

// SIGNAL 5
void QQuickTextControl::copyAvailable(bool _t1)
{
    void *_a[] = { Q_NULLPTR, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 5, _a);
}

// SIGNAL 6
void QQuickTextControl::selectionChanged()
{
    QMetaObject::activate(this, &staticMetaObject, 6, Q_NULLPTR);
}

// SIGNAL 7
void QQuickTextControl::cursorPositionChanged()
{
    QMetaObject::activate(this, &staticMetaObject, 7, Q_NULLPTR);
}

// SIGNAL 8
void QQuickTextControl::updateCursorRequest()
{
    QMetaObject::activate(this, &staticMetaObject, 8, Q_NULLPTR);
}

// SIGNAL 9
void QQuickTextControl::updateRequest()
{
    QMetaObject::activate(this, &staticMetaObject, 9, Q_NULLPTR);
}

// SIGNAL 10
void QQuickTextControl::cursorRectangleChanged()
{
    QMetaObject::activate(this, &staticMetaObject, 10, Q_NULLPTR);
}

// SIGNAL 11
void QQuickTextControl::linkActivated(const QString & _t1)
{
    void *_a[] = { Q_NULLPTR, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 11, _a);
}

// SIGNAL 12
void QQuickTextControl::linkHovered(const QString & _t1)
{
    void *_a[] = { Q_NULLPTR, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 12, _a);
}
QT_END_MOC_NAMESPACE
