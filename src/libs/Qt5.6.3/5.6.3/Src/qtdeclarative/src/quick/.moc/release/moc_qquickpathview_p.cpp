/****************************************************************************
** Meta object code from reading C++ file 'qquickpathview_p.h'
**
** Created by: The Qt Meta Object Compiler version 67 (Qt 5.6.3)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "../../items/qquickpathview_p.h"
#include <QtCore/qbytearray.h>
#include <QtCore/qmetatype.h>
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'qquickpathview_p.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 67
#error "This file was generated using the moc from 5.6.3. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
struct qt_meta_stringdata_QQuickPathView_t {
    QByteArrayData data[93];
    char stringdata0[1281];
};
#define QT_MOC_LITERAL(idx, ofs, len) \
    Q_STATIC_BYTE_ARRAY_DATA_HEADER_INITIALIZER_WITH_OFFSET(len, \
    qptrdiff(offsetof(qt_meta_stringdata_QQuickPathView_t, stringdata0) + ofs \
        - idx * sizeof(QByteArrayData)) \
    )
static const qt_meta_stringdata_QQuickPathView_t qt_meta_stringdata_QQuickPathView = {
    {
QT_MOC_LITERAL(0, 0, 14), // "QQuickPathView"
QT_MOC_LITERAL(1, 15, 19), // "currentIndexChanged"
QT_MOC_LITERAL(2, 35, 0), // ""
QT_MOC_LITERAL(3, 36, 18), // "currentItemChanged"
QT_MOC_LITERAL(4, 55, 13), // "offsetChanged"
QT_MOC_LITERAL(5, 69, 12), // "modelChanged"
QT_MOC_LITERAL(6, 82, 12), // "countChanged"
QT_MOC_LITERAL(7, 95, 11), // "pathChanged"
QT_MOC_LITERAL(8, 107, 30), // "preferredHighlightBeginChanged"
QT_MOC_LITERAL(9, 138, 28), // "preferredHighlightEndChanged"
QT_MOC_LITERAL(10, 167, 25), // "highlightRangeModeChanged"
QT_MOC_LITERAL(11, 193, 17), // "dragMarginChanged"
QT_MOC_LITERAL(12, 211, 19), // "snapPositionChanged"
QT_MOC_LITERAL(13, 231, 15), // "delegateChanged"
QT_MOC_LITERAL(14, 247, 20), // "pathItemCountChanged"
QT_MOC_LITERAL(15, 268, 27), // "maximumFlickVelocityChanged"
QT_MOC_LITERAL(16, 296, 24), // "flickDecelerationChanged"
QT_MOC_LITERAL(17, 321, 18), // "interactiveChanged"
QT_MOC_LITERAL(18, 340, 13), // "movingChanged"
QT_MOC_LITERAL(19, 354, 15), // "flickingChanged"
QT_MOC_LITERAL(20, 370, 15), // "draggingChanged"
QT_MOC_LITERAL(21, 386, 16), // "highlightChanged"
QT_MOC_LITERAL(22, 403, 20), // "highlightItemChanged"
QT_MOC_LITERAL(23, 424, 28), // "highlightMoveDurationChanged"
QT_MOC_LITERAL(24, 453, 15), // "movementStarted"
QT_MOC_LITERAL(25, 469, 13), // "movementEnded"
QT_MOC_LITERAL(26, 483, 12), // "flickStarted"
QT_MOC_LITERAL(27, 496, 10), // "flickEnded"
QT_MOC_LITERAL(28, 507, 11), // "dragStarted"
QT_MOC_LITERAL(29, 519, 9), // "dragEnded"
QT_MOC_LITERAL(30, 529, 15), // "snapModeChanged"
QT_MOC_LITERAL(31, 545, 21), // "cacheItemCountChanged"
QT_MOC_LITERAL(32, 567, 21), // "incrementCurrentIndex"
QT_MOC_LITERAL(33, 589, 21), // "decrementCurrentIndex"
QT_MOC_LITERAL(34, 611, 6), // "refill"
QT_MOC_LITERAL(35, 618, 6), // "ticked"
QT_MOC_LITERAL(36, 625, 14), // "movementEnding"
QT_MOC_LITERAL(37, 640, 12), // "modelUpdated"
QT_MOC_LITERAL(38, 653, 13), // "QQmlChangeSet"
QT_MOC_LITERAL(39, 667, 9), // "changeSet"
QT_MOC_LITERAL(40, 677, 5), // "reset"
QT_MOC_LITERAL(41, 683, 11), // "createdItem"
QT_MOC_LITERAL(42, 695, 5), // "index"
QT_MOC_LITERAL(43, 701, 4), // "item"
QT_MOC_LITERAL(44, 706, 8), // "initItem"
QT_MOC_LITERAL(45, 715, 14), // "destroyingItem"
QT_MOC_LITERAL(46, 730, 11), // "pathUpdated"
QT_MOC_LITERAL(47, 742, 19), // "positionViewAtIndex"
QT_MOC_LITERAL(48, 762, 4), // "mode"
QT_MOC_LITERAL(49, 767, 7), // "indexAt"
QT_MOC_LITERAL(50, 775, 1), // "x"
QT_MOC_LITERAL(51, 777, 1), // "y"
QT_MOC_LITERAL(52, 779, 6), // "itemAt"
QT_MOC_LITERAL(53, 786, 11), // "QQuickItem*"
QT_MOC_LITERAL(54, 798, 5), // "model"
QT_MOC_LITERAL(55, 804, 4), // "path"
QT_MOC_LITERAL(56, 809, 11), // "QQuickPath*"
QT_MOC_LITERAL(57, 821, 12), // "currentIndex"
QT_MOC_LITERAL(58, 834, 11), // "currentItem"
QT_MOC_LITERAL(59, 846, 6), // "offset"
QT_MOC_LITERAL(60, 853, 9), // "highlight"
QT_MOC_LITERAL(61, 863, 14), // "QQmlComponent*"
QT_MOC_LITERAL(62, 878, 13), // "highlightItem"
QT_MOC_LITERAL(63, 892, 23), // "preferredHighlightBegin"
QT_MOC_LITERAL(64, 916, 21), // "preferredHighlightEnd"
QT_MOC_LITERAL(65, 938, 18), // "highlightRangeMode"
QT_MOC_LITERAL(66, 957, 18), // "HighlightRangeMode"
QT_MOC_LITERAL(67, 976, 21), // "highlightMoveDuration"
QT_MOC_LITERAL(68, 998, 10), // "dragMargin"
QT_MOC_LITERAL(69, 1009, 20), // "maximumFlickVelocity"
QT_MOC_LITERAL(70, 1030, 17), // "flickDeceleration"
QT_MOC_LITERAL(71, 1048, 11), // "interactive"
QT_MOC_LITERAL(72, 1060, 6), // "moving"
QT_MOC_LITERAL(73, 1067, 8), // "flicking"
QT_MOC_LITERAL(74, 1076, 8), // "dragging"
QT_MOC_LITERAL(75, 1085, 5), // "count"
QT_MOC_LITERAL(76, 1091, 8), // "delegate"
QT_MOC_LITERAL(77, 1100, 13), // "pathItemCount"
QT_MOC_LITERAL(78, 1114, 8), // "snapMode"
QT_MOC_LITERAL(79, 1123, 8), // "SnapMode"
QT_MOC_LITERAL(80, 1132, 14), // "cacheItemCount"
QT_MOC_LITERAL(81, 1147, 16), // "NoHighlightRange"
QT_MOC_LITERAL(82, 1164, 10), // "ApplyRange"
QT_MOC_LITERAL(83, 1175, 20), // "StrictlyEnforceRange"
QT_MOC_LITERAL(84, 1196, 6), // "NoSnap"
QT_MOC_LITERAL(85, 1203, 10), // "SnapToItem"
QT_MOC_LITERAL(86, 1214, 11), // "SnapOneItem"
QT_MOC_LITERAL(87, 1226, 12), // "PositionMode"
QT_MOC_LITERAL(88, 1239, 9), // "Beginning"
QT_MOC_LITERAL(89, 1249, 6), // "Center"
QT_MOC_LITERAL(90, 1256, 3), // "End"
QT_MOC_LITERAL(91, 1260, 7), // "Contain"
QT_MOC_LITERAL(92, 1268, 12) // "SnapPosition"

    },
    "QQuickPathView\0currentIndexChanged\0\0"
    "currentItemChanged\0offsetChanged\0"
    "modelChanged\0countChanged\0pathChanged\0"
    "preferredHighlightBeginChanged\0"
    "preferredHighlightEndChanged\0"
    "highlightRangeModeChanged\0dragMarginChanged\0"
    "snapPositionChanged\0delegateChanged\0"
    "pathItemCountChanged\0maximumFlickVelocityChanged\0"
    "flickDecelerationChanged\0interactiveChanged\0"
    "movingChanged\0flickingChanged\0"
    "draggingChanged\0highlightChanged\0"
    "highlightItemChanged\0highlightMoveDurationChanged\0"
    "movementStarted\0movementEnded\0"
    "flickStarted\0flickEnded\0dragStarted\0"
    "dragEnded\0snapModeChanged\0"
    "cacheItemCountChanged\0incrementCurrentIndex\0"
    "decrementCurrentIndex\0refill\0ticked\0"
    "movementEnding\0modelUpdated\0QQmlChangeSet\0"
    "changeSet\0reset\0createdItem\0index\0"
    "item\0initItem\0destroyingItem\0pathUpdated\0"
    "positionViewAtIndex\0mode\0indexAt\0x\0y\0"
    "itemAt\0QQuickItem*\0model\0path\0QQuickPath*\0"
    "currentIndex\0currentItem\0offset\0"
    "highlight\0QQmlComponent*\0highlightItem\0"
    "preferredHighlightBegin\0preferredHighlightEnd\0"
    "highlightRangeMode\0HighlightRangeMode\0"
    "highlightMoveDuration\0dragMargin\0"
    "maximumFlickVelocity\0flickDeceleration\0"
    "interactive\0moving\0flicking\0dragging\0"
    "count\0delegate\0pathItemCount\0snapMode\0"
    "SnapMode\0cacheItemCount\0NoHighlightRange\0"
    "ApplyRange\0StrictlyEnforceRange\0NoSnap\0"
    "SnapToItem\0SnapOneItem\0PositionMode\0"
    "Beginning\0Center\0End\0Contain\0SnapPosition"
};
#undef QT_MOC_LITERAL

static const uint qt_meta_data_QQuickPathView[] = {

 // content:
       7,       // revision
       0,       // classname
       0,    0, // classinfo
      43,   14, // methods
      23,  298, // properties
       3,  390, // enums/sets
       0,    0, // constructors
       0,       // flags
      30,       // signalCount

 // signals: name, argc, parameters, tag, flags
       1,    0,  229,    2, 0x06 /* Public */,
       3,    0,  230,    2, 0x06 /* Public */,
       4,    0,  231,    2, 0x06 /* Public */,
       5,    0,  232,    2, 0x06 /* Public */,
       6,    0,  233,    2, 0x06 /* Public */,
       7,    0,  234,    2, 0x06 /* Public */,
       8,    0,  235,    2, 0x06 /* Public */,
       9,    0,  236,    2, 0x06 /* Public */,
      10,    0,  237,    2, 0x06 /* Public */,
      11,    0,  238,    2, 0x06 /* Public */,
      12,    0,  239,    2, 0x06 /* Public */,
      13,    0,  240,    2, 0x06 /* Public */,
      14,    0,  241,    2, 0x06 /* Public */,
      15,    0,  242,    2, 0x06 /* Public */,
      16,    0,  243,    2, 0x06 /* Public */,
      17,    0,  244,    2, 0x06 /* Public */,
      18,    0,  245,    2, 0x06 /* Public */,
      19,    0,  246,    2, 0x06 /* Public */,
      20,    0,  247,    2, 0x06 /* Public */,
      21,    0,  248,    2, 0x06 /* Public */,
      22,    0,  249,    2, 0x06 /* Public */,
      23,    0,  250,    2, 0x06 /* Public */,
      24,    0,  251,    2, 0x06 /* Public */,
      25,    0,  252,    2, 0x06 /* Public */,
      26,    0,  253,    2, 0x06 /* Public */,
      27,    0,  254,    2, 0x06 /* Public */,
      28,    0,  255,    2, 0x06 /* Public */,
      29,    0,  256,    2, 0x06 /* Public */,
      30,    0,  257,    2, 0x06 /* Public */,
      31,    0,  258,    2, 0x06 /* Public */,

 // slots: name, argc, parameters, tag, flags
      32,    0,  259,    2, 0x0a /* Public */,
      33,    0,  260,    2, 0x0a /* Public */,
      34,    0,  261,    2, 0x08 /* Private */,
      35,    0,  262,    2, 0x08 /* Private */,
      36,    0,  263,    2, 0x08 /* Private */,
      37,    2,  264,    2, 0x08 /* Private */,
      41,    2,  269,    2, 0x08 /* Private */,
      44,    2,  274,    2, 0x08 /* Private */,
      45,    1,  279,    2, 0x08 /* Private */,
      46,    0,  282,    2, 0x08 /* Private */,

 // methods: name, argc, parameters, tag, flags
      47,    2,  283,    2, 0x02 /* Public */,
      49,    2,  288,    2, 0x02 /* Public */,
      52,    2,  293,    2, 0x02 /* Public */,

 // signals: parameters
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,

 // slots: parameters
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void, 0x80000000 | 38, QMetaType::Bool,   39,   40,
    QMetaType::Void, QMetaType::Int, QMetaType::QObjectStar,   42,   43,
    QMetaType::Void, QMetaType::Int, QMetaType::QObjectStar,   42,   43,
    QMetaType::Void, QMetaType::QObjectStar,   43,
    QMetaType::Void,

 // methods: parameters
    QMetaType::Void, QMetaType::Int, QMetaType::Int,   42,   48,
    QMetaType::Int, QMetaType::QReal, QMetaType::QReal,   50,   51,
    0x80000000 | 53, QMetaType::QReal, QMetaType::QReal,   50,   51,

 // properties: name, type, flags
      54, QMetaType::QVariant, 0x00495103,
      55, 0x80000000 | 56, 0x0049510b,
      57, QMetaType::Int, 0x00495103,
      58, 0x80000000 | 53, 0x00495009,
      59, QMetaType::QReal, 0x00495103,
      60, 0x80000000 | 61, 0x0049510b,
      62, 0x80000000 | 53, 0x00495009,
      63, QMetaType::QReal, 0x00495103,
      64, QMetaType::QReal, 0x00495103,
      65, 0x80000000 | 66, 0x0049510b,
      67, QMetaType::Int, 0x00495103,
      68, QMetaType::QReal, 0x00495103,
      69, QMetaType::QReal, 0x00495103,
      70, QMetaType::QReal, 0x00495103,
      71, QMetaType::Bool, 0x00495103,
      72, QMetaType::Bool, 0x00495001,
      73, QMetaType::Bool, 0x00495001,
      74, QMetaType::Bool, 0x00495001,
      75, QMetaType::Int, 0x00495001,
      76, 0x80000000 | 61, 0x0049510b,
      77, QMetaType::Int, 0x00495107,
      78, 0x80000000 | 79, 0x0049510b,
      80, QMetaType::Int, 0x00495103,

 // properties: notify_signal_id
       3,
       5,
       0,
       1,
       2,
      19,
      20,
       6,
       7,
       8,
      21,
       9,
      13,
      14,
      15,
      16,
      17,
      18,
       4,
      11,
      12,
      28,
      29,

 // enums: name, flags, count, data
      66, 0x0,    3,  402,
      79, 0x0,    3,  408,
      87, 0x0,    5,  414,

 // enum data: key, value
      81, uint(QQuickPathView::NoHighlightRange),
      82, uint(QQuickPathView::ApplyRange),
      83, uint(QQuickPathView::StrictlyEnforceRange),
      84, uint(QQuickPathView::NoSnap),
      85, uint(QQuickPathView::SnapToItem),
      86, uint(QQuickPathView::SnapOneItem),
      88, uint(QQuickPathView::Beginning),
      89, uint(QQuickPathView::Center),
      90, uint(QQuickPathView::End),
      91, uint(QQuickPathView::Contain),
      92, uint(QQuickPathView::SnapPosition),

       0        // eod
};

void QQuickPathView::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        QQuickPathView *_t = static_cast<QQuickPathView *>(_o);
        Q_UNUSED(_t)
        switch (_id) {
        case 0: _t->currentIndexChanged(); break;
        case 1: _t->currentItemChanged(); break;
        case 2: _t->offsetChanged(); break;
        case 3: _t->modelChanged(); break;
        case 4: _t->countChanged(); break;
        case 5: _t->pathChanged(); break;
        case 6: _t->preferredHighlightBeginChanged(); break;
        case 7: _t->preferredHighlightEndChanged(); break;
        case 8: _t->highlightRangeModeChanged(); break;
        case 9: _t->dragMarginChanged(); break;
        case 10: _t->snapPositionChanged(); break;
        case 11: _t->delegateChanged(); break;
        case 12: _t->pathItemCountChanged(); break;
        case 13: _t->maximumFlickVelocityChanged(); break;
        case 14: _t->flickDecelerationChanged(); break;
        case 15: _t->interactiveChanged(); break;
        case 16: _t->movingChanged(); break;
        case 17: _t->flickingChanged(); break;
        case 18: _t->draggingChanged(); break;
        case 19: _t->highlightChanged(); break;
        case 20: _t->highlightItemChanged(); break;
        case 21: _t->highlightMoveDurationChanged(); break;
        case 22: _t->movementStarted(); break;
        case 23: _t->movementEnded(); break;
        case 24: _t->flickStarted(); break;
        case 25: _t->flickEnded(); break;
        case 26: _t->dragStarted(); break;
        case 27: _t->dragEnded(); break;
        case 28: _t->snapModeChanged(); break;
        case 29: _t->cacheItemCountChanged(); break;
        case 30: _t->incrementCurrentIndex(); break;
        case 31: _t->decrementCurrentIndex(); break;
        case 32: _t->refill(); break;
        case 33: _t->ticked(); break;
        case 34: _t->movementEnding(); break;
        case 35: _t->modelUpdated((*reinterpret_cast< const QQmlChangeSet(*)>(_a[1])),(*reinterpret_cast< bool(*)>(_a[2]))); break;
        case 36: _t->createdItem((*reinterpret_cast< int(*)>(_a[1])),(*reinterpret_cast< QObject*(*)>(_a[2]))); break;
        case 37: _t->initItem((*reinterpret_cast< int(*)>(_a[1])),(*reinterpret_cast< QObject*(*)>(_a[2]))); break;
        case 38: _t->destroyingItem((*reinterpret_cast< QObject*(*)>(_a[1]))); break;
        case 39: _t->pathUpdated(); break;
        case 40: _t->positionViewAtIndex((*reinterpret_cast< int(*)>(_a[1])),(*reinterpret_cast< int(*)>(_a[2]))); break;
        case 41: { int _r = _t->indexAt((*reinterpret_cast< qreal(*)>(_a[1])),(*reinterpret_cast< qreal(*)>(_a[2])));
            if (_a[0]) *reinterpret_cast< int*>(_a[0]) = _r; }  break;
        case 42: { QQuickItem* _r = _t->itemAt((*reinterpret_cast< qreal(*)>(_a[1])),(*reinterpret_cast< qreal(*)>(_a[2])));
            if (_a[0]) *reinterpret_cast< QQuickItem**>(_a[0]) = _r; }  break;
        default: ;
        }
    } else if (_c == QMetaObject::IndexOfMethod) {
        int *result = reinterpret_cast<int *>(_a[0]);
        void **func = reinterpret_cast<void **>(_a[1]);
        {
            typedef void (QQuickPathView::*_t)();
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&QQuickPathView::currentIndexChanged)) {
                *result = 0;
                return;
            }
        }
        {
            typedef void (QQuickPathView::*_t)();
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&QQuickPathView::currentItemChanged)) {
                *result = 1;
                return;
            }
        }
        {
            typedef void (QQuickPathView::*_t)();
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&QQuickPathView::offsetChanged)) {
                *result = 2;
                return;
            }
        }
        {
            typedef void (QQuickPathView::*_t)();
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&QQuickPathView::modelChanged)) {
                *result = 3;
                return;
            }
        }
        {
            typedef void (QQuickPathView::*_t)();
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&QQuickPathView::countChanged)) {
                *result = 4;
                return;
            }
        }
        {
            typedef void (QQuickPathView::*_t)();
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&QQuickPathView::pathChanged)) {
                *result = 5;
                return;
            }
        }
        {
            typedef void (QQuickPathView::*_t)();
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&QQuickPathView::preferredHighlightBeginChanged)) {
                *result = 6;
                return;
            }
        }
        {
            typedef void (QQuickPathView::*_t)();
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&QQuickPathView::preferredHighlightEndChanged)) {
                *result = 7;
                return;
            }
        }
        {
            typedef void (QQuickPathView::*_t)();
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&QQuickPathView::highlightRangeModeChanged)) {
                *result = 8;
                return;
            }
        }
        {
            typedef void (QQuickPathView::*_t)();
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&QQuickPathView::dragMarginChanged)) {
                *result = 9;
                return;
            }
        }
        {
            typedef void (QQuickPathView::*_t)();
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&QQuickPathView::snapPositionChanged)) {
                *result = 10;
                return;
            }
        }
        {
            typedef void (QQuickPathView::*_t)();
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&QQuickPathView::delegateChanged)) {
                *result = 11;
                return;
            }
        }
        {
            typedef void (QQuickPathView::*_t)();
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&QQuickPathView::pathItemCountChanged)) {
                *result = 12;
                return;
            }
        }
        {
            typedef void (QQuickPathView::*_t)();
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&QQuickPathView::maximumFlickVelocityChanged)) {
                *result = 13;
                return;
            }
        }
        {
            typedef void (QQuickPathView::*_t)();
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&QQuickPathView::flickDecelerationChanged)) {
                *result = 14;
                return;
            }
        }
        {
            typedef void (QQuickPathView::*_t)();
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&QQuickPathView::interactiveChanged)) {
                *result = 15;
                return;
            }
        }
        {
            typedef void (QQuickPathView::*_t)();
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&QQuickPathView::movingChanged)) {
                *result = 16;
                return;
            }
        }
        {
            typedef void (QQuickPathView::*_t)();
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&QQuickPathView::flickingChanged)) {
                *result = 17;
                return;
            }
        }
        {
            typedef void (QQuickPathView::*_t)();
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&QQuickPathView::draggingChanged)) {
                *result = 18;
                return;
            }
        }
        {
            typedef void (QQuickPathView::*_t)();
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&QQuickPathView::highlightChanged)) {
                *result = 19;
                return;
            }
        }
        {
            typedef void (QQuickPathView::*_t)();
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&QQuickPathView::highlightItemChanged)) {
                *result = 20;
                return;
            }
        }
        {
            typedef void (QQuickPathView::*_t)();
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&QQuickPathView::highlightMoveDurationChanged)) {
                *result = 21;
                return;
            }
        }
        {
            typedef void (QQuickPathView::*_t)();
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&QQuickPathView::movementStarted)) {
                *result = 22;
                return;
            }
        }
        {
            typedef void (QQuickPathView::*_t)();
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&QQuickPathView::movementEnded)) {
                *result = 23;
                return;
            }
        }
        {
            typedef void (QQuickPathView::*_t)();
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&QQuickPathView::flickStarted)) {
                *result = 24;
                return;
            }
        }
        {
            typedef void (QQuickPathView::*_t)();
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&QQuickPathView::flickEnded)) {
                *result = 25;
                return;
            }
        }
        {
            typedef void (QQuickPathView::*_t)();
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&QQuickPathView::dragStarted)) {
                *result = 26;
                return;
            }
        }
        {
            typedef void (QQuickPathView::*_t)();
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&QQuickPathView::dragEnded)) {
                *result = 27;
                return;
            }
        }
        {
            typedef void (QQuickPathView::*_t)();
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&QQuickPathView::snapModeChanged)) {
                *result = 28;
                return;
            }
        }
        {
            typedef void (QQuickPathView::*_t)();
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&QQuickPathView::cacheItemCountChanged)) {
                *result = 29;
                return;
            }
        }
    } else if (_c == QMetaObject::RegisterPropertyMetaType) {
        switch (_id) {
        default: *reinterpret_cast<int*>(_a[0]) = -1; break;
        case 19:
        case 5:
            *reinterpret_cast<int*>(_a[0]) = qRegisterMetaType< QQmlComponent* >(); break;
        case 6:
        case 3:
            *reinterpret_cast<int*>(_a[0]) = qRegisterMetaType< QQuickItem* >(); break;
        case 1:
            *reinterpret_cast<int*>(_a[0]) = qRegisterMetaType< QQuickPath* >(); break;
        }
    }

#ifndef QT_NO_PROPERTIES
    else if (_c == QMetaObject::ReadProperty) {
        QQuickPathView *_t = static_cast<QQuickPathView *>(_o);
        Q_UNUSED(_t)
        void *_v = _a[0];
        switch (_id) {
        case 0: *reinterpret_cast< QVariant*>(_v) = _t->model(); break;
        case 1: *reinterpret_cast< QQuickPath**>(_v) = _t->path(); break;
        case 2: *reinterpret_cast< int*>(_v) = _t->currentIndex(); break;
        case 3: *reinterpret_cast< QQuickItem**>(_v) = _t->currentItem(); break;
        case 4: *reinterpret_cast< qreal*>(_v) = _t->offset(); break;
        case 5: *reinterpret_cast< QQmlComponent**>(_v) = _t->highlight(); break;
        case 6: *reinterpret_cast< QQuickItem**>(_v) = _t->highlightItem(); break;
        case 7: *reinterpret_cast< qreal*>(_v) = _t->preferredHighlightBegin(); break;
        case 8: *reinterpret_cast< qreal*>(_v) = _t->preferredHighlightEnd(); break;
        case 9: *reinterpret_cast< HighlightRangeMode*>(_v) = _t->highlightRangeMode(); break;
        case 10: *reinterpret_cast< int*>(_v) = _t->highlightMoveDuration(); break;
        case 11: *reinterpret_cast< qreal*>(_v) = _t->dragMargin(); break;
        case 12: *reinterpret_cast< qreal*>(_v) = _t->maximumFlickVelocity(); break;
        case 13: *reinterpret_cast< qreal*>(_v) = _t->flickDeceleration(); break;
        case 14: *reinterpret_cast< bool*>(_v) = _t->isInteractive(); break;
        case 15: *reinterpret_cast< bool*>(_v) = _t->isMoving(); break;
        case 16: *reinterpret_cast< bool*>(_v) = _t->isFlicking(); break;
        case 17: *reinterpret_cast< bool*>(_v) = _t->isDragging(); break;
        case 18: *reinterpret_cast< int*>(_v) = _t->count(); break;
        case 19: *reinterpret_cast< QQmlComponent**>(_v) = _t->delegate(); break;
        case 20: *reinterpret_cast< int*>(_v) = _t->pathItemCount(); break;
        case 21: *reinterpret_cast< SnapMode*>(_v) = _t->snapMode(); break;
        case 22: *reinterpret_cast< int*>(_v) = _t->cacheItemCount(); break;
        default: break;
        }
    } else if (_c == QMetaObject::WriteProperty) {
        QQuickPathView *_t = static_cast<QQuickPathView *>(_o);
        Q_UNUSED(_t)
        void *_v = _a[0];
        switch (_id) {
        case 0: _t->setModel(*reinterpret_cast< QVariant*>(_v)); break;
        case 1: _t->setPath(*reinterpret_cast< QQuickPath**>(_v)); break;
        case 2: _t->setCurrentIndex(*reinterpret_cast< int*>(_v)); break;
        case 4: _t->setOffset(*reinterpret_cast< qreal*>(_v)); break;
        case 5: _t->setHighlight(*reinterpret_cast< QQmlComponent**>(_v)); break;
        case 7: _t->setPreferredHighlightBegin(*reinterpret_cast< qreal*>(_v)); break;
        case 8: _t->setPreferredHighlightEnd(*reinterpret_cast< qreal*>(_v)); break;
        case 9: _t->setHighlightRangeMode(*reinterpret_cast< HighlightRangeMode*>(_v)); break;
        case 10: _t->setHighlightMoveDuration(*reinterpret_cast< int*>(_v)); break;
        case 11: _t->setDragMargin(*reinterpret_cast< qreal*>(_v)); break;
        case 12: _t->setMaximumFlickVelocity(*reinterpret_cast< qreal*>(_v)); break;
        case 13: _t->setFlickDeceleration(*reinterpret_cast< qreal*>(_v)); break;
        case 14: _t->setInteractive(*reinterpret_cast< bool*>(_v)); break;
        case 19: _t->setDelegate(*reinterpret_cast< QQmlComponent**>(_v)); break;
        case 20: _t->setPathItemCount(*reinterpret_cast< int*>(_v)); break;
        case 21: _t->setSnapMode(*reinterpret_cast< SnapMode*>(_v)); break;
        case 22: _t->setCacheItemCount(*reinterpret_cast< int*>(_v)); break;
        default: break;
        }
    } else if (_c == QMetaObject::ResetProperty) {
        QQuickPathView *_t = static_cast<QQuickPathView *>(_o);
        Q_UNUSED(_t)
        switch (_id) {
        case 20: _t->resetPathItemCount(); break;
        default: break;
        }
    }
#endif // QT_NO_PROPERTIES
}

const QMetaObject QQuickPathView::staticMetaObject = {
    { &QQuickItem::staticMetaObject, qt_meta_stringdata_QQuickPathView.data,
      qt_meta_data_QQuickPathView,  qt_static_metacall, Q_NULLPTR, Q_NULLPTR}
};


const QMetaObject *QQuickPathView::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->dynamicMetaObject() : &staticMetaObject;
}

void *QQuickPathView::qt_metacast(const char *_clname)
{
    if (!_clname) return Q_NULLPTR;
    if (!strcmp(_clname, qt_meta_stringdata_QQuickPathView.stringdata0))
        return static_cast<void*>(const_cast< QQuickPathView*>(this));
    return QQuickItem::qt_metacast(_clname);
}

int QQuickPathView::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QQuickItem::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 43)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 43;
    } else if (_c == QMetaObject::RegisterMethodArgumentMetaType) {
        if (_id < 43)
            *reinterpret_cast<int*>(_a[0]) = -1;
        _id -= 43;
    }
#ifndef QT_NO_PROPERTIES
   else if (_c == QMetaObject::ReadProperty || _c == QMetaObject::WriteProperty
            || _c == QMetaObject::ResetProperty || _c == QMetaObject::RegisterPropertyMetaType) {
        qt_static_metacall(this, _c, _id, _a);
        _id -= 23;
    } else if (_c == QMetaObject::QueryPropertyDesignable) {
        _id -= 23;
    } else if (_c == QMetaObject::QueryPropertyScriptable) {
        _id -= 23;
    } else if (_c == QMetaObject::QueryPropertyStored) {
        _id -= 23;
    } else if (_c == QMetaObject::QueryPropertyEditable) {
        _id -= 23;
    } else if (_c == QMetaObject::QueryPropertyUser) {
        _id -= 23;
    }
#endif // QT_NO_PROPERTIES
    return _id;
}

// SIGNAL 0
void QQuickPathView::currentIndexChanged()
{
    QMetaObject::activate(this, &staticMetaObject, 0, Q_NULLPTR);
}

// SIGNAL 1
void QQuickPathView::currentItemChanged()
{
    QMetaObject::activate(this, &staticMetaObject, 1, Q_NULLPTR);
}

// SIGNAL 2
void QQuickPathView::offsetChanged()
{
    QMetaObject::activate(this, &staticMetaObject, 2, Q_NULLPTR);
}

// SIGNAL 3
void QQuickPathView::modelChanged()
{
    QMetaObject::activate(this, &staticMetaObject, 3, Q_NULLPTR);
}

// SIGNAL 4
void QQuickPathView::countChanged()
{
    QMetaObject::activate(this, &staticMetaObject, 4, Q_NULLPTR);
}

// SIGNAL 5
void QQuickPathView::pathChanged()
{
    QMetaObject::activate(this, &staticMetaObject, 5, Q_NULLPTR);
}

// SIGNAL 6
void QQuickPathView::preferredHighlightBeginChanged()
{
    QMetaObject::activate(this, &staticMetaObject, 6, Q_NULLPTR);
}

// SIGNAL 7
void QQuickPathView::preferredHighlightEndChanged()
{
    QMetaObject::activate(this, &staticMetaObject, 7, Q_NULLPTR);
}

// SIGNAL 8
void QQuickPathView::highlightRangeModeChanged()
{
    QMetaObject::activate(this, &staticMetaObject, 8, Q_NULLPTR);
}

// SIGNAL 9
void QQuickPathView::dragMarginChanged()
{
    QMetaObject::activate(this, &staticMetaObject, 9, Q_NULLPTR);
}

// SIGNAL 10
void QQuickPathView::snapPositionChanged()
{
    QMetaObject::activate(this, &staticMetaObject, 10, Q_NULLPTR);
}

// SIGNAL 11
void QQuickPathView::delegateChanged()
{
    QMetaObject::activate(this, &staticMetaObject, 11, Q_NULLPTR);
}

// SIGNAL 12
void QQuickPathView::pathItemCountChanged()
{
    QMetaObject::activate(this, &staticMetaObject, 12, Q_NULLPTR);
}

// SIGNAL 13
void QQuickPathView::maximumFlickVelocityChanged()
{
    QMetaObject::activate(this, &staticMetaObject, 13, Q_NULLPTR);
}

// SIGNAL 14
void QQuickPathView::flickDecelerationChanged()
{
    QMetaObject::activate(this, &staticMetaObject, 14, Q_NULLPTR);
}

// SIGNAL 15
void QQuickPathView::interactiveChanged()
{
    QMetaObject::activate(this, &staticMetaObject, 15, Q_NULLPTR);
}

// SIGNAL 16
void QQuickPathView::movingChanged()
{
    QMetaObject::activate(this, &staticMetaObject, 16, Q_NULLPTR);
}

// SIGNAL 17
void QQuickPathView::flickingChanged()
{
    QMetaObject::activate(this, &staticMetaObject, 17, Q_NULLPTR);
}

// SIGNAL 18
void QQuickPathView::draggingChanged()
{
    QMetaObject::activate(this, &staticMetaObject, 18, Q_NULLPTR);
}

// SIGNAL 19
void QQuickPathView::highlightChanged()
{
    QMetaObject::activate(this, &staticMetaObject, 19, Q_NULLPTR);
}

// SIGNAL 20
void QQuickPathView::highlightItemChanged()
{
    QMetaObject::activate(this, &staticMetaObject, 20, Q_NULLPTR);
}

// SIGNAL 21
void QQuickPathView::highlightMoveDurationChanged()
{
    QMetaObject::activate(this, &staticMetaObject, 21, Q_NULLPTR);
}

// SIGNAL 22
void QQuickPathView::movementStarted()
{
    QMetaObject::activate(this, &staticMetaObject, 22, Q_NULLPTR);
}

// SIGNAL 23
void QQuickPathView::movementEnded()
{
    QMetaObject::activate(this, &staticMetaObject, 23, Q_NULLPTR);
}

// SIGNAL 24
void QQuickPathView::flickStarted()
{
    QMetaObject::activate(this, &staticMetaObject, 24, Q_NULLPTR);
}

// SIGNAL 25
void QQuickPathView::flickEnded()
{
    QMetaObject::activate(this, &staticMetaObject, 25, Q_NULLPTR);
}

// SIGNAL 26
void QQuickPathView::dragStarted()
{
    QMetaObject::activate(this, &staticMetaObject, 26, Q_NULLPTR);
}

// SIGNAL 27
void QQuickPathView::dragEnded()
{
    QMetaObject::activate(this, &staticMetaObject, 27, Q_NULLPTR);
}

// SIGNAL 28
void QQuickPathView::snapModeChanged()
{
    QMetaObject::activate(this, &staticMetaObject, 28, Q_NULLPTR);
}

// SIGNAL 29
void QQuickPathView::cacheItemCountChanged()
{
    QMetaObject::activate(this, &staticMetaObject, 29, Q_NULLPTR);
}
struct qt_meta_stringdata_QQuickPathViewAttached_t {
    QByteArrayData data[8];
    char stringdata0[97];
};
#define QT_MOC_LITERAL(idx, ofs, len) \
    Q_STATIC_BYTE_ARRAY_DATA_HEADER_INITIALIZER_WITH_OFFSET(len, \
    qptrdiff(offsetof(qt_meta_stringdata_QQuickPathViewAttached_t, stringdata0) + ofs \
        - idx * sizeof(QByteArrayData)) \
    )
static const qt_meta_stringdata_QQuickPathViewAttached_t qt_meta_stringdata_QQuickPathViewAttached = {
    {
QT_MOC_LITERAL(0, 0, 22), // "QQuickPathViewAttached"
QT_MOC_LITERAL(1, 23, 18), // "currentItemChanged"
QT_MOC_LITERAL(2, 42, 0), // ""
QT_MOC_LITERAL(3, 43, 11), // "pathChanged"
QT_MOC_LITERAL(4, 55, 4), // "view"
QT_MOC_LITERAL(5, 60, 15), // "QQuickPathView*"
QT_MOC_LITERAL(6, 76, 13), // "isCurrentItem"
QT_MOC_LITERAL(7, 90, 6) // "onPath"

    },
    "QQuickPathViewAttached\0currentItemChanged\0"
    "\0pathChanged\0view\0QQuickPathView*\0"
    "isCurrentItem\0onPath"
};
#undef QT_MOC_LITERAL

static const uint qt_meta_data_QQuickPathViewAttached[] = {

 // content:
       7,       // revision
       0,       // classname
       0,    0, // classinfo
       2,   14, // methods
       3,   26, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       2,       // signalCount

 // signals: name, argc, parameters, tag, flags
       1,    0,   24,    2, 0x06 /* Public */,
       3,    0,   25,    2, 0x06 /* Public */,

 // signals: parameters
    QMetaType::Void,
    QMetaType::Void,

 // properties: name, type, flags
       4, 0x80000000 | 5, 0x00095409,
       6, QMetaType::Bool, 0x00495001,
       7, QMetaType::Bool, 0x00495001,

 // properties: notify_signal_id
       0,
       0,
       1,

       0        // eod
};

void QQuickPathViewAttached::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        QQuickPathViewAttached *_t = static_cast<QQuickPathViewAttached *>(_o);
        Q_UNUSED(_t)
        switch (_id) {
        case 0: _t->currentItemChanged(); break;
        case 1: _t->pathChanged(); break;
        default: ;
        }
    } else if (_c == QMetaObject::IndexOfMethod) {
        int *result = reinterpret_cast<int *>(_a[0]);
        void **func = reinterpret_cast<void **>(_a[1]);
        {
            typedef void (QQuickPathViewAttached::*_t)();
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&QQuickPathViewAttached::currentItemChanged)) {
                *result = 0;
                return;
            }
        }
        {
            typedef void (QQuickPathViewAttached::*_t)();
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&QQuickPathViewAttached::pathChanged)) {
                *result = 1;
                return;
            }
        }
    } else if (_c == QMetaObject::RegisterPropertyMetaType) {
        switch (_id) {
        default: *reinterpret_cast<int*>(_a[0]) = -1; break;
        case 0:
            *reinterpret_cast<int*>(_a[0]) = qRegisterMetaType< QQuickPathView* >(); break;
        }
    }

#ifndef QT_NO_PROPERTIES
    else if (_c == QMetaObject::ReadProperty) {
        QQuickPathViewAttached *_t = static_cast<QQuickPathViewAttached *>(_o);
        Q_UNUSED(_t)
        void *_v = _a[0];
        switch (_id) {
        case 0: *reinterpret_cast< QQuickPathView**>(_v) = _t->view(); break;
        case 1: *reinterpret_cast< bool*>(_v) = _t->isCurrentItem(); break;
        case 2: *reinterpret_cast< bool*>(_v) = _t->isOnPath(); break;
        default: break;
        }
    } else if (_c == QMetaObject::WriteProperty) {
    } else if (_c == QMetaObject::ResetProperty) {
    }
#endif // QT_NO_PROPERTIES
}

const QMetaObject QQuickPathViewAttached::staticMetaObject = {
    { &QObject::staticMetaObject, qt_meta_stringdata_QQuickPathViewAttached.data,
      qt_meta_data_QQuickPathViewAttached,  qt_static_metacall, Q_NULLPTR, Q_NULLPTR}
};


const QMetaObject *QQuickPathViewAttached::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->dynamicMetaObject() : &staticMetaObject;
}

void *QQuickPathViewAttached::qt_metacast(const char *_clname)
{
    if (!_clname) return Q_NULLPTR;
    if (!strcmp(_clname, qt_meta_stringdata_QQuickPathViewAttached.stringdata0))
        return static_cast<void*>(const_cast< QQuickPathViewAttached*>(this));
    return QObject::qt_metacast(_clname);
}

int QQuickPathViewAttached::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QObject::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 2)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 2;
    } else if (_c == QMetaObject::RegisterMethodArgumentMetaType) {
        if (_id < 2)
            *reinterpret_cast<int*>(_a[0]) = -1;
        _id -= 2;
    }
#ifndef QT_NO_PROPERTIES
   else if (_c == QMetaObject::ReadProperty || _c == QMetaObject::WriteProperty
            || _c == QMetaObject::ResetProperty || _c == QMetaObject::RegisterPropertyMetaType) {
        qt_static_metacall(this, _c, _id, _a);
        _id -= 3;
    } else if (_c == QMetaObject::QueryPropertyDesignable) {
        _id -= 3;
    } else if (_c == QMetaObject::QueryPropertyScriptable) {
        _id -= 3;
    } else if (_c == QMetaObject::QueryPropertyStored) {
        _id -= 3;
    } else if (_c == QMetaObject::QueryPropertyEditable) {
        _id -= 3;
    } else if (_c == QMetaObject::QueryPropertyUser) {
        _id -= 3;
    }
#endif // QT_NO_PROPERTIES
    return _id;
}

// SIGNAL 0
void QQuickPathViewAttached::currentItemChanged()
{
    QMetaObject::activate(this, &staticMetaObject, 0, Q_NULLPTR);
}

// SIGNAL 1
void QQuickPathViewAttached::pathChanged()
{
    QMetaObject::activate(this, &staticMetaObject, 1, Q_NULLPTR);
}
QT_END_MOC_NAMESPACE
