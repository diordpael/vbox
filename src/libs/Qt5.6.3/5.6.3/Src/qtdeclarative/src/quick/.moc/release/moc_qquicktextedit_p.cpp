/****************************************************************************
** Meta object code from reading C++ file 'qquicktextedit_p.h'
**
** Created by: The Qt Meta Object Compiler version 67 (Qt 5.6.3)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "../../items/qquicktextedit_p.h"
#include <QtCore/qbytearray.h>
#include <QtCore/qmetatype.h>
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'qquicktextedit_p.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 67
#error "This file was generated using the moc from 5.6.3. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
struct qt_meta_stringdata_QQuickTextEdit_t {
    QByteArrayData data[169];
    char stringdata0[2437];
};
#define QT_MOC_LITERAL(idx, ofs, len) \
    Q_STATIC_BYTE_ARRAY_DATA_HEADER_INITIALIZER_WITH_OFFSET(len, \
    qptrdiff(offsetof(qt_meta_stringdata_QQuickTextEdit_t, stringdata0) + ofs \
        - idx * sizeof(QByteArrayData)) \
    )
static const qt_meta_stringdata_QQuickTextEdit_t qt_meta_stringdata_QQuickTextEdit = {
    {
QT_MOC_LITERAL(0, 0, 14), // "QQuickTextEdit"
QT_MOC_LITERAL(1, 15, 11), // "textChanged"
QT_MOC_LITERAL(2, 27, 0), // ""
QT_MOC_LITERAL(3, 28, 18), // "contentSizeChanged"
QT_MOC_LITERAL(4, 47, 21), // "cursorPositionChanged"
QT_MOC_LITERAL(5, 69, 22), // "cursorRectangleChanged"
QT_MOC_LITERAL(6, 92, 21), // "selectionStartChanged"
QT_MOC_LITERAL(7, 114, 19), // "selectionEndChanged"
QT_MOC_LITERAL(8, 134, 19), // "selectedTextChanged"
QT_MOC_LITERAL(9, 154, 12), // "colorChanged"
QT_MOC_LITERAL(10, 167, 5), // "color"
QT_MOC_LITERAL(11, 173, 21), // "selectionColorChanged"
QT_MOC_LITERAL(12, 195, 24), // "selectedTextColorChanged"
QT_MOC_LITERAL(13, 220, 11), // "fontChanged"
QT_MOC_LITERAL(14, 232, 4), // "font"
QT_MOC_LITERAL(15, 237, 26), // "horizontalAlignmentChanged"
QT_MOC_LITERAL(16, 264, 26), // "QQuickTextEdit::HAlignment"
QT_MOC_LITERAL(17, 291, 9), // "alignment"
QT_MOC_LITERAL(18, 301, 24), // "verticalAlignmentChanged"
QT_MOC_LITERAL(19, 326, 26), // "QQuickTextEdit::VAlignment"
QT_MOC_LITERAL(20, 353, 15), // "wrapModeChanged"
QT_MOC_LITERAL(21, 369, 16), // "lineCountChanged"
QT_MOC_LITERAL(22, 386, 17), // "textFormatChanged"
QT_MOC_LITERAL(23, 404, 26), // "QQuickTextEdit::TextFormat"
QT_MOC_LITERAL(24, 431, 10), // "textFormat"
QT_MOC_LITERAL(25, 442, 15), // "readOnlyChanged"
QT_MOC_LITERAL(26, 458, 10), // "isReadOnly"
QT_MOC_LITERAL(27, 469, 20), // "cursorVisibleChanged"
QT_MOC_LITERAL(28, 490, 15), // "isCursorVisible"
QT_MOC_LITERAL(29, 506, 21), // "cursorDelegateChanged"
QT_MOC_LITERAL(30, 528, 25), // "activeFocusOnPressChanged"
QT_MOC_LITERAL(31, 554, 20), // "activeFocusOnPressed"
QT_MOC_LITERAL(32, 575, 26), // "persistentSelectionChanged"
QT_MOC_LITERAL(33, 602, 21), // "isPersistentSelection"
QT_MOC_LITERAL(34, 624, 17), // "textMarginChanged"
QT_MOC_LITERAL(35, 642, 10), // "textMargin"
QT_MOC_LITERAL(36, 653, 23), // "selectByKeyboardChanged"
QT_MOC_LITERAL(37, 677, 16), // "selectByKeyboard"
QT_MOC_LITERAL(38, 694, 20), // "selectByMouseChanged"
QT_MOC_LITERAL(39, 715, 13), // "selectByMouse"
QT_MOC_LITERAL(40, 729, 25), // "mouseSelectionModeChanged"
QT_MOC_LITERAL(41, 755, 29), // "QQuickTextEdit::SelectionMode"
QT_MOC_LITERAL(42, 785, 4), // "mode"
QT_MOC_LITERAL(43, 790, 13), // "linkActivated"
QT_MOC_LITERAL(44, 804, 4), // "link"
QT_MOC_LITERAL(45, 809, 11), // "linkHovered"
QT_MOC_LITERAL(46, 821, 15), // "canPasteChanged"
QT_MOC_LITERAL(47, 837, 14), // "canUndoChanged"
QT_MOC_LITERAL(48, 852, 14), // "canRedoChanged"
QT_MOC_LITERAL(49, 867, 27), // "inputMethodComposingChanged"
QT_MOC_LITERAL(50, 895, 35), // "effectiveHorizontalAlignmentC..."
QT_MOC_LITERAL(51, 931, 14), // "baseUrlChanged"
QT_MOC_LITERAL(52, 946, 23), // "inputMethodHintsChanged"
QT_MOC_LITERAL(53, 970, 17), // "renderTypeChanged"
QT_MOC_LITERAL(54, 988, 15), // "editingFinished"
QT_MOC_LITERAL(55, 1004, 14), // "paddingChanged"
QT_MOC_LITERAL(56, 1019, 17), // "topPaddingChanged"
QT_MOC_LITERAL(57, 1037, 18), // "leftPaddingChanged"
QT_MOC_LITERAL(58, 1056, 19), // "rightPaddingChanged"
QT_MOC_LITERAL(59, 1076, 20), // "bottomPaddingChanged"
QT_MOC_LITERAL(60, 1097, 9), // "selectAll"
QT_MOC_LITERAL(61, 1107, 10), // "selectWord"
QT_MOC_LITERAL(62, 1118, 6), // "select"
QT_MOC_LITERAL(63, 1125, 5), // "start"
QT_MOC_LITERAL(64, 1131, 3), // "end"
QT_MOC_LITERAL(65, 1135, 8), // "deselect"
QT_MOC_LITERAL(66, 1144, 13), // "isRightToLeft"
QT_MOC_LITERAL(67, 1158, 3), // "cut"
QT_MOC_LITERAL(68, 1162, 4), // "copy"
QT_MOC_LITERAL(69, 1167, 5), // "paste"
QT_MOC_LITERAL(70, 1173, 4), // "undo"
QT_MOC_LITERAL(71, 1178, 4), // "redo"
QT_MOC_LITERAL(72, 1183, 6), // "insert"
QT_MOC_LITERAL(73, 1190, 8), // "position"
QT_MOC_LITERAL(74, 1199, 4), // "text"
QT_MOC_LITERAL(75, 1204, 6), // "remove"
QT_MOC_LITERAL(76, 1211, 6), // "append"
QT_MOC_LITERAL(77, 1218, 13), // "q_textChanged"
QT_MOC_LITERAL(78, 1232, 16), // "q_contentsChange"
QT_MOC_LITERAL(79, 1249, 15), // "updateSelection"
QT_MOC_LITERAL(80, 1265, 18), // "moveCursorDelegate"
QT_MOC_LITERAL(81, 1284, 12), // "createCursor"
QT_MOC_LITERAL(82, 1297, 17), // "q_canPasteChanged"
QT_MOC_LITERAL(83, 1315, 19), // "updateWholeDocument"
QT_MOC_LITERAL(84, 1335, 15), // "invalidateBlock"
QT_MOC_LITERAL(85, 1351, 10), // "QTextBlock"
QT_MOC_LITERAL(86, 1362, 5), // "block"
QT_MOC_LITERAL(87, 1368, 12), // "updateCursor"
QT_MOC_LITERAL(88, 1381, 17), // "q_updateAlignment"
QT_MOC_LITERAL(89, 1399, 10), // "updateSize"
QT_MOC_LITERAL(90, 1410, 17), // "triggerPreprocess"
QT_MOC_LITERAL(91, 1428, 16), // "inputMethodQuery"
QT_MOC_LITERAL(92, 1445, 20), // "Qt::InputMethodQuery"
QT_MOC_LITERAL(93, 1466, 5), // "query"
QT_MOC_LITERAL(94, 1472, 8), // "argument"
QT_MOC_LITERAL(95, 1481, 19), // "positionToRectangle"
QT_MOC_LITERAL(96, 1501, 10), // "positionAt"
QT_MOC_LITERAL(97, 1512, 1), // "x"
QT_MOC_LITERAL(98, 1514, 1), // "y"
QT_MOC_LITERAL(99, 1516, 19), // "moveCursorSelection"
QT_MOC_LITERAL(100, 1536, 3), // "pos"
QT_MOC_LITERAL(101, 1540, 13), // "SelectionMode"
QT_MOC_LITERAL(102, 1554, 7), // "getText"
QT_MOC_LITERAL(103, 1562, 16), // "getFormattedText"
QT_MOC_LITERAL(104, 1579, 6), // "linkAt"
QT_MOC_LITERAL(105, 1586, 14), // "selectionColor"
QT_MOC_LITERAL(106, 1601, 17), // "selectedTextColor"
QT_MOC_LITERAL(107, 1619, 19), // "horizontalAlignment"
QT_MOC_LITERAL(108, 1639, 10), // "HAlignment"
QT_MOC_LITERAL(109, 1650, 28), // "effectiveHorizontalAlignment"
QT_MOC_LITERAL(110, 1679, 17), // "verticalAlignment"
QT_MOC_LITERAL(111, 1697, 10), // "VAlignment"
QT_MOC_LITERAL(112, 1708, 8), // "wrapMode"
QT_MOC_LITERAL(113, 1717, 8), // "WrapMode"
QT_MOC_LITERAL(114, 1726, 9), // "lineCount"
QT_MOC_LITERAL(115, 1736, 6), // "length"
QT_MOC_LITERAL(116, 1743, 12), // "contentWidth"
QT_MOC_LITERAL(117, 1756, 13), // "contentHeight"
QT_MOC_LITERAL(118, 1770, 12), // "paintedWidth"
QT_MOC_LITERAL(119, 1783, 13), // "paintedHeight"
QT_MOC_LITERAL(120, 1797, 10), // "TextFormat"
QT_MOC_LITERAL(121, 1808, 8), // "readOnly"
QT_MOC_LITERAL(122, 1817, 13), // "cursorVisible"
QT_MOC_LITERAL(123, 1831, 14), // "cursorPosition"
QT_MOC_LITERAL(124, 1846, 15), // "cursorRectangle"
QT_MOC_LITERAL(125, 1862, 14), // "cursorDelegate"
QT_MOC_LITERAL(126, 1877, 14), // "QQmlComponent*"
QT_MOC_LITERAL(127, 1892, 14), // "selectionStart"
QT_MOC_LITERAL(128, 1907, 12), // "selectionEnd"
QT_MOC_LITERAL(129, 1920, 12), // "selectedText"
QT_MOC_LITERAL(130, 1933, 18), // "activeFocusOnPress"
QT_MOC_LITERAL(131, 1952, 19), // "persistentSelection"
QT_MOC_LITERAL(132, 1972, 16), // "inputMethodHints"
QT_MOC_LITERAL(133, 1989, 20), // "Qt::InputMethodHints"
QT_MOC_LITERAL(134, 2010, 18), // "mouseSelectionMode"
QT_MOC_LITERAL(135, 2029, 8), // "canPaste"
QT_MOC_LITERAL(136, 2038, 7), // "canUndo"
QT_MOC_LITERAL(137, 2046, 7), // "canRedo"
QT_MOC_LITERAL(138, 2054, 20), // "inputMethodComposing"
QT_MOC_LITERAL(139, 2075, 7), // "baseUrl"
QT_MOC_LITERAL(140, 2083, 10), // "renderType"
QT_MOC_LITERAL(141, 2094, 10), // "RenderType"
QT_MOC_LITERAL(142, 2105, 12), // "textDocument"
QT_MOC_LITERAL(143, 2118, 19), // "QQuickTextDocument*"
QT_MOC_LITERAL(144, 2138, 11), // "hoveredLink"
QT_MOC_LITERAL(145, 2150, 7), // "padding"
QT_MOC_LITERAL(146, 2158, 10), // "topPadding"
QT_MOC_LITERAL(147, 2169, 11), // "leftPadding"
QT_MOC_LITERAL(148, 2181, 12), // "rightPadding"
QT_MOC_LITERAL(149, 2194, 13), // "bottomPadding"
QT_MOC_LITERAL(150, 2208, 9), // "AlignLeft"
QT_MOC_LITERAL(151, 2218, 10), // "AlignRight"
QT_MOC_LITERAL(152, 2229, 12), // "AlignHCenter"
QT_MOC_LITERAL(153, 2242, 12), // "AlignJustify"
QT_MOC_LITERAL(154, 2255, 8), // "AlignTop"
QT_MOC_LITERAL(155, 2264, 11), // "AlignBottom"
QT_MOC_LITERAL(156, 2276, 12), // "AlignVCenter"
QT_MOC_LITERAL(157, 2289, 9), // "PlainText"
QT_MOC_LITERAL(158, 2299, 8), // "RichText"
QT_MOC_LITERAL(159, 2308, 8), // "AutoText"
QT_MOC_LITERAL(160, 2317, 6), // "NoWrap"
QT_MOC_LITERAL(161, 2324, 8), // "WordWrap"
QT_MOC_LITERAL(162, 2333, 12), // "WrapAnywhere"
QT_MOC_LITERAL(163, 2346, 28), // "WrapAtWordBoundaryOrAnywhere"
QT_MOC_LITERAL(164, 2375, 4), // "Wrap"
QT_MOC_LITERAL(165, 2380, 16), // "SelectCharacters"
QT_MOC_LITERAL(166, 2397, 11), // "SelectWords"
QT_MOC_LITERAL(167, 2409, 11), // "QtRendering"
QT_MOC_LITERAL(168, 2421, 15) // "NativeRendering"

    },
    "QQuickTextEdit\0textChanged\0\0"
    "contentSizeChanged\0cursorPositionChanged\0"
    "cursorRectangleChanged\0selectionStartChanged\0"
    "selectionEndChanged\0selectedTextChanged\0"
    "colorChanged\0color\0selectionColorChanged\0"
    "selectedTextColorChanged\0fontChanged\0"
    "font\0horizontalAlignmentChanged\0"
    "QQuickTextEdit::HAlignment\0alignment\0"
    "verticalAlignmentChanged\0"
    "QQuickTextEdit::VAlignment\0wrapModeChanged\0"
    "lineCountChanged\0textFormatChanged\0"
    "QQuickTextEdit::TextFormat\0textFormat\0"
    "readOnlyChanged\0isReadOnly\0"
    "cursorVisibleChanged\0isCursorVisible\0"
    "cursorDelegateChanged\0activeFocusOnPressChanged\0"
    "activeFocusOnPressed\0persistentSelectionChanged\0"
    "isPersistentSelection\0textMarginChanged\0"
    "textMargin\0selectByKeyboardChanged\0"
    "selectByKeyboard\0selectByMouseChanged\0"
    "selectByMouse\0mouseSelectionModeChanged\0"
    "QQuickTextEdit::SelectionMode\0mode\0"
    "linkActivated\0link\0linkHovered\0"
    "canPasteChanged\0canUndoChanged\0"
    "canRedoChanged\0inputMethodComposingChanged\0"
    "effectiveHorizontalAlignmentChanged\0"
    "baseUrlChanged\0inputMethodHintsChanged\0"
    "renderTypeChanged\0editingFinished\0"
    "paddingChanged\0topPaddingChanged\0"
    "leftPaddingChanged\0rightPaddingChanged\0"
    "bottomPaddingChanged\0selectAll\0"
    "selectWord\0select\0start\0end\0deselect\0"
    "isRightToLeft\0cut\0copy\0paste\0undo\0"
    "redo\0insert\0position\0text\0remove\0"
    "append\0q_textChanged\0q_contentsChange\0"
    "updateSelection\0moveCursorDelegate\0"
    "createCursor\0q_canPasteChanged\0"
    "updateWholeDocument\0invalidateBlock\0"
    "QTextBlock\0block\0updateCursor\0"
    "q_updateAlignment\0updateSize\0"
    "triggerPreprocess\0inputMethodQuery\0"
    "Qt::InputMethodQuery\0query\0argument\0"
    "positionToRectangle\0positionAt\0x\0y\0"
    "moveCursorSelection\0pos\0SelectionMode\0"
    "getText\0getFormattedText\0linkAt\0"
    "selectionColor\0selectedTextColor\0"
    "horizontalAlignment\0HAlignment\0"
    "effectiveHorizontalAlignment\0"
    "verticalAlignment\0VAlignment\0wrapMode\0"
    "WrapMode\0lineCount\0length\0contentWidth\0"
    "contentHeight\0paintedWidth\0paintedHeight\0"
    "TextFormat\0readOnly\0cursorVisible\0"
    "cursorPosition\0cursorRectangle\0"
    "cursorDelegate\0QQmlComponent*\0"
    "selectionStart\0selectionEnd\0selectedText\0"
    "activeFocusOnPress\0persistentSelection\0"
    "inputMethodHints\0Qt::InputMethodHints\0"
    "mouseSelectionMode\0canPaste\0canUndo\0"
    "canRedo\0inputMethodComposing\0baseUrl\0"
    "renderType\0RenderType\0textDocument\0"
    "QQuickTextDocument*\0hoveredLink\0padding\0"
    "topPadding\0leftPadding\0rightPadding\0"
    "bottomPadding\0AlignLeft\0AlignRight\0"
    "AlignHCenter\0AlignJustify\0AlignTop\0"
    "AlignBottom\0AlignVCenter\0PlainText\0"
    "RichText\0AutoText\0NoWrap\0WordWrap\0"
    "WrapAnywhere\0WrapAtWordBoundaryOrAnywhere\0"
    "Wrap\0SelectCharacters\0SelectWords\0"
    "QtRendering\0NativeRendering"
};
#undef QT_MOC_LITERAL

static const uint qt_meta_data_QQuickTextEdit[] = {

 // content:
       7,       // revision
       0,       // classname
       0,    0, // classinfo
      74,   14, // methods
      44,  620, // properties
       6,  840, // enums/sets
       0,    0, // constructors
       0,       // flags
      41,       // signalCount

 // signals: name, argc, parameters, tag, flags
       1,    0,  458,    2, 0x06 /* Public */,
       3,    0,  459,    2, 0x06 /* Public */,
       4,    0,  460,    2, 0x06 /* Public */,
       5,    0,  461,    2, 0x06 /* Public */,
       6,    0,  462,    2, 0x06 /* Public */,
       7,    0,  463,    2, 0x06 /* Public */,
       8,    0,  464,    2, 0x06 /* Public */,
       9,    1,  465,    2, 0x06 /* Public */,
      11,    1,  468,    2, 0x06 /* Public */,
      12,    1,  471,    2, 0x06 /* Public */,
      13,    1,  474,    2, 0x06 /* Public */,
      15,    1,  477,    2, 0x06 /* Public */,
      18,    1,  480,    2, 0x06 /* Public */,
      20,    0,  483,    2, 0x06 /* Public */,
      21,    0,  484,    2, 0x06 /* Public */,
      22,    1,  485,    2, 0x06 /* Public */,
      25,    1,  488,    2, 0x06 /* Public */,
      27,    1,  491,    2, 0x06 /* Public */,
      29,    0,  494,    2, 0x06 /* Public */,
      30,    1,  495,    2, 0x06 /* Public */,
      32,    1,  498,    2, 0x06 /* Public */,
      34,    1,  501,    2, 0x06 /* Public */,
      36,    1,  504,    2, 0x86 /* Public | MethodRevisioned */,
      38,    1,  507,    2, 0x06 /* Public */,
      40,    1,  510,    2, 0x06 /* Public */,
      43,    1,  513,    2, 0x06 /* Public */,
      45,    1,  516,    2, 0x86 /* Public | MethodRevisioned */,
      46,    0,  519,    2, 0x06 /* Public */,
      47,    0,  520,    2, 0x06 /* Public */,
      48,    0,  521,    2, 0x06 /* Public */,
      49,    0,  522,    2, 0x06 /* Public */,
      50,    0,  523,    2, 0x06 /* Public */,
      51,    0,  524,    2, 0x06 /* Public */,
      52,    0,  525,    2, 0x06 /* Public */,
      53,    0,  526,    2, 0x06 /* Public */,
      54,    0,  527,    2, 0x86 /* Public | MethodRevisioned */,
      55,    0,  528,    2, 0x86 /* Public | MethodRevisioned */,
      56,    0,  529,    2, 0x86 /* Public | MethodRevisioned */,
      57,    0,  530,    2, 0x86 /* Public | MethodRevisioned */,
      58,    0,  531,    2, 0x86 /* Public | MethodRevisioned */,
      59,    0,  532,    2, 0x86 /* Public | MethodRevisioned */,

 // slots: name, argc, parameters, tag, flags
      60,    0,  533,    2, 0x0a /* Public */,
      61,    0,  534,    2, 0x0a /* Public */,
      62,    2,  535,    2, 0x0a /* Public */,
      65,    0,  540,    2, 0x0a /* Public */,
      66,    2,  541,    2, 0x0a /* Public */,
      67,    0,  546,    2, 0x0a /* Public */,
      68,    0,  547,    2, 0x0a /* Public */,
      69,    0,  548,    2, 0x0a /* Public */,
      70,    0,  549,    2, 0x0a /* Public */,
      71,    0,  550,    2, 0x0a /* Public */,
      72,    2,  551,    2, 0x0a /* Public */,
      75,    2,  556,    2, 0x0a /* Public */,
      76,    1,  561,    2, 0x8a /* Public | MethodRevisioned */,
      77,    0,  564,    2, 0x08 /* Private */,
      78,    3,  565,    2, 0x08 /* Private */,
      79,    0,  572,    2, 0x08 /* Private */,
      80,    0,  573,    2, 0x08 /* Private */,
      81,    0,  574,    2, 0x08 /* Private */,
      82,    0,  575,    2, 0x08 /* Private */,
      83,    0,  576,    2, 0x08 /* Private */,
      84,    1,  577,    2, 0x08 /* Private */,
      87,    0,  580,    2, 0x08 /* Private */,
      88,    0,  581,    2, 0x08 /* Private */,
      89,    0,  582,    2, 0x08 /* Private */,
      90,    0,  583,    2, 0x08 /* Private */,

 // methods: name, argc, parameters, tag, flags
      91,    2,  584,    2, 0x82 /* Public | MethodRevisioned */,
      95,    1,  589,    2, 0x02 /* Public */,
      96,    2,  592,    2, 0x02 /* Public */,
      99,    1,  597,    2, 0x02 /* Public */,
      99,    2,  600,    2, 0x02 /* Public */,
     102,    2,  605,    2, 0x02 /* Public */,
     103,    2,  610,    2, 0x02 /* Public */,
     104,    2,  615,    2, 0x82 /* Public | MethodRevisioned */,

 // signals: revision
       0,
       0,
       0,
       0,
       0,
       0,
       0,
       0,
       0,
       0,
       0,
       0,
       0,
       0,
       0,
       0,
       0,
       0,
       0,
       0,
       0,
       0,
       1,
       0,
       0,
       0,
       2,
       0,
       0,
       0,
       0,
       0,
       0,
       0,
       0,
       6,
       6,
       6,
       6,
       6,
       6,

 // slots: revision
       0,
       0,
       0,
       0,
       0,
       0,
       0,
       0,
       0,
       0,
       0,
       0,
       2,
       0,
       0,
       0,
       0,
       0,
       0,
       0,
       0,
       0,
       0,
       0,
       0,

 // methods: revision
       4,
       0,
       0,
       0,
       0,
       0,
       0,
       3,

 // signals: parameters
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void, QMetaType::QColor,   10,
    QMetaType::Void, QMetaType::QColor,   10,
    QMetaType::Void, QMetaType::QColor,   10,
    QMetaType::Void, QMetaType::QFont,   14,
    QMetaType::Void, 0x80000000 | 16,   17,
    QMetaType::Void, 0x80000000 | 19,   17,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void, 0x80000000 | 23,   24,
    QMetaType::Void, QMetaType::Bool,   26,
    QMetaType::Void, QMetaType::Bool,   28,
    QMetaType::Void,
    QMetaType::Void, QMetaType::Bool,   31,
    QMetaType::Void, QMetaType::Bool,   33,
    QMetaType::Void, QMetaType::QReal,   35,
    QMetaType::Void, QMetaType::Bool,   37,
    QMetaType::Void, QMetaType::Bool,   39,
    QMetaType::Void, 0x80000000 | 41,   42,
    QMetaType::Void, QMetaType::QString,   44,
    QMetaType::Void, QMetaType::QString,   44,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,

 // slots: parameters
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void, QMetaType::Int, QMetaType::Int,   63,   64,
    QMetaType::Void,
    QMetaType::Bool, QMetaType::Int, QMetaType::Int,   63,   64,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void, QMetaType::Int, QMetaType::QString,   73,   74,
    QMetaType::Void, QMetaType::Int, QMetaType::Int,   63,   64,
    QMetaType::Void, QMetaType::QString,   74,
    QMetaType::Void,
    QMetaType::Void, QMetaType::Int, QMetaType::Int, QMetaType::Int,    2,    2,    2,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void, 0x80000000 | 85,   86,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,

 // methods: parameters
    QMetaType::QVariant, 0x80000000 | 92, QMetaType::QVariant,   93,   94,
    QMetaType::QRectF, QMetaType::Int,    2,
    QMetaType::Int, QMetaType::QReal, QMetaType::QReal,   97,   98,
    QMetaType::Void, QMetaType::Int,  100,
    QMetaType::Void, QMetaType::Int, 0x80000000 | 101,  100,   42,
    QMetaType::QString, QMetaType::Int, QMetaType::Int,   63,   64,
    QMetaType::QString, QMetaType::Int, QMetaType::Int,   63,   64,
    QMetaType::QString, QMetaType::QReal, QMetaType::QReal,   97,   98,

 // properties: name, type, flags
      74, QMetaType::QString, 0x00495103,
      10, QMetaType::QColor, 0x00495103,
     105, QMetaType::QColor, 0x00495103,
     106, QMetaType::QColor, 0x00495103,
      14, QMetaType::QFont, 0x00495103,
     107, 0x80000000 | 108, 0x0049500f,
     109, 0x80000000 | 108, 0x00495009,
     110, 0x80000000 | 111, 0x0049500b,
     112, 0x80000000 | 113, 0x0049510b,
     114, QMetaType::Int, 0x00495001,
     115, QMetaType::Int, 0x00495001,
     116, QMetaType::QReal, 0x00495001,
     117, QMetaType::QReal, 0x00495001,
     118, QMetaType::QReal, 0x00495001,
     119, QMetaType::QReal, 0x00495001,
      24, 0x80000000 | 120, 0x0049510b,
     121, QMetaType::Bool, 0x00495103,
     122, QMetaType::Bool, 0x00495103,
     123, QMetaType::Int, 0x00495103,
     124, QMetaType::QRectF, 0x00495001,
     125, 0x80000000 | 126, 0x0049510b,
     127, QMetaType::Int, 0x00495001,
     128, QMetaType::Int, 0x00495001,
     129, QMetaType::QString, 0x00495001,
     130, QMetaType::Bool, 0x00495003,
     131, QMetaType::Bool, 0x00495103,
      35, QMetaType::QReal, 0x00495103,
     132, 0x80000000 | 133, 0x0049510b,
      37, QMetaType::Bool, 0x00c95103,
      39, QMetaType::Bool, 0x00495103,
     134, 0x80000000 | 101, 0x0049510b,
     135, QMetaType::Bool, 0x00495001,
     136, QMetaType::Bool, 0x00495001,
     137, QMetaType::Bool, 0x00495001,
     138, QMetaType::Bool, 0x00495001,
     139, QMetaType::QUrl, 0x00495107,
     140, 0x80000000 | 141, 0x0049510b,
     142, 0x80000000 | 143, 0x00895c09,
     144, QMetaType::QString, 0x00c95001,
     145, QMetaType::QReal, 0x00c95107,
     146, QMetaType::QReal, 0x00c95107,
     147, QMetaType::QReal, 0x00c95107,
     148, QMetaType::QReal, 0x00c95107,
     149, QMetaType::QReal, 0x00c95107,

 // properties: notify_signal_id
       0,
       7,
       8,
       9,
      10,
      11,
      31,
      12,
      13,
      14,
       0,
       1,
       1,
       1,
       1,
      15,
      16,
      17,
       2,
       3,
      18,
       4,
       5,
       6,
      19,
      20,
      21,
      33,
      22,
      23,
      24,
      27,
      28,
      29,
      30,
      32,
      34,
       0,
      26,
      36,
      37,
      38,
      39,
      40,

 // properties: revision
       0,
       0,
       0,
       0,
       0,
       0,
       0,
       0,
       0,
       0,
       0,
       0,
       0,
       0,
       0,
       0,
       0,
       0,
       0,
       0,
       0,
       0,
       0,
       0,
       0,
       0,
       0,
       0,
       1,
       0,
       0,
       0,
       0,
       0,
       0,
       0,
       0,
       1,
       2,
       6,
       6,
       6,
       6,
       6,

 // enums: name, flags, count, data
     108, 0x0,    4,  864,
     111, 0x0,    3,  872,
     120, 0x0,    3,  878,
     113, 0x0,    5,  884,
     101, 0x0,    2,  894,
     141, 0x0,    2,  898,

 // enum data: key, value
     150, uint(QQuickTextEdit::AlignLeft),
     151, uint(QQuickTextEdit::AlignRight),
     152, uint(QQuickTextEdit::AlignHCenter),
     153, uint(QQuickTextEdit::AlignJustify),
     154, uint(QQuickTextEdit::AlignTop),
     155, uint(QQuickTextEdit::AlignBottom),
     156, uint(QQuickTextEdit::AlignVCenter),
     157, uint(QQuickTextEdit::PlainText),
     158, uint(QQuickTextEdit::RichText),
     159, uint(QQuickTextEdit::AutoText),
     160, uint(QQuickTextEdit::NoWrap),
     161, uint(QQuickTextEdit::WordWrap),
     162, uint(QQuickTextEdit::WrapAnywhere),
     163, uint(QQuickTextEdit::WrapAtWordBoundaryOrAnywhere),
     164, uint(QQuickTextEdit::Wrap),
     165, uint(QQuickTextEdit::SelectCharacters),
     166, uint(QQuickTextEdit::SelectWords),
     167, uint(QQuickTextEdit::QtRendering),
     168, uint(QQuickTextEdit::NativeRendering),

       0        // eod
};

void QQuickTextEdit::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        QQuickTextEdit *_t = static_cast<QQuickTextEdit *>(_o);
        Q_UNUSED(_t)
        switch (_id) {
        case 0: _t->textChanged(); break;
        case 1: _t->contentSizeChanged(); break;
        case 2: _t->cursorPositionChanged(); break;
        case 3: _t->cursorRectangleChanged(); break;
        case 4: _t->selectionStartChanged(); break;
        case 5: _t->selectionEndChanged(); break;
        case 6: _t->selectedTextChanged(); break;
        case 7: _t->colorChanged((*reinterpret_cast< const QColor(*)>(_a[1]))); break;
        case 8: _t->selectionColorChanged((*reinterpret_cast< const QColor(*)>(_a[1]))); break;
        case 9: _t->selectedTextColorChanged((*reinterpret_cast< const QColor(*)>(_a[1]))); break;
        case 10: _t->fontChanged((*reinterpret_cast< const QFont(*)>(_a[1]))); break;
        case 11: _t->horizontalAlignmentChanged((*reinterpret_cast< QQuickTextEdit::HAlignment(*)>(_a[1]))); break;
        case 12: _t->verticalAlignmentChanged((*reinterpret_cast< QQuickTextEdit::VAlignment(*)>(_a[1]))); break;
        case 13: _t->wrapModeChanged(); break;
        case 14: _t->lineCountChanged(); break;
        case 15: _t->textFormatChanged((*reinterpret_cast< QQuickTextEdit::TextFormat(*)>(_a[1]))); break;
        case 16: _t->readOnlyChanged((*reinterpret_cast< bool(*)>(_a[1]))); break;
        case 17: _t->cursorVisibleChanged((*reinterpret_cast< bool(*)>(_a[1]))); break;
        case 18: _t->cursorDelegateChanged(); break;
        case 19: _t->activeFocusOnPressChanged((*reinterpret_cast< bool(*)>(_a[1]))); break;
        case 20: _t->persistentSelectionChanged((*reinterpret_cast< bool(*)>(_a[1]))); break;
        case 21: _t->textMarginChanged((*reinterpret_cast< qreal(*)>(_a[1]))); break;
        case 22: _t->selectByKeyboardChanged((*reinterpret_cast< bool(*)>(_a[1]))); break;
        case 23: _t->selectByMouseChanged((*reinterpret_cast< bool(*)>(_a[1]))); break;
        case 24: _t->mouseSelectionModeChanged((*reinterpret_cast< QQuickTextEdit::SelectionMode(*)>(_a[1]))); break;
        case 25: _t->linkActivated((*reinterpret_cast< const QString(*)>(_a[1]))); break;
        case 26: _t->linkHovered((*reinterpret_cast< const QString(*)>(_a[1]))); break;
        case 27: _t->canPasteChanged(); break;
        case 28: _t->canUndoChanged(); break;
        case 29: _t->canRedoChanged(); break;
        case 30: _t->inputMethodComposingChanged(); break;
        case 31: _t->effectiveHorizontalAlignmentChanged(); break;
        case 32: _t->baseUrlChanged(); break;
        case 33: _t->inputMethodHintsChanged(); break;
        case 34: _t->renderTypeChanged(); break;
        case 35: _t->editingFinished(); break;
        case 36: _t->paddingChanged(); break;
        case 37: _t->topPaddingChanged(); break;
        case 38: _t->leftPaddingChanged(); break;
        case 39: _t->rightPaddingChanged(); break;
        case 40: _t->bottomPaddingChanged(); break;
        case 41: _t->selectAll(); break;
        case 42: _t->selectWord(); break;
        case 43: _t->select((*reinterpret_cast< int(*)>(_a[1])),(*reinterpret_cast< int(*)>(_a[2]))); break;
        case 44: _t->deselect(); break;
        case 45: { bool _r = _t->isRightToLeft((*reinterpret_cast< int(*)>(_a[1])),(*reinterpret_cast< int(*)>(_a[2])));
            if (_a[0]) *reinterpret_cast< bool*>(_a[0]) = _r; }  break;
        case 46: _t->cut(); break;
        case 47: _t->copy(); break;
        case 48: _t->paste(); break;
        case 49: _t->undo(); break;
        case 50: _t->redo(); break;
        case 51: _t->insert((*reinterpret_cast< int(*)>(_a[1])),(*reinterpret_cast< const QString(*)>(_a[2]))); break;
        case 52: _t->remove((*reinterpret_cast< int(*)>(_a[1])),(*reinterpret_cast< int(*)>(_a[2]))); break;
        case 53: _t->append((*reinterpret_cast< const QString(*)>(_a[1]))); break;
        case 54: _t->q_textChanged(); break;
        case 55: _t->q_contentsChange((*reinterpret_cast< int(*)>(_a[1])),(*reinterpret_cast< int(*)>(_a[2])),(*reinterpret_cast< int(*)>(_a[3]))); break;
        case 56: _t->updateSelection(); break;
        case 57: _t->moveCursorDelegate(); break;
        case 58: _t->createCursor(); break;
        case 59: _t->q_canPasteChanged(); break;
        case 60: _t->updateWholeDocument(); break;
        case 61: _t->invalidateBlock((*reinterpret_cast< const QTextBlock(*)>(_a[1]))); break;
        case 62: _t->updateCursor(); break;
        case 63: _t->q_updateAlignment(); break;
        case 64: _t->updateSize(); break;
        case 65: _t->triggerPreprocess(); break;
        case 66: { QVariant _r = _t->inputMethodQuery((*reinterpret_cast< Qt::InputMethodQuery(*)>(_a[1])),(*reinterpret_cast< QVariant(*)>(_a[2])));
            if (_a[0]) *reinterpret_cast< QVariant*>(_a[0]) = _r; }  break;
        case 67: { QRectF _r = _t->positionToRectangle((*reinterpret_cast< int(*)>(_a[1])));
            if (_a[0]) *reinterpret_cast< QRectF*>(_a[0]) = _r; }  break;
        case 68: { int _r = _t->positionAt((*reinterpret_cast< qreal(*)>(_a[1])),(*reinterpret_cast< qreal(*)>(_a[2])));
            if (_a[0]) *reinterpret_cast< int*>(_a[0]) = _r; }  break;
        case 69: _t->moveCursorSelection((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 70: _t->moveCursorSelection((*reinterpret_cast< int(*)>(_a[1])),(*reinterpret_cast< SelectionMode(*)>(_a[2]))); break;
        case 71: { QString _r = _t->getText((*reinterpret_cast< int(*)>(_a[1])),(*reinterpret_cast< int(*)>(_a[2])));
            if (_a[0]) *reinterpret_cast< QString*>(_a[0]) = _r; }  break;
        case 72: { QString _r = _t->getFormattedText((*reinterpret_cast< int(*)>(_a[1])),(*reinterpret_cast< int(*)>(_a[2])));
            if (_a[0]) *reinterpret_cast< QString*>(_a[0]) = _r; }  break;
        case 73: { QString _r = _t->linkAt((*reinterpret_cast< qreal(*)>(_a[1])),(*reinterpret_cast< qreal(*)>(_a[2])));
            if (_a[0]) *reinterpret_cast< QString*>(_a[0]) = _r; }  break;
        default: ;
        }
    } else if (_c == QMetaObject::IndexOfMethod) {
        int *result = reinterpret_cast<int *>(_a[0]);
        void **func = reinterpret_cast<void **>(_a[1]);
        {
            typedef void (QQuickTextEdit::*_t)();
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&QQuickTextEdit::textChanged)) {
                *result = 0;
                return;
            }
        }
        {
            typedef void (QQuickTextEdit::*_t)();
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&QQuickTextEdit::contentSizeChanged)) {
                *result = 1;
                return;
            }
        }
        {
            typedef void (QQuickTextEdit::*_t)();
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&QQuickTextEdit::cursorPositionChanged)) {
                *result = 2;
                return;
            }
        }
        {
            typedef void (QQuickTextEdit::*_t)();
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&QQuickTextEdit::cursorRectangleChanged)) {
                *result = 3;
                return;
            }
        }
        {
            typedef void (QQuickTextEdit::*_t)();
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&QQuickTextEdit::selectionStartChanged)) {
                *result = 4;
                return;
            }
        }
        {
            typedef void (QQuickTextEdit::*_t)();
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&QQuickTextEdit::selectionEndChanged)) {
                *result = 5;
                return;
            }
        }
        {
            typedef void (QQuickTextEdit::*_t)();
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&QQuickTextEdit::selectedTextChanged)) {
                *result = 6;
                return;
            }
        }
        {
            typedef void (QQuickTextEdit::*_t)(const QColor & );
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&QQuickTextEdit::colorChanged)) {
                *result = 7;
                return;
            }
        }
        {
            typedef void (QQuickTextEdit::*_t)(const QColor & );
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&QQuickTextEdit::selectionColorChanged)) {
                *result = 8;
                return;
            }
        }
        {
            typedef void (QQuickTextEdit::*_t)(const QColor & );
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&QQuickTextEdit::selectedTextColorChanged)) {
                *result = 9;
                return;
            }
        }
        {
            typedef void (QQuickTextEdit::*_t)(const QFont & );
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&QQuickTextEdit::fontChanged)) {
                *result = 10;
                return;
            }
        }
        {
            typedef void (QQuickTextEdit::*_t)(QQuickTextEdit::HAlignment );
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&QQuickTextEdit::horizontalAlignmentChanged)) {
                *result = 11;
                return;
            }
        }
        {
            typedef void (QQuickTextEdit::*_t)(QQuickTextEdit::VAlignment );
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&QQuickTextEdit::verticalAlignmentChanged)) {
                *result = 12;
                return;
            }
        }
        {
            typedef void (QQuickTextEdit::*_t)();
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&QQuickTextEdit::wrapModeChanged)) {
                *result = 13;
                return;
            }
        }
        {
            typedef void (QQuickTextEdit::*_t)();
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&QQuickTextEdit::lineCountChanged)) {
                *result = 14;
                return;
            }
        }
        {
            typedef void (QQuickTextEdit::*_t)(QQuickTextEdit::TextFormat );
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&QQuickTextEdit::textFormatChanged)) {
                *result = 15;
                return;
            }
        }
        {
            typedef void (QQuickTextEdit::*_t)(bool );
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&QQuickTextEdit::readOnlyChanged)) {
                *result = 16;
                return;
            }
        }
        {
            typedef void (QQuickTextEdit::*_t)(bool );
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&QQuickTextEdit::cursorVisibleChanged)) {
                *result = 17;
                return;
            }
        }
        {
            typedef void (QQuickTextEdit::*_t)();
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&QQuickTextEdit::cursorDelegateChanged)) {
                *result = 18;
                return;
            }
        }
        {
            typedef void (QQuickTextEdit::*_t)(bool );
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&QQuickTextEdit::activeFocusOnPressChanged)) {
                *result = 19;
                return;
            }
        }
        {
            typedef void (QQuickTextEdit::*_t)(bool );
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&QQuickTextEdit::persistentSelectionChanged)) {
                *result = 20;
                return;
            }
        }
        {
            typedef void (QQuickTextEdit::*_t)(qreal );
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&QQuickTextEdit::textMarginChanged)) {
                *result = 21;
                return;
            }
        }
        {
            typedef void (QQuickTextEdit::*_t)(bool );
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&QQuickTextEdit::selectByKeyboardChanged)) {
                *result = 22;
                return;
            }
        }
        {
            typedef void (QQuickTextEdit::*_t)(bool );
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&QQuickTextEdit::selectByMouseChanged)) {
                *result = 23;
                return;
            }
        }
        {
            typedef void (QQuickTextEdit::*_t)(QQuickTextEdit::SelectionMode );
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&QQuickTextEdit::mouseSelectionModeChanged)) {
                *result = 24;
                return;
            }
        }
        {
            typedef void (QQuickTextEdit::*_t)(const QString & );
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&QQuickTextEdit::linkActivated)) {
                *result = 25;
                return;
            }
        }
        {
            typedef void (QQuickTextEdit::*_t)(const QString & );
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&QQuickTextEdit::linkHovered)) {
                *result = 26;
                return;
            }
        }
        {
            typedef void (QQuickTextEdit::*_t)();
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&QQuickTextEdit::canPasteChanged)) {
                *result = 27;
                return;
            }
        }
        {
            typedef void (QQuickTextEdit::*_t)();
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&QQuickTextEdit::canUndoChanged)) {
                *result = 28;
                return;
            }
        }
        {
            typedef void (QQuickTextEdit::*_t)();
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&QQuickTextEdit::canRedoChanged)) {
                *result = 29;
                return;
            }
        }
        {
            typedef void (QQuickTextEdit::*_t)();
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&QQuickTextEdit::inputMethodComposingChanged)) {
                *result = 30;
                return;
            }
        }
        {
            typedef void (QQuickTextEdit::*_t)();
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&QQuickTextEdit::effectiveHorizontalAlignmentChanged)) {
                *result = 31;
                return;
            }
        }
        {
            typedef void (QQuickTextEdit::*_t)();
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&QQuickTextEdit::baseUrlChanged)) {
                *result = 32;
                return;
            }
        }
        {
            typedef void (QQuickTextEdit::*_t)();
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&QQuickTextEdit::inputMethodHintsChanged)) {
                *result = 33;
                return;
            }
        }
        {
            typedef void (QQuickTextEdit::*_t)();
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&QQuickTextEdit::renderTypeChanged)) {
                *result = 34;
                return;
            }
        }
        {
            typedef void (QQuickTextEdit::*_t)();
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&QQuickTextEdit::editingFinished)) {
                *result = 35;
                return;
            }
        }
        {
            typedef void (QQuickTextEdit::*_t)();
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&QQuickTextEdit::paddingChanged)) {
                *result = 36;
                return;
            }
        }
        {
            typedef void (QQuickTextEdit::*_t)();
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&QQuickTextEdit::topPaddingChanged)) {
                *result = 37;
                return;
            }
        }
        {
            typedef void (QQuickTextEdit::*_t)();
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&QQuickTextEdit::leftPaddingChanged)) {
                *result = 38;
                return;
            }
        }
        {
            typedef void (QQuickTextEdit::*_t)();
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&QQuickTextEdit::rightPaddingChanged)) {
                *result = 39;
                return;
            }
        }
        {
            typedef void (QQuickTextEdit::*_t)();
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&QQuickTextEdit::bottomPaddingChanged)) {
                *result = 40;
                return;
            }
        }
    } else if (_c == QMetaObject::RegisterPropertyMetaType) {
        switch (_id) {
        default: *reinterpret_cast<int*>(_a[0]) = -1; break;
        case 20:
            *reinterpret_cast<int*>(_a[0]) = qRegisterMetaType< QQmlComponent* >(); break;
        }
    }

#ifndef QT_NO_PROPERTIES
    else if (_c == QMetaObject::ReadProperty) {
        QQuickTextEdit *_t = static_cast<QQuickTextEdit *>(_o);
        Q_UNUSED(_t)
        void *_v = _a[0];
        switch (_id) {
        case 0: *reinterpret_cast< QString*>(_v) = _t->text(); break;
        case 1: *reinterpret_cast< QColor*>(_v) = _t->color(); break;
        case 2: *reinterpret_cast< QColor*>(_v) = _t->selectionColor(); break;
        case 3: *reinterpret_cast< QColor*>(_v) = _t->selectedTextColor(); break;
        case 4: *reinterpret_cast< QFont*>(_v) = _t->font(); break;
        case 5: *reinterpret_cast< HAlignment*>(_v) = _t->hAlign(); break;
        case 6: *reinterpret_cast< HAlignment*>(_v) = _t->effectiveHAlign(); break;
        case 7: *reinterpret_cast< VAlignment*>(_v) = _t->vAlign(); break;
        case 8: *reinterpret_cast< WrapMode*>(_v) = _t->wrapMode(); break;
        case 9: *reinterpret_cast< int*>(_v) = _t->lineCount(); break;
        case 10: *reinterpret_cast< int*>(_v) = _t->length(); break;
        case 11: *reinterpret_cast< qreal*>(_v) = _t->contentWidth(); break;
        case 12: *reinterpret_cast< qreal*>(_v) = _t->contentHeight(); break;
        case 13: *reinterpret_cast< qreal*>(_v) = _t->contentWidth(); break;
        case 14: *reinterpret_cast< qreal*>(_v) = _t->contentHeight(); break;
        case 15: *reinterpret_cast< TextFormat*>(_v) = _t->textFormat(); break;
        case 16: *reinterpret_cast< bool*>(_v) = _t->isReadOnly(); break;
        case 17: *reinterpret_cast< bool*>(_v) = _t->isCursorVisible(); break;
        case 18: *reinterpret_cast< int*>(_v) = _t->cursorPosition(); break;
        case 19: *reinterpret_cast< QRectF*>(_v) = _t->cursorRectangle(); break;
        case 20: *reinterpret_cast< QQmlComponent**>(_v) = _t->cursorDelegate(); break;
        case 21: *reinterpret_cast< int*>(_v) = _t->selectionStart(); break;
        case 22: *reinterpret_cast< int*>(_v) = _t->selectionEnd(); break;
        case 23: *reinterpret_cast< QString*>(_v) = _t->selectedText(); break;
        case 24: *reinterpret_cast< bool*>(_v) = _t->focusOnPress(); break;
        case 25: *reinterpret_cast< bool*>(_v) = _t->persistentSelection(); break;
        case 26: *reinterpret_cast< qreal*>(_v) = _t->textMargin(); break;
        case 27: *reinterpret_cast< Qt::InputMethodHints*>(_v) = _t->inputMethodHints(); break;
        case 28: *reinterpret_cast< bool*>(_v) = _t->selectByKeyboard(); break;
        case 29: *reinterpret_cast< bool*>(_v) = _t->selectByMouse(); break;
        case 30: *reinterpret_cast< SelectionMode*>(_v) = _t->mouseSelectionMode(); break;
        case 31: *reinterpret_cast< bool*>(_v) = _t->canPaste(); break;
        case 32: *reinterpret_cast< bool*>(_v) = _t->canUndo(); break;
        case 33: *reinterpret_cast< bool*>(_v) = _t->canRedo(); break;
        case 34: *reinterpret_cast< bool*>(_v) = _t->isInputMethodComposing(); break;
        case 35: *reinterpret_cast< QUrl*>(_v) = _t->baseUrl(); break;
        case 36: *reinterpret_cast< RenderType*>(_v) = _t->renderType(); break;
        case 37: *reinterpret_cast< QQuickTextDocument**>(_v) = _t->textDocument(); break;
        case 38: *reinterpret_cast< QString*>(_v) = _t->hoveredLink(); break;
        case 39: *reinterpret_cast< qreal*>(_v) = _t->padding(); break;
        case 40: *reinterpret_cast< qreal*>(_v) = _t->topPadding(); break;
        case 41: *reinterpret_cast< qreal*>(_v) = _t->leftPadding(); break;
        case 42: *reinterpret_cast< qreal*>(_v) = _t->rightPadding(); break;
        case 43: *reinterpret_cast< qreal*>(_v) = _t->bottomPadding(); break;
        default: break;
        }
    } else if (_c == QMetaObject::WriteProperty) {
        QQuickTextEdit *_t = static_cast<QQuickTextEdit *>(_o);
        Q_UNUSED(_t)
        void *_v = _a[0];
        switch (_id) {
        case 0: _t->setText(*reinterpret_cast< QString*>(_v)); break;
        case 1: _t->setColor(*reinterpret_cast< QColor*>(_v)); break;
        case 2: _t->setSelectionColor(*reinterpret_cast< QColor*>(_v)); break;
        case 3: _t->setSelectedTextColor(*reinterpret_cast< QColor*>(_v)); break;
        case 4: _t->setFont(*reinterpret_cast< QFont*>(_v)); break;
        case 5: _t->setHAlign(*reinterpret_cast< HAlignment*>(_v)); break;
        case 7: _t->setVAlign(*reinterpret_cast< VAlignment*>(_v)); break;
        case 8: _t->setWrapMode(*reinterpret_cast< WrapMode*>(_v)); break;
        case 15: _t->setTextFormat(*reinterpret_cast< TextFormat*>(_v)); break;
        case 16: _t->setReadOnly(*reinterpret_cast< bool*>(_v)); break;
        case 17: _t->setCursorVisible(*reinterpret_cast< bool*>(_v)); break;
        case 18: _t->setCursorPosition(*reinterpret_cast< int*>(_v)); break;
        case 20: _t->setCursorDelegate(*reinterpret_cast< QQmlComponent**>(_v)); break;
        case 24: _t->setFocusOnPress(*reinterpret_cast< bool*>(_v)); break;
        case 25: _t->setPersistentSelection(*reinterpret_cast< bool*>(_v)); break;
        case 26: _t->setTextMargin(*reinterpret_cast< qreal*>(_v)); break;
        case 27: _t->setInputMethodHints(*reinterpret_cast< Qt::InputMethodHints*>(_v)); break;
        case 28: _t->setSelectByKeyboard(*reinterpret_cast< bool*>(_v)); break;
        case 29: _t->setSelectByMouse(*reinterpret_cast< bool*>(_v)); break;
        case 30: _t->setMouseSelectionMode(*reinterpret_cast< SelectionMode*>(_v)); break;
        case 35: _t->setBaseUrl(*reinterpret_cast< QUrl*>(_v)); break;
        case 36: _t->setRenderType(*reinterpret_cast< RenderType*>(_v)); break;
        case 39: _t->setPadding(*reinterpret_cast< qreal*>(_v)); break;
        case 40: _t->setTopPadding(*reinterpret_cast< qreal*>(_v)); break;
        case 41: _t->setLeftPadding(*reinterpret_cast< qreal*>(_v)); break;
        case 42: _t->setRightPadding(*reinterpret_cast< qreal*>(_v)); break;
        case 43: _t->setBottomPadding(*reinterpret_cast< qreal*>(_v)); break;
        default: break;
        }
    } else if (_c == QMetaObject::ResetProperty) {
        QQuickTextEdit *_t = static_cast<QQuickTextEdit *>(_o);
        Q_UNUSED(_t)
        switch (_id) {
        case 5: _t->resetHAlign(); break;
        case 35: _t->resetBaseUrl(); break;
        case 39: _t->resetPadding(); break;
        case 40: _t->resetTopPadding(); break;
        case 41: _t->resetLeftPadding(); break;
        case 42: _t->resetRightPadding(); break;
        case 43: _t->resetBottomPadding(); break;
        default: break;
        }
    }
#endif // QT_NO_PROPERTIES
}

const QMetaObject QQuickTextEdit::staticMetaObject = {
    { &QQuickImplicitSizeItem::staticMetaObject, qt_meta_stringdata_QQuickTextEdit.data,
      qt_meta_data_QQuickTextEdit,  qt_static_metacall, Q_NULLPTR, Q_NULLPTR}
};


const QMetaObject *QQuickTextEdit::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->dynamicMetaObject() : &staticMetaObject;
}

void *QQuickTextEdit::qt_metacast(const char *_clname)
{
    if (!_clname) return Q_NULLPTR;
    if (!strcmp(_clname, qt_meta_stringdata_QQuickTextEdit.stringdata0))
        return static_cast<void*>(const_cast< QQuickTextEdit*>(this));
    return QQuickImplicitSizeItem::qt_metacast(_clname);
}

int QQuickTextEdit::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QQuickImplicitSizeItem::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 74)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 74;
    } else if (_c == QMetaObject::RegisterMethodArgumentMetaType) {
        if (_id < 74)
            *reinterpret_cast<int*>(_a[0]) = -1;
        _id -= 74;
    }
#ifndef QT_NO_PROPERTIES
   else if (_c == QMetaObject::ReadProperty || _c == QMetaObject::WriteProperty
            || _c == QMetaObject::ResetProperty || _c == QMetaObject::RegisterPropertyMetaType) {
        qt_static_metacall(this, _c, _id, _a);
        _id -= 44;
    } else if (_c == QMetaObject::QueryPropertyDesignable) {
        _id -= 44;
    } else if (_c == QMetaObject::QueryPropertyScriptable) {
        _id -= 44;
    } else if (_c == QMetaObject::QueryPropertyStored) {
        _id -= 44;
    } else if (_c == QMetaObject::QueryPropertyEditable) {
        _id -= 44;
    } else if (_c == QMetaObject::QueryPropertyUser) {
        _id -= 44;
    }
#endif // QT_NO_PROPERTIES
    return _id;
}

// SIGNAL 0
void QQuickTextEdit::textChanged()
{
    QMetaObject::activate(this, &staticMetaObject, 0, Q_NULLPTR);
}

// SIGNAL 1
void QQuickTextEdit::contentSizeChanged()
{
    QMetaObject::activate(this, &staticMetaObject, 1, Q_NULLPTR);
}

// SIGNAL 2
void QQuickTextEdit::cursorPositionChanged()
{
    QMetaObject::activate(this, &staticMetaObject, 2, Q_NULLPTR);
}

// SIGNAL 3
void QQuickTextEdit::cursorRectangleChanged()
{
    QMetaObject::activate(this, &staticMetaObject, 3, Q_NULLPTR);
}

// SIGNAL 4
void QQuickTextEdit::selectionStartChanged()
{
    QMetaObject::activate(this, &staticMetaObject, 4, Q_NULLPTR);
}

// SIGNAL 5
void QQuickTextEdit::selectionEndChanged()
{
    QMetaObject::activate(this, &staticMetaObject, 5, Q_NULLPTR);
}

// SIGNAL 6
void QQuickTextEdit::selectedTextChanged()
{
    QMetaObject::activate(this, &staticMetaObject, 6, Q_NULLPTR);
}

// SIGNAL 7
void QQuickTextEdit::colorChanged(const QColor & _t1)
{
    void *_a[] = { Q_NULLPTR, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 7, _a);
}

// SIGNAL 8
void QQuickTextEdit::selectionColorChanged(const QColor & _t1)
{
    void *_a[] = { Q_NULLPTR, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 8, _a);
}

// SIGNAL 9
void QQuickTextEdit::selectedTextColorChanged(const QColor & _t1)
{
    void *_a[] = { Q_NULLPTR, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 9, _a);
}

// SIGNAL 10
void QQuickTextEdit::fontChanged(const QFont & _t1)
{
    void *_a[] = { Q_NULLPTR, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 10, _a);
}

// SIGNAL 11
void QQuickTextEdit::horizontalAlignmentChanged(QQuickTextEdit::HAlignment _t1)
{
    void *_a[] = { Q_NULLPTR, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 11, _a);
}

// SIGNAL 12
void QQuickTextEdit::verticalAlignmentChanged(QQuickTextEdit::VAlignment _t1)
{
    void *_a[] = { Q_NULLPTR, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 12, _a);
}

// SIGNAL 13
void QQuickTextEdit::wrapModeChanged()
{
    QMetaObject::activate(this, &staticMetaObject, 13, Q_NULLPTR);
}

// SIGNAL 14
void QQuickTextEdit::lineCountChanged()
{
    QMetaObject::activate(this, &staticMetaObject, 14, Q_NULLPTR);
}

// SIGNAL 15
void QQuickTextEdit::textFormatChanged(QQuickTextEdit::TextFormat _t1)
{
    void *_a[] = { Q_NULLPTR, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 15, _a);
}

// SIGNAL 16
void QQuickTextEdit::readOnlyChanged(bool _t1)
{
    void *_a[] = { Q_NULLPTR, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 16, _a);
}

// SIGNAL 17
void QQuickTextEdit::cursorVisibleChanged(bool _t1)
{
    void *_a[] = { Q_NULLPTR, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 17, _a);
}

// SIGNAL 18
void QQuickTextEdit::cursorDelegateChanged()
{
    QMetaObject::activate(this, &staticMetaObject, 18, Q_NULLPTR);
}

// SIGNAL 19
void QQuickTextEdit::activeFocusOnPressChanged(bool _t1)
{
    void *_a[] = { Q_NULLPTR, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 19, _a);
}

// SIGNAL 20
void QQuickTextEdit::persistentSelectionChanged(bool _t1)
{
    void *_a[] = { Q_NULLPTR, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 20, _a);
}

// SIGNAL 21
void QQuickTextEdit::textMarginChanged(qreal _t1)
{
    void *_a[] = { Q_NULLPTR, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 21, _a);
}

// SIGNAL 22
void QQuickTextEdit::selectByKeyboardChanged(bool _t1)
{
    void *_a[] = { Q_NULLPTR, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 22, _a);
}

// SIGNAL 23
void QQuickTextEdit::selectByMouseChanged(bool _t1)
{
    void *_a[] = { Q_NULLPTR, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 23, _a);
}

// SIGNAL 24
void QQuickTextEdit::mouseSelectionModeChanged(QQuickTextEdit::SelectionMode _t1)
{
    void *_a[] = { Q_NULLPTR, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 24, _a);
}

// SIGNAL 25
void QQuickTextEdit::linkActivated(const QString & _t1)
{
    void *_a[] = { Q_NULLPTR, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 25, _a);
}

// SIGNAL 26
void QQuickTextEdit::linkHovered(const QString & _t1)
{
    void *_a[] = { Q_NULLPTR, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 26, _a);
}

// SIGNAL 27
void QQuickTextEdit::canPasteChanged()
{
    QMetaObject::activate(this, &staticMetaObject, 27, Q_NULLPTR);
}

// SIGNAL 28
void QQuickTextEdit::canUndoChanged()
{
    QMetaObject::activate(this, &staticMetaObject, 28, Q_NULLPTR);
}

// SIGNAL 29
void QQuickTextEdit::canRedoChanged()
{
    QMetaObject::activate(this, &staticMetaObject, 29, Q_NULLPTR);
}

// SIGNAL 30
void QQuickTextEdit::inputMethodComposingChanged()
{
    QMetaObject::activate(this, &staticMetaObject, 30, Q_NULLPTR);
}

// SIGNAL 31
void QQuickTextEdit::effectiveHorizontalAlignmentChanged()
{
    QMetaObject::activate(this, &staticMetaObject, 31, Q_NULLPTR);
}

// SIGNAL 32
void QQuickTextEdit::baseUrlChanged()
{
    QMetaObject::activate(this, &staticMetaObject, 32, Q_NULLPTR);
}

// SIGNAL 33
void QQuickTextEdit::inputMethodHintsChanged()
{
    QMetaObject::activate(this, &staticMetaObject, 33, Q_NULLPTR);
}

// SIGNAL 34
void QQuickTextEdit::renderTypeChanged()
{
    QMetaObject::activate(this, &staticMetaObject, 34, Q_NULLPTR);
}

// SIGNAL 35
void QQuickTextEdit::editingFinished()
{
    QMetaObject::activate(this, &staticMetaObject, 35, Q_NULLPTR);
}

// SIGNAL 36
void QQuickTextEdit::paddingChanged()
{
    QMetaObject::activate(this, &staticMetaObject, 36, Q_NULLPTR);
}

// SIGNAL 37
void QQuickTextEdit::topPaddingChanged()
{
    QMetaObject::activate(this, &staticMetaObject, 37, Q_NULLPTR);
}

// SIGNAL 38
void QQuickTextEdit::leftPaddingChanged()
{
    QMetaObject::activate(this, &staticMetaObject, 38, Q_NULLPTR);
}

// SIGNAL 39
void QQuickTextEdit::rightPaddingChanged()
{
    QMetaObject::activate(this, &staticMetaObject, 39, Q_NULLPTR);
}

// SIGNAL 40
void QQuickTextEdit::bottomPaddingChanged()
{
    QMetaObject::activate(this, &staticMetaObject, 40, Q_NULLPTR);
}
QT_END_MOC_NAMESPACE
