/****************************************************************************
** Meta object code from reading C++ file 'qquickshadereffectnode_p.h'
**
** Created by: The Qt Meta Object Compiler version 67 (Qt 5.6.3)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "../../items/qquickshadereffectnode_p.h"
#include <QtCore/qbytearray.h>
#include <QtCore/qmetatype.h>
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'qquickshadereffectnode_p.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 67
#error "This file was generated using the moc from 5.6.3. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
struct qt_meta_stringdata_QQuickShaderEffectNode_t {
    QByteArrayData data[8];
    char stringdata0[113];
};
#define QT_MOC_LITERAL(idx, ofs, len) \
    Q_STATIC_BYTE_ARRAY_DATA_HEADER_INITIALIZER_WITH_OFFSET(len, \
    qptrdiff(offsetof(qt_meta_stringdata_QQuickShaderEffectNode_t, stringdata0) + ofs \
        - idx * sizeof(QByteArrayData)) \
    )
static const qt_meta_stringdata_QQuickShaderEffectNode_t qt_meta_stringdata_QQuickShaderEffectNode = {
    {
QT_MOC_LITERAL(0, 0, 22), // "QQuickShaderEffectNode"
QT_MOC_LITERAL(1, 23, 19), // "logAndStatusChanged"
QT_MOC_LITERAL(2, 43, 0), // ""
QT_MOC_LITERAL(3, 44, 6), // "status"
QT_MOC_LITERAL(4, 51, 12), // "dirtyTexture"
QT_MOC_LITERAL(5, 64, 16), // "markDirtyTexture"
QT_MOC_LITERAL(6, 81, 24), // "textureProviderDestroyed"
QT_MOC_LITERAL(7, 106, 6) // "object"

    },
    "QQuickShaderEffectNode\0logAndStatusChanged\0"
    "\0status\0dirtyTexture\0markDirtyTexture\0"
    "textureProviderDestroyed\0object"
};
#undef QT_MOC_LITERAL

static const uint qt_meta_data_QQuickShaderEffectNode[] = {

 // content:
       7,       // revision
       0,       // classname
       0,    0, // classinfo
       4,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       2,       // signalCount

 // signals: name, argc, parameters, tag, flags
       1,    2,   34,    2, 0x06 /* Public */,
       4,    0,   39,    2, 0x06 /* Public */,

 // slots: name, argc, parameters, tag, flags
       5,    0,   40,    2, 0x08 /* Private */,
       6,    1,   41,    2, 0x08 /* Private */,

 // signals: parameters
    QMetaType::Void, QMetaType::QString, QMetaType::Int,    2,    3,
    QMetaType::Void,

 // slots: parameters
    QMetaType::Void,
    QMetaType::Void, QMetaType::QObjectStar,    7,

       0        // eod
};

void QQuickShaderEffectNode::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        QQuickShaderEffectNode *_t = static_cast<QQuickShaderEffectNode *>(_o);
        Q_UNUSED(_t)
        switch (_id) {
        case 0: _t->logAndStatusChanged((*reinterpret_cast< const QString(*)>(_a[1])),(*reinterpret_cast< int(*)>(_a[2]))); break;
        case 1: _t->dirtyTexture(); break;
        case 2: _t->markDirtyTexture(); break;
        case 3: _t->textureProviderDestroyed((*reinterpret_cast< QObject*(*)>(_a[1]))); break;
        default: ;
        }
    } else if (_c == QMetaObject::IndexOfMethod) {
        int *result = reinterpret_cast<int *>(_a[0]);
        void **func = reinterpret_cast<void **>(_a[1]);
        {
            typedef void (QQuickShaderEffectNode::*_t)(const QString & , int );
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&QQuickShaderEffectNode::logAndStatusChanged)) {
                *result = 0;
                return;
            }
        }
        {
            typedef void (QQuickShaderEffectNode::*_t)();
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&QQuickShaderEffectNode::dirtyTexture)) {
                *result = 1;
                return;
            }
        }
    }
}

const QMetaObject QQuickShaderEffectNode::staticMetaObject = {
    { &QObject::staticMetaObject, qt_meta_stringdata_QQuickShaderEffectNode.data,
      qt_meta_data_QQuickShaderEffectNode,  qt_static_metacall, Q_NULLPTR, Q_NULLPTR}
};


const QMetaObject *QQuickShaderEffectNode::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->dynamicMetaObject() : &staticMetaObject;
}

void *QQuickShaderEffectNode::qt_metacast(const char *_clname)
{
    if (!_clname) return Q_NULLPTR;
    if (!strcmp(_clname, qt_meta_stringdata_QQuickShaderEffectNode.stringdata0))
        return static_cast<void*>(const_cast< QQuickShaderEffectNode*>(this));
    if (!strcmp(_clname, "QSGGeometryNode"))
        return static_cast< QSGGeometryNode*>(const_cast< QQuickShaderEffectNode*>(this));
    return QObject::qt_metacast(_clname);
}

int QQuickShaderEffectNode::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QObject::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 4)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 4;
    } else if (_c == QMetaObject::RegisterMethodArgumentMetaType) {
        if (_id < 4)
            *reinterpret_cast<int*>(_a[0]) = -1;
        _id -= 4;
    }
    return _id;
}

// SIGNAL 0
void QQuickShaderEffectNode::logAndStatusChanged(const QString & _t1, int _t2)
{
    void *_a[] = { Q_NULLPTR, const_cast<void*>(reinterpret_cast<const void*>(&_t1)), const_cast<void*>(reinterpret_cast<const void*>(&_t2)) };
    QMetaObject::activate(this, &staticMetaObject, 0, _a);
}

// SIGNAL 1
void QQuickShaderEffectNode::dirtyTexture()
{
    QMetaObject::activate(this, &staticMetaObject, 1, Q_NULLPTR);
}
QT_END_MOC_NAMESPACE
