/****************************************************************************
** Meta object code from reading C++ file 'qquickitemview_p.h'
**
** Created by: The Qt Meta Object Compiler version 67 (Qt 5.6.3)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "../../items/qquickitemview_p.h"
#include <QtCore/qbytearray.h>
#include <QtCore/qmetatype.h>
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'qquickitemview_p.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 67
#error "This file was generated using the moc from 5.6.3. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
struct qt_meta_stringdata_QQuickItemView_t {
    QByteArrayData data[108];
    char stringdata0[1745];
};
#define QT_MOC_LITERAL(idx, ofs, len) \
    Q_STATIC_BYTE_ARRAY_DATA_HEADER_INITIALIZER_WITH_OFFSET(len, \
    qptrdiff(offsetof(qt_meta_stringdata_QQuickItemView_t, stringdata0) + ofs \
        - idx * sizeof(QByteArrayData)) \
    )
static const qt_meta_stringdata_QQuickItemView_t qt_meta_stringdata_QQuickItemView = {
    {
QT_MOC_LITERAL(0, 0, 14), // "QQuickItemView"
QT_MOC_LITERAL(1, 15, 12), // "modelChanged"
QT_MOC_LITERAL(2, 28, 0), // ""
QT_MOC_LITERAL(3, 29, 15), // "delegateChanged"
QT_MOC_LITERAL(4, 45, 12), // "countChanged"
QT_MOC_LITERAL(5, 58, 19), // "currentIndexChanged"
QT_MOC_LITERAL(6, 78, 18), // "currentItemChanged"
QT_MOC_LITERAL(7, 97, 25), // "keyNavigationWrapsChanged"
QT_MOC_LITERAL(8, 123, 18), // "cacheBufferChanged"
QT_MOC_LITERAL(9, 142, 29), // "displayMarginBeginningChanged"
QT_MOC_LITERAL(10, 172, 23), // "displayMarginEndChanged"
QT_MOC_LITERAL(11, 196, 22), // "layoutDirectionChanged"
QT_MOC_LITERAL(12, 219, 31), // "effectiveLayoutDirectionChanged"
QT_MOC_LITERAL(13, 251, 30), // "verticalLayoutDirectionChanged"
QT_MOC_LITERAL(14, 282, 13), // "headerChanged"
QT_MOC_LITERAL(15, 296, 13), // "footerChanged"
QT_MOC_LITERAL(16, 310, 17), // "headerItemChanged"
QT_MOC_LITERAL(17, 328, 17), // "footerItemChanged"
QT_MOC_LITERAL(18, 346, 25), // "populateTransitionChanged"
QT_MOC_LITERAL(19, 372, 20), // "addTransitionChanged"
QT_MOC_LITERAL(20, 393, 29), // "addDisplacedTransitionChanged"
QT_MOC_LITERAL(21, 423, 21), // "moveTransitionChanged"
QT_MOC_LITERAL(22, 445, 30), // "moveDisplacedTransitionChanged"
QT_MOC_LITERAL(23, 476, 23), // "removeTransitionChanged"
QT_MOC_LITERAL(24, 500, 32), // "removeDisplacedTransitionChanged"
QT_MOC_LITERAL(25, 533, 26), // "displacedTransitionChanged"
QT_MOC_LITERAL(26, 560, 16), // "highlightChanged"
QT_MOC_LITERAL(27, 577, 20), // "highlightItemChanged"
QT_MOC_LITERAL(28, 598, 34), // "highlightFollowsCurrentItemCh..."
QT_MOC_LITERAL(29, 633, 25), // "highlightRangeModeChanged"
QT_MOC_LITERAL(30, 659, 30), // "preferredHighlightBeginChanged"
QT_MOC_LITERAL(31, 690, 28), // "preferredHighlightEndChanged"
QT_MOC_LITERAL(32, 719, 28), // "highlightMoveDurationChanged"
QT_MOC_LITERAL(33, 748, 14), // "destroyRemoved"
QT_MOC_LITERAL(34, 763, 11), // "createdItem"
QT_MOC_LITERAL(35, 775, 5), // "index"
QT_MOC_LITERAL(36, 781, 4), // "item"
QT_MOC_LITERAL(37, 786, 8), // "initItem"
QT_MOC_LITERAL(38, 795, 12), // "modelUpdated"
QT_MOC_LITERAL(39, 808, 13), // "QQmlChangeSet"
QT_MOC_LITERAL(40, 822, 9), // "changeSet"
QT_MOC_LITERAL(41, 832, 5), // "reset"
QT_MOC_LITERAL(42, 838, 14), // "destroyingItem"
QT_MOC_LITERAL(43, 853, 11), // "animStopped"
QT_MOC_LITERAL(44, 865, 22), // "trackedPositionChanged"
QT_MOC_LITERAL(45, 888, 19), // "positionViewAtIndex"
QT_MOC_LITERAL(46, 908, 4), // "mode"
QT_MOC_LITERAL(47, 913, 7), // "indexAt"
QT_MOC_LITERAL(48, 921, 1), // "x"
QT_MOC_LITERAL(49, 923, 1), // "y"
QT_MOC_LITERAL(50, 925, 6), // "itemAt"
QT_MOC_LITERAL(51, 932, 11), // "QQuickItem*"
QT_MOC_LITERAL(52, 944, 23), // "positionViewAtBeginning"
QT_MOC_LITERAL(53, 968, 17), // "positionViewAtEnd"
QT_MOC_LITERAL(54, 986, 11), // "forceLayout"
QT_MOC_LITERAL(55, 998, 5), // "model"
QT_MOC_LITERAL(56, 1004, 8), // "delegate"
QT_MOC_LITERAL(57, 1013, 14), // "QQmlComponent*"
QT_MOC_LITERAL(58, 1028, 5), // "count"
QT_MOC_LITERAL(59, 1034, 12), // "currentIndex"
QT_MOC_LITERAL(60, 1047, 11), // "currentItem"
QT_MOC_LITERAL(61, 1059, 18), // "keyNavigationWraps"
QT_MOC_LITERAL(62, 1078, 11), // "cacheBuffer"
QT_MOC_LITERAL(63, 1090, 22), // "displayMarginBeginning"
QT_MOC_LITERAL(64, 1113, 16), // "displayMarginEnd"
QT_MOC_LITERAL(65, 1130, 15), // "layoutDirection"
QT_MOC_LITERAL(66, 1146, 19), // "Qt::LayoutDirection"
QT_MOC_LITERAL(67, 1166, 24), // "effectiveLayoutDirection"
QT_MOC_LITERAL(68, 1191, 23), // "verticalLayoutDirection"
QT_MOC_LITERAL(69, 1215, 23), // "VerticalLayoutDirection"
QT_MOC_LITERAL(70, 1239, 6), // "header"
QT_MOC_LITERAL(71, 1246, 10), // "headerItem"
QT_MOC_LITERAL(72, 1257, 6), // "footer"
QT_MOC_LITERAL(73, 1264, 10), // "footerItem"
QT_MOC_LITERAL(74, 1275, 8), // "populate"
QT_MOC_LITERAL(75, 1284, 17), // "QQuickTransition*"
QT_MOC_LITERAL(76, 1302, 3), // "add"
QT_MOC_LITERAL(77, 1306, 12), // "addDisplaced"
QT_MOC_LITERAL(78, 1319, 4), // "move"
QT_MOC_LITERAL(79, 1324, 13), // "moveDisplaced"
QT_MOC_LITERAL(80, 1338, 6), // "remove"
QT_MOC_LITERAL(81, 1345, 15), // "removeDisplaced"
QT_MOC_LITERAL(82, 1361, 9), // "displaced"
QT_MOC_LITERAL(83, 1371, 9), // "highlight"
QT_MOC_LITERAL(84, 1381, 13), // "highlightItem"
QT_MOC_LITERAL(85, 1395, 27), // "highlightFollowsCurrentItem"
QT_MOC_LITERAL(86, 1423, 18), // "highlightRangeMode"
QT_MOC_LITERAL(87, 1442, 18), // "HighlightRangeMode"
QT_MOC_LITERAL(88, 1461, 23), // "preferredHighlightBegin"
QT_MOC_LITERAL(89, 1485, 21), // "preferredHighlightEnd"
QT_MOC_LITERAL(90, 1507, 21), // "highlightMoveDuration"
QT_MOC_LITERAL(91, 1529, 15), // "LayoutDirection"
QT_MOC_LITERAL(92, 1545, 11), // "LeftToRight"
QT_MOC_LITERAL(93, 1557, 11), // "RightToLeft"
QT_MOC_LITERAL(94, 1569, 19), // "VerticalTopToBottom"
QT_MOC_LITERAL(95, 1589, 19), // "VerticalBottomToTop"
QT_MOC_LITERAL(96, 1609, 11), // "TopToBottom"
QT_MOC_LITERAL(97, 1621, 11), // "BottomToTop"
QT_MOC_LITERAL(98, 1633, 16), // "NoHighlightRange"
QT_MOC_LITERAL(99, 1650, 10), // "ApplyRange"
QT_MOC_LITERAL(100, 1661, 20), // "StrictlyEnforceRange"
QT_MOC_LITERAL(101, 1682, 12), // "PositionMode"
QT_MOC_LITERAL(102, 1695, 9), // "Beginning"
QT_MOC_LITERAL(103, 1705, 6), // "Center"
QT_MOC_LITERAL(104, 1712, 3), // "End"
QT_MOC_LITERAL(105, 1716, 7), // "Visible"
QT_MOC_LITERAL(106, 1724, 7), // "Contain"
QT_MOC_LITERAL(107, 1732, 12) // "SnapPosition"

    },
    "QQuickItemView\0modelChanged\0\0"
    "delegateChanged\0countChanged\0"
    "currentIndexChanged\0currentItemChanged\0"
    "keyNavigationWrapsChanged\0cacheBufferChanged\0"
    "displayMarginBeginningChanged\0"
    "displayMarginEndChanged\0layoutDirectionChanged\0"
    "effectiveLayoutDirectionChanged\0"
    "verticalLayoutDirectionChanged\0"
    "headerChanged\0footerChanged\0"
    "headerItemChanged\0footerItemChanged\0"
    "populateTransitionChanged\0"
    "addTransitionChanged\0addDisplacedTransitionChanged\0"
    "moveTransitionChanged\0"
    "moveDisplacedTransitionChanged\0"
    "removeTransitionChanged\0"
    "removeDisplacedTransitionChanged\0"
    "displacedTransitionChanged\0highlightChanged\0"
    "highlightItemChanged\0"
    "highlightFollowsCurrentItemChanged\0"
    "highlightRangeModeChanged\0"
    "preferredHighlightBeginChanged\0"
    "preferredHighlightEndChanged\0"
    "highlightMoveDurationChanged\0"
    "destroyRemoved\0createdItem\0index\0item\0"
    "initItem\0modelUpdated\0QQmlChangeSet\0"
    "changeSet\0reset\0destroyingItem\0"
    "animStopped\0trackedPositionChanged\0"
    "positionViewAtIndex\0mode\0indexAt\0x\0y\0"
    "itemAt\0QQuickItem*\0positionViewAtBeginning\0"
    "positionViewAtEnd\0forceLayout\0model\0"
    "delegate\0QQmlComponent*\0count\0"
    "currentIndex\0currentItem\0keyNavigationWraps\0"
    "cacheBuffer\0displayMarginBeginning\0"
    "displayMarginEnd\0layoutDirection\0"
    "Qt::LayoutDirection\0effectiveLayoutDirection\0"
    "verticalLayoutDirection\0VerticalLayoutDirection\0"
    "header\0headerItem\0footer\0footerItem\0"
    "populate\0QQuickTransition*\0add\0"
    "addDisplaced\0move\0moveDisplaced\0remove\0"
    "removeDisplaced\0displaced\0highlight\0"
    "highlightItem\0highlightFollowsCurrentItem\0"
    "highlightRangeMode\0HighlightRangeMode\0"
    "preferredHighlightBegin\0preferredHighlightEnd\0"
    "highlightMoveDuration\0LayoutDirection\0"
    "LeftToRight\0RightToLeft\0VerticalTopToBottom\0"
    "VerticalBottomToTop\0TopToBottom\0"
    "BottomToTop\0NoHighlightRange\0ApplyRange\0"
    "StrictlyEnforceRange\0PositionMode\0"
    "Beginning\0Center\0End\0Visible\0Contain\0"
    "SnapPosition"
};
#undef QT_MOC_LITERAL

static const uint qt_meta_data_QQuickItemView[] = {

 // content:
       7,       // revision
       0,       // classname
       0,    0, // classinfo
      44,   14, // methods
      31,  348, // properties
       4,  503, // enums/sets
       0,    0, // constructors
       0,       // flags
      31,       // signalCount

 // signals: name, argc, parameters, tag, flags
       1,    0,  278,    2, 0x06 /* Public */,
       3,    0,  279,    2, 0x06 /* Public */,
       4,    0,  280,    2, 0x06 /* Public */,
       5,    0,  281,    2, 0x06 /* Public */,
       6,    0,  282,    2, 0x06 /* Public */,
       7,    0,  283,    2, 0x06 /* Public */,
       8,    0,  284,    2, 0x06 /* Public */,
       9,    0,  285,    2, 0x06 /* Public */,
      10,    0,  286,    2, 0x06 /* Public */,
      11,    0,  287,    2, 0x06 /* Public */,
      12,    0,  288,    2, 0x06 /* Public */,
      13,    0,  289,    2, 0x06 /* Public */,
      14,    0,  290,    2, 0x06 /* Public */,
      15,    0,  291,    2, 0x06 /* Public */,
      16,    0,  292,    2, 0x06 /* Public */,
      17,    0,  293,    2, 0x06 /* Public */,
      18,    0,  294,    2, 0x06 /* Public */,
      19,    0,  295,    2, 0x06 /* Public */,
      20,    0,  296,    2, 0x06 /* Public */,
      21,    0,  297,    2, 0x06 /* Public */,
      22,    0,  298,    2, 0x06 /* Public */,
      23,    0,  299,    2, 0x06 /* Public */,
      24,    0,  300,    2, 0x06 /* Public */,
      25,    0,  301,    2, 0x06 /* Public */,
      26,    0,  302,    2, 0x06 /* Public */,
      27,    0,  303,    2, 0x06 /* Public */,
      28,    0,  304,    2, 0x06 /* Public */,
      29,    0,  305,    2, 0x06 /* Public */,
      30,    0,  306,    2, 0x06 /* Public */,
      31,    0,  307,    2, 0x06 /* Public */,
      32,    0,  308,    2, 0x06 /* Public */,

 // slots: name, argc, parameters, tag, flags
      33,    0,  309,    2, 0x09 /* Protected */,
      34,    2,  310,    2, 0x09 /* Protected */,
      37,    2,  315,    2, 0x09 /* Protected */,
      38,    2,  320,    2, 0x09 /* Protected */,
      42,    1,  325,    2, 0x09 /* Protected */,
      43,    0,  328,    2, 0x09 /* Protected */,
      44,    0,  329,    2, 0x09 /* Protected */,

 // methods: name, argc, parameters, tag, flags
      45,    2,  330,    2, 0x02 /* Public */,
      47,    2,  335,    2, 0x02 /* Public */,
      50,    2,  340,    2, 0x02 /* Public */,
      52,    0,  345,    2, 0x02 /* Public */,
      53,    0,  346,    2, 0x02 /* Public */,
      54,    0,  347,    2, 0x82 /* Public | MethodRevisioned */,

 // signals: revision
       0,
       0,
       0,
       0,
       0,
       0,
       0,
       0,
       0,
       0,
       0,
       0,
       0,
       0,
       0,
       0,
       0,
       0,
       0,
       0,
       0,
       0,
       0,
       0,
       0,
       0,
       0,
       0,
       0,
       0,
       0,

 // slots: revision
       0,
       0,
       0,
       0,
       0,
       0,
       0,

 // methods: revision
       0,
       0,
       0,
       0,
       0,
       1,

 // signals: parameters
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,

 // slots: parameters
    QMetaType::Void,
    QMetaType::Void, QMetaType::Int, QMetaType::QObjectStar,   35,   36,
    QMetaType::Void, QMetaType::Int, QMetaType::QObjectStar,   35,   36,
    QMetaType::Void, 0x80000000 | 39, QMetaType::Bool,   40,   41,
    QMetaType::Void, QMetaType::QObjectStar,   36,
    QMetaType::Void,
    QMetaType::Void,

 // methods: parameters
    QMetaType::Void, QMetaType::Int, QMetaType::Int,   35,   46,
    QMetaType::Int, QMetaType::QReal, QMetaType::QReal,   48,   49,
    0x80000000 | 51, QMetaType::QReal, QMetaType::QReal,   48,   49,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,

 // properties: name, type, flags
      55, QMetaType::QVariant, 0x00495103,
      56, 0x80000000 | 57, 0x0049510b,
      58, QMetaType::Int, 0x00495001,
      59, QMetaType::Int, 0x00495103,
      60, 0x80000000 | 51, 0x00495009,
      61, QMetaType::Bool, 0x00495003,
      62, QMetaType::Int, 0x00495103,
      63, QMetaType::Int, 0x00c95103,
      64, QMetaType::Int, 0x00c95103,
      65, 0x80000000 | 66, 0x0049510b,
      67, 0x80000000 | 66, 0x00495009,
      68, 0x80000000 | 69, 0x0049510b,
      70, 0x80000000 | 57, 0x0049510b,
      71, 0x80000000 | 51, 0x00495009,
      72, 0x80000000 | 57, 0x0049510b,
      73, 0x80000000 | 51, 0x00495009,
      74, 0x80000000 | 75, 0x0049500b,
      76, 0x80000000 | 75, 0x0049500b,
      77, 0x80000000 | 75, 0x0049500b,
      78, 0x80000000 | 75, 0x0049500b,
      79, 0x80000000 | 75, 0x0049500b,
      80, 0x80000000 | 75, 0x0049500b,
      81, 0x80000000 | 75, 0x0049500b,
      82, 0x80000000 | 75, 0x0049500b,
      83, 0x80000000 | 57, 0x0049510b,
      84, 0x80000000 | 51, 0x00495009,
      85, QMetaType::Bool, 0x00495103,
      86, 0x80000000 | 87, 0x0049510b,
      88, QMetaType::QReal, 0x00495107,
      89, QMetaType::QReal, 0x00495107,
      90, QMetaType::Int, 0x00495103,

 // properties: notify_signal_id
       0,
       1,
       2,
       3,
       4,
       5,
       6,
       7,
       8,
       9,
      10,
      11,
      12,
      14,
      13,
      15,
      16,
      17,
      18,
      19,
      20,
      21,
      22,
      23,
      24,
      25,
      26,
      27,
      28,
      29,
      30,

 // properties: revision
       0,
       0,
       0,
       0,
       0,
       0,
       0,
       2,
       2,
       0,
       0,
       0,
       0,
       0,
       0,
       0,
       0,
       0,
       0,
       0,
       0,
       0,
       0,
       0,
       0,
       0,
       0,
       0,
       0,
       0,
       0,

 // enums: name, flags, count, data
      91, 0x0,    4,  519,
      69, 0x0,    2,  527,
      87, 0x0,    3,  531,
     101, 0x0,    6,  537,

 // enum data: key, value
      92, uint(QQuickItemView::LeftToRight),
      93, uint(QQuickItemView::RightToLeft),
      94, uint(QQuickItemView::VerticalTopToBottom),
      95, uint(QQuickItemView::VerticalBottomToTop),
      96, uint(QQuickItemView::TopToBottom),
      97, uint(QQuickItemView::BottomToTop),
      98, uint(QQuickItemView::NoHighlightRange),
      99, uint(QQuickItemView::ApplyRange),
     100, uint(QQuickItemView::StrictlyEnforceRange),
     102, uint(QQuickItemView::Beginning),
     103, uint(QQuickItemView::Center),
     104, uint(QQuickItemView::End),
     105, uint(QQuickItemView::Visible),
     106, uint(QQuickItemView::Contain),
     107, uint(QQuickItemView::SnapPosition),

       0        // eod
};

void QQuickItemView::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        QQuickItemView *_t = static_cast<QQuickItemView *>(_o);
        Q_UNUSED(_t)
        switch (_id) {
        case 0: _t->modelChanged(); break;
        case 1: _t->delegateChanged(); break;
        case 2: _t->countChanged(); break;
        case 3: _t->currentIndexChanged(); break;
        case 4: _t->currentItemChanged(); break;
        case 5: _t->keyNavigationWrapsChanged(); break;
        case 6: _t->cacheBufferChanged(); break;
        case 7: _t->displayMarginBeginningChanged(); break;
        case 8: _t->displayMarginEndChanged(); break;
        case 9: _t->layoutDirectionChanged(); break;
        case 10: _t->effectiveLayoutDirectionChanged(); break;
        case 11: _t->verticalLayoutDirectionChanged(); break;
        case 12: _t->headerChanged(); break;
        case 13: _t->footerChanged(); break;
        case 14: _t->headerItemChanged(); break;
        case 15: _t->footerItemChanged(); break;
        case 16: _t->populateTransitionChanged(); break;
        case 17: _t->addTransitionChanged(); break;
        case 18: _t->addDisplacedTransitionChanged(); break;
        case 19: _t->moveTransitionChanged(); break;
        case 20: _t->moveDisplacedTransitionChanged(); break;
        case 21: _t->removeTransitionChanged(); break;
        case 22: _t->removeDisplacedTransitionChanged(); break;
        case 23: _t->displacedTransitionChanged(); break;
        case 24: _t->highlightChanged(); break;
        case 25: _t->highlightItemChanged(); break;
        case 26: _t->highlightFollowsCurrentItemChanged(); break;
        case 27: _t->highlightRangeModeChanged(); break;
        case 28: _t->preferredHighlightBeginChanged(); break;
        case 29: _t->preferredHighlightEndChanged(); break;
        case 30: _t->highlightMoveDurationChanged(); break;
        case 31: _t->destroyRemoved(); break;
        case 32: _t->createdItem((*reinterpret_cast< int(*)>(_a[1])),(*reinterpret_cast< QObject*(*)>(_a[2]))); break;
        case 33: _t->initItem((*reinterpret_cast< int(*)>(_a[1])),(*reinterpret_cast< QObject*(*)>(_a[2]))); break;
        case 34: _t->modelUpdated((*reinterpret_cast< const QQmlChangeSet(*)>(_a[1])),(*reinterpret_cast< bool(*)>(_a[2]))); break;
        case 35: _t->destroyingItem((*reinterpret_cast< QObject*(*)>(_a[1]))); break;
        case 36: _t->animStopped(); break;
        case 37: _t->trackedPositionChanged(); break;
        case 38: _t->positionViewAtIndex((*reinterpret_cast< int(*)>(_a[1])),(*reinterpret_cast< int(*)>(_a[2]))); break;
        case 39: { int _r = _t->indexAt((*reinterpret_cast< qreal(*)>(_a[1])),(*reinterpret_cast< qreal(*)>(_a[2])));
            if (_a[0]) *reinterpret_cast< int*>(_a[0]) = _r; }  break;
        case 40: { QQuickItem* _r = _t->itemAt((*reinterpret_cast< qreal(*)>(_a[1])),(*reinterpret_cast< qreal(*)>(_a[2])));
            if (_a[0]) *reinterpret_cast< QQuickItem**>(_a[0]) = _r; }  break;
        case 41: _t->positionViewAtBeginning(); break;
        case 42: _t->positionViewAtEnd(); break;
        case 43: _t->forceLayout(); break;
        default: ;
        }
    } else if (_c == QMetaObject::IndexOfMethod) {
        int *result = reinterpret_cast<int *>(_a[0]);
        void **func = reinterpret_cast<void **>(_a[1]);
        {
            typedef void (QQuickItemView::*_t)();
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&QQuickItemView::modelChanged)) {
                *result = 0;
                return;
            }
        }
        {
            typedef void (QQuickItemView::*_t)();
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&QQuickItemView::delegateChanged)) {
                *result = 1;
                return;
            }
        }
        {
            typedef void (QQuickItemView::*_t)();
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&QQuickItemView::countChanged)) {
                *result = 2;
                return;
            }
        }
        {
            typedef void (QQuickItemView::*_t)();
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&QQuickItemView::currentIndexChanged)) {
                *result = 3;
                return;
            }
        }
        {
            typedef void (QQuickItemView::*_t)();
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&QQuickItemView::currentItemChanged)) {
                *result = 4;
                return;
            }
        }
        {
            typedef void (QQuickItemView::*_t)();
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&QQuickItemView::keyNavigationWrapsChanged)) {
                *result = 5;
                return;
            }
        }
        {
            typedef void (QQuickItemView::*_t)();
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&QQuickItemView::cacheBufferChanged)) {
                *result = 6;
                return;
            }
        }
        {
            typedef void (QQuickItemView::*_t)();
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&QQuickItemView::displayMarginBeginningChanged)) {
                *result = 7;
                return;
            }
        }
        {
            typedef void (QQuickItemView::*_t)();
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&QQuickItemView::displayMarginEndChanged)) {
                *result = 8;
                return;
            }
        }
        {
            typedef void (QQuickItemView::*_t)();
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&QQuickItemView::layoutDirectionChanged)) {
                *result = 9;
                return;
            }
        }
        {
            typedef void (QQuickItemView::*_t)();
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&QQuickItemView::effectiveLayoutDirectionChanged)) {
                *result = 10;
                return;
            }
        }
        {
            typedef void (QQuickItemView::*_t)();
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&QQuickItemView::verticalLayoutDirectionChanged)) {
                *result = 11;
                return;
            }
        }
        {
            typedef void (QQuickItemView::*_t)();
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&QQuickItemView::headerChanged)) {
                *result = 12;
                return;
            }
        }
        {
            typedef void (QQuickItemView::*_t)();
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&QQuickItemView::footerChanged)) {
                *result = 13;
                return;
            }
        }
        {
            typedef void (QQuickItemView::*_t)();
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&QQuickItemView::headerItemChanged)) {
                *result = 14;
                return;
            }
        }
        {
            typedef void (QQuickItemView::*_t)();
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&QQuickItemView::footerItemChanged)) {
                *result = 15;
                return;
            }
        }
        {
            typedef void (QQuickItemView::*_t)();
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&QQuickItemView::populateTransitionChanged)) {
                *result = 16;
                return;
            }
        }
        {
            typedef void (QQuickItemView::*_t)();
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&QQuickItemView::addTransitionChanged)) {
                *result = 17;
                return;
            }
        }
        {
            typedef void (QQuickItemView::*_t)();
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&QQuickItemView::addDisplacedTransitionChanged)) {
                *result = 18;
                return;
            }
        }
        {
            typedef void (QQuickItemView::*_t)();
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&QQuickItemView::moveTransitionChanged)) {
                *result = 19;
                return;
            }
        }
        {
            typedef void (QQuickItemView::*_t)();
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&QQuickItemView::moveDisplacedTransitionChanged)) {
                *result = 20;
                return;
            }
        }
        {
            typedef void (QQuickItemView::*_t)();
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&QQuickItemView::removeTransitionChanged)) {
                *result = 21;
                return;
            }
        }
        {
            typedef void (QQuickItemView::*_t)();
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&QQuickItemView::removeDisplacedTransitionChanged)) {
                *result = 22;
                return;
            }
        }
        {
            typedef void (QQuickItemView::*_t)();
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&QQuickItemView::displacedTransitionChanged)) {
                *result = 23;
                return;
            }
        }
        {
            typedef void (QQuickItemView::*_t)();
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&QQuickItemView::highlightChanged)) {
                *result = 24;
                return;
            }
        }
        {
            typedef void (QQuickItemView::*_t)();
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&QQuickItemView::highlightItemChanged)) {
                *result = 25;
                return;
            }
        }
        {
            typedef void (QQuickItemView::*_t)();
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&QQuickItemView::highlightFollowsCurrentItemChanged)) {
                *result = 26;
                return;
            }
        }
        {
            typedef void (QQuickItemView::*_t)();
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&QQuickItemView::highlightRangeModeChanged)) {
                *result = 27;
                return;
            }
        }
        {
            typedef void (QQuickItemView::*_t)();
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&QQuickItemView::preferredHighlightBeginChanged)) {
                *result = 28;
                return;
            }
        }
        {
            typedef void (QQuickItemView::*_t)();
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&QQuickItemView::preferredHighlightEndChanged)) {
                *result = 29;
                return;
            }
        }
        {
            typedef void (QQuickItemView::*_t)();
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&QQuickItemView::highlightMoveDurationChanged)) {
                *result = 30;
                return;
            }
        }
    } else if (_c == QMetaObject::RegisterPropertyMetaType) {
        switch (_id) {
        default: *reinterpret_cast<int*>(_a[0]) = -1; break;
        case 24:
        case 14:
        case 12:
        case 1:
            *reinterpret_cast<int*>(_a[0]) = qRegisterMetaType< QQmlComponent* >(); break;
        case 25:
        case 15:
        case 13:
        case 4:
            *reinterpret_cast<int*>(_a[0]) = qRegisterMetaType< QQuickItem* >(); break;
        }
    }

#ifndef QT_NO_PROPERTIES
    else if (_c == QMetaObject::ReadProperty) {
        QQuickItemView *_t = static_cast<QQuickItemView *>(_o);
        Q_UNUSED(_t)
        void *_v = _a[0];
        switch (_id) {
        case 0: *reinterpret_cast< QVariant*>(_v) = _t->model(); break;
        case 1: *reinterpret_cast< QQmlComponent**>(_v) = _t->delegate(); break;
        case 2: *reinterpret_cast< int*>(_v) = _t->count(); break;
        case 3: *reinterpret_cast< int*>(_v) = _t->currentIndex(); break;
        case 4: *reinterpret_cast< QQuickItem**>(_v) = _t->currentItem(); break;
        case 5: *reinterpret_cast< bool*>(_v) = _t->isWrapEnabled(); break;
        case 6: *reinterpret_cast< int*>(_v) = _t->cacheBuffer(); break;
        case 7: *reinterpret_cast< int*>(_v) = _t->displayMarginBeginning(); break;
        case 8: *reinterpret_cast< int*>(_v) = _t->displayMarginEnd(); break;
        case 9: *reinterpret_cast< Qt::LayoutDirection*>(_v) = _t->layoutDirection(); break;
        case 10: *reinterpret_cast< Qt::LayoutDirection*>(_v) = _t->effectiveLayoutDirection(); break;
        case 11: *reinterpret_cast< VerticalLayoutDirection*>(_v) = _t->verticalLayoutDirection(); break;
        case 12: *reinterpret_cast< QQmlComponent**>(_v) = _t->header(); break;
        case 13: *reinterpret_cast< QQuickItem**>(_v) = _t->headerItem(); break;
        case 14: *reinterpret_cast< QQmlComponent**>(_v) = _t->footer(); break;
        case 15: *reinterpret_cast< QQuickItem**>(_v) = _t->footerItem(); break;
        case 16: *reinterpret_cast< QQuickTransition**>(_v) = _t->populateTransition(); break;
        case 17: *reinterpret_cast< QQuickTransition**>(_v) = _t->addTransition(); break;
        case 18: *reinterpret_cast< QQuickTransition**>(_v) = _t->addDisplacedTransition(); break;
        case 19: *reinterpret_cast< QQuickTransition**>(_v) = _t->moveTransition(); break;
        case 20: *reinterpret_cast< QQuickTransition**>(_v) = _t->moveDisplacedTransition(); break;
        case 21: *reinterpret_cast< QQuickTransition**>(_v) = _t->removeTransition(); break;
        case 22: *reinterpret_cast< QQuickTransition**>(_v) = _t->removeDisplacedTransition(); break;
        case 23: *reinterpret_cast< QQuickTransition**>(_v) = _t->displacedTransition(); break;
        case 24: *reinterpret_cast< QQmlComponent**>(_v) = _t->highlight(); break;
        case 25: *reinterpret_cast< QQuickItem**>(_v) = _t->highlightItem(); break;
        case 26: *reinterpret_cast< bool*>(_v) = _t->highlightFollowsCurrentItem(); break;
        case 27: *reinterpret_cast< HighlightRangeMode*>(_v) = _t->highlightRangeMode(); break;
        case 28: *reinterpret_cast< qreal*>(_v) = _t->preferredHighlightBegin(); break;
        case 29: *reinterpret_cast< qreal*>(_v) = _t->preferredHighlightEnd(); break;
        case 30: *reinterpret_cast< int*>(_v) = _t->highlightMoveDuration(); break;
        default: break;
        }
    } else if (_c == QMetaObject::WriteProperty) {
        QQuickItemView *_t = static_cast<QQuickItemView *>(_o);
        Q_UNUSED(_t)
        void *_v = _a[0];
        switch (_id) {
        case 0: _t->setModel(*reinterpret_cast< QVariant*>(_v)); break;
        case 1: _t->setDelegate(*reinterpret_cast< QQmlComponent**>(_v)); break;
        case 3: _t->setCurrentIndex(*reinterpret_cast< int*>(_v)); break;
        case 5: _t->setWrapEnabled(*reinterpret_cast< bool*>(_v)); break;
        case 6: _t->setCacheBuffer(*reinterpret_cast< int*>(_v)); break;
        case 7: _t->setDisplayMarginBeginning(*reinterpret_cast< int*>(_v)); break;
        case 8: _t->setDisplayMarginEnd(*reinterpret_cast< int*>(_v)); break;
        case 9: _t->setLayoutDirection(*reinterpret_cast< Qt::LayoutDirection*>(_v)); break;
        case 11: _t->setVerticalLayoutDirection(*reinterpret_cast< VerticalLayoutDirection*>(_v)); break;
        case 12: _t->setHeader(*reinterpret_cast< QQmlComponent**>(_v)); break;
        case 14: _t->setFooter(*reinterpret_cast< QQmlComponent**>(_v)); break;
        case 16: _t->setPopulateTransition(*reinterpret_cast< QQuickTransition**>(_v)); break;
        case 17: _t->setAddTransition(*reinterpret_cast< QQuickTransition**>(_v)); break;
        case 18: _t->setAddDisplacedTransition(*reinterpret_cast< QQuickTransition**>(_v)); break;
        case 19: _t->setMoveTransition(*reinterpret_cast< QQuickTransition**>(_v)); break;
        case 20: _t->setMoveDisplacedTransition(*reinterpret_cast< QQuickTransition**>(_v)); break;
        case 21: _t->setRemoveTransition(*reinterpret_cast< QQuickTransition**>(_v)); break;
        case 22: _t->setRemoveDisplacedTransition(*reinterpret_cast< QQuickTransition**>(_v)); break;
        case 23: _t->setDisplacedTransition(*reinterpret_cast< QQuickTransition**>(_v)); break;
        case 24: _t->setHighlight(*reinterpret_cast< QQmlComponent**>(_v)); break;
        case 26: _t->setHighlightFollowsCurrentItem(*reinterpret_cast< bool*>(_v)); break;
        case 27: _t->setHighlightRangeMode(*reinterpret_cast< HighlightRangeMode*>(_v)); break;
        case 28: _t->setPreferredHighlightBegin(*reinterpret_cast< qreal*>(_v)); break;
        case 29: _t->setPreferredHighlightEnd(*reinterpret_cast< qreal*>(_v)); break;
        case 30: _t->setHighlightMoveDuration(*reinterpret_cast< int*>(_v)); break;
        default: break;
        }
    } else if (_c == QMetaObject::ResetProperty) {
        QQuickItemView *_t = static_cast<QQuickItemView *>(_o);
        Q_UNUSED(_t)
        switch (_id) {
        case 28: _t->resetPreferredHighlightBegin(); break;
        case 29: _t->resetPreferredHighlightEnd(); break;
        default: break;
        }
    }
#endif // QT_NO_PROPERTIES
}

const QMetaObject QQuickItemView::staticMetaObject = {
    { &QQuickFlickable::staticMetaObject, qt_meta_stringdata_QQuickItemView.data,
      qt_meta_data_QQuickItemView,  qt_static_metacall, Q_NULLPTR, Q_NULLPTR}
};


const QMetaObject *QQuickItemView::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->dynamicMetaObject() : &staticMetaObject;
}

void *QQuickItemView::qt_metacast(const char *_clname)
{
    if (!_clname) return Q_NULLPTR;
    if (!strcmp(_clname, qt_meta_stringdata_QQuickItemView.stringdata0))
        return static_cast<void*>(const_cast< QQuickItemView*>(this));
    return QQuickFlickable::qt_metacast(_clname);
}

int QQuickItemView::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QQuickFlickable::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 44)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 44;
    } else if (_c == QMetaObject::RegisterMethodArgumentMetaType) {
        if (_id < 44)
            *reinterpret_cast<int*>(_a[0]) = -1;
        _id -= 44;
    }
#ifndef QT_NO_PROPERTIES
   else if (_c == QMetaObject::ReadProperty || _c == QMetaObject::WriteProperty
            || _c == QMetaObject::ResetProperty || _c == QMetaObject::RegisterPropertyMetaType) {
        qt_static_metacall(this, _c, _id, _a);
        _id -= 31;
    } else if (_c == QMetaObject::QueryPropertyDesignable) {
        _id -= 31;
    } else if (_c == QMetaObject::QueryPropertyScriptable) {
        _id -= 31;
    } else if (_c == QMetaObject::QueryPropertyStored) {
        _id -= 31;
    } else if (_c == QMetaObject::QueryPropertyEditable) {
        _id -= 31;
    } else if (_c == QMetaObject::QueryPropertyUser) {
        _id -= 31;
    }
#endif // QT_NO_PROPERTIES
    return _id;
}

// SIGNAL 0
void QQuickItemView::modelChanged()
{
    QMetaObject::activate(this, &staticMetaObject, 0, Q_NULLPTR);
}

// SIGNAL 1
void QQuickItemView::delegateChanged()
{
    QMetaObject::activate(this, &staticMetaObject, 1, Q_NULLPTR);
}

// SIGNAL 2
void QQuickItemView::countChanged()
{
    QMetaObject::activate(this, &staticMetaObject, 2, Q_NULLPTR);
}

// SIGNAL 3
void QQuickItemView::currentIndexChanged()
{
    QMetaObject::activate(this, &staticMetaObject, 3, Q_NULLPTR);
}

// SIGNAL 4
void QQuickItemView::currentItemChanged()
{
    QMetaObject::activate(this, &staticMetaObject, 4, Q_NULLPTR);
}

// SIGNAL 5
void QQuickItemView::keyNavigationWrapsChanged()
{
    QMetaObject::activate(this, &staticMetaObject, 5, Q_NULLPTR);
}

// SIGNAL 6
void QQuickItemView::cacheBufferChanged()
{
    QMetaObject::activate(this, &staticMetaObject, 6, Q_NULLPTR);
}

// SIGNAL 7
void QQuickItemView::displayMarginBeginningChanged()
{
    QMetaObject::activate(this, &staticMetaObject, 7, Q_NULLPTR);
}

// SIGNAL 8
void QQuickItemView::displayMarginEndChanged()
{
    QMetaObject::activate(this, &staticMetaObject, 8, Q_NULLPTR);
}

// SIGNAL 9
void QQuickItemView::layoutDirectionChanged()
{
    QMetaObject::activate(this, &staticMetaObject, 9, Q_NULLPTR);
}

// SIGNAL 10
void QQuickItemView::effectiveLayoutDirectionChanged()
{
    QMetaObject::activate(this, &staticMetaObject, 10, Q_NULLPTR);
}

// SIGNAL 11
void QQuickItemView::verticalLayoutDirectionChanged()
{
    QMetaObject::activate(this, &staticMetaObject, 11, Q_NULLPTR);
}

// SIGNAL 12
void QQuickItemView::headerChanged()
{
    QMetaObject::activate(this, &staticMetaObject, 12, Q_NULLPTR);
}

// SIGNAL 13
void QQuickItemView::footerChanged()
{
    QMetaObject::activate(this, &staticMetaObject, 13, Q_NULLPTR);
}

// SIGNAL 14
void QQuickItemView::headerItemChanged()
{
    QMetaObject::activate(this, &staticMetaObject, 14, Q_NULLPTR);
}

// SIGNAL 15
void QQuickItemView::footerItemChanged()
{
    QMetaObject::activate(this, &staticMetaObject, 15, Q_NULLPTR);
}

// SIGNAL 16
void QQuickItemView::populateTransitionChanged()
{
    QMetaObject::activate(this, &staticMetaObject, 16, Q_NULLPTR);
}

// SIGNAL 17
void QQuickItemView::addTransitionChanged()
{
    QMetaObject::activate(this, &staticMetaObject, 17, Q_NULLPTR);
}

// SIGNAL 18
void QQuickItemView::addDisplacedTransitionChanged()
{
    QMetaObject::activate(this, &staticMetaObject, 18, Q_NULLPTR);
}

// SIGNAL 19
void QQuickItemView::moveTransitionChanged()
{
    QMetaObject::activate(this, &staticMetaObject, 19, Q_NULLPTR);
}

// SIGNAL 20
void QQuickItemView::moveDisplacedTransitionChanged()
{
    QMetaObject::activate(this, &staticMetaObject, 20, Q_NULLPTR);
}

// SIGNAL 21
void QQuickItemView::removeTransitionChanged()
{
    QMetaObject::activate(this, &staticMetaObject, 21, Q_NULLPTR);
}

// SIGNAL 22
void QQuickItemView::removeDisplacedTransitionChanged()
{
    QMetaObject::activate(this, &staticMetaObject, 22, Q_NULLPTR);
}

// SIGNAL 23
void QQuickItemView::displacedTransitionChanged()
{
    QMetaObject::activate(this, &staticMetaObject, 23, Q_NULLPTR);
}

// SIGNAL 24
void QQuickItemView::highlightChanged()
{
    QMetaObject::activate(this, &staticMetaObject, 24, Q_NULLPTR);
}

// SIGNAL 25
void QQuickItemView::highlightItemChanged()
{
    QMetaObject::activate(this, &staticMetaObject, 25, Q_NULLPTR);
}

// SIGNAL 26
void QQuickItemView::highlightFollowsCurrentItemChanged()
{
    QMetaObject::activate(this, &staticMetaObject, 26, Q_NULLPTR);
}

// SIGNAL 27
void QQuickItemView::highlightRangeModeChanged()
{
    QMetaObject::activate(this, &staticMetaObject, 27, Q_NULLPTR);
}

// SIGNAL 28
void QQuickItemView::preferredHighlightBeginChanged()
{
    QMetaObject::activate(this, &staticMetaObject, 28, Q_NULLPTR);
}

// SIGNAL 29
void QQuickItemView::preferredHighlightEndChanged()
{
    QMetaObject::activate(this, &staticMetaObject, 29, Q_NULLPTR);
}

// SIGNAL 30
void QQuickItemView::highlightMoveDurationChanged()
{
    QMetaObject::activate(this, &staticMetaObject, 30, Q_NULLPTR);
}
struct qt_meta_stringdata_QQuickItemViewAttached_t {
    QByteArrayData data[17];
    char stringdata0[221];
};
#define QT_MOC_LITERAL(idx, ofs, len) \
    Q_STATIC_BYTE_ARRAY_DATA_HEADER_INITIALIZER_WITH_OFFSET(len, \
    qptrdiff(offsetof(qt_meta_stringdata_QQuickItemViewAttached_t, stringdata0) + ofs \
        - idx * sizeof(QByteArrayData)) \
    )
static const qt_meta_stringdata_QQuickItemViewAttached_t qt_meta_stringdata_QQuickItemViewAttached = {
    {
QT_MOC_LITERAL(0, 0, 22), // "QQuickItemViewAttached"
QT_MOC_LITERAL(1, 23, 11), // "viewChanged"
QT_MOC_LITERAL(2, 35, 0), // ""
QT_MOC_LITERAL(3, 36, 18), // "currentItemChanged"
QT_MOC_LITERAL(4, 55, 18), // "delayRemoveChanged"
QT_MOC_LITERAL(5, 74, 3), // "add"
QT_MOC_LITERAL(6, 78, 6), // "remove"
QT_MOC_LITERAL(7, 85, 14), // "sectionChanged"
QT_MOC_LITERAL(8, 100, 18), // "prevSectionChanged"
QT_MOC_LITERAL(9, 119, 18), // "nextSectionChanged"
QT_MOC_LITERAL(10, 138, 4), // "view"
QT_MOC_LITERAL(11, 143, 15), // "QQuickItemView*"
QT_MOC_LITERAL(12, 159, 13), // "isCurrentItem"
QT_MOC_LITERAL(13, 173, 11), // "delayRemove"
QT_MOC_LITERAL(14, 185, 7), // "section"
QT_MOC_LITERAL(15, 193, 15), // "previousSection"
QT_MOC_LITERAL(16, 209, 11) // "nextSection"

    },
    "QQuickItemViewAttached\0viewChanged\0\0"
    "currentItemChanged\0delayRemoveChanged\0"
    "add\0remove\0sectionChanged\0prevSectionChanged\0"
    "nextSectionChanged\0view\0QQuickItemView*\0"
    "isCurrentItem\0delayRemove\0section\0"
    "previousSection\0nextSection"
};
#undef QT_MOC_LITERAL

static const uint qt_meta_data_QQuickItemViewAttached[] = {

 // content:
       7,       // revision
       0,       // classname
       0,    0, // classinfo
       8,   14, // methods
       6,   62, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       8,       // signalCount

 // signals: name, argc, parameters, tag, flags
       1,    0,   54,    2, 0x06 /* Public */,
       3,    0,   55,    2, 0x06 /* Public */,
       4,    0,   56,    2, 0x06 /* Public */,
       5,    0,   57,    2, 0x06 /* Public */,
       6,    0,   58,    2, 0x06 /* Public */,
       7,    0,   59,    2, 0x06 /* Public */,
       8,    0,   60,    2, 0x06 /* Public */,
       9,    0,   61,    2, 0x06 /* Public */,

 // signals: parameters
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,

 // properties: name, type, flags
      10, 0x80000000 | 11, 0x00495009,
      12, QMetaType::Bool, 0x00495001,
      13, QMetaType::Bool, 0x00495103,
      14, QMetaType::QString, 0x00495001,
      15, QMetaType::QString, 0x00495001,
      16, QMetaType::QString, 0x00495001,

 // properties: notify_signal_id
       0,
       1,
       2,
       5,
       6,
       7,

       0        // eod
};

void QQuickItemViewAttached::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        QQuickItemViewAttached *_t = static_cast<QQuickItemViewAttached *>(_o);
        Q_UNUSED(_t)
        switch (_id) {
        case 0: _t->viewChanged(); break;
        case 1: _t->currentItemChanged(); break;
        case 2: _t->delayRemoveChanged(); break;
        case 3: _t->add(); break;
        case 4: _t->remove(); break;
        case 5: _t->sectionChanged(); break;
        case 6: _t->prevSectionChanged(); break;
        case 7: _t->nextSectionChanged(); break;
        default: ;
        }
    } else if (_c == QMetaObject::IndexOfMethod) {
        int *result = reinterpret_cast<int *>(_a[0]);
        void **func = reinterpret_cast<void **>(_a[1]);
        {
            typedef void (QQuickItemViewAttached::*_t)();
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&QQuickItemViewAttached::viewChanged)) {
                *result = 0;
                return;
            }
        }
        {
            typedef void (QQuickItemViewAttached::*_t)();
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&QQuickItemViewAttached::currentItemChanged)) {
                *result = 1;
                return;
            }
        }
        {
            typedef void (QQuickItemViewAttached::*_t)();
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&QQuickItemViewAttached::delayRemoveChanged)) {
                *result = 2;
                return;
            }
        }
        {
            typedef void (QQuickItemViewAttached::*_t)();
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&QQuickItemViewAttached::add)) {
                *result = 3;
                return;
            }
        }
        {
            typedef void (QQuickItemViewAttached::*_t)();
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&QQuickItemViewAttached::remove)) {
                *result = 4;
                return;
            }
        }
        {
            typedef void (QQuickItemViewAttached::*_t)();
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&QQuickItemViewAttached::sectionChanged)) {
                *result = 5;
                return;
            }
        }
        {
            typedef void (QQuickItemViewAttached::*_t)();
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&QQuickItemViewAttached::prevSectionChanged)) {
                *result = 6;
                return;
            }
        }
        {
            typedef void (QQuickItemViewAttached::*_t)();
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&QQuickItemViewAttached::nextSectionChanged)) {
                *result = 7;
                return;
            }
        }
    } else if (_c == QMetaObject::RegisterPropertyMetaType) {
        switch (_id) {
        default: *reinterpret_cast<int*>(_a[0]) = -1; break;
        case 0:
            *reinterpret_cast<int*>(_a[0]) = qRegisterMetaType< QQuickItemView* >(); break;
        }
    }

#ifndef QT_NO_PROPERTIES
    else if (_c == QMetaObject::ReadProperty) {
        QQuickItemViewAttached *_t = static_cast<QQuickItemViewAttached *>(_o);
        Q_UNUSED(_t)
        void *_v = _a[0];
        switch (_id) {
        case 0: *reinterpret_cast< QQuickItemView**>(_v) = _t->view(); break;
        case 1: *reinterpret_cast< bool*>(_v) = _t->isCurrentItem(); break;
        case 2: *reinterpret_cast< bool*>(_v) = _t->delayRemove(); break;
        case 3: *reinterpret_cast< QString*>(_v) = _t->section(); break;
        case 4: *reinterpret_cast< QString*>(_v) = _t->prevSection(); break;
        case 5: *reinterpret_cast< QString*>(_v) = _t->nextSection(); break;
        default: break;
        }
    } else if (_c == QMetaObject::WriteProperty) {
        QQuickItemViewAttached *_t = static_cast<QQuickItemViewAttached *>(_o);
        Q_UNUSED(_t)
        void *_v = _a[0];
        switch (_id) {
        case 2: _t->setDelayRemove(*reinterpret_cast< bool*>(_v)); break;
        default: break;
        }
    } else if (_c == QMetaObject::ResetProperty) {
    }
#endif // QT_NO_PROPERTIES
}

const QMetaObject QQuickItemViewAttached::staticMetaObject = {
    { &QObject::staticMetaObject, qt_meta_stringdata_QQuickItemViewAttached.data,
      qt_meta_data_QQuickItemViewAttached,  qt_static_metacall, Q_NULLPTR, Q_NULLPTR}
};


const QMetaObject *QQuickItemViewAttached::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->dynamicMetaObject() : &staticMetaObject;
}

void *QQuickItemViewAttached::qt_metacast(const char *_clname)
{
    if (!_clname) return Q_NULLPTR;
    if (!strcmp(_clname, qt_meta_stringdata_QQuickItemViewAttached.stringdata0))
        return static_cast<void*>(const_cast< QQuickItemViewAttached*>(this));
    return QObject::qt_metacast(_clname);
}

int QQuickItemViewAttached::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QObject::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 8)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 8;
    } else if (_c == QMetaObject::RegisterMethodArgumentMetaType) {
        if (_id < 8)
            *reinterpret_cast<int*>(_a[0]) = -1;
        _id -= 8;
    }
#ifndef QT_NO_PROPERTIES
   else if (_c == QMetaObject::ReadProperty || _c == QMetaObject::WriteProperty
            || _c == QMetaObject::ResetProperty || _c == QMetaObject::RegisterPropertyMetaType) {
        qt_static_metacall(this, _c, _id, _a);
        _id -= 6;
    } else if (_c == QMetaObject::QueryPropertyDesignable) {
        _id -= 6;
    } else if (_c == QMetaObject::QueryPropertyScriptable) {
        _id -= 6;
    } else if (_c == QMetaObject::QueryPropertyStored) {
        _id -= 6;
    } else if (_c == QMetaObject::QueryPropertyEditable) {
        _id -= 6;
    } else if (_c == QMetaObject::QueryPropertyUser) {
        _id -= 6;
    }
#endif // QT_NO_PROPERTIES
    return _id;
}

// SIGNAL 0
void QQuickItemViewAttached::viewChanged()
{
    QMetaObject::activate(this, &staticMetaObject, 0, Q_NULLPTR);
}

// SIGNAL 1
void QQuickItemViewAttached::currentItemChanged()
{
    QMetaObject::activate(this, &staticMetaObject, 1, Q_NULLPTR);
}

// SIGNAL 2
void QQuickItemViewAttached::delayRemoveChanged()
{
    QMetaObject::activate(this, &staticMetaObject, 2, Q_NULLPTR);
}

// SIGNAL 3
void QQuickItemViewAttached::add()
{
    QMetaObject::activate(this, &staticMetaObject, 3, Q_NULLPTR);
}

// SIGNAL 4
void QQuickItemViewAttached::remove()
{
    QMetaObject::activate(this, &staticMetaObject, 4, Q_NULLPTR);
}

// SIGNAL 5
void QQuickItemViewAttached::sectionChanged()
{
    QMetaObject::activate(this, &staticMetaObject, 5, Q_NULLPTR);
}

// SIGNAL 6
void QQuickItemViewAttached::prevSectionChanged()
{
    QMetaObject::activate(this, &staticMetaObject, 6, Q_NULLPTR);
}

// SIGNAL 7
void QQuickItemViewAttached::nextSectionChanged()
{
    QMetaObject::activate(this, &staticMetaObject, 7, Q_NULLPTR);
}
QT_END_MOC_NAMESPACE
