/****************************************************************************
** Meta object code from reading C++ file 'qquickmultipointtoucharea_p.h'
**
** Created by: The Qt Meta Object Compiler version 67 (Qt 5.6.3)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "../../items/qquickmultipointtoucharea_p.h"
#include <QtCore/qbytearray.h>
#include <QtCore/qmetatype.h>
#include <QtCore/QList>
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'qquickmultipointtoucharea_p.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 67
#error "This file was generated using the moc from 5.6.3. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
struct qt_meta_stringdata_QQuickTouchPoint_t {
    QByteArrayData data[28];
    char stringdata0[291];
};
#define QT_MOC_LITERAL(idx, ofs, len) \
    Q_STATIC_BYTE_ARRAY_DATA_HEADER_INITIALIZER_WITH_OFFSET(len, \
    qptrdiff(offsetof(qt_meta_stringdata_QQuickTouchPoint_t, stringdata0) + ofs \
        - idx * sizeof(QByteArrayData)) \
    )
static const qt_meta_stringdata_QQuickTouchPoint_t qt_meta_stringdata_QQuickTouchPoint = {
    {
QT_MOC_LITERAL(0, 0, 16), // "QQuickTouchPoint"
QT_MOC_LITERAL(1, 17, 14), // "pressedChanged"
QT_MOC_LITERAL(2, 32, 0), // ""
QT_MOC_LITERAL(3, 33, 14), // "pointIdChanged"
QT_MOC_LITERAL(4, 48, 8), // "xChanged"
QT_MOC_LITERAL(5, 57, 8), // "yChanged"
QT_MOC_LITERAL(6, 66, 15), // "pressureChanged"
QT_MOC_LITERAL(7, 82, 15), // "velocityChanged"
QT_MOC_LITERAL(8, 98, 11), // "areaChanged"
QT_MOC_LITERAL(9, 110, 13), // "startXChanged"
QT_MOC_LITERAL(10, 124, 13), // "startYChanged"
QT_MOC_LITERAL(11, 138, 16), // "previousXChanged"
QT_MOC_LITERAL(12, 155, 16), // "previousYChanged"
QT_MOC_LITERAL(13, 172, 13), // "sceneXChanged"
QT_MOC_LITERAL(14, 186, 13), // "sceneYChanged"
QT_MOC_LITERAL(15, 200, 7), // "pointId"
QT_MOC_LITERAL(16, 208, 7), // "pressed"
QT_MOC_LITERAL(17, 216, 1), // "x"
QT_MOC_LITERAL(18, 218, 1), // "y"
QT_MOC_LITERAL(19, 220, 8), // "pressure"
QT_MOC_LITERAL(20, 229, 8), // "velocity"
QT_MOC_LITERAL(21, 238, 4), // "area"
QT_MOC_LITERAL(22, 243, 6), // "startX"
QT_MOC_LITERAL(23, 250, 6), // "startY"
QT_MOC_LITERAL(24, 257, 9), // "previousX"
QT_MOC_LITERAL(25, 267, 9), // "previousY"
QT_MOC_LITERAL(26, 277, 6), // "sceneX"
QT_MOC_LITERAL(27, 284, 6) // "sceneY"

    },
    "QQuickTouchPoint\0pressedChanged\0\0"
    "pointIdChanged\0xChanged\0yChanged\0"
    "pressureChanged\0velocityChanged\0"
    "areaChanged\0startXChanged\0startYChanged\0"
    "previousXChanged\0previousYChanged\0"
    "sceneXChanged\0sceneYChanged\0pointId\0"
    "pressed\0x\0y\0pressure\0velocity\0area\0"
    "startX\0startY\0previousX\0previousY\0"
    "sceneX\0sceneY"
};
#undef QT_MOC_LITERAL

static const uint qt_meta_data_QQuickTouchPoint[] = {

 // content:
       7,       // revision
       0,       // classname
       0,    0, // classinfo
      13,   14, // methods
      13,   92, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
      13,       // signalCount

 // signals: name, argc, parameters, tag, flags
       1,    0,   79,    2, 0x06 /* Public */,
       3,    0,   80,    2, 0x06 /* Public */,
       4,    0,   81,    2, 0x06 /* Public */,
       5,    0,   82,    2, 0x06 /* Public */,
       6,    0,   83,    2, 0x06 /* Public */,
       7,    0,   84,    2, 0x06 /* Public */,
       8,    0,   85,    2, 0x06 /* Public */,
       9,    0,   86,    2, 0x06 /* Public */,
      10,    0,   87,    2, 0x06 /* Public */,
      11,    0,   88,    2, 0x06 /* Public */,
      12,    0,   89,    2, 0x06 /* Public */,
      13,    0,   90,    2, 0x06 /* Public */,
      14,    0,   91,    2, 0x06 /* Public */,

 // signals: parameters
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,

 // properties: name, type, flags
      15, QMetaType::Int, 0x00495001,
      16, QMetaType::Bool, 0x00495001,
      17, QMetaType::QReal, 0x00495001,
      18, QMetaType::QReal, 0x00495001,
      19, QMetaType::QReal, 0x00495001,
      20, QMetaType::QVector2D, 0x00495001,
      21, QMetaType::QRectF, 0x00495001,
      22, QMetaType::QReal, 0x00495001,
      23, QMetaType::QReal, 0x00495001,
      24, QMetaType::QReal, 0x00495001,
      25, QMetaType::QReal, 0x00495001,
      26, QMetaType::QReal, 0x00495001,
      27, QMetaType::QReal, 0x00495001,

 // properties: notify_signal_id
       1,
       0,
       2,
       3,
       4,
       5,
       6,
       7,
       8,
       9,
      10,
      11,
      12,

       0        // eod
};

void QQuickTouchPoint::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        QQuickTouchPoint *_t = static_cast<QQuickTouchPoint *>(_o);
        Q_UNUSED(_t)
        switch (_id) {
        case 0: _t->pressedChanged(); break;
        case 1: _t->pointIdChanged(); break;
        case 2: _t->xChanged(); break;
        case 3: _t->yChanged(); break;
        case 4: _t->pressureChanged(); break;
        case 5: _t->velocityChanged(); break;
        case 6: _t->areaChanged(); break;
        case 7: _t->startXChanged(); break;
        case 8: _t->startYChanged(); break;
        case 9: _t->previousXChanged(); break;
        case 10: _t->previousYChanged(); break;
        case 11: _t->sceneXChanged(); break;
        case 12: _t->sceneYChanged(); break;
        default: ;
        }
    } else if (_c == QMetaObject::IndexOfMethod) {
        int *result = reinterpret_cast<int *>(_a[0]);
        void **func = reinterpret_cast<void **>(_a[1]);
        {
            typedef void (QQuickTouchPoint::*_t)();
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&QQuickTouchPoint::pressedChanged)) {
                *result = 0;
                return;
            }
        }
        {
            typedef void (QQuickTouchPoint::*_t)();
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&QQuickTouchPoint::pointIdChanged)) {
                *result = 1;
                return;
            }
        }
        {
            typedef void (QQuickTouchPoint::*_t)();
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&QQuickTouchPoint::xChanged)) {
                *result = 2;
                return;
            }
        }
        {
            typedef void (QQuickTouchPoint::*_t)();
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&QQuickTouchPoint::yChanged)) {
                *result = 3;
                return;
            }
        }
        {
            typedef void (QQuickTouchPoint::*_t)();
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&QQuickTouchPoint::pressureChanged)) {
                *result = 4;
                return;
            }
        }
        {
            typedef void (QQuickTouchPoint::*_t)();
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&QQuickTouchPoint::velocityChanged)) {
                *result = 5;
                return;
            }
        }
        {
            typedef void (QQuickTouchPoint::*_t)();
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&QQuickTouchPoint::areaChanged)) {
                *result = 6;
                return;
            }
        }
        {
            typedef void (QQuickTouchPoint::*_t)();
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&QQuickTouchPoint::startXChanged)) {
                *result = 7;
                return;
            }
        }
        {
            typedef void (QQuickTouchPoint::*_t)();
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&QQuickTouchPoint::startYChanged)) {
                *result = 8;
                return;
            }
        }
        {
            typedef void (QQuickTouchPoint::*_t)();
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&QQuickTouchPoint::previousXChanged)) {
                *result = 9;
                return;
            }
        }
        {
            typedef void (QQuickTouchPoint::*_t)();
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&QQuickTouchPoint::previousYChanged)) {
                *result = 10;
                return;
            }
        }
        {
            typedef void (QQuickTouchPoint::*_t)();
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&QQuickTouchPoint::sceneXChanged)) {
                *result = 11;
                return;
            }
        }
        {
            typedef void (QQuickTouchPoint::*_t)();
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&QQuickTouchPoint::sceneYChanged)) {
                *result = 12;
                return;
            }
        }
    }
#ifndef QT_NO_PROPERTIES
    else if (_c == QMetaObject::ReadProperty) {
        QQuickTouchPoint *_t = static_cast<QQuickTouchPoint *>(_o);
        Q_UNUSED(_t)
        void *_v = _a[0];
        switch (_id) {
        case 0: *reinterpret_cast< int*>(_v) = _t->pointId(); break;
        case 1: *reinterpret_cast< bool*>(_v) = _t->pressed(); break;
        case 2: *reinterpret_cast< qreal*>(_v) = _t->x(); break;
        case 3: *reinterpret_cast< qreal*>(_v) = _t->y(); break;
        case 4: *reinterpret_cast< qreal*>(_v) = _t->pressure(); break;
        case 5: *reinterpret_cast< QVector2D*>(_v) = _t->velocity(); break;
        case 6: *reinterpret_cast< QRectF*>(_v) = _t->area(); break;
        case 7: *reinterpret_cast< qreal*>(_v) = _t->startX(); break;
        case 8: *reinterpret_cast< qreal*>(_v) = _t->startY(); break;
        case 9: *reinterpret_cast< qreal*>(_v) = _t->previousX(); break;
        case 10: *reinterpret_cast< qreal*>(_v) = _t->previousY(); break;
        case 11: *reinterpret_cast< qreal*>(_v) = _t->sceneX(); break;
        case 12: *reinterpret_cast< qreal*>(_v) = _t->sceneY(); break;
        default: break;
        }
    } else if (_c == QMetaObject::WriteProperty) {
    } else if (_c == QMetaObject::ResetProperty) {
    }
#endif // QT_NO_PROPERTIES
    Q_UNUSED(_a);
}

const QMetaObject QQuickTouchPoint::staticMetaObject = {
    { &QObject::staticMetaObject, qt_meta_stringdata_QQuickTouchPoint.data,
      qt_meta_data_QQuickTouchPoint,  qt_static_metacall, Q_NULLPTR, Q_NULLPTR}
};


const QMetaObject *QQuickTouchPoint::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->dynamicMetaObject() : &staticMetaObject;
}

void *QQuickTouchPoint::qt_metacast(const char *_clname)
{
    if (!_clname) return Q_NULLPTR;
    if (!strcmp(_clname, qt_meta_stringdata_QQuickTouchPoint.stringdata0))
        return static_cast<void*>(const_cast< QQuickTouchPoint*>(this));
    return QObject::qt_metacast(_clname);
}

int QQuickTouchPoint::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QObject::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 13)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 13;
    } else if (_c == QMetaObject::RegisterMethodArgumentMetaType) {
        if (_id < 13)
            *reinterpret_cast<int*>(_a[0]) = -1;
        _id -= 13;
    }
#ifndef QT_NO_PROPERTIES
   else if (_c == QMetaObject::ReadProperty || _c == QMetaObject::WriteProperty
            || _c == QMetaObject::ResetProperty || _c == QMetaObject::RegisterPropertyMetaType) {
        qt_static_metacall(this, _c, _id, _a);
        _id -= 13;
    } else if (_c == QMetaObject::QueryPropertyDesignable) {
        _id -= 13;
    } else if (_c == QMetaObject::QueryPropertyScriptable) {
        _id -= 13;
    } else if (_c == QMetaObject::QueryPropertyStored) {
        _id -= 13;
    } else if (_c == QMetaObject::QueryPropertyEditable) {
        _id -= 13;
    } else if (_c == QMetaObject::QueryPropertyUser) {
        _id -= 13;
    }
#endif // QT_NO_PROPERTIES
    return _id;
}

// SIGNAL 0
void QQuickTouchPoint::pressedChanged()
{
    QMetaObject::activate(this, &staticMetaObject, 0, Q_NULLPTR);
}

// SIGNAL 1
void QQuickTouchPoint::pointIdChanged()
{
    QMetaObject::activate(this, &staticMetaObject, 1, Q_NULLPTR);
}

// SIGNAL 2
void QQuickTouchPoint::xChanged()
{
    QMetaObject::activate(this, &staticMetaObject, 2, Q_NULLPTR);
}

// SIGNAL 3
void QQuickTouchPoint::yChanged()
{
    QMetaObject::activate(this, &staticMetaObject, 3, Q_NULLPTR);
}

// SIGNAL 4
void QQuickTouchPoint::pressureChanged()
{
    QMetaObject::activate(this, &staticMetaObject, 4, Q_NULLPTR);
}

// SIGNAL 5
void QQuickTouchPoint::velocityChanged()
{
    QMetaObject::activate(this, &staticMetaObject, 5, Q_NULLPTR);
}

// SIGNAL 6
void QQuickTouchPoint::areaChanged()
{
    QMetaObject::activate(this, &staticMetaObject, 6, Q_NULLPTR);
}

// SIGNAL 7
void QQuickTouchPoint::startXChanged()
{
    QMetaObject::activate(this, &staticMetaObject, 7, Q_NULLPTR);
}

// SIGNAL 8
void QQuickTouchPoint::startYChanged()
{
    QMetaObject::activate(this, &staticMetaObject, 8, Q_NULLPTR);
}

// SIGNAL 9
void QQuickTouchPoint::previousXChanged()
{
    QMetaObject::activate(this, &staticMetaObject, 9, Q_NULLPTR);
}

// SIGNAL 10
void QQuickTouchPoint::previousYChanged()
{
    QMetaObject::activate(this, &staticMetaObject, 10, Q_NULLPTR);
}

// SIGNAL 11
void QQuickTouchPoint::sceneXChanged()
{
    QMetaObject::activate(this, &staticMetaObject, 11, Q_NULLPTR);
}

// SIGNAL 12
void QQuickTouchPoint::sceneYChanged()
{
    QMetaObject::activate(this, &staticMetaObject, 12, Q_NULLPTR);
}
struct qt_meta_stringdata_QQuickGrabGestureEvent_t {
    QByteArrayData data[6];
    char stringdata0[81];
};
#define QT_MOC_LITERAL(idx, ofs, len) \
    Q_STATIC_BYTE_ARRAY_DATA_HEADER_INITIALIZER_WITH_OFFSET(len, \
    qptrdiff(offsetof(qt_meta_stringdata_QQuickGrabGestureEvent_t, stringdata0) + ofs \
        - idx * sizeof(QByteArrayData)) \
    )
static const qt_meta_stringdata_QQuickGrabGestureEvent_t qt_meta_stringdata_QQuickGrabGestureEvent = {
    {
QT_MOC_LITERAL(0, 0, 22), // "QQuickGrabGestureEvent"
QT_MOC_LITERAL(1, 23, 4), // "grab"
QT_MOC_LITERAL(2, 28, 0), // ""
QT_MOC_LITERAL(3, 29, 11), // "touchPoints"
QT_MOC_LITERAL(4, 41, 25), // "QQmlListProperty<QObject>"
QT_MOC_LITERAL(5, 67, 13) // "dragThreshold"

    },
    "QQuickGrabGestureEvent\0grab\0\0touchPoints\0"
    "QQmlListProperty<QObject>\0dragThreshold"
};
#undef QT_MOC_LITERAL

static const uint qt_meta_data_QQuickGrabGestureEvent[] = {

 // content:
       7,       // revision
       0,       // classname
       0,    0, // classinfo
       1,   14, // methods
       2,   20, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       0,       // signalCount

 // methods: name, argc, parameters, tag, flags
       1,    0,   19,    2, 0x02 /* Public */,

 // methods: parameters
    QMetaType::Void,

 // properties: name, type, flags
       3, 0x80000000 | 4, 0x00095009,
       5, QMetaType::QReal, 0x00095001,

       0        // eod
};

void QQuickGrabGestureEvent::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        QQuickGrabGestureEvent *_t = static_cast<QQuickGrabGestureEvent *>(_o);
        Q_UNUSED(_t)
        switch (_id) {
        case 0: _t->grab(); break;
        default: ;
        }
    } else if (_c == QMetaObject::RegisterPropertyMetaType) {
        switch (_id) {
        default: *reinterpret_cast<int*>(_a[0]) = -1; break;
        case 0:
            *reinterpret_cast<int*>(_a[0]) = qRegisterMetaType< QQmlListProperty<QObject> >(); break;
        }
    }

#ifndef QT_NO_PROPERTIES
    else if (_c == QMetaObject::ReadProperty) {
        QQuickGrabGestureEvent *_t = static_cast<QQuickGrabGestureEvent *>(_o);
        Q_UNUSED(_t)
        void *_v = _a[0];
        switch (_id) {
        case 0: *reinterpret_cast< QQmlListProperty<QObject>*>(_v) = _t->touchPoints(); break;
        case 1: *reinterpret_cast< qreal*>(_v) = _t->dragThreshold(); break;
        default: break;
        }
    } else if (_c == QMetaObject::WriteProperty) {
    } else if (_c == QMetaObject::ResetProperty) {
    }
#endif // QT_NO_PROPERTIES
}

const QMetaObject QQuickGrabGestureEvent::staticMetaObject = {
    { &QObject::staticMetaObject, qt_meta_stringdata_QQuickGrabGestureEvent.data,
      qt_meta_data_QQuickGrabGestureEvent,  qt_static_metacall, Q_NULLPTR, Q_NULLPTR}
};


const QMetaObject *QQuickGrabGestureEvent::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->dynamicMetaObject() : &staticMetaObject;
}

void *QQuickGrabGestureEvent::qt_metacast(const char *_clname)
{
    if (!_clname) return Q_NULLPTR;
    if (!strcmp(_clname, qt_meta_stringdata_QQuickGrabGestureEvent.stringdata0))
        return static_cast<void*>(const_cast< QQuickGrabGestureEvent*>(this));
    return QObject::qt_metacast(_clname);
}

int QQuickGrabGestureEvent::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QObject::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 1)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 1;
    } else if (_c == QMetaObject::RegisterMethodArgumentMetaType) {
        if (_id < 1)
            *reinterpret_cast<int*>(_a[0]) = -1;
        _id -= 1;
    }
#ifndef QT_NO_PROPERTIES
   else if (_c == QMetaObject::ReadProperty || _c == QMetaObject::WriteProperty
            || _c == QMetaObject::ResetProperty || _c == QMetaObject::RegisterPropertyMetaType) {
        qt_static_metacall(this, _c, _id, _a);
        _id -= 2;
    } else if (_c == QMetaObject::QueryPropertyDesignable) {
        _id -= 2;
    } else if (_c == QMetaObject::QueryPropertyScriptable) {
        _id -= 2;
    } else if (_c == QMetaObject::QueryPropertyStored) {
        _id -= 2;
    } else if (_c == QMetaObject::QueryPropertyEditable) {
        _id -= 2;
    } else if (_c == QMetaObject::QueryPropertyUser) {
        _id -= 2;
    }
#endif // QT_NO_PROPERTIES
    return _id;
}
struct qt_meta_stringdata_QQuickMultiPointTouchArea_t {
    QByteArrayData data[19];
    char stringdata0[307];
};
#define QT_MOC_LITERAL(idx, ofs, len) \
    Q_STATIC_BYTE_ARRAY_DATA_HEADER_INITIALIZER_WITH_OFFSET(len, \
    qptrdiff(offsetof(qt_meta_stringdata_QQuickMultiPointTouchArea_t, stringdata0) + ofs \
        - idx * sizeof(QByteArrayData)) \
    )
static const qt_meta_stringdata_QQuickMultiPointTouchArea_t qt_meta_stringdata_QQuickMultiPointTouchArea = {
    {
QT_MOC_LITERAL(0, 0, 25), // "QQuickMultiPointTouchArea"
QT_MOC_LITERAL(1, 26, 7), // "pressed"
QT_MOC_LITERAL(2, 34, 0), // ""
QT_MOC_LITERAL(3, 35, 15), // "QList<QObject*>"
QT_MOC_LITERAL(4, 51, 11), // "touchPoints"
QT_MOC_LITERAL(5, 63, 7), // "updated"
QT_MOC_LITERAL(6, 71, 8), // "released"
QT_MOC_LITERAL(7, 80, 8), // "canceled"
QT_MOC_LITERAL(8, 89, 14), // "gestureStarted"
QT_MOC_LITERAL(9, 104, 23), // "QQuickGrabGestureEvent*"
QT_MOC_LITERAL(10, 128, 7), // "gesture"
QT_MOC_LITERAL(11, 136, 12), // "touchUpdated"
QT_MOC_LITERAL(12, 149, 25), // "minimumTouchPointsChanged"
QT_MOC_LITERAL(13, 175, 25), // "maximumTouchPointsChanged"
QT_MOC_LITERAL(14, 201, 19), // "mouseEnabledChanged"
QT_MOC_LITERAL(15, 221, 34), // "QQmlListProperty<QQuickTouchP..."
QT_MOC_LITERAL(16, 256, 18), // "minimumTouchPoints"
QT_MOC_LITERAL(17, 275, 18), // "maximumTouchPoints"
QT_MOC_LITERAL(18, 294, 12) // "mouseEnabled"

    },
    "QQuickMultiPointTouchArea\0pressed\0\0"
    "QList<QObject*>\0touchPoints\0updated\0"
    "released\0canceled\0gestureStarted\0"
    "QQuickGrabGestureEvent*\0gesture\0"
    "touchUpdated\0minimumTouchPointsChanged\0"
    "maximumTouchPointsChanged\0mouseEnabledChanged\0"
    "QQmlListProperty<QQuickTouchPoint>\0"
    "minimumTouchPoints\0maximumTouchPoints\0"
    "mouseEnabled"
};
#undef QT_MOC_LITERAL

static const uint qt_meta_data_QQuickMultiPointTouchArea[] = {

 // content:
       7,       // revision
       0,       // classname
       0,    0, // classinfo
       9,   14, // methods
       4,   80, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       9,       // signalCount

 // signals: name, argc, parameters, tag, flags
       1,    1,   59,    2, 0x06 /* Public */,
       5,    1,   62,    2, 0x06 /* Public */,
       6,    1,   65,    2, 0x06 /* Public */,
       7,    1,   68,    2, 0x06 /* Public */,
       8,    1,   71,    2, 0x06 /* Public */,
      11,    1,   74,    2, 0x06 /* Public */,
      12,    0,   77,    2, 0x06 /* Public */,
      13,    0,   78,    2, 0x06 /* Public */,
      14,    0,   79,    2, 0x06 /* Public */,

 // signals: parameters
    QMetaType::Void, 0x80000000 | 3,    4,
    QMetaType::Void, 0x80000000 | 3,    4,
    QMetaType::Void, 0x80000000 | 3,    4,
    QMetaType::Void, 0x80000000 | 3,    4,
    QMetaType::Void, 0x80000000 | 9,   10,
    QMetaType::Void, 0x80000000 | 3,    4,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,

 // properties: name, type, flags
       4, 0x80000000 | 15, 0x00095009,
      16, QMetaType::Int, 0x00495103,
      17, QMetaType::Int, 0x00495103,
      18, QMetaType::Bool, 0x00495103,

 // properties: notify_signal_id
       0,
       6,
       7,
       8,

       0        // eod
};

void QQuickMultiPointTouchArea::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        QQuickMultiPointTouchArea *_t = static_cast<QQuickMultiPointTouchArea *>(_o);
        Q_UNUSED(_t)
        switch (_id) {
        case 0: _t->pressed((*reinterpret_cast< const QList<QObject*>(*)>(_a[1]))); break;
        case 1: _t->updated((*reinterpret_cast< const QList<QObject*>(*)>(_a[1]))); break;
        case 2: _t->released((*reinterpret_cast< const QList<QObject*>(*)>(_a[1]))); break;
        case 3: _t->canceled((*reinterpret_cast< const QList<QObject*>(*)>(_a[1]))); break;
        case 4: _t->gestureStarted((*reinterpret_cast< QQuickGrabGestureEvent*(*)>(_a[1]))); break;
        case 5: _t->touchUpdated((*reinterpret_cast< const QList<QObject*>(*)>(_a[1]))); break;
        case 6: _t->minimumTouchPointsChanged(); break;
        case 7: _t->maximumTouchPointsChanged(); break;
        case 8: _t->mouseEnabledChanged(); break;
        default: ;
        }
    } else if (_c == QMetaObject::RegisterMethodArgumentMetaType) {
        switch (_id) {
        default: *reinterpret_cast<int*>(_a[0]) = -1; break;
        case 0:
            switch (*reinterpret_cast<int*>(_a[1])) {
            default: *reinterpret_cast<int*>(_a[0]) = -1; break;
            case 0:
                *reinterpret_cast<int*>(_a[0]) = qRegisterMetaType< QList<QObject*> >(); break;
            }
            break;
        case 1:
            switch (*reinterpret_cast<int*>(_a[1])) {
            default: *reinterpret_cast<int*>(_a[0]) = -1; break;
            case 0:
                *reinterpret_cast<int*>(_a[0]) = qRegisterMetaType< QList<QObject*> >(); break;
            }
            break;
        case 2:
            switch (*reinterpret_cast<int*>(_a[1])) {
            default: *reinterpret_cast<int*>(_a[0]) = -1; break;
            case 0:
                *reinterpret_cast<int*>(_a[0]) = qRegisterMetaType< QList<QObject*> >(); break;
            }
            break;
        case 3:
            switch (*reinterpret_cast<int*>(_a[1])) {
            default: *reinterpret_cast<int*>(_a[0]) = -1; break;
            case 0:
                *reinterpret_cast<int*>(_a[0]) = qRegisterMetaType< QList<QObject*> >(); break;
            }
            break;
        case 4:
            switch (*reinterpret_cast<int*>(_a[1])) {
            default: *reinterpret_cast<int*>(_a[0]) = -1; break;
            case 0:
                *reinterpret_cast<int*>(_a[0]) = qRegisterMetaType< QQuickGrabGestureEvent* >(); break;
            }
            break;
        case 5:
            switch (*reinterpret_cast<int*>(_a[1])) {
            default: *reinterpret_cast<int*>(_a[0]) = -1; break;
            case 0:
                *reinterpret_cast<int*>(_a[0]) = qRegisterMetaType< QList<QObject*> >(); break;
            }
            break;
        }
    } else if (_c == QMetaObject::IndexOfMethod) {
        int *result = reinterpret_cast<int *>(_a[0]);
        void **func = reinterpret_cast<void **>(_a[1]);
        {
            typedef void (QQuickMultiPointTouchArea::*_t)(const QList<QObject*> & );
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&QQuickMultiPointTouchArea::pressed)) {
                *result = 0;
                return;
            }
        }
        {
            typedef void (QQuickMultiPointTouchArea::*_t)(const QList<QObject*> & );
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&QQuickMultiPointTouchArea::updated)) {
                *result = 1;
                return;
            }
        }
        {
            typedef void (QQuickMultiPointTouchArea::*_t)(const QList<QObject*> & );
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&QQuickMultiPointTouchArea::released)) {
                *result = 2;
                return;
            }
        }
        {
            typedef void (QQuickMultiPointTouchArea::*_t)(const QList<QObject*> & );
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&QQuickMultiPointTouchArea::canceled)) {
                *result = 3;
                return;
            }
        }
        {
            typedef void (QQuickMultiPointTouchArea::*_t)(QQuickGrabGestureEvent * );
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&QQuickMultiPointTouchArea::gestureStarted)) {
                *result = 4;
                return;
            }
        }
        {
            typedef void (QQuickMultiPointTouchArea::*_t)(const QList<QObject*> & );
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&QQuickMultiPointTouchArea::touchUpdated)) {
                *result = 5;
                return;
            }
        }
        {
            typedef void (QQuickMultiPointTouchArea::*_t)();
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&QQuickMultiPointTouchArea::minimumTouchPointsChanged)) {
                *result = 6;
                return;
            }
        }
        {
            typedef void (QQuickMultiPointTouchArea::*_t)();
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&QQuickMultiPointTouchArea::maximumTouchPointsChanged)) {
                *result = 7;
                return;
            }
        }
        {
            typedef void (QQuickMultiPointTouchArea::*_t)();
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&QQuickMultiPointTouchArea::mouseEnabledChanged)) {
                *result = 8;
                return;
            }
        }
    } else if (_c == QMetaObject::RegisterPropertyMetaType) {
        switch (_id) {
        default: *reinterpret_cast<int*>(_a[0]) = -1; break;
        case 0:
            *reinterpret_cast<int*>(_a[0]) = qRegisterMetaType< QQmlListProperty<QQuickTouchPoint> >(); break;
        }
    }

#ifndef QT_NO_PROPERTIES
    else if (_c == QMetaObject::ReadProperty) {
        QQuickMultiPointTouchArea *_t = static_cast<QQuickMultiPointTouchArea *>(_o);
        Q_UNUSED(_t)
        void *_v = _a[0];
        switch (_id) {
        case 0: *reinterpret_cast< QQmlListProperty<QQuickTouchPoint>*>(_v) = _t->touchPoints(); break;
        case 1: *reinterpret_cast< int*>(_v) = _t->minimumTouchPoints(); break;
        case 2: *reinterpret_cast< int*>(_v) = _t->maximumTouchPoints(); break;
        case 3: *reinterpret_cast< bool*>(_v) = _t->mouseEnabled(); break;
        default: break;
        }
    } else if (_c == QMetaObject::WriteProperty) {
        QQuickMultiPointTouchArea *_t = static_cast<QQuickMultiPointTouchArea *>(_o);
        Q_UNUSED(_t)
        void *_v = _a[0];
        switch (_id) {
        case 1: _t->setMinimumTouchPoints(*reinterpret_cast< int*>(_v)); break;
        case 2: _t->setMaximumTouchPoints(*reinterpret_cast< int*>(_v)); break;
        case 3: _t->setMouseEnabled(*reinterpret_cast< bool*>(_v)); break;
        default: break;
        }
    } else if (_c == QMetaObject::ResetProperty) {
    }
#endif // QT_NO_PROPERTIES
}

const QMetaObject QQuickMultiPointTouchArea::staticMetaObject = {
    { &QQuickItem::staticMetaObject, qt_meta_stringdata_QQuickMultiPointTouchArea.data,
      qt_meta_data_QQuickMultiPointTouchArea,  qt_static_metacall, Q_NULLPTR, Q_NULLPTR}
};


const QMetaObject *QQuickMultiPointTouchArea::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->dynamicMetaObject() : &staticMetaObject;
}

void *QQuickMultiPointTouchArea::qt_metacast(const char *_clname)
{
    if (!_clname) return Q_NULLPTR;
    if (!strcmp(_clname, qt_meta_stringdata_QQuickMultiPointTouchArea.stringdata0))
        return static_cast<void*>(const_cast< QQuickMultiPointTouchArea*>(this));
    return QQuickItem::qt_metacast(_clname);
}

int QQuickMultiPointTouchArea::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QQuickItem::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 9)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 9;
    } else if (_c == QMetaObject::RegisterMethodArgumentMetaType) {
        if (_id < 9)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 9;
    }
#ifndef QT_NO_PROPERTIES
   else if (_c == QMetaObject::ReadProperty || _c == QMetaObject::WriteProperty
            || _c == QMetaObject::ResetProperty || _c == QMetaObject::RegisterPropertyMetaType) {
        qt_static_metacall(this, _c, _id, _a);
        _id -= 4;
    } else if (_c == QMetaObject::QueryPropertyDesignable) {
        _id -= 4;
    } else if (_c == QMetaObject::QueryPropertyScriptable) {
        _id -= 4;
    } else if (_c == QMetaObject::QueryPropertyStored) {
        _id -= 4;
    } else if (_c == QMetaObject::QueryPropertyEditable) {
        _id -= 4;
    } else if (_c == QMetaObject::QueryPropertyUser) {
        _id -= 4;
    }
#endif // QT_NO_PROPERTIES
    return _id;
}

// SIGNAL 0
void QQuickMultiPointTouchArea::pressed(const QList<QObject*> & _t1)
{
    void *_a[] = { Q_NULLPTR, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 0, _a);
}

// SIGNAL 1
void QQuickMultiPointTouchArea::updated(const QList<QObject*> & _t1)
{
    void *_a[] = { Q_NULLPTR, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 1, _a);
}

// SIGNAL 2
void QQuickMultiPointTouchArea::released(const QList<QObject*> & _t1)
{
    void *_a[] = { Q_NULLPTR, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 2, _a);
}

// SIGNAL 3
void QQuickMultiPointTouchArea::canceled(const QList<QObject*> & _t1)
{
    void *_a[] = { Q_NULLPTR, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 3, _a);
}

// SIGNAL 4
void QQuickMultiPointTouchArea::gestureStarted(QQuickGrabGestureEvent * _t1)
{
    void *_a[] = { Q_NULLPTR, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 4, _a);
}

// SIGNAL 5
void QQuickMultiPointTouchArea::touchUpdated(const QList<QObject*> & _t1)
{
    void *_a[] = { Q_NULLPTR, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 5, _a);
}

// SIGNAL 6
void QQuickMultiPointTouchArea::minimumTouchPointsChanged()
{
    QMetaObject::activate(this, &staticMetaObject, 6, Q_NULLPTR);
}

// SIGNAL 7
void QQuickMultiPointTouchArea::maximumTouchPointsChanged()
{
    QMetaObject::activate(this, &staticMetaObject, 7, Q_NULLPTR);
}

// SIGNAL 8
void QQuickMultiPointTouchArea::mouseEnabledChanged()
{
    QMetaObject::activate(this, &staticMetaObject, 8, Q_NULLPTR);
}
QT_END_MOC_NAMESPACE
