/****************************************************************************
** Meta object code from reading C++ file 'qquickwindowattached_p.h'
**
** Created by: The Qt Meta Object Compiler version 67 (Qt 5.6.3)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "../../items/qquickwindowattached_p.h"
#include <QtCore/qbytearray.h>
#include <QtCore/qmetatype.h>
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'qquickwindowattached_p.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 67
#error "This file was generated using the moc from 5.6.3. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
struct qt_meta_stringdata_QQuickWindowAttached_t {
    QByteArrayData data[18];
    char stringdata0[242];
};
#define QT_MOC_LITERAL(idx, ofs, len) \
    Q_STATIC_BYTE_ARRAY_DATA_HEADER_INITIALIZER_WITH_OFFSET(len, \
    qptrdiff(offsetof(qt_meta_stringdata_QQuickWindowAttached_t, stringdata0) + ofs \
        - idx * sizeof(QByteArrayData)) \
    )
static const qt_meta_stringdata_QQuickWindowAttached_t qt_meta_stringdata_QQuickWindowAttached = {
    {
QT_MOC_LITERAL(0, 0, 20), // "QQuickWindowAttached"
QT_MOC_LITERAL(1, 21, 17), // "visibilityChanged"
QT_MOC_LITERAL(2, 39, 0), // ""
QT_MOC_LITERAL(3, 40, 13), // "activeChanged"
QT_MOC_LITERAL(4, 54, 22), // "activeFocusItemChanged"
QT_MOC_LITERAL(5, 77, 18), // "contentItemChanged"
QT_MOC_LITERAL(6, 96, 12), // "widthChanged"
QT_MOC_LITERAL(7, 109, 13), // "heightChanged"
QT_MOC_LITERAL(8, 123, 13), // "windowChanged"
QT_MOC_LITERAL(9, 137, 13), // "QQuickWindow*"
QT_MOC_LITERAL(10, 151, 10), // "visibility"
QT_MOC_LITERAL(11, 162, 19), // "QWindow::Visibility"
QT_MOC_LITERAL(12, 182, 6), // "active"
QT_MOC_LITERAL(13, 189, 15), // "activeFocusItem"
QT_MOC_LITERAL(14, 205, 11), // "QQuickItem*"
QT_MOC_LITERAL(15, 217, 11), // "contentItem"
QT_MOC_LITERAL(16, 229, 5), // "width"
QT_MOC_LITERAL(17, 235, 6) // "height"

    },
    "QQuickWindowAttached\0visibilityChanged\0"
    "\0activeChanged\0activeFocusItemChanged\0"
    "contentItemChanged\0widthChanged\0"
    "heightChanged\0windowChanged\0QQuickWindow*\0"
    "visibility\0QWindow::Visibility\0active\0"
    "activeFocusItem\0QQuickItem*\0contentItem\0"
    "width\0height"
};
#undef QT_MOC_LITERAL

static const uint qt_meta_data_QQuickWindowAttached[] = {

 // content:
       7,       // revision
       0,       // classname
       0,    0, // classinfo
       7,   14, // methods
       6,   58, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       6,       // signalCount

 // signals: name, argc, parameters, tag, flags
       1,    0,   49,    2, 0x06 /* Public */,
       3,    0,   50,    2, 0x06 /* Public */,
       4,    0,   51,    2, 0x06 /* Public */,
       5,    0,   52,    2, 0x06 /* Public */,
       6,    0,   53,    2, 0x06 /* Public */,
       7,    0,   54,    2, 0x06 /* Public */,

 // slots: name, argc, parameters, tag, flags
       8,    1,   55,    2, 0x09 /* Protected */,

 // signals: parameters
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,

 // slots: parameters
    QMetaType::Void, 0x80000000 | 9,    2,

 // properties: name, type, flags
      10, 0x80000000 | 11, 0x00495009,
      12, QMetaType::Bool, 0x00495001,
      13, 0x80000000 | 14, 0x00495009,
      15, 0x80000000 | 14, 0x00495009,
      16, QMetaType::Int, 0x00495001,
      17, QMetaType::Int, 0x00495001,

 // properties: notify_signal_id
       0,
       1,
       2,
       3,
       4,
       5,

       0        // eod
};

void QQuickWindowAttached::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        QQuickWindowAttached *_t = static_cast<QQuickWindowAttached *>(_o);
        Q_UNUSED(_t)
        switch (_id) {
        case 0: _t->visibilityChanged(); break;
        case 1: _t->activeChanged(); break;
        case 2: _t->activeFocusItemChanged(); break;
        case 3: _t->contentItemChanged(); break;
        case 4: _t->widthChanged(); break;
        case 5: _t->heightChanged(); break;
        case 6: _t->windowChanged((*reinterpret_cast< QQuickWindow*(*)>(_a[1]))); break;
        default: ;
        }
    } else if (_c == QMetaObject::IndexOfMethod) {
        int *result = reinterpret_cast<int *>(_a[0]);
        void **func = reinterpret_cast<void **>(_a[1]);
        {
            typedef void (QQuickWindowAttached::*_t)();
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&QQuickWindowAttached::visibilityChanged)) {
                *result = 0;
                return;
            }
        }
        {
            typedef void (QQuickWindowAttached::*_t)();
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&QQuickWindowAttached::activeChanged)) {
                *result = 1;
                return;
            }
        }
        {
            typedef void (QQuickWindowAttached::*_t)();
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&QQuickWindowAttached::activeFocusItemChanged)) {
                *result = 2;
                return;
            }
        }
        {
            typedef void (QQuickWindowAttached::*_t)();
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&QQuickWindowAttached::contentItemChanged)) {
                *result = 3;
                return;
            }
        }
        {
            typedef void (QQuickWindowAttached::*_t)();
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&QQuickWindowAttached::widthChanged)) {
                *result = 4;
                return;
            }
        }
        {
            typedef void (QQuickWindowAttached::*_t)();
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&QQuickWindowAttached::heightChanged)) {
                *result = 5;
                return;
            }
        }
    }
#ifndef QT_NO_PROPERTIES
    else if (_c == QMetaObject::ReadProperty) {
        QQuickWindowAttached *_t = static_cast<QQuickWindowAttached *>(_o);
        Q_UNUSED(_t)
        void *_v = _a[0];
        switch (_id) {
        case 0: *reinterpret_cast< QWindow::Visibility*>(_v) = _t->visibility(); break;
        case 1: *reinterpret_cast< bool*>(_v) = _t->isActive(); break;
        case 2: *reinterpret_cast< QQuickItem**>(_v) = _t->activeFocusItem(); break;
        case 3: *reinterpret_cast< QQuickItem**>(_v) = _t->contentItem(); break;
        case 4: *reinterpret_cast< int*>(_v) = _t->width(); break;
        case 5: *reinterpret_cast< int*>(_v) = _t->height(); break;
        default: break;
        }
    } else if (_c == QMetaObject::WriteProperty) {
    } else if (_c == QMetaObject::ResetProperty) {
    }
#endif // QT_NO_PROPERTIES
}

static const QMetaObject * const qt_meta_extradata_QQuickWindowAttached[] = {
        &QWindow::staticMetaObject,
    Q_NULLPTR
};

const QMetaObject QQuickWindowAttached::staticMetaObject = {
    { &QObject::staticMetaObject, qt_meta_stringdata_QQuickWindowAttached.data,
      qt_meta_data_QQuickWindowAttached,  qt_static_metacall, qt_meta_extradata_QQuickWindowAttached, Q_NULLPTR}
};


const QMetaObject *QQuickWindowAttached::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->dynamicMetaObject() : &staticMetaObject;
}

void *QQuickWindowAttached::qt_metacast(const char *_clname)
{
    if (!_clname) return Q_NULLPTR;
    if (!strcmp(_clname, qt_meta_stringdata_QQuickWindowAttached.stringdata0))
        return static_cast<void*>(const_cast< QQuickWindowAttached*>(this));
    return QObject::qt_metacast(_clname);
}

int QQuickWindowAttached::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QObject::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 7)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 7;
    } else if (_c == QMetaObject::RegisterMethodArgumentMetaType) {
        if (_id < 7)
            *reinterpret_cast<int*>(_a[0]) = -1;
        _id -= 7;
    }
#ifndef QT_NO_PROPERTIES
   else if (_c == QMetaObject::ReadProperty || _c == QMetaObject::WriteProperty
            || _c == QMetaObject::ResetProperty || _c == QMetaObject::RegisterPropertyMetaType) {
        qt_static_metacall(this, _c, _id, _a);
        _id -= 6;
    } else if (_c == QMetaObject::QueryPropertyDesignable) {
        _id -= 6;
    } else if (_c == QMetaObject::QueryPropertyScriptable) {
        _id -= 6;
    } else if (_c == QMetaObject::QueryPropertyStored) {
        _id -= 6;
    } else if (_c == QMetaObject::QueryPropertyEditable) {
        _id -= 6;
    } else if (_c == QMetaObject::QueryPropertyUser) {
        _id -= 6;
    }
#endif // QT_NO_PROPERTIES
    return _id;
}

// SIGNAL 0
void QQuickWindowAttached::visibilityChanged()
{
    QMetaObject::activate(this, &staticMetaObject, 0, Q_NULLPTR);
}

// SIGNAL 1
void QQuickWindowAttached::activeChanged()
{
    QMetaObject::activate(this, &staticMetaObject, 1, Q_NULLPTR);
}

// SIGNAL 2
void QQuickWindowAttached::activeFocusItemChanged()
{
    QMetaObject::activate(this, &staticMetaObject, 2, Q_NULLPTR);
}

// SIGNAL 3
void QQuickWindowAttached::contentItemChanged()
{
    QMetaObject::activate(this, &staticMetaObject, 3, Q_NULLPTR);
}

// SIGNAL 4
void QQuickWindowAttached::widthChanged()
{
    QMetaObject::activate(this, &staticMetaObject, 4, Q_NULLPTR);
}

// SIGNAL 5
void QQuickWindowAttached::heightChanged()
{
    QMetaObject::activate(this, &staticMetaObject, 5, Q_NULLPTR);
}
QT_END_MOC_NAMESPACE
