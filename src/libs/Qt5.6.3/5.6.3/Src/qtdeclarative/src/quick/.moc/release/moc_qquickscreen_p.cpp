/****************************************************************************
** Meta object code from reading C++ file 'qquickscreen_p.h'
**
** Created by: The Qt Meta Object Compiler version 67 (Qt 5.6.3)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "../../items/qquickscreen_p.h"
#include <QtCore/qbytearray.h>
#include <QtCore/qmetatype.h>
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'qquickscreen_p.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 67
#error "This file was generated using the moc from 5.6.3. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
struct qt_meta_stringdata_QQuickScreenAttached_t {
    QByteArrayData data[30];
    char stringdata0[480];
};
#define QT_MOC_LITERAL(idx, ofs, len) \
    Q_STATIC_BYTE_ARRAY_DATA_HEADER_INITIALIZER_WITH_OFFSET(len, \
    qptrdiff(offsetof(qt_meta_stringdata_QQuickScreenAttached_t, stringdata0) + ofs \
        - idx * sizeof(QByteArrayData)) \
    )
static const qt_meta_stringdata_QQuickScreenAttached_t qt_meta_stringdata_QQuickScreenAttached = {
    {
QT_MOC_LITERAL(0, 0, 20), // "QQuickScreenAttached"
QT_MOC_LITERAL(1, 21, 11), // "nameChanged"
QT_MOC_LITERAL(2, 33, 0), // ""
QT_MOC_LITERAL(3, 34, 12), // "widthChanged"
QT_MOC_LITERAL(4, 47, 13), // "heightChanged"
QT_MOC_LITERAL(5, 61, 22), // "desktopGeometryChanged"
QT_MOC_LITERAL(6, 84, 26), // "logicalPixelDensityChanged"
QT_MOC_LITERAL(7, 111, 19), // "pixelDensityChanged"
QT_MOC_LITERAL(8, 131, 23), // "devicePixelRatioChanged"
QT_MOC_LITERAL(9, 155, 25), // "primaryOrientationChanged"
QT_MOC_LITERAL(10, 181, 18), // "orientationChanged"
QT_MOC_LITERAL(11, 200, 28), // "orientationUpdateMaskChanged"
QT_MOC_LITERAL(12, 229, 13), // "screenChanged"
QT_MOC_LITERAL(13, 243, 8), // "QScreen*"
QT_MOC_LITERAL(14, 252, 12), // "angleBetween"
QT_MOC_LITERAL(15, 265, 1), // "a"
QT_MOC_LITERAL(16, 267, 1), // "b"
QT_MOC_LITERAL(17, 269, 4), // "name"
QT_MOC_LITERAL(18, 274, 5), // "width"
QT_MOC_LITERAL(19, 280, 6), // "height"
QT_MOC_LITERAL(20, 287, 21), // "desktopAvailableWidth"
QT_MOC_LITERAL(21, 309, 22), // "desktopAvailableHeight"
QT_MOC_LITERAL(22, 332, 19), // "logicalPixelDensity"
QT_MOC_LITERAL(23, 352, 12), // "pixelDensity"
QT_MOC_LITERAL(24, 365, 16), // "devicePixelRatio"
QT_MOC_LITERAL(25, 382, 18), // "primaryOrientation"
QT_MOC_LITERAL(26, 401, 21), // "Qt::ScreenOrientation"
QT_MOC_LITERAL(27, 423, 11), // "orientation"
QT_MOC_LITERAL(28, 435, 21), // "orientationUpdateMask"
QT_MOC_LITERAL(29, 457, 22) // "Qt::ScreenOrientations"

    },
    "QQuickScreenAttached\0nameChanged\0\0"
    "widthChanged\0heightChanged\0"
    "desktopGeometryChanged\0"
    "logicalPixelDensityChanged\0"
    "pixelDensityChanged\0devicePixelRatioChanged\0"
    "primaryOrientationChanged\0orientationChanged\0"
    "orientationUpdateMaskChanged\0screenChanged\0"
    "QScreen*\0angleBetween\0a\0b\0name\0width\0"
    "height\0desktopAvailableWidth\0"
    "desktopAvailableHeight\0logicalPixelDensity\0"
    "pixelDensity\0devicePixelRatio\0"
    "primaryOrientation\0Qt::ScreenOrientation\0"
    "orientation\0orientationUpdateMask\0"
    "Qt::ScreenOrientations"
};
#undef QT_MOC_LITERAL

static const uint qt_meta_data_QQuickScreenAttached[] = {

 // content:
       7,       // revision
       0,       // classname
       0,    0, // classinfo
      12,   14, // methods
      11,   92, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
      10,       // signalCount

 // signals: name, argc, parameters, tag, flags
       1,    0,   74,    2, 0x06 /* Public */,
       3,    0,   75,    2, 0x06 /* Public */,
       4,    0,   76,    2, 0x06 /* Public */,
       5,    0,   77,    2, 0x06 /* Public */,
       6,    0,   78,    2, 0x06 /* Public */,
       7,    0,   79,    2, 0x06 /* Public */,
       8,    0,   80,    2, 0x06 /* Public */,
       9,    0,   81,    2, 0x06 /* Public */,
      10,    0,   82,    2, 0x06 /* Public */,
      11,    0,   83,    2, 0x06 /* Public */,

 // slots: name, argc, parameters, tag, flags
      12,    1,   84,    2, 0x09 /* Protected */,

 // methods: name, argc, parameters, tag, flags
      14,    2,   87,    2, 0x02 /* Public */,

 // signals: parameters
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,

 // slots: parameters
    QMetaType::Void, 0x80000000 | 13,    2,

 // methods: parameters
    QMetaType::Int, QMetaType::Int, QMetaType::Int,   15,   16,

 // properties: name, type, flags
      17, QMetaType::QString, 0x00495001,
      18, QMetaType::Int, 0x00495001,
      19, QMetaType::Int, 0x00495001,
      20, QMetaType::Int, 0x00495001,
      21, QMetaType::Int, 0x00495001,
      22, QMetaType::QReal, 0x00495001,
      23, QMetaType::QReal, 0x00495001,
      24, QMetaType::QReal, 0x00495001,
      25, 0x80000000 | 26, 0x00495009,
      27, 0x80000000 | 26, 0x00495009,
      28, 0x80000000 | 29, 0x0049510b,

 // properties: notify_signal_id
       0,
       1,
       2,
       3,
       3,
       4,
       5,
       6,
       7,
       8,
       9,

       0        // eod
};

void QQuickScreenAttached::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        QQuickScreenAttached *_t = static_cast<QQuickScreenAttached *>(_o);
        Q_UNUSED(_t)
        switch (_id) {
        case 0: _t->nameChanged(); break;
        case 1: _t->widthChanged(); break;
        case 2: _t->heightChanged(); break;
        case 3: _t->desktopGeometryChanged(); break;
        case 4: _t->logicalPixelDensityChanged(); break;
        case 5: _t->pixelDensityChanged(); break;
        case 6: _t->devicePixelRatioChanged(); break;
        case 7: _t->primaryOrientationChanged(); break;
        case 8: _t->orientationChanged(); break;
        case 9: _t->orientationUpdateMaskChanged(); break;
        case 10: _t->screenChanged((*reinterpret_cast< QScreen*(*)>(_a[1]))); break;
        case 11: { int _r = _t->angleBetween((*reinterpret_cast< int(*)>(_a[1])),(*reinterpret_cast< int(*)>(_a[2])));
            if (_a[0]) *reinterpret_cast< int*>(_a[0]) = _r; }  break;
        default: ;
        }
    } else if (_c == QMetaObject::IndexOfMethod) {
        int *result = reinterpret_cast<int *>(_a[0]);
        void **func = reinterpret_cast<void **>(_a[1]);
        {
            typedef void (QQuickScreenAttached::*_t)();
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&QQuickScreenAttached::nameChanged)) {
                *result = 0;
                return;
            }
        }
        {
            typedef void (QQuickScreenAttached::*_t)();
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&QQuickScreenAttached::widthChanged)) {
                *result = 1;
                return;
            }
        }
        {
            typedef void (QQuickScreenAttached::*_t)();
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&QQuickScreenAttached::heightChanged)) {
                *result = 2;
                return;
            }
        }
        {
            typedef void (QQuickScreenAttached::*_t)();
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&QQuickScreenAttached::desktopGeometryChanged)) {
                *result = 3;
                return;
            }
        }
        {
            typedef void (QQuickScreenAttached::*_t)();
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&QQuickScreenAttached::logicalPixelDensityChanged)) {
                *result = 4;
                return;
            }
        }
        {
            typedef void (QQuickScreenAttached::*_t)();
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&QQuickScreenAttached::pixelDensityChanged)) {
                *result = 5;
                return;
            }
        }
        {
            typedef void (QQuickScreenAttached::*_t)();
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&QQuickScreenAttached::devicePixelRatioChanged)) {
                *result = 6;
                return;
            }
        }
        {
            typedef void (QQuickScreenAttached::*_t)();
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&QQuickScreenAttached::primaryOrientationChanged)) {
                *result = 7;
                return;
            }
        }
        {
            typedef void (QQuickScreenAttached::*_t)();
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&QQuickScreenAttached::orientationChanged)) {
                *result = 8;
                return;
            }
        }
        {
            typedef void (QQuickScreenAttached::*_t)();
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&QQuickScreenAttached::orientationUpdateMaskChanged)) {
                *result = 9;
                return;
            }
        }
    }
#ifndef QT_NO_PROPERTIES
    else if (_c == QMetaObject::ReadProperty) {
        QQuickScreenAttached *_t = static_cast<QQuickScreenAttached *>(_o);
        Q_UNUSED(_t)
        void *_v = _a[0];
        switch (_id) {
        case 0: *reinterpret_cast< QString*>(_v) = _t->name(); break;
        case 1: *reinterpret_cast< int*>(_v) = _t->width(); break;
        case 2: *reinterpret_cast< int*>(_v) = _t->height(); break;
        case 3: *reinterpret_cast< int*>(_v) = _t->desktopAvailableWidth(); break;
        case 4: *reinterpret_cast< int*>(_v) = _t->desktopAvailableHeight(); break;
        case 5: *reinterpret_cast< qreal*>(_v) = _t->logicalPixelDensity(); break;
        case 6: *reinterpret_cast< qreal*>(_v) = _t->pixelDensity(); break;
        case 7: *reinterpret_cast< qreal*>(_v) = _t->devicePixelRatio(); break;
        case 8: *reinterpret_cast< Qt::ScreenOrientation*>(_v) = _t->primaryOrientation(); break;
        case 9: *reinterpret_cast< Qt::ScreenOrientation*>(_v) = _t->orientation(); break;
        case 10: *reinterpret_cast< Qt::ScreenOrientations*>(_v) = _t->orientationUpdateMask(); break;
        default: break;
        }
    } else if (_c == QMetaObject::WriteProperty) {
        QQuickScreenAttached *_t = static_cast<QQuickScreenAttached *>(_o);
        Q_UNUSED(_t)
        void *_v = _a[0];
        switch (_id) {
        case 10: _t->setOrientationUpdateMask(*reinterpret_cast< Qt::ScreenOrientations*>(_v)); break;
        default: break;
        }
    } else if (_c == QMetaObject::ResetProperty) {
    }
#endif // QT_NO_PROPERTIES
}

const QMetaObject QQuickScreenAttached::staticMetaObject = {
    { &QObject::staticMetaObject, qt_meta_stringdata_QQuickScreenAttached.data,
      qt_meta_data_QQuickScreenAttached,  qt_static_metacall, Q_NULLPTR, Q_NULLPTR}
};


const QMetaObject *QQuickScreenAttached::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->dynamicMetaObject() : &staticMetaObject;
}

void *QQuickScreenAttached::qt_metacast(const char *_clname)
{
    if (!_clname) return Q_NULLPTR;
    if (!strcmp(_clname, qt_meta_stringdata_QQuickScreenAttached.stringdata0))
        return static_cast<void*>(const_cast< QQuickScreenAttached*>(this));
    return QObject::qt_metacast(_clname);
}

int QQuickScreenAttached::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QObject::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 12)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 12;
    } else if (_c == QMetaObject::RegisterMethodArgumentMetaType) {
        if (_id < 12)
            *reinterpret_cast<int*>(_a[0]) = -1;
        _id -= 12;
    }
#ifndef QT_NO_PROPERTIES
   else if (_c == QMetaObject::ReadProperty || _c == QMetaObject::WriteProperty
            || _c == QMetaObject::ResetProperty || _c == QMetaObject::RegisterPropertyMetaType) {
        qt_static_metacall(this, _c, _id, _a);
        _id -= 11;
    } else if (_c == QMetaObject::QueryPropertyDesignable) {
        _id -= 11;
    } else if (_c == QMetaObject::QueryPropertyScriptable) {
        _id -= 11;
    } else if (_c == QMetaObject::QueryPropertyStored) {
        _id -= 11;
    } else if (_c == QMetaObject::QueryPropertyEditable) {
        _id -= 11;
    } else if (_c == QMetaObject::QueryPropertyUser) {
        _id -= 11;
    }
#endif // QT_NO_PROPERTIES
    return _id;
}

// SIGNAL 0
void QQuickScreenAttached::nameChanged()
{
    QMetaObject::activate(this, &staticMetaObject, 0, Q_NULLPTR);
}

// SIGNAL 1
void QQuickScreenAttached::widthChanged()
{
    QMetaObject::activate(this, &staticMetaObject, 1, Q_NULLPTR);
}

// SIGNAL 2
void QQuickScreenAttached::heightChanged()
{
    QMetaObject::activate(this, &staticMetaObject, 2, Q_NULLPTR);
}

// SIGNAL 3
void QQuickScreenAttached::desktopGeometryChanged()
{
    QMetaObject::activate(this, &staticMetaObject, 3, Q_NULLPTR);
}

// SIGNAL 4
void QQuickScreenAttached::logicalPixelDensityChanged()
{
    QMetaObject::activate(this, &staticMetaObject, 4, Q_NULLPTR);
}

// SIGNAL 5
void QQuickScreenAttached::pixelDensityChanged()
{
    QMetaObject::activate(this, &staticMetaObject, 5, Q_NULLPTR);
}

// SIGNAL 6
void QQuickScreenAttached::devicePixelRatioChanged()
{
    QMetaObject::activate(this, &staticMetaObject, 6, Q_NULLPTR);
}

// SIGNAL 7
void QQuickScreenAttached::primaryOrientationChanged()
{
    QMetaObject::activate(this, &staticMetaObject, 7, Q_NULLPTR);
}

// SIGNAL 8
void QQuickScreenAttached::orientationChanged()
{
    QMetaObject::activate(this, &staticMetaObject, 8, Q_NULLPTR);
}

// SIGNAL 9
void QQuickScreenAttached::orientationUpdateMaskChanged()
{
    QMetaObject::activate(this, &staticMetaObject, 9, Q_NULLPTR);
}
struct qt_meta_stringdata_QQuickScreen_t {
    QByteArrayData data[1];
    char stringdata0[13];
};
#define QT_MOC_LITERAL(idx, ofs, len) \
    Q_STATIC_BYTE_ARRAY_DATA_HEADER_INITIALIZER_WITH_OFFSET(len, \
    qptrdiff(offsetof(qt_meta_stringdata_QQuickScreen_t, stringdata0) + ofs \
        - idx * sizeof(QByteArrayData)) \
    )
static const qt_meta_stringdata_QQuickScreen_t qt_meta_stringdata_QQuickScreen = {
    {
QT_MOC_LITERAL(0, 0, 12) // "QQuickScreen"

    },
    "QQuickScreen"
};
#undef QT_MOC_LITERAL

static const uint qt_meta_data_QQuickScreen[] = {

 // content:
       7,       // revision
       0,       // classname
       0,    0, // classinfo
       0,    0, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       0,       // signalCount

       0        // eod
};

void QQuickScreen::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    Q_UNUSED(_o);
    Q_UNUSED(_id);
    Q_UNUSED(_c);
    Q_UNUSED(_a);
}

const QMetaObject QQuickScreen::staticMetaObject = {
    { &QObject::staticMetaObject, qt_meta_stringdata_QQuickScreen.data,
      qt_meta_data_QQuickScreen,  qt_static_metacall, Q_NULLPTR, Q_NULLPTR}
};


const QMetaObject *QQuickScreen::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->dynamicMetaObject() : &staticMetaObject;
}

void *QQuickScreen::qt_metacast(const char *_clname)
{
    if (!_clname) return Q_NULLPTR;
    if (!strcmp(_clname, qt_meta_stringdata_QQuickScreen.stringdata0))
        return static_cast<void*>(const_cast< QQuickScreen*>(this));
    return QObject::qt_metacast(_clname);
}

int QQuickScreen::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QObject::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    return _id;
}
QT_END_MOC_NAMESPACE
