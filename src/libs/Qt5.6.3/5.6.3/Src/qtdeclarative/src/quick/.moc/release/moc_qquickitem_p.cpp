/****************************************************************************
** Meta object code from reading C++ file 'qquickitem_p.h'
**
** Created by: The Qt Meta Object Compiler version 67 (Qt 5.6.3)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "../../items/qquickitem_p.h"
#include <QtCore/qbytearray.h>
#include <QtCore/qmetatype.h>
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'qquickitem_p.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 67
#error "This file was generated using the moc from 5.6.3. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
struct qt_meta_stringdata_QQuickItemLayer_t {
    QByteArrayData data[31];
    char stringdata0[428];
};
#define QT_MOC_LITERAL(idx, ofs, len) \
    Q_STATIC_BYTE_ARRAY_DATA_HEADER_INITIALIZER_WITH_OFFSET(len, \
    qptrdiff(offsetof(qt_meta_stringdata_QQuickItemLayer_t, stringdata0) + ofs \
        - idx * sizeof(QByteArrayData)) \
    )
static const qt_meta_stringdata_QQuickItemLayer_t qt_meta_stringdata_QQuickItemLayer = {
    {
QT_MOC_LITERAL(0, 0, 15), // "QQuickItemLayer"
QT_MOC_LITERAL(1, 16, 14), // "enabledChanged"
QT_MOC_LITERAL(2, 31, 0), // ""
QT_MOC_LITERAL(3, 32, 7), // "enabled"
QT_MOC_LITERAL(4, 40, 11), // "sizeChanged"
QT_MOC_LITERAL(5, 52, 4), // "size"
QT_MOC_LITERAL(6, 57, 13), // "mipmapChanged"
QT_MOC_LITERAL(7, 71, 6), // "mipmap"
QT_MOC_LITERAL(8, 78, 15), // "wrapModeChanged"
QT_MOC_LITERAL(9, 94, 34), // "QQuickShaderEffectSource::Wra..."
QT_MOC_LITERAL(10, 129, 4), // "mode"
QT_MOC_LITERAL(11, 134, 11), // "nameChanged"
QT_MOC_LITERAL(12, 146, 4), // "name"
QT_MOC_LITERAL(13, 151, 13), // "effectChanged"
QT_MOC_LITERAL(14, 165, 14), // "QQmlComponent*"
QT_MOC_LITERAL(15, 180, 9), // "component"
QT_MOC_LITERAL(16, 190, 13), // "smoothChanged"
QT_MOC_LITERAL(17, 204, 6), // "smooth"
QT_MOC_LITERAL(18, 211, 13), // "formatChanged"
QT_MOC_LITERAL(19, 225, 32), // "QQuickShaderEffectSource::Format"
QT_MOC_LITERAL(20, 258, 6), // "format"
QT_MOC_LITERAL(21, 265, 17), // "sourceRectChanged"
QT_MOC_LITERAL(22, 283, 10), // "sourceRect"
QT_MOC_LITERAL(23, 294, 23), // "textureMirroringChanged"
QT_MOC_LITERAL(24, 318, 42), // "QQuickShaderEffectSource::Tex..."
QT_MOC_LITERAL(25, 361, 9), // "mirroring"
QT_MOC_LITERAL(26, 371, 11), // "textureSize"
QT_MOC_LITERAL(27, 383, 8), // "wrapMode"
QT_MOC_LITERAL(28, 392, 11), // "samplerName"
QT_MOC_LITERAL(29, 404, 6), // "effect"
QT_MOC_LITERAL(30, 411, 16) // "textureMirroring"

    },
    "QQuickItemLayer\0enabledChanged\0\0enabled\0"
    "sizeChanged\0size\0mipmapChanged\0mipmap\0"
    "wrapModeChanged\0QQuickShaderEffectSource::WrapMode\0"
    "mode\0nameChanged\0name\0effectChanged\0"
    "QQmlComponent*\0component\0smoothChanged\0"
    "smooth\0formatChanged\0"
    "QQuickShaderEffectSource::Format\0"
    "format\0sourceRectChanged\0sourceRect\0"
    "textureMirroringChanged\0"
    "QQuickShaderEffectSource::TextureMirroring\0"
    "mirroring\0textureSize\0wrapMode\0"
    "samplerName\0effect\0textureMirroring"
};
#undef QT_MOC_LITERAL

static const uint qt_meta_data_QQuickItemLayer[] = {

 // content:
       7,       // revision
       0,       // classname
       0,    0, // classinfo
      10,   14, // methods
      10,   94, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
      10,       // signalCount

 // signals: name, argc, parameters, tag, flags
       1,    1,   64,    2, 0x06 /* Public */,
       4,    1,   67,    2, 0x06 /* Public */,
       6,    1,   70,    2, 0x06 /* Public */,
       8,    1,   73,    2, 0x06 /* Public */,
      11,    1,   76,    2, 0x06 /* Public */,
      13,    1,   79,    2, 0x06 /* Public */,
      16,    1,   82,    2, 0x06 /* Public */,
      18,    1,   85,    2, 0x06 /* Public */,
      21,    1,   88,    2, 0x06 /* Public */,
      23,    1,   91,    2, 0x06 /* Public */,

 // signals: parameters
    QMetaType::Void, QMetaType::Bool,    3,
    QMetaType::Void, QMetaType::QSize,    5,
    QMetaType::Void, QMetaType::Bool,    7,
    QMetaType::Void, 0x80000000 | 9,   10,
    QMetaType::Void, QMetaType::QByteArray,   12,
    QMetaType::Void, 0x80000000 | 14,   15,
    QMetaType::Void, QMetaType::Bool,   17,
    QMetaType::Void, 0x80000000 | 19,   20,
    QMetaType::Void, QMetaType::QRectF,   22,
    QMetaType::Void, 0x80000000 | 24,   25,

 // properties: name, type, flags
       3, QMetaType::Bool, 0x00495103,
      26, QMetaType::QSize, 0x00495003,
      22, QMetaType::QRectF, 0x00495103,
       7, QMetaType::Bool, 0x00495103,
      17, QMetaType::Bool, 0x00495103,
      27, 0x80000000 | 9, 0x0049510b,
      20, 0x80000000 | 19, 0x0049510b,
      28, QMetaType::QByteArray, 0x00495003,
      29, 0x80000000 | 14, 0x0049510b,
      30, 0x80000000 | 24, 0x0049510b,

 // properties: notify_signal_id
       0,
       1,
       8,
       2,
       6,
       3,
       7,
       4,
       5,
       9,

       0        // eod
};

void QQuickItemLayer::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        QQuickItemLayer *_t = static_cast<QQuickItemLayer *>(_o);
        Q_UNUSED(_t)
        switch (_id) {
        case 0: _t->enabledChanged((*reinterpret_cast< bool(*)>(_a[1]))); break;
        case 1: _t->sizeChanged((*reinterpret_cast< const QSize(*)>(_a[1]))); break;
        case 2: _t->mipmapChanged((*reinterpret_cast< bool(*)>(_a[1]))); break;
        case 3: _t->wrapModeChanged((*reinterpret_cast< QQuickShaderEffectSource::WrapMode(*)>(_a[1]))); break;
        case 4: _t->nameChanged((*reinterpret_cast< const QByteArray(*)>(_a[1]))); break;
        case 5: _t->effectChanged((*reinterpret_cast< QQmlComponent*(*)>(_a[1]))); break;
        case 6: _t->smoothChanged((*reinterpret_cast< bool(*)>(_a[1]))); break;
        case 7: _t->formatChanged((*reinterpret_cast< QQuickShaderEffectSource::Format(*)>(_a[1]))); break;
        case 8: _t->sourceRectChanged((*reinterpret_cast< const QRectF(*)>(_a[1]))); break;
        case 9: _t->textureMirroringChanged((*reinterpret_cast< QQuickShaderEffectSource::TextureMirroring(*)>(_a[1]))); break;
        default: ;
        }
    } else if (_c == QMetaObject::RegisterMethodArgumentMetaType) {
        switch (_id) {
        default: *reinterpret_cast<int*>(_a[0]) = -1; break;
        case 5:
            switch (*reinterpret_cast<int*>(_a[1])) {
            default: *reinterpret_cast<int*>(_a[0]) = -1; break;
            case 0:
                *reinterpret_cast<int*>(_a[0]) = qRegisterMetaType< QQmlComponent* >(); break;
            }
            break;
        }
    } else if (_c == QMetaObject::IndexOfMethod) {
        int *result = reinterpret_cast<int *>(_a[0]);
        void **func = reinterpret_cast<void **>(_a[1]);
        {
            typedef void (QQuickItemLayer::*_t)(bool );
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&QQuickItemLayer::enabledChanged)) {
                *result = 0;
                return;
            }
        }
        {
            typedef void (QQuickItemLayer::*_t)(const QSize & );
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&QQuickItemLayer::sizeChanged)) {
                *result = 1;
                return;
            }
        }
        {
            typedef void (QQuickItemLayer::*_t)(bool );
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&QQuickItemLayer::mipmapChanged)) {
                *result = 2;
                return;
            }
        }
        {
            typedef void (QQuickItemLayer::*_t)(QQuickShaderEffectSource::WrapMode );
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&QQuickItemLayer::wrapModeChanged)) {
                *result = 3;
                return;
            }
        }
        {
            typedef void (QQuickItemLayer::*_t)(const QByteArray & );
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&QQuickItemLayer::nameChanged)) {
                *result = 4;
                return;
            }
        }
        {
            typedef void (QQuickItemLayer::*_t)(QQmlComponent * );
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&QQuickItemLayer::effectChanged)) {
                *result = 5;
                return;
            }
        }
        {
            typedef void (QQuickItemLayer::*_t)(bool );
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&QQuickItemLayer::smoothChanged)) {
                *result = 6;
                return;
            }
        }
        {
            typedef void (QQuickItemLayer::*_t)(QQuickShaderEffectSource::Format );
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&QQuickItemLayer::formatChanged)) {
                *result = 7;
                return;
            }
        }
        {
            typedef void (QQuickItemLayer::*_t)(const QRectF & );
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&QQuickItemLayer::sourceRectChanged)) {
                *result = 8;
                return;
            }
        }
        {
            typedef void (QQuickItemLayer::*_t)(QQuickShaderEffectSource::TextureMirroring );
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&QQuickItemLayer::textureMirroringChanged)) {
                *result = 9;
                return;
            }
        }
    } else if (_c == QMetaObject::RegisterPropertyMetaType) {
        switch (_id) {
        default: *reinterpret_cast<int*>(_a[0]) = -1; break;
        case 8:
            *reinterpret_cast<int*>(_a[0]) = qRegisterMetaType< QQmlComponent* >(); break;
        }
    }

#ifndef QT_NO_PROPERTIES
    else if (_c == QMetaObject::ReadProperty) {
        QQuickItemLayer *_t = static_cast<QQuickItemLayer *>(_o);
        Q_UNUSED(_t)
        void *_v = _a[0];
        switch (_id) {
        case 0: *reinterpret_cast< bool*>(_v) = _t->enabled(); break;
        case 1: *reinterpret_cast< QSize*>(_v) = _t->size(); break;
        case 2: *reinterpret_cast< QRectF*>(_v) = _t->sourceRect(); break;
        case 3: *reinterpret_cast< bool*>(_v) = _t->mipmap(); break;
        case 4: *reinterpret_cast< bool*>(_v) = _t->smooth(); break;
        case 5: *reinterpret_cast< QQuickShaderEffectSource::WrapMode*>(_v) = _t->wrapMode(); break;
        case 6: *reinterpret_cast< QQuickShaderEffectSource::Format*>(_v) = _t->format(); break;
        case 7: *reinterpret_cast< QByteArray*>(_v) = _t->name(); break;
        case 8: *reinterpret_cast< QQmlComponent**>(_v) = _t->effect(); break;
        case 9: *reinterpret_cast< QQuickShaderEffectSource::TextureMirroring*>(_v) = _t->textureMirroring(); break;
        default: break;
        }
    } else if (_c == QMetaObject::WriteProperty) {
        QQuickItemLayer *_t = static_cast<QQuickItemLayer *>(_o);
        Q_UNUSED(_t)
        void *_v = _a[0];
        switch (_id) {
        case 0: _t->setEnabled(*reinterpret_cast< bool*>(_v)); break;
        case 1: _t->setSize(*reinterpret_cast< QSize*>(_v)); break;
        case 2: _t->setSourceRect(*reinterpret_cast< QRectF*>(_v)); break;
        case 3: _t->setMipmap(*reinterpret_cast< bool*>(_v)); break;
        case 4: _t->setSmooth(*reinterpret_cast< bool*>(_v)); break;
        case 5: _t->setWrapMode(*reinterpret_cast< QQuickShaderEffectSource::WrapMode*>(_v)); break;
        case 6: _t->setFormat(*reinterpret_cast< QQuickShaderEffectSource::Format*>(_v)); break;
        case 7: _t->setName(*reinterpret_cast< QByteArray*>(_v)); break;
        case 8: _t->setEffect(*reinterpret_cast< QQmlComponent**>(_v)); break;
        case 9: _t->setTextureMirroring(*reinterpret_cast< QQuickShaderEffectSource::TextureMirroring*>(_v)); break;
        default: break;
        }
    } else if (_c == QMetaObject::ResetProperty) {
    }
#endif // QT_NO_PROPERTIES
}

static const QMetaObject * const qt_meta_extradata_QQuickItemLayer[] = {
        &QQuickShaderEffectSource::staticMetaObject,
    Q_NULLPTR
};

const QMetaObject QQuickItemLayer::staticMetaObject = {
    { &QObject::staticMetaObject, qt_meta_stringdata_QQuickItemLayer.data,
      qt_meta_data_QQuickItemLayer,  qt_static_metacall, qt_meta_extradata_QQuickItemLayer, Q_NULLPTR}
};


const QMetaObject *QQuickItemLayer::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->dynamicMetaObject() : &staticMetaObject;
}

void *QQuickItemLayer::qt_metacast(const char *_clname)
{
    if (!_clname) return Q_NULLPTR;
    if (!strcmp(_clname, qt_meta_stringdata_QQuickItemLayer.stringdata0))
        return static_cast<void*>(const_cast< QQuickItemLayer*>(this));
    if (!strcmp(_clname, "QQuickItemChangeListener"))
        return static_cast< QQuickItemChangeListener*>(const_cast< QQuickItemLayer*>(this));
    return QObject::qt_metacast(_clname);
}

int QQuickItemLayer::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QObject::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 10)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 10;
    } else if (_c == QMetaObject::RegisterMethodArgumentMetaType) {
        if (_id < 10)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 10;
    }
#ifndef QT_NO_PROPERTIES
   else if (_c == QMetaObject::ReadProperty || _c == QMetaObject::WriteProperty
            || _c == QMetaObject::ResetProperty || _c == QMetaObject::RegisterPropertyMetaType) {
        qt_static_metacall(this, _c, _id, _a);
        _id -= 10;
    } else if (_c == QMetaObject::QueryPropertyDesignable) {
        _id -= 10;
    } else if (_c == QMetaObject::QueryPropertyScriptable) {
        _id -= 10;
    } else if (_c == QMetaObject::QueryPropertyStored) {
        _id -= 10;
    } else if (_c == QMetaObject::QueryPropertyEditable) {
        _id -= 10;
    } else if (_c == QMetaObject::QueryPropertyUser) {
        _id -= 10;
    }
#endif // QT_NO_PROPERTIES
    return _id;
}

// SIGNAL 0
void QQuickItemLayer::enabledChanged(bool _t1)
{
    void *_a[] = { Q_NULLPTR, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 0, _a);
}

// SIGNAL 1
void QQuickItemLayer::sizeChanged(const QSize & _t1)
{
    void *_a[] = { Q_NULLPTR, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 1, _a);
}

// SIGNAL 2
void QQuickItemLayer::mipmapChanged(bool _t1)
{
    void *_a[] = { Q_NULLPTR, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 2, _a);
}

// SIGNAL 3
void QQuickItemLayer::wrapModeChanged(QQuickShaderEffectSource::WrapMode _t1)
{
    void *_a[] = { Q_NULLPTR, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 3, _a);
}

// SIGNAL 4
void QQuickItemLayer::nameChanged(const QByteArray & _t1)
{
    void *_a[] = { Q_NULLPTR, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 4, _a);
}

// SIGNAL 5
void QQuickItemLayer::effectChanged(QQmlComponent * _t1)
{
    void *_a[] = { Q_NULLPTR, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 5, _a);
}

// SIGNAL 6
void QQuickItemLayer::smoothChanged(bool _t1)
{
    void *_a[] = { Q_NULLPTR, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 6, _a);
}

// SIGNAL 7
void QQuickItemLayer::formatChanged(QQuickShaderEffectSource::Format _t1)
{
    void *_a[] = { Q_NULLPTR, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 7, _a);
}

// SIGNAL 8
void QQuickItemLayer::sourceRectChanged(const QRectF & _t1)
{
    void *_a[] = { Q_NULLPTR, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 8, _a);
}

// SIGNAL 9
void QQuickItemLayer::textureMirroringChanged(QQuickShaderEffectSource::TextureMirroring _t1)
{
    void *_a[] = { Q_NULLPTR, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 9, _a);
}
struct qt_meta_stringdata_QQuickKeyNavigationAttached_t {
    QByteArrayData data[20];
    char stringdata0[200];
};
#define QT_MOC_LITERAL(idx, ofs, len) \
    Q_STATIC_BYTE_ARRAY_DATA_HEADER_INITIALIZER_WITH_OFFSET(len, \
    qptrdiff(offsetof(qt_meta_stringdata_QQuickKeyNavigationAttached_t, stringdata0) + ofs \
        - idx * sizeof(QByteArrayData)) \
    )
static const qt_meta_stringdata_QQuickKeyNavigationAttached_t qt_meta_stringdata_QQuickKeyNavigationAttached = {
    {
QT_MOC_LITERAL(0, 0, 27), // "QQuickKeyNavigationAttached"
QT_MOC_LITERAL(1, 28, 11), // "leftChanged"
QT_MOC_LITERAL(2, 40, 0), // ""
QT_MOC_LITERAL(3, 41, 12), // "rightChanged"
QT_MOC_LITERAL(4, 54, 9), // "upChanged"
QT_MOC_LITERAL(5, 64, 11), // "downChanged"
QT_MOC_LITERAL(6, 76, 10), // "tabChanged"
QT_MOC_LITERAL(7, 87, 14), // "backtabChanged"
QT_MOC_LITERAL(8, 102, 15), // "priorityChanged"
QT_MOC_LITERAL(9, 118, 4), // "left"
QT_MOC_LITERAL(10, 123, 11), // "QQuickItem*"
QT_MOC_LITERAL(11, 135, 5), // "right"
QT_MOC_LITERAL(12, 141, 2), // "up"
QT_MOC_LITERAL(13, 144, 4), // "down"
QT_MOC_LITERAL(14, 149, 3), // "tab"
QT_MOC_LITERAL(15, 153, 7), // "backtab"
QT_MOC_LITERAL(16, 161, 8), // "priority"
QT_MOC_LITERAL(17, 170, 8), // "Priority"
QT_MOC_LITERAL(18, 179, 10), // "BeforeItem"
QT_MOC_LITERAL(19, 190, 9) // "AfterItem"

    },
    "QQuickKeyNavigationAttached\0leftChanged\0"
    "\0rightChanged\0upChanged\0downChanged\0"
    "tabChanged\0backtabChanged\0priorityChanged\0"
    "left\0QQuickItem*\0right\0up\0down\0tab\0"
    "backtab\0priority\0Priority\0BeforeItem\0"
    "AfterItem"
};
#undef QT_MOC_LITERAL

static const uint qt_meta_data_QQuickKeyNavigationAttached[] = {

 // content:
       7,       // revision
       0,       // classname
       0,    0, // classinfo
       7,   14, // methods
       7,   56, // properties
       1,   84, // enums/sets
       0,    0, // constructors
       0,       // flags
       7,       // signalCount

 // signals: name, argc, parameters, tag, flags
       1,    0,   49,    2, 0x06 /* Public */,
       3,    0,   50,    2, 0x06 /* Public */,
       4,    0,   51,    2, 0x06 /* Public */,
       5,    0,   52,    2, 0x06 /* Public */,
       6,    0,   53,    2, 0x06 /* Public */,
       7,    0,   54,    2, 0x06 /* Public */,
       8,    0,   55,    2, 0x06 /* Public */,

 // signals: parameters
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,

 // properties: name, type, flags
       9, 0x80000000 | 10, 0x0049510b,
      11, 0x80000000 | 10, 0x0049510b,
      12, 0x80000000 | 10, 0x0049510b,
      13, 0x80000000 | 10, 0x0049510b,
      14, 0x80000000 | 10, 0x0049510b,
      15, 0x80000000 | 10, 0x0049510b,
      16, 0x80000000 | 17, 0x0049510b,

 // properties: notify_signal_id
       0,
       1,
       2,
       3,
       4,
       5,
       6,

 // enums: name, flags, count, data
      17, 0x0,    2,   88,

 // enum data: key, value
      18, uint(QQuickKeyNavigationAttached::BeforeItem),
      19, uint(QQuickKeyNavigationAttached::AfterItem),

       0        // eod
};

void QQuickKeyNavigationAttached::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        QQuickKeyNavigationAttached *_t = static_cast<QQuickKeyNavigationAttached *>(_o);
        Q_UNUSED(_t)
        switch (_id) {
        case 0: _t->leftChanged(); break;
        case 1: _t->rightChanged(); break;
        case 2: _t->upChanged(); break;
        case 3: _t->downChanged(); break;
        case 4: _t->tabChanged(); break;
        case 5: _t->backtabChanged(); break;
        case 6: _t->priorityChanged(); break;
        default: ;
        }
    } else if (_c == QMetaObject::IndexOfMethod) {
        int *result = reinterpret_cast<int *>(_a[0]);
        void **func = reinterpret_cast<void **>(_a[1]);
        {
            typedef void (QQuickKeyNavigationAttached::*_t)();
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&QQuickKeyNavigationAttached::leftChanged)) {
                *result = 0;
                return;
            }
        }
        {
            typedef void (QQuickKeyNavigationAttached::*_t)();
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&QQuickKeyNavigationAttached::rightChanged)) {
                *result = 1;
                return;
            }
        }
        {
            typedef void (QQuickKeyNavigationAttached::*_t)();
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&QQuickKeyNavigationAttached::upChanged)) {
                *result = 2;
                return;
            }
        }
        {
            typedef void (QQuickKeyNavigationAttached::*_t)();
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&QQuickKeyNavigationAttached::downChanged)) {
                *result = 3;
                return;
            }
        }
        {
            typedef void (QQuickKeyNavigationAttached::*_t)();
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&QQuickKeyNavigationAttached::tabChanged)) {
                *result = 4;
                return;
            }
        }
        {
            typedef void (QQuickKeyNavigationAttached::*_t)();
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&QQuickKeyNavigationAttached::backtabChanged)) {
                *result = 5;
                return;
            }
        }
        {
            typedef void (QQuickKeyNavigationAttached::*_t)();
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&QQuickKeyNavigationAttached::priorityChanged)) {
                *result = 6;
                return;
            }
        }
    } else if (_c == QMetaObject::RegisterPropertyMetaType) {
        switch (_id) {
        default: *reinterpret_cast<int*>(_a[0]) = -1; break;
        case 5:
        case 4:
        case 3:
        case 2:
        case 1:
        case 0:
            *reinterpret_cast<int*>(_a[0]) = qRegisterMetaType< QQuickItem* >(); break;
        }
    }

#ifndef QT_NO_PROPERTIES
    else if (_c == QMetaObject::ReadProperty) {
        QQuickKeyNavigationAttached *_t = static_cast<QQuickKeyNavigationAttached *>(_o);
        Q_UNUSED(_t)
        void *_v = _a[0];
        switch (_id) {
        case 0: *reinterpret_cast< QQuickItem**>(_v) = _t->left(); break;
        case 1: *reinterpret_cast< QQuickItem**>(_v) = _t->right(); break;
        case 2: *reinterpret_cast< QQuickItem**>(_v) = _t->up(); break;
        case 3: *reinterpret_cast< QQuickItem**>(_v) = _t->down(); break;
        case 4: *reinterpret_cast< QQuickItem**>(_v) = _t->tab(); break;
        case 5: *reinterpret_cast< QQuickItem**>(_v) = _t->backtab(); break;
        case 6: *reinterpret_cast< Priority*>(_v) = _t->priority(); break;
        default: break;
        }
    } else if (_c == QMetaObject::WriteProperty) {
        QQuickKeyNavigationAttached *_t = static_cast<QQuickKeyNavigationAttached *>(_o);
        Q_UNUSED(_t)
        void *_v = _a[0];
        switch (_id) {
        case 0: _t->setLeft(*reinterpret_cast< QQuickItem**>(_v)); break;
        case 1: _t->setRight(*reinterpret_cast< QQuickItem**>(_v)); break;
        case 2: _t->setUp(*reinterpret_cast< QQuickItem**>(_v)); break;
        case 3: _t->setDown(*reinterpret_cast< QQuickItem**>(_v)); break;
        case 4: _t->setTab(*reinterpret_cast< QQuickItem**>(_v)); break;
        case 5: _t->setBacktab(*reinterpret_cast< QQuickItem**>(_v)); break;
        case 6: _t->setPriority(*reinterpret_cast< Priority*>(_v)); break;
        default: break;
        }
    } else if (_c == QMetaObject::ResetProperty) {
    }
#endif // QT_NO_PROPERTIES
}

const QMetaObject QQuickKeyNavigationAttached::staticMetaObject = {
    { &QObject::staticMetaObject, qt_meta_stringdata_QQuickKeyNavigationAttached.data,
      qt_meta_data_QQuickKeyNavigationAttached,  qt_static_metacall, Q_NULLPTR, Q_NULLPTR}
};


const QMetaObject *QQuickKeyNavigationAttached::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->dynamicMetaObject() : &staticMetaObject;
}

void *QQuickKeyNavigationAttached::qt_metacast(const char *_clname)
{
    if (!_clname) return Q_NULLPTR;
    if (!strcmp(_clname, qt_meta_stringdata_QQuickKeyNavigationAttached.stringdata0))
        return static_cast<void*>(const_cast< QQuickKeyNavigationAttached*>(this));
    if (!strcmp(_clname, "QQuickItemKeyFilter"))
        return static_cast< QQuickItemKeyFilter*>(const_cast< QQuickKeyNavigationAttached*>(this));
    return QObject::qt_metacast(_clname);
}

int QQuickKeyNavigationAttached::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QObject::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 7)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 7;
    } else if (_c == QMetaObject::RegisterMethodArgumentMetaType) {
        if (_id < 7)
            *reinterpret_cast<int*>(_a[0]) = -1;
        _id -= 7;
    }
#ifndef QT_NO_PROPERTIES
   else if (_c == QMetaObject::ReadProperty || _c == QMetaObject::WriteProperty
            || _c == QMetaObject::ResetProperty || _c == QMetaObject::RegisterPropertyMetaType) {
        qt_static_metacall(this, _c, _id, _a);
        _id -= 7;
    } else if (_c == QMetaObject::QueryPropertyDesignable) {
        _id -= 7;
    } else if (_c == QMetaObject::QueryPropertyScriptable) {
        _id -= 7;
    } else if (_c == QMetaObject::QueryPropertyStored) {
        _id -= 7;
    } else if (_c == QMetaObject::QueryPropertyEditable) {
        _id -= 7;
    } else if (_c == QMetaObject::QueryPropertyUser) {
        _id -= 7;
    }
#endif // QT_NO_PROPERTIES
    return _id;
}

// SIGNAL 0
void QQuickKeyNavigationAttached::leftChanged()
{
    QMetaObject::activate(this, &staticMetaObject, 0, Q_NULLPTR);
}

// SIGNAL 1
void QQuickKeyNavigationAttached::rightChanged()
{
    QMetaObject::activate(this, &staticMetaObject, 1, Q_NULLPTR);
}

// SIGNAL 2
void QQuickKeyNavigationAttached::upChanged()
{
    QMetaObject::activate(this, &staticMetaObject, 2, Q_NULLPTR);
}

// SIGNAL 3
void QQuickKeyNavigationAttached::downChanged()
{
    QMetaObject::activate(this, &staticMetaObject, 3, Q_NULLPTR);
}

// SIGNAL 4
void QQuickKeyNavigationAttached::tabChanged()
{
    QMetaObject::activate(this, &staticMetaObject, 4, Q_NULLPTR);
}

// SIGNAL 5
void QQuickKeyNavigationAttached::backtabChanged()
{
    QMetaObject::activate(this, &staticMetaObject, 5, Q_NULLPTR);
}

// SIGNAL 6
void QQuickKeyNavigationAttached::priorityChanged()
{
    QMetaObject::activate(this, &staticMetaObject, 6, Q_NULLPTR);
}
struct qt_meta_stringdata_QQuickLayoutMirroringAttached_t {
    QByteArrayData data[6];
    char stringdata0[93];
};
#define QT_MOC_LITERAL(idx, ofs, len) \
    Q_STATIC_BYTE_ARRAY_DATA_HEADER_INITIALIZER_WITH_OFFSET(len, \
    qptrdiff(offsetof(qt_meta_stringdata_QQuickLayoutMirroringAttached_t, stringdata0) + ofs \
        - idx * sizeof(QByteArrayData)) \
    )
static const qt_meta_stringdata_QQuickLayoutMirroringAttached_t qt_meta_stringdata_QQuickLayoutMirroringAttached = {
    {
QT_MOC_LITERAL(0, 0, 29), // "QQuickLayoutMirroringAttached"
QT_MOC_LITERAL(1, 30, 14), // "enabledChanged"
QT_MOC_LITERAL(2, 45, 0), // ""
QT_MOC_LITERAL(3, 46, 22), // "childrenInheritChanged"
QT_MOC_LITERAL(4, 69, 7), // "enabled"
QT_MOC_LITERAL(5, 77, 15) // "childrenInherit"

    },
    "QQuickLayoutMirroringAttached\0"
    "enabledChanged\0\0childrenInheritChanged\0"
    "enabled\0childrenInherit"
};
#undef QT_MOC_LITERAL

static const uint qt_meta_data_QQuickLayoutMirroringAttached[] = {

 // content:
       7,       // revision
       0,       // classname
       0,    0, // classinfo
       2,   14, // methods
       2,   26, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       2,       // signalCount

 // signals: name, argc, parameters, tag, flags
       1,    0,   24,    2, 0x06 /* Public */,
       3,    0,   25,    2, 0x06 /* Public */,

 // signals: parameters
    QMetaType::Void,
    QMetaType::Void,

 // properties: name, type, flags
       4, QMetaType::Bool, 0x00495107,
       5, QMetaType::Bool, 0x00495103,

 // properties: notify_signal_id
       0,
       1,

       0        // eod
};

void QQuickLayoutMirroringAttached::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        QQuickLayoutMirroringAttached *_t = static_cast<QQuickLayoutMirroringAttached *>(_o);
        Q_UNUSED(_t)
        switch (_id) {
        case 0: _t->enabledChanged(); break;
        case 1: _t->childrenInheritChanged(); break;
        default: ;
        }
    } else if (_c == QMetaObject::IndexOfMethod) {
        int *result = reinterpret_cast<int *>(_a[0]);
        void **func = reinterpret_cast<void **>(_a[1]);
        {
            typedef void (QQuickLayoutMirroringAttached::*_t)();
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&QQuickLayoutMirroringAttached::enabledChanged)) {
                *result = 0;
                return;
            }
        }
        {
            typedef void (QQuickLayoutMirroringAttached::*_t)();
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&QQuickLayoutMirroringAttached::childrenInheritChanged)) {
                *result = 1;
                return;
            }
        }
    }
#ifndef QT_NO_PROPERTIES
    else if (_c == QMetaObject::ReadProperty) {
        QQuickLayoutMirroringAttached *_t = static_cast<QQuickLayoutMirroringAttached *>(_o);
        Q_UNUSED(_t)
        void *_v = _a[0];
        switch (_id) {
        case 0: *reinterpret_cast< bool*>(_v) = _t->enabled(); break;
        case 1: *reinterpret_cast< bool*>(_v) = _t->childrenInherit(); break;
        default: break;
        }
    } else if (_c == QMetaObject::WriteProperty) {
        QQuickLayoutMirroringAttached *_t = static_cast<QQuickLayoutMirroringAttached *>(_o);
        Q_UNUSED(_t)
        void *_v = _a[0];
        switch (_id) {
        case 0: _t->setEnabled(*reinterpret_cast< bool*>(_v)); break;
        case 1: _t->setChildrenInherit(*reinterpret_cast< bool*>(_v)); break;
        default: break;
        }
    } else if (_c == QMetaObject::ResetProperty) {
        QQuickLayoutMirroringAttached *_t = static_cast<QQuickLayoutMirroringAttached *>(_o);
        Q_UNUSED(_t)
        switch (_id) {
        case 0: _t->resetEnabled(); break;
        default: break;
        }
    }
#endif // QT_NO_PROPERTIES
    Q_UNUSED(_a);
}

const QMetaObject QQuickLayoutMirroringAttached::staticMetaObject = {
    { &QObject::staticMetaObject, qt_meta_stringdata_QQuickLayoutMirroringAttached.data,
      qt_meta_data_QQuickLayoutMirroringAttached,  qt_static_metacall, Q_NULLPTR, Q_NULLPTR}
};


const QMetaObject *QQuickLayoutMirroringAttached::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->dynamicMetaObject() : &staticMetaObject;
}

void *QQuickLayoutMirroringAttached::qt_metacast(const char *_clname)
{
    if (!_clname) return Q_NULLPTR;
    if (!strcmp(_clname, qt_meta_stringdata_QQuickLayoutMirroringAttached.stringdata0))
        return static_cast<void*>(const_cast< QQuickLayoutMirroringAttached*>(this));
    return QObject::qt_metacast(_clname);
}

int QQuickLayoutMirroringAttached::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QObject::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 2)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 2;
    } else if (_c == QMetaObject::RegisterMethodArgumentMetaType) {
        if (_id < 2)
            *reinterpret_cast<int*>(_a[0]) = -1;
        _id -= 2;
    }
#ifndef QT_NO_PROPERTIES
   else if (_c == QMetaObject::ReadProperty || _c == QMetaObject::WriteProperty
            || _c == QMetaObject::ResetProperty || _c == QMetaObject::RegisterPropertyMetaType) {
        qt_static_metacall(this, _c, _id, _a);
        _id -= 2;
    } else if (_c == QMetaObject::QueryPropertyDesignable) {
        _id -= 2;
    } else if (_c == QMetaObject::QueryPropertyScriptable) {
        _id -= 2;
    } else if (_c == QMetaObject::QueryPropertyStored) {
        _id -= 2;
    } else if (_c == QMetaObject::QueryPropertyEditable) {
        _id -= 2;
    } else if (_c == QMetaObject::QueryPropertyUser) {
        _id -= 2;
    }
#endif // QT_NO_PROPERTIES
    return _id;
}

// SIGNAL 0
void QQuickLayoutMirroringAttached::enabledChanged()
{
    QMetaObject::activate(this, &staticMetaObject, 0, Q_NULLPTR);
}

// SIGNAL 1
void QQuickLayoutMirroringAttached::childrenInheritChanged()
{
    QMetaObject::activate(this, &staticMetaObject, 1, Q_NULLPTR);
}
struct qt_meta_stringdata_QQuickEnterKeyAttached_t {
    QByteArrayData data[5];
    char stringdata0[58];
};
#define QT_MOC_LITERAL(idx, ofs, len) \
    Q_STATIC_BYTE_ARRAY_DATA_HEADER_INITIALIZER_WITH_OFFSET(len, \
    qptrdiff(offsetof(qt_meta_stringdata_QQuickEnterKeyAttached_t, stringdata0) + ofs \
        - idx * sizeof(QByteArrayData)) \
    )
static const qt_meta_stringdata_QQuickEnterKeyAttached_t qt_meta_stringdata_QQuickEnterKeyAttached = {
    {
QT_MOC_LITERAL(0, 0, 22), // "QQuickEnterKeyAttached"
QT_MOC_LITERAL(1, 23, 11), // "typeChanged"
QT_MOC_LITERAL(2, 35, 0), // ""
QT_MOC_LITERAL(3, 36, 4), // "type"
QT_MOC_LITERAL(4, 41, 16) // "Qt::EnterKeyType"

    },
    "QQuickEnterKeyAttached\0typeChanged\0\0"
    "type\0Qt::EnterKeyType"
};
#undef QT_MOC_LITERAL

static const uint qt_meta_data_QQuickEnterKeyAttached[] = {

 // content:
       7,       // revision
       0,       // classname
       0,    0, // classinfo
       1,   14, // methods
       1,   20, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       1,       // signalCount

 // signals: name, argc, parameters, tag, flags
       1,    0,   19,    2, 0x06 /* Public */,

 // signals: parameters
    QMetaType::Void,

 // properties: name, type, flags
       3, 0x80000000 | 4, 0x0049510b,

 // properties: notify_signal_id
       0,

       0        // eod
};

void QQuickEnterKeyAttached::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        QQuickEnterKeyAttached *_t = static_cast<QQuickEnterKeyAttached *>(_o);
        Q_UNUSED(_t)
        switch (_id) {
        case 0: _t->typeChanged(); break;
        default: ;
        }
    } else if (_c == QMetaObject::IndexOfMethod) {
        int *result = reinterpret_cast<int *>(_a[0]);
        void **func = reinterpret_cast<void **>(_a[1]);
        {
            typedef void (QQuickEnterKeyAttached::*_t)();
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&QQuickEnterKeyAttached::typeChanged)) {
                *result = 0;
                return;
            }
        }
    }
#ifndef QT_NO_PROPERTIES
    else if (_c == QMetaObject::ReadProperty) {
        QQuickEnterKeyAttached *_t = static_cast<QQuickEnterKeyAttached *>(_o);
        Q_UNUSED(_t)
        void *_v = _a[0];
        switch (_id) {
        case 0: *reinterpret_cast< Qt::EnterKeyType*>(_v) = _t->type(); break;
        default: break;
        }
    } else if (_c == QMetaObject::WriteProperty) {
        QQuickEnterKeyAttached *_t = static_cast<QQuickEnterKeyAttached *>(_o);
        Q_UNUSED(_t)
        void *_v = _a[0];
        switch (_id) {
        case 0: _t->setType(*reinterpret_cast< Qt::EnterKeyType*>(_v)); break;
        default: break;
        }
    } else if (_c == QMetaObject::ResetProperty) {
    }
#endif // QT_NO_PROPERTIES
    Q_UNUSED(_a);
}

const QMetaObject QQuickEnterKeyAttached::staticMetaObject = {
    { &QObject::staticMetaObject, qt_meta_stringdata_QQuickEnterKeyAttached.data,
      qt_meta_data_QQuickEnterKeyAttached,  qt_static_metacall, Q_NULLPTR, Q_NULLPTR}
};


const QMetaObject *QQuickEnterKeyAttached::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->dynamicMetaObject() : &staticMetaObject;
}

void *QQuickEnterKeyAttached::qt_metacast(const char *_clname)
{
    if (!_clname) return Q_NULLPTR;
    if (!strcmp(_clname, qt_meta_stringdata_QQuickEnterKeyAttached.stringdata0))
        return static_cast<void*>(const_cast< QQuickEnterKeyAttached*>(this));
    return QObject::qt_metacast(_clname);
}

int QQuickEnterKeyAttached::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QObject::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 1)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 1;
    } else if (_c == QMetaObject::RegisterMethodArgumentMetaType) {
        if (_id < 1)
            *reinterpret_cast<int*>(_a[0]) = -1;
        _id -= 1;
    }
#ifndef QT_NO_PROPERTIES
   else if (_c == QMetaObject::ReadProperty || _c == QMetaObject::WriteProperty
            || _c == QMetaObject::ResetProperty || _c == QMetaObject::RegisterPropertyMetaType) {
        qt_static_metacall(this, _c, _id, _a);
        _id -= 1;
    } else if (_c == QMetaObject::QueryPropertyDesignable) {
        _id -= 1;
    } else if (_c == QMetaObject::QueryPropertyScriptable) {
        _id -= 1;
    } else if (_c == QMetaObject::QueryPropertyStored) {
        _id -= 1;
    } else if (_c == QMetaObject::QueryPropertyEditable) {
        _id -= 1;
    } else if (_c == QMetaObject::QueryPropertyUser) {
        _id -= 1;
    }
#endif // QT_NO_PROPERTIES
    return _id;
}

// SIGNAL 0
void QQuickEnterKeyAttached::typeChanged()
{
    QMetaObject::activate(this, &staticMetaObject, 0, Q_NULLPTR);
}
struct qt_meta_stringdata_QQuickKeysAttached_t {
    QByteArrayData data[53];
    char stringdata0[700];
};
#define QT_MOC_LITERAL(idx, ofs, len) \
    Q_STATIC_BYTE_ARRAY_DATA_HEADER_INITIALIZER_WITH_OFFSET(len, \
    qptrdiff(offsetof(qt_meta_stringdata_QQuickKeysAttached_t, stringdata0) + ofs \
        - idx * sizeof(QByteArrayData)) \
    )
static const qt_meta_stringdata_QQuickKeysAttached_t qt_meta_stringdata_QQuickKeysAttached = {
    {
QT_MOC_LITERAL(0, 0, 18), // "QQuickKeysAttached"
QT_MOC_LITERAL(1, 19, 14), // "enabledChanged"
QT_MOC_LITERAL(2, 34, 0), // ""
QT_MOC_LITERAL(3, 35, 15), // "priorityChanged"
QT_MOC_LITERAL(4, 51, 7), // "pressed"
QT_MOC_LITERAL(5, 59, 15), // "QQuickKeyEvent*"
QT_MOC_LITERAL(6, 75, 5), // "event"
QT_MOC_LITERAL(7, 81, 8), // "released"
QT_MOC_LITERAL(8, 90, 13), // "digit0Pressed"
QT_MOC_LITERAL(9, 104, 13), // "digit1Pressed"
QT_MOC_LITERAL(10, 118, 13), // "digit2Pressed"
QT_MOC_LITERAL(11, 132, 13), // "digit3Pressed"
QT_MOC_LITERAL(12, 146, 13), // "digit4Pressed"
QT_MOC_LITERAL(13, 160, 13), // "digit5Pressed"
QT_MOC_LITERAL(14, 174, 13), // "digit6Pressed"
QT_MOC_LITERAL(15, 188, 13), // "digit7Pressed"
QT_MOC_LITERAL(16, 202, 13), // "digit8Pressed"
QT_MOC_LITERAL(17, 216, 13), // "digit9Pressed"
QT_MOC_LITERAL(18, 230, 11), // "leftPressed"
QT_MOC_LITERAL(19, 242, 12), // "rightPressed"
QT_MOC_LITERAL(20, 255, 9), // "upPressed"
QT_MOC_LITERAL(21, 265, 11), // "downPressed"
QT_MOC_LITERAL(22, 277, 10), // "tabPressed"
QT_MOC_LITERAL(23, 288, 14), // "backtabPressed"
QT_MOC_LITERAL(24, 303, 15), // "asteriskPressed"
QT_MOC_LITERAL(25, 319, 17), // "numberSignPressed"
QT_MOC_LITERAL(26, 337, 13), // "escapePressed"
QT_MOC_LITERAL(27, 351, 13), // "returnPressed"
QT_MOC_LITERAL(28, 365, 12), // "enterPressed"
QT_MOC_LITERAL(29, 378, 13), // "deletePressed"
QT_MOC_LITERAL(30, 392, 12), // "spacePressed"
QT_MOC_LITERAL(31, 405, 11), // "backPressed"
QT_MOC_LITERAL(32, 417, 13), // "cancelPressed"
QT_MOC_LITERAL(33, 431, 13), // "selectPressed"
QT_MOC_LITERAL(34, 445, 10), // "yesPressed"
QT_MOC_LITERAL(35, 456, 9), // "noPressed"
QT_MOC_LITERAL(36, 466, 15), // "context1Pressed"
QT_MOC_LITERAL(37, 482, 15), // "context2Pressed"
QT_MOC_LITERAL(38, 498, 15), // "context3Pressed"
QT_MOC_LITERAL(39, 514, 15), // "context4Pressed"
QT_MOC_LITERAL(40, 530, 11), // "callPressed"
QT_MOC_LITERAL(41, 542, 13), // "hangupPressed"
QT_MOC_LITERAL(42, 556, 11), // "flipPressed"
QT_MOC_LITERAL(43, 568, 11), // "menuPressed"
QT_MOC_LITERAL(44, 580, 15), // "volumeUpPressed"
QT_MOC_LITERAL(45, 596, 17), // "volumeDownPressed"
QT_MOC_LITERAL(46, 614, 7), // "enabled"
QT_MOC_LITERAL(47, 622, 9), // "forwardTo"
QT_MOC_LITERAL(48, 632, 28), // "QQmlListProperty<QQuickItem>"
QT_MOC_LITERAL(49, 661, 8), // "priority"
QT_MOC_LITERAL(50, 670, 8), // "Priority"
QT_MOC_LITERAL(51, 679, 10), // "BeforeItem"
QT_MOC_LITERAL(52, 690, 9) // "AfterItem"

    },
    "QQuickKeysAttached\0enabledChanged\0\0"
    "priorityChanged\0pressed\0QQuickKeyEvent*\0"
    "event\0released\0digit0Pressed\0digit1Pressed\0"
    "digit2Pressed\0digit3Pressed\0digit4Pressed\0"
    "digit5Pressed\0digit6Pressed\0digit7Pressed\0"
    "digit8Pressed\0digit9Pressed\0leftPressed\0"
    "rightPressed\0upPressed\0downPressed\0"
    "tabPressed\0backtabPressed\0asteriskPressed\0"
    "numberSignPressed\0escapePressed\0"
    "returnPressed\0enterPressed\0deletePressed\0"
    "spacePressed\0backPressed\0cancelPressed\0"
    "selectPressed\0yesPressed\0noPressed\0"
    "context1Pressed\0context2Pressed\0"
    "context3Pressed\0context4Pressed\0"
    "callPressed\0hangupPressed\0flipPressed\0"
    "menuPressed\0volumeUpPressed\0"
    "volumeDownPressed\0enabled\0forwardTo\0"
    "QQmlListProperty<QQuickItem>\0priority\0"
    "Priority\0BeforeItem\0AfterItem"
};
#undef QT_MOC_LITERAL

static const uint qt_meta_data_QQuickKeysAttached[] = {

 // content:
       7,       // revision
       0,       // classname
       0,    0, // classinfo
      42,   14, // methods
       3,  346, // properties
       1,  358, // enums/sets
       0,    0, // constructors
       0,       // flags
      42,       // signalCount

 // signals: name, argc, parameters, tag, flags
       1,    0,  224,    2, 0x06 /* Public */,
       3,    0,  225,    2, 0x06 /* Public */,
       4,    1,  226,    2, 0x06 /* Public */,
       7,    1,  229,    2, 0x06 /* Public */,
       8,    1,  232,    2, 0x06 /* Public */,
       9,    1,  235,    2, 0x06 /* Public */,
      10,    1,  238,    2, 0x06 /* Public */,
      11,    1,  241,    2, 0x06 /* Public */,
      12,    1,  244,    2, 0x06 /* Public */,
      13,    1,  247,    2, 0x06 /* Public */,
      14,    1,  250,    2, 0x06 /* Public */,
      15,    1,  253,    2, 0x06 /* Public */,
      16,    1,  256,    2, 0x06 /* Public */,
      17,    1,  259,    2, 0x06 /* Public */,
      18,    1,  262,    2, 0x06 /* Public */,
      19,    1,  265,    2, 0x06 /* Public */,
      20,    1,  268,    2, 0x06 /* Public */,
      21,    1,  271,    2, 0x06 /* Public */,
      22,    1,  274,    2, 0x06 /* Public */,
      23,    1,  277,    2, 0x06 /* Public */,
      24,    1,  280,    2, 0x06 /* Public */,
      25,    1,  283,    2, 0x06 /* Public */,
      26,    1,  286,    2, 0x06 /* Public */,
      27,    1,  289,    2, 0x06 /* Public */,
      28,    1,  292,    2, 0x06 /* Public */,
      29,    1,  295,    2, 0x06 /* Public */,
      30,    1,  298,    2, 0x06 /* Public */,
      31,    1,  301,    2, 0x06 /* Public */,
      32,    1,  304,    2, 0x06 /* Public */,
      33,    1,  307,    2, 0x06 /* Public */,
      34,    1,  310,    2, 0x06 /* Public */,
      35,    1,  313,    2, 0x06 /* Public */,
      36,    1,  316,    2, 0x06 /* Public */,
      37,    1,  319,    2, 0x06 /* Public */,
      38,    1,  322,    2, 0x06 /* Public */,
      39,    1,  325,    2, 0x06 /* Public */,
      40,    1,  328,    2, 0x06 /* Public */,
      41,    1,  331,    2, 0x06 /* Public */,
      42,    1,  334,    2, 0x06 /* Public */,
      43,    1,  337,    2, 0x06 /* Public */,
      44,    1,  340,    2, 0x06 /* Public */,
      45,    1,  343,    2, 0x06 /* Public */,

 // signals: parameters
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void, 0x80000000 | 5,    6,
    QMetaType::Void, 0x80000000 | 5,    6,
    QMetaType::Void, 0x80000000 | 5,    6,
    QMetaType::Void, 0x80000000 | 5,    6,
    QMetaType::Void, 0x80000000 | 5,    6,
    QMetaType::Void, 0x80000000 | 5,    6,
    QMetaType::Void, 0x80000000 | 5,    6,
    QMetaType::Void, 0x80000000 | 5,    6,
    QMetaType::Void, 0x80000000 | 5,    6,
    QMetaType::Void, 0x80000000 | 5,    6,
    QMetaType::Void, 0x80000000 | 5,    6,
    QMetaType::Void, 0x80000000 | 5,    6,
    QMetaType::Void, 0x80000000 | 5,    6,
    QMetaType::Void, 0x80000000 | 5,    6,
    QMetaType::Void, 0x80000000 | 5,    6,
    QMetaType::Void, 0x80000000 | 5,    6,
    QMetaType::Void, 0x80000000 | 5,    6,
    QMetaType::Void, 0x80000000 | 5,    6,
    QMetaType::Void, 0x80000000 | 5,    6,
    QMetaType::Void, 0x80000000 | 5,    6,
    QMetaType::Void, 0x80000000 | 5,    6,
    QMetaType::Void, 0x80000000 | 5,    6,
    QMetaType::Void, 0x80000000 | 5,    6,
    QMetaType::Void, 0x80000000 | 5,    6,
    QMetaType::Void, 0x80000000 | 5,    6,
    QMetaType::Void, 0x80000000 | 5,    6,
    QMetaType::Void, 0x80000000 | 5,    6,
    QMetaType::Void, 0x80000000 | 5,    6,
    QMetaType::Void, 0x80000000 | 5,    6,
    QMetaType::Void, 0x80000000 | 5,    6,
    QMetaType::Void, 0x80000000 | 5,    6,
    QMetaType::Void, 0x80000000 | 5,    6,
    QMetaType::Void, 0x80000000 | 5,    6,
    QMetaType::Void, 0x80000000 | 5,    6,
    QMetaType::Void, 0x80000000 | 5,    6,
    QMetaType::Void, 0x80000000 | 5,    6,
    QMetaType::Void, 0x80000000 | 5,    6,
    QMetaType::Void, 0x80000000 | 5,    6,
    QMetaType::Void, 0x80000000 | 5,    6,
    QMetaType::Void, 0x80000000 | 5,    6,

 // properties: name, type, flags
      46, QMetaType::Bool, 0x00495103,
      47, 0x80000000 | 48, 0x00095009,
      49, 0x80000000 | 50, 0x0049510b,

 // properties: notify_signal_id
       0,
       0,
       1,

 // enums: name, flags, count, data
      50, 0x0,    2,  362,

 // enum data: key, value
      51, uint(QQuickKeysAttached::BeforeItem),
      52, uint(QQuickKeysAttached::AfterItem),

       0        // eod
};

void QQuickKeysAttached::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        QQuickKeysAttached *_t = static_cast<QQuickKeysAttached *>(_o);
        Q_UNUSED(_t)
        switch (_id) {
        case 0: _t->enabledChanged(); break;
        case 1: _t->priorityChanged(); break;
        case 2: _t->pressed((*reinterpret_cast< QQuickKeyEvent*(*)>(_a[1]))); break;
        case 3: _t->released((*reinterpret_cast< QQuickKeyEvent*(*)>(_a[1]))); break;
        case 4: _t->digit0Pressed((*reinterpret_cast< QQuickKeyEvent*(*)>(_a[1]))); break;
        case 5: _t->digit1Pressed((*reinterpret_cast< QQuickKeyEvent*(*)>(_a[1]))); break;
        case 6: _t->digit2Pressed((*reinterpret_cast< QQuickKeyEvent*(*)>(_a[1]))); break;
        case 7: _t->digit3Pressed((*reinterpret_cast< QQuickKeyEvent*(*)>(_a[1]))); break;
        case 8: _t->digit4Pressed((*reinterpret_cast< QQuickKeyEvent*(*)>(_a[1]))); break;
        case 9: _t->digit5Pressed((*reinterpret_cast< QQuickKeyEvent*(*)>(_a[1]))); break;
        case 10: _t->digit6Pressed((*reinterpret_cast< QQuickKeyEvent*(*)>(_a[1]))); break;
        case 11: _t->digit7Pressed((*reinterpret_cast< QQuickKeyEvent*(*)>(_a[1]))); break;
        case 12: _t->digit8Pressed((*reinterpret_cast< QQuickKeyEvent*(*)>(_a[1]))); break;
        case 13: _t->digit9Pressed((*reinterpret_cast< QQuickKeyEvent*(*)>(_a[1]))); break;
        case 14: _t->leftPressed((*reinterpret_cast< QQuickKeyEvent*(*)>(_a[1]))); break;
        case 15: _t->rightPressed((*reinterpret_cast< QQuickKeyEvent*(*)>(_a[1]))); break;
        case 16: _t->upPressed((*reinterpret_cast< QQuickKeyEvent*(*)>(_a[1]))); break;
        case 17: _t->downPressed((*reinterpret_cast< QQuickKeyEvent*(*)>(_a[1]))); break;
        case 18: _t->tabPressed((*reinterpret_cast< QQuickKeyEvent*(*)>(_a[1]))); break;
        case 19: _t->backtabPressed((*reinterpret_cast< QQuickKeyEvent*(*)>(_a[1]))); break;
        case 20: _t->asteriskPressed((*reinterpret_cast< QQuickKeyEvent*(*)>(_a[1]))); break;
        case 21: _t->numberSignPressed((*reinterpret_cast< QQuickKeyEvent*(*)>(_a[1]))); break;
        case 22: _t->escapePressed((*reinterpret_cast< QQuickKeyEvent*(*)>(_a[1]))); break;
        case 23: _t->returnPressed((*reinterpret_cast< QQuickKeyEvent*(*)>(_a[1]))); break;
        case 24: _t->enterPressed((*reinterpret_cast< QQuickKeyEvent*(*)>(_a[1]))); break;
        case 25: _t->deletePressed((*reinterpret_cast< QQuickKeyEvent*(*)>(_a[1]))); break;
        case 26: _t->spacePressed((*reinterpret_cast< QQuickKeyEvent*(*)>(_a[1]))); break;
        case 27: _t->backPressed((*reinterpret_cast< QQuickKeyEvent*(*)>(_a[1]))); break;
        case 28: _t->cancelPressed((*reinterpret_cast< QQuickKeyEvent*(*)>(_a[1]))); break;
        case 29: _t->selectPressed((*reinterpret_cast< QQuickKeyEvent*(*)>(_a[1]))); break;
        case 30: _t->yesPressed((*reinterpret_cast< QQuickKeyEvent*(*)>(_a[1]))); break;
        case 31: _t->noPressed((*reinterpret_cast< QQuickKeyEvent*(*)>(_a[1]))); break;
        case 32: _t->context1Pressed((*reinterpret_cast< QQuickKeyEvent*(*)>(_a[1]))); break;
        case 33: _t->context2Pressed((*reinterpret_cast< QQuickKeyEvent*(*)>(_a[1]))); break;
        case 34: _t->context3Pressed((*reinterpret_cast< QQuickKeyEvent*(*)>(_a[1]))); break;
        case 35: _t->context4Pressed((*reinterpret_cast< QQuickKeyEvent*(*)>(_a[1]))); break;
        case 36: _t->callPressed((*reinterpret_cast< QQuickKeyEvent*(*)>(_a[1]))); break;
        case 37: _t->hangupPressed((*reinterpret_cast< QQuickKeyEvent*(*)>(_a[1]))); break;
        case 38: _t->flipPressed((*reinterpret_cast< QQuickKeyEvent*(*)>(_a[1]))); break;
        case 39: _t->menuPressed((*reinterpret_cast< QQuickKeyEvent*(*)>(_a[1]))); break;
        case 40: _t->volumeUpPressed((*reinterpret_cast< QQuickKeyEvent*(*)>(_a[1]))); break;
        case 41: _t->volumeDownPressed((*reinterpret_cast< QQuickKeyEvent*(*)>(_a[1]))); break;
        default: ;
        }
    } else if (_c == QMetaObject::IndexOfMethod) {
        int *result = reinterpret_cast<int *>(_a[0]);
        void **func = reinterpret_cast<void **>(_a[1]);
        {
            typedef void (QQuickKeysAttached::*_t)();
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&QQuickKeysAttached::enabledChanged)) {
                *result = 0;
                return;
            }
        }
        {
            typedef void (QQuickKeysAttached::*_t)();
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&QQuickKeysAttached::priorityChanged)) {
                *result = 1;
                return;
            }
        }
        {
            typedef void (QQuickKeysAttached::*_t)(QQuickKeyEvent * );
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&QQuickKeysAttached::pressed)) {
                *result = 2;
                return;
            }
        }
        {
            typedef void (QQuickKeysAttached::*_t)(QQuickKeyEvent * );
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&QQuickKeysAttached::released)) {
                *result = 3;
                return;
            }
        }
        {
            typedef void (QQuickKeysAttached::*_t)(QQuickKeyEvent * );
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&QQuickKeysAttached::digit0Pressed)) {
                *result = 4;
                return;
            }
        }
        {
            typedef void (QQuickKeysAttached::*_t)(QQuickKeyEvent * );
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&QQuickKeysAttached::digit1Pressed)) {
                *result = 5;
                return;
            }
        }
        {
            typedef void (QQuickKeysAttached::*_t)(QQuickKeyEvent * );
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&QQuickKeysAttached::digit2Pressed)) {
                *result = 6;
                return;
            }
        }
        {
            typedef void (QQuickKeysAttached::*_t)(QQuickKeyEvent * );
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&QQuickKeysAttached::digit3Pressed)) {
                *result = 7;
                return;
            }
        }
        {
            typedef void (QQuickKeysAttached::*_t)(QQuickKeyEvent * );
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&QQuickKeysAttached::digit4Pressed)) {
                *result = 8;
                return;
            }
        }
        {
            typedef void (QQuickKeysAttached::*_t)(QQuickKeyEvent * );
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&QQuickKeysAttached::digit5Pressed)) {
                *result = 9;
                return;
            }
        }
        {
            typedef void (QQuickKeysAttached::*_t)(QQuickKeyEvent * );
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&QQuickKeysAttached::digit6Pressed)) {
                *result = 10;
                return;
            }
        }
        {
            typedef void (QQuickKeysAttached::*_t)(QQuickKeyEvent * );
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&QQuickKeysAttached::digit7Pressed)) {
                *result = 11;
                return;
            }
        }
        {
            typedef void (QQuickKeysAttached::*_t)(QQuickKeyEvent * );
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&QQuickKeysAttached::digit8Pressed)) {
                *result = 12;
                return;
            }
        }
        {
            typedef void (QQuickKeysAttached::*_t)(QQuickKeyEvent * );
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&QQuickKeysAttached::digit9Pressed)) {
                *result = 13;
                return;
            }
        }
        {
            typedef void (QQuickKeysAttached::*_t)(QQuickKeyEvent * );
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&QQuickKeysAttached::leftPressed)) {
                *result = 14;
                return;
            }
        }
        {
            typedef void (QQuickKeysAttached::*_t)(QQuickKeyEvent * );
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&QQuickKeysAttached::rightPressed)) {
                *result = 15;
                return;
            }
        }
        {
            typedef void (QQuickKeysAttached::*_t)(QQuickKeyEvent * );
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&QQuickKeysAttached::upPressed)) {
                *result = 16;
                return;
            }
        }
        {
            typedef void (QQuickKeysAttached::*_t)(QQuickKeyEvent * );
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&QQuickKeysAttached::downPressed)) {
                *result = 17;
                return;
            }
        }
        {
            typedef void (QQuickKeysAttached::*_t)(QQuickKeyEvent * );
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&QQuickKeysAttached::tabPressed)) {
                *result = 18;
                return;
            }
        }
        {
            typedef void (QQuickKeysAttached::*_t)(QQuickKeyEvent * );
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&QQuickKeysAttached::backtabPressed)) {
                *result = 19;
                return;
            }
        }
        {
            typedef void (QQuickKeysAttached::*_t)(QQuickKeyEvent * );
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&QQuickKeysAttached::asteriskPressed)) {
                *result = 20;
                return;
            }
        }
        {
            typedef void (QQuickKeysAttached::*_t)(QQuickKeyEvent * );
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&QQuickKeysAttached::numberSignPressed)) {
                *result = 21;
                return;
            }
        }
        {
            typedef void (QQuickKeysAttached::*_t)(QQuickKeyEvent * );
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&QQuickKeysAttached::escapePressed)) {
                *result = 22;
                return;
            }
        }
        {
            typedef void (QQuickKeysAttached::*_t)(QQuickKeyEvent * );
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&QQuickKeysAttached::returnPressed)) {
                *result = 23;
                return;
            }
        }
        {
            typedef void (QQuickKeysAttached::*_t)(QQuickKeyEvent * );
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&QQuickKeysAttached::enterPressed)) {
                *result = 24;
                return;
            }
        }
        {
            typedef void (QQuickKeysAttached::*_t)(QQuickKeyEvent * );
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&QQuickKeysAttached::deletePressed)) {
                *result = 25;
                return;
            }
        }
        {
            typedef void (QQuickKeysAttached::*_t)(QQuickKeyEvent * );
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&QQuickKeysAttached::spacePressed)) {
                *result = 26;
                return;
            }
        }
        {
            typedef void (QQuickKeysAttached::*_t)(QQuickKeyEvent * );
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&QQuickKeysAttached::backPressed)) {
                *result = 27;
                return;
            }
        }
        {
            typedef void (QQuickKeysAttached::*_t)(QQuickKeyEvent * );
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&QQuickKeysAttached::cancelPressed)) {
                *result = 28;
                return;
            }
        }
        {
            typedef void (QQuickKeysAttached::*_t)(QQuickKeyEvent * );
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&QQuickKeysAttached::selectPressed)) {
                *result = 29;
                return;
            }
        }
        {
            typedef void (QQuickKeysAttached::*_t)(QQuickKeyEvent * );
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&QQuickKeysAttached::yesPressed)) {
                *result = 30;
                return;
            }
        }
        {
            typedef void (QQuickKeysAttached::*_t)(QQuickKeyEvent * );
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&QQuickKeysAttached::noPressed)) {
                *result = 31;
                return;
            }
        }
        {
            typedef void (QQuickKeysAttached::*_t)(QQuickKeyEvent * );
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&QQuickKeysAttached::context1Pressed)) {
                *result = 32;
                return;
            }
        }
        {
            typedef void (QQuickKeysAttached::*_t)(QQuickKeyEvent * );
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&QQuickKeysAttached::context2Pressed)) {
                *result = 33;
                return;
            }
        }
        {
            typedef void (QQuickKeysAttached::*_t)(QQuickKeyEvent * );
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&QQuickKeysAttached::context3Pressed)) {
                *result = 34;
                return;
            }
        }
        {
            typedef void (QQuickKeysAttached::*_t)(QQuickKeyEvent * );
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&QQuickKeysAttached::context4Pressed)) {
                *result = 35;
                return;
            }
        }
        {
            typedef void (QQuickKeysAttached::*_t)(QQuickKeyEvent * );
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&QQuickKeysAttached::callPressed)) {
                *result = 36;
                return;
            }
        }
        {
            typedef void (QQuickKeysAttached::*_t)(QQuickKeyEvent * );
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&QQuickKeysAttached::hangupPressed)) {
                *result = 37;
                return;
            }
        }
        {
            typedef void (QQuickKeysAttached::*_t)(QQuickKeyEvent * );
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&QQuickKeysAttached::flipPressed)) {
                *result = 38;
                return;
            }
        }
        {
            typedef void (QQuickKeysAttached::*_t)(QQuickKeyEvent * );
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&QQuickKeysAttached::menuPressed)) {
                *result = 39;
                return;
            }
        }
        {
            typedef void (QQuickKeysAttached::*_t)(QQuickKeyEvent * );
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&QQuickKeysAttached::volumeUpPressed)) {
                *result = 40;
                return;
            }
        }
        {
            typedef void (QQuickKeysAttached::*_t)(QQuickKeyEvent * );
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&QQuickKeysAttached::volumeDownPressed)) {
                *result = 41;
                return;
            }
        }
    } else if (_c == QMetaObject::RegisterPropertyMetaType) {
        switch (_id) {
        default: *reinterpret_cast<int*>(_a[0]) = -1; break;
        case 1:
            *reinterpret_cast<int*>(_a[0]) = qRegisterMetaType< QQmlListProperty<QQuickItem> >(); break;
        }
    }

#ifndef QT_NO_PROPERTIES
    else if (_c == QMetaObject::ReadProperty) {
        QQuickKeysAttached *_t = static_cast<QQuickKeysAttached *>(_o);
        Q_UNUSED(_t)
        void *_v = _a[0];
        switch (_id) {
        case 0: *reinterpret_cast< bool*>(_v) = _t->enabled(); break;
        case 1: *reinterpret_cast< QQmlListProperty<QQuickItem>*>(_v) = _t->forwardTo(); break;
        case 2: *reinterpret_cast< Priority*>(_v) = _t->priority(); break;
        default: break;
        }
    } else if (_c == QMetaObject::WriteProperty) {
        QQuickKeysAttached *_t = static_cast<QQuickKeysAttached *>(_o);
        Q_UNUSED(_t)
        void *_v = _a[0];
        switch (_id) {
        case 0: _t->setEnabled(*reinterpret_cast< bool*>(_v)); break;
        case 2: _t->setPriority(*reinterpret_cast< Priority*>(_v)); break;
        default: break;
        }
    } else if (_c == QMetaObject::ResetProperty) {
    }
#endif // QT_NO_PROPERTIES
}

const QMetaObject QQuickKeysAttached::staticMetaObject = {
    { &QObject::staticMetaObject, qt_meta_stringdata_QQuickKeysAttached.data,
      qt_meta_data_QQuickKeysAttached,  qt_static_metacall, Q_NULLPTR, Q_NULLPTR}
};


const QMetaObject *QQuickKeysAttached::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->dynamicMetaObject() : &staticMetaObject;
}

void *QQuickKeysAttached::qt_metacast(const char *_clname)
{
    if (!_clname) return Q_NULLPTR;
    if (!strcmp(_clname, qt_meta_stringdata_QQuickKeysAttached.stringdata0))
        return static_cast<void*>(const_cast< QQuickKeysAttached*>(this));
    if (!strcmp(_clname, "QQuickItemKeyFilter"))
        return static_cast< QQuickItemKeyFilter*>(const_cast< QQuickKeysAttached*>(this));
    return QObject::qt_metacast(_clname);
}

int QQuickKeysAttached::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QObject::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 42)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 42;
    } else if (_c == QMetaObject::RegisterMethodArgumentMetaType) {
        if (_id < 42)
            *reinterpret_cast<int*>(_a[0]) = -1;
        _id -= 42;
    }
#ifndef QT_NO_PROPERTIES
   else if (_c == QMetaObject::ReadProperty || _c == QMetaObject::WriteProperty
            || _c == QMetaObject::ResetProperty || _c == QMetaObject::RegisterPropertyMetaType) {
        qt_static_metacall(this, _c, _id, _a);
        _id -= 3;
    } else if (_c == QMetaObject::QueryPropertyDesignable) {
        _id -= 3;
    } else if (_c == QMetaObject::QueryPropertyScriptable) {
        _id -= 3;
    } else if (_c == QMetaObject::QueryPropertyStored) {
        _id -= 3;
    } else if (_c == QMetaObject::QueryPropertyEditable) {
        _id -= 3;
    } else if (_c == QMetaObject::QueryPropertyUser) {
        _id -= 3;
    }
#endif // QT_NO_PROPERTIES
    return _id;
}

// SIGNAL 0
void QQuickKeysAttached::enabledChanged()
{
    QMetaObject::activate(this, &staticMetaObject, 0, Q_NULLPTR);
}

// SIGNAL 1
void QQuickKeysAttached::priorityChanged()
{
    QMetaObject::activate(this, &staticMetaObject, 1, Q_NULLPTR);
}

// SIGNAL 2
void QQuickKeysAttached::pressed(QQuickKeyEvent * _t1)
{
    void *_a[] = { Q_NULLPTR, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 2, _a);
}

// SIGNAL 3
void QQuickKeysAttached::released(QQuickKeyEvent * _t1)
{
    void *_a[] = { Q_NULLPTR, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 3, _a);
}

// SIGNAL 4
void QQuickKeysAttached::digit0Pressed(QQuickKeyEvent * _t1)
{
    void *_a[] = { Q_NULLPTR, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 4, _a);
}

// SIGNAL 5
void QQuickKeysAttached::digit1Pressed(QQuickKeyEvent * _t1)
{
    void *_a[] = { Q_NULLPTR, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 5, _a);
}

// SIGNAL 6
void QQuickKeysAttached::digit2Pressed(QQuickKeyEvent * _t1)
{
    void *_a[] = { Q_NULLPTR, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 6, _a);
}

// SIGNAL 7
void QQuickKeysAttached::digit3Pressed(QQuickKeyEvent * _t1)
{
    void *_a[] = { Q_NULLPTR, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 7, _a);
}

// SIGNAL 8
void QQuickKeysAttached::digit4Pressed(QQuickKeyEvent * _t1)
{
    void *_a[] = { Q_NULLPTR, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 8, _a);
}

// SIGNAL 9
void QQuickKeysAttached::digit5Pressed(QQuickKeyEvent * _t1)
{
    void *_a[] = { Q_NULLPTR, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 9, _a);
}

// SIGNAL 10
void QQuickKeysAttached::digit6Pressed(QQuickKeyEvent * _t1)
{
    void *_a[] = { Q_NULLPTR, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 10, _a);
}

// SIGNAL 11
void QQuickKeysAttached::digit7Pressed(QQuickKeyEvent * _t1)
{
    void *_a[] = { Q_NULLPTR, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 11, _a);
}

// SIGNAL 12
void QQuickKeysAttached::digit8Pressed(QQuickKeyEvent * _t1)
{
    void *_a[] = { Q_NULLPTR, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 12, _a);
}

// SIGNAL 13
void QQuickKeysAttached::digit9Pressed(QQuickKeyEvent * _t1)
{
    void *_a[] = { Q_NULLPTR, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 13, _a);
}

// SIGNAL 14
void QQuickKeysAttached::leftPressed(QQuickKeyEvent * _t1)
{
    void *_a[] = { Q_NULLPTR, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 14, _a);
}

// SIGNAL 15
void QQuickKeysAttached::rightPressed(QQuickKeyEvent * _t1)
{
    void *_a[] = { Q_NULLPTR, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 15, _a);
}

// SIGNAL 16
void QQuickKeysAttached::upPressed(QQuickKeyEvent * _t1)
{
    void *_a[] = { Q_NULLPTR, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 16, _a);
}

// SIGNAL 17
void QQuickKeysAttached::downPressed(QQuickKeyEvent * _t1)
{
    void *_a[] = { Q_NULLPTR, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 17, _a);
}

// SIGNAL 18
void QQuickKeysAttached::tabPressed(QQuickKeyEvent * _t1)
{
    void *_a[] = { Q_NULLPTR, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 18, _a);
}

// SIGNAL 19
void QQuickKeysAttached::backtabPressed(QQuickKeyEvent * _t1)
{
    void *_a[] = { Q_NULLPTR, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 19, _a);
}

// SIGNAL 20
void QQuickKeysAttached::asteriskPressed(QQuickKeyEvent * _t1)
{
    void *_a[] = { Q_NULLPTR, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 20, _a);
}

// SIGNAL 21
void QQuickKeysAttached::numberSignPressed(QQuickKeyEvent * _t1)
{
    void *_a[] = { Q_NULLPTR, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 21, _a);
}

// SIGNAL 22
void QQuickKeysAttached::escapePressed(QQuickKeyEvent * _t1)
{
    void *_a[] = { Q_NULLPTR, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 22, _a);
}

// SIGNAL 23
void QQuickKeysAttached::returnPressed(QQuickKeyEvent * _t1)
{
    void *_a[] = { Q_NULLPTR, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 23, _a);
}

// SIGNAL 24
void QQuickKeysAttached::enterPressed(QQuickKeyEvent * _t1)
{
    void *_a[] = { Q_NULLPTR, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 24, _a);
}

// SIGNAL 25
void QQuickKeysAttached::deletePressed(QQuickKeyEvent * _t1)
{
    void *_a[] = { Q_NULLPTR, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 25, _a);
}

// SIGNAL 26
void QQuickKeysAttached::spacePressed(QQuickKeyEvent * _t1)
{
    void *_a[] = { Q_NULLPTR, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 26, _a);
}

// SIGNAL 27
void QQuickKeysAttached::backPressed(QQuickKeyEvent * _t1)
{
    void *_a[] = { Q_NULLPTR, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 27, _a);
}

// SIGNAL 28
void QQuickKeysAttached::cancelPressed(QQuickKeyEvent * _t1)
{
    void *_a[] = { Q_NULLPTR, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 28, _a);
}

// SIGNAL 29
void QQuickKeysAttached::selectPressed(QQuickKeyEvent * _t1)
{
    void *_a[] = { Q_NULLPTR, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 29, _a);
}

// SIGNAL 30
void QQuickKeysAttached::yesPressed(QQuickKeyEvent * _t1)
{
    void *_a[] = { Q_NULLPTR, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 30, _a);
}

// SIGNAL 31
void QQuickKeysAttached::noPressed(QQuickKeyEvent * _t1)
{
    void *_a[] = { Q_NULLPTR, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 31, _a);
}

// SIGNAL 32
void QQuickKeysAttached::context1Pressed(QQuickKeyEvent * _t1)
{
    void *_a[] = { Q_NULLPTR, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 32, _a);
}

// SIGNAL 33
void QQuickKeysAttached::context2Pressed(QQuickKeyEvent * _t1)
{
    void *_a[] = { Q_NULLPTR, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 33, _a);
}

// SIGNAL 34
void QQuickKeysAttached::context3Pressed(QQuickKeyEvent * _t1)
{
    void *_a[] = { Q_NULLPTR, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 34, _a);
}

// SIGNAL 35
void QQuickKeysAttached::context4Pressed(QQuickKeyEvent * _t1)
{
    void *_a[] = { Q_NULLPTR, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 35, _a);
}

// SIGNAL 36
void QQuickKeysAttached::callPressed(QQuickKeyEvent * _t1)
{
    void *_a[] = { Q_NULLPTR, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 36, _a);
}

// SIGNAL 37
void QQuickKeysAttached::hangupPressed(QQuickKeyEvent * _t1)
{
    void *_a[] = { Q_NULLPTR, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 37, _a);
}

// SIGNAL 38
void QQuickKeysAttached::flipPressed(QQuickKeyEvent * _t1)
{
    void *_a[] = { Q_NULLPTR, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 38, _a);
}

// SIGNAL 39
void QQuickKeysAttached::menuPressed(QQuickKeyEvent * _t1)
{
    void *_a[] = { Q_NULLPTR, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 39, _a);
}

// SIGNAL 40
void QQuickKeysAttached::volumeUpPressed(QQuickKeyEvent * _t1)
{
    void *_a[] = { Q_NULLPTR, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 40, _a);
}

// SIGNAL 41
void QQuickKeysAttached::volumeDownPressed(QQuickKeyEvent * _t1)
{
    void *_a[] = { Q_NULLPTR, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 41, _a);
}
QT_END_MOC_NAMESPACE
