/****************************************************************************
** Meta object code from reading C++ file 'qquicktext_p.h'
**
** Created by: The Qt Meta Object Compiler version 67 (Qt 5.6.3)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "../../items/qquicktext_p.h"
#include <QtCore/qbytearray.h>
#include <QtCore/qmetatype.h>
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'qquicktext_p.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 67
#error "This file was generated using the moc from 5.6.3. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
struct qt_meta_stringdata_QQuickText_t {
    QByteArrayData data[123];
    char stringdata0[1666];
};
#define QT_MOC_LITERAL(idx, ofs, len) \
    Q_STATIC_BYTE_ARRAY_DATA_HEADER_INITIALIZER_WITH_OFFSET(len, \
    qptrdiff(offsetof(qt_meta_stringdata_QQuickText_t, stringdata0) + ofs \
        - idx * sizeof(QByteArrayData)) \
    )
static const qt_meta_stringdata_QQuickText_t qt_meta_stringdata_QQuickText = {
    {
QT_MOC_LITERAL(0, 0, 10), // "QQuickText"
QT_MOC_LITERAL(1, 11, 11), // "textChanged"
QT_MOC_LITERAL(2, 23, 0), // ""
QT_MOC_LITERAL(3, 24, 4), // "text"
QT_MOC_LITERAL(4, 29, 13), // "linkActivated"
QT_MOC_LITERAL(5, 43, 4), // "link"
QT_MOC_LITERAL(6, 48, 11), // "linkHovered"
QT_MOC_LITERAL(7, 60, 11), // "fontChanged"
QT_MOC_LITERAL(8, 72, 4), // "font"
QT_MOC_LITERAL(9, 77, 12), // "colorChanged"
QT_MOC_LITERAL(10, 90, 16), // "linkColorChanged"
QT_MOC_LITERAL(11, 107, 12), // "styleChanged"
QT_MOC_LITERAL(12, 120, 21), // "QQuickText::TextStyle"
QT_MOC_LITERAL(13, 142, 5), // "style"
QT_MOC_LITERAL(14, 148, 17), // "styleColorChanged"
QT_MOC_LITERAL(15, 166, 26), // "horizontalAlignmentChanged"
QT_MOC_LITERAL(16, 193, 22), // "QQuickText::HAlignment"
QT_MOC_LITERAL(17, 216, 9), // "alignment"
QT_MOC_LITERAL(18, 226, 24), // "verticalAlignmentChanged"
QT_MOC_LITERAL(19, 251, 22), // "QQuickText::VAlignment"
QT_MOC_LITERAL(20, 274, 15), // "wrapModeChanged"
QT_MOC_LITERAL(21, 290, 16), // "lineCountChanged"
QT_MOC_LITERAL(22, 307, 16), // "truncatedChanged"
QT_MOC_LITERAL(23, 324, 23), // "maximumLineCountChanged"
QT_MOC_LITERAL(24, 348, 17), // "textFormatChanged"
QT_MOC_LITERAL(25, 366, 22), // "QQuickText::TextFormat"
QT_MOC_LITERAL(26, 389, 10), // "textFormat"
QT_MOC_LITERAL(27, 400, 16), // "elideModeChanged"
QT_MOC_LITERAL(28, 417, 25), // "QQuickText::TextElideMode"
QT_MOC_LITERAL(29, 443, 4), // "mode"
QT_MOC_LITERAL(30, 448, 18), // "contentSizeChanged"
QT_MOC_LITERAL(31, 467, 17), // "lineHeightChanged"
QT_MOC_LITERAL(32, 485, 10), // "lineHeight"
QT_MOC_LITERAL(33, 496, 21), // "lineHeightModeChanged"
QT_MOC_LITERAL(34, 518, 14), // "LineHeightMode"
QT_MOC_LITERAL(35, 533, 19), // "fontSizeModeChanged"
QT_MOC_LITERAL(36, 553, 23), // "minimumPixelSizeChanged"
QT_MOC_LITERAL(37, 577, 23), // "minimumPointSizeChanged"
QT_MOC_LITERAL(38, 601, 35), // "effectiveHorizontalAlignmentC..."
QT_MOC_LITERAL(39, 637, 11), // "lineLaidOut"
QT_MOC_LITERAL(40, 649, 15), // "QQuickTextLine*"
QT_MOC_LITERAL(41, 665, 4), // "line"
QT_MOC_LITERAL(42, 670, 14), // "baseUrlChanged"
QT_MOC_LITERAL(43, 685, 17), // "renderTypeChanged"
QT_MOC_LITERAL(44, 703, 14), // "paddingChanged"
QT_MOC_LITERAL(45, 718, 17), // "topPaddingChanged"
QT_MOC_LITERAL(46, 736, 18), // "leftPaddingChanged"
QT_MOC_LITERAL(47, 755, 19), // "rightPaddingChanged"
QT_MOC_LITERAL(48, 775, 20), // "bottomPaddingChanged"
QT_MOC_LITERAL(49, 796, 14), // "q_updateLayout"
QT_MOC_LITERAL(50, 811, 17), // "triggerPreprocess"
QT_MOC_LITERAL(51, 829, 21), // "imageDownloadFinished"
QT_MOC_LITERAL(52, 851, 8), // "doLayout"
QT_MOC_LITERAL(53, 860, 6), // "linkAt"
QT_MOC_LITERAL(54, 867, 1), // "x"
QT_MOC_LITERAL(55, 869, 1), // "y"
QT_MOC_LITERAL(56, 871, 5), // "color"
QT_MOC_LITERAL(57, 877, 9), // "linkColor"
QT_MOC_LITERAL(58, 887, 9), // "TextStyle"
QT_MOC_LITERAL(59, 897, 10), // "styleColor"
QT_MOC_LITERAL(60, 908, 19), // "horizontalAlignment"
QT_MOC_LITERAL(61, 928, 10), // "HAlignment"
QT_MOC_LITERAL(62, 939, 28), // "effectiveHorizontalAlignment"
QT_MOC_LITERAL(63, 968, 17), // "verticalAlignment"
QT_MOC_LITERAL(64, 986, 10), // "VAlignment"
QT_MOC_LITERAL(65, 997, 8), // "wrapMode"
QT_MOC_LITERAL(66, 1006, 8), // "WrapMode"
QT_MOC_LITERAL(67, 1015, 9), // "lineCount"
QT_MOC_LITERAL(68, 1025, 9), // "truncated"
QT_MOC_LITERAL(69, 1035, 16), // "maximumLineCount"
QT_MOC_LITERAL(70, 1052, 10), // "TextFormat"
QT_MOC_LITERAL(71, 1063, 5), // "elide"
QT_MOC_LITERAL(72, 1069, 13), // "TextElideMode"
QT_MOC_LITERAL(73, 1083, 12), // "contentWidth"
QT_MOC_LITERAL(74, 1096, 13), // "contentHeight"
QT_MOC_LITERAL(75, 1110, 12), // "paintedWidth"
QT_MOC_LITERAL(76, 1123, 13), // "paintedHeight"
QT_MOC_LITERAL(77, 1137, 14), // "lineHeightMode"
QT_MOC_LITERAL(78, 1152, 7), // "baseUrl"
QT_MOC_LITERAL(79, 1160, 16), // "minimumPixelSize"
QT_MOC_LITERAL(80, 1177, 16), // "minimumPointSize"
QT_MOC_LITERAL(81, 1194, 12), // "fontSizeMode"
QT_MOC_LITERAL(82, 1207, 12), // "FontSizeMode"
QT_MOC_LITERAL(83, 1220, 10), // "renderType"
QT_MOC_LITERAL(84, 1231, 10), // "RenderType"
QT_MOC_LITERAL(85, 1242, 11), // "hoveredLink"
QT_MOC_LITERAL(86, 1254, 7), // "padding"
QT_MOC_LITERAL(87, 1262, 10), // "topPadding"
QT_MOC_LITERAL(88, 1273, 11), // "leftPadding"
QT_MOC_LITERAL(89, 1285, 12), // "rightPadding"
QT_MOC_LITERAL(90, 1298, 13), // "bottomPadding"
QT_MOC_LITERAL(91, 1312, 9), // "AlignLeft"
QT_MOC_LITERAL(92, 1322, 10), // "AlignRight"
QT_MOC_LITERAL(93, 1333, 12), // "AlignHCenter"
QT_MOC_LITERAL(94, 1346, 12), // "AlignJustify"
QT_MOC_LITERAL(95, 1359, 8), // "AlignTop"
QT_MOC_LITERAL(96, 1368, 11), // "AlignBottom"
QT_MOC_LITERAL(97, 1380, 12), // "AlignVCenter"
QT_MOC_LITERAL(98, 1393, 6), // "Normal"
QT_MOC_LITERAL(99, 1400, 7), // "Outline"
QT_MOC_LITERAL(100, 1408, 6), // "Raised"
QT_MOC_LITERAL(101, 1415, 6), // "Sunken"
QT_MOC_LITERAL(102, 1422, 9), // "PlainText"
QT_MOC_LITERAL(103, 1432, 8), // "RichText"
QT_MOC_LITERAL(104, 1441, 8), // "AutoText"
QT_MOC_LITERAL(105, 1450, 10), // "StyledText"
QT_MOC_LITERAL(106, 1461, 9), // "ElideLeft"
QT_MOC_LITERAL(107, 1471, 10), // "ElideRight"
QT_MOC_LITERAL(108, 1482, 11), // "ElideMiddle"
QT_MOC_LITERAL(109, 1494, 9), // "ElideNone"
QT_MOC_LITERAL(110, 1504, 6), // "NoWrap"
QT_MOC_LITERAL(111, 1511, 8), // "WordWrap"
QT_MOC_LITERAL(112, 1520, 12), // "WrapAnywhere"
QT_MOC_LITERAL(113, 1533, 28), // "WrapAtWordBoundaryOrAnywhere"
QT_MOC_LITERAL(114, 1562, 4), // "Wrap"
QT_MOC_LITERAL(115, 1567, 11), // "QtRendering"
QT_MOC_LITERAL(116, 1579, 15), // "NativeRendering"
QT_MOC_LITERAL(117, 1595, 18), // "ProportionalHeight"
QT_MOC_LITERAL(118, 1614, 11), // "FixedHeight"
QT_MOC_LITERAL(119, 1626, 9), // "FixedSize"
QT_MOC_LITERAL(120, 1636, 13), // "HorizontalFit"
QT_MOC_LITERAL(121, 1650, 11), // "VerticalFit"
QT_MOC_LITERAL(122, 1662, 3) // "Fit"

    },
    "QQuickText\0textChanged\0\0text\0linkActivated\0"
    "link\0linkHovered\0fontChanged\0font\0"
    "colorChanged\0linkColorChanged\0"
    "styleChanged\0QQuickText::TextStyle\0"
    "style\0styleColorChanged\0"
    "horizontalAlignmentChanged\0"
    "QQuickText::HAlignment\0alignment\0"
    "verticalAlignmentChanged\0"
    "QQuickText::VAlignment\0wrapModeChanged\0"
    "lineCountChanged\0truncatedChanged\0"
    "maximumLineCountChanged\0textFormatChanged\0"
    "QQuickText::TextFormat\0textFormat\0"
    "elideModeChanged\0QQuickText::TextElideMode\0"
    "mode\0contentSizeChanged\0lineHeightChanged\0"
    "lineHeight\0lineHeightModeChanged\0"
    "LineHeightMode\0fontSizeModeChanged\0"
    "minimumPixelSizeChanged\0minimumPointSizeChanged\0"
    "effectiveHorizontalAlignmentChanged\0"
    "lineLaidOut\0QQuickTextLine*\0line\0"
    "baseUrlChanged\0renderTypeChanged\0"
    "paddingChanged\0topPaddingChanged\0"
    "leftPaddingChanged\0rightPaddingChanged\0"
    "bottomPaddingChanged\0q_updateLayout\0"
    "triggerPreprocess\0imageDownloadFinished\0"
    "doLayout\0linkAt\0x\0y\0color\0linkColor\0"
    "TextStyle\0styleColor\0horizontalAlignment\0"
    "HAlignment\0effectiveHorizontalAlignment\0"
    "verticalAlignment\0VAlignment\0wrapMode\0"
    "WrapMode\0lineCount\0truncated\0"
    "maximumLineCount\0TextFormat\0elide\0"
    "TextElideMode\0contentWidth\0contentHeight\0"
    "paintedWidth\0paintedHeight\0lineHeightMode\0"
    "baseUrl\0minimumPixelSize\0minimumPointSize\0"
    "fontSizeMode\0FontSizeMode\0renderType\0"
    "RenderType\0hoveredLink\0padding\0"
    "topPadding\0leftPadding\0rightPadding\0"
    "bottomPadding\0AlignLeft\0AlignRight\0"
    "AlignHCenter\0AlignJustify\0AlignTop\0"
    "AlignBottom\0AlignVCenter\0Normal\0Outline\0"
    "Raised\0Sunken\0PlainText\0RichText\0"
    "AutoText\0StyledText\0ElideLeft\0ElideRight\0"
    "ElideMiddle\0ElideNone\0NoWrap\0WordWrap\0"
    "WrapAnywhere\0WrapAtWordBoundaryOrAnywhere\0"
    "Wrap\0QtRendering\0NativeRendering\0"
    "ProportionalHeight\0FixedHeight\0FixedSize\0"
    "HorizontalFit\0VerticalFit\0Fit"
};
#undef QT_MOC_LITERAL

static const uint qt_meta_data_QQuickText[] = {

 // content:
       7,       // revision
       0,       // classname
       0,    0, // classinfo
      36,   14, // methods
      32,  294, // properties
       9,  454, // enums/sets
       0,    0, // constructors
       0,       // flags
      31,       // signalCount

 // signals: name, argc, parameters, tag, flags
       1,    1,  230,    2, 0x06 /* Public */,
       4,    1,  233,    2, 0x06 /* Public */,
       6,    1,  236,    2, 0x86 /* Public | MethodRevisioned */,
       7,    1,  239,    2, 0x06 /* Public */,
       9,    0,  242,    2, 0x06 /* Public */,
      10,    0,  243,    2, 0x06 /* Public */,
      11,    1,  244,    2, 0x06 /* Public */,
      14,    0,  247,    2, 0x06 /* Public */,
      15,    1,  248,    2, 0x06 /* Public */,
      18,    1,  251,    2, 0x06 /* Public */,
      20,    0,  254,    2, 0x06 /* Public */,
      21,    0,  255,    2, 0x06 /* Public */,
      22,    0,  256,    2, 0x06 /* Public */,
      23,    0,  257,    2, 0x06 /* Public */,
      24,    1,  258,    2, 0x06 /* Public */,
      27,    1,  261,    2, 0x06 /* Public */,
      30,    0,  264,    2, 0x06 /* Public */,
      31,    1,  265,    2, 0x06 /* Public */,
      33,    1,  268,    2, 0x06 /* Public */,
      35,    0,  271,    2, 0x06 /* Public */,
      36,    0,  272,    2, 0x06 /* Public */,
      37,    0,  273,    2, 0x06 /* Public */,
      38,    0,  274,    2, 0x06 /* Public */,
      39,    1,  275,    2, 0x06 /* Public */,
      42,    0,  278,    2, 0x06 /* Public */,
      43,    0,  279,    2, 0x06 /* Public */,
      44,    0,  280,    2, 0x86 /* Public | MethodRevisioned */,
      45,    0,  281,    2, 0x86 /* Public | MethodRevisioned */,
      46,    0,  282,    2, 0x86 /* Public | MethodRevisioned */,
      47,    0,  283,    2, 0x86 /* Public | MethodRevisioned */,
      48,    0,  284,    2, 0x86 /* Public | MethodRevisioned */,

 // slots: name, argc, parameters, tag, flags
      49,    0,  285,    2, 0x08 /* Private */,
      50,    0,  286,    2, 0x08 /* Private */,
      51,    0,  287,    2, 0x08 /* Private */,

 // methods: name, argc, parameters, tag, flags
      52,    0,  288,    2, 0x02 /* Public */,
      53,    2,  289,    2, 0x82 /* Public | MethodRevisioned */,

 // signals: revision
       0,
       0,
       2,
       0,
       0,
       0,
       0,
       0,
       0,
       0,
       0,
       0,
       0,
       0,
       0,
       0,
       0,
       0,
       0,
       0,
       0,
       0,
       0,
       0,
       0,
       0,
       6,
       6,
       6,
       6,
       6,

 // slots: revision
       0,
       0,
       0,

 // methods: revision
       0,
       3,

 // signals: parameters
    QMetaType::Void, QMetaType::QString,    3,
    QMetaType::Void, QMetaType::QString,    5,
    QMetaType::Void, QMetaType::QString,    5,
    QMetaType::Void, QMetaType::QFont,    8,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void, 0x80000000 | 12,   13,
    QMetaType::Void,
    QMetaType::Void, 0x80000000 | 16,   17,
    QMetaType::Void, 0x80000000 | 19,   17,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void, 0x80000000 | 25,   26,
    QMetaType::Void, 0x80000000 | 28,   29,
    QMetaType::Void,
    QMetaType::Void, QMetaType::QReal,   32,
    QMetaType::Void, 0x80000000 | 34,   29,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void, 0x80000000 | 40,   41,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,

 // slots: parameters
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,

 // methods: parameters
    QMetaType::Void,
    QMetaType::QString, QMetaType::QReal, QMetaType::QReal,   54,   55,

 // properties: name, type, flags
       3, QMetaType::QString, 0x00495103,
       8, QMetaType::QFont, 0x00495103,
      56, QMetaType::QColor, 0x00495103,
      57, QMetaType::QColor, 0x00495103,
      13, 0x80000000 | 58, 0x0049510b,
      59, QMetaType::QColor, 0x00495103,
      60, 0x80000000 | 61, 0x0049500f,
      62, 0x80000000 | 61, 0x00495009,
      63, 0x80000000 | 64, 0x0049500b,
      65, 0x80000000 | 66, 0x0049510b,
      67, QMetaType::Int, 0x00495001,
      68, QMetaType::Bool, 0x00495001,
      69, QMetaType::Int, 0x00495107,
      26, 0x80000000 | 70, 0x0049510b,
      71, 0x80000000 | 72, 0x0049500b,
      73, QMetaType::QReal, 0x00495001,
      74, QMetaType::QReal, 0x00495001,
      75, QMetaType::QReal, 0x00495001,
      76, QMetaType::QReal, 0x00495001,
      32, QMetaType::QReal, 0x00495103,
      77, 0x80000000 | 34, 0x0049510b,
      78, QMetaType::QUrl, 0x00495107,
      79, QMetaType::Int, 0x00495103,
      80, QMetaType::Int, 0x00495103,
      81, 0x80000000 | 82, 0x0049510b,
      83, 0x80000000 | 84, 0x0049510b,
      85, QMetaType::QString, 0x00c95001,
      86, QMetaType::QReal, 0x00c95107,
      87, QMetaType::QReal, 0x00c95107,
      88, QMetaType::QReal, 0x00c95107,
      89, QMetaType::QReal, 0x00c95107,
      90, QMetaType::QReal, 0x00c95107,

 // properties: notify_signal_id
       0,
       3,
       4,
       5,
       6,
       7,
       8,
      22,
       9,
      10,
      11,
      12,
      13,
      14,
      15,
      16,
      16,
      16,
      16,
      17,
      18,
      24,
      20,
      21,
      19,
      25,
       2,
      26,
      27,
      28,
      29,
      30,

 // properties: revision
       0,
       0,
       0,
       0,
       0,
       0,
       0,
       0,
       0,
       0,
       0,
       0,
       0,
       0,
       0,
       0,
       0,
       0,
       0,
       0,
       0,
       0,
       0,
       0,
       0,
       0,
       2,
       6,
       6,
       6,
       6,
       6,

 // enums: name, flags, count, data
      61, 0x0,    4,  490,
      64, 0x0,    3,  498,
      58, 0x0,    4,  504,
      70, 0x0,    4,  512,
      72, 0x0,    4,  520,
      66, 0x0,    5,  528,
      84, 0x0,    2,  538,
      34, 0x0,    2,  542,
      82, 0x0,    4,  546,

 // enum data: key, value
      91, uint(QQuickText::AlignLeft),
      92, uint(QQuickText::AlignRight),
      93, uint(QQuickText::AlignHCenter),
      94, uint(QQuickText::AlignJustify),
      95, uint(QQuickText::AlignTop),
      96, uint(QQuickText::AlignBottom),
      97, uint(QQuickText::AlignVCenter),
      98, uint(QQuickText::Normal),
      99, uint(QQuickText::Outline),
     100, uint(QQuickText::Raised),
     101, uint(QQuickText::Sunken),
     102, uint(QQuickText::PlainText),
     103, uint(QQuickText::RichText),
     104, uint(QQuickText::AutoText),
     105, uint(QQuickText::StyledText),
     106, uint(QQuickText::ElideLeft),
     107, uint(QQuickText::ElideRight),
     108, uint(QQuickText::ElideMiddle),
     109, uint(QQuickText::ElideNone),
     110, uint(QQuickText::NoWrap),
     111, uint(QQuickText::WordWrap),
     112, uint(QQuickText::WrapAnywhere),
     113, uint(QQuickText::WrapAtWordBoundaryOrAnywhere),
     114, uint(QQuickText::Wrap),
     115, uint(QQuickText::QtRendering),
     116, uint(QQuickText::NativeRendering),
     117, uint(QQuickText::ProportionalHeight),
     118, uint(QQuickText::FixedHeight),
     119, uint(QQuickText::FixedSize),
     120, uint(QQuickText::HorizontalFit),
     121, uint(QQuickText::VerticalFit),
     122, uint(QQuickText::Fit),

       0        // eod
};

void QQuickText::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        QQuickText *_t = static_cast<QQuickText *>(_o);
        Q_UNUSED(_t)
        switch (_id) {
        case 0: _t->textChanged((*reinterpret_cast< const QString(*)>(_a[1]))); break;
        case 1: _t->linkActivated((*reinterpret_cast< const QString(*)>(_a[1]))); break;
        case 2: _t->linkHovered((*reinterpret_cast< const QString(*)>(_a[1]))); break;
        case 3: _t->fontChanged((*reinterpret_cast< const QFont(*)>(_a[1]))); break;
        case 4: _t->colorChanged(); break;
        case 5: _t->linkColorChanged(); break;
        case 6: _t->styleChanged((*reinterpret_cast< QQuickText::TextStyle(*)>(_a[1]))); break;
        case 7: _t->styleColorChanged(); break;
        case 8: _t->horizontalAlignmentChanged((*reinterpret_cast< QQuickText::HAlignment(*)>(_a[1]))); break;
        case 9: _t->verticalAlignmentChanged((*reinterpret_cast< QQuickText::VAlignment(*)>(_a[1]))); break;
        case 10: _t->wrapModeChanged(); break;
        case 11: _t->lineCountChanged(); break;
        case 12: _t->truncatedChanged(); break;
        case 13: _t->maximumLineCountChanged(); break;
        case 14: _t->textFormatChanged((*reinterpret_cast< QQuickText::TextFormat(*)>(_a[1]))); break;
        case 15: _t->elideModeChanged((*reinterpret_cast< QQuickText::TextElideMode(*)>(_a[1]))); break;
        case 16: _t->contentSizeChanged(); break;
        case 17: _t->lineHeightChanged((*reinterpret_cast< qreal(*)>(_a[1]))); break;
        case 18: _t->lineHeightModeChanged((*reinterpret_cast< LineHeightMode(*)>(_a[1]))); break;
        case 19: _t->fontSizeModeChanged(); break;
        case 20: _t->minimumPixelSizeChanged(); break;
        case 21: _t->minimumPointSizeChanged(); break;
        case 22: _t->effectiveHorizontalAlignmentChanged(); break;
        case 23: _t->lineLaidOut((*reinterpret_cast< QQuickTextLine*(*)>(_a[1]))); break;
        case 24: _t->baseUrlChanged(); break;
        case 25: _t->renderTypeChanged(); break;
        case 26: _t->paddingChanged(); break;
        case 27: _t->topPaddingChanged(); break;
        case 28: _t->leftPaddingChanged(); break;
        case 29: _t->rightPaddingChanged(); break;
        case 30: _t->bottomPaddingChanged(); break;
        case 31: _t->q_updateLayout(); break;
        case 32: _t->triggerPreprocess(); break;
        case 33: _t->imageDownloadFinished(); break;
        case 34: _t->doLayout(); break;
        case 35: { QString _r = _t->linkAt((*reinterpret_cast< qreal(*)>(_a[1])),(*reinterpret_cast< qreal(*)>(_a[2])));
            if (_a[0]) *reinterpret_cast< QString*>(_a[0]) = _r; }  break;
        default: ;
        }
    } else if (_c == QMetaObject::RegisterMethodArgumentMetaType) {
        switch (_id) {
        default: *reinterpret_cast<int*>(_a[0]) = -1; break;
        case 23:
            switch (*reinterpret_cast<int*>(_a[1])) {
            default: *reinterpret_cast<int*>(_a[0]) = -1; break;
            case 0:
                *reinterpret_cast<int*>(_a[0]) = qRegisterMetaType< QQuickTextLine* >(); break;
            }
            break;
        }
    } else if (_c == QMetaObject::IndexOfMethod) {
        int *result = reinterpret_cast<int *>(_a[0]);
        void **func = reinterpret_cast<void **>(_a[1]);
        {
            typedef void (QQuickText::*_t)(const QString & );
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&QQuickText::textChanged)) {
                *result = 0;
                return;
            }
        }
        {
            typedef void (QQuickText::*_t)(const QString & );
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&QQuickText::linkActivated)) {
                *result = 1;
                return;
            }
        }
        {
            typedef void (QQuickText::*_t)(const QString & );
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&QQuickText::linkHovered)) {
                *result = 2;
                return;
            }
        }
        {
            typedef void (QQuickText::*_t)(const QFont & );
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&QQuickText::fontChanged)) {
                *result = 3;
                return;
            }
        }
        {
            typedef void (QQuickText::*_t)();
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&QQuickText::colorChanged)) {
                *result = 4;
                return;
            }
        }
        {
            typedef void (QQuickText::*_t)();
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&QQuickText::linkColorChanged)) {
                *result = 5;
                return;
            }
        }
        {
            typedef void (QQuickText::*_t)(QQuickText::TextStyle );
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&QQuickText::styleChanged)) {
                *result = 6;
                return;
            }
        }
        {
            typedef void (QQuickText::*_t)();
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&QQuickText::styleColorChanged)) {
                *result = 7;
                return;
            }
        }
        {
            typedef void (QQuickText::*_t)(QQuickText::HAlignment );
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&QQuickText::horizontalAlignmentChanged)) {
                *result = 8;
                return;
            }
        }
        {
            typedef void (QQuickText::*_t)(QQuickText::VAlignment );
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&QQuickText::verticalAlignmentChanged)) {
                *result = 9;
                return;
            }
        }
        {
            typedef void (QQuickText::*_t)();
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&QQuickText::wrapModeChanged)) {
                *result = 10;
                return;
            }
        }
        {
            typedef void (QQuickText::*_t)();
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&QQuickText::lineCountChanged)) {
                *result = 11;
                return;
            }
        }
        {
            typedef void (QQuickText::*_t)();
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&QQuickText::truncatedChanged)) {
                *result = 12;
                return;
            }
        }
        {
            typedef void (QQuickText::*_t)();
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&QQuickText::maximumLineCountChanged)) {
                *result = 13;
                return;
            }
        }
        {
            typedef void (QQuickText::*_t)(QQuickText::TextFormat );
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&QQuickText::textFormatChanged)) {
                *result = 14;
                return;
            }
        }
        {
            typedef void (QQuickText::*_t)(QQuickText::TextElideMode );
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&QQuickText::elideModeChanged)) {
                *result = 15;
                return;
            }
        }
        {
            typedef void (QQuickText::*_t)();
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&QQuickText::contentSizeChanged)) {
                *result = 16;
                return;
            }
        }
        {
            typedef void (QQuickText::*_t)(qreal );
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&QQuickText::lineHeightChanged)) {
                *result = 17;
                return;
            }
        }
        {
            typedef void (QQuickText::*_t)(LineHeightMode );
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&QQuickText::lineHeightModeChanged)) {
                *result = 18;
                return;
            }
        }
        {
            typedef void (QQuickText::*_t)();
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&QQuickText::fontSizeModeChanged)) {
                *result = 19;
                return;
            }
        }
        {
            typedef void (QQuickText::*_t)();
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&QQuickText::minimumPixelSizeChanged)) {
                *result = 20;
                return;
            }
        }
        {
            typedef void (QQuickText::*_t)();
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&QQuickText::minimumPointSizeChanged)) {
                *result = 21;
                return;
            }
        }
        {
            typedef void (QQuickText::*_t)();
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&QQuickText::effectiveHorizontalAlignmentChanged)) {
                *result = 22;
                return;
            }
        }
        {
            typedef void (QQuickText::*_t)(QQuickTextLine * );
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&QQuickText::lineLaidOut)) {
                *result = 23;
                return;
            }
        }
        {
            typedef void (QQuickText::*_t)();
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&QQuickText::baseUrlChanged)) {
                *result = 24;
                return;
            }
        }
        {
            typedef void (QQuickText::*_t)();
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&QQuickText::renderTypeChanged)) {
                *result = 25;
                return;
            }
        }
        {
            typedef void (QQuickText::*_t)();
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&QQuickText::paddingChanged)) {
                *result = 26;
                return;
            }
        }
        {
            typedef void (QQuickText::*_t)();
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&QQuickText::topPaddingChanged)) {
                *result = 27;
                return;
            }
        }
        {
            typedef void (QQuickText::*_t)();
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&QQuickText::leftPaddingChanged)) {
                *result = 28;
                return;
            }
        }
        {
            typedef void (QQuickText::*_t)();
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&QQuickText::rightPaddingChanged)) {
                *result = 29;
                return;
            }
        }
        {
            typedef void (QQuickText::*_t)();
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&QQuickText::bottomPaddingChanged)) {
                *result = 30;
                return;
            }
        }
    }
#ifndef QT_NO_PROPERTIES
    else if (_c == QMetaObject::ReadProperty) {
        QQuickText *_t = static_cast<QQuickText *>(_o);
        Q_UNUSED(_t)
        void *_v = _a[0];
        switch (_id) {
        case 0: *reinterpret_cast< QString*>(_v) = _t->text(); break;
        case 1: *reinterpret_cast< QFont*>(_v) = _t->font(); break;
        case 2: *reinterpret_cast< QColor*>(_v) = _t->color(); break;
        case 3: *reinterpret_cast< QColor*>(_v) = _t->linkColor(); break;
        case 4: *reinterpret_cast< TextStyle*>(_v) = _t->style(); break;
        case 5: *reinterpret_cast< QColor*>(_v) = _t->styleColor(); break;
        case 6: *reinterpret_cast< HAlignment*>(_v) = _t->hAlign(); break;
        case 7: *reinterpret_cast< HAlignment*>(_v) = _t->effectiveHAlign(); break;
        case 8: *reinterpret_cast< VAlignment*>(_v) = _t->vAlign(); break;
        case 9: *reinterpret_cast< WrapMode*>(_v) = _t->wrapMode(); break;
        case 10: *reinterpret_cast< int*>(_v) = _t->lineCount(); break;
        case 11: *reinterpret_cast< bool*>(_v) = _t->truncated(); break;
        case 12: *reinterpret_cast< int*>(_v) = _t->maximumLineCount(); break;
        case 13: *reinterpret_cast< TextFormat*>(_v) = _t->textFormat(); break;
        case 14: *reinterpret_cast< TextElideMode*>(_v) = _t->elideMode(); break;
        case 15: *reinterpret_cast< qreal*>(_v) = _t->contentWidth(); break;
        case 16: *reinterpret_cast< qreal*>(_v) = _t->contentHeight(); break;
        case 17: *reinterpret_cast< qreal*>(_v) = _t->contentWidth(); break;
        case 18: *reinterpret_cast< qreal*>(_v) = _t->contentHeight(); break;
        case 19: *reinterpret_cast< qreal*>(_v) = _t->lineHeight(); break;
        case 20: *reinterpret_cast< LineHeightMode*>(_v) = _t->lineHeightMode(); break;
        case 21: *reinterpret_cast< QUrl*>(_v) = _t->baseUrl(); break;
        case 22: *reinterpret_cast< int*>(_v) = _t->minimumPixelSize(); break;
        case 23: *reinterpret_cast< int*>(_v) = _t->minimumPointSize(); break;
        case 24: *reinterpret_cast< FontSizeMode*>(_v) = _t->fontSizeMode(); break;
        case 25: *reinterpret_cast< RenderType*>(_v) = _t->renderType(); break;
        case 26: *reinterpret_cast< QString*>(_v) = _t->hoveredLink(); break;
        case 27: *reinterpret_cast< qreal*>(_v) = _t->padding(); break;
        case 28: *reinterpret_cast< qreal*>(_v) = _t->topPadding(); break;
        case 29: *reinterpret_cast< qreal*>(_v) = _t->leftPadding(); break;
        case 30: *reinterpret_cast< qreal*>(_v) = _t->rightPadding(); break;
        case 31: *reinterpret_cast< qreal*>(_v) = _t->bottomPadding(); break;
        default: break;
        }
    } else if (_c == QMetaObject::WriteProperty) {
        QQuickText *_t = static_cast<QQuickText *>(_o);
        Q_UNUSED(_t)
        void *_v = _a[0];
        switch (_id) {
        case 0: _t->setText(*reinterpret_cast< QString*>(_v)); break;
        case 1: _t->setFont(*reinterpret_cast< QFont*>(_v)); break;
        case 2: _t->setColor(*reinterpret_cast< QColor*>(_v)); break;
        case 3: _t->setLinkColor(*reinterpret_cast< QColor*>(_v)); break;
        case 4: _t->setStyle(*reinterpret_cast< TextStyle*>(_v)); break;
        case 5: _t->setStyleColor(*reinterpret_cast< QColor*>(_v)); break;
        case 6: _t->setHAlign(*reinterpret_cast< HAlignment*>(_v)); break;
        case 8: _t->setVAlign(*reinterpret_cast< VAlignment*>(_v)); break;
        case 9: _t->setWrapMode(*reinterpret_cast< WrapMode*>(_v)); break;
        case 12: _t->setMaximumLineCount(*reinterpret_cast< int*>(_v)); break;
        case 13: _t->setTextFormat(*reinterpret_cast< TextFormat*>(_v)); break;
        case 14: _t->setElideMode(*reinterpret_cast< TextElideMode*>(_v)); break;
        case 19: _t->setLineHeight(*reinterpret_cast< qreal*>(_v)); break;
        case 20: _t->setLineHeightMode(*reinterpret_cast< LineHeightMode*>(_v)); break;
        case 21: _t->setBaseUrl(*reinterpret_cast< QUrl*>(_v)); break;
        case 22: _t->setMinimumPixelSize(*reinterpret_cast< int*>(_v)); break;
        case 23: _t->setMinimumPointSize(*reinterpret_cast< int*>(_v)); break;
        case 24: _t->setFontSizeMode(*reinterpret_cast< FontSizeMode*>(_v)); break;
        case 25: _t->setRenderType(*reinterpret_cast< RenderType*>(_v)); break;
        case 27: _t->setPadding(*reinterpret_cast< qreal*>(_v)); break;
        case 28: _t->setTopPadding(*reinterpret_cast< qreal*>(_v)); break;
        case 29: _t->setLeftPadding(*reinterpret_cast< qreal*>(_v)); break;
        case 30: _t->setRightPadding(*reinterpret_cast< qreal*>(_v)); break;
        case 31: _t->setBottomPadding(*reinterpret_cast< qreal*>(_v)); break;
        default: break;
        }
    } else if (_c == QMetaObject::ResetProperty) {
        QQuickText *_t = static_cast<QQuickText *>(_o);
        Q_UNUSED(_t)
        switch (_id) {
        case 6: _t->resetHAlign(); break;
        case 12: _t->resetMaximumLineCount(); break;
        case 21: _t->resetBaseUrl(); break;
        case 27: _t->resetPadding(); break;
        case 28: _t->resetTopPadding(); break;
        case 29: _t->resetLeftPadding(); break;
        case 30: _t->resetRightPadding(); break;
        case 31: _t->resetBottomPadding(); break;
        default: break;
        }
    }
#endif // QT_NO_PROPERTIES
}

const QMetaObject QQuickText::staticMetaObject = {
    { &QQuickImplicitSizeItem::staticMetaObject, qt_meta_stringdata_QQuickText.data,
      qt_meta_data_QQuickText,  qt_static_metacall, Q_NULLPTR, Q_NULLPTR}
};


const QMetaObject *QQuickText::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->dynamicMetaObject() : &staticMetaObject;
}

void *QQuickText::qt_metacast(const char *_clname)
{
    if (!_clname) return Q_NULLPTR;
    if (!strcmp(_clname, qt_meta_stringdata_QQuickText.stringdata0))
        return static_cast<void*>(const_cast< QQuickText*>(this));
    return QQuickImplicitSizeItem::qt_metacast(_clname);
}

int QQuickText::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QQuickImplicitSizeItem::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 36)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 36;
    } else if (_c == QMetaObject::RegisterMethodArgumentMetaType) {
        if (_id < 36)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 36;
    }
#ifndef QT_NO_PROPERTIES
   else if (_c == QMetaObject::ReadProperty || _c == QMetaObject::WriteProperty
            || _c == QMetaObject::ResetProperty || _c == QMetaObject::RegisterPropertyMetaType) {
        qt_static_metacall(this, _c, _id, _a);
        _id -= 32;
    } else if (_c == QMetaObject::QueryPropertyDesignable) {
        _id -= 32;
    } else if (_c == QMetaObject::QueryPropertyScriptable) {
        _id -= 32;
    } else if (_c == QMetaObject::QueryPropertyStored) {
        _id -= 32;
    } else if (_c == QMetaObject::QueryPropertyEditable) {
        _id -= 32;
    } else if (_c == QMetaObject::QueryPropertyUser) {
        _id -= 32;
    }
#endif // QT_NO_PROPERTIES
    return _id;
}

// SIGNAL 0
void QQuickText::textChanged(const QString & _t1)
{
    void *_a[] = { Q_NULLPTR, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 0, _a);
}

// SIGNAL 1
void QQuickText::linkActivated(const QString & _t1)
{
    void *_a[] = { Q_NULLPTR, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 1, _a);
}

// SIGNAL 2
void QQuickText::linkHovered(const QString & _t1)
{
    void *_a[] = { Q_NULLPTR, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 2, _a);
}

// SIGNAL 3
void QQuickText::fontChanged(const QFont & _t1)
{
    void *_a[] = { Q_NULLPTR, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 3, _a);
}

// SIGNAL 4
void QQuickText::colorChanged()
{
    QMetaObject::activate(this, &staticMetaObject, 4, Q_NULLPTR);
}

// SIGNAL 5
void QQuickText::linkColorChanged()
{
    QMetaObject::activate(this, &staticMetaObject, 5, Q_NULLPTR);
}

// SIGNAL 6
void QQuickText::styleChanged(QQuickText::TextStyle _t1)
{
    void *_a[] = { Q_NULLPTR, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 6, _a);
}

// SIGNAL 7
void QQuickText::styleColorChanged()
{
    QMetaObject::activate(this, &staticMetaObject, 7, Q_NULLPTR);
}

// SIGNAL 8
void QQuickText::horizontalAlignmentChanged(QQuickText::HAlignment _t1)
{
    void *_a[] = { Q_NULLPTR, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 8, _a);
}

// SIGNAL 9
void QQuickText::verticalAlignmentChanged(QQuickText::VAlignment _t1)
{
    void *_a[] = { Q_NULLPTR, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 9, _a);
}

// SIGNAL 10
void QQuickText::wrapModeChanged()
{
    QMetaObject::activate(this, &staticMetaObject, 10, Q_NULLPTR);
}

// SIGNAL 11
void QQuickText::lineCountChanged()
{
    QMetaObject::activate(this, &staticMetaObject, 11, Q_NULLPTR);
}

// SIGNAL 12
void QQuickText::truncatedChanged()
{
    QMetaObject::activate(this, &staticMetaObject, 12, Q_NULLPTR);
}

// SIGNAL 13
void QQuickText::maximumLineCountChanged()
{
    QMetaObject::activate(this, &staticMetaObject, 13, Q_NULLPTR);
}

// SIGNAL 14
void QQuickText::textFormatChanged(QQuickText::TextFormat _t1)
{
    void *_a[] = { Q_NULLPTR, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 14, _a);
}

// SIGNAL 15
void QQuickText::elideModeChanged(QQuickText::TextElideMode _t1)
{
    void *_a[] = { Q_NULLPTR, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 15, _a);
}

// SIGNAL 16
void QQuickText::contentSizeChanged()
{
    QMetaObject::activate(this, &staticMetaObject, 16, Q_NULLPTR);
}

// SIGNAL 17
void QQuickText::lineHeightChanged(qreal _t1)
{
    void *_a[] = { Q_NULLPTR, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 17, _a);
}

// SIGNAL 18
void QQuickText::lineHeightModeChanged(LineHeightMode _t1)
{
    void *_a[] = { Q_NULLPTR, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 18, _a);
}

// SIGNAL 19
void QQuickText::fontSizeModeChanged()
{
    QMetaObject::activate(this, &staticMetaObject, 19, Q_NULLPTR);
}

// SIGNAL 20
void QQuickText::minimumPixelSizeChanged()
{
    QMetaObject::activate(this, &staticMetaObject, 20, Q_NULLPTR);
}

// SIGNAL 21
void QQuickText::minimumPointSizeChanged()
{
    QMetaObject::activate(this, &staticMetaObject, 21, Q_NULLPTR);
}

// SIGNAL 22
void QQuickText::effectiveHorizontalAlignmentChanged()
{
    QMetaObject::activate(this, &staticMetaObject, 22, Q_NULLPTR);
}

// SIGNAL 23
void QQuickText::lineLaidOut(QQuickTextLine * _t1)
{
    void *_a[] = { Q_NULLPTR, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 23, _a);
}

// SIGNAL 24
void QQuickText::baseUrlChanged()
{
    QMetaObject::activate(this, &staticMetaObject, 24, Q_NULLPTR);
}

// SIGNAL 25
void QQuickText::renderTypeChanged()
{
    QMetaObject::activate(this, &staticMetaObject, 25, Q_NULLPTR);
}

// SIGNAL 26
void QQuickText::paddingChanged()
{
    QMetaObject::activate(this, &staticMetaObject, 26, Q_NULLPTR);
}

// SIGNAL 27
void QQuickText::topPaddingChanged()
{
    QMetaObject::activate(this, &staticMetaObject, 27, Q_NULLPTR);
}

// SIGNAL 28
void QQuickText::leftPaddingChanged()
{
    QMetaObject::activate(this, &staticMetaObject, 28, Q_NULLPTR);
}

// SIGNAL 29
void QQuickText::rightPaddingChanged()
{
    QMetaObject::activate(this, &staticMetaObject, 29, Q_NULLPTR);
}

// SIGNAL 30
void QQuickText::bottomPaddingChanged()
{
    QMetaObject::activate(this, &staticMetaObject, 30, Q_NULLPTR);
}
struct qt_meta_stringdata_QQuickTextLine_t {
    QByteArrayData data[6];
    char stringdata0[39];
};
#define QT_MOC_LITERAL(idx, ofs, len) \
    Q_STATIC_BYTE_ARRAY_DATA_HEADER_INITIALIZER_WITH_OFFSET(len, \
    qptrdiff(offsetof(qt_meta_stringdata_QQuickTextLine_t, stringdata0) + ofs \
        - idx * sizeof(QByteArrayData)) \
    )
static const qt_meta_stringdata_QQuickTextLine_t qt_meta_stringdata_QQuickTextLine = {
    {
QT_MOC_LITERAL(0, 0, 14), // "QQuickTextLine"
QT_MOC_LITERAL(1, 15, 6), // "number"
QT_MOC_LITERAL(2, 22, 5), // "width"
QT_MOC_LITERAL(3, 28, 6), // "height"
QT_MOC_LITERAL(4, 35, 1), // "x"
QT_MOC_LITERAL(5, 37, 1) // "y"

    },
    "QQuickTextLine\0number\0width\0height\0x\0"
    "y"
};
#undef QT_MOC_LITERAL

static const uint qt_meta_data_QQuickTextLine[] = {

 // content:
       7,       // revision
       0,       // classname
       0,    0, // classinfo
       0,    0, // methods
       5,   14, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       0,       // signalCount

 // properties: name, type, flags
       1, QMetaType::Int, 0x00095001,
       2, QMetaType::QReal, 0x00095103,
       3, QMetaType::QReal, 0x00095103,
       4, QMetaType::QReal, 0x00095103,
       5, QMetaType::QReal, 0x00095103,

       0        // eod
};

void QQuickTextLine::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{

#ifndef QT_NO_PROPERTIES
    if (_c == QMetaObject::ReadProperty) {
        QQuickTextLine *_t = static_cast<QQuickTextLine *>(_o);
        Q_UNUSED(_t)
        void *_v = _a[0];
        switch (_id) {
        case 0: *reinterpret_cast< int*>(_v) = _t->number(); break;
        case 1: *reinterpret_cast< qreal*>(_v) = _t->width(); break;
        case 2: *reinterpret_cast< qreal*>(_v) = _t->height(); break;
        case 3: *reinterpret_cast< qreal*>(_v) = _t->x(); break;
        case 4: *reinterpret_cast< qreal*>(_v) = _t->y(); break;
        default: break;
        }
    } else if (_c == QMetaObject::WriteProperty) {
        QQuickTextLine *_t = static_cast<QQuickTextLine *>(_o);
        Q_UNUSED(_t)
        void *_v = _a[0];
        switch (_id) {
        case 1: _t->setWidth(*reinterpret_cast< qreal*>(_v)); break;
        case 2: _t->setHeight(*reinterpret_cast< qreal*>(_v)); break;
        case 3: _t->setX(*reinterpret_cast< qreal*>(_v)); break;
        case 4: _t->setY(*reinterpret_cast< qreal*>(_v)); break;
        default: break;
        }
    } else if (_c == QMetaObject::ResetProperty) {
    }
#endif // QT_NO_PROPERTIES
    Q_UNUSED(_o);
    Q_UNUSED(_id);
    Q_UNUSED(_c);
    Q_UNUSED(_a);
}

const QMetaObject QQuickTextLine::staticMetaObject = {
    { &QObject::staticMetaObject, qt_meta_stringdata_QQuickTextLine.data,
      qt_meta_data_QQuickTextLine,  qt_static_metacall, Q_NULLPTR, Q_NULLPTR}
};


const QMetaObject *QQuickTextLine::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->dynamicMetaObject() : &staticMetaObject;
}

void *QQuickTextLine::qt_metacast(const char *_clname)
{
    if (!_clname) return Q_NULLPTR;
    if (!strcmp(_clname, qt_meta_stringdata_QQuickTextLine.stringdata0))
        return static_cast<void*>(const_cast< QQuickTextLine*>(this));
    return QObject::qt_metacast(_clname);
}

int QQuickTextLine::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QObject::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    
#ifndef QT_NO_PROPERTIES
   if (_c == QMetaObject::ReadProperty || _c == QMetaObject::WriteProperty
            || _c == QMetaObject::ResetProperty || _c == QMetaObject::RegisterPropertyMetaType) {
        qt_static_metacall(this, _c, _id, _a);
        _id -= 5;
    } else if (_c == QMetaObject::QueryPropertyDesignable) {
        _id -= 5;
    } else if (_c == QMetaObject::QueryPropertyScriptable) {
        _id -= 5;
    } else if (_c == QMetaObject::QueryPropertyStored) {
        _id -= 5;
    } else if (_c == QMetaObject::QueryPropertyEditable) {
        _id -= 5;
    } else if (_c == QMetaObject::QueryPropertyUser) {
        _id -= 5;
    }
#endif // QT_NO_PROPERTIES
    return _id;
}
QT_END_MOC_NAMESPACE
