/****************************************************************************
** Meta object code from reading C++ file 'qquickshortcut_p.h'
**
** Created by: The Qt Meta Object Compiler version 67 (Qt 5.6.3)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "../../util/qquickshortcut_p.h"
#include <QtCore/qbytearray.h>
#include <QtCore/qmetatype.h>
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'qquickshortcut_p.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 67
#error "This file was generated using the moc from 5.6.3. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
struct qt_meta_stringdata_QQuickShortcut_t {
    QByteArrayData data[15];
    char stringdata0[191];
};
#define QT_MOC_LITERAL(idx, ofs, len) \
    Q_STATIC_BYTE_ARRAY_DATA_HEADER_INITIALIZER_WITH_OFFSET(len, \
    qptrdiff(offsetof(qt_meta_stringdata_QQuickShortcut_t, stringdata0) + ofs \
        - idx * sizeof(QByteArrayData)) \
    )
static const qt_meta_stringdata_QQuickShortcut_t qt_meta_stringdata_QQuickShortcut = {
    {
QT_MOC_LITERAL(0, 0, 14), // "QQuickShortcut"
QT_MOC_LITERAL(1, 15, 15), // "sequenceChanged"
QT_MOC_LITERAL(2, 31, 0), // ""
QT_MOC_LITERAL(3, 32, 14), // "enabledChanged"
QT_MOC_LITERAL(4, 47, 17), // "autoRepeatChanged"
QT_MOC_LITERAL(5, 65, 14), // "contextChanged"
QT_MOC_LITERAL(6, 80, 9), // "activated"
QT_MOC_LITERAL(7, 90, 20), // "activatedAmbiguously"
QT_MOC_LITERAL(8, 111, 8), // "sequence"
QT_MOC_LITERAL(9, 120, 10), // "nativeText"
QT_MOC_LITERAL(10, 131, 12), // "portableText"
QT_MOC_LITERAL(11, 144, 7), // "enabled"
QT_MOC_LITERAL(12, 152, 10), // "autoRepeat"
QT_MOC_LITERAL(13, 163, 7), // "context"
QT_MOC_LITERAL(14, 171, 19) // "Qt::ShortcutContext"

    },
    "QQuickShortcut\0sequenceChanged\0\0"
    "enabledChanged\0autoRepeatChanged\0"
    "contextChanged\0activated\0activatedAmbiguously\0"
    "sequence\0nativeText\0portableText\0"
    "enabled\0autoRepeat\0context\0"
    "Qt::ShortcutContext"
};
#undef QT_MOC_LITERAL

static const uint qt_meta_data_QQuickShortcut[] = {

 // content:
       7,       // revision
       0,       // classname
       0,    0, // classinfo
       6,   14, // methods
       6,   50, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       6,       // signalCount

 // signals: name, argc, parameters, tag, flags
       1,    0,   44,    2, 0x06 /* Public */,
       3,    0,   45,    2, 0x06 /* Public */,
       4,    0,   46,    2, 0x06 /* Public */,
       5,    0,   47,    2, 0x06 /* Public */,
       6,    0,   48,    2, 0x06 /* Public */,
       7,    0,   49,    2, 0x06 /* Public */,

 // signals: parameters
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,

 // properties: name, type, flags
       8, QMetaType::QVariant, 0x00495903,
       9, QMetaType::QString, 0x00c95801,
      10, QMetaType::QString, 0x00c95801,
      11, QMetaType::Bool, 0x00495903,
      12, QMetaType::Bool, 0x00495903,
      13, 0x80000000 | 14, 0x0049590b,

 // properties: notify_signal_id
       0,
       0,
       0,
       1,
       2,
       3,

 // properties: revision
       0,
       1,
       1,
       0,
       0,
       0,

       0        // eod
};

void QQuickShortcut::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        QQuickShortcut *_t = static_cast<QQuickShortcut *>(_o);
        Q_UNUSED(_t)
        switch (_id) {
        case 0: _t->sequenceChanged(); break;
        case 1: _t->enabledChanged(); break;
        case 2: _t->autoRepeatChanged(); break;
        case 3: _t->contextChanged(); break;
        case 4: _t->activated(); break;
        case 5: _t->activatedAmbiguously(); break;
        default: ;
        }
    } else if (_c == QMetaObject::IndexOfMethod) {
        int *result = reinterpret_cast<int *>(_a[0]);
        void **func = reinterpret_cast<void **>(_a[1]);
        {
            typedef void (QQuickShortcut::*_t)();
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&QQuickShortcut::sequenceChanged)) {
                *result = 0;
                return;
            }
        }
        {
            typedef void (QQuickShortcut::*_t)();
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&QQuickShortcut::enabledChanged)) {
                *result = 1;
                return;
            }
        }
        {
            typedef void (QQuickShortcut::*_t)();
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&QQuickShortcut::autoRepeatChanged)) {
                *result = 2;
                return;
            }
        }
        {
            typedef void (QQuickShortcut::*_t)();
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&QQuickShortcut::contextChanged)) {
                *result = 3;
                return;
            }
        }
        {
            typedef void (QQuickShortcut::*_t)();
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&QQuickShortcut::activated)) {
                *result = 4;
                return;
            }
        }
        {
            typedef void (QQuickShortcut::*_t)();
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&QQuickShortcut::activatedAmbiguously)) {
                *result = 5;
                return;
            }
        }
    }
#ifndef QT_NO_PROPERTIES
    else if (_c == QMetaObject::ReadProperty) {
        QQuickShortcut *_t = static_cast<QQuickShortcut *>(_o);
        Q_UNUSED(_t)
        void *_v = _a[0];
        switch (_id) {
        case 0: *reinterpret_cast< QVariant*>(_v) = _t->sequence(); break;
        case 1: *reinterpret_cast< QString*>(_v) = _t->nativeText(); break;
        case 2: *reinterpret_cast< QString*>(_v) = _t->portableText(); break;
        case 3: *reinterpret_cast< bool*>(_v) = _t->isEnabled(); break;
        case 4: *reinterpret_cast< bool*>(_v) = _t->autoRepeat(); break;
        case 5: *reinterpret_cast< Qt::ShortcutContext*>(_v) = _t->context(); break;
        default: break;
        }
    } else if (_c == QMetaObject::WriteProperty) {
        QQuickShortcut *_t = static_cast<QQuickShortcut *>(_o);
        Q_UNUSED(_t)
        void *_v = _a[0];
        switch (_id) {
        case 0: _t->setSequence(*reinterpret_cast< QVariant*>(_v)); break;
        case 3: _t->setEnabled(*reinterpret_cast< bool*>(_v)); break;
        case 4: _t->setAutoRepeat(*reinterpret_cast< bool*>(_v)); break;
        case 5: _t->setContext(*reinterpret_cast< Qt::ShortcutContext*>(_v)); break;
        default: break;
        }
    } else if (_c == QMetaObject::ResetProperty) {
    }
#endif // QT_NO_PROPERTIES
    Q_UNUSED(_a);
}

const QMetaObject QQuickShortcut::staticMetaObject = {
    { &QObject::staticMetaObject, qt_meta_stringdata_QQuickShortcut.data,
      qt_meta_data_QQuickShortcut,  qt_static_metacall, Q_NULLPTR, Q_NULLPTR}
};


const QMetaObject *QQuickShortcut::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->dynamicMetaObject() : &staticMetaObject;
}

void *QQuickShortcut::qt_metacast(const char *_clname)
{
    if (!_clname) return Q_NULLPTR;
    if (!strcmp(_clname, qt_meta_stringdata_QQuickShortcut.stringdata0))
        return static_cast<void*>(const_cast< QQuickShortcut*>(this));
    if (!strcmp(_clname, "QQmlParserStatus"))
        return static_cast< QQmlParserStatus*>(const_cast< QQuickShortcut*>(this));
    if (!strcmp(_clname, "org.qt-project.Qt.QQmlParserStatus"))
        return static_cast< QQmlParserStatus*>(const_cast< QQuickShortcut*>(this));
    return QObject::qt_metacast(_clname);
}

int QQuickShortcut::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QObject::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 6)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 6;
    } else if (_c == QMetaObject::RegisterMethodArgumentMetaType) {
        if (_id < 6)
            *reinterpret_cast<int*>(_a[0]) = -1;
        _id -= 6;
    }
#ifndef QT_NO_PROPERTIES
   else if (_c == QMetaObject::ReadProperty || _c == QMetaObject::WriteProperty
            || _c == QMetaObject::ResetProperty || _c == QMetaObject::RegisterPropertyMetaType) {
        qt_static_metacall(this, _c, _id, _a);
        _id -= 6;
    } else if (_c == QMetaObject::QueryPropertyDesignable) {
        _id -= 6;
    } else if (_c == QMetaObject::QueryPropertyScriptable) {
        _id -= 6;
    } else if (_c == QMetaObject::QueryPropertyStored) {
        _id -= 6;
    } else if (_c == QMetaObject::QueryPropertyEditable) {
        _id -= 6;
    } else if (_c == QMetaObject::QueryPropertyUser) {
        _id -= 6;
    }
#endif // QT_NO_PROPERTIES
    return _id;
}

// SIGNAL 0
void QQuickShortcut::sequenceChanged()
{
    QMetaObject::activate(this, &staticMetaObject, 0, Q_NULLPTR);
}

// SIGNAL 1
void QQuickShortcut::enabledChanged()
{
    QMetaObject::activate(this, &staticMetaObject, 1, Q_NULLPTR);
}

// SIGNAL 2
void QQuickShortcut::autoRepeatChanged()
{
    QMetaObject::activate(this, &staticMetaObject, 2, Q_NULLPTR);
}

// SIGNAL 3
void QQuickShortcut::contextChanged()
{
    QMetaObject::activate(this, &staticMetaObject, 3, Q_NULLPTR);
}

// SIGNAL 4
void QQuickShortcut::activated()
{
    QMetaObject::activate(this, &staticMetaObject, 4, Q_NULLPTR);
}

// SIGNAL 5
void QQuickShortcut::activatedAmbiguously()
{
    QMetaObject::activate(this, &staticMetaObject, 5, Q_NULLPTR);
}
QT_END_MOC_NAMESPACE
