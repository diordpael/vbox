/****************************************************************************
** Meta object code from reading C++ file 'qquickanimatorcontroller_p.h'
**
** Created by: The Qt Meta Object Compiler version 67 (Qt 5.6.3)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "../../util/qquickanimatorcontroller_p.h"
#include <QtCore/qbytearray.h>
#include <QtCore/qmetatype.h>
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'qquickanimatorcontroller_p.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 67
#error "This file was generated using the moc from 5.6.3. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
struct qt_meta_stringdata_QQuickAnimatorController_t {
    QByteArrayData data[3];
    char stringdata0[40];
};
#define QT_MOC_LITERAL(idx, ofs, len) \
    Q_STATIC_BYTE_ARRAY_DATA_HEADER_INITIALIZER_WITH_OFFSET(len, \
    qptrdiff(offsetof(qt_meta_stringdata_QQuickAnimatorController_t, stringdata0) + ofs \
        - idx * sizeof(QByteArrayData)) \
    )
static const qt_meta_stringdata_QQuickAnimatorController_t qt_meta_stringdata_QQuickAnimatorController = {
    {
QT_MOC_LITERAL(0, 0, 24), // "QQuickAnimatorController"
QT_MOC_LITERAL(1, 25, 13), // "itemDestroyed"
QT_MOC_LITERAL(2, 39, 0) // ""

    },
    "QQuickAnimatorController\0itemDestroyed\0"
    ""
};
#undef QT_MOC_LITERAL

static const uint qt_meta_data_QQuickAnimatorController[] = {

 // content:
       7,       // revision
       0,       // classname
       0,    0, // classinfo
       1,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       0,       // signalCount

 // slots: name, argc, parameters, tag, flags
       1,    1,   19,    2, 0x0a /* Public */,

 // slots: parameters
    QMetaType::Void, QMetaType::QObjectStar,    2,

       0        // eod
};

void QQuickAnimatorController::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        QQuickAnimatorController *_t = static_cast<QQuickAnimatorController *>(_o);
        Q_UNUSED(_t)
        switch (_id) {
        case 0: _t->itemDestroyed((*reinterpret_cast< QObject*(*)>(_a[1]))); break;
        default: ;
        }
    }
}

const QMetaObject QQuickAnimatorController::staticMetaObject = {
    { &QObject::staticMetaObject, qt_meta_stringdata_QQuickAnimatorController.data,
      qt_meta_data_QQuickAnimatorController,  qt_static_metacall, Q_NULLPTR, Q_NULLPTR}
};


const QMetaObject *QQuickAnimatorController::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->dynamicMetaObject() : &staticMetaObject;
}

void *QQuickAnimatorController::qt_metacast(const char *_clname)
{
    if (!_clname) return Q_NULLPTR;
    if (!strcmp(_clname, qt_meta_stringdata_QQuickAnimatorController.stringdata0))
        return static_cast<void*>(const_cast< QQuickAnimatorController*>(this));
    if (!strcmp(_clname, "QAnimationJobChangeListener"))
        return static_cast< QAnimationJobChangeListener*>(const_cast< QQuickAnimatorController*>(this));
    return QObject::qt_metacast(_clname);
}

int QQuickAnimatorController::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QObject::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 1)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 1;
    } else if (_c == QMetaObject::RegisterMethodArgumentMetaType) {
        if (_id < 1)
            *reinterpret_cast<int*>(_a[0]) = -1;
        _id -= 1;
    }
    return _id;
}
struct qt_meta_stringdata_QQuickAnimatorControllerGuiThreadEntity_t {
    QByteArrayData data[3];
    char stringdata0[54];
};
#define QT_MOC_LITERAL(idx, ofs, len) \
    Q_STATIC_BYTE_ARRAY_DATA_HEADER_INITIALIZER_WITH_OFFSET(len, \
    qptrdiff(offsetof(qt_meta_stringdata_QQuickAnimatorControllerGuiThreadEntity_t, stringdata0) + ofs \
        - idx * sizeof(QByteArrayData)) \
    )
static const qt_meta_stringdata_QQuickAnimatorControllerGuiThreadEntity_t qt_meta_stringdata_QQuickAnimatorControllerGuiThreadEntity = {
    {
QT_MOC_LITERAL(0, 0, 39), // "QQuickAnimatorControllerGuiTh..."
QT_MOC_LITERAL(1, 40, 12), // "frameSwapped"
QT_MOC_LITERAL(2, 53, 0) // ""

    },
    "QQuickAnimatorControllerGuiThreadEntity\0"
    "frameSwapped\0"
};
#undef QT_MOC_LITERAL

static const uint qt_meta_data_QQuickAnimatorControllerGuiThreadEntity[] = {

 // content:
       7,       // revision
       0,       // classname
       0,    0, // classinfo
       1,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       0,       // signalCount

 // slots: name, argc, parameters, tag, flags
       1,    0,   19,    2, 0x0a /* Public */,

 // slots: parameters
    QMetaType::Void,

       0        // eod
};

void QQuickAnimatorControllerGuiThreadEntity::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        QQuickAnimatorControllerGuiThreadEntity *_t = static_cast<QQuickAnimatorControllerGuiThreadEntity *>(_o);
        Q_UNUSED(_t)
        switch (_id) {
        case 0: _t->frameSwapped(); break;
        default: ;
        }
    }
    Q_UNUSED(_a);
}

const QMetaObject QQuickAnimatorControllerGuiThreadEntity::staticMetaObject = {
    { &QObject::staticMetaObject, qt_meta_stringdata_QQuickAnimatorControllerGuiThreadEntity.data,
      qt_meta_data_QQuickAnimatorControllerGuiThreadEntity,  qt_static_metacall, Q_NULLPTR, Q_NULLPTR}
};


const QMetaObject *QQuickAnimatorControllerGuiThreadEntity::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->dynamicMetaObject() : &staticMetaObject;
}

void *QQuickAnimatorControllerGuiThreadEntity::qt_metacast(const char *_clname)
{
    if (!_clname) return Q_NULLPTR;
    if (!strcmp(_clname, qt_meta_stringdata_QQuickAnimatorControllerGuiThreadEntity.stringdata0))
        return static_cast<void*>(const_cast< QQuickAnimatorControllerGuiThreadEntity*>(this));
    return QObject::qt_metacast(_clname);
}

int QQuickAnimatorControllerGuiThreadEntity::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QObject::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 1)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 1;
    } else if (_c == QMetaObject::RegisterMethodArgumentMetaType) {
        if (_id < 1)
            *reinterpret_cast<int*>(_a[0]) = -1;
        _id -= 1;
    }
    return _id;
}
QT_END_MOC_NAMESPACE
