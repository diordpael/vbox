/****************************************************************************
** Meta object code from reading C++ file 'qquickevents_p_p.h'
**
** Created by: The Qt Meta Object Compiler version 67 (Qt 5.6.3)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "../../items/qquickevents_p_p.h"
#include <QtCore/qbytearray.h>
#include <QtCore/qmetatype.h>
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'qquickevents_p_p.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 67
#error "This file was generated using the moc from 5.6.3. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
struct qt_meta_stringdata_QQuickKeyEvent_t {
    QByteArrayData data[11];
    char stringdata0[112];
};
#define QT_MOC_LITERAL(idx, ofs, len) \
    Q_STATIC_BYTE_ARRAY_DATA_HEADER_INITIALIZER_WITH_OFFSET(len, \
    qptrdiff(offsetof(qt_meta_stringdata_QQuickKeyEvent_t, stringdata0) + ofs \
        - idx * sizeof(QByteArrayData)) \
    )
static const qt_meta_stringdata_QQuickKeyEvent_t qt_meta_stringdata_QQuickKeyEvent = {
    {
QT_MOC_LITERAL(0, 0, 14), // "QQuickKeyEvent"
QT_MOC_LITERAL(1, 15, 7), // "matches"
QT_MOC_LITERAL(2, 23, 0), // ""
QT_MOC_LITERAL(3, 24, 25), // "QKeySequence::StandardKey"
QT_MOC_LITERAL(4, 50, 3), // "key"
QT_MOC_LITERAL(5, 54, 4), // "text"
QT_MOC_LITERAL(6, 59, 9), // "modifiers"
QT_MOC_LITERAL(7, 69, 12), // "isAutoRepeat"
QT_MOC_LITERAL(8, 82, 5), // "count"
QT_MOC_LITERAL(9, 88, 14), // "nativeScanCode"
QT_MOC_LITERAL(10, 103, 8) // "accepted"

    },
    "QQuickKeyEvent\0matches\0\0"
    "QKeySequence::StandardKey\0key\0text\0"
    "modifiers\0isAutoRepeat\0count\0"
    "nativeScanCode\0accepted"
};
#undef QT_MOC_LITERAL

static const uint qt_meta_data_QQuickKeyEvent[] = {

 // content:
       7,       // revision
       0,       // classname
       0,    0, // classinfo
       1,   14, // methods
       7,   23, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       0,       // signalCount

 // methods: name, argc, parameters, tag, flags
       1,    1,   20,    2, 0x82 /* Public | MethodRevisioned */,

 // methods: revision
       2,

 // methods: parameters
    QMetaType::Bool, 0x80000000 | 3,    4,

 // properties: name, type, flags
       4, QMetaType::Int, 0x00095001,
       5, QMetaType::QString, 0x00095001,
       6, QMetaType::Int, 0x00095001,
       7, QMetaType::Bool, 0x00095001,
       8, QMetaType::Int, 0x00095001,
       9, QMetaType::UInt, 0x00095001,
      10, QMetaType::Bool, 0x00095103,

       0        // eod
};

void QQuickKeyEvent::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        QQuickKeyEvent *_t = static_cast<QQuickKeyEvent *>(_o);
        Q_UNUSED(_t)
        switch (_id) {
        case 0: { bool _r = _t->matches((*reinterpret_cast< QKeySequence::StandardKey(*)>(_a[1])));
            if (_a[0]) *reinterpret_cast< bool*>(_a[0]) = _r; }  break;
        default: ;
        }
    }
#ifndef QT_NO_PROPERTIES
    else if (_c == QMetaObject::ReadProperty) {
        QQuickKeyEvent *_t = static_cast<QQuickKeyEvent *>(_o);
        Q_UNUSED(_t)
        void *_v = _a[0];
        switch (_id) {
        case 0: *reinterpret_cast< int*>(_v) = _t->key(); break;
        case 1: *reinterpret_cast< QString*>(_v) = _t->text(); break;
        case 2: *reinterpret_cast< int*>(_v) = _t->modifiers(); break;
        case 3: *reinterpret_cast< bool*>(_v) = _t->isAutoRepeat(); break;
        case 4: *reinterpret_cast< int*>(_v) = _t->count(); break;
        case 5: *reinterpret_cast< quint32*>(_v) = _t->nativeScanCode(); break;
        case 6: *reinterpret_cast< bool*>(_v) = _t->isAccepted(); break;
        default: break;
        }
    } else if (_c == QMetaObject::WriteProperty) {
        QQuickKeyEvent *_t = static_cast<QQuickKeyEvent *>(_o);
        Q_UNUSED(_t)
        void *_v = _a[0];
        switch (_id) {
        case 6: _t->setAccepted(*reinterpret_cast< bool*>(_v)); break;
        default: break;
        }
    } else if (_c == QMetaObject::ResetProperty) {
    }
#endif // QT_NO_PROPERTIES
}

const QMetaObject QQuickKeyEvent::staticMetaObject = {
    { &QObject::staticMetaObject, qt_meta_stringdata_QQuickKeyEvent.data,
      qt_meta_data_QQuickKeyEvent,  qt_static_metacall, Q_NULLPTR, Q_NULLPTR}
};


const QMetaObject *QQuickKeyEvent::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->dynamicMetaObject() : &staticMetaObject;
}

void *QQuickKeyEvent::qt_metacast(const char *_clname)
{
    if (!_clname) return Q_NULLPTR;
    if (!strcmp(_clname, qt_meta_stringdata_QQuickKeyEvent.stringdata0))
        return static_cast<void*>(const_cast< QQuickKeyEvent*>(this));
    return QObject::qt_metacast(_clname);
}

int QQuickKeyEvent::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QObject::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 1)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 1;
    } else if (_c == QMetaObject::RegisterMethodArgumentMetaType) {
        if (_id < 1)
            *reinterpret_cast<int*>(_a[0]) = -1;
        _id -= 1;
    }
#ifndef QT_NO_PROPERTIES
   else if (_c == QMetaObject::ReadProperty || _c == QMetaObject::WriteProperty
            || _c == QMetaObject::ResetProperty || _c == QMetaObject::RegisterPropertyMetaType) {
        qt_static_metacall(this, _c, _id, _a);
        _id -= 7;
    } else if (_c == QMetaObject::QueryPropertyDesignable) {
        _id -= 7;
    } else if (_c == QMetaObject::QueryPropertyScriptable) {
        _id -= 7;
    } else if (_c == QMetaObject::QueryPropertyStored) {
        _id -= 7;
    } else if (_c == QMetaObject::QueryPropertyEditable) {
        _id -= 7;
    } else if (_c == QMetaObject::QueryPropertyUser) {
        _id -= 7;
    }
#endif // QT_NO_PROPERTIES
    return _id;
}
struct qt_meta_stringdata_QQuickMouseEvent_t {
    QByteArrayData data[9];
    char stringdata0[71];
};
#define QT_MOC_LITERAL(idx, ofs, len) \
    Q_STATIC_BYTE_ARRAY_DATA_HEADER_INITIALIZER_WITH_OFFSET(len, \
    qptrdiff(offsetof(qt_meta_stringdata_QQuickMouseEvent_t, stringdata0) + ofs \
        - idx * sizeof(QByteArrayData)) \
    )
static const qt_meta_stringdata_QQuickMouseEvent_t qt_meta_stringdata_QQuickMouseEvent = {
    {
QT_MOC_LITERAL(0, 0, 16), // "QQuickMouseEvent"
QT_MOC_LITERAL(1, 17, 1), // "x"
QT_MOC_LITERAL(2, 19, 1), // "y"
QT_MOC_LITERAL(3, 21, 6), // "button"
QT_MOC_LITERAL(4, 28, 7), // "buttons"
QT_MOC_LITERAL(5, 36, 9), // "modifiers"
QT_MOC_LITERAL(6, 46, 7), // "wasHeld"
QT_MOC_LITERAL(7, 54, 7), // "isClick"
QT_MOC_LITERAL(8, 62, 8) // "accepted"

    },
    "QQuickMouseEvent\0x\0y\0button\0buttons\0"
    "modifiers\0wasHeld\0isClick\0accepted"
};
#undef QT_MOC_LITERAL

static const uint qt_meta_data_QQuickMouseEvent[] = {

 // content:
       7,       // revision
       0,       // classname
       0,    0, // classinfo
       0,    0, // methods
       8,   14, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       0,       // signalCount

 // properties: name, type, flags
       1, QMetaType::QReal, 0x00095001,
       2, QMetaType::QReal, 0x00095001,
       3, QMetaType::Int, 0x00095001,
       4, QMetaType::Int, 0x00095001,
       5, QMetaType::Int, 0x00095001,
       6, QMetaType::Bool, 0x00095001,
       7, QMetaType::Bool, 0x00095001,
       8, QMetaType::Bool, 0x00095103,

       0        // eod
};

void QQuickMouseEvent::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{

#ifndef QT_NO_PROPERTIES
    if (_c == QMetaObject::ReadProperty) {
        QQuickMouseEvent *_t = static_cast<QQuickMouseEvent *>(_o);
        Q_UNUSED(_t)
        void *_v = _a[0];
        switch (_id) {
        case 0: *reinterpret_cast< qreal*>(_v) = _t->x(); break;
        case 1: *reinterpret_cast< qreal*>(_v) = _t->y(); break;
        case 2: *reinterpret_cast< int*>(_v) = _t->button(); break;
        case 3: *reinterpret_cast< int*>(_v) = _t->buttons(); break;
        case 4: *reinterpret_cast< int*>(_v) = _t->modifiers(); break;
        case 5: *reinterpret_cast< bool*>(_v) = _t->wasHeld(); break;
        case 6: *reinterpret_cast< bool*>(_v) = _t->isClick(); break;
        case 7: *reinterpret_cast< bool*>(_v) = _t->isAccepted(); break;
        default: break;
        }
    } else if (_c == QMetaObject::WriteProperty) {
        QQuickMouseEvent *_t = static_cast<QQuickMouseEvent *>(_o);
        Q_UNUSED(_t)
        void *_v = _a[0];
        switch (_id) {
        case 7: _t->setAccepted(*reinterpret_cast< bool*>(_v)); break;
        default: break;
        }
    } else if (_c == QMetaObject::ResetProperty) {
    }
#endif // QT_NO_PROPERTIES
    Q_UNUSED(_o);
    Q_UNUSED(_id);
    Q_UNUSED(_c);
    Q_UNUSED(_a);
}

const QMetaObject QQuickMouseEvent::staticMetaObject = {
    { &QObject::staticMetaObject, qt_meta_stringdata_QQuickMouseEvent.data,
      qt_meta_data_QQuickMouseEvent,  qt_static_metacall, Q_NULLPTR, Q_NULLPTR}
};


const QMetaObject *QQuickMouseEvent::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->dynamicMetaObject() : &staticMetaObject;
}

void *QQuickMouseEvent::qt_metacast(const char *_clname)
{
    if (!_clname) return Q_NULLPTR;
    if (!strcmp(_clname, qt_meta_stringdata_QQuickMouseEvent.stringdata0))
        return static_cast<void*>(const_cast< QQuickMouseEvent*>(this));
    return QObject::qt_metacast(_clname);
}

int QQuickMouseEvent::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QObject::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    
#ifndef QT_NO_PROPERTIES
   if (_c == QMetaObject::ReadProperty || _c == QMetaObject::WriteProperty
            || _c == QMetaObject::ResetProperty || _c == QMetaObject::RegisterPropertyMetaType) {
        qt_static_metacall(this, _c, _id, _a);
        _id -= 8;
    } else if (_c == QMetaObject::QueryPropertyDesignable) {
        _id -= 8;
    } else if (_c == QMetaObject::QueryPropertyScriptable) {
        _id -= 8;
    } else if (_c == QMetaObject::QueryPropertyStored) {
        _id -= 8;
    } else if (_c == QMetaObject::QueryPropertyEditable) {
        _id -= 8;
    } else if (_c == QMetaObject::QueryPropertyUser) {
        _id -= 8;
    }
#endif // QT_NO_PROPERTIES
    return _id;
}
struct qt_meta_stringdata_QQuickWheelEvent_t {
    QByteArrayData data[8];
    char stringdata0[70];
};
#define QT_MOC_LITERAL(idx, ofs, len) \
    Q_STATIC_BYTE_ARRAY_DATA_HEADER_INITIALIZER_WITH_OFFSET(len, \
    qptrdiff(offsetof(qt_meta_stringdata_QQuickWheelEvent_t, stringdata0) + ofs \
        - idx * sizeof(QByteArrayData)) \
    )
static const qt_meta_stringdata_QQuickWheelEvent_t qt_meta_stringdata_QQuickWheelEvent = {
    {
QT_MOC_LITERAL(0, 0, 16), // "QQuickWheelEvent"
QT_MOC_LITERAL(1, 17, 1), // "x"
QT_MOC_LITERAL(2, 19, 1), // "y"
QT_MOC_LITERAL(3, 21, 10), // "angleDelta"
QT_MOC_LITERAL(4, 32, 10), // "pixelDelta"
QT_MOC_LITERAL(5, 43, 7), // "buttons"
QT_MOC_LITERAL(6, 51, 9), // "modifiers"
QT_MOC_LITERAL(7, 61, 8) // "accepted"

    },
    "QQuickWheelEvent\0x\0y\0angleDelta\0"
    "pixelDelta\0buttons\0modifiers\0accepted"
};
#undef QT_MOC_LITERAL

static const uint qt_meta_data_QQuickWheelEvent[] = {

 // content:
       7,       // revision
       0,       // classname
       0,    0, // classinfo
       0,    0, // methods
       7,   14, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       0,       // signalCount

 // properties: name, type, flags
       1, QMetaType::QReal, 0x00095001,
       2, QMetaType::QReal, 0x00095001,
       3, QMetaType::QPoint, 0x00095001,
       4, QMetaType::QPoint, 0x00095001,
       5, QMetaType::Int, 0x00095001,
       6, QMetaType::Int, 0x00095001,
       7, QMetaType::Bool, 0x00095103,

       0        // eod
};

void QQuickWheelEvent::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{

#ifndef QT_NO_PROPERTIES
    if (_c == QMetaObject::ReadProperty) {
        QQuickWheelEvent *_t = static_cast<QQuickWheelEvent *>(_o);
        Q_UNUSED(_t)
        void *_v = _a[0];
        switch (_id) {
        case 0: *reinterpret_cast< qreal*>(_v) = _t->x(); break;
        case 1: *reinterpret_cast< qreal*>(_v) = _t->y(); break;
        case 2: *reinterpret_cast< QPoint*>(_v) = _t->angleDelta(); break;
        case 3: *reinterpret_cast< QPoint*>(_v) = _t->pixelDelta(); break;
        case 4: *reinterpret_cast< int*>(_v) = _t->buttons(); break;
        case 5: *reinterpret_cast< int*>(_v) = _t->modifiers(); break;
        case 6: *reinterpret_cast< bool*>(_v) = _t->isAccepted(); break;
        default: break;
        }
    } else if (_c == QMetaObject::WriteProperty) {
        QQuickWheelEvent *_t = static_cast<QQuickWheelEvent *>(_o);
        Q_UNUSED(_t)
        void *_v = _a[0];
        switch (_id) {
        case 6: _t->setAccepted(*reinterpret_cast< bool*>(_v)); break;
        default: break;
        }
    } else if (_c == QMetaObject::ResetProperty) {
    }
#endif // QT_NO_PROPERTIES
    Q_UNUSED(_o);
    Q_UNUSED(_id);
    Q_UNUSED(_c);
    Q_UNUSED(_a);
}

const QMetaObject QQuickWheelEvent::staticMetaObject = {
    { &QObject::staticMetaObject, qt_meta_stringdata_QQuickWheelEvent.data,
      qt_meta_data_QQuickWheelEvent,  qt_static_metacall, Q_NULLPTR, Q_NULLPTR}
};


const QMetaObject *QQuickWheelEvent::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->dynamicMetaObject() : &staticMetaObject;
}

void *QQuickWheelEvent::qt_metacast(const char *_clname)
{
    if (!_clname) return Q_NULLPTR;
    if (!strcmp(_clname, qt_meta_stringdata_QQuickWheelEvent.stringdata0))
        return static_cast<void*>(const_cast< QQuickWheelEvent*>(this));
    return QObject::qt_metacast(_clname);
}

int QQuickWheelEvent::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QObject::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    
#ifndef QT_NO_PROPERTIES
   if (_c == QMetaObject::ReadProperty || _c == QMetaObject::WriteProperty
            || _c == QMetaObject::ResetProperty || _c == QMetaObject::RegisterPropertyMetaType) {
        qt_static_metacall(this, _c, _id, _a);
        _id -= 7;
    } else if (_c == QMetaObject::QueryPropertyDesignable) {
        _id -= 7;
    } else if (_c == QMetaObject::QueryPropertyScriptable) {
        _id -= 7;
    } else if (_c == QMetaObject::QueryPropertyStored) {
        _id -= 7;
    } else if (_c == QMetaObject::QueryPropertyEditable) {
        _id -= 7;
    } else if (_c == QMetaObject::QueryPropertyUser) {
        _id -= 7;
    }
#endif // QT_NO_PROPERTIES
    return _id;
}
QT_END_MOC_NAMESPACE
