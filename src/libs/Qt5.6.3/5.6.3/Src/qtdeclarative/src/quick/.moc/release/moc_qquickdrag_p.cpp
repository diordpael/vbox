/****************************************************************************
** Meta object code from reading C++ file 'qquickdrag_p.h'
**
** Created by: The Qt Meta Object Compiler version 67 (Qt 5.6.3)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "../../items/qquickdrag_p.h"
#include <QtCore/qbytearray.h>
#include <QtCore/qmetatype.h>
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'qquickdrag_p.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 67
#error "This file was generated using the moc from 5.6.3. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
struct qt_meta_stringdata_QQuickDragMimeData_t {
    QByteArrayData data[1];
    char stringdata0[19];
};
#define QT_MOC_LITERAL(idx, ofs, len) \
    Q_STATIC_BYTE_ARRAY_DATA_HEADER_INITIALIZER_WITH_OFFSET(len, \
    qptrdiff(offsetof(qt_meta_stringdata_QQuickDragMimeData_t, stringdata0) + ofs \
        - idx * sizeof(QByteArrayData)) \
    )
static const qt_meta_stringdata_QQuickDragMimeData_t qt_meta_stringdata_QQuickDragMimeData = {
    {
QT_MOC_LITERAL(0, 0, 18) // "QQuickDragMimeData"

    },
    "QQuickDragMimeData"
};
#undef QT_MOC_LITERAL

static const uint qt_meta_data_QQuickDragMimeData[] = {

 // content:
       7,       // revision
       0,       // classname
       0,    0, // classinfo
       0,    0, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       0,       // signalCount

       0        // eod
};

void QQuickDragMimeData::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    Q_UNUSED(_o);
    Q_UNUSED(_id);
    Q_UNUSED(_c);
    Q_UNUSED(_a);
}

const QMetaObject QQuickDragMimeData::staticMetaObject = {
    { &QMimeData::staticMetaObject, qt_meta_stringdata_QQuickDragMimeData.data,
      qt_meta_data_QQuickDragMimeData,  qt_static_metacall, Q_NULLPTR, Q_NULLPTR}
};


const QMetaObject *QQuickDragMimeData::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->dynamicMetaObject() : &staticMetaObject;
}

void *QQuickDragMimeData::qt_metacast(const char *_clname)
{
    if (!_clname) return Q_NULLPTR;
    if (!strcmp(_clname, qt_meta_stringdata_QQuickDragMimeData.stringdata0))
        return static_cast<void*>(const_cast< QQuickDragMimeData*>(this));
    return QMimeData::qt_metacast(_clname);
}

int QQuickDragMimeData::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QMimeData::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    return _id;
}
struct qt_meta_stringdata_QQuickDrag_t {
    QByteArrayData data[32];
    char stringdata0[342];
};
#define QT_MOC_LITERAL(idx, ofs, len) \
    Q_STATIC_BYTE_ARRAY_DATA_HEADER_INITIALIZER_WITH_OFFSET(len, \
    qptrdiff(offsetof(qt_meta_stringdata_QQuickDrag_t, stringdata0) + ofs \
        - idx * sizeof(QByteArrayData)) \
    )
static const qt_meta_stringdata_QQuickDrag_t qt_meta_stringdata_QQuickDrag = {
    {
QT_MOC_LITERAL(0, 0, 10), // "QQuickDrag"
QT_MOC_LITERAL(1, 11, 13), // "targetChanged"
QT_MOC_LITERAL(2, 25, 0), // ""
QT_MOC_LITERAL(3, 26, 11), // "axisChanged"
QT_MOC_LITERAL(4, 38, 15), // "minimumXChanged"
QT_MOC_LITERAL(5, 54, 15), // "maximumXChanged"
QT_MOC_LITERAL(6, 70, 15), // "minimumYChanged"
QT_MOC_LITERAL(7, 86, 15), // "maximumYChanged"
QT_MOC_LITERAL(8, 102, 13), // "activeChanged"
QT_MOC_LITERAL(9, 116, 21), // "filterChildrenChanged"
QT_MOC_LITERAL(10, 138, 15), // "smoothedChanged"
QT_MOC_LITERAL(11, 154, 16), // "thresholdChanged"
QT_MOC_LITERAL(12, 171, 6), // "target"
QT_MOC_LITERAL(13, 178, 11), // "QQuickItem*"
QT_MOC_LITERAL(14, 190, 4), // "axis"
QT_MOC_LITERAL(15, 195, 4), // "Axis"
QT_MOC_LITERAL(16, 200, 8), // "minimumX"
QT_MOC_LITERAL(17, 209, 8), // "maximumX"
QT_MOC_LITERAL(18, 218, 8), // "minimumY"
QT_MOC_LITERAL(19, 227, 8), // "maximumY"
QT_MOC_LITERAL(20, 236, 6), // "active"
QT_MOC_LITERAL(21, 243, 14), // "filterChildren"
QT_MOC_LITERAL(22, 258, 8), // "smoothed"
QT_MOC_LITERAL(23, 267, 9), // "threshold"
QT_MOC_LITERAL(24, 277, 8), // "DragType"
QT_MOC_LITERAL(25, 286, 4), // "None"
QT_MOC_LITERAL(26, 291, 9), // "Automatic"
QT_MOC_LITERAL(27, 301, 8), // "Internal"
QT_MOC_LITERAL(28, 310, 5), // "XAxis"
QT_MOC_LITERAL(29, 316, 5), // "YAxis"
QT_MOC_LITERAL(30, 322, 9), // "XAndYAxis"
QT_MOC_LITERAL(31, 332, 9) // "XandYAxis"

    },
    "QQuickDrag\0targetChanged\0\0axisChanged\0"
    "minimumXChanged\0maximumXChanged\0"
    "minimumYChanged\0maximumYChanged\0"
    "activeChanged\0filterChildrenChanged\0"
    "smoothedChanged\0thresholdChanged\0"
    "target\0QQuickItem*\0axis\0Axis\0minimumX\0"
    "maximumX\0minimumY\0maximumY\0active\0"
    "filterChildren\0smoothed\0threshold\0"
    "DragType\0None\0Automatic\0Internal\0XAxis\0"
    "YAxis\0XAndYAxis\0XandYAxis"
};
#undef QT_MOC_LITERAL

static const uint qt_meta_data_QQuickDrag[] = {

 // content:
       7,       // revision
       0,       // classname
       0,    0, // classinfo
      10,   14, // methods
      10,   74, // properties
       2,  114, // enums/sets
       0,    0, // constructors
       0,       // flags
      10,       // signalCount

 // signals: name, argc, parameters, tag, flags
       1,    0,   64,    2, 0x06 /* Public */,
       3,    0,   65,    2, 0x06 /* Public */,
       4,    0,   66,    2, 0x06 /* Public */,
       5,    0,   67,    2, 0x06 /* Public */,
       6,    0,   68,    2, 0x06 /* Public */,
       7,    0,   69,    2, 0x06 /* Public */,
       8,    0,   70,    2, 0x06 /* Public */,
       9,    0,   71,    2, 0x06 /* Public */,
      10,    0,   72,    2, 0x06 /* Public */,
      11,    0,   73,    2, 0x06 /* Public */,

 // signals: parameters
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,

 // properties: name, type, flags
      12, 0x80000000 | 13, 0x0049510f,
      14, 0x80000000 | 15, 0x0049510b,
      16, QMetaType::QReal, 0x00495003,
      17, QMetaType::QReal, 0x00495003,
      18, QMetaType::QReal, 0x00495003,
      19, QMetaType::QReal, 0x00495003,
      20, QMetaType::Bool, 0x00495001,
      21, QMetaType::Bool, 0x00495103,
      22, QMetaType::Bool, 0x00495103,
      23, QMetaType::QReal, 0x00495107,

 // properties: notify_signal_id
       0,
       1,
       2,
       3,
       4,
       5,
       6,
       7,
       8,
       9,

 // enums: name, flags, count, data
      24, 0x0,    3,  122,
      15, 0x0,    4,  128,

 // enum data: key, value
      25, uint(QQuickDrag::None),
      26, uint(QQuickDrag::Automatic),
      27, uint(QQuickDrag::Internal),
      28, uint(QQuickDrag::XAxis),
      29, uint(QQuickDrag::YAxis),
      30, uint(QQuickDrag::XAndYAxis),
      31, uint(QQuickDrag::XandYAxis),

       0        // eod
};

void QQuickDrag::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        QQuickDrag *_t = static_cast<QQuickDrag *>(_o);
        Q_UNUSED(_t)
        switch (_id) {
        case 0: _t->targetChanged(); break;
        case 1: _t->axisChanged(); break;
        case 2: _t->minimumXChanged(); break;
        case 3: _t->maximumXChanged(); break;
        case 4: _t->minimumYChanged(); break;
        case 5: _t->maximumYChanged(); break;
        case 6: _t->activeChanged(); break;
        case 7: _t->filterChildrenChanged(); break;
        case 8: _t->smoothedChanged(); break;
        case 9: _t->thresholdChanged(); break;
        default: ;
        }
    } else if (_c == QMetaObject::IndexOfMethod) {
        int *result = reinterpret_cast<int *>(_a[0]);
        void **func = reinterpret_cast<void **>(_a[1]);
        {
            typedef void (QQuickDrag::*_t)();
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&QQuickDrag::targetChanged)) {
                *result = 0;
                return;
            }
        }
        {
            typedef void (QQuickDrag::*_t)();
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&QQuickDrag::axisChanged)) {
                *result = 1;
                return;
            }
        }
        {
            typedef void (QQuickDrag::*_t)();
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&QQuickDrag::minimumXChanged)) {
                *result = 2;
                return;
            }
        }
        {
            typedef void (QQuickDrag::*_t)();
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&QQuickDrag::maximumXChanged)) {
                *result = 3;
                return;
            }
        }
        {
            typedef void (QQuickDrag::*_t)();
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&QQuickDrag::minimumYChanged)) {
                *result = 4;
                return;
            }
        }
        {
            typedef void (QQuickDrag::*_t)();
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&QQuickDrag::maximumYChanged)) {
                *result = 5;
                return;
            }
        }
        {
            typedef void (QQuickDrag::*_t)();
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&QQuickDrag::activeChanged)) {
                *result = 6;
                return;
            }
        }
        {
            typedef void (QQuickDrag::*_t)();
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&QQuickDrag::filterChildrenChanged)) {
                *result = 7;
                return;
            }
        }
        {
            typedef void (QQuickDrag::*_t)();
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&QQuickDrag::smoothedChanged)) {
                *result = 8;
                return;
            }
        }
        {
            typedef void (QQuickDrag::*_t)();
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&QQuickDrag::thresholdChanged)) {
                *result = 9;
                return;
            }
        }
    } else if (_c == QMetaObject::RegisterPropertyMetaType) {
        switch (_id) {
        default: *reinterpret_cast<int*>(_a[0]) = -1; break;
        case 0:
            *reinterpret_cast<int*>(_a[0]) = qRegisterMetaType< QQuickItem* >(); break;
        }
    }

#ifndef QT_NO_PROPERTIES
    else if (_c == QMetaObject::ReadProperty) {
        QQuickDrag *_t = static_cast<QQuickDrag *>(_o);
        Q_UNUSED(_t)
        void *_v = _a[0];
        switch (_id) {
        case 0: *reinterpret_cast< QQuickItem**>(_v) = _t->target(); break;
        case 1: *reinterpret_cast< Axis*>(_v) = _t->axis(); break;
        case 2: *reinterpret_cast< qreal*>(_v) = _t->xmin(); break;
        case 3: *reinterpret_cast< qreal*>(_v) = _t->xmax(); break;
        case 4: *reinterpret_cast< qreal*>(_v) = _t->ymin(); break;
        case 5: *reinterpret_cast< qreal*>(_v) = _t->ymax(); break;
        case 6: *reinterpret_cast< bool*>(_v) = _t->active(); break;
        case 7: *reinterpret_cast< bool*>(_v) = _t->filterChildren(); break;
        case 8: *reinterpret_cast< bool*>(_v) = _t->smoothed(); break;
        case 9: *reinterpret_cast< qreal*>(_v) = _t->threshold(); break;
        default: break;
        }
    } else if (_c == QMetaObject::WriteProperty) {
        QQuickDrag *_t = static_cast<QQuickDrag *>(_o);
        Q_UNUSED(_t)
        void *_v = _a[0];
        switch (_id) {
        case 0: _t->setTarget(*reinterpret_cast< QQuickItem**>(_v)); break;
        case 1: _t->setAxis(*reinterpret_cast< Axis*>(_v)); break;
        case 2: _t->setXmin(*reinterpret_cast< qreal*>(_v)); break;
        case 3: _t->setXmax(*reinterpret_cast< qreal*>(_v)); break;
        case 4: _t->setYmin(*reinterpret_cast< qreal*>(_v)); break;
        case 5: _t->setYmax(*reinterpret_cast< qreal*>(_v)); break;
        case 7: _t->setFilterChildren(*reinterpret_cast< bool*>(_v)); break;
        case 8: _t->setSmoothed(*reinterpret_cast< bool*>(_v)); break;
        case 9: _t->setThreshold(*reinterpret_cast< qreal*>(_v)); break;
        default: break;
        }
    } else if (_c == QMetaObject::ResetProperty) {
        QQuickDrag *_t = static_cast<QQuickDrag *>(_o);
        Q_UNUSED(_t)
        switch (_id) {
        case 0: _t->resetTarget(); break;
        case 9: _t->resetThreshold(); break;
        default: break;
        }
    }
#endif // QT_NO_PROPERTIES
}

const QMetaObject QQuickDrag::staticMetaObject = {
    { &QObject::staticMetaObject, qt_meta_stringdata_QQuickDrag.data,
      qt_meta_data_QQuickDrag,  qt_static_metacall, Q_NULLPTR, Q_NULLPTR}
};


const QMetaObject *QQuickDrag::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->dynamicMetaObject() : &staticMetaObject;
}

void *QQuickDrag::qt_metacast(const char *_clname)
{
    if (!_clname) return Q_NULLPTR;
    if (!strcmp(_clname, qt_meta_stringdata_QQuickDrag.stringdata0))
        return static_cast<void*>(const_cast< QQuickDrag*>(this));
    return QObject::qt_metacast(_clname);
}

int QQuickDrag::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QObject::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 10)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 10;
    } else if (_c == QMetaObject::RegisterMethodArgumentMetaType) {
        if (_id < 10)
            *reinterpret_cast<int*>(_a[0]) = -1;
        _id -= 10;
    }
#ifndef QT_NO_PROPERTIES
   else if (_c == QMetaObject::ReadProperty || _c == QMetaObject::WriteProperty
            || _c == QMetaObject::ResetProperty || _c == QMetaObject::RegisterPropertyMetaType) {
        qt_static_metacall(this, _c, _id, _a);
        _id -= 10;
    } else if (_c == QMetaObject::QueryPropertyDesignable) {
        _id -= 10;
    } else if (_c == QMetaObject::QueryPropertyScriptable) {
        _id -= 10;
    } else if (_c == QMetaObject::QueryPropertyStored) {
        _id -= 10;
    } else if (_c == QMetaObject::QueryPropertyEditable) {
        _id -= 10;
    } else if (_c == QMetaObject::QueryPropertyUser) {
        _id -= 10;
    }
#endif // QT_NO_PROPERTIES
    return _id;
}

// SIGNAL 0
void QQuickDrag::targetChanged()
{
    QMetaObject::activate(this, &staticMetaObject, 0, Q_NULLPTR);
}

// SIGNAL 1
void QQuickDrag::axisChanged()
{
    QMetaObject::activate(this, &staticMetaObject, 1, Q_NULLPTR);
}

// SIGNAL 2
void QQuickDrag::minimumXChanged()
{
    QMetaObject::activate(this, &staticMetaObject, 2, Q_NULLPTR);
}

// SIGNAL 3
void QQuickDrag::maximumXChanged()
{
    QMetaObject::activate(this, &staticMetaObject, 3, Q_NULLPTR);
}

// SIGNAL 4
void QQuickDrag::minimumYChanged()
{
    QMetaObject::activate(this, &staticMetaObject, 4, Q_NULLPTR);
}

// SIGNAL 5
void QQuickDrag::maximumYChanged()
{
    QMetaObject::activate(this, &staticMetaObject, 5, Q_NULLPTR);
}

// SIGNAL 6
void QQuickDrag::activeChanged()
{
    QMetaObject::activate(this, &staticMetaObject, 6, Q_NULLPTR);
}

// SIGNAL 7
void QQuickDrag::filterChildrenChanged()
{
    QMetaObject::activate(this, &staticMetaObject, 7, Q_NULLPTR);
}

// SIGNAL 8
void QQuickDrag::smoothedChanged()
{
    QMetaObject::activate(this, &staticMetaObject, 8, Q_NULLPTR);
}

// SIGNAL 9
void QQuickDrag::thresholdChanged()
{
    QMetaObject::activate(this, &staticMetaObject, 9, Q_NULLPTR);
}
struct qt_meta_stringdata_QQuickDragAttached_t {
    QByteArrayData data[31];
    char stringdata0[383];
};
#define QT_MOC_LITERAL(idx, ofs, len) \
    Q_STATIC_BYTE_ARRAY_DATA_HEADER_INITIALIZER_WITH_OFFSET(len, \
    qptrdiff(offsetof(qt_meta_stringdata_QQuickDragAttached_t, stringdata0) + ofs \
        - idx * sizeof(QByteArrayData)) \
    )
static const qt_meta_stringdata_QQuickDragAttached_t qt_meta_stringdata_QQuickDragAttached = {
    {
QT_MOC_LITERAL(0, 0, 18), // "QQuickDragAttached"
QT_MOC_LITERAL(1, 19, 11), // "dragStarted"
QT_MOC_LITERAL(2, 31, 0), // ""
QT_MOC_LITERAL(3, 32, 12), // "dragFinished"
QT_MOC_LITERAL(4, 45, 14), // "Qt::DropAction"
QT_MOC_LITERAL(5, 60, 10), // "dropAction"
QT_MOC_LITERAL(6, 71, 13), // "activeChanged"
QT_MOC_LITERAL(7, 85, 13), // "sourceChanged"
QT_MOC_LITERAL(8, 99, 13), // "targetChanged"
QT_MOC_LITERAL(9, 113, 14), // "hotSpotChanged"
QT_MOC_LITERAL(10, 128, 11), // "keysChanged"
QT_MOC_LITERAL(11, 140, 15), // "mimeDataChanged"
QT_MOC_LITERAL(12, 156, 23), // "supportedActionsChanged"
QT_MOC_LITERAL(13, 180, 21), // "proposedActionChanged"
QT_MOC_LITERAL(14, 202, 15), // "dragTypeChanged"
QT_MOC_LITERAL(15, 218, 5), // "start"
QT_MOC_LITERAL(16, 224, 15), // "QQmlV4Function*"
QT_MOC_LITERAL(17, 240, 9), // "startDrag"
QT_MOC_LITERAL(18, 250, 6), // "cancel"
QT_MOC_LITERAL(19, 257, 4), // "drop"
QT_MOC_LITERAL(20, 262, 6), // "active"
QT_MOC_LITERAL(21, 269, 6), // "source"
QT_MOC_LITERAL(22, 276, 6), // "target"
QT_MOC_LITERAL(23, 283, 7), // "hotSpot"
QT_MOC_LITERAL(24, 291, 4), // "keys"
QT_MOC_LITERAL(25, 296, 8), // "mimeData"
QT_MOC_LITERAL(26, 305, 16), // "supportedActions"
QT_MOC_LITERAL(27, 322, 15), // "Qt::DropActions"
QT_MOC_LITERAL(28, 338, 14), // "proposedAction"
QT_MOC_LITERAL(29, 353, 8), // "dragType"
QT_MOC_LITERAL(30, 362, 20) // "QQuickDrag::DragType"

    },
    "QQuickDragAttached\0dragStarted\0\0"
    "dragFinished\0Qt::DropAction\0dropAction\0"
    "activeChanged\0sourceChanged\0targetChanged\0"
    "hotSpotChanged\0keysChanged\0mimeDataChanged\0"
    "supportedActionsChanged\0proposedActionChanged\0"
    "dragTypeChanged\0start\0QQmlV4Function*\0"
    "startDrag\0cancel\0drop\0active\0source\0"
    "target\0hotSpot\0keys\0mimeData\0"
    "supportedActions\0Qt::DropActions\0"
    "proposedAction\0dragType\0QQuickDrag::DragType"
};
#undef QT_MOC_LITERAL

static const uint qt_meta_data_QQuickDragAttached[] = {

 // content:
       7,       // revision
       0,       // classname
       0,    0, // classinfo
      15,   14, // methods
       9,  110, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
      11,       // signalCount

 // signals: name, argc, parameters, tag, flags
       1,    0,   89,    2, 0x06 /* Public */,
       3,    1,   90,    2, 0x06 /* Public */,
       6,    0,   93,    2, 0x06 /* Public */,
       7,    0,   94,    2, 0x06 /* Public */,
       8,    0,   95,    2, 0x06 /* Public */,
       9,    0,   96,    2, 0x06 /* Public */,
      10,    0,   97,    2, 0x06 /* Public */,
      11,    0,   98,    2, 0x06 /* Public */,
      12,    0,   99,    2, 0x06 /* Public */,
      13,    0,  100,    2, 0x06 /* Public */,
      14,    0,  101,    2, 0x06 /* Public */,

 // slots: name, argc, parameters, tag, flags
      15,    1,  102,    2, 0x0a /* Public */,
      17,    1,  105,    2, 0x0a /* Public */,
      18,    0,  108,    2, 0x0a /* Public */,

 // methods: name, argc, parameters, tag, flags
      19,    0,  109,    2, 0x02 /* Public */,

 // signals: parameters
    QMetaType::Void,
    QMetaType::Void, 0x80000000 | 4,    5,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,

 // slots: parameters
    QMetaType::Void, 0x80000000 | 16,    2,
    QMetaType::Void, 0x80000000 | 16,    2,
    QMetaType::Void,

 // methods: parameters
    QMetaType::Int,

 // properties: name, type, flags
      20, QMetaType::Bool, 0x00495103,
      21, QMetaType::QObjectStar, 0x00495107,
      22, QMetaType::QObjectStar, 0x00495001,
      23, QMetaType::QPointF, 0x00495103,
      24, QMetaType::QStringList, 0x00495103,
      25, QMetaType::QVariantMap, 0x00495103,
      26, 0x80000000 | 27, 0x0049510b,
      28, 0x80000000 | 4, 0x0049510b,
      29, 0x80000000 | 30, 0x0049510b,

 // properties: notify_signal_id
       2,
       3,
       4,
       5,
       6,
       7,
       8,
       9,
      10,

       0        // eod
};

void QQuickDragAttached::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        QQuickDragAttached *_t = static_cast<QQuickDragAttached *>(_o);
        Q_UNUSED(_t)
        switch (_id) {
        case 0: _t->dragStarted(); break;
        case 1: _t->dragFinished((*reinterpret_cast< Qt::DropAction(*)>(_a[1]))); break;
        case 2: _t->activeChanged(); break;
        case 3: _t->sourceChanged(); break;
        case 4: _t->targetChanged(); break;
        case 5: _t->hotSpotChanged(); break;
        case 6: _t->keysChanged(); break;
        case 7: _t->mimeDataChanged(); break;
        case 8: _t->supportedActionsChanged(); break;
        case 9: _t->proposedActionChanged(); break;
        case 10: _t->dragTypeChanged(); break;
        case 11: _t->start((*reinterpret_cast< QQmlV4Function*(*)>(_a[1]))); break;
        case 12: _t->startDrag((*reinterpret_cast< QQmlV4Function*(*)>(_a[1]))); break;
        case 13: _t->cancel(); break;
        case 14: { int _r = _t->drop();
            if (_a[0]) *reinterpret_cast< int*>(_a[0]) = _r; }  break;
        default: ;
        }
    } else if (_c == QMetaObject::IndexOfMethod) {
        int *result = reinterpret_cast<int *>(_a[0]);
        void **func = reinterpret_cast<void **>(_a[1]);
        {
            typedef void (QQuickDragAttached::*_t)();
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&QQuickDragAttached::dragStarted)) {
                *result = 0;
                return;
            }
        }
        {
            typedef void (QQuickDragAttached::*_t)(Qt::DropAction );
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&QQuickDragAttached::dragFinished)) {
                *result = 1;
                return;
            }
        }
        {
            typedef void (QQuickDragAttached::*_t)();
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&QQuickDragAttached::activeChanged)) {
                *result = 2;
                return;
            }
        }
        {
            typedef void (QQuickDragAttached::*_t)();
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&QQuickDragAttached::sourceChanged)) {
                *result = 3;
                return;
            }
        }
        {
            typedef void (QQuickDragAttached::*_t)();
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&QQuickDragAttached::targetChanged)) {
                *result = 4;
                return;
            }
        }
        {
            typedef void (QQuickDragAttached::*_t)();
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&QQuickDragAttached::hotSpotChanged)) {
                *result = 5;
                return;
            }
        }
        {
            typedef void (QQuickDragAttached::*_t)();
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&QQuickDragAttached::keysChanged)) {
                *result = 6;
                return;
            }
        }
        {
            typedef void (QQuickDragAttached::*_t)();
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&QQuickDragAttached::mimeDataChanged)) {
                *result = 7;
                return;
            }
        }
        {
            typedef void (QQuickDragAttached::*_t)();
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&QQuickDragAttached::supportedActionsChanged)) {
                *result = 8;
                return;
            }
        }
        {
            typedef void (QQuickDragAttached::*_t)();
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&QQuickDragAttached::proposedActionChanged)) {
                *result = 9;
                return;
            }
        }
        {
            typedef void (QQuickDragAttached::*_t)();
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&QQuickDragAttached::dragTypeChanged)) {
                *result = 10;
                return;
            }
        }
    }
#ifndef QT_NO_PROPERTIES
    else if (_c == QMetaObject::ReadProperty) {
        QQuickDragAttached *_t = static_cast<QQuickDragAttached *>(_o);
        Q_UNUSED(_t)
        void *_v = _a[0];
        switch (_id) {
        case 0: *reinterpret_cast< bool*>(_v) = _t->isActive(); break;
        case 1: *reinterpret_cast< QObject**>(_v) = _t->source(); break;
        case 2: *reinterpret_cast< QObject**>(_v) = _t->target(); break;
        case 3: *reinterpret_cast< QPointF*>(_v) = _t->hotSpot(); break;
        case 4: *reinterpret_cast< QStringList*>(_v) = _t->keys(); break;
        case 5: *reinterpret_cast< QVariantMap*>(_v) = _t->mimeData(); break;
        case 6: *reinterpret_cast< Qt::DropActions*>(_v) = _t->supportedActions(); break;
        case 7: *reinterpret_cast< Qt::DropAction*>(_v) = _t->proposedAction(); break;
        case 8: *reinterpret_cast< QQuickDrag::DragType*>(_v) = _t->dragType(); break;
        default: break;
        }
    } else if (_c == QMetaObject::WriteProperty) {
        QQuickDragAttached *_t = static_cast<QQuickDragAttached *>(_o);
        Q_UNUSED(_t)
        void *_v = _a[0];
        switch (_id) {
        case 0: _t->setActive(*reinterpret_cast< bool*>(_v)); break;
        case 1: _t->setSource(*reinterpret_cast< QObject**>(_v)); break;
        case 3: _t->setHotSpot(*reinterpret_cast< QPointF*>(_v)); break;
        case 4: _t->setKeys(*reinterpret_cast< QStringList*>(_v)); break;
        case 5: _t->setMimeData(*reinterpret_cast< QVariantMap*>(_v)); break;
        case 6: _t->setSupportedActions(*reinterpret_cast< Qt::DropActions*>(_v)); break;
        case 7: _t->setProposedAction(*reinterpret_cast< Qt::DropAction*>(_v)); break;
        case 8: _t->setDragType(*reinterpret_cast< QQuickDrag::DragType*>(_v)); break;
        default: break;
        }
    } else if (_c == QMetaObject::ResetProperty) {
        QQuickDragAttached *_t = static_cast<QQuickDragAttached *>(_o);
        Q_UNUSED(_t)
        switch (_id) {
        case 1: _t->resetSource(); break;
        default: break;
        }
    }
#endif // QT_NO_PROPERTIES
}

static const QMetaObject * const qt_meta_extradata_QQuickDragAttached[] = {
        &QQuickDrag::staticMetaObject,
    Q_NULLPTR
};

const QMetaObject QQuickDragAttached::staticMetaObject = {
    { &QObject::staticMetaObject, qt_meta_stringdata_QQuickDragAttached.data,
      qt_meta_data_QQuickDragAttached,  qt_static_metacall, qt_meta_extradata_QQuickDragAttached, Q_NULLPTR}
};


const QMetaObject *QQuickDragAttached::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->dynamicMetaObject() : &staticMetaObject;
}

void *QQuickDragAttached::qt_metacast(const char *_clname)
{
    if (!_clname) return Q_NULLPTR;
    if (!strcmp(_clname, qt_meta_stringdata_QQuickDragAttached.stringdata0))
        return static_cast<void*>(const_cast< QQuickDragAttached*>(this));
    return QObject::qt_metacast(_clname);
}

int QQuickDragAttached::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QObject::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 15)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 15;
    } else if (_c == QMetaObject::RegisterMethodArgumentMetaType) {
        if (_id < 15)
            *reinterpret_cast<int*>(_a[0]) = -1;
        _id -= 15;
    }
#ifndef QT_NO_PROPERTIES
   else if (_c == QMetaObject::ReadProperty || _c == QMetaObject::WriteProperty
            || _c == QMetaObject::ResetProperty || _c == QMetaObject::RegisterPropertyMetaType) {
        qt_static_metacall(this, _c, _id, _a);
        _id -= 9;
    } else if (_c == QMetaObject::QueryPropertyDesignable) {
        _id -= 9;
    } else if (_c == QMetaObject::QueryPropertyScriptable) {
        _id -= 9;
    } else if (_c == QMetaObject::QueryPropertyStored) {
        _id -= 9;
    } else if (_c == QMetaObject::QueryPropertyEditable) {
        _id -= 9;
    } else if (_c == QMetaObject::QueryPropertyUser) {
        _id -= 9;
    }
#endif // QT_NO_PROPERTIES
    return _id;
}

// SIGNAL 0
void QQuickDragAttached::dragStarted()
{
    QMetaObject::activate(this, &staticMetaObject, 0, Q_NULLPTR);
}

// SIGNAL 1
void QQuickDragAttached::dragFinished(Qt::DropAction _t1)
{
    void *_a[] = { Q_NULLPTR, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 1, _a);
}

// SIGNAL 2
void QQuickDragAttached::activeChanged()
{
    QMetaObject::activate(this, &staticMetaObject, 2, Q_NULLPTR);
}

// SIGNAL 3
void QQuickDragAttached::sourceChanged()
{
    QMetaObject::activate(this, &staticMetaObject, 3, Q_NULLPTR);
}

// SIGNAL 4
void QQuickDragAttached::targetChanged()
{
    QMetaObject::activate(this, &staticMetaObject, 4, Q_NULLPTR);
}

// SIGNAL 5
void QQuickDragAttached::hotSpotChanged()
{
    QMetaObject::activate(this, &staticMetaObject, 5, Q_NULLPTR);
}

// SIGNAL 6
void QQuickDragAttached::keysChanged()
{
    QMetaObject::activate(this, &staticMetaObject, 6, Q_NULLPTR);
}

// SIGNAL 7
void QQuickDragAttached::mimeDataChanged()
{
    QMetaObject::activate(this, &staticMetaObject, 7, Q_NULLPTR);
}

// SIGNAL 8
void QQuickDragAttached::supportedActionsChanged()
{
    QMetaObject::activate(this, &staticMetaObject, 8, Q_NULLPTR);
}

// SIGNAL 9
void QQuickDragAttached::proposedActionChanged()
{
    QMetaObject::activate(this, &staticMetaObject, 9, Q_NULLPTR);
}

// SIGNAL 10
void QQuickDragAttached::dragTypeChanged()
{
    QMetaObject::activate(this, &staticMetaObject, 10, Q_NULLPTR);
}
QT_END_MOC_NAMESPACE
