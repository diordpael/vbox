/****************************************************************************
** Meta object code from reading C++ file 'qquickflickable_p.h'
**
** Created by: The Qt Meta Object Compiler version 67 (Qt 5.6.3)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "../../items/qquickflickable_p.h"
#include <QtCore/qbytearray.h>
#include <QtCore/qmetatype.h>
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'qquickflickable_p.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 67
#error "This file was generated using the moc from 5.6.3. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
struct qt_meta_stringdata_QQuickFlickable_t {
    QByteArrayData data[106];
    char stringdata0[1706];
};
#define QT_MOC_LITERAL(idx, ofs, len) \
    Q_STATIC_BYTE_ARRAY_DATA_HEADER_INITIALIZER_WITH_OFFSET(len, \
    qptrdiff(offsetof(qt_meta_stringdata_QQuickFlickable_t, stringdata0) + ofs \
        - idx * sizeof(QByteArrayData)) \
    )
static const qt_meta_stringdata_QQuickFlickable_t qt_meta_stringdata_QQuickFlickable = {
    {
QT_MOC_LITERAL(0, 0, 15), // "QQuickFlickable"
QT_MOC_LITERAL(1, 16, 15), // "DefaultProperty"
QT_MOC_LITERAL(2, 32, 13), // "flickableData"
QT_MOC_LITERAL(3, 46, 19), // "contentWidthChanged"
QT_MOC_LITERAL(4, 66, 0), // ""
QT_MOC_LITERAL(5, 67, 20), // "contentHeightChanged"
QT_MOC_LITERAL(6, 88, 15), // "contentXChanged"
QT_MOC_LITERAL(7, 104, 15), // "contentYChanged"
QT_MOC_LITERAL(8, 120, 16), // "topMarginChanged"
QT_MOC_LITERAL(9, 137, 19), // "bottomMarginChanged"
QT_MOC_LITERAL(10, 157, 17), // "leftMarginChanged"
QT_MOC_LITERAL(11, 175, 18), // "rightMarginChanged"
QT_MOC_LITERAL(12, 194, 14), // "originYChanged"
QT_MOC_LITERAL(13, 209, 14), // "originXChanged"
QT_MOC_LITERAL(14, 224, 13), // "movingChanged"
QT_MOC_LITERAL(15, 238, 25), // "movingHorizontallyChanged"
QT_MOC_LITERAL(16, 264, 23), // "movingVerticallyChanged"
QT_MOC_LITERAL(17, 288, 15), // "flickingChanged"
QT_MOC_LITERAL(18, 304, 27), // "flickingHorizontallyChanged"
QT_MOC_LITERAL(19, 332, 25), // "flickingVerticallyChanged"
QT_MOC_LITERAL(20, 358, 15), // "draggingChanged"
QT_MOC_LITERAL(21, 374, 27), // "draggingHorizontallyChanged"
QT_MOC_LITERAL(22, 402, 25), // "draggingVerticallyChanged"
QT_MOC_LITERAL(23, 428, 25), // "horizontalVelocityChanged"
QT_MOC_LITERAL(24, 454, 23), // "verticalVelocityChanged"
QT_MOC_LITERAL(25, 478, 19), // "isAtBoundaryChanged"
QT_MOC_LITERAL(26, 498, 25), // "flickableDirectionChanged"
QT_MOC_LITERAL(27, 524, 18), // "interactiveChanged"
QT_MOC_LITERAL(28, 543, 21), // "boundsBehaviorChanged"
QT_MOC_LITERAL(29, 565, 14), // "reboundChanged"
QT_MOC_LITERAL(30, 580, 27), // "maximumFlickVelocityChanged"
QT_MOC_LITERAL(31, 608, 24), // "flickDecelerationChanged"
QT_MOC_LITERAL(32, 633, 17), // "pressDelayChanged"
QT_MOC_LITERAL(33, 651, 15), // "movementStarted"
QT_MOC_LITERAL(34, 667, 13), // "movementEnded"
QT_MOC_LITERAL(35, 681, 12), // "flickStarted"
QT_MOC_LITERAL(36, 694, 10), // "flickEnded"
QT_MOC_LITERAL(37, 705, 11), // "dragStarted"
QT_MOC_LITERAL(38, 717, 9), // "dragEnded"
QT_MOC_LITERAL(39, 727, 19), // "pixelAlignedChanged"
QT_MOC_LITERAL(40, 747, 16), // "movementStarting"
QT_MOC_LITERAL(41, 764, 14), // "movementEnding"
QT_MOC_LITERAL(42, 779, 15), // "hMovementEnding"
QT_MOC_LITERAL(43, 795, 15), // "vMovementEnding"
QT_MOC_LITERAL(44, 811, 25), // "velocityTimelineCompleted"
QT_MOC_LITERAL(45, 837, 17), // "timelineCompleted"
QT_MOC_LITERAL(46, 855, 13), // "resizeContent"
QT_MOC_LITERAL(47, 869, 1), // "w"
QT_MOC_LITERAL(48, 871, 1), // "h"
QT_MOC_LITERAL(49, 873, 6), // "center"
QT_MOC_LITERAL(50, 880, 14), // "returnToBounds"
QT_MOC_LITERAL(51, 895, 5), // "flick"
QT_MOC_LITERAL(52, 901, 9), // "xVelocity"
QT_MOC_LITERAL(53, 911, 9), // "yVelocity"
QT_MOC_LITERAL(54, 921, 11), // "cancelFlick"
QT_MOC_LITERAL(55, 933, 12), // "contentWidth"
QT_MOC_LITERAL(56, 946, 13), // "contentHeight"
QT_MOC_LITERAL(57, 960, 8), // "contentX"
QT_MOC_LITERAL(58, 969, 8), // "contentY"
QT_MOC_LITERAL(59, 978, 11), // "contentItem"
QT_MOC_LITERAL(60, 990, 11), // "QQuickItem*"
QT_MOC_LITERAL(61, 1002, 9), // "topMargin"
QT_MOC_LITERAL(62, 1012, 12), // "bottomMargin"
QT_MOC_LITERAL(63, 1025, 7), // "originY"
QT_MOC_LITERAL(64, 1033, 10), // "leftMargin"
QT_MOC_LITERAL(65, 1044, 11), // "rightMargin"
QT_MOC_LITERAL(66, 1056, 7), // "originX"
QT_MOC_LITERAL(67, 1064, 18), // "horizontalVelocity"
QT_MOC_LITERAL(68, 1083, 16), // "verticalVelocity"
QT_MOC_LITERAL(69, 1100, 14), // "boundsBehavior"
QT_MOC_LITERAL(70, 1115, 14), // "BoundsBehavior"
QT_MOC_LITERAL(71, 1130, 7), // "rebound"
QT_MOC_LITERAL(72, 1138, 17), // "QQuickTransition*"
QT_MOC_LITERAL(73, 1156, 20), // "maximumFlickVelocity"
QT_MOC_LITERAL(74, 1177, 17), // "flickDeceleration"
QT_MOC_LITERAL(75, 1195, 6), // "moving"
QT_MOC_LITERAL(76, 1202, 18), // "movingHorizontally"
QT_MOC_LITERAL(77, 1221, 16), // "movingVertically"
QT_MOC_LITERAL(78, 1238, 8), // "flicking"
QT_MOC_LITERAL(79, 1247, 20), // "flickingHorizontally"
QT_MOC_LITERAL(80, 1268, 18), // "flickingVertically"
QT_MOC_LITERAL(81, 1287, 8), // "dragging"
QT_MOC_LITERAL(82, 1296, 20), // "draggingHorizontally"
QT_MOC_LITERAL(83, 1317, 18), // "draggingVertically"
QT_MOC_LITERAL(84, 1336, 18), // "flickableDirection"
QT_MOC_LITERAL(85, 1355, 18), // "FlickableDirection"
QT_MOC_LITERAL(86, 1374, 11), // "interactive"
QT_MOC_LITERAL(87, 1386, 10), // "pressDelay"
QT_MOC_LITERAL(88, 1397, 6), // "atXEnd"
QT_MOC_LITERAL(89, 1404, 6), // "atYEnd"
QT_MOC_LITERAL(90, 1411, 12), // "atXBeginning"
QT_MOC_LITERAL(91, 1424, 12), // "atYBeginning"
QT_MOC_LITERAL(92, 1437, 11), // "visibleArea"
QT_MOC_LITERAL(93, 1449, 27), // "QQuickFlickableVisibleArea*"
QT_MOC_LITERAL(94, 1477, 12), // "pixelAligned"
QT_MOC_LITERAL(95, 1490, 25), // "QQmlListProperty<QObject>"
QT_MOC_LITERAL(96, 1516, 17), // "flickableChildren"
QT_MOC_LITERAL(97, 1534, 28), // "QQmlListProperty<QQuickItem>"
QT_MOC_LITERAL(98, 1563, 12), // "StopAtBounds"
QT_MOC_LITERAL(99, 1576, 14), // "DragOverBounds"
QT_MOC_LITERAL(100, 1591, 15), // "OvershootBounds"
QT_MOC_LITERAL(101, 1607, 22), // "DragAndOvershootBounds"
QT_MOC_LITERAL(102, 1630, 18), // "AutoFlickDirection"
QT_MOC_LITERAL(103, 1649, 15), // "HorizontalFlick"
QT_MOC_LITERAL(104, 1665, 13), // "VerticalFlick"
QT_MOC_LITERAL(105, 1679, 26) // "HorizontalAndVerticalFlick"

    },
    "QQuickFlickable\0DefaultProperty\0"
    "flickableData\0contentWidthChanged\0\0"
    "contentHeightChanged\0contentXChanged\0"
    "contentYChanged\0topMarginChanged\0"
    "bottomMarginChanged\0leftMarginChanged\0"
    "rightMarginChanged\0originYChanged\0"
    "originXChanged\0movingChanged\0"
    "movingHorizontallyChanged\0"
    "movingVerticallyChanged\0flickingChanged\0"
    "flickingHorizontallyChanged\0"
    "flickingVerticallyChanged\0draggingChanged\0"
    "draggingHorizontallyChanged\0"
    "draggingVerticallyChanged\0"
    "horizontalVelocityChanged\0"
    "verticalVelocityChanged\0isAtBoundaryChanged\0"
    "flickableDirectionChanged\0interactiveChanged\0"
    "boundsBehaviorChanged\0reboundChanged\0"
    "maximumFlickVelocityChanged\0"
    "flickDecelerationChanged\0pressDelayChanged\0"
    "movementStarted\0movementEnded\0"
    "flickStarted\0flickEnded\0dragStarted\0"
    "dragEnded\0pixelAlignedChanged\0"
    "movementStarting\0movementEnding\0"
    "hMovementEnding\0vMovementEnding\0"
    "velocityTimelineCompleted\0timelineCompleted\0"
    "resizeContent\0w\0h\0center\0returnToBounds\0"
    "flick\0xVelocity\0yVelocity\0cancelFlick\0"
    "contentWidth\0contentHeight\0contentX\0"
    "contentY\0contentItem\0QQuickItem*\0"
    "topMargin\0bottomMargin\0originY\0"
    "leftMargin\0rightMargin\0originX\0"
    "horizontalVelocity\0verticalVelocity\0"
    "boundsBehavior\0BoundsBehavior\0rebound\0"
    "QQuickTransition*\0maximumFlickVelocity\0"
    "flickDeceleration\0moving\0movingHorizontally\0"
    "movingVertically\0flicking\0"
    "flickingHorizontally\0flickingVertically\0"
    "dragging\0draggingHorizontally\0"
    "draggingVertically\0flickableDirection\0"
    "FlickableDirection\0interactive\0"
    "pressDelay\0atXEnd\0atYEnd\0atXBeginning\0"
    "atYBeginning\0visibleArea\0"
    "QQuickFlickableVisibleArea*\0pixelAligned\0"
    "QQmlListProperty<QObject>\0flickableChildren\0"
    "QQmlListProperty<QQuickItem>\0StopAtBounds\0"
    "DragOverBounds\0OvershootBounds\0"
    "DragAndOvershootBounds\0AutoFlickDirection\0"
    "HorizontalFlick\0VerticalFlick\0"
    "HorizontalAndVerticalFlick"
};
#undef QT_MOC_LITERAL

static const uint qt_meta_data_QQuickFlickable[] = {

 // content:
       7,       // revision
       0,       // classname
       1,   14, // classinfo
      45,   16, // methods
      37,  300, // properties
       2,  448, // enums/sets
       0,    0, // constructors
       0,       // flags
      36,       // signalCount

 // classinfo: key, value
       1,    2,

 // signals: name, argc, parameters, tag, flags
       3,    0,  241,    4, 0x06 /* Public */,
       5,    0,  242,    4, 0x06 /* Public */,
       6,    0,  243,    4, 0x06 /* Public */,
       7,    0,  244,    4, 0x06 /* Public */,
       8,    0,  245,    4, 0x06 /* Public */,
       9,    0,  246,    4, 0x06 /* Public */,
      10,    0,  247,    4, 0x06 /* Public */,
      11,    0,  248,    4, 0x06 /* Public */,
      12,    0,  249,    4, 0x06 /* Public */,
      13,    0,  250,    4, 0x06 /* Public */,
      14,    0,  251,    4, 0x06 /* Public */,
      15,    0,  252,    4, 0x06 /* Public */,
      16,    0,  253,    4, 0x06 /* Public */,
      17,    0,  254,    4, 0x06 /* Public */,
      18,    0,  255,    4, 0x06 /* Public */,
      19,    0,  256,    4, 0x06 /* Public */,
      20,    0,  257,    4, 0x06 /* Public */,
      21,    0,  258,    4, 0x06 /* Public */,
      22,    0,  259,    4, 0x06 /* Public */,
      23,    0,  260,    4, 0x06 /* Public */,
      24,    0,  261,    4, 0x06 /* Public */,
      25,    0,  262,    4, 0x06 /* Public */,
      26,    0,  263,    4, 0x06 /* Public */,
      27,    0,  264,    4, 0x06 /* Public */,
      28,    0,  265,    4, 0x06 /* Public */,
      29,    0,  266,    4, 0x06 /* Public */,
      30,    0,  267,    4, 0x06 /* Public */,
      31,    0,  268,    4, 0x06 /* Public */,
      32,    0,  269,    4, 0x06 /* Public */,
      33,    0,  270,    4, 0x06 /* Public */,
      34,    0,  271,    4, 0x06 /* Public */,
      35,    0,  272,    4, 0x06 /* Public */,
      36,    0,  273,    4, 0x06 /* Public */,
      37,    0,  274,    4, 0x06 /* Public */,
      38,    0,  275,    4, 0x06 /* Public */,
      39,    0,  276,    4, 0x06 /* Public */,

 // slots: name, argc, parameters, tag, flags
      40,    0,  277,    4, 0x09 /* Protected */,
      41,    0,  278,    4, 0x09 /* Protected */,
      41,    2,  279,    4, 0x09 /* Protected */,
      44,    0,  284,    4, 0x09 /* Protected */,
      45,    0,  285,    4, 0x09 /* Protected */,

 // methods: name, argc, parameters, tag, flags
      46,    3,  286,    4, 0x02 /* Public */,
      50,    0,  293,    4, 0x02 /* Public */,
      51,    2,  294,    4, 0x02 /* Public */,
      54,    0,  299,    4, 0x02 /* Public */,

 // signals: parameters
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,

 // slots: parameters
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void, QMetaType::Bool, QMetaType::Bool,   42,   43,
    QMetaType::Void,
    QMetaType::Void,

 // methods: parameters
    QMetaType::Void, QMetaType::QReal, QMetaType::QReal, QMetaType::QPointF,   47,   48,   49,
    QMetaType::Void,
    QMetaType::Void, QMetaType::QReal, QMetaType::QReal,   52,   53,
    QMetaType::Void,

 // properties: name, type, flags
      55, QMetaType::QReal, 0x00495103,
      56, QMetaType::QReal, 0x00495103,
      57, QMetaType::QReal, 0x00495103,
      58, QMetaType::QReal, 0x00495103,
      59, 0x80000000 | 60, 0x00095409,
      61, QMetaType::QReal, 0x00495103,
      62, QMetaType::QReal, 0x00495103,
      63, QMetaType::QReal, 0x00495001,
      64, QMetaType::QReal, 0x00495103,
      65, QMetaType::QReal, 0x00495103,
      66, QMetaType::QReal, 0x00495001,
      67, QMetaType::QReal, 0x00495001,
      68, QMetaType::QReal, 0x00495001,
      69, 0x80000000 | 70, 0x0049510b,
      71, 0x80000000 | 72, 0x0049510b,
      73, QMetaType::QReal, 0x00495103,
      74, QMetaType::QReal, 0x00495103,
      75, QMetaType::Bool, 0x00495001,
      76, QMetaType::Bool, 0x00495001,
      77, QMetaType::Bool, 0x00495001,
      78, QMetaType::Bool, 0x00495001,
      79, QMetaType::Bool, 0x00495001,
      80, QMetaType::Bool, 0x00495001,
      81, QMetaType::Bool, 0x00495001,
      82, QMetaType::Bool, 0x00495001,
      83, QMetaType::Bool, 0x00495001,
      84, 0x80000000 | 85, 0x0049510b,
      86, QMetaType::Bool, 0x00495103,
      87, QMetaType::Int, 0x00495103,
      88, QMetaType::Bool, 0x00495001,
      89, QMetaType::Bool, 0x00495001,
      90, QMetaType::Bool, 0x00495001,
      91, QMetaType::Bool, 0x00495001,
      92, 0x80000000 | 93, 0x00095409,
      94, QMetaType::Bool, 0x00495103,
       2, 0x80000000 | 95, 0x00095009,
      96, 0x80000000 | 97, 0x00095009,

 // properties: notify_signal_id
       0,
       1,
       2,
       3,
       0,
       4,
       5,
       8,
       6,
       7,
       9,
      19,
      20,
      24,
      25,
      26,
      27,
      10,
      11,
      12,
      13,
      14,
      15,
      16,
      17,
      18,
      22,
      23,
      28,
      21,
      21,
      21,
      21,
       0,
      35,
       0,
       0,

 // enums: name, flags, count, data
      70, 0x1,    4,  456,
      85, 0x0,    4,  464,

 // enum data: key, value
      98, uint(QQuickFlickable::StopAtBounds),
      99, uint(QQuickFlickable::DragOverBounds),
     100, uint(QQuickFlickable::OvershootBounds),
     101, uint(QQuickFlickable::DragAndOvershootBounds),
     102, uint(QQuickFlickable::AutoFlickDirection),
     103, uint(QQuickFlickable::HorizontalFlick),
     104, uint(QQuickFlickable::VerticalFlick),
     105, uint(QQuickFlickable::HorizontalAndVerticalFlick),

       0        // eod
};

void QQuickFlickable::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        QQuickFlickable *_t = static_cast<QQuickFlickable *>(_o);
        Q_UNUSED(_t)
        switch (_id) {
        case 0: _t->contentWidthChanged(); break;
        case 1: _t->contentHeightChanged(); break;
        case 2: _t->contentXChanged(); break;
        case 3: _t->contentYChanged(); break;
        case 4: _t->topMarginChanged(); break;
        case 5: _t->bottomMarginChanged(); break;
        case 6: _t->leftMarginChanged(); break;
        case 7: _t->rightMarginChanged(); break;
        case 8: _t->originYChanged(); break;
        case 9: _t->originXChanged(); break;
        case 10: _t->movingChanged(); break;
        case 11: _t->movingHorizontallyChanged(); break;
        case 12: _t->movingVerticallyChanged(); break;
        case 13: _t->flickingChanged(); break;
        case 14: _t->flickingHorizontallyChanged(); break;
        case 15: _t->flickingVerticallyChanged(); break;
        case 16: _t->draggingChanged(); break;
        case 17: _t->draggingHorizontallyChanged(); break;
        case 18: _t->draggingVerticallyChanged(); break;
        case 19: _t->horizontalVelocityChanged(); break;
        case 20: _t->verticalVelocityChanged(); break;
        case 21: _t->isAtBoundaryChanged(); break;
        case 22: _t->flickableDirectionChanged(); break;
        case 23: _t->interactiveChanged(); break;
        case 24: _t->boundsBehaviorChanged(); break;
        case 25: _t->reboundChanged(); break;
        case 26: _t->maximumFlickVelocityChanged(); break;
        case 27: _t->flickDecelerationChanged(); break;
        case 28: _t->pressDelayChanged(); break;
        case 29: _t->movementStarted(); break;
        case 30: _t->movementEnded(); break;
        case 31: _t->flickStarted(); break;
        case 32: _t->flickEnded(); break;
        case 33: _t->dragStarted(); break;
        case 34: _t->dragEnded(); break;
        case 35: _t->pixelAlignedChanged(); break;
        case 36: _t->movementStarting(); break;
        case 37: _t->movementEnding(); break;
        case 38: _t->movementEnding((*reinterpret_cast< bool(*)>(_a[1])),(*reinterpret_cast< bool(*)>(_a[2]))); break;
        case 39: _t->velocityTimelineCompleted(); break;
        case 40: _t->timelineCompleted(); break;
        case 41: _t->resizeContent((*reinterpret_cast< qreal(*)>(_a[1])),(*reinterpret_cast< qreal(*)>(_a[2])),(*reinterpret_cast< QPointF(*)>(_a[3]))); break;
        case 42: _t->returnToBounds(); break;
        case 43: _t->flick((*reinterpret_cast< qreal(*)>(_a[1])),(*reinterpret_cast< qreal(*)>(_a[2]))); break;
        case 44: _t->cancelFlick(); break;
        default: ;
        }
    } else if (_c == QMetaObject::IndexOfMethod) {
        int *result = reinterpret_cast<int *>(_a[0]);
        void **func = reinterpret_cast<void **>(_a[1]);
        {
            typedef void (QQuickFlickable::*_t)();
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&QQuickFlickable::contentWidthChanged)) {
                *result = 0;
                return;
            }
        }
        {
            typedef void (QQuickFlickable::*_t)();
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&QQuickFlickable::contentHeightChanged)) {
                *result = 1;
                return;
            }
        }
        {
            typedef void (QQuickFlickable::*_t)();
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&QQuickFlickable::contentXChanged)) {
                *result = 2;
                return;
            }
        }
        {
            typedef void (QQuickFlickable::*_t)();
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&QQuickFlickable::contentYChanged)) {
                *result = 3;
                return;
            }
        }
        {
            typedef void (QQuickFlickable::*_t)();
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&QQuickFlickable::topMarginChanged)) {
                *result = 4;
                return;
            }
        }
        {
            typedef void (QQuickFlickable::*_t)();
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&QQuickFlickable::bottomMarginChanged)) {
                *result = 5;
                return;
            }
        }
        {
            typedef void (QQuickFlickable::*_t)();
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&QQuickFlickable::leftMarginChanged)) {
                *result = 6;
                return;
            }
        }
        {
            typedef void (QQuickFlickable::*_t)();
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&QQuickFlickable::rightMarginChanged)) {
                *result = 7;
                return;
            }
        }
        {
            typedef void (QQuickFlickable::*_t)();
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&QQuickFlickable::originYChanged)) {
                *result = 8;
                return;
            }
        }
        {
            typedef void (QQuickFlickable::*_t)();
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&QQuickFlickable::originXChanged)) {
                *result = 9;
                return;
            }
        }
        {
            typedef void (QQuickFlickable::*_t)();
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&QQuickFlickable::movingChanged)) {
                *result = 10;
                return;
            }
        }
        {
            typedef void (QQuickFlickable::*_t)();
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&QQuickFlickable::movingHorizontallyChanged)) {
                *result = 11;
                return;
            }
        }
        {
            typedef void (QQuickFlickable::*_t)();
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&QQuickFlickable::movingVerticallyChanged)) {
                *result = 12;
                return;
            }
        }
        {
            typedef void (QQuickFlickable::*_t)();
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&QQuickFlickable::flickingChanged)) {
                *result = 13;
                return;
            }
        }
        {
            typedef void (QQuickFlickable::*_t)();
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&QQuickFlickable::flickingHorizontallyChanged)) {
                *result = 14;
                return;
            }
        }
        {
            typedef void (QQuickFlickable::*_t)();
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&QQuickFlickable::flickingVerticallyChanged)) {
                *result = 15;
                return;
            }
        }
        {
            typedef void (QQuickFlickable::*_t)();
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&QQuickFlickable::draggingChanged)) {
                *result = 16;
                return;
            }
        }
        {
            typedef void (QQuickFlickable::*_t)();
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&QQuickFlickable::draggingHorizontallyChanged)) {
                *result = 17;
                return;
            }
        }
        {
            typedef void (QQuickFlickable::*_t)();
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&QQuickFlickable::draggingVerticallyChanged)) {
                *result = 18;
                return;
            }
        }
        {
            typedef void (QQuickFlickable::*_t)();
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&QQuickFlickable::horizontalVelocityChanged)) {
                *result = 19;
                return;
            }
        }
        {
            typedef void (QQuickFlickable::*_t)();
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&QQuickFlickable::verticalVelocityChanged)) {
                *result = 20;
                return;
            }
        }
        {
            typedef void (QQuickFlickable::*_t)();
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&QQuickFlickable::isAtBoundaryChanged)) {
                *result = 21;
                return;
            }
        }
        {
            typedef void (QQuickFlickable::*_t)();
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&QQuickFlickable::flickableDirectionChanged)) {
                *result = 22;
                return;
            }
        }
        {
            typedef void (QQuickFlickable::*_t)();
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&QQuickFlickable::interactiveChanged)) {
                *result = 23;
                return;
            }
        }
        {
            typedef void (QQuickFlickable::*_t)();
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&QQuickFlickable::boundsBehaviorChanged)) {
                *result = 24;
                return;
            }
        }
        {
            typedef void (QQuickFlickable::*_t)();
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&QQuickFlickable::reboundChanged)) {
                *result = 25;
                return;
            }
        }
        {
            typedef void (QQuickFlickable::*_t)();
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&QQuickFlickable::maximumFlickVelocityChanged)) {
                *result = 26;
                return;
            }
        }
        {
            typedef void (QQuickFlickable::*_t)();
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&QQuickFlickable::flickDecelerationChanged)) {
                *result = 27;
                return;
            }
        }
        {
            typedef void (QQuickFlickable::*_t)();
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&QQuickFlickable::pressDelayChanged)) {
                *result = 28;
                return;
            }
        }
        {
            typedef void (QQuickFlickable::*_t)();
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&QQuickFlickable::movementStarted)) {
                *result = 29;
                return;
            }
        }
        {
            typedef void (QQuickFlickable::*_t)();
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&QQuickFlickable::movementEnded)) {
                *result = 30;
                return;
            }
        }
        {
            typedef void (QQuickFlickable::*_t)();
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&QQuickFlickable::flickStarted)) {
                *result = 31;
                return;
            }
        }
        {
            typedef void (QQuickFlickable::*_t)();
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&QQuickFlickable::flickEnded)) {
                *result = 32;
                return;
            }
        }
        {
            typedef void (QQuickFlickable::*_t)();
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&QQuickFlickable::dragStarted)) {
                *result = 33;
                return;
            }
        }
        {
            typedef void (QQuickFlickable::*_t)();
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&QQuickFlickable::dragEnded)) {
                *result = 34;
                return;
            }
        }
        {
            typedef void (QQuickFlickable::*_t)();
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&QQuickFlickable::pixelAlignedChanged)) {
                *result = 35;
                return;
            }
        }
    } else if (_c == QMetaObject::RegisterPropertyMetaType) {
        switch (_id) {
        default: *reinterpret_cast<int*>(_a[0]) = -1; break;
        case 35:
            *reinterpret_cast<int*>(_a[0]) = qRegisterMetaType< QQmlListProperty<QObject> >(); break;
        case 36:
            *reinterpret_cast<int*>(_a[0]) = qRegisterMetaType< QQmlListProperty<QQuickItem> >(); break;
        case 4:
            *reinterpret_cast<int*>(_a[0]) = qRegisterMetaType< QQuickItem* >(); break;
        }
    }

#ifndef QT_NO_PROPERTIES
    else if (_c == QMetaObject::ReadProperty) {
        QQuickFlickable *_t = static_cast<QQuickFlickable *>(_o);
        Q_UNUSED(_t)
        void *_v = _a[0];
        switch (_id) {
        case 0: *reinterpret_cast< qreal*>(_v) = _t->contentWidth(); break;
        case 1: *reinterpret_cast< qreal*>(_v) = _t->contentHeight(); break;
        case 2: *reinterpret_cast< qreal*>(_v) = _t->contentX(); break;
        case 3: *reinterpret_cast< qreal*>(_v) = _t->contentY(); break;
        case 4: *reinterpret_cast< QQuickItem**>(_v) = _t->contentItem(); break;
        case 5: *reinterpret_cast< qreal*>(_v) = _t->topMargin(); break;
        case 6: *reinterpret_cast< qreal*>(_v) = _t->bottomMargin(); break;
        case 7: *reinterpret_cast< qreal*>(_v) = _t->originY(); break;
        case 8: *reinterpret_cast< qreal*>(_v) = _t->leftMargin(); break;
        case 9: *reinterpret_cast< qreal*>(_v) = _t->rightMargin(); break;
        case 10: *reinterpret_cast< qreal*>(_v) = _t->originX(); break;
        case 11: *reinterpret_cast< qreal*>(_v) = _t->horizontalVelocity(); break;
        case 12: *reinterpret_cast< qreal*>(_v) = _t->verticalVelocity(); break;
        case 13: *reinterpret_cast<int*>(_v) = QFlag(_t->boundsBehavior()); break;
        case 14: *reinterpret_cast< QQuickTransition**>(_v) = _t->rebound(); break;
        case 15: *reinterpret_cast< qreal*>(_v) = _t->maximumFlickVelocity(); break;
        case 16: *reinterpret_cast< qreal*>(_v) = _t->flickDeceleration(); break;
        case 17: *reinterpret_cast< bool*>(_v) = _t->isMoving(); break;
        case 18: *reinterpret_cast< bool*>(_v) = _t->isMovingHorizontally(); break;
        case 19: *reinterpret_cast< bool*>(_v) = _t->isMovingVertically(); break;
        case 20: *reinterpret_cast< bool*>(_v) = _t->isFlicking(); break;
        case 21: *reinterpret_cast< bool*>(_v) = _t->isFlickingHorizontally(); break;
        case 22: *reinterpret_cast< bool*>(_v) = _t->isFlickingVertically(); break;
        case 23: *reinterpret_cast< bool*>(_v) = _t->isDragging(); break;
        case 24: *reinterpret_cast< bool*>(_v) = _t->isDraggingHorizontally(); break;
        case 25: *reinterpret_cast< bool*>(_v) = _t->isDraggingVertically(); break;
        case 26: *reinterpret_cast< FlickableDirection*>(_v) = _t->flickableDirection(); break;
        case 27: *reinterpret_cast< bool*>(_v) = _t->isInteractive(); break;
        case 28: *reinterpret_cast< int*>(_v) = _t->pressDelay(); break;
        case 29: *reinterpret_cast< bool*>(_v) = _t->isAtXEnd(); break;
        case 30: *reinterpret_cast< bool*>(_v) = _t->isAtYEnd(); break;
        case 31: *reinterpret_cast< bool*>(_v) = _t->isAtXBeginning(); break;
        case 32: *reinterpret_cast< bool*>(_v) = _t->isAtYBeginning(); break;
        case 33: *reinterpret_cast< QQuickFlickableVisibleArea**>(_v) = _t->visibleArea(); break;
        case 34: *reinterpret_cast< bool*>(_v) = _t->pixelAligned(); break;
        case 35: *reinterpret_cast< QQmlListProperty<QObject>*>(_v) = _t->flickableData(); break;
        case 36: *reinterpret_cast< QQmlListProperty<QQuickItem>*>(_v) = _t->flickableChildren(); break;
        default: break;
        }
    } else if (_c == QMetaObject::WriteProperty) {
        QQuickFlickable *_t = static_cast<QQuickFlickable *>(_o);
        Q_UNUSED(_t)
        void *_v = _a[0];
        switch (_id) {
        case 0: _t->setContentWidth(*reinterpret_cast< qreal*>(_v)); break;
        case 1: _t->setContentHeight(*reinterpret_cast< qreal*>(_v)); break;
        case 2: _t->setContentX(*reinterpret_cast< qreal*>(_v)); break;
        case 3: _t->setContentY(*reinterpret_cast< qreal*>(_v)); break;
        case 5: _t->setTopMargin(*reinterpret_cast< qreal*>(_v)); break;
        case 6: _t->setBottomMargin(*reinterpret_cast< qreal*>(_v)); break;
        case 8: _t->setLeftMargin(*reinterpret_cast< qreal*>(_v)); break;
        case 9: _t->setRightMargin(*reinterpret_cast< qreal*>(_v)); break;
        case 13: _t->setBoundsBehavior(QFlag(*reinterpret_cast<int*>(_v))); break;
        case 14: _t->setRebound(*reinterpret_cast< QQuickTransition**>(_v)); break;
        case 15: _t->setMaximumFlickVelocity(*reinterpret_cast< qreal*>(_v)); break;
        case 16: _t->setFlickDeceleration(*reinterpret_cast< qreal*>(_v)); break;
        case 26: _t->setFlickableDirection(*reinterpret_cast< FlickableDirection*>(_v)); break;
        case 27: _t->setInteractive(*reinterpret_cast< bool*>(_v)); break;
        case 28: _t->setPressDelay(*reinterpret_cast< int*>(_v)); break;
        case 34: _t->setPixelAligned(*reinterpret_cast< bool*>(_v)); break;
        default: break;
        }
    } else if (_c == QMetaObject::ResetProperty) {
    }
#endif // QT_NO_PROPERTIES
}

const QMetaObject QQuickFlickable::staticMetaObject = {
    { &QQuickItem::staticMetaObject, qt_meta_stringdata_QQuickFlickable.data,
      qt_meta_data_QQuickFlickable,  qt_static_metacall, Q_NULLPTR, Q_NULLPTR}
};


const QMetaObject *QQuickFlickable::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->dynamicMetaObject() : &staticMetaObject;
}

void *QQuickFlickable::qt_metacast(const char *_clname)
{
    if (!_clname) return Q_NULLPTR;
    if (!strcmp(_clname, qt_meta_stringdata_QQuickFlickable.stringdata0))
        return static_cast<void*>(const_cast< QQuickFlickable*>(this));
    return QQuickItem::qt_metacast(_clname);
}

int QQuickFlickable::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QQuickItem::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 45)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 45;
    } else if (_c == QMetaObject::RegisterMethodArgumentMetaType) {
        if (_id < 45)
            *reinterpret_cast<int*>(_a[0]) = -1;
        _id -= 45;
    }
#ifndef QT_NO_PROPERTIES
   else if (_c == QMetaObject::ReadProperty || _c == QMetaObject::WriteProperty
            || _c == QMetaObject::ResetProperty || _c == QMetaObject::RegisterPropertyMetaType) {
        qt_static_metacall(this, _c, _id, _a);
        _id -= 37;
    } else if (_c == QMetaObject::QueryPropertyDesignable) {
        _id -= 37;
    } else if (_c == QMetaObject::QueryPropertyScriptable) {
        _id -= 37;
    } else if (_c == QMetaObject::QueryPropertyStored) {
        _id -= 37;
    } else if (_c == QMetaObject::QueryPropertyEditable) {
        _id -= 37;
    } else if (_c == QMetaObject::QueryPropertyUser) {
        _id -= 37;
    }
#endif // QT_NO_PROPERTIES
    return _id;
}

// SIGNAL 0
void QQuickFlickable::contentWidthChanged()
{
    QMetaObject::activate(this, &staticMetaObject, 0, Q_NULLPTR);
}

// SIGNAL 1
void QQuickFlickable::contentHeightChanged()
{
    QMetaObject::activate(this, &staticMetaObject, 1, Q_NULLPTR);
}

// SIGNAL 2
void QQuickFlickable::contentXChanged()
{
    QMetaObject::activate(this, &staticMetaObject, 2, Q_NULLPTR);
}

// SIGNAL 3
void QQuickFlickable::contentYChanged()
{
    QMetaObject::activate(this, &staticMetaObject, 3, Q_NULLPTR);
}

// SIGNAL 4
void QQuickFlickable::topMarginChanged()
{
    QMetaObject::activate(this, &staticMetaObject, 4, Q_NULLPTR);
}

// SIGNAL 5
void QQuickFlickable::bottomMarginChanged()
{
    QMetaObject::activate(this, &staticMetaObject, 5, Q_NULLPTR);
}

// SIGNAL 6
void QQuickFlickable::leftMarginChanged()
{
    QMetaObject::activate(this, &staticMetaObject, 6, Q_NULLPTR);
}

// SIGNAL 7
void QQuickFlickable::rightMarginChanged()
{
    QMetaObject::activate(this, &staticMetaObject, 7, Q_NULLPTR);
}

// SIGNAL 8
void QQuickFlickable::originYChanged()
{
    QMetaObject::activate(this, &staticMetaObject, 8, Q_NULLPTR);
}

// SIGNAL 9
void QQuickFlickable::originXChanged()
{
    QMetaObject::activate(this, &staticMetaObject, 9, Q_NULLPTR);
}

// SIGNAL 10
void QQuickFlickable::movingChanged()
{
    QMetaObject::activate(this, &staticMetaObject, 10, Q_NULLPTR);
}

// SIGNAL 11
void QQuickFlickable::movingHorizontallyChanged()
{
    QMetaObject::activate(this, &staticMetaObject, 11, Q_NULLPTR);
}

// SIGNAL 12
void QQuickFlickable::movingVerticallyChanged()
{
    QMetaObject::activate(this, &staticMetaObject, 12, Q_NULLPTR);
}

// SIGNAL 13
void QQuickFlickable::flickingChanged()
{
    QMetaObject::activate(this, &staticMetaObject, 13, Q_NULLPTR);
}

// SIGNAL 14
void QQuickFlickable::flickingHorizontallyChanged()
{
    QMetaObject::activate(this, &staticMetaObject, 14, Q_NULLPTR);
}

// SIGNAL 15
void QQuickFlickable::flickingVerticallyChanged()
{
    QMetaObject::activate(this, &staticMetaObject, 15, Q_NULLPTR);
}

// SIGNAL 16
void QQuickFlickable::draggingChanged()
{
    QMetaObject::activate(this, &staticMetaObject, 16, Q_NULLPTR);
}

// SIGNAL 17
void QQuickFlickable::draggingHorizontallyChanged()
{
    QMetaObject::activate(this, &staticMetaObject, 17, Q_NULLPTR);
}

// SIGNAL 18
void QQuickFlickable::draggingVerticallyChanged()
{
    QMetaObject::activate(this, &staticMetaObject, 18, Q_NULLPTR);
}

// SIGNAL 19
void QQuickFlickable::horizontalVelocityChanged()
{
    QMetaObject::activate(this, &staticMetaObject, 19, Q_NULLPTR);
}

// SIGNAL 20
void QQuickFlickable::verticalVelocityChanged()
{
    QMetaObject::activate(this, &staticMetaObject, 20, Q_NULLPTR);
}

// SIGNAL 21
void QQuickFlickable::isAtBoundaryChanged()
{
    QMetaObject::activate(this, &staticMetaObject, 21, Q_NULLPTR);
}

// SIGNAL 22
void QQuickFlickable::flickableDirectionChanged()
{
    QMetaObject::activate(this, &staticMetaObject, 22, Q_NULLPTR);
}

// SIGNAL 23
void QQuickFlickable::interactiveChanged()
{
    QMetaObject::activate(this, &staticMetaObject, 23, Q_NULLPTR);
}

// SIGNAL 24
void QQuickFlickable::boundsBehaviorChanged()
{
    QMetaObject::activate(this, &staticMetaObject, 24, Q_NULLPTR);
}

// SIGNAL 25
void QQuickFlickable::reboundChanged()
{
    QMetaObject::activate(this, &staticMetaObject, 25, Q_NULLPTR);
}

// SIGNAL 26
void QQuickFlickable::maximumFlickVelocityChanged()
{
    QMetaObject::activate(this, &staticMetaObject, 26, Q_NULLPTR);
}

// SIGNAL 27
void QQuickFlickable::flickDecelerationChanged()
{
    QMetaObject::activate(this, &staticMetaObject, 27, Q_NULLPTR);
}

// SIGNAL 28
void QQuickFlickable::pressDelayChanged()
{
    QMetaObject::activate(this, &staticMetaObject, 28, Q_NULLPTR);
}

// SIGNAL 29
void QQuickFlickable::movementStarted()
{
    QMetaObject::activate(this, &staticMetaObject, 29, Q_NULLPTR);
}

// SIGNAL 30
void QQuickFlickable::movementEnded()
{
    QMetaObject::activate(this, &staticMetaObject, 30, Q_NULLPTR);
}

// SIGNAL 31
void QQuickFlickable::flickStarted()
{
    QMetaObject::activate(this, &staticMetaObject, 31, Q_NULLPTR);
}

// SIGNAL 32
void QQuickFlickable::flickEnded()
{
    QMetaObject::activate(this, &staticMetaObject, 32, Q_NULLPTR);
}

// SIGNAL 33
void QQuickFlickable::dragStarted()
{
    QMetaObject::activate(this, &staticMetaObject, 33, Q_NULLPTR);
}

// SIGNAL 34
void QQuickFlickable::dragEnded()
{
    QMetaObject::activate(this, &staticMetaObject, 34, Q_NULLPTR);
}

// SIGNAL 35
void QQuickFlickable::pixelAlignedChanged()
{
    QMetaObject::activate(this, &staticMetaObject, 35, Q_NULLPTR);
}
QT_END_MOC_NAMESPACE
