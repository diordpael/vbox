/****************************************************************************
** Meta object code from reading C++ file 'qquicktextinput_p.h'
**
** Created by: The Qt Meta Object Compiler version 67 (Qt 5.6.3)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "../../items/qquicktextinput_p.h"
#include <QtCore/qbytearray.h>
#include <QtCore/qmetatype.h>
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'qquicktextinput_p.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 67
#error "This file was generated using the moc from 5.6.3. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
struct qt_meta_stringdata_QQuickTextInput_t {
    QByteArrayData data[165];
    char stringdata0[2449];
};
#define QT_MOC_LITERAL(idx, ofs, len) \
    Q_STATIC_BYTE_ARRAY_DATA_HEADER_INITIALIZER_WITH_OFFSET(len, \
    qptrdiff(offsetof(qt_meta_stringdata_QQuickTextInput_t, stringdata0) + ofs \
        - idx * sizeof(QByteArrayData)) \
    )
static const qt_meta_stringdata_QQuickTextInput_t qt_meta_stringdata_QQuickTextInput = {
    {
QT_MOC_LITERAL(0, 0, 15), // "QQuickTextInput"
QT_MOC_LITERAL(1, 16, 11), // "textChanged"
QT_MOC_LITERAL(2, 28, 0), // ""
QT_MOC_LITERAL(3, 29, 21), // "cursorPositionChanged"
QT_MOC_LITERAL(4, 51, 22), // "cursorRectangleChanged"
QT_MOC_LITERAL(5, 74, 21), // "selectionStartChanged"
QT_MOC_LITERAL(6, 96, 19), // "selectionEndChanged"
QT_MOC_LITERAL(7, 116, 19), // "selectedTextChanged"
QT_MOC_LITERAL(8, 136, 8), // "accepted"
QT_MOC_LITERAL(9, 145, 22), // "acceptableInputChanged"
QT_MOC_LITERAL(10, 168, 15), // "editingFinished"
QT_MOC_LITERAL(11, 184, 12), // "colorChanged"
QT_MOC_LITERAL(12, 197, 21), // "selectionColorChanged"
QT_MOC_LITERAL(13, 219, 24), // "selectedTextColorChanged"
QT_MOC_LITERAL(14, 244, 11), // "fontChanged"
QT_MOC_LITERAL(15, 256, 4), // "font"
QT_MOC_LITERAL(16, 261, 26), // "horizontalAlignmentChanged"
QT_MOC_LITERAL(17, 288, 27), // "QQuickTextInput::HAlignment"
QT_MOC_LITERAL(18, 316, 9), // "alignment"
QT_MOC_LITERAL(19, 326, 24), // "verticalAlignmentChanged"
QT_MOC_LITERAL(20, 351, 27), // "QQuickTextInput::VAlignment"
QT_MOC_LITERAL(21, 379, 15), // "wrapModeChanged"
QT_MOC_LITERAL(22, 395, 15), // "readOnlyChanged"
QT_MOC_LITERAL(23, 411, 10), // "isReadOnly"
QT_MOC_LITERAL(24, 422, 20), // "cursorVisibleChanged"
QT_MOC_LITERAL(25, 443, 15), // "isCursorVisible"
QT_MOC_LITERAL(26, 459, 21), // "cursorDelegateChanged"
QT_MOC_LITERAL(27, 481, 20), // "maximumLengthChanged"
QT_MOC_LITERAL(28, 502, 13), // "maximumLength"
QT_MOC_LITERAL(29, 516, 16), // "validatorChanged"
QT_MOC_LITERAL(30, 533, 16), // "inputMaskChanged"
QT_MOC_LITERAL(31, 550, 9), // "inputMask"
QT_MOC_LITERAL(32, 560, 15), // "echoModeChanged"
QT_MOC_LITERAL(33, 576, 25), // "QQuickTextInput::EchoMode"
QT_MOC_LITERAL(34, 602, 8), // "echoMode"
QT_MOC_LITERAL(35, 611, 24), // "passwordCharacterChanged"
QT_MOC_LITERAL(36, 636, 24), // "passwordMaskDelayChanged"
QT_MOC_LITERAL(37, 661, 5), // "delay"
QT_MOC_LITERAL(38, 667, 18), // "displayTextChanged"
QT_MOC_LITERAL(39, 686, 25), // "activeFocusOnPressChanged"
QT_MOC_LITERAL(40, 712, 18), // "activeFocusOnPress"
QT_MOC_LITERAL(41, 731, 17), // "autoScrollChanged"
QT_MOC_LITERAL(42, 749, 10), // "autoScroll"
QT_MOC_LITERAL(43, 760, 20), // "selectByMouseChanged"
QT_MOC_LITERAL(44, 781, 13), // "selectByMouse"
QT_MOC_LITERAL(45, 795, 25), // "mouseSelectionModeChanged"
QT_MOC_LITERAL(46, 821, 30), // "QQuickTextInput::SelectionMode"
QT_MOC_LITERAL(47, 852, 4), // "mode"
QT_MOC_LITERAL(48, 857, 26), // "persistentSelectionChanged"
QT_MOC_LITERAL(49, 884, 15), // "canPasteChanged"
QT_MOC_LITERAL(50, 900, 14), // "canUndoChanged"
QT_MOC_LITERAL(51, 915, 14), // "canRedoChanged"
QT_MOC_LITERAL(52, 930, 27), // "inputMethodComposingChanged"
QT_MOC_LITERAL(53, 958, 35), // "effectiveHorizontalAlignmentC..."
QT_MOC_LITERAL(54, 994, 18), // "contentSizeChanged"
QT_MOC_LITERAL(55, 1013, 23), // "inputMethodHintsChanged"
QT_MOC_LITERAL(56, 1037, 17), // "renderTypeChanged"
QT_MOC_LITERAL(57, 1055, 14), // "paddingChanged"
QT_MOC_LITERAL(58, 1070, 17), // "topPaddingChanged"
QT_MOC_LITERAL(59, 1088, 18), // "leftPaddingChanged"
QT_MOC_LITERAL(60, 1107, 19), // "rightPaddingChanged"
QT_MOC_LITERAL(61, 1127, 20), // "bottomPaddingChanged"
QT_MOC_LITERAL(62, 1148, 9), // "selectAll"
QT_MOC_LITERAL(63, 1158, 10), // "selectWord"
QT_MOC_LITERAL(64, 1169, 6), // "select"
QT_MOC_LITERAL(65, 1176, 5), // "start"
QT_MOC_LITERAL(66, 1182, 3), // "end"
QT_MOC_LITERAL(67, 1186, 8), // "deselect"
QT_MOC_LITERAL(68, 1195, 13), // "isRightToLeft"
QT_MOC_LITERAL(69, 1209, 3), // "cut"
QT_MOC_LITERAL(70, 1213, 4), // "copy"
QT_MOC_LITERAL(71, 1218, 5), // "paste"
QT_MOC_LITERAL(72, 1224, 4), // "undo"
QT_MOC_LITERAL(73, 1229, 4), // "redo"
QT_MOC_LITERAL(74, 1234, 6), // "insert"
QT_MOC_LITERAL(75, 1241, 8), // "position"
QT_MOC_LITERAL(76, 1250, 4), // "text"
QT_MOC_LITERAL(77, 1255, 6), // "remove"
QT_MOC_LITERAL(78, 1262, 13), // "ensureVisible"
QT_MOC_LITERAL(79, 1276, 16), // "selectionChanged"
QT_MOC_LITERAL(80, 1293, 12), // "createCursor"
QT_MOC_LITERAL(81, 1306, 21), // "updateCursorRectangle"
QT_MOC_LITERAL(82, 1328, 6), // "scroll"
QT_MOC_LITERAL(83, 1335, 17), // "q_canPasteChanged"
QT_MOC_LITERAL(84, 1353, 17), // "q_updateAlignment"
QT_MOC_LITERAL(85, 1371, 17), // "triggerPreprocess"
QT_MOC_LITERAL(86, 1389, 18), // "q_validatorChanged"
QT_MOC_LITERAL(87, 1408, 10), // "positionAt"
QT_MOC_LITERAL(88, 1419, 15), // "QQmlV4Function*"
QT_MOC_LITERAL(89, 1435, 4), // "args"
QT_MOC_LITERAL(90, 1440, 19), // "positionToRectangle"
QT_MOC_LITERAL(91, 1460, 3), // "pos"
QT_MOC_LITERAL(92, 1464, 19), // "moveCursorSelection"
QT_MOC_LITERAL(93, 1484, 13), // "SelectionMode"
QT_MOC_LITERAL(94, 1498, 16), // "inputMethodQuery"
QT_MOC_LITERAL(95, 1515, 20), // "Qt::InputMethodQuery"
QT_MOC_LITERAL(96, 1536, 5), // "query"
QT_MOC_LITERAL(97, 1542, 8), // "argument"
QT_MOC_LITERAL(98, 1551, 7), // "getText"
QT_MOC_LITERAL(99, 1559, 6), // "length"
QT_MOC_LITERAL(100, 1566, 5), // "color"
QT_MOC_LITERAL(101, 1572, 14), // "selectionColor"
QT_MOC_LITERAL(102, 1587, 17), // "selectedTextColor"
QT_MOC_LITERAL(103, 1605, 19), // "horizontalAlignment"
QT_MOC_LITERAL(104, 1625, 10), // "HAlignment"
QT_MOC_LITERAL(105, 1636, 28), // "effectiveHorizontalAlignment"
QT_MOC_LITERAL(106, 1665, 17), // "verticalAlignment"
QT_MOC_LITERAL(107, 1683, 10), // "VAlignment"
QT_MOC_LITERAL(108, 1694, 8), // "wrapMode"
QT_MOC_LITERAL(109, 1703, 8), // "WrapMode"
QT_MOC_LITERAL(110, 1712, 8), // "readOnly"
QT_MOC_LITERAL(111, 1721, 13), // "cursorVisible"
QT_MOC_LITERAL(112, 1735, 14), // "cursorPosition"
QT_MOC_LITERAL(113, 1750, 15), // "cursorRectangle"
QT_MOC_LITERAL(114, 1766, 14), // "cursorDelegate"
QT_MOC_LITERAL(115, 1781, 14), // "QQmlComponent*"
QT_MOC_LITERAL(116, 1796, 14), // "selectionStart"
QT_MOC_LITERAL(117, 1811, 12), // "selectionEnd"
QT_MOC_LITERAL(118, 1824, 12), // "selectedText"
QT_MOC_LITERAL(119, 1837, 9), // "validator"
QT_MOC_LITERAL(120, 1847, 11), // "QValidator*"
QT_MOC_LITERAL(121, 1859, 16), // "inputMethodHints"
QT_MOC_LITERAL(122, 1876, 20), // "Qt::InputMethodHints"
QT_MOC_LITERAL(123, 1897, 15), // "acceptableInput"
QT_MOC_LITERAL(124, 1913, 8), // "EchoMode"
QT_MOC_LITERAL(125, 1922, 17), // "passwordCharacter"
QT_MOC_LITERAL(126, 1940, 17), // "passwordMaskDelay"
QT_MOC_LITERAL(127, 1958, 11), // "displayText"
QT_MOC_LITERAL(128, 1970, 18), // "mouseSelectionMode"
QT_MOC_LITERAL(129, 1989, 19), // "persistentSelection"
QT_MOC_LITERAL(130, 2009, 8), // "canPaste"
QT_MOC_LITERAL(131, 2018, 7), // "canUndo"
QT_MOC_LITERAL(132, 2026, 7), // "canRedo"
QT_MOC_LITERAL(133, 2034, 20), // "inputMethodComposing"
QT_MOC_LITERAL(134, 2055, 12), // "contentWidth"
QT_MOC_LITERAL(135, 2068, 13), // "contentHeight"
QT_MOC_LITERAL(136, 2082, 10), // "renderType"
QT_MOC_LITERAL(137, 2093, 10), // "RenderType"
QT_MOC_LITERAL(138, 2104, 7), // "padding"
QT_MOC_LITERAL(139, 2112, 10), // "topPadding"
QT_MOC_LITERAL(140, 2123, 11), // "leftPadding"
QT_MOC_LITERAL(141, 2135, 12), // "rightPadding"
QT_MOC_LITERAL(142, 2148, 13), // "bottomPadding"
QT_MOC_LITERAL(143, 2162, 6), // "Normal"
QT_MOC_LITERAL(144, 2169, 6), // "NoEcho"
QT_MOC_LITERAL(145, 2176, 8), // "Password"
QT_MOC_LITERAL(146, 2185, 18), // "PasswordEchoOnEdit"
QT_MOC_LITERAL(147, 2204, 9), // "AlignLeft"
QT_MOC_LITERAL(148, 2214, 10), // "AlignRight"
QT_MOC_LITERAL(149, 2225, 12), // "AlignHCenter"
QT_MOC_LITERAL(150, 2238, 8), // "AlignTop"
QT_MOC_LITERAL(151, 2247, 11), // "AlignBottom"
QT_MOC_LITERAL(152, 2259, 12), // "AlignVCenter"
QT_MOC_LITERAL(153, 2272, 6), // "NoWrap"
QT_MOC_LITERAL(154, 2279, 8), // "WordWrap"
QT_MOC_LITERAL(155, 2288, 12), // "WrapAnywhere"
QT_MOC_LITERAL(156, 2301, 28), // "WrapAtWordBoundaryOrAnywhere"
QT_MOC_LITERAL(157, 2330, 4), // "Wrap"
QT_MOC_LITERAL(158, 2335, 16), // "SelectCharacters"
QT_MOC_LITERAL(159, 2352, 11), // "SelectWords"
QT_MOC_LITERAL(160, 2364, 14), // "CursorPosition"
QT_MOC_LITERAL(161, 2379, 23), // "CursorBetweenCharacters"
QT_MOC_LITERAL(162, 2403, 17), // "CursorOnCharacter"
QT_MOC_LITERAL(163, 2421, 11), // "QtRendering"
QT_MOC_LITERAL(164, 2433, 15) // "NativeRendering"

    },
    "QQuickTextInput\0textChanged\0\0"
    "cursorPositionChanged\0cursorRectangleChanged\0"
    "selectionStartChanged\0selectionEndChanged\0"
    "selectedTextChanged\0accepted\0"
    "acceptableInputChanged\0editingFinished\0"
    "colorChanged\0selectionColorChanged\0"
    "selectedTextColorChanged\0fontChanged\0"
    "font\0horizontalAlignmentChanged\0"
    "QQuickTextInput::HAlignment\0alignment\0"
    "verticalAlignmentChanged\0"
    "QQuickTextInput::VAlignment\0wrapModeChanged\0"
    "readOnlyChanged\0isReadOnly\0"
    "cursorVisibleChanged\0isCursorVisible\0"
    "cursorDelegateChanged\0maximumLengthChanged\0"
    "maximumLength\0validatorChanged\0"
    "inputMaskChanged\0inputMask\0echoModeChanged\0"
    "QQuickTextInput::EchoMode\0echoMode\0"
    "passwordCharacterChanged\0"
    "passwordMaskDelayChanged\0delay\0"
    "displayTextChanged\0activeFocusOnPressChanged\0"
    "activeFocusOnPress\0autoScrollChanged\0"
    "autoScroll\0selectByMouseChanged\0"
    "selectByMouse\0mouseSelectionModeChanged\0"
    "QQuickTextInput::SelectionMode\0mode\0"
    "persistentSelectionChanged\0canPasteChanged\0"
    "canUndoChanged\0canRedoChanged\0"
    "inputMethodComposingChanged\0"
    "effectiveHorizontalAlignmentChanged\0"
    "contentSizeChanged\0inputMethodHintsChanged\0"
    "renderTypeChanged\0paddingChanged\0"
    "topPaddingChanged\0leftPaddingChanged\0"
    "rightPaddingChanged\0bottomPaddingChanged\0"
    "selectAll\0selectWord\0select\0start\0end\0"
    "deselect\0isRightToLeft\0cut\0copy\0paste\0"
    "undo\0redo\0insert\0position\0text\0remove\0"
    "ensureVisible\0selectionChanged\0"
    "createCursor\0updateCursorRectangle\0"
    "scroll\0q_canPasteChanged\0q_updateAlignment\0"
    "triggerPreprocess\0q_validatorChanged\0"
    "positionAt\0QQmlV4Function*\0args\0"
    "positionToRectangle\0pos\0moveCursorSelection\0"
    "SelectionMode\0inputMethodQuery\0"
    "Qt::InputMethodQuery\0query\0argument\0"
    "getText\0length\0color\0selectionColor\0"
    "selectedTextColor\0horizontalAlignment\0"
    "HAlignment\0effectiveHorizontalAlignment\0"
    "verticalAlignment\0VAlignment\0wrapMode\0"
    "WrapMode\0readOnly\0cursorVisible\0"
    "cursorPosition\0cursorRectangle\0"
    "cursorDelegate\0QQmlComponent*\0"
    "selectionStart\0selectionEnd\0selectedText\0"
    "validator\0QValidator*\0inputMethodHints\0"
    "Qt::InputMethodHints\0acceptableInput\0"
    "EchoMode\0passwordCharacter\0passwordMaskDelay\0"
    "displayText\0mouseSelectionMode\0"
    "persistentSelection\0canPaste\0canUndo\0"
    "canRedo\0inputMethodComposing\0contentWidth\0"
    "contentHeight\0renderType\0RenderType\0"
    "padding\0topPadding\0leftPadding\0"
    "rightPadding\0bottomPadding\0Normal\0"
    "NoEcho\0Password\0PasswordEchoOnEdit\0"
    "AlignLeft\0AlignRight\0AlignHCenter\0"
    "AlignTop\0AlignBottom\0AlignVCenter\0"
    "NoWrap\0WordWrap\0WrapAnywhere\0"
    "WrapAtWordBoundaryOrAnywhere\0Wrap\0"
    "SelectCharacters\0SelectWords\0"
    "CursorPosition\0CursorBetweenCharacters\0"
    "CursorOnCharacter\0QtRendering\0"
    "NativeRendering"
};
#undef QT_MOC_LITERAL

static const uint qt_meta_data_QQuickTextInput[] = {

 // content:
       7,       // revision
       0,       // classname
       0,    0, // classinfo
      71,   14, // methods
      44,  575, // properties
       7,  795, // enums/sets
       0,    0, // constructors
       0,       // flags
      44,       // signalCount

 // signals: name, argc, parameters, tag, flags
       1,    0,  440,    2, 0x06 /* Public */,
       3,    0,  441,    2, 0x06 /* Public */,
       4,    0,  442,    2, 0x06 /* Public */,
       5,    0,  443,    2, 0x06 /* Public */,
       6,    0,  444,    2, 0x06 /* Public */,
       7,    0,  445,    2, 0x06 /* Public */,
       8,    0,  446,    2, 0x06 /* Public */,
       9,    0,  447,    2, 0x06 /* Public */,
      10,    0,  448,    2, 0x86 /* Public | MethodRevisioned */,
      11,    0,  449,    2, 0x06 /* Public */,
      12,    0,  450,    2, 0x06 /* Public */,
      13,    0,  451,    2, 0x06 /* Public */,
      14,    1,  452,    2, 0x06 /* Public */,
      16,    1,  455,    2, 0x06 /* Public */,
      19,    1,  458,    2, 0x06 /* Public */,
      21,    0,  461,    2, 0x06 /* Public */,
      22,    1,  462,    2, 0x06 /* Public */,
      24,    1,  465,    2, 0x06 /* Public */,
      26,    0,  468,    2, 0x06 /* Public */,
      27,    1,  469,    2, 0x06 /* Public */,
      29,    0,  472,    2, 0x06 /* Public */,
      30,    1,  473,    2, 0x06 /* Public */,
      32,    1,  476,    2, 0x06 /* Public */,
      35,    0,  479,    2, 0x06 /* Public */,
      36,    1,  480,    2, 0x86 /* Public | MethodRevisioned */,
      38,    0,  483,    2, 0x06 /* Public */,
      39,    1,  484,    2, 0x06 /* Public */,
      41,    1,  487,    2, 0x06 /* Public */,
      43,    1,  490,    2, 0x06 /* Public */,
      45,    1,  493,    2, 0x06 /* Public */,
      48,    0,  496,    2, 0x06 /* Public */,
      49,    0,  497,    2, 0x06 /* Public */,
      50,    0,  498,    2, 0x06 /* Public */,
      51,    0,  499,    2, 0x06 /* Public */,
      52,    0,  500,    2, 0x06 /* Public */,
      53,    0,  501,    2, 0x06 /* Public */,
      54,    0,  502,    2, 0x06 /* Public */,
      55,    0,  503,    2, 0x06 /* Public */,
      56,    0,  504,    2, 0x06 /* Public */,
      57,    0,  505,    2, 0x86 /* Public | MethodRevisioned */,
      58,    0,  506,    2, 0x86 /* Public | MethodRevisioned */,
      59,    0,  507,    2, 0x86 /* Public | MethodRevisioned */,
      60,    0,  508,    2, 0x86 /* Public | MethodRevisioned */,
      61,    0,  509,    2, 0x86 /* Public | MethodRevisioned */,

 // slots: name, argc, parameters, tag, flags
      62,    0,  510,    2, 0x0a /* Public */,
      63,    0,  511,    2, 0x0a /* Public */,
      64,    2,  512,    2, 0x0a /* Public */,
      67,    0,  517,    2, 0x0a /* Public */,
      68,    2,  518,    2, 0x0a /* Public */,
      69,    0,  523,    2, 0x0a /* Public */,
      70,    0,  524,    2, 0x0a /* Public */,
      71,    0,  525,    2, 0x0a /* Public */,
      72,    0,  526,    2, 0x0a /* Public */,
      73,    0,  527,    2, 0x0a /* Public */,
      74,    2,  528,    2, 0x0a /* Public */,
      77,    2,  533,    2, 0x0a /* Public */,
      78,    1,  538,    2, 0x8a /* Public | MethodRevisioned */,
      79,    0,  541,    2, 0x08 /* Private */,
      80,    0,  542,    2, 0x08 /* Private */,
      81,    1,  543,    2, 0x08 /* Private */,
      81,    0,  546,    2, 0x28 /* Private | MethodCloned */,
      83,    0,  547,    2, 0x08 /* Private */,
      84,    0,  548,    2, 0x08 /* Private */,
      85,    0,  549,    2, 0x08 /* Private */,
      86,    0,  550,    2, 0x08 /* Private */,

 // methods: name, argc, parameters, tag, flags
      87,    1,  551,    2, 0x02 /* Public */,
      90,    1,  554,    2, 0x02 /* Public */,
      92,    1,  557,    2, 0x02 /* Public */,
      92,    2,  560,    2, 0x02 /* Public */,
      94,    2,  565,    2, 0x82 /* Public | MethodRevisioned */,
      98,    2,  570,    2, 0x02 /* Public */,

 // signals: revision
       0,
       0,
       0,
       0,
       0,
       0,
       0,
       0,
       2,
       0,
       0,
       0,
       0,
       0,
       0,
       0,
       0,
       0,
       0,
       0,
       0,
       0,
       0,
       0,
       3,
       0,
       0,
       0,
       0,
       0,
       0,
       0,
       0,
       0,
       0,
       0,
       0,
       0,
       0,
       6,
       6,
       6,
       6,
       6,

 // slots: revision
       0,
       0,
       0,
       0,
       0,
       0,
       0,
       0,
       0,
       0,
       0,
       0,
       3,
       0,
       0,
       0,
       0,
       0,
       0,
       0,
       0,

 // methods: revision
       0,
       0,
       0,
       0,
       3,
       0,

 // signals: parameters
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void, QMetaType::QFont,   15,
    QMetaType::Void, 0x80000000 | 17,   18,
    QMetaType::Void, 0x80000000 | 20,   18,
    QMetaType::Void,
    QMetaType::Void, QMetaType::Bool,   23,
    QMetaType::Void, QMetaType::Bool,   25,
    QMetaType::Void,
    QMetaType::Void, QMetaType::Int,   28,
    QMetaType::Void,
    QMetaType::Void, QMetaType::QString,   31,
    QMetaType::Void, 0x80000000 | 33,   34,
    QMetaType::Void,
    QMetaType::Void, QMetaType::Int,   37,
    QMetaType::Void,
    QMetaType::Void, QMetaType::Bool,   40,
    QMetaType::Void, QMetaType::Bool,   42,
    QMetaType::Void, QMetaType::Bool,   44,
    QMetaType::Void, 0x80000000 | 46,   47,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,

 // slots: parameters
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void, QMetaType::Int, QMetaType::Int,   65,   66,
    QMetaType::Void,
    QMetaType::Bool, QMetaType::Int, QMetaType::Int,   65,   66,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void, QMetaType::Int, QMetaType::QString,   75,   76,
    QMetaType::Void, QMetaType::Int, QMetaType::Int,   65,   66,
    QMetaType::Void, QMetaType::Int,   75,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void, QMetaType::Bool,   82,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,

 // methods: parameters
    QMetaType::Void, 0x80000000 | 88,   89,
    QMetaType::QRectF, QMetaType::Int,   91,
    QMetaType::Void, QMetaType::Int,   91,
    QMetaType::Void, QMetaType::Int, 0x80000000 | 93,   91,   47,
    QMetaType::QVariant, 0x80000000 | 95, QMetaType::QVariant,   96,   97,
    QMetaType::QString, QMetaType::Int, QMetaType::Int,   65,   66,

 // properties: name, type, flags
      76, QMetaType::QString, 0x00495103,
      99, QMetaType::Int, 0x00495001,
     100, QMetaType::QColor, 0x00495103,
     101, QMetaType::QColor, 0x00495103,
     102, QMetaType::QColor, 0x00495103,
      15, QMetaType::QFont, 0x00495103,
     103, 0x80000000 | 104, 0x0049500f,
     105, 0x80000000 | 104, 0x00495009,
     106, 0x80000000 | 107, 0x0049500b,
     108, 0x80000000 | 109, 0x0049510b,
     110, QMetaType::Bool, 0x00495103,
     111, QMetaType::Bool, 0x00495103,
     112, QMetaType::Int, 0x00495103,
     113, QMetaType::QRectF, 0x00495001,
     114, 0x80000000 | 115, 0x0049510b,
     116, QMetaType::Int, 0x00495001,
     117, QMetaType::Int, 0x00495001,
     118, QMetaType::QString, 0x00495001,
      28, QMetaType::Int, 0x00495003,
     119, 0x80000000 | 120, 0x0049510b,
      31, QMetaType::QString, 0x00495103,
     121, 0x80000000 | 122, 0x0049510b,
     123, QMetaType::Bool, 0x00495001,
      34, 0x80000000 | 124, 0x0049510b,
      40, QMetaType::Bool, 0x00495003,
     125, QMetaType::QString, 0x00495103,
     126, QMetaType::Int, 0x00c95107,
     127, QMetaType::QString, 0x00495001,
      42, QMetaType::Bool, 0x00495103,
      44, QMetaType::Bool, 0x00495103,
     128, 0x80000000 | 93, 0x0049510b,
     129, QMetaType::Bool, 0x00495103,
     130, QMetaType::Bool, 0x00495001,
     131, QMetaType::Bool, 0x00495001,
     132, QMetaType::Bool, 0x00495001,
     133, QMetaType::Bool, 0x00495001,
     134, QMetaType::QReal, 0x00495001,
     135, QMetaType::QReal, 0x00495001,
     136, 0x80000000 | 137, 0x0049510b,
     138, QMetaType::QReal, 0x00c95107,
     139, QMetaType::QReal, 0x00c95107,
     140, QMetaType::QReal, 0x00c95107,
     141, QMetaType::QReal, 0x00c95107,
     142, QMetaType::QReal, 0x00c95107,

 // properties: notify_signal_id
       0,
       0,
       9,
      10,
      11,
      12,
      13,
      35,
      14,
      15,
      16,
      17,
       1,
       2,
      18,
       3,
       4,
       5,
      19,
      20,
      21,
      37,
       7,
      22,
      26,
      23,
      24,
      25,
      27,
      28,
      29,
      30,
      31,
      32,
      33,
      34,
      36,
      36,
      38,
      39,
      40,
      41,
      42,
      43,

 // properties: revision
       0,
       0,
       0,
       0,
       0,
       0,
       0,
       0,
       0,
       0,
       0,
       0,
       0,
       0,
       0,
       0,
       0,
       0,
       0,
       0,
       0,
       0,
       0,
       0,
       0,
       0,
       3,
       0,
       0,
       0,
       0,
       0,
       0,
       0,
       0,
       0,
       0,
       0,
       0,
       6,
       6,
       6,
       6,
       6,

 // enums: name, flags, count, data
     124, 0x0,    4,  823,
     104, 0x0,    3,  831,
     107, 0x0,    3,  837,
     109, 0x0,    5,  843,
      93, 0x0,    2,  853,
     160, 0x0,    2,  857,
     137, 0x0,    2,  861,

 // enum data: key, value
     143, uint(QQuickTextInput::Normal),
     144, uint(QQuickTextInput::NoEcho),
     145, uint(QQuickTextInput::Password),
     146, uint(QQuickTextInput::PasswordEchoOnEdit),
     147, uint(QQuickTextInput::AlignLeft),
     148, uint(QQuickTextInput::AlignRight),
     149, uint(QQuickTextInput::AlignHCenter),
     150, uint(QQuickTextInput::AlignTop),
     151, uint(QQuickTextInput::AlignBottom),
     152, uint(QQuickTextInput::AlignVCenter),
     153, uint(QQuickTextInput::NoWrap),
     154, uint(QQuickTextInput::WordWrap),
     155, uint(QQuickTextInput::WrapAnywhere),
     156, uint(QQuickTextInput::WrapAtWordBoundaryOrAnywhere),
     157, uint(QQuickTextInput::Wrap),
     158, uint(QQuickTextInput::SelectCharacters),
     159, uint(QQuickTextInput::SelectWords),
     161, uint(QQuickTextInput::CursorBetweenCharacters),
     162, uint(QQuickTextInput::CursorOnCharacter),
     163, uint(QQuickTextInput::QtRendering),
     164, uint(QQuickTextInput::NativeRendering),

       0        // eod
};

void QQuickTextInput::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        QQuickTextInput *_t = static_cast<QQuickTextInput *>(_o);
        Q_UNUSED(_t)
        switch (_id) {
        case 0: _t->textChanged(); break;
        case 1: _t->cursorPositionChanged(); break;
        case 2: _t->cursorRectangleChanged(); break;
        case 3: _t->selectionStartChanged(); break;
        case 4: _t->selectionEndChanged(); break;
        case 5: _t->selectedTextChanged(); break;
        case 6: _t->accepted(); break;
        case 7: _t->acceptableInputChanged(); break;
        case 8: _t->editingFinished(); break;
        case 9: _t->colorChanged(); break;
        case 10: _t->selectionColorChanged(); break;
        case 11: _t->selectedTextColorChanged(); break;
        case 12: _t->fontChanged((*reinterpret_cast< const QFont(*)>(_a[1]))); break;
        case 13: _t->horizontalAlignmentChanged((*reinterpret_cast< QQuickTextInput::HAlignment(*)>(_a[1]))); break;
        case 14: _t->verticalAlignmentChanged((*reinterpret_cast< QQuickTextInput::VAlignment(*)>(_a[1]))); break;
        case 15: _t->wrapModeChanged(); break;
        case 16: _t->readOnlyChanged((*reinterpret_cast< bool(*)>(_a[1]))); break;
        case 17: _t->cursorVisibleChanged((*reinterpret_cast< bool(*)>(_a[1]))); break;
        case 18: _t->cursorDelegateChanged(); break;
        case 19: _t->maximumLengthChanged((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 20: _t->validatorChanged(); break;
        case 21: _t->inputMaskChanged((*reinterpret_cast< const QString(*)>(_a[1]))); break;
        case 22: _t->echoModeChanged((*reinterpret_cast< QQuickTextInput::EchoMode(*)>(_a[1]))); break;
        case 23: _t->passwordCharacterChanged(); break;
        case 24: _t->passwordMaskDelayChanged((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 25: _t->displayTextChanged(); break;
        case 26: _t->activeFocusOnPressChanged((*reinterpret_cast< bool(*)>(_a[1]))); break;
        case 27: _t->autoScrollChanged((*reinterpret_cast< bool(*)>(_a[1]))); break;
        case 28: _t->selectByMouseChanged((*reinterpret_cast< bool(*)>(_a[1]))); break;
        case 29: _t->mouseSelectionModeChanged((*reinterpret_cast< QQuickTextInput::SelectionMode(*)>(_a[1]))); break;
        case 30: _t->persistentSelectionChanged(); break;
        case 31: _t->canPasteChanged(); break;
        case 32: _t->canUndoChanged(); break;
        case 33: _t->canRedoChanged(); break;
        case 34: _t->inputMethodComposingChanged(); break;
        case 35: _t->effectiveHorizontalAlignmentChanged(); break;
        case 36: _t->contentSizeChanged(); break;
        case 37: _t->inputMethodHintsChanged(); break;
        case 38: _t->renderTypeChanged(); break;
        case 39: _t->paddingChanged(); break;
        case 40: _t->topPaddingChanged(); break;
        case 41: _t->leftPaddingChanged(); break;
        case 42: _t->rightPaddingChanged(); break;
        case 43: _t->bottomPaddingChanged(); break;
        case 44: _t->selectAll(); break;
        case 45: _t->selectWord(); break;
        case 46: _t->select((*reinterpret_cast< int(*)>(_a[1])),(*reinterpret_cast< int(*)>(_a[2]))); break;
        case 47: _t->deselect(); break;
        case 48: { bool _r = _t->isRightToLeft((*reinterpret_cast< int(*)>(_a[1])),(*reinterpret_cast< int(*)>(_a[2])));
            if (_a[0]) *reinterpret_cast< bool*>(_a[0]) = _r; }  break;
        case 49: _t->cut(); break;
        case 50: _t->copy(); break;
        case 51: _t->paste(); break;
        case 52: _t->undo(); break;
        case 53: _t->redo(); break;
        case 54: _t->insert((*reinterpret_cast< int(*)>(_a[1])),(*reinterpret_cast< const QString(*)>(_a[2]))); break;
        case 55: _t->remove((*reinterpret_cast< int(*)>(_a[1])),(*reinterpret_cast< int(*)>(_a[2]))); break;
        case 56: _t->ensureVisible((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 57: _t->selectionChanged(); break;
        case 58: _t->createCursor(); break;
        case 59: _t->updateCursorRectangle((*reinterpret_cast< bool(*)>(_a[1]))); break;
        case 60: _t->updateCursorRectangle(); break;
        case 61: _t->q_canPasteChanged(); break;
        case 62: _t->q_updateAlignment(); break;
        case 63: _t->triggerPreprocess(); break;
        case 64: _t->q_validatorChanged(); break;
        case 65: _t->positionAt((*reinterpret_cast< QQmlV4Function*(*)>(_a[1]))); break;
        case 66: { QRectF _r = _t->positionToRectangle((*reinterpret_cast< int(*)>(_a[1])));
            if (_a[0]) *reinterpret_cast< QRectF*>(_a[0]) = _r; }  break;
        case 67: _t->moveCursorSelection((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 68: _t->moveCursorSelection((*reinterpret_cast< int(*)>(_a[1])),(*reinterpret_cast< SelectionMode(*)>(_a[2]))); break;
        case 69: { QVariant _r = _t->inputMethodQuery((*reinterpret_cast< Qt::InputMethodQuery(*)>(_a[1])),(*reinterpret_cast< QVariant(*)>(_a[2])));
            if (_a[0]) *reinterpret_cast< QVariant*>(_a[0]) = _r; }  break;
        case 70: { QString _r = _t->getText((*reinterpret_cast< int(*)>(_a[1])),(*reinterpret_cast< int(*)>(_a[2])));
            if (_a[0]) *reinterpret_cast< QString*>(_a[0]) = _r; }  break;
        default: ;
        }
    } else if (_c == QMetaObject::IndexOfMethod) {
        int *result = reinterpret_cast<int *>(_a[0]);
        void **func = reinterpret_cast<void **>(_a[1]);
        {
            typedef void (QQuickTextInput::*_t)();
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&QQuickTextInput::textChanged)) {
                *result = 0;
                return;
            }
        }
        {
            typedef void (QQuickTextInput::*_t)();
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&QQuickTextInput::cursorPositionChanged)) {
                *result = 1;
                return;
            }
        }
        {
            typedef void (QQuickTextInput::*_t)();
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&QQuickTextInput::cursorRectangleChanged)) {
                *result = 2;
                return;
            }
        }
        {
            typedef void (QQuickTextInput::*_t)();
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&QQuickTextInput::selectionStartChanged)) {
                *result = 3;
                return;
            }
        }
        {
            typedef void (QQuickTextInput::*_t)();
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&QQuickTextInput::selectionEndChanged)) {
                *result = 4;
                return;
            }
        }
        {
            typedef void (QQuickTextInput::*_t)();
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&QQuickTextInput::selectedTextChanged)) {
                *result = 5;
                return;
            }
        }
        {
            typedef void (QQuickTextInput::*_t)();
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&QQuickTextInput::accepted)) {
                *result = 6;
                return;
            }
        }
        {
            typedef void (QQuickTextInput::*_t)();
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&QQuickTextInput::acceptableInputChanged)) {
                *result = 7;
                return;
            }
        }
        {
            typedef void (QQuickTextInput::*_t)();
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&QQuickTextInput::editingFinished)) {
                *result = 8;
                return;
            }
        }
        {
            typedef void (QQuickTextInput::*_t)();
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&QQuickTextInput::colorChanged)) {
                *result = 9;
                return;
            }
        }
        {
            typedef void (QQuickTextInput::*_t)();
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&QQuickTextInput::selectionColorChanged)) {
                *result = 10;
                return;
            }
        }
        {
            typedef void (QQuickTextInput::*_t)();
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&QQuickTextInput::selectedTextColorChanged)) {
                *result = 11;
                return;
            }
        }
        {
            typedef void (QQuickTextInput::*_t)(const QFont & );
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&QQuickTextInput::fontChanged)) {
                *result = 12;
                return;
            }
        }
        {
            typedef void (QQuickTextInput::*_t)(QQuickTextInput::HAlignment );
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&QQuickTextInput::horizontalAlignmentChanged)) {
                *result = 13;
                return;
            }
        }
        {
            typedef void (QQuickTextInput::*_t)(QQuickTextInput::VAlignment );
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&QQuickTextInput::verticalAlignmentChanged)) {
                *result = 14;
                return;
            }
        }
        {
            typedef void (QQuickTextInput::*_t)();
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&QQuickTextInput::wrapModeChanged)) {
                *result = 15;
                return;
            }
        }
        {
            typedef void (QQuickTextInput::*_t)(bool );
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&QQuickTextInput::readOnlyChanged)) {
                *result = 16;
                return;
            }
        }
        {
            typedef void (QQuickTextInput::*_t)(bool );
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&QQuickTextInput::cursorVisibleChanged)) {
                *result = 17;
                return;
            }
        }
        {
            typedef void (QQuickTextInput::*_t)();
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&QQuickTextInput::cursorDelegateChanged)) {
                *result = 18;
                return;
            }
        }
        {
            typedef void (QQuickTextInput::*_t)(int );
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&QQuickTextInput::maximumLengthChanged)) {
                *result = 19;
                return;
            }
        }
        {
            typedef void (QQuickTextInput::*_t)();
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&QQuickTextInput::validatorChanged)) {
                *result = 20;
                return;
            }
        }
        {
            typedef void (QQuickTextInput::*_t)(const QString & );
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&QQuickTextInput::inputMaskChanged)) {
                *result = 21;
                return;
            }
        }
        {
            typedef void (QQuickTextInput::*_t)(QQuickTextInput::EchoMode );
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&QQuickTextInput::echoModeChanged)) {
                *result = 22;
                return;
            }
        }
        {
            typedef void (QQuickTextInput::*_t)();
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&QQuickTextInput::passwordCharacterChanged)) {
                *result = 23;
                return;
            }
        }
        {
            typedef void (QQuickTextInput::*_t)(int );
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&QQuickTextInput::passwordMaskDelayChanged)) {
                *result = 24;
                return;
            }
        }
        {
            typedef void (QQuickTextInput::*_t)();
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&QQuickTextInput::displayTextChanged)) {
                *result = 25;
                return;
            }
        }
        {
            typedef void (QQuickTextInput::*_t)(bool );
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&QQuickTextInput::activeFocusOnPressChanged)) {
                *result = 26;
                return;
            }
        }
        {
            typedef void (QQuickTextInput::*_t)(bool );
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&QQuickTextInput::autoScrollChanged)) {
                *result = 27;
                return;
            }
        }
        {
            typedef void (QQuickTextInput::*_t)(bool );
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&QQuickTextInput::selectByMouseChanged)) {
                *result = 28;
                return;
            }
        }
        {
            typedef void (QQuickTextInput::*_t)(QQuickTextInput::SelectionMode );
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&QQuickTextInput::mouseSelectionModeChanged)) {
                *result = 29;
                return;
            }
        }
        {
            typedef void (QQuickTextInput::*_t)();
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&QQuickTextInput::persistentSelectionChanged)) {
                *result = 30;
                return;
            }
        }
        {
            typedef void (QQuickTextInput::*_t)();
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&QQuickTextInput::canPasteChanged)) {
                *result = 31;
                return;
            }
        }
        {
            typedef void (QQuickTextInput::*_t)();
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&QQuickTextInput::canUndoChanged)) {
                *result = 32;
                return;
            }
        }
        {
            typedef void (QQuickTextInput::*_t)();
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&QQuickTextInput::canRedoChanged)) {
                *result = 33;
                return;
            }
        }
        {
            typedef void (QQuickTextInput::*_t)();
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&QQuickTextInput::inputMethodComposingChanged)) {
                *result = 34;
                return;
            }
        }
        {
            typedef void (QQuickTextInput::*_t)();
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&QQuickTextInput::effectiveHorizontalAlignmentChanged)) {
                *result = 35;
                return;
            }
        }
        {
            typedef void (QQuickTextInput::*_t)();
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&QQuickTextInput::contentSizeChanged)) {
                *result = 36;
                return;
            }
        }
        {
            typedef void (QQuickTextInput::*_t)();
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&QQuickTextInput::inputMethodHintsChanged)) {
                *result = 37;
                return;
            }
        }
        {
            typedef void (QQuickTextInput::*_t)();
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&QQuickTextInput::renderTypeChanged)) {
                *result = 38;
                return;
            }
        }
        {
            typedef void (QQuickTextInput::*_t)();
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&QQuickTextInput::paddingChanged)) {
                *result = 39;
                return;
            }
        }
        {
            typedef void (QQuickTextInput::*_t)();
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&QQuickTextInput::topPaddingChanged)) {
                *result = 40;
                return;
            }
        }
        {
            typedef void (QQuickTextInput::*_t)();
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&QQuickTextInput::leftPaddingChanged)) {
                *result = 41;
                return;
            }
        }
        {
            typedef void (QQuickTextInput::*_t)();
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&QQuickTextInput::rightPaddingChanged)) {
                *result = 42;
                return;
            }
        }
        {
            typedef void (QQuickTextInput::*_t)();
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&QQuickTextInput::bottomPaddingChanged)) {
                *result = 43;
                return;
            }
        }
    } else if (_c == QMetaObject::RegisterPropertyMetaType) {
        switch (_id) {
        default: *reinterpret_cast<int*>(_a[0]) = -1; break;
        case 14:
            *reinterpret_cast<int*>(_a[0]) = qRegisterMetaType< QQmlComponent* >(); break;
        case 19:
            *reinterpret_cast<int*>(_a[0]) = qRegisterMetaType< QValidator* >(); break;
        }
    }

#ifndef QT_NO_PROPERTIES
    else if (_c == QMetaObject::ReadProperty) {
        QQuickTextInput *_t = static_cast<QQuickTextInput *>(_o);
        Q_UNUSED(_t)
        void *_v = _a[0];
        switch (_id) {
        case 0: *reinterpret_cast< QString*>(_v) = _t->text(); break;
        case 1: *reinterpret_cast< int*>(_v) = _t->length(); break;
        case 2: *reinterpret_cast< QColor*>(_v) = _t->color(); break;
        case 3: *reinterpret_cast< QColor*>(_v) = _t->selectionColor(); break;
        case 4: *reinterpret_cast< QColor*>(_v) = _t->selectedTextColor(); break;
        case 5: *reinterpret_cast< QFont*>(_v) = _t->font(); break;
        case 6: *reinterpret_cast< HAlignment*>(_v) = _t->hAlign(); break;
        case 7: *reinterpret_cast< HAlignment*>(_v) = _t->effectiveHAlign(); break;
        case 8: *reinterpret_cast< VAlignment*>(_v) = _t->vAlign(); break;
        case 9: *reinterpret_cast< WrapMode*>(_v) = _t->wrapMode(); break;
        case 10: *reinterpret_cast< bool*>(_v) = _t->isReadOnly(); break;
        case 11: *reinterpret_cast< bool*>(_v) = _t->isCursorVisible(); break;
        case 12: *reinterpret_cast< int*>(_v) = _t->cursorPosition(); break;
        case 13: *reinterpret_cast< QRectF*>(_v) = _t->cursorRectangle(); break;
        case 14: *reinterpret_cast< QQmlComponent**>(_v) = _t->cursorDelegate(); break;
        case 15: *reinterpret_cast< int*>(_v) = _t->selectionStart(); break;
        case 16: *reinterpret_cast< int*>(_v) = _t->selectionEnd(); break;
        case 17: *reinterpret_cast< QString*>(_v) = _t->selectedText(); break;
        case 18: *reinterpret_cast< int*>(_v) = _t->maxLength(); break;
        case 19: *reinterpret_cast< QValidator**>(_v) = _t->validator(); break;
        case 20: *reinterpret_cast< QString*>(_v) = _t->inputMask(); break;
        case 21: *reinterpret_cast< Qt::InputMethodHints*>(_v) = _t->inputMethodHints(); break;
        case 22: *reinterpret_cast< bool*>(_v) = _t->hasAcceptableInput(); break;
        case 23: *reinterpret_cast< EchoMode*>(_v) = _t->echoMode(); break;
        case 24: *reinterpret_cast< bool*>(_v) = _t->focusOnPress(); break;
        case 25: *reinterpret_cast< QString*>(_v) = _t->passwordCharacter(); break;
        case 26: *reinterpret_cast< int*>(_v) = _t->passwordMaskDelay(); break;
        case 27: *reinterpret_cast< QString*>(_v) = _t->displayText(); break;
        case 28: *reinterpret_cast< bool*>(_v) = _t->autoScroll(); break;
        case 29: *reinterpret_cast< bool*>(_v) = _t->selectByMouse(); break;
        case 30: *reinterpret_cast< SelectionMode*>(_v) = _t->mouseSelectionMode(); break;
        case 31: *reinterpret_cast< bool*>(_v) = _t->persistentSelection(); break;
        case 32: *reinterpret_cast< bool*>(_v) = _t->canPaste(); break;
        case 33: *reinterpret_cast< bool*>(_v) = _t->canUndo(); break;
        case 34: *reinterpret_cast< bool*>(_v) = _t->canRedo(); break;
        case 35: *reinterpret_cast< bool*>(_v) = _t->isInputMethodComposing(); break;
        case 36: *reinterpret_cast< qreal*>(_v) = _t->contentWidth(); break;
        case 37: *reinterpret_cast< qreal*>(_v) = _t->contentHeight(); break;
        case 38: *reinterpret_cast< RenderType*>(_v) = _t->renderType(); break;
        case 39: *reinterpret_cast< qreal*>(_v) = _t->padding(); break;
        case 40: *reinterpret_cast< qreal*>(_v) = _t->topPadding(); break;
        case 41: *reinterpret_cast< qreal*>(_v) = _t->leftPadding(); break;
        case 42: *reinterpret_cast< qreal*>(_v) = _t->rightPadding(); break;
        case 43: *reinterpret_cast< qreal*>(_v) = _t->bottomPadding(); break;
        default: break;
        }
    } else if (_c == QMetaObject::WriteProperty) {
        QQuickTextInput *_t = static_cast<QQuickTextInput *>(_o);
        Q_UNUSED(_t)
        void *_v = _a[0];
        switch (_id) {
        case 0: _t->setText(*reinterpret_cast< QString*>(_v)); break;
        case 2: _t->setColor(*reinterpret_cast< QColor*>(_v)); break;
        case 3: _t->setSelectionColor(*reinterpret_cast< QColor*>(_v)); break;
        case 4: _t->setSelectedTextColor(*reinterpret_cast< QColor*>(_v)); break;
        case 5: _t->setFont(*reinterpret_cast< QFont*>(_v)); break;
        case 6: _t->setHAlign(*reinterpret_cast< HAlignment*>(_v)); break;
        case 8: _t->setVAlign(*reinterpret_cast< VAlignment*>(_v)); break;
        case 9: _t->setWrapMode(*reinterpret_cast< WrapMode*>(_v)); break;
        case 10: _t->setReadOnly(*reinterpret_cast< bool*>(_v)); break;
        case 11: _t->setCursorVisible(*reinterpret_cast< bool*>(_v)); break;
        case 12: _t->setCursorPosition(*reinterpret_cast< int*>(_v)); break;
        case 14: _t->setCursorDelegate(*reinterpret_cast< QQmlComponent**>(_v)); break;
        case 18: _t->setMaxLength(*reinterpret_cast< int*>(_v)); break;
        case 19: _t->setValidator(*reinterpret_cast< QValidator**>(_v)); break;
        case 20: _t->setInputMask(*reinterpret_cast< QString*>(_v)); break;
        case 21: _t->setInputMethodHints(*reinterpret_cast< Qt::InputMethodHints*>(_v)); break;
        case 23: _t->setEchoMode(*reinterpret_cast< EchoMode*>(_v)); break;
        case 24: _t->setFocusOnPress(*reinterpret_cast< bool*>(_v)); break;
        case 25: _t->setPasswordCharacter(*reinterpret_cast< QString*>(_v)); break;
        case 26: _t->setPasswordMaskDelay(*reinterpret_cast< int*>(_v)); break;
        case 28: _t->setAutoScroll(*reinterpret_cast< bool*>(_v)); break;
        case 29: _t->setSelectByMouse(*reinterpret_cast< bool*>(_v)); break;
        case 30: _t->setMouseSelectionMode(*reinterpret_cast< SelectionMode*>(_v)); break;
        case 31: _t->setPersistentSelection(*reinterpret_cast< bool*>(_v)); break;
        case 38: _t->setRenderType(*reinterpret_cast< RenderType*>(_v)); break;
        case 39: _t->setPadding(*reinterpret_cast< qreal*>(_v)); break;
        case 40: _t->setTopPadding(*reinterpret_cast< qreal*>(_v)); break;
        case 41: _t->setLeftPadding(*reinterpret_cast< qreal*>(_v)); break;
        case 42: _t->setRightPadding(*reinterpret_cast< qreal*>(_v)); break;
        case 43: _t->setBottomPadding(*reinterpret_cast< qreal*>(_v)); break;
        default: break;
        }
    } else if (_c == QMetaObject::ResetProperty) {
        QQuickTextInput *_t = static_cast<QQuickTextInput *>(_o);
        Q_UNUSED(_t)
        switch (_id) {
        case 6: _t->resetHAlign(); break;
        case 26: _t->resetPasswordMaskDelay(); break;
        case 39: _t->resetPadding(); break;
        case 40: _t->resetTopPadding(); break;
        case 41: _t->resetLeftPadding(); break;
        case 42: _t->resetRightPadding(); break;
        case 43: _t->resetBottomPadding(); break;
        default: break;
        }
    }
#endif // QT_NO_PROPERTIES
}

const QMetaObject QQuickTextInput::staticMetaObject = {
    { &QQuickImplicitSizeItem::staticMetaObject, qt_meta_stringdata_QQuickTextInput.data,
      qt_meta_data_QQuickTextInput,  qt_static_metacall, Q_NULLPTR, Q_NULLPTR}
};


const QMetaObject *QQuickTextInput::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->dynamicMetaObject() : &staticMetaObject;
}

void *QQuickTextInput::qt_metacast(const char *_clname)
{
    if (!_clname) return Q_NULLPTR;
    if (!strcmp(_clname, qt_meta_stringdata_QQuickTextInput.stringdata0))
        return static_cast<void*>(const_cast< QQuickTextInput*>(this));
    return QQuickImplicitSizeItem::qt_metacast(_clname);
}

int QQuickTextInput::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QQuickImplicitSizeItem::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 71)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 71;
    } else if (_c == QMetaObject::RegisterMethodArgumentMetaType) {
        if (_id < 71)
            *reinterpret_cast<int*>(_a[0]) = -1;
        _id -= 71;
    }
#ifndef QT_NO_PROPERTIES
   else if (_c == QMetaObject::ReadProperty || _c == QMetaObject::WriteProperty
            || _c == QMetaObject::ResetProperty || _c == QMetaObject::RegisterPropertyMetaType) {
        qt_static_metacall(this, _c, _id, _a);
        _id -= 44;
    } else if (_c == QMetaObject::QueryPropertyDesignable) {
        _id -= 44;
    } else if (_c == QMetaObject::QueryPropertyScriptable) {
        _id -= 44;
    } else if (_c == QMetaObject::QueryPropertyStored) {
        _id -= 44;
    } else if (_c == QMetaObject::QueryPropertyEditable) {
        _id -= 44;
    } else if (_c == QMetaObject::QueryPropertyUser) {
        _id -= 44;
    }
#endif // QT_NO_PROPERTIES
    return _id;
}

// SIGNAL 0
void QQuickTextInput::textChanged()
{
    QMetaObject::activate(this, &staticMetaObject, 0, Q_NULLPTR);
}

// SIGNAL 1
void QQuickTextInput::cursorPositionChanged()
{
    QMetaObject::activate(this, &staticMetaObject, 1, Q_NULLPTR);
}

// SIGNAL 2
void QQuickTextInput::cursorRectangleChanged()
{
    QMetaObject::activate(this, &staticMetaObject, 2, Q_NULLPTR);
}

// SIGNAL 3
void QQuickTextInput::selectionStartChanged()
{
    QMetaObject::activate(this, &staticMetaObject, 3, Q_NULLPTR);
}

// SIGNAL 4
void QQuickTextInput::selectionEndChanged()
{
    QMetaObject::activate(this, &staticMetaObject, 4, Q_NULLPTR);
}

// SIGNAL 5
void QQuickTextInput::selectedTextChanged()
{
    QMetaObject::activate(this, &staticMetaObject, 5, Q_NULLPTR);
}

// SIGNAL 6
void QQuickTextInput::accepted()
{
    QMetaObject::activate(this, &staticMetaObject, 6, Q_NULLPTR);
}

// SIGNAL 7
void QQuickTextInput::acceptableInputChanged()
{
    QMetaObject::activate(this, &staticMetaObject, 7, Q_NULLPTR);
}

// SIGNAL 8
void QQuickTextInput::editingFinished()
{
    QMetaObject::activate(this, &staticMetaObject, 8, Q_NULLPTR);
}

// SIGNAL 9
void QQuickTextInput::colorChanged()
{
    QMetaObject::activate(this, &staticMetaObject, 9, Q_NULLPTR);
}

// SIGNAL 10
void QQuickTextInput::selectionColorChanged()
{
    QMetaObject::activate(this, &staticMetaObject, 10, Q_NULLPTR);
}

// SIGNAL 11
void QQuickTextInput::selectedTextColorChanged()
{
    QMetaObject::activate(this, &staticMetaObject, 11, Q_NULLPTR);
}

// SIGNAL 12
void QQuickTextInput::fontChanged(const QFont & _t1)
{
    void *_a[] = { Q_NULLPTR, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 12, _a);
}

// SIGNAL 13
void QQuickTextInput::horizontalAlignmentChanged(QQuickTextInput::HAlignment _t1)
{
    void *_a[] = { Q_NULLPTR, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 13, _a);
}

// SIGNAL 14
void QQuickTextInput::verticalAlignmentChanged(QQuickTextInput::VAlignment _t1)
{
    void *_a[] = { Q_NULLPTR, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 14, _a);
}

// SIGNAL 15
void QQuickTextInput::wrapModeChanged()
{
    QMetaObject::activate(this, &staticMetaObject, 15, Q_NULLPTR);
}

// SIGNAL 16
void QQuickTextInput::readOnlyChanged(bool _t1)
{
    void *_a[] = { Q_NULLPTR, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 16, _a);
}

// SIGNAL 17
void QQuickTextInput::cursorVisibleChanged(bool _t1)
{
    void *_a[] = { Q_NULLPTR, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 17, _a);
}

// SIGNAL 18
void QQuickTextInput::cursorDelegateChanged()
{
    QMetaObject::activate(this, &staticMetaObject, 18, Q_NULLPTR);
}

// SIGNAL 19
void QQuickTextInput::maximumLengthChanged(int _t1)
{
    void *_a[] = { Q_NULLPTR, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 19, _a);
}

// SIGNAL 20
void QQuickTextInput::validatorChanged()
{
    QMetaObject::activate(this, &staticMetaObject, 20, Q_NULLPTR);
}

// SIGNAL 21
void QQuickTextInput::inputMaskChanged(const QString & _t1)
{
    void *_a[] = { Q_NULLPTR, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 21, _a);
}

// SIGNAL 22
void QQuickTextInput::echoModeChanged(QQuickTextInput::EchoMode _t1)
{
    void *_a[] = { Q_NULLPTR, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 22, _a);
}

// SIGNAL 23
void QQuickTextInput::passwordCharacterChanged()
{
    QMetaObject::activate(this, &staticMetaObject, 23, Q_NULLPTR);
}

// SIGNAL 24
void QQuickTextInput::passwordMaskDelayChanged(int _t1)
{
    void *_a[] = { Q_NULLPTR, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 24, _a);
}

// SIGNAL 25
void QQuickTextInput::displayTextChanged()
{
    QMetaObject::activate(this, &staticMetaObject, 25, Q_NULLPTR);
}

// SIGNAL 26
void QQuickTextInput::activeFocusOnPressChanged(bool _t1)
{
    void *_a[] = { Q_NULLPTR, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 26, _a);
}

// SIGNAL 27
void QQuickTextInput::autoScrollChanged(bool _t1)
{
    void *_a[] = { Q_NULLPTR, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 27, _a);
}

// SIGNAL 28
void QQuickTextInput::selectByMouseChanged(bool _t1)
{
    void *_a[] = { Q_NULLPTR, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 28, _a);
}

// SIGNAL 29
void QQuickTextInput::mouseSelectionModeChanged(QQuickTextInput::SelectionMode _t1)
{
    void *_a[] = { Q_NULLPTR, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 29, _a);
}

// SIGNAL 30
void QQuickTextInput::persistentSelectionChanged()
{
    QMetaObject::activate(this, &staticMetaObject, 30, Q_NULLPTR);
}

// SIGNAL 31
void QQuickTextInput::canPasteChanged()
{
    QMetaObject::activate(this, &staticMetaObject, 31, Q_NULLPTR);
}

// SIGNAL 32
void QQuickTextInput::canUndoChanged()
{
    QMetaObject::activate(this, &staticMetaObject, 32, Q_NULLPTR);
}

// SIGNAL 33
void QQuickTextInput::canRedoChanged()
{
    QMetaObject::activate(this, &staticMetaObject, 33, Q_NULLPTR);
}

// SIGNAL 34
void QQuickTextInput::inputMethodComposingChanged()
{
    QMetaObject::activate(this, &staticMetaObject, 34, Q_NULLPTR);
}

// SIGNAL 35
void QQuickTextInput::effectiveHorizontalAlignmentChanged()
{
    QMetaObject::activate(this, &staticMetaObject, 35, Q_NULLPTR);
}

// SIGNAL 36
void QQuickTextInput::contentSizeChanged()
{
    QMetaObject::activate(this, &staticMetaObject, 36, Q_NULLPTR);
}

// SIGNAL 37
void QQuickTextInput::inputMethodHintsChanged()
{
    QMetaObject::activate(this, &staticMetaObject, 37, Q_NULLPTR);
}

// SIGNAL 38
void QQuickTextInput::renderTypeChanged()
{
    QMetaObject::activate(this, &staticMetaObject, 38, Q_NULLPTR);
}

// SIGNAL 39
void QQuickTextInput::paddingChanged()
{
    QMetaObject::activate(this, &staticMetaObject, 39, Q_NULLPTR);
}

// SIGNAL 40
void QQuickTextInput::topPaddingChanged()
{
    QMetaObject::activate(this, &staticMetaObject, 40, Q_NULLPTR);
}

// SIGNAL 41
void QQuickTextInput::leftPaddingChanged()
{
    QMetaObject::activate(this, &staticMetaObject, 41, Q_NULLPTR);
}

// SIGNAL 42
void QQuickTextInput::rightPaddingChanged()
{
    QMetaObject::activate(this, &staticMetaObject, 42, Q_NULLPTR);
}

// SIGNAL 43
void QQuickTextInput::bottomPaddingChanged()
{
    QMetaObject::activate(this, &staticMetaObject, 43, Q_NULLPTR);
}
QT_END_MOC_NAMESPACE
