/****************************************************************************
** Meta object code from reading C++ file 'qquickapplication_p.h'
**
** Created by: The Qt Meta Object Compiler version 67 (Qt 5.6.3)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "../../util/qquickapplication_p.h"
#include <QtCore/qbytearray.h>
#include <QtCore/qmetatype.h>
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'qquickapplication_p.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 67
#error "This file was generated using the moc from 5.6.3. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
struct qt_meta_stringdata_QQuickApplication_t {
    QByteArrayData data[11];
    char stringdata0[163];
};
#define QT_MOC_LITERAL(idx, ofs, len) \
    Q_STATIC_BYTE_ARRAY_DATA_HEADER_INITIALIZER_WITH_OFFSET(len, \
    qptrdiff(offsetof(qt_meta_stringdata_QQuickApplication_t, stringdata0) + ofs \
        - idx * sizeof(QByteArrayData)) \
    )
static const qt_meta_stringdata_QQuickApplication_t qt_meta_stringdata_QQuickApplication = {
    {
QT_MOC_LITERAL(0, 0, 17), // "QQuickApplication"
QT_MOC_LITERAL(1, 18, 13), // "activeChanged"
QT_MOC_LITERAL(2, 32, 0), // ""
QT_MOC_LITERAL(3, 33, 22), // "layoutDirectionChanged"
QT_MOC_LITERAL(4, 56, 12), // "stateChanged"
QT_MOC_LITERAL(5, 69, 20), // "Qt::ApplicationState"
QT_MOC_LITERAL(6, 90, 5), // "state"
QT_MOC_LITERAL(7, 96, 6), // "active"
QT_MOC_LITERAL(8, 103, 15), // "layoutDirection"
QT_MOC_LITERAL(9, 119, 19), // "Qt::LayoutDirection"
QT_MOC_LITERAL(10, 139, 23) // "supportsMultipleWindows"

    },
    "QQuickApplication\0activeChanged\0\0"
    "layoutDirectionChanged\0stateChanged\0"
    "Qt::ApplicationState\0state\0active\0"
    "layoutDirection\0Qt::LayoutDirection\0"
    "supportsMultipleWindows"
};
#undef QT_MOC_LITERAL

static const uint qt_meta_data_QQuickApplication[] = {

 // content:
       7,       // revision
       0,       // classname
       0,    0, // classinfo
       3,   14, // methods
       4,   34, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       3,       // signalCount

 // signals: name, argc, parameters, tag, flags
       1,    0,   29,    2, 0x06 /* Public */,
       3,    0,   30,    2, 0x06 /* Public */,
       4,    1,   31,    2, 0x06 /* Public */,

 // signals: parameters
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void, 0x80000000 | 5,    6,

 // properties: name, type, flags
       7, QMetaType::Bool, 0x00495001,
       8, 0x80000000 | 9, 0x00495009,
      10, QMetaType::Bool, 0x00095401,
       6, 0x80000000 | 5, 0x00495009,

 // properties: notify_signal_id
       0,
       1,
       0,
       2,

       0        // eod
};

void QQuickApplication::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        QQuickApplication *_t = static_cast<QQuickApplication *>(_o);
        Q_UNUSED(_t)
        switch (_id) {
        case 0: _t->activeChanged(); break;
        case 1: _t->layoutDirectionChanged(); break;
        case 2: _t->stateChanged((*reinterpret_cast< Qt::ApplicationState(*)>(_a[1]))); break;
        default: ;
        }
    } else if (_c == QMetaObject::IndexOfMethod) {
        int *result = reinterpret_cast<int *>(_a[0]);
        void **func = reinterpret_cast<void **>(_a[1]);
        {
            typedef void (QQuickApplication::*_t)();
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&QQuickApplication::activeChanged)) {
                *result = 0;
                return;
            }
        }
        {
            typedef void (QQuickApplication::*_t)();
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&QQuickApplication::layoutDirectionChanged)) {
                *result = 1;
                return;
            }
        }
        {
            typedef void (QQuickApplication::*_t)(Qt::ApplicationState );
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&QQuickApplication::stateChanged)) {
                *result = 2;
                return;
            }
        }
    }
#ifndef QT_NO_PROPERTIES
    else if (_c == QMetaObject::ReadProperty) {
        QQuickApplication *_t = static_cast<QQuickApplication *>(_o);
        Q_UNUSED(_t)
        void *_v = _a[0];
        switch (_id) {
        case 0: *reinterpret_cast< bool*>(_v) = _t->active(); break;
        case 1: *reinterpret_cast< Qt::LayoutDirection*>(_v) = _t->layoutDirection(); break;
        case 2: *reinterpret_cast< bool*>(_v) = _t->supportsMultipleWindows(); break;
        case 3: *reinterpret_cast< Qt::ApplicationState*>(_v) = _t->state(); break;
        default: break;
        }
    } else if (_c == QMetaObject::WriteProperty) {
    } else if (_c == QMetaObject::ResetProperty) {
    }
#endif // QT_NO_PROPERTIES
}

const QMetaObject QQuickApplication::staticMetaObject = {
    { &QQmlApplication::staticMetaObject, qt_meta_stringdata_QQuickApplication.data,
      qt_meta_data_QQuickApplication,  qt_static_metacall, Q_NULLPTR, Q_NULLPTR}
};


const QMetaObject *QQuickApplication::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->dynamicMetaObject() : &staticMetaObject;
}

void *QQuickApplication::qt_metacast(const char *_clname)
{
    if (!_clname) return Q_NULLPTR;
    if (!strcmp(_clname, qt_meta_stringdata_QQuickApplication.stringdata0))
        return static_cast<void*>(const_cast< QQuickApplication*>(this));
    return QQmlApplication::qt_metacast(_clname);
}

int QQuickApplication::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QQmlApplication::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 3)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 3;
    } else if (_c == QMetaObject::RegisterMethodArgumentMetaType) {
        if (_id < 3)
            *reinterpret_cast<int*>(_a[0]) = -1;
        _id -= 3;
    }
#ifndef QT_NO_PROPERTIES
   else if (_c == QMetaObject::ReadProperty || _c == QMetaObject::WriteProperty
            || _c == QMetaObject::ResetProperty || _c == QMetaObject::RegisterPropertyMetaType) {
        qt_static_metacall(this, _c, _id, _a);
        _id -= 4;
    } else if (_c == QMetaObject::QueryPropertyDesignable) {
        _id -= 4;
    } else if (_c == QMetaObject::QueryPropertyScriptable) {
        _id -= 4;
    } else if (_c == QMetaObject::QueryPropertyStored) {
        _id -= 4;
    } else if (_c == QMetaObject::QueryPropertyEditable) {
        _id -= 4;
    } else if (_c == QMetaObject::QueryPropertyUser) {
        _id -= 4;
    }
#endif // QT_NO_PROPERTIES
    return _id;
}

// SIGNAL 0
void QQuickApplication::activeChanged()
{
    QMetaObject::activate(this, &staticMetaObject, 0, Q_NULLPTR);
}

// SIGNAL 1
void QQuickApplication::layoutDirectionChanged()
{
    QMetaObject::activate(this, &staticMetaObject, 1, Q_NULLPTR);
}

// SIGNAL 2
void QQuickApplication::stateChanged(Qt::ApplicationState _t1)
{
    void *_a[] = { Q_NULLPTR, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 2, _a);
}
QT_END_MOC_NAMESPACE
