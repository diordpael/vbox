/****************************************************************************
** Meta object code from reading C++ file 'qv4profiling_p.h'
**
** Created by: The Qt Meta Object Compiler version 67 (Qt 5.6.3)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "../../jsruntime/qv4profiling_p.h"
#include <QtCore/qbytearray.h>
#include <QtCore/qmetatype.h>
#include <QtCore/QVector>
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'qv4profiling_p.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 67
#error "This file was generated using the moc from 5.6.3. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
struct qt_meta_stringdata_QV4__Profiling__Profiler_t {
    QByteArrayData data[12];
    char stringdata0[214];
};
#define QT_MOC_LITERAL(idx, ofs, len) \
    Q_STATIC_BYTE_ARRAY_DATA_HEADER_INITIALIZER_WITH_OFFSET(len, \
    qptrdiff(offsetof(qt_meta_stringdata_QV4__Profiling__Profiler_t, stringdata0) + ofs \
        - idx * sizeof(QByteArrayData)) \
    )
static const qt_meta_stringdata_QV4__Profiling__Profiler_t qt_meta_stringdata_QV4__Profiling__Profiler = {
    {
QT_MOC_LITERAL(0, 0, 24), // "QV4::Profiling::Profiler"
QT_MOC_LITERAL(1, 25, 9), // "dataReady"
QT_MOC_LITERAL(2, 35, 0), // ""
QT_MOC_LITERAL(3, 36, 47), // "QVector<QV4::Profiling::Funct..."
QT_MOC_LITERAL(4, 84, 51), // "QVector<QV4::Profiling::Memor..."
QT_MOC_LITERAL(5, 136, 13), // "stopProfiling"
QT_MOC_LITERAL(6, 150, 14), // "startProfiling"
QT_MOC_LITERAL(7, 165, 8), // "features"
QT_MOC_LITERAL(8, 174, 10), // "reportData"
QT_MOC_LITERAL(9, 185, 8), // "setTimer"
QT_MOC_LITERAL(10, 194, 13), // "QElapsedTimer"
QT_MOC_LITERAL(11, 208, 5) // "timer"

    },
    "QV4::Profiling::Profiler\0dataReady\0\0"
    "QVector<QV4::Profiling::FunctionCallProperties>\0"
    "QVector<QV4::Profiling::MemoryAllocationProperties>\0"
    "stopProfiling\0startProfiling\0features\0"
    "reportData\0setTimer\0QElapsedTimer\0"
    "timer"
};
#undef QT_MOC_LITERAL

static const uint qt_meta_data_QV4__Profiling__Profiler[] = {

 // content:
       7,       // revision
       0,       // classname
       0,    0, // classinfo
       5,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       1,       // signalCount

 // signals: name, argc, parameters, tag, flags
       1,    2,   39,    2, 0x06 /* Public */,

 // slots: name, argc, parameters, tag, flags
       5,    0,   44,    2, 0x0a /* Public */,
       6,    1,   45,    2, 0x0a /* Public */,
       8,    0,   48,    2, 0x0a /* Public */,
       9,    1,   49,    2, 0x0a /* Public */,

 // signals: parameters
    QMetaType::Void, 0x80000000 | 3, 0x80000000 | 4,    2,    2,

 // slots: parameters
    QMetaType::Void,
    QMetaType::Void, QMetaType::ULongLong,    7,
    QMetaType::Void,
    QMetaType::Void, 0x80000000 | 10,   11,

       0        // eod
};

void QV4::Profiling::Profiler::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        Profiler *_t = static_cast<Profiler *>(_o);
        Q_UNUSED(_t)
        switch (_id) {
        case 0: _t->dataReady((*reinterpret_cast< const QVector<QV4::Profiling::FunctionCallProperties>(*)>(_a[1])),(*reinterpret_cast< const QVector<QV4::Profiling::MemoryAllocationProperties>(*)>(_a[2]))); break;
        case 1: _t->stopProfiling(); break;
        case 2: _t->startProfiling((*reinterpret_cast< quint64(*)>(_a[1]))); break;
        case 3: _t->reportData(); break;
        case 4: _t->setTimer((*reinterpret_cast< const QElapsedTimer(*)>(_a[1]))); break;
        default: ;
        }
    } else if (_c == QMetaObject::RegisterMethodArgumentMetaType) {
        switch (_id) {
        default: *reinterpret_cast<int*>(_a[0]) = -1; break;
        case 0:
            switch (*reinterpret_cast<int*>(_a[1])) {
            default: *reinterpret_cast<int*>(_a[0]) = -1; break;
            case 0:
                *reinterpret_cast<int*>(_a[0]) = qRegisterMetaType< QVector<QV4::Profiling::FunctionCallProperties> >(); break;
            case 1:
                *reinterpret_cast<int*>(_a[0]) = qRegisterMetaType< QVector<QV4::Profiling::MemoryAllocationProperties> >(); break;
            }
            break;
        }
    } else if (_c == QMetaObject::IndexOfMethod) {
        int *result = reinterpret_cast<int *>(_a[0]);
        void **func = reinterpret_cast<void **>(_a[1]);
        {
            typedef void (Profiler::*_t)(const QVector<QV4::Profiling::FunctionCallProperties> & , const QVector<QV4::Profiling::MemoryAllocationProperties> & );
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&Profiler::dataReady)) {
                *result = 0;
                return;
            }
        }
    }
}

const QMetaObject QV4::Profiling::Profiler::staticMetaObject = {
    { &QObject::staticMetaObject, qt_meta_stringdata_QV4__Profiling__Profiler.data,
      qt_meta_data_QV4__Profiling__Profiler,  qt_static_metacall, Q_NULLPTR, Q_NULLPTR}
};


const QMetaObject *QV4::Profiling::Profiler::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->dynamicMetaObject() : &staticMetaObject;
}

void *QV4::Profiling::Profiler::qt_metacast(const char *_clname)
{
    if (!_clname) return Q_NULLPTR;
    if (!strcmp(_clname, qt_meta_stringdata_QV4__Profiling__Profiler.stringdata0))
        return static_cast<void*>(const_cast< Profiler*>(this));
    return QObject::qt_metacast(_clname);
}

int QV4::Profiling::Profiler::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QObject::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 5)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 5;
    } else if (_c == QMetaObject::RegisterMethodArgumentMetaType) {
        if (_id < 5)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 5;
    }
    return _id;
}

// SIGNAL 0
void QV4::Profiling::Profiler::dataReady(const QVector<QV4::Profiling::FunctionCallProperties> & _t1, const QVector<QV4::Profiling::MemoryAllocationProperties> & _t2)
{
    void *_a[] = { Q_NULLPTR, const_cast<void*>(reinterpret_cast<const void*>(&_t1)), const_cast<void*>(reinterpret_cast<const void*>(&_t2)) };
    QMetaObject::activate(this, &staticMetaObject, 0, _a);
}
QT_END_MOC_NAMESPACE
