/****************************************************************************
** Meta object code from reading C++ file 'qqmldebugserviceinterfaces_p.h'
**
** Created by: The Qt Meta Object Compiler version 67 (Qt 5.6.3)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "../../debugger/qqmldebugserviceinterfaces_p.h"
#include <QtCore/qbytearray.h>
#include <QtCore/qmetatype.h>
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'qqmldebugserviceinterfaces_p.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 67
#error "This file was generated using the moc from 5.6.3. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
struct qt_meta_stringdata_QV4DebugService_t {
    QByteArrayData data[1];
    char stringdata0[16];
};
#define QT_MOC_LITERAL(idx, ofs, len) \
    Q_STATIC_BYTE_ARRAY_DATA_HEADER_INITIALIZER_WITH_OFFSET(len, \
    qptrdiff(offsetof(qt_meta_stringdata_QV4DebugService_t, stringdata0) + ofs \
        - idx * sizeof(QByteArrayData)) \
    )
static const qt_meta_stringdata_QV4DebugService_t qt_meta_stringdata_QV4DebugService = {
    {
QT_MOC_LITERAL(0, 0, 15) // "QV4DebugService"

    },
    "QV4DebugService"
};
#undef QT_MOC_LITERAL

static const uint qt_meta_data_QV4DebugService[] = {

 // content:
       7,       // revision
       0,       // classname
       0,    0, // classinfo
       0,    0, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       0,       // signalCount

       0        // eod
};

void QV4DebugService::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    Q_UNUSED(_o);
    Q_UNUSED(_id);
    Q_UNUSED(_c);
    Q_UNUSED(_a);
}

const QMetaObject QV4DebugService::staticMetaObject = {
    { &QQmlDebugService::staticMetaObject, qt_meta_stringdata_QV4DebugService.data,
      qt_meta_data_QV4DebugService,  qt_static_metacall, Q_NULLPTR, Q_NULLPTR}
};


const QMetaObject *QV4DebugService::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->dynamicMetaObject() : &staticMetaObject;
}

void *QV4DebugService::qt_metacast(const char *_clname)
{
    if (!_clname) return Q_NULLPTR;
    if (!strcmp(_clname, qt_meta_stringdata_QV4DebugService.stringdata0))
        return static_cast<void*>(const_cast< QV4DebugService*>(this));
    return QQmlDebugService::qt_metacast(_clname);
}

int QV4DebugService::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QQmlDebugService::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    return _id;
}
struct qt_meta_stringdata_QQmlProfilerService_t {
    QByteArrayData data[1];
    char stringdata0[20];
};
#define QT_MOC_LITERAL(idx, ofs, len) \
    Q_STATIC_BYTE_ARRAY_DATA_HEADER_INITIALIZER_WITH_OFFSET(len, \
    qptrdiff(offsetof(qt_meta_stringdata_QQmlProfilerService_t, stringdata0) + ofs \
        - idx * sizeof(QByteArrayData)) \
    )
static const qt_meta_stringdata_QQmlProfilerService_t qt_meta_stringdata_QQmlProfilerService = {
    {
QT_MOC_LITERAL(0, 0, 19) // "QQmlProfilerService"

    },
    "QQmlProfilerService"
};
#undef QT_MOC_LITERAL

static const uint qt_meta_data_QQmlProfilerService[] = {

 // content:
       7,       // revision
       0,       // classname
       0,    0, // classinfo
       0,    0, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       0,       // signalCount

       0        // eod
};

void QQmlProfilerService::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    Q_UNUSED(_o);
    Q_UNUSED(_id);
    Q_UNUSED(_c);
    Q_UNUSED(_a);
}

const QMetaObject QQmlProfilerService::staticMetaObject = {
    { &QQmlDebugService::staticMetaObject, qt_meta_stringdata_QQmlProfilerService.data,
      qt_meta_data_QQmlProfilerService,  qt_static_metacall, Q_NULLPTR, Q_NULLPTR}
};


const QMetaObject *QQmlProfilerService::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->dynamicMetaObject() : &staticMetaObject;
}

void *QQmlProfilerService::qt_metacast(const char *_clname)
{
    if (!_clname) return Q_NULLPTR;
    if (!strcmp(_clname, qt_meta_stringdata_QQmlProfilerService.stringdata0))
        return static_cast<void*>(const_cast< QQmlProfilerService*>(this));
    return QQmlDebugService::qt_metacast(_clname);
}

int QQmlProfilerService::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QQmlDebugService::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    return _id;
}
struct qt_meta_stringdata_QQmlEngineDebugService_t {
    QByteArrayData data[1];
    char stringdata0[23];
};
#define QT_MOC_LITERAL(idx, ofs, len) \
    Q_STATIC_BYTE_ARRAY_DATA_HEADER_INITIALIZER_WITH_OFFSET(len, \
    qptrdiff(offsetof(qt_meta_stringdata_QQmlEngineDebugService_t, stringdata0) + ofs \
        - idx * sizeof(QByteArrayData)) \
    )
static const qt_meta_stringdata_QQmlEngineDebugService_t qt_meta_stringdata_QQmlEngineDebugService = {
    {
QT_MOC_LITERAL(0, 0, 22) // "QQmlEngineDebugService"

    },
    "QQmlEngineDebugService"
};
#undef QT_MOC_LITERAL

static const uint qt_meta_data_QQmlEngineDebugService[] = {

 // content:
       7,       // revision
       0,       // classname
       0,    0, // classinfo
       0,    0, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       0,       // signalCount

       0        // eod
};

void QQmlEngineDebugService::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    Q_UNUSED(_o);
    Q_UNUSED(_id);
    Q_UNUSED(_c);
    Q_UNUSED(_a);
}

const QMetaObject QQmlEngineDebugService::staticMetaObject = {
    { &QQmlDebugService::staticMetaObject, qt_meta_stringdata_QQmlEngineDebugService.data,
      qt_meta_data_QQmlEngineDebugService,  qt_static_metacall, Q_NULLPTR, Q_NULLPTR}
};


const QMetaObject *QQmlEngineDebugService::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->dynamicMetaObject() : &staticMetaObject;
}

void *QQmlEngineDebugService::qt_metacast(const char *_clname)
{
    if (!_clname) return Q_NULLPTR;
    if (!strcmp(_clname, qt_meta_stringdata_QQmlEngineDebugService.stringdata0))
        return static_cast<void*>(const_cast< QQmlEngineDebugService*>(this));
    return QQmlDebugService::qt_metacast(_clname);
}

int QQmlEngineDebugService::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QQmlDebugService::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    return _id;
}
struct qt_meta_stringdata_QQmlInspectorService_t {
    QByteArrayData data[1];
    char stringdata0[21];
};
#define QT_MOC_LITERAL(idx, ofs, len) \
    Q_STATIC_BYTE_ARRAY_DATA_HEADER_INITIALIZER_WITH_OFFSET(len, \
    qptrdiff(offsetof(qt_meta_stringdata_QQmlInspectorService_t, stringdata0) + ofs \
        - idx * sizeof(QByteArrayData)) \
    )
static const qt_meta_stringdata_QQmlInspectorService_t qt_meta_stringdata_QQmlInspectorService = {
    {
QT_MOC_LITERAL(0, 0, 20) // "QQmlInspectorService"

    },
    "QQmlInspectorService"
};
#undef QT_MOC_LITERAL

static const uint qt_meta_data_QQmlInspectorService[] = {

 // content:
       7,       // revision
       0,       // classname
       0,    0, // classinfo
       0,    0, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       0,       // signalCount

       0        // eod
};

void QQmlInspectorService::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    Q_UNUSED(_o);
    Q_UNUSED(_id);
    Q_UNUSED(_c);
    Q_UNUSED(_a);
}

const QMetaObject QQmlInspectorService::staticMetaObject = {
    { &QQmlDebugService::staticMetaObject, qt_meta_stringdata_QQmlInspectorService.data,
      qt_meta_data_QQmlInspectorService,  qt_static_metacall, Q_NULLPTR, Q_NULLPTR}
};


const QMetaObject *QQmlInspectorService::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->dynamicMetaObject() : &staticMetaObject;
}

void *QQmlInspectorService::qt_metacast(const char *_clname)
{
    if (!_clname) return Q_NULLPTR;
    if (!strcmp(_clname, qt_meta_stringdata_QQmlInspectorService.stringdata0))
        return static_cast<void*>(const_cast< QQmlInspectorService*>(this));
    return QQmlDebugService::qt_metacast(_clname);
}

int QQmlInspectorService::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QQmlDebugService::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    return _id;
}
struct qt_meta_stringdata_QQmlNativeDebugService_t {
    QByteArrayData data[1];
    char stringdata0[23];
};
#define QT_MOC_LITERAL(idx, ofs, len) \
    Q_STATIC_BYTE_ARRAY_DATA_HEADER_INITIALIZER_WITH_OFFSET(len, \
    qptrdiff(offsetof(qt_meta_stringdata_QQmlNativeDebugService_t, stringdata0) + ofs \
        - idx * sizeof(QByteArrayData)) \
    )
static const qt_meta_stringdata_QQmlNativeDebugService_t qt_meta_stringdata_QQmlNativeDebugService = {
    {
QT_MOC_LITERAL(0, 0, 22) // "QQmlNativeDebugService"

    },
    "QQmlNativeDebugService"
};
#undef QT_MOC_LITERAL

static const uint qt_meta_data_QQmlNativeDebugService[] = {

 // content:
       7,       // revision
       0,       // classname
       0,    0, // classinfo
       0,    0, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       0,       // signalCount

       0        // eod
};

void QQmlNativeDebugService::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    Q_UNUSED(_o);
    Q_UNUSED(_id);
    Q_UNUSED(_c);
    Q_UNUSED(_a);
}

const QMetaObject QQmlNativeDebugService::staticMetaObject = {
    { &QQmlDebugService::staticMetaObject, qt_meta_stringdata_QQmlNativeDebugService.data,
      qt_meta_data_QQmlNativeDebugService,  qt_static_metacall, Q_NULLPTR, Q_NULLPTR}
};


const QMetaObject *QQmlNativeDebugService::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->dynamicMetaObject() : &staticMetaObject;
}

void *QQmlNativeDebugService::qt_metacast(const char *_clname)
{
    if (!_clname) return Q_NULLPTR;
    if (!strcmp(_clname, qt_meta_stringdata_QQmlNativeDebugService.stringdata0))
        return static_cast<void*>(const_cast< QQmlNativeDebugService*>(this));
    return QQmlDebugService::qt_metacast(_clname);
}

int QQmlNativeDebugService::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QQmlDebugService::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    return _id;
}
QT_END_MOC_NAMESPACE
