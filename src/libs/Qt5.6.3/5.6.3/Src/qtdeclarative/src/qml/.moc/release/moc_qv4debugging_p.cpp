/****************************************************************************
** Meta object code from reading C++ file 'qv4debugging_p.h'
**
** Created by: The Qt Meta Object Compiler version 67 (Qt 5.6.3)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "../../jsruntime/qv4debugging_p.h"
#include <QtCore/qbytearray.h>
#include <QtCore/qmetatype.h>
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'qv4debugging_p.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 67
#error "This file was generated using the moc from 5.6.3. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
struct qt_meta_stringdata_QV4__Debugging__Debugger_t {
    QByteArrayData data[1];
    char stringdata0[25];
};
#define QT_MOC_LITERAL(idx, ofs, len) \
    Q_STATIC_BYTE_ARRAY_DATA_HEADER_INITIALIZER_WITH_OFFSET(len, \
    qptrdiff(offsetof(qt_meta_stringdata_QV4__Debugging__Debugger_t, stringdata0) + ofs \
        - idx * sizeof(QByteArrayData)) \
    )
static const qt_meta_stringdata_QV4__Debugging__Debugger_t qt_meta_stringdata_QV4__Debugging__Debugger = {
    {
QT_MOC_LITERAL(0, 0, 24) // "QV4::Debugging::Debugger"

    },
    "QV4::Debugging::Debugger"
};
#undef QT_MOC_LITERAL

static const uint qt_meta_data_QV4__Debugging__Debugger[] = {

 // content:
       7,       // revision
       0,       // classname
       0,    0, // classinfo
       0,    0, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       0,       // signalCount

       0        // eod
};

void QV4::Debugging::Debugger::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    Q_UNUSED(_o);
    Q_UNUSED(_id);
    Q_UNUSED(_c);
    Q_UNUSED(_a);
}

const QMetaObject QV4::Debugging::Debugger::staticMetaObject = {
    { &QObject::staticMetaObject, qt_meta_stringdata_QV4__Debugging__Debugger.data,
      qt_meta_data_QV4__Debugging__Debugger,  qt_static_metacall, Q_NULLPTR, Q_NULLPTR}
};


const QMetaObject *QV4::Debugging::Debugger::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->dynamicMetaObject() : &staticMetaObject;
}

void *QV4::Debugging::Debugger::qt_metacast(const char *_clname)
{
    if (!_clname) return Q_NULLPTR;
    if (!strcmp(_clname, qt_meta_stringdata_QV4__Debugging__Debugger.stringdata0))
        return static_cast<void*>(const_cast< Debugger*>(this));
    return QObject::qt_metacast(_clname);
}

int QV4::Debugging::Debugger::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QObject::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    return _id;
}
struct qt_meta_stringdata_QV4__Debugging__V4Debugger_t {
    QByteArrayData data[10];
    char stringdata0[140];
};
#define QT_MOC_LITERAL(idx, ofs, len) \
    Q_STATIC_BYTE_ARRAY_DATA_HEADER_INITIALIZER_WITH_OFFSET(len, \
    qptrdiff(offsetof(qt_meta_stringdata_QV4__Debugging__V4Debugger_t, stringdata0) + ofs \
        - idx * sizeof(QByteArrayData)) \
    )
static const qt_meta_stringdata_QV4__Debugging__V4Debugger_t qt_meta_stringdata_QV4__Debugging__V4Debugger = {
    {
QT_MOC_LITERAL(0, 0, 26), // "QV4::Debugging::V4Debugger"
QT_MOC_LITERAL(1, 27, 16), // "sourcesCollected"
QT_MOC_LITERAL(2, 44, 0), // ""
QT_MOC_LITERAL(3, 45, 27), // "QV4::Debugging::V4Debugger*"
QT_MOC_LITERAL(4, 73, 4), // "self"
QT_MOC_LITERAL(5, 78, 7), // "sources"
QT_MOC_LITERAL(6, 86, 3), // "seq"
QT_MOC_LITERAL(7, 90, 14), // "debuggerPaused"
QT_MOC_LITERAL(8, 105, 27), // "QV4::Debugging::PauseReason"
QT_MOC_LITERAL(9, 133, 6) // "reason"

    },
    "QV4::Debugging::V4Debugger\0sourcesCollected\0"
    "\0QV4::Debugging::V4Debugger*\0self\0"
    "sources\0seq\0debuggerPaused\0"
    "QV4::Debugging::PauseReason\0reason"
};
#undef QT_MOC_LITERAL

static const uint qt_meta_data_QV4__Debugging__V4Debugger[] = {

 // content:
       7,       // revision
       0,       // classname
       0,    0, // classinfo
       2,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       2,       // signalCount

 // signals: name, argc, parameters, tag, flags
       1,    3,   24,    2, 0x06 /* Public */,
       7,    2,   31,    2, 0x06 /* Public */,

 // signals: parameters
    QMetaType::Void, 0x80000000 | 3, QMetaType::QStringList, QMetaType::Int,    4,    5,    6,
    QMetaType::Void, 0x80000000 | 3, 0x80000000 | 8,    4,    9,

       0        // eod
};

void QV4::Debugging::V4Debugger::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        V4Debugger *_t = static_cast<V4Debugger *>(_o);
        Q_UNUSED(_t)
        switch (_id) {
        case 0: _t->sourcesCollected((*reinterpret_cast< QV4::Debugging::V4Debugger*(*)>(_a[1])),(*reinterpret_cast< const QStringList(*)>(_a[2])),(*reinterpret_cast< int(*)>(_a[3]))); break;
        case 1: _t->debuggerPaused((*reinterpret_cast< QV4::Debugging::V4Debugger*(*)>(_a[1])),(*reinterpret_cast< QV4::Debugging::PauseReason(*)>(_a[2]))); break;
        default: ;
        }
    } else if (_c == QMetaObject::RegisterMethodArgumentMetaType) {
        switch (_id) {
        default: *reinterpret_cast<int*>(_a[0]) = -1; break;
        case 0:
            switch (*reinterpret_cast<int*>(_a[1])) {
            default: *reinterpret_cast<int*>(_a[0]) = -1; break;
            case 0:
                *reinterpret_cast<int*>(_a[0]) = qRegisterMetaType< QV4::Debugging::V4Debugger* >(); break;
            }
            break;
        case 1:
            switch (*reinterpret_cast<int*>(_a[1])) {
            default: *reinterpret_cast<int*>(_a[0]) = -1; break;
            case 1:
                *reinterpret_cast<int*>(_a[0]) = qRegisterMetaType< QV4::Debugging::PauseReason >(); break;
            case 0:
                *reinterpret_cast<int*>(_a[0]) = qRegisterMetaType< QV4::Debugging::V4Debugger* >(); break;
            }
            break;
        }
    } else if (_c == QMetaObject::IndexOfMethod) {
        int *result = reinterpret_cast<int *>(_a[0]);
        void **func = reinterpret_cast<void **>(_a[1]);
        {
            typedef void (V4Debugger::*_t)(QV4::Debugging::V4Debugger * , const QStringList & , int );
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&V4Debugger::sourcesCollected)) {
                *result = 0;
                return;
            }
        }
        {
            typedef void (V4Debugger::*_t)(QV4::Debugging::V4Debugger * , QV4::Debugging::PauseReason );
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&V4Debugger::debuggerPaused)) {
                *result = 1;
                return;
            }
        }
    }
}

const QMetaObject QV4::Debugging::V4Debugger::staticMetaObject = {
    { &Debugger::staticMetaObject, qt_meta_stringdata_QV4__Debugging__V4Debugger.data,
      qt_meta_data_QV4__Debugging__V4Debugger,  qt_static_metacall, Q_NULLPTR, Q_NULLPTR}
};


const QMetaObject *QV4::Debugging::V4Debugger::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->dynamicMetaObject() : &staticMetaObject;
}

void *QV4::Debugging::V4Debugger::qt_metacast(const char *_clname)
{
    if (!_clname) return Q_NULLPTR;
    if (!strcmp(_clname, qt_meta_stringdata_QV4__Debugging__V4Debugger.stringdata0))
        return static_cast<void*>(const_cast< V4Debugger*>(this));
    return Debugger::qt_metacast(_clname);
}

int QV4::Debugging::V4Debugger::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = Debugger::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 2)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 2;
    } else if (_c == QMetaObject::RegisterMethodArgumentMetaType) {
        if (_id < 2)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 2;
    }
    return _id;
}

// SIGNAL 0
void QV4::Debugging::V4Debugger::sourcesCollected(QV4::Debugging::V4Debugger * _t1, const QStringList & _t2, int _t3)
{
    void *_a[] = { Q_NULLPTR, const_cast<void*>(reinterpret_cast<const void*>(&_t1)), const_cast<void*>(reinterpret_cast<const void*>(&_t2)), const_cast<void*>(reinterpret_cast<const void*>(&_t3)) };
    QMetaObject::activate(this, &staticMetaObject, 0, _a);
}

// SIGNAL 1
void QV4::Debugging::V4Debugger::debuggerPaused(QV4::Debugging::V4Debugger * _t1, QV4::Debugging::PauseReason _t2)
{
    void *_a[] = { Q_NULLPTR, const_cast<void*>(reinterpret_cast<const void*>(&_t1)), const_cast<void*>(reinterpret_cast<const void*>(&_t2)) };
    QMetaObject::activate(this, &staticMetaObject, 1, _a);
}
QT_END_MOC_NAMESPACE
