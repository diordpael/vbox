/****************************************************************************
** Meta object code from reading C++ file 'qqmlprofiler_p.h'
**
** Created by: The Qt Meta Object Compiler version 67 (Qt 5.6.3)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "../../debugger/qqmlprofiler_p.h"
#include <QtCore/qbytearray.h>
#include <QtCore/qmetatype.h>
#include <QtCore/QVector>
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'qqmlprofiler_p.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 67
#error "This file was generated using the moc from 5.6.3. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
struct qt_meta_stringdata_QQmlProfiler_t {
    QByteArrayData data[11];
    char stringdata0[128];
};
#define QT_MOC_LITERAL(idx, ofs, len) \
    Q_STATIC_BYTE_ARRAY_DATA_HEADER_INITIALIZER_WITH_OFFSET(len, \
    qptrdiff(offsetof(qt_meta_stringdata_QQmlProfiler_t, stringdata0) + ofs \
        - idx * sizeof(QByteArrayData)) \
    )
static const qt_meta_stringdata_QQmlProfiler_t qt_meta_stringdata_QQmlProfiler = {
    {
QT_MOC_LITERAL(0, 0, 12), // "QQmlProfiler"
QT_MOC_LITERAL(1, 13, 9), // "dataReady"
QT_MOC_LITERAL(2, 23, 0), // ""
QT_MOC_LITERAL(3, 24, 25), // "QVector<QQmlProfilerData>"
QT_MOC_LITERAL(4, 50, 14), // "startProfiling"
QT_MOC_LITERAL(5, 65, 8), // "features"
QT_MOC_LITERAL(6, 74, 13), // "stopProfiling"
QT_MOC_LITERAL(7, 88, 10), // "reportData"
QT_MOC_LITERAL(8, 99, 8), // "setTimer"
QT_MOC_LITERAL(9, 108, 13), // "QElapsedTimer"
QT_MOC_LITERAL(10, 122, 5) // "timer"

    },
    "QQmlProfiler\0dataReady\0\0"
    "QVector<QQmlProfilerData>\0startProfiling\0"
    "features\0stopProfiling\0reportData\0"
    "setTimer\0QElapsedTimer\0timer"
};
#undef QT_MOC_LITERAL

static const uint qt_meta_data_QQmlProfiler[] = {

 // content:
       7,       // revision
       0,       // classname
       0,    0, // classinfo
       5,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       1,       // signalCount

 // signals: name, argc, parameters, tag, flags
       1,    1,   39,    2, 0x06 /* Public */,

 // slots: name, argc, parameters, tag, flags
       4,    1,   42,    2, 0x0a /* Public */,
       6,    0,   45,    2, 0x0a /* Public */,
       7,    0,   46,    2, 0x0a /* Public */,
       8,    1,   47,    2, 0x0a /* Public */,

 // signals: parameters
    QMetaType::Void, 0x80000000 | 3,    2,

 // slots: parameters
    QMetaType::Void, QMetaType::ULongLong,    5,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void, 0x80000000 | 9,   10,

       0        // eod
};

void QQmlProfiler::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        QQmlProfiler *_t = static_cast<QQmlProfiler *>(_o);
        Q_UNUSED(_t)
        switch (_id) {
        case 0: _t->dataReady((*reinterpret_cast< const QVector<QQmlProfilerData>(*)>(_a[1]))); break;
        case 1: _t->startProfiling((*reinterpret_cast< quint64(*)>(_a[1]))); break;
        case 2: _t->stopProfiling(); break;
        case 3: _t->reportData(); break;
        case 4: _t->setTimer((*reinterpret_cast< const QElapsedTimer(*)>(_a[1]))); break;
        default: ;
        }
    } else if (_c == QMetaObject::RegisterMethodArgumentMetaType) {
        switch (_id) {
        default: *reinterpret_cast<int*>(_a[0]) = -1; break;
        case 0:
            switch (*reinterpret_cast<int*>(_a[1])) {
            default: *reinterpret_cast<int*>(_a[0]) = -1; break;
            case 0:
                *reinterpret_cast<int*>(_a[0]) = qRegisterMetaType< QVector<QQmlProfilerData> >(); break;
            }
            break;
        }
    } else if (_c == QMetaObject::IndexOfMethod) {
        int *result = reinterpret_cast<int *>(_a[0]);
        void **func = reinterpret_cast<void **>(_a[1]);
        {
            typedef void (QQmlProfiler::*_t)(const QVector<QQmlProfilerData> & );
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&QQmlProfiler::dataReady)) {
                *result = 0;
                return;
            }
        }
    }
}

const QMetaObject QQmlProfiler::staticMetaObject = {
    { &QObject::staticMetaObject, qt_meta_stringdata_QQmlProfiler.data,
      qt_meta_data_QQmlProfiler,  qt_static_metacall, Q_NULLPTR, Q_NULLPTR}
};


const QMetaObject *QQmlProfiler::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->dynamicMetaObject() : &staticMetaObject;
}

void *QQmlProfiler::qt_metacast(const char *_clname)
{
    if (!_clname) return Q_NULLPTR;
    if (!strcmp(_clname, qt_meta_stringdata_QQmlProfiler.stringdata0))
        return static_cast<void*>(const_cast< QQmlProfiler*>(this));
    if (!strcmp(_clname, "QQmlProfilerDefinitions"))
        return static_cast< QQmlProfilerDefinitions*>(const_cast< QQmlProfiler*>(this));
    return QObject::qt_metacast(_clname);
}

int QQmlProfiler::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QObject::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 5)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 5;
    } else if (_c == QMetaObject::RegisterMethodArgumentMetaType) {
        if (_id < 5)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 5;
    }
    return _id;
}

// SIGNAL 0
void QQmlProfiler::dataReady(const QVector<QQmlProfilerData> & _t1)
{
    void *_a[] = { Q_NULLPTR, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 0, _a);
}
QT_END_MOC_NAMESPACE
