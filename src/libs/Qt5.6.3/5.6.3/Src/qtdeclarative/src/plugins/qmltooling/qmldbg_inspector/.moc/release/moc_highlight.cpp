/****************************************************************************
** Meta object code from reading C++ file 'highlight.h'
**
** Created by: The Qt Meta Object Compiler version 67 (Qt 5.6.3)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "../../highlight.h"
#include <QtCore/qbytearray.h>
#include <QtCore/qmetatype.h>
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'highlight.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 67
#error "This file was generated using the moc from 5.6.3. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
struct qt_meta_stringdata_QmlJSDebugger__Highlight_t {
    QByteArrayData data[3];
    char stringdata0[33];
};
#define QT_MOC_LITERAL(idx, ofs, len) \
    Q_STATIC_BYTE_ARRAY_DATA_HEADER_INITIALIZER_WITH_OFFSET(len, \
    qptrdiff(offsetof(qt_meta_stringdata_QmlJSDebugger__Highlight_t, stringdata0) + ofs \
        - idx * sizeof(QByteArrayData)) \
    )
static const qt_meta_stringdata_QmlJSDebugger__Highlight_t qt_meta_stringdata_QmlJSDebugger__Highlight = {
    {
QT_MOC_LITERAL(0, 0, 24), // "QmlJSDebugger::Highlight"
QT_MOC_LITERAL(1, 25, 6), // "adjust"
QT_MOC_LITERAL(2, 32, 0) // ""

    },
    "QmlJSDebugger::Highlight\0adjust\0"
};
#undef QT_MOC_LITERAL

static const uint qt_meta_data_QmlJSDebugger__Highlight[] = {

 // content:
       7,       // revision
       0,       // classname
       0,    0, // classinfo
       1,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       0,       // signalCount

 // slots: name, argc, parameters, tag, flags
       1,    0,   19,    2, 0x08 /* Private */,

 // slots: parameters
    QMetaType::Void,

       0        // eod
};

void QmlJSDebugger::Highlight::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        Highlight *_t = static_cast<Highlight *>(_o);
        Q_UNUSED(_t)
        switch (_id) {
        case 0: _t->adjust(); break;
        default: ;
        }
    }
    Q_UNUSED(_a);
}

const QMetaObject QmlJSDebugger::Highlight::staticMetaObject = {
    { &QQuickPaintedItem::staticMetaObject, qt_meta_stringdata_QmlJSDebugger__Highlight.data,
      qt_meta_data_QmlJSDebugger__Highlight,  qt_static_metacall, Q_NULLPTR, Q_NULLPTR}
};


const QMetaObject *QmlJSDebugger::Highlight::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->dynamicMetaObject() : &staticMetaObject;
}

void *QmlJSDebugger::Highlight::qt_metacast(const char *_clname)
{
    if (!_clname) return Q_NULLPTR;
    if (!strcmp(_clname, qt_meta_stringdata_QmlJSDebugger__Highlight.stringdata0))
        return static_cast<void*>(const_cast< Highlight*>(this));
    return QQuickPaintedItem::qt_metacast(_clname);
}

int QmlJSDebugger::Highlight::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QQuickPaintedItem::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 1)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 1;
    } else if (_c == QMetaObject::RegisterMethodArgumentMetaType) {
        if (_id < 1)
            *reinterpret_cast<int*>(_a[0]) = -1;
        _id -= 1;
    }
    return _id;
}
struct qt_meta_stringdata_QmlJSDebugger__SelectionHighlight_t {
    QByteArrayData data[3];
    char stringdata0[54];
};
#define QT_MOC_LITERAL(idx, ofs, len) \
    Q_STATIC_BYTE_ARRAY_DATA_HEADER_INITIALIZER_WITH_OFFSET(len, \
    qptrdiff(offsetof(qt_meta_stringdata_QmlJSDebugger__SelectionHighlight_t, stringdata0) + ofs \
        - idx * sizeof(QByteArrayData)) \
    )
static const qt_meta_stringdata_QmlJSDebugger__SelectionHighlight_t qt_meta_stringdata_QmlJSDebugger__SelectionHighlight = {
    {
QT_MOC_LITERAL(0, 0, 33), // "QmlJSDebugger::SelectionHighl..."
QT_MOC_LITERAL(1, 34, 18), // "disableNameDisplay"
QT_MOC_LITERAL(2, 53, 0) // ""

    },
    "QmlJSDebugger::SelectionHighlight\0"
    "disableNameDisplay\0"
};
#undef QT_MOC_LITERAL

static const uint qt_meta_data_QmlJSDebugger__SelectionHighlight[] = {

 // content:
       7,       // revision
       0,       // classname
       0,    0, // classinfo
       1,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       0,       // signalCount

 // slots: name, argc, parameters, tag, flags
       1,    0,   19,    2, 0x08 /* Private */,

 // slots: parameters
    QMetaType::Void,

       0        // eod
};

void QmlJSDebugger::SelectionHighlight::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        SelectionHighlight *_t = static_cast<SelectionHighlight *>(_o);
        Q_UNUSED(_t)
        switch (_id) {
        case 0: _t->disableNameDisplay(); break;
        default: ;
        }
    }
    Q_UNUSED(_a);
}

const QMetaObject QmlJSDebugger::SelectionHighlight::staticMetaObject = {
    { &Highlight::staticMetaObject, qt_meta_stringdata_QmlJSDebugger__SelectionHighlight.data,
      qt_meta_data_QmlJSDebugger__SelectionHighlight,  qt_static_metacall, Q_NULLPTR, Q_NULLPTR}
};


const QMetaObject *QmlJSDebugger::SelectionHighlight::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->dynamicMetaObject() : &staticMetaObject;
}

void *QmlJSDebugger::SelectionHighlight::qt_metacast(const char *_clname)
{
    if (!_clname) return Q_NULLPTR;
    if (!strcmp(_clname, qt_meta_stringdata_QmlJSDebugger__SelectionHighlight.stringdata0))
        return static_cast<void*>(const_cast< SelectionHighlight*>(this));
    return Highlight::qt_metacast(_clname);
}

int QmlJSDebugger::SelectionHighlight::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = Highlight::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 1)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 1;
    } else if (_c == QMetaObject::RegisterMethodArgumentMetaType) {
        if (_id < 1)
            *reinterpret_cast<int*>(_a[0]) = -1;
        _id -= 1;
    }
    return _id;
}
QT_END_MOC_NAMESPACE
