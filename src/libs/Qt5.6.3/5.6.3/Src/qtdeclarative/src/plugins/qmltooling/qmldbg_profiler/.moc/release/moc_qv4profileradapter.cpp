/****************************************************************************
** Meta object code from reading C++ file 'qv4profileradapter.h'
**
** Created by: The Qt Meta Object Compiler version 67 (Qt 5.6.3)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "../../qv4profileradapter.h"
#include <QtCore/qbytearray.h>
#include <QtCore/qmetatype.h>
#include <QtCore/QVector>
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'qv4profileradapter.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 67
#error "This file was generated using the moc from 5.6.3. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
struct qt_meta_stringdata_QV4ProfilerAdapter_t {
    QByteArrayData data[5];
    char stringdata0[132];
};
#define QT_MOC_LITERAL(idx, ofs, len) \
    Q_STATIC_BYTE_ARRAY_DATA_HEADER_INITIALIZER_WITH_OFFSET(len, \
    qptrdiff(offsetof(qt_meta_stringdata_QV4ProfilerAdapter_t, stringdata0) + ofs \
        - idx * sizeof(QByteArrayData)) \
    )
static const qt_meta_stringdata_QV4ProfilerAdapter_t qt_meta_stringdata_QV4ProfilerAdapter = {
    {
QT_MOC_LITERAL(0, 0, 18), // "QV4ProfilerAdapter"
QT_MOC_LITERAL(1, 19, 11), // "receiveData"
QT_MOC_LITERAL(2, 31, 0), // ""
QT_MOC_LITERAL(3, 32, 47), // "QVector<QV4::Profiling::Funct..."
QT_MOC_LITERAL(4, 80, 51) // "QVector<QV4::Profiling::Memor..."

    },
    "QV4ProfilerAdapter\0receiveData\0\0"
    "QVector<QV4::Profiling::FunctionCallProperties>\0"
    "QVector<QV4::Profiling::MemoryAllocationProperties>"
};
#undef QT_MOC_LITERAL

static const uint qt_meta_data_QV4ProfilerAdapter[] = {

 // content:
       7,       // revision
       0,       // classname
       0,    0, // classinfo
       1,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       0,       // signalCount

 // slots: name, argc, parameters, tag, flags
       1,    2,   19,    2, 0x0a /* Public */,

 // slots: parameters
    QMetaType::Void, 0x80000000 | 3, 0x80000000 | 4,    2,    2,

       0        // eod
};

void QV4ProfilerAdapter::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        QV4ProfilerAdapter *_t = static_cast<QV4ProfilerAdapter *>(_o);
        Q_UNUSED(_t)
        switch (_id) {
        case 0: _t->receiveData((*reinterpret_cast< const QVector<QV4::Profiling::FunctionCallProperties>(*)>(_a[1])),(*reinterpret_cast< const QVector<QV4::Profiling::MemoryAllocationProperties>(*)>(_a[2]))); break;
        default: ;
        }
    } else if (_c == QMetaObject::RegisterMethodArgumentMetaType) {
        switch (_id) {
        default: *reinterpret_cast<int*>(_a[0]) = -1; break;
        case 0:
            switch (*reinterpret_cast<int*>(_a[1])) {
            default: *reinterpret_cast<int*>(_a[0]) = -1; break;
            case 0:
                *reinterpret_cast<int*>(_a[0]) = qRegisterMetaType< QVector<QV4::Profiling::FunctionCallProperties> >(); break;
            case 1:
                *reinterpret_cast<int*>(_a[0]) = qRegisterMetaType< QVector<QV4::Profiling::MemoryAllocationProperties> >(); break;
            }
            break;
        }
    }
}

const QMetaObject QV4ProfilerAdapter::staticMetaObject = {
    { &QQmlAbstractProfilerAdapter::staticMetaObject, qt_meta_stringdata_QV4ProfilerAdapter.data,
      qt_meta_data_QV4ProfilerAdapter,  qt_static_metacall, Q_NULLPTR, Q_NULLPTR}
};


const QMetaObject *QV4ProfilerAdapter::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->dynamicMetaObject() : &staticMetaObject;
}

void *QV4ProfilerAdapter::qt_metacast(const char *_clname)
{
    if (!_clname) return Q_NULLPTR;
    if (!strcmp(_clname, qt_meta_stringdata_QV4ProfilerAdapter.stringdata0))
        return static_cast<void*>(const_cast< QV4ProfilerAdapter*>(this));
    return QQmlAbstractProfilerAdapter::qt_metacast(_clname);
}

int QV4ProfilerAdapter::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QQmlAbstractProfilerAdapter::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 1)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 1;
    } else if (_c == QMetaObject::RegisterMethodArgumentMetaType) {
        if (_id < 1)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 1;
    }
    return _id;
}
QT_END_MOC_NAMESPACE
