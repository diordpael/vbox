/****************************************************************************
** Meta object code from reading C++ file 'qquickviewinspector.h'
**
** Created by: The Qt Meta Object Compiler version 67 (Qt 5.6.3)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "../../qquickviewinspector.h"
#include <QtCore/qbytearray.h>
#include <QtCore/qmetatype.h>
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'qquickviewinspector.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 67
#error "This file was generated using the moc from 5.6.3. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
struct qt_meta_stringdata_QmlJSDebugger__QQuickViewInspector_t {
    QByteArrayData data[7];
    char stringdata0[113];
};
#define QT_MOC_LITERAL(idx, ofs, len) \
    Q_STATIC_BYTE_ARRAY_DATA_HEADER_INITIALIZER_WITH_OFFSET(len, \
    qptrdiff(offsetof(qt_meta_stringdata_QmlJSDebugger__QQuickViewInspector_t, stringdata0) + ofs \
        - idx * sizeof(QByteArrayData)) \
    )
static const qt_meta_stringdata_QmlJSDebugger__QQuickViewInspector_t qt_meta_stringdata_QmlJSDebugger__QQuickViewInspector = {
    {
QT_MOC_LITERAL(0, 0, 34), // "QmlJSDebugger::QQuickViewInsp..."
QT_MOC_LITERAL(1, 35, 23), // "removeFromSelectedItems"
QT_MOC_LITERAL(2, 59, 0), // ""
QT_MOC_LITERAL(3, 60, 12), // "onViewStatus"
QT_MOC_LITERAL(4, 73, 18), // "QQuickView::Status"
QT_MOC_LITERAL(5, 92, 6), // "status"
QT_MOC_LITERAL(6, 99, 13) // "applyAppOnTop"

    },
    "QmlJSDebugger::QQuickViewInspector\0"
    "removeFromSelectedItems\0\0onViewStatus\0"
    "QQuickView::Status\0status\0applyAppOnTop"
};
#undef QT_MOC_LITERAL

static const uint qt_meta_data_QmlJSDebugger__QQuickViewInspector[] = {

 // content:
       7,       // revision
       0,       // classname
       0,    0, // classinfo
       3,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       0,       // signalCount

 // slots: name, argc, parameters, tag, flags
       1,    1,   29,    2, 0x08 /* Private */,
       3,    1,   32,    2, 0x08 /* Private */,
       6,    0,   35,    2, 0x08 /* Private */,

 // slots: parameters
    QMetaType::Void, QMetaType::QObjectStar,    2,
    QMetaType::Void, 0x80000000 | 4,    5,
    QMetaType::Void,

       0        // eod
};

void QmlJSDebugger::QQuickViewInspector::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        QQuickViewInspector *_t = static_cast<QQuickViewInspector *>(_o);
        Q_UNUSED(_t)
        switch (_id) {
        case 0: _t->removeFromSelectedItems((*reinterpret_cast< QObject*(*)>(_a[1]))); break;
        case 1: _t->onViewStatus((*reinterpret_cast< QQuickView::Status(*)>(_a[1]))); break;
        case 2: _t->applyAppOnTop(); break;
        default: ;
        }
    }
}

const QMetaObject QmlJSDebugger::QQuickViewInspector::staticMetaObject = {
    { &AbstractViewInspector::staticMetaObject, qt_meta_stringdata_QmlJSDebugger__QQuickViewInspector.data,
      qt_meta_data_QmlJSDebugger__QQuickViewInspector,  qt_static_metacall, Q_NULLPTR, Q_NULLPTR}
};


const QMetaObject *QmlJSDebugger::QQuickViewInspector::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->dynamicMetaObject() : &staticMetaObject;
}

void *QmlJSDebugger::QQuickViewInspector::qt_metacast(const char *_clname)
{
    if (!_clname) return Q_NULLPTR;
    if (!strcmp(_clname, qt_meta_stringdata_QmlJSDebugger__QQuickViewInspector.stringdata0))
        return static_cast<void*>(const_cast< QQuickViewInspector*>(this));
    return AbstractViewInspector::qt_metacast(_clname);
}

int QmlJSDebugger::QQuickViewInspector::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = AbstractViewInspector::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 3)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 3;
    } else if (_c == QMetaObject::RegisterMethodArgumentMetaType) {
        if (_id < 3)
            *reinterpret_cast<int*>(_a[0]) = -1;
        _id -= 3;
    }
    return _id;
}
QT_END_MOC_NAMESPACE
