/****************************************************************************
** Meta object code from reading C++ file 'qqmlenginedebugservice.h'
**
** Created by: The Qt Meta Object Compiler version 67 (Qt 5.6.3)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "../../qqmlenginedebugservice.h"
#include <QtCore/qbytearray.h>
#include <QtCore/qmetatype.h>
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'qqmlenginedebugservice.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 67
#error "This file was generated using the moc from 5.6.3. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
struct qt_meta_stringdata_QQmlEngineDebugServiceImpl_t {
    QByteArrayData data[10];
    char stringdata0[104];
};
#define QT_MOC_LITERAL(idx, ofs, len) \
    Q_STATIC_BYTE_ARRAY_DATA_HEADER_INITIALIZER_WITH_OFFSET(len, \
    qptrdiff(offsetof(qt_meta_stringdata_QQmlEngineDebugServiceImpl_t, stringdata0) + ofs \
        - idx * sizeof(QByteArrayData)) \
    )
static const qt_meta_stringdata_QQmlEngineDebugServiceImpl_t qt_meta_stringdata_QQmlEngineDebugServiceImpl = {
    {
QT_MOC_LITERAL(0, 0, 26), // "QQmlEngineDebugServiceImpl"
QT_MOC_LITERAL(1, 27, 14), // "processMessage"
QT_MOC_LITERAL(2, 42, 0), // ""
QT_MOC_LITERAL(3, 43, 3), // "msg"
QT_MOC_LITERAL(4, 47, 15), // "propertyChanged"
QT_MOC_LITERAL(5, 63, 2), // "id"
QT_MOC_LITERAL(6, 66, 8), // "objectId"
QT_MOC_LITERAL(7, 75, 13), // "QMetaProperty"
QT_MOC_LITERAL(8, 89, 8), // "property"
QT_MOC_LITERAL(9, 98, 5) // "value"

    },
    "QQmlEngineDebugServiceImpl\0processMessage\0"
    "\0msg\0propertyChanged\0id\0objectId\0"
    "QMetaProperty\0property\0value"
};
#undef QT_MOC_LITERAL

static const uint qt_meta_data_QQmlEngineDebugServiceImpl[] = {

 // content:
       7,       // revision
       0,       // classname
       0,    0, // classinfo
       2,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       0,       // signalCount

 // slots: name, argc, parameters, tag, flags
       1,    1,   24,    2, 0x08 /* Private */,
       4,    4,   27,    2, 0x08 /* Private */,

 // slots: parameters
    QMetaType::Void, QMetaType::QByteArray,    3,
    QMetaType::Void, QMetaType::Int, QMetaType::Int, 0x80000000 | 7, QMetaType::QVariant,    5,    6,    8,    9,

       0        // eod
};

void QQmlEngineDebugServiceImpl::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        QQmlEngineDebugServiceImpl *_t = static_cast<QQmlEngineDebugServiceImpl *>(_o);
        Q_UNUSED(_t)
        switch (_id) {
        case 0: _t->processMessage((*reinterpret_cast< const QByteArray(*)>(_a[1]))); break;
        case 1: _t->propertyChanged((*reinterpret_cast< int(*)>(_a[1])),(*reinterpret_cast< int(*)>(_a[2])),(*reinterpret_cast< const QMetaProperty(*)>(_a[3])),(*reinterpret_cast< const QVariant(*)>(_a[4]))); break;
        default: ;
        }
    }
}

const QMetaObject QQmlEngineDebugServiceImpl::staticMetaObject = {
    { &QQmlEngineDebugService::staticMetaObject, qt_meta_stringdata_QQmlEngineDebugServiceImpl.data,
      qt_meta_data_QQmlEngineDebugServiceImpl,  qt_static_metacall, Q_NULLPTR, Q_NULLPTR}
};


const QMetaObject *QQmlEngineDebugServiceImpl::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->dynamicMetaObject() : &staticMetaObject;
}

void *QQmlEngineDebugServiceImpl::qt_metacast(const char *_clname)
{
    if (!_clname) return Q_NULLPTR;
    if (!strcmp(_clname, qt_meta_stringdata_QQmlEngineDebugServiceImpl.stringdata0))
        return static_cast<void*>(const_cast< QQmlEngineDebugServiceImpl*>(this));
    return QQmlEngineDebugService::qt_metacast(_clname);
}

int QQmlEngineDebugServiceImpl::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QQmlEngineDebugService::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 2)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 2;
    } else if (_c == QMetaObject::RegisterMethodArgumentMetaType) {
        if (_id < 2)
            *reinterpret_cast<int*>(_a[0]) = -1;
        _id -= 2;
    }
    return _id;
}
QT_END_MOC_NAMESPACE
