/****************************************************************************
** Meta object code from reading C++ file 'qquickselectionmode_p.h'
**
** Created by: The Qt Meta Object Compiler version 67 (Qt 5.6.3)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "../../qquickselectionmode_p.h"
#include <QtCore/qbytearray.h>
#include <QtCore/qmetatype.h>
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'qquickselectionmode_p.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 67
#error "This file was generated using the moc from 5.6.3. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
struct qt_meta_stringdata_QQuickSelectionMode_t {
    QByteArrayData data[7];
    char stringdata0[115];
};
#define QT_MOC_LITERAL(idx, ofs, len) \
    Q_STATIC_BYTE_ARRAY_DATA_HEADER_INITIALIZER_WITH_OFFSET(len, \
    qptrdiff(offsetof(qt_meta_stringdata_QQuickSelectionMode_t, stringdata0) + ofs \
        - idx * sizeof(QByteArrayData)) \
    )
static const qt_meta_stringdata_QQuickSelectionMode_t qt_meta_stringdata_QQuickSelectionMode = {
    {
QT_MOC_LITERAL(0, 0, 19), // "QQuickSelectionMode"
QT_MOC_LITERAL(1, 20, 13), // "SelectionMode"
QT_MOC_LITERAL(2, 34, 11), // "NoSelection"
QT_MOC_LITERAL(3, 46, 15), // "SingleSelection"
QT_MOC_LITERAL(4, 62, 17), // "ExtendedSelection"
QT_MOC_LITERAL(5, 80, 14), // "MultiSelection"
QT_MOC_LITERAL(6, 95, 19) // "ContiguousSelection"

    },
    "QQuickSelectionMode\0SelectionMode\0"
    "NoSelection\0SingleSelection\0"
    "ExtendedSelection\0MultiSelection\0"
    "ContiguousSelection"
};
#undef QT_MOC_LITERAL

static const uint qt_meta_data_QQuickSelectionMode[] = {

 // content:
       7,       // revision
       0,       // classname
       0,    0, // classinfo
       0,    0, // methods
       0,    0, // properties
       1,   14, // enums/sets
       0,    0, // constructors
       4,       // flags
       0,       // signalCount

 // enums: name, flags, count, data
       1, 0x0,    5,   18,

 // enum data: key, value
       2, uint(QQuickSelectionMode::NoSelection),
       3, uint(QQuickSelectionMode::SingleSelection),
       4, uint(QQuickSelectionMode::ExtendedSelection),
       5, uint(QQuickSelectionMode::MultiSelection),
       6, uint(QQuickSelectionMode::ContiguousSelection),

       0        // eod
};

const QMetaObject QQuickSelectionMode::staticMetaObject = {
    { Q_NULLPTR, qt_meta_stringdata_QQuickSelectionMode.data,
      qt_meta_data_QQuickSelectionMode,  Q_NULLPTR, Q_NULLPTR, Q_NULLPTR}
};

QT_END_MOC_NAMESPACE
