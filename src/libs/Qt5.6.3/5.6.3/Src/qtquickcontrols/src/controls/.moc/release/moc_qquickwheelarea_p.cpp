/****************************************************************************
** Meta object code from reading C++ file 'qquickwheelarea_p.h'
**
** Created by: The Qt Meta Object Compiler version 67 (Qt 5.6.3)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "../../Private/qquickwheelarea_p.h"
#include <QtCore/qbytearray.h>
#include <QtCore/qmetatype.h>
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'qquickwheelarea_p.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 67
#error "This file was generated using the moc from 5.6.3. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
struct qt_meta_stringdata_QQuickWheelArea_t {
    QByteArrayData data[18];
    char stringdata0[301];
};
#define QT_MOC_LITERAL(idx, ofs, len) \
    Q_STATIC_BYTE_ARRAY_DATA_HEADER_INITIALIZER_WITH_OFFSET(len, \
    qptrdiff(offsetof(qt_meta_stringdata_QQuickWheelArea_t, stringdata0) + ofs \
        - idx * sizeof(QByteArrayData)) \
    )
static const qt_meta_stringdata_QQuickWheelArea_t qt_meta_stringdata_QQuickWheelArea = {
    {
QT_MOC_LITERAL(0, 0, 15), // "QQuickWheelArea"
QT_MOC_LITERAL(1, 16, 20), // "verticalValueChanged"
QT_MOC_LITERAL(2, 37, 0), // ""
QT_MOC_LITERAL(3, 38, 22), // "horizontalValueChanged"
QT_MOC_LITERAL(4, 61, 18), // "verticalWheelMoved"
QT_MOC_LITERAL(5, 80, 20), // "horizontalWheelMoved"
QT_MOC_LITERAL(6, 101, 18), // "scrollSpeedChanged"
QT_MOC_LITERAL(7, 120, 13), // "activeChanged"
QT_MOC_LITERAL(8, 134, 13), // "verticalDelta"
QT_MOC_LITERAL(9, 148, 15), // "horizontalDelta"
QT_MOC_LITERAL(10, 164, 22), // "horizontalMinimumValue"
QT_MOC_LITERAL(11, 187, 22), // "horizontalMaximumValue"
QT_MOC_LITERAL(12, 210, 20), // "verticalMinimumValue"
QT_MOC_LITERAL(13, 231, 20), // "verticalMaximumValue"
QT_MOC_LITERAL(14, 252, 15), // "horizontalValue"
QT_MOC_LITERAL(15, 268, 13), // "verticalValue"
QT_MOC_LITERAL(16, 282, 11), // "scrollSpeed"
QT_MOC_LITERAL(17, 294, 6) // "active"

    },
    "QQuickWheelArea\0verticalValueChanged\0"
    "\0horizontalValueChanged\0verticalWheelMoved\0"
    "horizontalWheelMoved\0scrollSpeedChanged\0"
    "activeChanged\0verticalDelta\0horizontalDelta\0"
    "horizontalMinimumValue\0horizontalMaximumValue\0"
    "verticalMinimumValue\0verticalMaximumValue\0"
    "horizontalValue\0verticalValue\0scrollSpeed\0"
    "active"
};
#undef QT_MOC_LITERAL

static const uint qt_meta_data_QQuickWheelArea[] = {

 // content:
       7,       // revision
       0,       // classname
       0,    0, // classinfo
       6,   14, // methods
      10,   50, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       6,       // signalCount

 // signals: name, argc, parameters, tag, flags
       1,    0,   44,    2, 0x06 /* Public */,
       3,    0,   45,    2, 0x06 /* Public */,
       4,    0,   46,    2, 0x06 /* Public */,
       5,    0,   47,    2, 0x06 /* Public */,
       6,    0,   48,    2, 0x06 /* Public */,
       7,    0,   49,    2, 0x06 /* Public */,

 // signals: parameters
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,

 // properties: name, type, flags
       8, QMetaType::QReal, 0x00495103,
       9, QMetaType::QReal, 0x00495103,
      10, QMetaType::QReal, 0x00095103,
      11, QMetaType::QReal, 0x00095103,
      12, QMetaType::QReal, 0x00095103,
      13, QMetaType::QReal, 0x00095103,
      14, QMetaType::QReal, 0x00095103,
      15, QMetaType::QReal, 0x00095103,
      16, QMetaType::QReal, 0x00495103,
      17, QMetaType::Bool, 0x00495103,

 // properties: notify_signal_id
       2,
       3,
       0,
       0,
       0,
       0,
       0,
       0,
       4,
       5,

       0        // eod
};

void QQuickWheelArea::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        QQuickWheelArea *_t = static_cast<QQuickWheelArea *>(_o);
        Q_UNUSED(_t)
        switch (_id) {
        case 0: _t->verticalValueChanged(); break;
        case 1: _t->horizontalValueChanged(); break;
        case 2: _t->verticalWheelMoved(); break;
        case 3: _t->horizontalWheelMoved(); break;
        case 4: _t->scrollSpeedChanged(); break;
        case 5: _t->activeChanged(); break;
        default: ;
        }
    } else if (_c == QMetaObject::IndexOfMethod) {
        int *result = reinterpret_cast<int *>(_a[0]);
        void **func = reinterpret_cast<void **>(_a[1]);
        {
            typedef void (QQuickWheelArea::*_t)();
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&QQuickWheelArea::verticalValueChanged)) {
                *result = 0;
                return;
            }
        }
        {
            typedef void (QQuickWheelArea::*_t)();
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&QQuickWheelArea::horizontalValueChanged)) {
                *result = 1;
                return;
            }
        }
        {
            typedef void (QQuickWheelArea::*_t)();
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&QQuickWheelArea::verticalWheelMoved)) {
                *result = 2;
                return;
            }
        }
        {
            typedef void (QQuickWheelArea::*_t)();
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&QQuickWheelArea::horizontalWheelMoved)) {
                *result = 3;
                return;
            }
        }
        {
            typedef void (QQuickWheelArea::*_t)();
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&QQuickWheelArea::scrollSpeedChanged)) {
                *result = 4;
                return;
            }
        }
        {
            typedef void (QQuickWheelArea::*_t)();
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&QQuickWheelArea::activeChanged)) {
                *result = 5;
                return;
            }
        }
    }
#ifndef QT_NO_PROPERTIES
    else if (_c == QMetaObject::ReadProperty) {
        QQuickWheelArea *_t = static_cast<QQuickWheelArea *>(_o);
        Q_UNUSED(_t)
        void *_v = _a[0];
        switch (_id) {
        case 0: *reinterpret_cast< qreal*>(_v) = _t->verticalDelta(); break;
        case 1: *reinterpret_cast< qreal*>(_v) = _t->horizontalDelta(); break;
        case 2: *reinterpret_cast< qreal*>(_v) = _t->horizontalMinimumValue(); break;
        case 3: *reinterpret_cast< qreal*>(_v) = _t->horizontalMaximumValue(); break;
        case 4: *reinterpret_cast< qreal*>(_v) = _t->verticalMinimumValue(); break;
        case 5: *reinterpret_cast< qreal*>(_v) = _t->verticalMaximumValue(); break;
        case 6: *reinterpret_cast< qreal*>(_v) = _t->horizontalValue(); break;
        case 7: *reinterpret_cast< qreal*>(_v) = _t->verticalValue(); break;
        case 8: *reinterpret_cast< qreal*>(_v) = _t->scrollSpeed(); break;
        case 9: *reinterpret_cast< bool*>(_v) = _t->isActive(); break;
        default: break;
        }
    } else if (_c == QMetaObject::WriteProperty) {
        QQuickWheelArea *_t = static_cast<QQuickWheelArea *>(_o);
        Q_UNUSED(_t)
        void *_v = _a[0];
        switch (_id) {
        case 0: _t->setVerticalDelta(*reinterpret_cast< qreal*>(_v)); break;
        case 1: _t->setHorizontalDelta(*reinterpret_cast< qreal*>(_v)); break;
        case 2: _t->setHorizontalMinimumValue(*reinterpret_cast< qreal*>(_v)); break;
        case 3: _t->setHorizontalMaximumValue(*reinterpret_cast< qreal*>(_v)); break;
        case 4: _t->setVerticalMinimumValue(*reinterpret_cast< qreal*>(_v)); break;
        case 5: _t->setVerticalMaximumValue(*reinterpret_cast< qreal*>(_v)); break;
        case 6: _t->setHorizontalValue(*reinterpret_cast< qreal*>(_v)); break;
        case 7: _t->setVerticalValue(*reinterpret_cast< qreal*>(_v)); break;
        case 8: _t->setScrollSpeed(*reinterpret_cast< qreal*>(_v)); break;
        case 9: _t->setActive(*reinterpret_cast< bool*>(_v)); break;
        default: break;
        }
    } else if (_c == QMetaObject::ResetProperty) {
    }
#endif // QT_NO_PROPERTIES
    Q_UNUSED(_a);
}

const QMetaObject QQuickWheelArea::staticMetaObject = {
    { &QQuickItem::staticMetaObject, qt_meta_stringdata_QQuickWheelArea.data,
      qt_meta_data_QQuickWheelArea,  qt_static_metacall, Q_NULLPTR, Q_NULLPTR}
};


const QMetaObject *QQuickWheelArea::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->dynamicMetaObject() : &staticMetaObject;
}

void *QQuickWheelArea::qt_metacast(const char *_clname)
{
    if (!_clname) return Q_NULLPTR;
    if (!strcmp(_clname, qt_meta_stringdata_QQuickWheelArea.stringdata0))
        return static_cast<void*>(const_cast< QQuickWheelArea*>(this));
    return QQuickItem::qt_metacast(_clname);
}

int QQuickWheelArea::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QQuickItem::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 6)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 6;
    } else if (_c == QMetaObject::RegisterMethodArgumentMetaType) {
        if (_id < 6)
            *reinterpret_cast<int*>(_a[0]) = -1;
        _id -= 6;
    }
#ifndef QT_NO_PROPERTIES
   else if (_c == QMetaObject::ReadProperty || _c == QMetaObject::WriteProperty
            || _c == QMetaObject::ResetProperty || _c == QMetaObject::RegisterPropertyMetaType) {
        qt_static_metacall(this, _c, _id, _a);
        _id -= 10;
    } else if (_c == QMetaObject::QueryPropertyDesignable) {
        _id -= 10;
    } else if (_c == QMetaObject::QueryPropertyScriptable) {
        _id -= 10;
    } else if (_c == QMetaObject::QueryPropertyStored) {
        _id -= 10;
    } else if (_c == QMetaObject::QueryPropertyEditable) {
        _id -= 10;
    } else if (_c == QMetaObject::QueryPropertyUser) {
        _id -= 10;
    }
#endif // QT_NO_PROPERTIES
    return _id;
}

// SIGNAL 0
void QQuickWheelArea::verticalValueChanged()
{
    QMetaObject::activate(this, &staticMetaObject, 0, Q_NULLPTR);
}

// SIGNAL 1
void QQuickWheelArea::horizontalValueChanged()
{
    QMetaObject::activate(this, &staticMetaObject, 1, Q_NULLPTR);
}

// SIGNAL 2
void QQuickWheelArea::verticalWheelMoved()
{
    QMetaObject::activate(this, &staticMetaObject, 2, Q_NULLPTR);
}

// SIGNAL 3
void QQuickWheelArea::horizontalWheelMoved()
{
    QMetaObject::activate(this, &staticMetaObject, 3, Q_NULLPTR);
}

// SIGNAL 4
void QQuickWheelArea::scrollSpeedChanged()
{
    QMetaObject::activate(this, &staticMetaObject, 4, Q_NULLPTR);
}

// SIGNAL 5
void QQuickWheelArea::activeChanged()
{
    QMetaObject::activate(this, &staticMetaObject, 5, Q_NULLPTR);
}
QT_END_MOC_NAMESPACE
