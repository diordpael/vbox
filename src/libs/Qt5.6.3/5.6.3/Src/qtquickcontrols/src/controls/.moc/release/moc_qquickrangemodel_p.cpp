/****************************************************************************
** Meta object code from reading C++ file 'qquickrangemodel_p.h'
**
** Created by: The Qt Meta Object Compiler version 67 (Qt 5.6.3)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "../../Private/qquickrangemodel_p.h"
#include <QtCore/qbytearray.h>
#include <QtCore/qmetatype.h>
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'qquickrangemodel_p.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 67
#error "This file was generated using the moc from 5.6.3. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
struct qt_meta_stringdata_QQuickRangeModel_t {
    QByteArrayData data[28];
    char stringdata0[375];
};
#define QT_MOC_LITERAL(idx, ofs, len) \
    Q_STATIC_BYTE_ARRAY_DATA_HEADER_INITIALIZER_WITH_OFFSET(len, \
    qptrdiff(offsetof(qt_meta_stringdata_QQuickRangeModel_t, stringdata0) + ofs \
        - idx * sizeof(QByteArrayData)) \
    )
static const qt_meta_stringdata_QQuickRangeModel_t qt_meta_stringdata_QQuickRangeModel = {
    {
QT_MOC_LITERAL(0, 0, 16), // "QQuickRangeModel"
QT_MOC_LITERAL(1, 17, 12), // "valueChanged"
QT_MOC_LITERAL(2, 30, 0), // ""
QT_MOC_LITERAL(3, 31, 5), // "value"
QT_MOC_LITERAL(4, 37, 15), // "positionChanged"
QT_MOC_LITERAL(5, 53, 8), // "position"
QT_MOC_LITERAL(6, 62, 15), // "stepSizeChanged"
QT_MOC_LITERAL(7, 78, 8), // "stepSize"
QT_MOC_LITERAL(8, 87, 15), // "invertedChanged"
QT_MOC_LITERAL(9, 103, 8), // "inverted"
QT_MOC_LITERAL(10, 112, 14), // "minimumChanged"
QT_MOC_LITERAL(11, 127, 3), // "min"
QT_MOC_LITERAL(12, 131, 14), // "maximumChanged"
QT_MOC_LITERAL(13, 146, 3), // "max"
QT_MOC_LITERAL(14, 150, 24), // "positionAtMinimumChanged"
QT_MOC_LITERAL(15, 175, 24), // "positionAtMaximumChanged"
QT_MOC_LITERAL(16, 200, 9), // "toMinimum"
QT_MOC_LITERAL(17, 210, 9), // "toMaximum"
QT_MOC_LITERAL(18, 220, 8), // "setValue"
QT_MOC_LITERAL(19, 229, 11), // "setPosition"
QT_MOC_LITERAL(20, 241, 18), // "increaseSingleStep"
QT_MOC_LITERAL(21, 260, 18), // "decreaseSingleStep"
QT_MOC_LITERAL(22, 279, 16), // "valueForPosition"
QT_MOC_LITERAL(23, 296, 16), // "positionForValue"
QT_MOC_LITERAL(24, 313, 12), // "minimumValue"
QT_MOC_LITERAL(25, 326, 12), // "maximumValue"
QT_MOC_LITERAL(26, 339, 17), // "positionAtMinimum"
QT_MOC_LITERAL(27, 357, 17) // "positionAtMaximum"

    },
    "QQuickRangeModel\0valueChanged\0\0value\0"
    "positionChanged\0position\0stepSizeChanged\0"
    "stepSize\0invertedChanged\0inverted\0"
    "minimumChanged\0min\0maximumChanged\0max\0"
    "positionAtMinimumChanged\0"
    "positionAtMaximumChanged\0toMinimum\0"
    "toMaximum\0setValue\0setPosition\0"
    "increaseSingleStep\0decreaseSingleStep\0"
    "valueForPosition\0positionForValue\0"
    "minimumValue\0maximumValue\0positionAtMinimum\0"
    "positionAtMaximum"
};
#undef QT_MOC_LITERAL

static const uint qt_meta_data_QQuickRangeModel[] = {

 // content:
       7,       // revision
       0,       // classname
       0,    0, // classinfo
      16,   14, // methods
       8,  134, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       8,       // signalCount

 // signals: name, argc, parameters, tag, flags
       1,    1,   94,    2, 0x06 /* Public */,
       4,    1,   97,    2, 0x06 /* Public */,
       6,    1,  100,    2, 0x06 /* Public */,
       8,    1,  103,    2, 0x06 /* Public */,
      10,    1,  106,    2, 0x06 /* Public */,
      12,    1,  109,    2, 0x06 /* Public */,
      14,    1,  112,    2, 0x06 /* Public */,
      15,    1,  115,    2, 0x06 /* Public */,

 // slots: name, argc, parameters, tag, flags
      16,    0,  118,    2, 0x0a /* Public */,
      17,    0,  119,    2, 0x0a /* Public */,
      18,    1,  120,    2, 0x0a /* Public */,
      19,    1,  123,    2, 0x0a /* Public */,
      20,    0,  126,    2, 0x0a /* Public */,
      21,    0,  127,    2, 0x0a /* Public */,

 // methods: name, argc, parameters, tag, flags
      22,    1,  128,    2, 0x02 /* Public */,
      23,    1,  131,    2, 0x02 /* Public */,

 // signals: parameters
    QMetaType::Void, QMetaType::QReal,    3,
    QMetaType::Void, QMetaType::QReal,    5,
    QMetaType::Void, QMetaType::QReal,    7,
    QMetaType::Void, QMetaType::Bool,    9,
    QMetaType::Void, QMetaType::QReal,   11,
    QMetaType::Void, QMetaType::QReal,   13,
    QMetaType::Void, QMetaType::QReal,   11,
    QMetaType::Void, QMetaType::QReal,   13,

 // slots: parameters
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void, QMetaType::QReal,    3,
    QMetaType::Void, QMetaType::QReal,    5,
    QMetaType::Void,
    QMetaType::Void,

 // methods: parameters
    QMetaType::QReal, QMetaType::QReal,    5,
    QMetaType::QReal, QMetaType::QReal,    3,

 // properties: name, type, flags
       3, QMetaType::QReal, 0x00595103,
      24, QMetaType::QReal, 0x00495003,
      25, QMetaType::QReal, 0x00495003,
       7, QMetaType::QReal, 0x00495103,
       5, QMetaType::QReal, 0x00495103,
      26, QMetaType::QReal, 0x00495103,
      27, QMetaType::QReal, 0x00495103,
       9, QMetaType::Bool, 0x00495103,

 // properties: notify_signal_id
       0,
       4,
       5,
       2,
       1,
       6,
       7,
       3,

       0        // eod
};

void QQuickRangeModel::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        QQuickRangeModel *_t = static_cast<QQuickRangeModel *>(_o);
        Q_UNUSED(_t)
        switch (_id) {
        case 0: _t->valueChanged((*reinterpret_cast< qreal(*)>(_a[1]))); break;
        case 1: _t->positionChanged((*reinterpret_cast< qreal(*)>(_a[1]))); break;
        case 2: _t->stepSizeChanged((*reinterpret_cast< qreal(*)>(_a[1]))); break;
        case 3: _t->invertedChanged((*reinterpret_cast< bool(*)>(_a[1]))); break;
        case 4: _t->minimumChanged((*reinterpret_cast< qreal(*)>(_a[1]))); break;
        case 5: _t->maximumChanged((*reinterpret_cast< qreal(*)>(_a[1]))); break;
        case 6: _t->positionAtMinimumChanged((*reinterpret_cast< qreal(*)>(_a[1]))); break;
        case 7: _t->positionAtMaximumChanged((*reinterpret_cast< qreal(*)>(_a[1]))); break;
        case 8: _t->toMinimum(); break;
        case 9: _t->toMaximum(); break;
        case 10: _t->setValue((*reinterpret_cast< qreal(*)>(_a[1]))); break;
        case 11: _t->setPosition((*reinterpret_cast< qreal(*)>(_a[1]))); break;
        case 12: _t->increaseSingleStep(); break;
        case 13: _t->decreaseSingleStep(); break;
        case 14: { qreal _r = _t->valueForPosition((*reinterpret_cast< qreal(*)>(_a[1])));
            if (_a[0]) *reinterpret_cast< qreal*>(_a[0]) = _r; }  break;
        case 15: { qreal _r = _t->positionForValue((*reinterpret_cast< qreal(*)>(_a[1])));
            if (_a[0]) *reinterpret_cast< qreal*>(_a[0]) = _r; }  break;
        default: ;
        }
    } else if (_c == QMetaObject::IndexOfMethod) {
        int *result = reinterpret_cast<int *>(_a[0]);
        void **func = reinterpret_cast<void **>(_a[1]);
        {
            typedef void (QQuickRangeModel::*_t)(qreal );
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&QQuickRangeModel::valueChanged)) {
                *result = 0;
                return;
            }
        }
        {
            typedef void (QQuickRangeModel::*_t)(qreal );
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&QQuickRangeModel::positionChanged)) {
                *result = 1;
                return;
            }
        }
        {
            typedef void (QQuickRangeModel::*_t)(qreal );
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&QQuickRangeModel::stepSizeChanged)) {
                *result = 2;
                return;
            }
        }
        {
            typedef void (QQuickRangeModel::*_t)(bool );
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&QQuickRangeModel::invertedChanged)) {
                *result = 3;
                return;
            }
        }
        {
            typedef void (QQuickRangeModel::*_t)(qreal );
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&QQuickRangeModel::minimumChanged)) {
                *result = 4;
                return;
            }
        }
        {
            typedef void (QQuickRangeModel::*_t)(qreal );
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&QQuickRangeModel::maximumChanged)) {
                *result = 5;
                return;
            }
        }
        {
            typedef void (QQuickRangeModel::*_t)(qreal );
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&QQuickRangeModel::positionAtMinimumChanged)) {
                *result = 6;
                return;
            }
        }
        {
            typedef void (QQuickRangeModel::*_t)(qreal );
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&QQuickRangeModel::positionAtMaximumChanged)) {
                *result = 7;
                return;
            }
        }
    }
#ifndef QT_NO_PROPERTIES
    else if (_c == QMetaObject::ReadProperty) {
        QQuickRangeModel *_t = static_cast<QQuickRangeModel *>(_o);
        Q_UNUSED(_t)
        void *_v = _a[0];
        switch (_id) {
        case 0: *reinterpret_cast< qreal*>(_v) = _t->value(); break;
        case 1: *reinterpret_cast< qreal*>(_v) = _t->minimum(); break;
        case 2: *reinterpret_cast< qreal*>(_v) = _t->maximum(); break;
        case 3: *reinterpret_cast< qreal*>(_v) = _t->stepSize(); break;
        case 4: *reinterpret_cast< qreal*>(_v) = _t->position(); break;
        case 5: *reinterpret_cast< qreal*>(_v) = _t->positionAtMinimum(); break;
        case 6: *reinterpret_cast< qreal*>(_v) = _t->positionAtMaximum(); break;
        case 7: *reinterpret_cast< bool*>(_v) = _t->inverted(); break;
        default: break;
        }
    } else if (_c == QMetaObject::WriteProperty) {
        QQuickRangeModel *_t = static_cast<QQuickRangeModel *>(_o);
        Q_UNUSED(_t)
        void *_v = _a[0];
        switch (_id) {
        case 0: _t->setValue(*reinterpret_cast< qreal*>(_v)); break;
        case 1: _t->setMinimum(*reinterpret_cast< qreal*>(_v)); break;
        case 2: _t->setMaximum(*reinterpret_cast< qreal*>(_v)); break;
        case 3: _t->setStepSize(*reinterpret_cast< qreal*>(_v)); break;
        case 4: _t->setPosition(*reinterpret_cast< qreal*>(_v)); break;
        case 5: _t->setPositionAtMinimum(*reinterpret_cast< qreal*>(_v)); break;
        case 6: _t->setPositionAtMaximum(*reinterpret_cast< qreal*>(_v)); break;
        case 7: _t->setInverted(*reinterpret_cast< bool*>(_v)); break;
        default: break;
        }
    } else if (_c == QMetaObject::ResetProperty) {
    }
#endif // QT_NO_PROPERTIES
}

const QMetaObject QQuickRangeModel::staticMetaObject = {
    { &QObject::staticMetaObject, qt_meta_stringdata_QQuickRangeModel.data,
      qt_meta_data_QQuickRangeModel,  qt_static_metacall, Q_NULLPTR, Q_NULLPTR}
};


const QMetaObject *QQuickRangeModel::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->dynamicMetaObject() : &staticMetaObject;
}

void *QQuickRangeModel::qt_metacast(const char *_clname)
{
    if (!_clname) return Q_NULLPTR;
    if (!strcmp(_clname, qt_meta_stringdata_QQuickRangeModel.stringdata0))
        return static_cast<void*>(const_cast< QQuickRangeModel*>(this));
    if (!strcmp(_clname, "QQmlParserStatus"))
        return static_cast< QQmlParserStatus*>(const_cast< QQuickRangeModel*>(this));
    if (!strcmp(_clname, "org.qt-project.Qt.QQmlParserStatus"))
        return static_cast< QQmlParserStatus*>(const_cast< QQuickRangeModel*>(this));
    return QObject::qt_metacast(_clname);
}

int QQuickRangeModel::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QObject::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 16)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 16;
    } else if (_c == QMetaObject::RegisterMethodArgumentMetaType) {
        if (_id < 16)
            *reinterpret_cast<int*>(_a[0]) = -1;
        _id -= 16;
    }
#ifndef QT_NO_PROPERTIES
   else if (_c == QMetaObject::ReadProperty || _c == QMetaObject::WriteProperty
            || _c == QMetaObject::ResetProperty || _c == QMetaObject::RegisterPropertyMetaType) {
        qt_static_metacall(this, _c, _id, _a);
        _id -= 8;
    } else if (_c == QMetaObject::QueryPropertyDesignable) {
        _id -= 8;
    } else if (_c == QMetaObject::QueryPropertyScriptable) {
        _id -= 8;
    } else if (_c == QMetaObject::QueryPropertyStored) {
        _id -= 8;
    } else if (_c == QMetaObject::QueryPropertyEditable) {
        _id -= 8;
    } else if (_c == QMetaObject::QueryPropertyUser) {
        _id -= 8;
    }
#endif // QT_NO_PROPERTIES
    return _id;
}

// SIGNAL 0
void QQuickRangeModel::valueChanged(qreal _t1)
{
    void *_a[] = { Q_NULLPTR, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 0, _a);
}

// SIGNAL 1
void QQuickRangeModel::positionChanged(qreal _t1)
{
    void *_a[] = { Q_NULLPTR, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 1, _a);
}

// SIGNAL 2
void QQuickRangeModel::stepSizeChanged(qreal _t1)
{
    void *_a[] = { Q_NULLPTR, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 2, _a);
}

// SIGNAL 3
void QQuickRangeModel::invertedChanged(bool _t1)
{
    void *_a[] = { Q_NULLPTR, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 3, _a);
}

// SIGNAL 4
void QQuickRangeModel::minimumChanged(qreal _t1)
{
    void *_a[] = { Q_NULLPTR, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 4, _a);
}

// SIGNAL 5
void QQuickRangeModel::maximumChanged(qreal _t1)
{
    void *_a[] = { Q_NULLPTR, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 5, _a);
}

// SIGNAL 6
void QQuickRangeModel::positionAtMinimumChanged(qreal _t1)
{
    void *_a[] = { Q_NULLPTR, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 6, _a);
}

// SIGNAL 7
void QQuickRangeModel::positionAtMaximumChanged(qreal _t1)
{
    void *_a[] = { Q_NULLPTR, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 7, _a);
}
QT_END_MOC_NAMESPACE
