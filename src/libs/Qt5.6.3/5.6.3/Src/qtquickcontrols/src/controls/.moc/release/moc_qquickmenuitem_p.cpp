/****************************************************************************
** Meta object code from reading C++ file 'qquickmenuitem_p.h'
**
** Created by: The Qt Meta Object Compiler version 67 (Qt 5.6.3)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "../../qquickmenuitem_p.h"
#include <QtCore/qbytearray.h>
#include <QtCore/qmetatype.h>
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'qquickmenuitem_p.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 67
#error "This file was generated using the moc from 5.6.3. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
struct qt_meta_stringdata_QQuickMenuItemType_t {
    QByteArrayData data[6];
    char stringdata0[68];
};
#define QT_MOC_LITERAL(idx, ofs, len) \
    Q_STATIC_BYTE_ARRAY_DATA_HEADER_INITIALIZER_WITH_OFFSET(len, \
    qptrdiff(offsetof(qt_meta_stringdata_QQuickMenuItemType_t, stringdata0) + ofs \
        - idx * sizeof(QByteArrayData)) \
    )
static const qt_meta_stringdata_QQuickMenuItemType_t qt_meta_stringdata_QQuickMenuItemType = {
    {
QT_MOC_LITERAL(0, 0, 18), // "QQuickMenuItemType"
QT_MOC_LITERAL(1, 19, 12), // "MenuItemType"
QT_MOC_LITERAL(2, 32, 9), // "Separator"
QT_MOC_LITERAL(3, 42, 4), // "Item"
QT_MOC_LITERAL(4, 47, 4), // "Menu"
QT_MOC_LITERAL(5, 52, 15) // "ScrollIndicator"

    },
    "QQuickMenuItemType\0MenuItemType\0"
    "Separator\0Item\0Menu\0ScrollIndicator"
};
#undef QT_MOC_LITERAL

static const uint qt_meta_data_QQuickMenuItemType[] = {

 // content:
       7,       // revision
       0,       // classname
       0,    0, // classinfo
       0,    0, // methods
       0,    0, // properties
       1,   14, // enums/sets
       0,    0, // constructors
       4,       // flags
       0,       // signalCount

 // enums: name, flags, count, data
       1, 0x0,    4,   18,

 // enum data: key, value
       2, uint(QQuickMenuItemType::Separator),
       3, uint(QQuickMenuItemType::Item),
       4, uint(QQuickMenuItemType::Menu),
       5, uint(QQuickMenuItemType::ScrollIndicator),

       0        // eod
};

const QMetaObject QQuickMenuItemType::staticMetaObject = {
    { Q_NULLPTR, qt_meta_stringdata_QQuickMenuItemType.data,
      qt_meta_data_QQuickMenuItemType,  Q_NULLPTR, Q_NULLPTR, Q_NULLPTR}
};

struct qt_meta_stringdata_QQuickMenuBase_t {
    QByteArrayData data[10];
    char stringdata0[126];
};
#define QT_MOC_LITERAL(idx, ofs, len) \
    Q_STATIC_BYTE_ARRAY_DATA_HEADER_INITIALIZER_WITH_OFFSET(len, \
    qptrdiff(offsetof(qt_meta_stringdata_QQuickMenuBase_t, stringdata0) + ofs \
        - idx * sizeof(QByteArrayData)) \
    )
static const qt_meta_stringdata_QQuickMenuBase_t qt_meta_stringdata_QQuickMenuBase = {
    {
QT_MOC_LITERAL(0, 0, 14), // "QQuickMenuBase"
QT_MOC_LITERAL(1, 15, 14), // "visibleChanged"
QT_MOC_LITERAL(2, 30, 0), // ""
QT_MOC_LITERAL(3, 31, 7), // "visible"
QT_MOC_LITERAL(4, 39, 4), // "type"
QT_MOC_LITERAL(5, 44, 32), // "QQuickMenuItemType::MenuItemType"
QT_MOC_LITERAL(6, 77, 12), // "__parentMenu"
QT_MOC_LITERAL(7, 90, 10), // "__isNative"
QT_MOC_LITERAL(8, 101, 12), // "__visualItem"
QT_MOC_LITERAL(9, 114, 11) // "QQuickItem*"

    },
    "QQuickMenuBase\0visibleChanged\0\0visible\0"
    "type\0QQuickMenuItemType::MenuItemType\0"
    "__parentMenu\0__isNative\0__visualItem\0"
    "QQuickItem*"
};
#undef QT_MOC_LITERAL

static const uint qt_meta_data_QQuickMenuBase[] = {

 // content:
       7,       // revision
       0,       // classname
       0,    0, // classinfo
       1,   14, // methods
       5,   20, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       1,       // signalCount

 // signals: name, argc, parameters, tag, flags
       1,    0,   19,    2, 0x06 /* Public */,

 // signals: parameters
    QMetaType::Void,

 // properties: name, type, flags
       3, QMetaType::Bool, 0x00495103,
       4, 0x80000000 | 5, 0x00095409,
       6, QMetaType::QObjectStar, 0x00095401,
       7, QMetaType::Bool, 0x00095401,
       8, 0x80000000 | 9, 0x0009500b,

 // properties: notify_signal_id
       0,
       0,
       0,
       0,
       0,

       0        // eod
};

void QQuickMenuBase::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        QQuickMenuBase *_t = static_cast<QQuickMenuBase *>(_o);
        Q_UNUSED(_t)
        switch (_id) {
        case 0: _t->visibleChanged(); break;
        default: ;
        }
    } else if (_c == QMetaObject::IndexOfMethod) {
        int *result = reinterpret_cast<int *>(_a[0]);
        void **func = reinterpret_cast<void **>(_a[1]);
        {
            typedef void (QQuickMenuBase::*_t)();
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&QQuickMenuBase::visibleChanged)) {
                *result = 0;
                return;
            }
        }
    }
#ifndef QT_NO_PROPERTIES
    else if (_c == QMetaObject::ReadProperty) {
        QQuickMenuBase *_t = static_cast<QQuickMenuBase *>(_o);
        Q_UNUSED(_t)
        void *_v = _a[0];
        switch (_id) {
        case 0: *reinterpret_cast< bool*>(_v) = _t->visible(); break;
        case 1: *reinterpret_cast< QQuickMenuItemType::MenuItemType*>(_v) = _t->type(); break;
        case 2: *reinterpret_cast< QObject**>(_v) = _t->parentMenuOrMenuBar(); break;
        case 3: *reinterpret_cast< bool*>(_v) = _t->isNative(); break;
        case 4: *reinterpret_cast< QQuickItem**>(_v) = _t->visualItem(); break;
        default: break;
        }
    } else if (_c == QMetaObject::WriteProperty) {
        QQuickMenuBase *_t = static_cast<QQuickMenuBase *>(_o);
        Q_UNUSED(_t)
        void *_v = _a[0];
        switch (_id) {
        case 0: _t->setVisible(*reinterpret_cast< bool*>(_v)); break;
        case 4: _t->setVisualItem(*reinterpret_cast< QQuickItem**>(_v)); break;
        default: break;
        }
    } else if (_c == QMetaObject::ResetProperty) {
    }
#endif // QT_NO_PROPERTIES
    Q_UNUSED(_a);
}

static const QMetaObject * const qt_meta_extradata_QQuickMenuBase[] = {
        &QQuickMenuItemType::staticMetaObject,
    Q_NULLPTR
};

const QMetaObject QQuickMenuBase::staticMetaObject = {
    { &QObject::staticMetaObject, qt_meta_stringdata_QQuickMenuBase.data,
      qt_meta_data_QQuickMenuBase,  qt_static_metacall, qt_meta_extradata_QQuickMenuBase, Q_NULLPTR}
};


const QMetaObject *QQuickMenuBase::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->dynamicMetaObject() : &staticMetaObject;
}

void *QQuickMenuBase::qt_metacast(const char *_clname)
{
    if (!_clname) return Q_NULLPTR;
    if (!strcmp(_clname, qt_meta_stringdata_QQuickMenuBase.stringdata0))
        return static_cast<void*>(const_cast< QQuickMenuBase*>(this));
    return QObject::qt_metacast(_clname);
}

int QQuickMenuBase::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QObject::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 1)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 1;
    } else if (_c == QMetaObject::RegisterMethodArgumentMetaType) {
        if (_id < 1)
            *reinterpret_cast<int*>(_a[0]) = -1;
        _id -= 1;
    }
#ifndef QT_NO_PROPERTIES
   else if (_c == QMetaObject::ReadProperty || _c == QMetaObject::WriteProperty
            || _c == QMetaObject::ResetProperty || _c == QMetaObject::RegisterPropertyMetaType) {
        qt_static_metacall(this, _c, _id, _a);
        _id -= 5;
    } else if (_c == QMetaObject::QueryPropertyDesignable) {
        _id -= 5;
    } else if (_c == QMetaObject::QueryPropertyScriptable) {
        _id -= 5;
    } else if (_c == QMetaObject::QueryPropertyStored) {
        _id -= 5;
    } else if (_c == QMetaObject::QueryPropertyEditable) {
        _id -= 5;
    } else if (_c == QMetaObject::QueryPropertyUser) {
        _id -= 5;
    }
#endif // QT_NO_PROPERTIES
    return _id;
}

// SIGNAL 0
void QQuickMenuBase::visibleChanged()
{
    QMetaObject::activate(this, &staticMetaObject, 0, Q_NULLPTR);
}
struct qt_meta_stringdata_QQuickMenuSeparator_t {
    QByteArrayData data[1];
    char stringdata0[20];
};
#define QT_MOC_LITERAL(idx, ofs, len) \
    Q_STATIC_BYTE_ARRAY_DATA_HEADER_INITIALIZER_WITH_OFFSET(len, \
    qptrdiff(offsetof(qt_meta_stringdata_QQuickMenuSeparator_t, stringdata0) + ofs \
        - idx * sizeof(QByteArrayData)) \
    )
static const qt_meta_stringdata_QQuickMenuSeparator_t qt_meta_stringdata_QQuickMenuSeparator = {
    {
QT_MOC_LITERAL(0, 0, 19) // "QQuickMenuSeparator"

    },
    "QQuickMenuSeparator"
};
#undef QT_MOC_LITERAL

static const uint qt_meta_data_QQuickMenuSeparator[] = {

 // content:
       7,       // revision
       0,       // classname
       0,    0, // classinfo
       0,    0, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       0,       // signalCount

       0        // eod
};

void QQuickMenuSeparator::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    Q_UNUSED(_o);
    Q_UNUSED(_id);
    Q_UNUSED(_c);
    Q_UNUSED(_a);
}

const QMetaObject QQuickMenuSeparator::staticMetaObject = {
    { &QQuickMenuBase::staticMetaObject, qt_meta_stringdata_QQuickMenuSeparator.data,
      qt_meta_data_QQuickMenuSeparator,  qt_static_metacall, Q_NULLPTR, Q_NULLPTR}
};


const QMetaObject *QQuickMenuSeparator::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->dynamicMetaObject() : &staticMetaObject;
}

void *QQuickMenuSeparator::qt_metacast(const char *_clname)
{
    if (!_clname) return Q_NULLPTR;
    if (!strcmp(_clname, qt_meta_stringdata_QQuickMenuSeparator.stringdata0))
        return static_cast<void*>(const_cast< QQuickMenuSeparator*>(this));
    return QQuickMenuBase::qt_metacast(_clname);
}

int QQuickMenuSeparator::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QQuickMenuBase::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    return _id;
}
struct qt_meta_stringdata_QQuickMenuText_t {
    QByteArrayData data[14];
    char stringdata0[164];
};
#define QT_MOC_LITERAL(idx, ofs, len) \
    Q_STATIC_BYTE_ARRAY_DATA_HEADER_INITIALIZER_WITH_OFFSET(len, \
    qptrdiff(offsetof(qt_meta_stringdata_QQuickMenuText_t, stringdata0) + ofs \
        - idx * sizeof(QByteArrayData)) \
    )
static const qt_meta_stringdata_QQuickMenuText_t qt_meta_stringdata_QQuickMenuText = {
    {
QT_MOC_LITERAL(0, 0, 14), // "QQuickMenuText"
QT_MOC_LITERAL(1, 15, 14), // "enabledChanged"
QT_MOC_LITERAL(2, 30, 0), // ""
QT_MOC_LITERAL(3, 31, 17), // "iconSourceChanged"
QT_MOC_LITERAL(4, 49, 15), // "iconNameChanged"
QT_MOC_LITERAL(5, 65, 13), // "__textChanged"
QT_MOC_LITERAL(6, 79, 13), // "__iconChanged"
QT_MOC_LITERAL(7, 93, 10), // "updateText"
QT_MOC_LITERAL(8, 104, 13), // "updateEnabled"
QT_MOC_LITERAL(9, 118, 10), // "updateIcon"
QT_MOC_LITERAL(10, 129, 7), // "enabled"
QT_MOC_LITERAL(11, 137, 10), // "iconSource"
QT_MOC_LITERAL(12, 148, 8), // "iconName"
QT_MOC_LITERAL(13, 157, 6) // "__icon"

    },
    "QQuickMenuText\0enabledChanged\0\0"
    "iconSourceChanged\0iconNameChanged\0"
    "__textChanged\0__iconChanged\0updateText\0"
    "updateEnabled\0updateIcon\0enabled\0"
    "iconSource\0iconName\0__icon"
};
#undef QT_MOC_LITERAL

static const uint qt_meta_data_QQuickMenuText[] = {

 // content:
       7,       // revision
       0,       // classname
       0,    0, // classinfo
       8,   14, // methods
       4,   62, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       5,       // signalCount

 // signals: name, argc, parameters, tag, flags
       1,    0,   54,    2, 0x06 /* Public */,
       3,    0,   55,    2, 0x06 /* Public */,
       4,    0,   56,    2, 0x06 /* Public */,
       5,    0,   57,    2, 0x06 /* Public */,
       6,    0,   58,    2, 0x06 /* Public */,

 // slots: name, argc, parameters, tag, flags
       7,    0,   59,    2, 0x09 /* Protected */,
       8,    0,   60,    2, 0x09 /* Protected */,
       9,    0,   61,    2, 0x09 /* Protected */,

 // signals: parameters
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,

 // slots: parameters
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,

 // properties: name, type, flags
      10, QMetaType::Bool, 0x00495103,
      11, QMetaType::QUrl, 0x00495103,
      12, QMetaType::QString, 0x00495103,
      13, QMetaType::QVariant, 0x00495001,

 // properties: notify_signal_id
       0,
       1,
       2,
       4,

       0        // eod
};

void QQuickMenuText::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        QQuickMenuText *_t = static_cast<QQuickMenuText *>(_o);
        Q_UNUSED(_t)
        switch (_id) {
        case 0: _t->enabledChanged(); break;
        case 1: _t->iconSourceChanged(); break;
        case 2: _t->iconNameChanged(); break;
        case 3: _t->__textChanged(); break;
        case 4: _t->__iconChanged(); break;
        case 5: _t->updateText(); break;
        case 6: _t->updateEnabled(); break;
        case 7: _t->updateIcon(); break;
        default: ;
        }
    } else if (_c == QMetaObject::IndexOfMethod) {
        int *result = reinterpret_cast<int *>(_a[0]);
        void **func = reinterpret_cast<void **>(_a[1]);
        {
            typedef void (QQuickMenuText::*_t)();
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&QQuickMenuText::enabledChanged)) {
                *result = 0;
                return;
            }
        }
        {
            typedef void (QQuickMenuText::*_t)();
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&QQuickMenuText::iconSourceChanged)) {
                *result = 1;
                return;
            }
        }
        {
            typedef void (QQuickMenuText::*_t)();
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&QQuickMenuText::iconNameChanged)) {
                *result = 2;
                return;
            }
        }
        {
            typedef void (QQuickMenuText::*_t)();
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&QQuickMenuText::__textChanged)) {
                *result = 3;
                return;
            }
        }
        {
            typedef void (QQuickMenuText::*_t)();
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&QQuickMenuText::__iconChanged)) {
                *result = 4;
                return;
            }
        }
    }
#ifndef QT_NO_PROPERTIES
    else if (_c == QMetaObject::ReadProperty) {
        QQuickMenuText *_t = static_cast<QQuickMenuText *>(_o);
        Q_UNUSED(_t)
        void *_v = _a[0];
        switch (_id) {
        case 0: *reinterpret_cast< bool*>(_v) = _t->enabled(); break;
        case 1: *reinterpret_cast< QUrl*>(_v) = _t->iconSource(); break;
        case 2: *reinterpret_cast< QString*>(_v) = _t->iconName(); break;
        case 3: *reinterpret_cast< QVariant*>(_v) = _t->iconVariant(); break;
        default: break;
        }
    } else if (_c == QMetaObject::WriteProperty) {
        QQuickMenuText *_t = static_cast<QQuickMenuText *>(_o);
        Q_UNUSED(_t)
        void *_v = _a[0];
        switch (_id) {
        case 0: _t->setEnabled(*reinterpret_cast< bool*>(_v)); break;
        case 1: _t->setIconSource(*reinterpret_cast< QUrl*>(_v)); break;
        case 2: _t->setIconName(*reinterpret_cast< QString*>(_v)); break;
        default: break;
        }
    } else if (_c == QMetaObject::ResetProperty) {
    }
#endif // QT_NO_PROPERTIES
    Q_UNUSED(_a);
}

const QMetaObject QQuickMenuText::staticMetaObject = {
    { &QQuickMenuBase::staticMetaObject, qt_meta_stringdata_QQuickMenuText.data,
      qt_meta_data_QQuickMenuText,  qt_static_metacall, Q_NULLPTR, Q_NULLPTR}
};


const QMetaObject *QQuickMenuText::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->dynamicMetaObject() : &staticMetaObject;
}

void *QQuickMenuText::qt_metacast(const char *_clname)
{
    if (!_clname) return Q_NULLPTR;
    if (!strcmp(_clname, qt_meta_stringdata_QQuickMenuText.stringdata0))
        return static_cast<void*>(const_cast< QQuickMenuText*>(this));
    return QQuickMenuBase::qt_metacast(_clname);
}

int QQuickMenuText::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QQuickMenuBase::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 8)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 8;
    } else if (_c == QMetaObject::RegisterMethodArgumentMetaType) {
        if (_id < 8)
            *reinterpret_cast<int*>(_a[0]) = -1;
        _id -= 8;
    }
#ifndef QT_NO_PROPERTIES
   else if (_c == QMetaObject::ReadProperty || _c == QMetaObject::WriteProperty
            || _c == QMetaObject::ResetProperty || _c == QMetaObject::RegisterPropertyMetaType) {
        qt_static_metacall(this, _c, _id, _a);
        _id -= 4;
    } else if (_c == QMetaObject::QueryPropertyDesignable) {
        _id -= 4;
    } else if (_c == QMetaObject::QueryPropertyScriptable) {
        _id -= 4;
    } else if (_c == QMetaObject::QueryPropertyStored) {
        _id -= 4;
    } else if (_c == QMetaObject::QueryPropertyEditable) {
        _id -= 4;
    } else if (_c == QMetaObject::QueryPropertyUser) {
        _id -= 4;
    }
#endif // QT_NO_PROPERTIES
    return _id;
}

// SIGNAL 0
void QQuickMenuText::enabledChanged()
{
    QMetaObject::activate(this, &staticMetaObject, 0, Q_NULLPTR);
}

// SIGNAL 1
void QQuickMenuText::iconSourceChanged()
{
    QMetaObject::activate(this, &staticMetaObject, 1, Q_NULLPTR);
}

// SIGNAL 2
void QQuickMenuText::iconNameChanged()
{
    QMetaObject::activate(this, &staticMetaObject, 2, Q_NULLPTR);
}

// SIGNAL 3
void QQuickMenuText::__textChanged()
{
    QMetaObject::activate(this, &staticMetaObject, 3, Q_NULLPTR);
}

// SIGNAL 4
void QQuickMenuText::__iconChanged()
{
    QMetaObject::activate(this, &staticMetaObject, 4, Q_NULLPTR);
}
struct qt_meta_stringdata_QQuickMenuItem1_t {
    QByteArrayData data[23];
    char stringdata0[290];
};
#define QT_MOC_LITERAL(idx, ofs, len) \
    Q_STATIC_BYTE_ARRAY_DATA_HEADER_INITIALIZER_WITH_OFFSET(len, \
    qptrdiff(offsetof(qt_meta_stringdata_QQuickMenuItem1_t, stringdata0) + ofs \
        - idx * sizeof(QByteArrayData)) \
    )
static const qt_meta_stringdata_QQuickMenuItem1_t qt_meta_stringdata_QQuickMenuItem1 = {
    {
QT_MOC_LITERAL(0, 0, 15), // "QQuickMenuItem1"
QT_MOC_LITERAL(1, 16, 9), // "triggered"
QT_MOC_LITERAL(2, 26, 0), // ""
QT_MOC_LITERAL(3, 27, 7), // "toggled"
QT_MOC_LITERAL(4, 35, 7), // "checked"
QT_MOC_LITERAL(5, 43, 11), // "textChanged"
QT_MOC_LITERAL(6, 55, 16), // "checkableChanged"
QT_MOC_LITERAL(7, 72, 21), // "exclusiveGroupChanged"
QT_MOC_LITERAL(8, 94, 15), // "shortcutChanged"
QT_MOC_LITERAL(9, 110, 13), // "actionChanged"
QT_MOC_LITERAL(10, 124, 7), // "trigger"
QT_MOC_LITERAL(11, 132, 14), // "updateShortcut"
QT_MOC_LITERAL(12, 147, 15), // "updateCheckable"
QT_MOC_LITERAL(13, 163, 13), // "updateChecked"
QT_MOC_LITERAL(14, 177, 12), // "bindToAction"
QT_MOC_LITERAL(15, 190, 13), // "QQuickAction*"
QT_MOC_LITERAL(16, 204, 6), // "action"
QT_MOC_LITERAL(17, 211, 16), // "unbindFromAction"
QT_MOC_LITERAL(18, 228, 4), // "text"
QT_MOC_LITERAL(19, 233, 9), // "checkable"
QT_MOC_LITERAL(20, 243, 14), // "exclusiveGroup"
QT_MOC_LITERAL(21, 258, 22), // "QQuickExclusiveGroup1*"
QT_MOC_LITERAL(22, 281, 8) // "shortcut"

    },
    "QQuickMenuItem1\0triggered\0\0toggled\0"
    "checked\0textChanged\0checkableChanged\0"
    "exclusiveGroupChanged\0shortcutChanged\0"
    "actionChanged\0trigger\0updateShortcut\0"
    "updateCheckable\0updateChecked\0"
    "bindToAction\0QQuickAction*\0action\0"
    "unbindFromAction\0text\0checkable\0"
    "exclusiveGroup\0QQuickExclusiveGroup1*\0"
    "shortcut"
};
#undef QT_MOC_LITERAL

static const uint qt_meta_data_QQuickMenuItem1[] = {

 // content:
       7,       // revision
       0,       // classname
       0,    0, // classinfo
      13,   14, // methods
       6,   98, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       7,       // signalCount

 // signals: name, argc, parameters, tag, flags
       1,    0,   79,    2, 0x06 /* Public */,
       3,    1,   80,    2, 0x06 /* Public */,
       5,    0,   83,    2, 0x06 /* Public */,
       6,    0,   84,    2, 0x06 /* Public */,
       7,    0,   85,    2, 0x06 /* Public */,
       8,    0,   86,    2, 0x06 /* Public */,
       9,    0,   87,    2, 0x06 /* Public */,

 // slots: name, argc, parameters, tag, flags
      10,    0,   88,    2, 0x0a /* Public */,
      11,    0,   89,    2, 0x09 /* Protected */,
      12,    0,   90,    2, 0x09 /* Protected */,
      13,    0,   91,    2, 0x09 /* Protected */,
      14,    1,   92,    2, 0x09 /* Protected */,
      17,    1,   95,    2, 0x09 /* Protected */,

 // signals: parameters
    QMetaType::Void,
    QMetaType::Void, QMetaType::Bool,    4,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,

 // slots: parameters
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void, 0x80000000 | 15,   16,
    QMetaType::Void, QMetaType::QObjectStar,   16,

 // properties: name, type, flags
      18, QMetaType::QString, 0x00495103,
      19, QMetaType::Bool, 0x00495103,
       4, QMetaType::Bool, 0x00495103,
      20, 0x80000000 | 21, 0x0049510b,
      22, QMetaType::QVariant, 0x00495103,
      16, 0x80000000 | 15, 0x0049500b,

 // properties: notify_signal_id
       2,
       3,
       1,
       4,
       5,
       6,

       0        // eod
};

void QQuickMenuItem1::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        QQuickMenuItem1 *_t = static_cast<QQuickMenuItem1 *>(_o);
        Q_UNUSED(_t)
        switch (_id) {
        case 0: _t->triggered(); break;
        case 1: _t->toggled((*reinterpret_cast< bool(*)>(_a[1]))); break;
        case 2: _t->textChanged(); break;
        case 3: _t->checkableChanged(); break;
        case 4: _t->exclusiveGroupChanged(); break;
        case 5: _t->shortcutChanged(); break;
        case 6: _t->actionChanged(); break;
        case 7: _t->trigger(); break;
        case 8: _t->updateShortcut(); break;
        case 9: _t->updateCheckable(); break;
        case 10: _t->updateChecked(); break;
        case 11: _t->bindToAction((*reinterpret_cast< QQuickAction*(*)>(_a[1]))); break;
        case 12: _t->unbindFromAction((*reinterpret_cast< QObject*(*)>(_a[1]))); break;
        default: ;
        }
    } else if (_c == QMetaObject::IndexOfMethod) {
        int *result = reinterpret_cast<int *>(_a[0]);
        void **func = reinterpret_cast<void **>(_a[1]);
        {
            typedef void (QQuickMenuItem1::*_t)();
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&QQuickMenuItem1::triggered)) {
                *result = 0;
                return;
            }
        }
        {
            typedef void (QQuickMenuItem1::*_t)(bool );
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&QQuickMenuItem1::toggled)) {
                *result = 1;
                return;
            }
        }
        {
            typedef void (QQuickMenuItem1::*_t)();
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&QQuickMenuItem1::textChanged)) {
                *result = 2;
                return;
            }
        }
        {
            typedef void (QQuickMenuItem1::*_t)();
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&QQuickMenuItem1::checkableChanged)) {
                *result = 3;
                return;
            }
        }
        {
            typedef void (QQuickMenuItem1::*_t)();
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&QQuickMenuItem1::exclusiveGroupChanged)) {
                *result = 4;
                return;
            }
        }
        {
            typedef void (QQuickMenuItem1::*_t)();
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&QQuickMenuItem1::shortcutChanged)) {
                *result = 5;
                return;
            }
        }
        {
            typedef void (QQuickMenuItem1::*_t)();
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&QQuickMenuItem1::actionChanged)) {
                *result = 6;
                return;
            }
        }
    }
#ifndef QT_NO_PROPERTIES
    else if (_c == QMetaObject::ReadProperty) {
        QQuickMenuItem1 *_t = static_cast<QQuickMenuItem1 *>(_o);
        Q_UNUSED(_t)
        void *_v = _a[0];
        switch (_id) {
        case 0: *reinterpret_cast< QString*>(_v) = _t->text(); break;
        case 1: *reinterpret_cast< bool*>(_v) = _t->checkable(); break;
        case 2: *reinterpret_cast< bool*>(_v) = _t->checked(); break;
        case 3: *reinterpret_cast< QQuickExclusiveGroup1**>(_v) = _t->exclusiveGroup(); break;
        case 4: *reinterpret_cast< QVariant*>(_v) = _t->shortcut(); break;
        case 5: *reinterpret_cast< QQuickAction**>(_v) = _t->boundAction(); break;
        default: break;
        }
    } else if (_c == QMetaObject::WriteProperty) {
        QQuickMenuItem1 *_t = static_cast<QQuickMenuItem1 *>(_o);
        Q_UNUSED(_t)
        void *_v = _a[0];
        switch (_id) {
        case 0: _t->setText(*reinterpret_cast< QString*>(_v)); break;
        case 1: _t->setCheckable(*reinterpret_cast< bool*>(_v)); break;
        case 2: _t->setChecked(*reinterpret_cast< bool*>(_v)); break;
        case 3: _t->setExclusiveGroup(*reinterpret_cast< QQuickExclusiveGroup1**>(_v)); break;
        case 4: _t->setShortcut(*reinterpret_cast< QVariant*>(_v)); break;
        case 5: _t->setBoundAction(*reinterpret_cast< QQuickAction**>(_v)); break;
        default: break;
        }
    } else if (_c == QMetaObject::ResetProperty) {
    }
#endif // QT_NO_PROPERTIES
}

const QMetaObject QQuickMenuItem1::staticMetaObject = {
    { &QQuickMenuText::staticMetaObject, qt_meta_stringdata_QQuickMenuItem1.data,
      qt_meta_data_QQuickMenuItem1,  qt_static_metacall, Q_NULLPTR, Q_NULLPTR}
};


const QMetaObject *QQuickMenuItem1::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->dynamicMetaObject() : &staticMetaObject;
}

void *QQuickMenuItem1::qt_metacast(const char *_clname)
{
    if (!_clname) return Q_NULLPTR;
    if (!strcmp(_clname, qt_meta_stringdata_QQuickMenuItem1.stringdata0))
        return static_cast<void*>(const_cast< QQuickMenuItem1*>(this));
    return QQuickMenuText::qt_metacast(_clname);
}

int QQuickMenuItem1::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QQuickMenuText::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 13)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 13;
    } else if (_c == QMetaObject::RegisterMethodArgumentMetaType) {
        if (_id < 13)
            *reinterpret_cast<int*>(_a[0]) = -1;
        _id -= 13;
    }
#ifndef QT_NO_PROPERTIES
   else if (_c == QMetaObject::ReadProperty || _c == QMetaObject::WriteProperty
            || _c == QMetaObject::ResetProperty || _c == QMetaObject::RegisterPropertyMetaType) {
        qt_static_metacall(this, _c, _id, _a);
        _id -= 6;
    } else if (_c == QMetaObject::QueryPropertyDesignable) {
        _id -= 6;
    } else if (_c == QMetaObject::QueryPropertyScriptable) {
        _id -= 6;
    } else if (_c == QMetaObject::QueryPropertyStored) {
        _id -= 6;
    } else if (_c == QMetaObject::QueryPropertyEditable) {
        _id -= 6;
    } else if (_c == QMetaObject::QueryPropertyUser) {
        _id -= 6;
    }
#endif // QT_NO_PROPERTIES
    return _id;
}

// SIGNAL 0
void QQuickMenuItem1::triggered()
{
    QMetaObject::activate(this, &staticMetaObject, 0, Q_NULLPTR);
}

// SIGNAL 1
void QQuickMenuItem1::toggled(bool _t1)
{
    void *_a[] = { Q_NULLPTR, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 1, _a);
}

// SIGNAL 2
void QQuickMenuItem1::textChanged()
{
    QMetaObject::activate(this, &staticMetaObject, 2, Q_NULLPTR);
}

// SIGNAL 3
void QQuickMenuItem1::checkableChanged()
{
    QMetaObject::activate(this, &staticMetaObject, 3, Q_NULLPTR);
}

// SIGNAL 4
void QQuickMenuItem1::exclusiveGroupChanged()
{
    QMetaObject::activate(this, &staticMetaObject, 4, Q_NULLPTR);
}

// SIGNAL 5
void QQuickMenuItem1::shortcutChanged()
{
    QMetaObject::activate(this, &staticMetaObject, 5, Q_NULLPTR);
}

// SIGNAL 6
void QQuickMenuItem1::actionChanged()
{
    QMetaObject::activate(this, &staticMetaObject, 6, Q_NULLPTR);
}
QT_END_MOC_NAMESPACE
