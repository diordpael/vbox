/****************************************************************************
** Meta object code from reading C++ file 'qquickstack_p.h'
**
** Created by: The Qt Meta Object Compiler version 67 (Qt 5.6.3)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "../../qquickstack_p.h"
#include <QtCore/qbytearray.h>
#include <QtCore/qmetatype.h>
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'qquickstack_p.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 67
#error "This file was generated using the moc from 5.6.3. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
struct qt_meta_stringdata_QQuickStack_t {
    QByteArrayData data[17];
    char stringdata0[153];
};
#define QT_MOC_LITERAL(idx, ofs, len) \
    Q_STATIC_BYTE_ARRAY_DATA_HEADER_INITIALIZER_WITH_OFFSET(len, \
    qptrdiff(offsetof(qt_meta_stringdata_QQuickStack_t, stringdata0) + ofs \
        - idx * sizeof(QByteArrayData)) \
    )
static const qt_meta_stringdata_QQuickStack_t qt_meta_stringdata_QQuickStack = {
    {
QT_MOC_LITERAL(0, 0, 11), // "QQuickStack"
QT_MOC_LITERAL(1, 12, 13), // "statusChanged"
QT_MOC_LITERAL(2, 26, 0), // ""
QT_MOC_LITERAL(3, 27, 11), // "viewChanged"
QT_MOC_LITERAL(4, 39, 12), // "indexChanged"
QT_MOC_LITERAL(5, 52, 5), // "index"
QT_MOC_LITERAL(6, 58, 7), // "__index"
QT_MOC_LITERAL(7, 66, 6), // "status"
QT_MOC_LITERAL(8, 73, 6), // "Status"
QT_MOC_LITERAL(9, 80, 8), // "__status"
QT_MOC_LITERAL(10, 89, 4), // "view"
QT_MOC_LITERAL(11, 94, 11), // "QQuickItem*"
QT_MOC_LITERAL(12, 106, 6), // "__view"
QT_MOC_LITERAL(13, 113, 8), // "Inactive"
QT_MOC_LITERAL(14, 122, 12), // "Deactivating"
QT_MOC_LITERAL(15, 135, 10), // "Activating"
QT_MOC_LITERAL(16, 146, 6) // "Active"

    },
    "QQuickStack\0statusChanged\0\0viewChanged\0"
    "indexChanged\0index\0__index\0status\0"
    "Status\0__status\0view\0QQuickItem*\0"
    "__view\0Inactive\0Deactivating\0Activating\0"
    "Active"
};
#undef QT_MOC_LITERAL

static const uint qt_meta_data_QQuickStack[] = {

 // content:
       7,       // revision
       0,       // classname
       0,    0, // classinfo
       3,   14, // methods
       6,   32, // properties
       1,   56, // enums/sets
       0,    0, // constructors
       0,       // flags
       3,       // signalCount

 // signals: name, argc, parameters, tag, flags
       1,    0,   29,    2, 0x06 /* Public */,
       3,    0,   30,    2, 0x06 /* Public */,
       4,    0,   31,    2, 0x06 /* Public */,

 // signals: parameters
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,

 // properties: name, type, flags
       5, QMetaType::Int, 0x00495001,
       6, QMetaType::Int, 0x00495003,
       7, 0x80000000 | 8, 0x00495009,
       9, 0x80000000 | 8, 0x0049500b,
      10, 0x80000000 | 11, 0x00495009,
      12, 0x80000000 | 11, 0x0049500b,

 // properties: notify_signal_id
       2,
       2,
       0,
       0,
       1,
       1,

 // enums: name, flags, count, data
       8, 0x0,    4,   60,

 // enum data: key, value
      13, uint(QQuickStack::Inactive),
      14, uint(QQuickStack::Deactivating),
      15, uint(QQuickStack::Activating),
      16, uint(QQuickStack::Active),

       0        // eod
};

void QQuickStack::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        QQuickStack *_t = static_cast<QQuickStack *>(_o);
        Q_UNUSED(_t)
        switch (_id) {
        case 0: _t->statusChanged(); break;
        case 1: _t->viewChanged(); break;
        case 2: _t->indexChanged(); break;
        default: ;
        }
    } else if (_c == QMetaObject::IndexOfMethod) {
        int *result = reinterpret_cast<int *>(_a[0]);
        void **func = reinterpret_cast<void **>(_a[1]);
        {
            typedef void (QQuickStack::*_t)();
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&QQuickStack::statusChanged)) {
                *result = 0;
                return;
            }
        }
        {
            typedef void (QQuickStack::*_t)();
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&QQuickStack::viewChanged)) {
                *result = 1;
                return;
            }
        }
        {
            typedef void (QQuickStack::*_t)();
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&QQuickStack::indexChanged)) {
                *result = 2;
                return;
            }
        }
    } else if (_c == QMetaObject::RegisterPropertyMetaType) {
        switch (_id) {
        default: *reinterpret_cast<int*>(_a[0]) = -1; break;
        case 5:
        case 4:
            *reinterpret_cast<int*>(_a[0]) = qRegisterMetaType< QQuickItem* >(); break;
        }
    }

#ifndef QT_NO_PROPERTIES
    else if (_c == QMetaObject::ReadProperty) {
        QQuickStack *_t = static_cast<QQuickStack *>(_o);
        Q_UNUSED(_t)
        void *_v = _a[0];
        switch (_id) {
        case 0: *reinterpret_cast< int*>(_v) = _t->index(); break;
        case 1: *reinterpret_cast< int*>(_v) = _t->index(); break;
        case 2: *reinterpret_cast< Status*>(_v) = _t->status(); break;
        case 3: *reinterpret_cast< Status*>(_v) = _t->status(); break;
        case 4: *reinterpret_cast< QQuickItem**>(_v) = _t->view(); break;
        case 5: *reinterpret_cast< QQuickItem**>(_v) = _t->view(); break;
        default: break;
        }
    } else if (_c == QMetaObject::WriteProperty) {
        QQuickStack *_t = static_cast<QQuickStack *>(_o);
        Q_UNUSED(_t)
        void *_v = _a[0];
        switch (_id) {
        case 1: _t->setIndex(*reinterpret_cast< int*>(_v)); break;
        case 3: _t->setStatus(*reinterpret_cast< Status*>(_v)); break;
        case 5: _t->setView(*reinterpret_cast< QQuickItem**>(_v)); break;
        default: break;
        }
    } else if (_c == QMetaObject::ResetProperty) {
    }
#endif // QT_NO_PROPERTIES
}

const QMetaObject QQuickStack::staticMetaObject = {
    { &QObject::staticMetaObject, qt_meta_stringdata_QQuickStack.data,
      qt_meta_data_QQuickStack,  qt_static_metacall, Q_NULLPTR, Q_NULLPTR}
};


const QMetaObject *QQuickStack::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->dynamicMetaObject() : &staticMetaObject;
}

void *QQuickStack::qt_metacast(const char *_clname)
{
    if (!_clname) return Q_NULLPTR;
    if (!strcmp(_clname, qt_meta_stringdata_QQuickStack.stringdata0))
        return static_cast<void*>(const_cast< QQuickStack*>(this));
    return QObject::qt_metacast(_clname);
}

int QQuickStack::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QObject::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 3)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 3;
    } else if (_c == QMetaObject::RegisterMethodArgumentMetaType) {
        if (_id < 3)
            *reinterpret_cast<int*>(_a[0]) = -1;
        _id -= 3;
    }
#ifndef QT_NO_PROPERTIES
   else if (_c == QMetaObject::ReadProperty || _c == QMetaObject::WriteProperty
            || _c == QMetaObject::ResetProperty || _c == QMetaObject::RegisterPropertyMetaType) {
        qt_static_metacall(this, _c, _id, _a);
        _id -= 6;
    } else if (_c == QMetaObject::QueryPropertyDesignable) {
        _id -= 6;
    } else if (_c == QMetaObject::QueryPropertyScriptable) {
        _id -= 6;
    } else if (_c == QMetaObject::QueryPropertyStored) {
        _id -= 6;
    } else if (_c == QMetaObject::QueryPropertyEditable) {
        _id -= 6;
    } else if (_c == QMetaObject::QueryPropertyUser) {
        _id -= 6;
    }
#endif // QT_NO_PROPERTIES
    return _id;
}

// SIGNAL 0
void QQuickStack::statusChanged()
{
    QMetaObject::activate(this, &staticMetaObject, 0, Q_NULLPTR);
}

// SIGNAL 1
void QQuickStack::viewChanged()
{
    QMetaObject::activate(this, &staticMetaObject, 1, Q_NULLPTR);
}

// SIGNAL 2
void QQuickStack::indexChanged()
{
    QMetaObject::activate(this, &staticMetaObject, 2, Q_NULLPTR);
}
QT_END_MOC_NAMESPACE
