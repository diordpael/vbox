/****************************************************************************
** Meta object code from reading C++ file 'qquicktooltip_p.h'
**
** Created by: The Qt Meta Object Compiler version 67 (Qt 5.6.3)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "../../Private/qquicktooltip_p.h"
#include <QtCore/qbytearray.h>
#include <QtCore/qmetatype.h>
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'qquicktooltip_p.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 67
#error "This file was generated using the moc from 5.6.3. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
struct qt_meta_stringdata_QQuickTooltip_t {
    QByteArrayData data[8];
    char stringdata0[59];
};
#define QT_MOC_LITERAL(idx, ofs, len) \
    Q_STATIC_BYTE_ARRAY_DATA_HEADER_INITIALIZER_WITH_OFFSET(len, \
    qptrdiff(offsetof(qt_meta_stringdata_QQuickTooltip_t, stringdata0) + ofs \
        - idx * sizeof(QByteArrayData)) \
    )
static const qt_meta_stringdata_QQuickTooltip_t qt_meta_stringdata_QQuickTooltip = {
    {
QT_MOC_LITERAL(0, 0, 13), // "QQuickTooltip"
QT_MOC_LITERAL(1, 14, 8), // "showText"
QT_MOC_LITERAL(2, 23, 0), // ""
QT_MOC_LITERAL(3, 24, 11), // "QQuickItem*"
QT_MOC_LITERAL(4, 36, 4), // "item"
QT_MOC_LITERAL(5, 41, 3), // "pos"
QT_MOC_LITERAL(6, 45, 4), // "text"
QT_MOC_LITERAL(7, 50, 8) // "hideText"

    },
    "QQuickTooltip\0showText\0\0QQuickItem*\0"
    "item\0pos\0text\0hideText"
};
#undef QT_MOC_LITERAL

static const uint qt_meta_data_QQuickTooltip[] = {

 // content:
       7,       // revision
       0,       // classname
       0,    0, // classinfo
       2,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       0,       // signalCount

 // methods: name, argc, parameters, tag, flags
       1,    3,   24,    2, 0x02 /* Public */,
       7,    0,   31,    2, 0x02 /* Public */,

 // methods: parameters
    QMetaType::Void, 0x80000000 | 3, QMetaType::QPointF, QMetaType::QString,    4,    5,    6,
    QMetaType::Void,

       0        // eod
};

void QQuickTooltip::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        QQuickTooltip *_t = static_cast<QQuickTooltip *>(_o);
        Q_UNUSED(_t)
        switch (_id) {
        case 0: _t->showText((*reinterpret_cast< QQuickItem*(*)>(_a[1])),(*reinterpret_cast< const QPointF(*)>(_a[2])),(*reinterpret_cast< const QString(*)>(_a[3]))); break;
        case 1: _t->hideText(); break;
        default: ;
        }
    }
}

const QMetaObject QQuickTooltip::staticMetaObject = {
    { &QObject::staticMetaObject, qt_meta_stringdata_QQuickTooltip.data,
      qt_meta_data_QQuickTooltip,  qt_static_metacall, Q_NULLPTR, Q_NULLPTR}
};


const QMetaObject *QQuickTooltip::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->dynamicMetaObject() : &staticMetaObject;
}

void *QQuickTooltip::qt_metacast(const char *_clname)
{
    if (!_clname) return Q_NULLPTR;
    if (!strcmp(_clname, qt_meta_stringdata_QQuickTooltip.stringdata0))
        return static_cast<void*>(const_cast< QQuickTooltip*>(this));
    return QObject::qt_metacast(_clname);
}

int QQuickTooltip::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QObject::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 2)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 2;
    } else if (_c == QMetaObject::RegisterMethodArgumentMetaType) {
        if (_id < 2)
            *reinterpret_cast<int*>(_a[0]) = -1;
        _id -= 2;
    }
    return _id;
}
QT_END_MOC_NAMESPACE
