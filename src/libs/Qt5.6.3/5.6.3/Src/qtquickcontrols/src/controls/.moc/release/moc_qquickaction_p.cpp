/****************************************************************************
** Meta object code from reading C++ file 'qquickaction_p.h'
**
** Created by: The Qt Meta Object Compiler version 67 (Qt 5.6.3)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "../../qquickaction_p.h"
#include <QtCore/qbytearray.h>
#include <QtCore/qmetatype.h>
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'qquickaction_p.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 67
#error "This file was generated using the moc from 5.6.3. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
struct qt_meta_stringdata_QQuickAction_t {
    QByteArrayData data[27];
    char stringdata0[307];
};
#define QT_MOC_LITERAL(idx, ofs, len) \
    Q_STATIC_BYTE_ARRAY_DATA_HEADER_INITIALIZER_WITH_OFFSET(len, \
    qptrdiff(offsetof(qt_meta_stringdata_QQuickAction_t, stringdata0) + ofs \
        - idx * sizeof(QByteArrayData)) \
    )
static const qt_meta_stringdata_QQuickAction_t qt_meta_stringdata_QQuickAction = {
    {
QT_MOC_LITERAL(0, 0, 12), // "QQuickAction"
QT_MOC_LITERAL(1, 13, 9), // "triggered"
QT_MOC_LITERAL(2, 23, 0), // ""
QT_MOC_LITERAL(3, 24, 6), // "source"
QT_MOC_LITERAL(4, 31, 7), // "toggled"
QT_MOC_LITERAL(5, 39, 7), // "checked"
QT_MOC_LITERAL(6, 47, 11), // "textChanged"
QT_MOC_LITERAL(7, 59, 15), // "shortcutChanged"
QT_MOC_LITERAL(8, 75, 8), // "shortcut"
QT_MOC_LITERAL(9, 84, 11), // "iconChanged"
QT_MOC_LITERAL(10, 96, 15), // "iconNameChanged"
QT_MOC_LITERAL(11, 112, 17), // "iconSourceChanged"
QT_MOC_LITERAL(12, 130, 14), // "tooltipChanged"
QT_MOC_LITERAL(13, 145, 3), // "arg"
QT_MOC_LITERAL(14, 149, 14), // "enabledChanged"
QT_MOC_LITERAL(15, 164, 16), // "checkableChanged"
QT_MOC_LITERAL(16, 181, 21), // "exclusiveGroupChanged"
QT_MOC_LITERAL(17, 203, 7), // "trigger"
QT_MOC_LITERAL(18, 211, 4), // "text"
QT_MOC_LITERAL(19, 216, 10), // "iconSource"
QT_MOC_LITERAL(20, 227, 8), // "iconName"
QT_MOC_LITERAL(21, 236, 6), // "__icon"
QT_MOC_LITERAL(22, 243, 7), // "tooltip"
QT_MOC_LITERAL(23, 251, 7), // "enabled"
QT_MOC_LITERAL(24, 259, 9), // "checkable"
QT_MOC_LITERAL(25, 269, 14), // "exclusiveGroup"
QT_MOC_LITERAL(26, 284, 22) // "QQuickExclusiveGroup1*"

    },
    "QQuickAction\0triggered\0\0source\0toggled\0"
    "checked\0textChanged\0shortcutChanged\0"
    "shortcut\0iconChanged\0iconNameChanged\0"
    "iconSourceChanged\0tooltipChanged\0arg\0"
    "enabledChanged\0checkableChanged\0"
    "exclusiveGroupChanged\0trigger\0text\0"
    "iconSource\0iconName\0__icon\0tooltip\0"
    "enabled\0checkable\0exclusiveGroup\0"
    "QQuickExclusiveGroup1*"
};
#undef QT_MOC_LITERAL

static const uint qt_meta_data_QQuickAction[] = {

 // content:
       7,       // revision
       0,       // classname
       0,    0, // classinfo
      14,   14, // methods
      10,  108, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
      12,       // signalCount

 // signals: name, argc, parameters, tag, flags
       1,    1,   84,    2, 0x06 /* Public */,
       1,    0,   87,    2, 0x26 /* Public | MethodCloned */,
       4,    1,   88,    2, 0x06 /* Public */,
       6,    0,   91,    2, 0x06 /* Public */,
       7,    1,   92,    2, 0x06 /* Public */,
       9,    0,   95,    2, 0x06 /* Public */,
      10,    0,   96,    2, 0x06 /* Public */,
      11,    0,   97,    2, 0x06 /* Public */,
      12,    1,   98,    2, 0x06 /* Public */,
      14,    0,  101,    2, 0x06 /* Public */,
      15,    0,  102,    2, 0x06 /* Public */,
      16,    0,  103,    2, 0x06 /* Public */,

 // slots: name, argc, parameters, tag, flags
      17,    1,  104,    2, 0x0a /* Public */,
      17,    0,  107,    2, 0x2a /* Public | MethodCloned */,

 // signals: parameters
    QMetaType::Void, QMetaType::QObjectStar,    3,
    QMetaType::Void,
    QMetaType::Void, QMetaType::Bool,    5,
    QMetaType::Void,
    QMetaType::Void, QMetaType::QVariant,    8,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void, QMetaType::QString,   13,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,

 // slots: parameters
    QMetaType::Void, QMetaType::QObjectStar,    3,
    QMetaType::Void,

 // properties: name, type, flags
      18, QMetaType::QString, 0x00495107,
      19, QMetaType::QUrl, 0x00495107,
      20, QMetaType::QString, 0x00495103,
      21, QMetaType::QVariant, 0x00495001,
      22, QMetaType::QString, 0x00495107,
      23, QMetaType::Bool, 0x00495103,
      24, QMetaType::Bool, 0x00495103,
       5, QMetaType::Bool, 0x00495103,
      25, 0x80000000 | 26, 0x0049510b,
       8, QMetaType::QVariant, 0x00495103,

 // properties: notify_signal_id
       3,
       7,
       6,
       5,
       8,
       9,
      10,
       2,
      11,
       4,

       0        // eod
};

void QQuickAction::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        QQuickAction *_t = static_cast<QQuickAction *>(_o);
        Q_UNUSED(_t)
        switch (_id) {
        case 0: _t->triggered((*reinterpret_cast< QObject*(*)>(_a[1]))); break;
        case 1: _t->triggered(); break;
        case 2: _t->toggled((*reinterpret_cast< bool(*)>(_a[1]))); break;
        case 3: _t->textChanged(); break;
        case 4: _t->shortcutChanged((*reinterpret_cast< const QVariant(*)>(_a[1]))); break;
        case 5: _t->iconChanged(); break;
        case 6: _t->iconNameChanged(); break;
        case 7: _t->iconSourceChanged(); break;
        case 8: _t->tooltipChanged((*reinterpret_cast< const QString(*)>(_a[1]))); break;
        case 9: _t->enabledChanged(); break;
        case 10: _t->checkableChanged(); break;
        case 11: _t->exclusiveGroupChanged(); break;
        case 12: _t->trigger((*reinterpret_cast< QObject*(*)>(_a[1]))); break;
        case 13: _t->trigger(); break;
        default: ;
        }
    } else if (_c == QMetaObject::IndexOfMethod) {
        int *result = reinterpret_cast<int *>(_a[0]);
        void **func = reinterpret_cast<void **>(_a[1]);
        {
            typedef void (QQuickAction::*_t)(QObject * );
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&QQuickAction::triggered)) {
                *result = 0;
                return;
            }
        }
        {
            typedef void (QQuickAction::*_t)(bool );
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&QQuickAction::toggled)) {
                *result = 2;
                return;
            }
        }
        {
            typedef void (QQuickAction::*_t)();
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&QQuickAction::textChanged)) {
                *result = 3;
                return;
            }
        }
        {
            typedef void (QQuickAction::*_t)(const QVariant & );
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&QQuickAction::shortcutChanged)) {
                *result = 4;
                return;
            }
        }
        {
            typedef void (QQuickAction::*_t)();
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&QQuickAction::iconChanged)) {
                *result = 5;
                return;
            }
        }
        {
            typedef void (QQuickAction::*_t)();
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&QQuickAction::iconNameChanged)) {
                *result = 6;
                return;
            }
        }
        {
            typedef void (QQuickAction::*_t)();
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&QQuickAction::iconSourceChanged)) {
                *result = 7;
                return;
            }
        }
        {
            typedef void (QQuickAction::*_t)(const QString & );
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&QQuickAction::tooltipChanged)) {
                *result = 8;
                return;
            }
        }
        {
            typedef void (QQuickAction::*_t)();
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&QQuickAction::enabledChanged)) {
                *result = 9;
                return;
            }
        }
        {
            typedef void (QQuickAction::*_t)();
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&QQuickAction::checkableChanged)) {
                *result = 10;
                return;
            }
        }
        {
            typedef void (QQuickAction::*_t)();
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&QQuickAction::exclusiveGroupChanged)) {
                *result = 11;
                return;
            }
        }
    }
#ifndef QT_NO_PROPERTIES
    else if (_c == QMetaObject::ReadProperty) {
        QQuickAction *_t = static_cast<QQuickAction *>(_o);
        Q_UNUSED(_t)
        void *_v = _a[0];
        switch (_id) {
        case 0: *reinterpret_cast< QString*>(_v) = _t->text(); break;
        case 1: *reinterpret_cast< QUrl*>(_v) = _t->iconSource(); break;
        case 2: *reinterpret_cast< QString*>(_v) = _t->iconName(); break;
        case 3: *reinterpret_cast< QVariant*>(_v) = _t->iconVariant(); break;
        case 4: *reinterpret_cast< QString*>(_v) = _t->tooltip(); break;
        case 5: *reinterpret_cast< bool*>(_v) = _t->isEnabled(); break;
        case 6: *reinterpret_cast< bool*>(_v) = _t->isCheckable(); break;
        case 7: *reinterpret_cast< bool*>(_v) = _t->isChecked(); break;
        case 8: *reinterpret_cast< QQuickExclusiveGroup1**>(_v) = _t->exclusiveGroup(); break;
        case 9: *reinterpret_cast< QVariant*>(_v) = _t->shortcut(); break;
        default: break;
        }
    } else if (_c == QMetaObject::WriteProperty) {
        QQuickAction *_t = static_cast<QQuickAction *>(_o);
        Q_UNUSED(_t)
        void *_v = _a[0];
        switch (_id) {
        case 0: _t->setText(*reinterpret_cast< QString*>(_v)); break;
        case 1: _t->setIconSource(*reinterpret_cast< QUrl*>(_v)); break;
        case 2: _t->setIconName(*reinterpret_cast< QString*>(_v)); break;
        case 4: _t->setTooltip(*reinterpret_cast< QString*>(_v)); break;
        case 5: _t->setEnabled(*reinterpret_cast< bool*>(_v)); break;
        case 6: _t->setCheckable(*reinterpret_cast< bool*>(_v)); break;
        case 7: _t->setChecked(*reinterpret_cast< bool*>(_v)); break;
        case 8: _t->setExclusiveGroup(*reinterpret_cast< QQuickExclusiveGroup1**>(_v)); break;
        case 9: _t->setShortcut(*reinterpret_cast< QVariant*>(_v)); break;
        default: break;
        }
    } else if (_c == QMetaObject::ResetProperty) {
        QQuickAction *_t = static_cast<QQuickAction *>(_o);
        Q_UNUSED(_t)
        switch (_id) {
        case 0: _t->resetText(); break;
        case 1: _t->resetIconSource(); break;
        case 4: _t->resetTooltip(); break;
        default: break;
        }
    }
#endif // QT_NO_PROPERTIES
}

const QMetaObject QQuickAction::staticMetaObject = {
    { &QObject::staticMetaObject, qt_meta_stringdata_QQuickAction.data,
      qt_meta_data_QQuickAction,  qt_static_metacall, Q_NULLPTR, Q_NULLPTR}
};


const QMetaObject *QQuickAction::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->dynamicMetaObject() : &staticMetaObject;
}

void *QQuickAction::qt_metacast(const char *_clname)
{
    if (!_clname) return Q_NULLPTR;
    if (!strcmp(_clname, qt_meta_stringdata_QQuickAction.stringdata0))
        return static_cast<void*>(const_cast< QQuickAction*>(this));
    return QObject::qt_metacast(_clname);
}

int QQuickAction::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QObject::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 14)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 14;
    } else if (_c == QMetaObject::RegisterMethodArgumentMetaType) {
        if (_id < 14)
            *reinterpret_cast<int*>(_a[0]) = -1;
        _id -= 14;
    }
#ifndef QT_NO_PROPERTIES
   else if (_c == QMetaObject::ReadProperty || _c == QMetaObject::WriteProperty
            || _c == QMetaObject::ResetProperty || _c == QMetaObject::RegisterPropertyMetaType) {
        qt_static_metacall(this, _c, _id, _a);
        _id -= 10;
    } else if (_c == QMetaObject::QueryPropertyDesignable) {
        bool *_b = reinterpret_cast<bool*>(_a[0]);
        switch (_id) {
        case 7: *_b = isCheckable(); break;
        default: break;
        }
        _id -= 10;
    } else if (_c == QMetaObject::QueryPropertyScriptable) {
        _id -= 10;
    } else if (_c == QMetaObject::QueryPropertyStored) {
        _id -= 10;
    } else if (_c == QMetaObject::QueryPropertyEditable) {
        _id -= 10;
    } else if (_c == QMetaObject::QueryPropertyUser) {
        _id -= 10;
    }
#endif // QT_NO_PROPERTIES
    return _id;
}

// SIGNAL 0
void QQuickAction::triggered(QObject * _t1)
{
    void *_a[] = { Q_NULLPTR, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 0, _a);
}

// SIGNAL 2
void QQuickAction::toggled(bool _t1)
{
    void *_a[] = { Q_NULLPTR, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 2, _a);
}

// SIGNAL 3
void QQuickAction::textChanged()
{
    QMetaObject::activate(this, &staticMetaObject, 3, Q_NULLPTR);
}

// SIGNAL 4
void QQuickAction::shortcutChanged(const QVariant & _t1)
{
    void *_a[] = { Q_NULLPTR, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 4, _a);
}

// SIGNAL 5
void QQuickAction::iconChanged()
{
    QMetaObject::activate(this, &staticMetaObject, 5, Q_NULLPTR);
}

// SIGNAL 6
void QQuickAction::iconNameChanged()
{
    QMetaObject::activate(this, &staticMetaObject, 6, Q_NULLPTR);
}

// SIGNAL 7
void QQuickAction::iconSourceChanged()
{
    QMetaObject::activate(this, &staticMetaObject, 7, Q_NULLPTR);
}

// SIGNAL 8
void QQuickAction::tooltipChanged(const QString & _t1)
{
    void *_a[] = { Q_NULLPTR, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 8, _a);
}

// SIGNAL 9
void QQuickAction::enabledChanged()
{
    QMetaObject::activate(this, &staticMetaObject, 9, Q_NULLPTR);
}

// SIGNAL 10
void QQuickAction::checkableChanged()
{
    QMetaObject::activate(this, &staticMetaObject, 10, Q_NULLPTR);
}

// SIGNAL 11
void QQuickAction::exclusiveGroupChanged()
{
    QMetaObject::activate(this, &staticMetaObject, 11, Q_NULLPTR);
}
QT_END_MOC_NAMESPACE
