/****************************************************************************
** Meta object code from reading C++ file 'qquickstyleitem_p.h'
**
** Created by: The Qt Meta Object Compiler version 67 (Qt 5.6.3)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "../../Private/qquickstyleitem_p.h"
#include <QtCore/qbytearray.h>
#include <QtCore/qmetatype.h>
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'qquickstyleitem_p.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 67
#error "This file was generated using the moc from 5.6.3. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
struct qt_meta_stringdata_QQuickStyleItem_t {
    QByteArrayData data[75];
    char stringdata0[864];
};
#define QT_MOC_LITERAL(idx, ofs, len) \
    Q_STATIC_BYTE_ARRAY_DATA_HEADER_INITIALIZER_WITH_OFFSET(len, \
    qptrdiff(offsetof(qt_meta_stringdata_QQuickStyleItem_t, stringdata0) + ofs \
        - idx * sizeof(QByteArrayData)) \
    )
static const qt_meta_stringdata_QQuickStyleItem_t qt_meta_stringdata_QQuickStyleItem = {
    {
QT_MOC_LITERAL(0, 0, 15), // "QQuickStyleItem"
QT_MOC_LITERAL(1, 16, 18), // "elementTypeChanged"
QT_MOC_LITERAL(2, 35, 0), // ""
QT_MOC_LITERAL(3, 36, 11), // "textChanged"
QT_MOC_LITERAL(4, 48, 13), // "sunkenChanged"
QT_MOC_LITERAL(5, 62, 13), // "raisedChanged"
QT_MOC_LITERAL(6, 76, 13), // "activeChanged"
QT_MOC_LITERAL(7, 90, 15), // "selectedChanged"
QT_MOC_LITERAL(8, 106, 15), // "hasFocusChanged"
QT_MOC_LITERAL(9, 122, 9), // "onChanged"
QT_MOC_LITERAL(10, 132, 12), // "hoverChanged"
QT_MOC_LITERAL(11, 145, 17), // "horizontalChanged"
QT_MOC_LITERAL(12, 163, 16), // "transientChanged"
QT_MOC_LITERAL(13, 180, 14), // "minimumChanged"
QT_MOC_LITERAL(14, 195, 14), // "maximumChanged"
QT_MOC_LITERAL(15, 210, 11), // "stepChanged"
QT_MOC_LITERAL(16, 222, 12), // "valueChanged"
QT_MOC_LITERAL(17, 235, 20), // "activeControlChanged"
QT_MOC_LITERAL(18, 256, 11), // "infoChanged"
QT_MOC_LITERAL(19, 268, 12), // "styleChanged"
QT_MOC_LITERAL(20, 281, 19), // "paintMarginsChanged"
QT_MOC_LITERAL(21, 301, 11), // "hintChanged"
QT_MOC_LITERAL(22, 313, 17), // "propertiesChanged"
QT_MOC_LITERAL(23, 331, 11), // "fontChanged"
QT_MOC_LITERAL(24, 343, 19), // "contentWidthChanged"
QT_MOC_LITERAL(25, 363, 3), // "arg"
QT_MOC_LITERAL(26, 367, 20), // "contentHeightChanged"
QT_MOC_LITERAL(27, 388, 19), // "textureWidthChanged"
QT_MOC_LITERAL(28, 408, 1), // "w"
QT_MOC_LITERAL(29, 410, 20), // "textureHeightChanged"
QT_MOC_LITERAL(30, 431, 1), // "h"
QT_MOC_LITERAL(31, 433, 11), // "pixelMetric"
QT_MOC_LITERAL(32, 445, 9), // "styleHint"
QT_MOC_LITERAL(33, 455, 14), // "updateSizeHint"
QT_MOC_LITERAL(34, 470, 10), // "updateRect"
QT_MOC_LITERAL(35, 481, 20), // "updateBaselineOffset"
QT_MOC_LITERAL(36, 502, 10), // "updateItem"
QT_MOC_LITERAL(37, 513, 7), // "hitTest"
QT_MOC_LITERAL(38, 521, 1), // "x"
QT_MOC_LITERAL(39, 523, 1), // "y"
QT_MOC_LITERAL(40, 525, 14), // "subControlRect"
QT_MOC_LITERAL(41, 540, 16), // "subcontrolString"
QT_MOC_LITERAL(42, 557, 10), // "elidedText"
QT_MOC_LITERAL(43, 568, 4), // "text"
QT_MOC_LITERAL(44, 573, 9), // "elideMode"
QT_MOC_LITERAL(45, 583, 5), // "width"
QT_MOC_LITERAL(46, 589, 12), // "hasThemeIcon"
QT_MOC_LITERAL(47, 602, 9), // "textWidth"
QT_MOC_LITERAL(48, 612, 10), // "textHeight"
QT_MOC_LITERAL(49, 623, 6), // "border"
QT_MOC_LITERAL(50, 630, 14), // "QQuickPadding*"
QT_MOC_LITERAL(51, 645, 6), // "sunken"
QT_MOC_LITERAL(52, 652, 6), // "raised"
QT_MOC_LITERAL(53, 659, 6), // "active"
QT_MOC_LITERAL(54, 666, 8), // "selected"
QT_MOC_LITERAL(55, 675, 8), // "hasFocus"
QT_MOC_LITERAL(56, 684, 2), // "on"
QT_MOC_LITERAL(57, 687, 5), // "hover"
QT_MOC_LITERAL(58, 693, 10), // "horizontal"
QT_MOC_LITERAL(59, 704, 11), // "isTransient"
QT_MOC_LITERAL(60, 716, 11), // "elementType"
QT_MOC_LITERAL(61, 728, 13), // "activeControl"
QT_MOC_LITERAL(62, 742, 5), // "style"
QT_MOC_LITERAL(63, 748, 5), // "hints"
QT_MOC_LITERAL(64, 754, 10), // "properties"
QT_MOC_LITERAL(65, 765, 4), // "font"
QT_MOC_LITERAL(66, 770, 7), // "minimum"
QT_MOC_LITERAL(67, 778, 7), // "maximum"
QT_MOC_LITERAL(68, 786, 5), // "value"
QT_MOC_LITERAL(69, 792, 4), // "step"
QT_MOC_LITERAL(70, 797, 12), // "paintMargins"
QT_MOC_LITERAL(71, 810, 12), // "contentWidth"
QT_MOC_LITERAL(72, 823, 13), // "contentHeight"
QT_MOC_LITERAL(73, 837, 12), // "textureWidth"
QT_MOC_LITERAL(74, 850, 13) // "textureHeight"

    },
    "QQuickStyleItem\0elementTypeChanged\0\0"
    "textChanged\0sunkenChanged\0raisedChanged\0"
    "activeChanged\0selectedChanged\0"
    "hasFocusChanged\0onChanged\0hoverChanged\0"
    "horizontalChanged\0transientChanged\0"
    "minimumChanged\0maximumChanged\0stepChanged\0"
    "valueChanged\0activeControlChanged\0"
    "infoChanged\0styleChanged\0paintMarginsChanged\0"
    "hintChanged\0propertiesChanged\0fontChanged\0"
    "contentWidthChanged\0arg\0contentHeightChanged\0"
    "textureWidthChanged\0w\0textureHeightChanged\0"
    "h\0pixelMetric\0styleHint\0updateSizeHint\0"
    "updateRect\0updateBaselineOffset\0"
    "updateItem\0hitTest\0x\0y\0subControlRect\0"
    "subcontrolString\0elidedText\0text\0"
    "elideMode\0width\0hasThemeIcon\0textWidth\0"
    "textHeight\0border\0QQuickPadding*\0"
    "sunken\0raised\0active\0selected\0hasFocus\0"
    "on\0hover\0horizontal\0isTransient\0"
    "elementType\0activeControl\0style\0hints\0"
    "properties\0font\0minimum\0maximum\0value\0"
    "step\0paintMargins\0contentWidth\0"
    "contentHeight\0textureWidth\0textureHeight"
};
#undef QT_MOC_LITERAL

static const uint qt_meta_data_QQuickStyleItem[] = {

 // content:
       7,       // revision
       0,       // classname
       0,    0, // classinfo
      38,   14, // methods
      26,  272, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
      26,       // signalCount

 // signals: name, argc, parameters, tag, flags
       1,    0,  204,    2, 0x06 /* Public */,
       3,    0,  205,    2, 0x06 /* Public */,
       4,    0,  206,    2, 0x06 /* Public */,
       5,    0,  207,    2, 0x06 /* Public */,
       6,    0,  208,    2, 0x06 /* Public */,
       7,    0,  209,    2, 0x06 /* Public */,
       8,    0,  210,    2, 0x06 /* Public */,
       9,    0,  211,    2, 0x06 /* Public */,
      10,    0,  212,    2, 0x06 /* Public */,
      11,    0,  213,    2, 0x06 /* Public */,
      12,    0,  214,    2, 0x06 /* Public */,
      13,    0,  215,    2, 0x06 /* Public */,
      14,    0,  216,    2, 0x06 /* Public */,
      15,    0,  217,    2, 0x06 /* Public */,
      16,    0,  218,    2, 0x06 /* Public */,
      17,    0,  219,    2, 0x06 /* Public */,
      18,    0,  220,    2, 0x06 /* Public */,
      19,    0,  221,    2, 0x06 /* Public */,
      20,    0,  222,    2, 0x06 /* Public */,
      21,    0,  223,    2, 0x06 /* Public */,
      22,    0,  224,    2, 0x06 /* Public */,
      23,    0,  225,    2, 0x06 /* Public */,
      24,    1,  226,    2, 0x06 /* Public */,
      26,    1,  229,    2, 0x06 /* Public */,
      27,    1,  232,    2, 0x06 /* Public */,
      29,    1,  235,    2, 0x06 /* Public */,

 // slots: name, argc, parameters, tag, flags
      31,    1,  238,    2, 0x0a /* Public */,
      32,    1,  241,    2, 0x0a /* Public */,
      33,    0,  244,    2, 0x0a /* Public */,
      34,    0,  245,    2, 0x0a /* Public */,
      35,    0,  246,    2, 0x0a /* Public */,
      36,    0,  247,    2, 0x0a /* Public */,
      37,    2,  248,    2, 0x0a /* Public */,
      40,    1,  253,    2, 0x0a /* Public */,
      42,    3,  256,    2, 0x0a /* Public */,
      46,    1,  263,    2, 0x0a /* Public */,

 // methods: name, argc, parameters, tag, flags
      47,    1,  266,    2, 0x02 /* Public */,
      48,    1,  269,    2, 0x02 /* Public */,

 // signals: parameters
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void, QMetaType::Int,   25,
    QMetaType::Void, QMetaType::Int,   25,
    QMetaType::Void, QMetaType::Int,   28,
    QMetaType::Void, QMetaType::Int,   30,

 // slots: parameters
    QMetaType::Int, QMetaType::QString,    2,
    QMetaType::QVariant, QMetaType::QString,    2,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::QString, QMetaType::Int, QMetaType::Int,   38,   39,
    QMetaType::QRectF, QMetaType::QString,   41,
    QMetaType::QString, QMetaType::QString, QMetaType::Int, QMetaType::Int,   43,   44,   45,
    QMetaType::Bool, QMetaType::QString,    2,

 // methods: parameters
    QMetaType::QReal, QMetaType::QString,    2,
    QMetaType::QReal, QMetaType::QString,    2,

 // properties: name, type, flags
      49, 0x80000000 | 50, 0x00095409,
      51, QMetaType::Bool, 0x00495103,
      52, QMetaType::Bool, 0x00495103,
      53, QMetaType::Bool, 0x00495103,
      54, QMetaType::Bool, 0x00495103,
      55, QMetaType::Bool, 0x00495003,
      56, QMetaType::Bool, 0x00495103,
      57, QMetaType::Bool, 0x00495103,
      58, QMetaType::Bool, 0x00495103,
      59, QMetaType::Bool, 0x00495003,
      60, QMetaType::QString, 0x00495103,
      43, QMetaType::QString, 0x00495103,
      61, QMetaType::QString, 0x00495103,
      62, QMetaType::QString, 0x00495001,
      63, QMetaType::QVariantMap, 0x00495107,
      64, QMetaType::QVariantMap, 0x00495103,
      65, QMetaType::QFont, 0x00495001,
      66, QMetaType::Int, 0x00495103,
      67, QMetaType::Int, 0x00495103,
      68, QMetaType::Int, 0x00495103,
      69, QMetaType::Int, 0x00495103,
      70, QMetaType::Int, 0x00495103,
      71, QMetaType::Int, 0x00495103,
      72, QMetaType::Int, 0x00495103,
      73, QMetaType::Int, 0x00495103,
      74, QMetaType::Int, 0x00495103,

 // properties: notify_signal_id
       0,
       2,
       3,
       4,
       5,
       6,
       7,
       8,
       9,
      10,
       0,
       1,
      15,
      17,
      19,
      20,
      21,
      11,
      12,
      14,
      13,
      18,
      22,
      23,
      24,
      25,

       0        // eod
};

void QQuickStyleItem::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        QQuickStyleItem *_t = static_cast<QQuickStyleItem *>(_o);
        Q_UNUSED(_t)
        switch (_id) {
        case 0: _t->elementTypeChanged(); break;
        case 1: _t->textChanged(); break;
        case 2: _t->sunkenChanged(); break;
        case 3: _t->raisedChanged(); break;
        case 4: _t->activeChanged(); break;
        case 5: _t->selectedChanged(); break;
        case 6: _t->hasFocusChanged(); break;
        case 7: _t->onChanged(); break;
        case 8: _t->hoverChanged(); break;
        case 9: _t->horizontalChanged(); break;
        case 10: _t->transientChanged(); break;
        case 11: _t->minimumChanged(); break;
        case 12: _t->maximumChanged(); break;
        case 13: _t->stepChanged(); break;
        case 14: _t->valueChanged(); break;
        case 15: _t->activeControlChanged(); break;
        case 16: _t->infoChanged(); break;
        case 17: _t->styleChanged(); break;
        case 18: _t->paintMarginsChanged(); break;
        case 19: _t->hintChanged(); break;
        case 20: _t->propertiesChanged(); break;
        case 21: _t->fontChanged(); break;
        case 22: _t->contentWidthChanged((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 23: _t->contentHeightChanged((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 24: _t->textureWidthChanged((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 25: _t->textureHeightChanged((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 26: { int _r = _t->pixelMetric((*reinterpret_cast< const QString(*)>(_a[1])));
            if (_a[0]) *reinterpret_cast< int*>(_a[0]) = _r; }  break;
        case 27: { QVariant _r = _t->styleHint((*reinterpret_cast< const QString(*)>(_a[1])));
            if (_a[0]) *reinterpret_cast< QVariant*>(_a[0]) = _r; }  break;
        case 28: _t->updateSizeHint(); break;
        case 29: _t->updateRect(); break;
        case 30: _t->updateBaselineOffset(); break;
        case 31: _t->updateItem(); break;
        case 32: { QString _r = _t->hitTest((*reinterpret_cast< int(*)>(_a[1])),(*reinterpret_cast< int(*)>(_a[2])));
            if (_a[0]) *reinterpret_cast< QString*>(_a[0]) = _r; }  break;
        case 33: { QRectF _r = _t->subControlRect((*reinterpret_cast< const QString(*)>(_a[1])));
            if (_a[0]) *reinterpret_cast< QRectF*>(_a[0]) = _r; }  break;
        case 34: { QString _r = _t->elidedText((*reinterpret_cast< const QString(*)>(_a[1])),(*reinterpret_cast< int(*)>(_a[2])),(*reinterpret_cast< int(*)>(_a[3])));
            if (_a[0]) *reinterpret_cast< QString*>(_a[0]) = _r; }  break;
        case 35: { bool _r = _t->hasThemeIcon((*reinterpret_cast< const QString(*)>(_a[1])));
            if (_a[0]) *reinterpret_cast< bool*>(_a[0]) = _r; }  break;
        case 36: { qreal _r = _t->textWidth((*reinterpret_cast< const QString(*)>(_a[1])));
            if (_a[0]) *reinterpret_cast< qreal*>(_a[0]) = _r; }  break;
        case 37: { qreal _r = _t->textHeight((*reinterpret_cast< const QString(*)>(_a[1])));
            if (_a[0]) *reinterpret_cast< qreal*>(_a[0]) = _r; }  break;
        default: ;
        }
    } else if (_c == QMetaObject::IndexOfMethod) {
        int *result = reinterpret_cast<int *>(_a[0]);
        void **func = reinterpret_cast<void **>(_a[1]);
        {
            typedef void (QQuickStyleItem::*_t)();
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&QQuickStyleItem::elementTypeChanged)) {
                *result = 0;
                return;
            }
        }
        {
            typedef void (QQuickStyleItem::*_t)();
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&QQuickStyleItem::textChanged)) {
                *result = 1;
                return;
            }
        }
        {
            typedef void (QQuickStyleItem::*_t)();
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&QQuickStyleItem::sunkenChanged)) {
                *result = 2;
                return;
            }
        }
        {
            typedef void (QQuickStyleItem::*_t)();
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&QQuickStyleItem::raisedChanged)) {
                *result = 3;
                return;
            }
        }
        {
            typedef void (QQuickStyleItem::*_t)();
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&QQuickStyleItem::activeChanged)) {
                *result = 4;
                return;
            }
        }
        {
            typedef void (QQuickStyleItem::*_t)();
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&QQuickStyleItem::selectedChanged)) {
                *result = 5;
                return;
            }
        }
        {
            typedef void (QQuickStyleItem::*_t)();
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&QQuickStyleItem::hasFocusChanged)) {
                *result = 6;
                return;
            }
        }
        {
            typedef void (QQuickStyleItem::*_t)();
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&QQuickStyleItem::onChanged)) {
                *result = 7;
                return;
            }
        }
        {
            typedef void (QQuickStyleItem::*_t)();
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&QQuickStyleItem::hoverChanged)) {
                *result = 8;
                return;
            }
        }
        {
            typedef void (QQuickStyleItem::*_t)();
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&QQuickStyleItem::horizontalChanged)) {
                *result = 9;
                return;
            }
        }
        {
            typedef void (QQuickStyleItem::*_t)();
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&QQuickStyleItem::transientChanged)) {
                *result = 10;
                return;
            }
        }
        {
            typedef void (QQuickStyleItem::*_t)();
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&QQuickStyleItem::minimumChanged)) {
                *result = 11;
                return;
            }
        }
        {
            typedef void (QQuickStyleItem::*_t)();
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&QQuickStyleItem::maximumChanged)) {
                *result = 12;
                return;
            }
        }
        {
            typedef void (QQuickStyleItem::*_t)();
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&QQuickStyleItem::stepChanged)) {
                *result = 13;
                return;
            }
        }
        {
            typedef void (QQuickStyleItem::*_t)();
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&QQuickStyleItem::valueChanged)) {
                *result = 14;
                return;
            }
        }
        {
            typedef void (QQuickStyleItem::*_t)();
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&QQuickStyleItem::activeControlChanged)) {
                *result = 15;
                return;
            }
        }
        {
            typedef void (QQuickStyleItem::*_t)();
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&QQuickStyleItem::infoChanged)) {
                *result = 16;
                return;
            }
        }
        {
            typedef void (QQuickStyleItem::*_t)();
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&QQuickStyleItem::styleChanged)) {
                *result = 17;
                return;
            }
        }
        {
            typedef void (QQuickStyleItem::*_t)();
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&QQuickStyleItem::paintMarginsChanged)) {
                *result = 18;
                return;
            }
        }
        {
            typedef void (QQuickStyleItem::*_t)();
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&QQuickStyleItem::hintChanged)) {
                *result = 19;
                return;
            }
        }
        {
            typedef void (QQuickStyleItem::*_t)();
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&QQuickStyleItem::propertiesChanged)) {
                *result = 20;
                return;
            }
        }
        {
            typedef void (QQuickStyleItem::*_t)();
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&QQuickStyleItem::fontChanged)) {
                *result = 21;
                return;
            }
        }
        {
            typedef void (QQuickStyleItem::*_t)(int );
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&QQuickStyleItem::contentWidthChanged)) {
                *result = 22;
                return;
            }
        }
        {
            typedef void (QQuickStyleItem::*_t)(int );
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&QQuickStyleItem::contentHeightChanged)) {
                *result = 23;
                return;
            }
        }
        {
            typedef void (QQuickStyleItem::*_t)(int );
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&QQuickStyleItem::textureWidthChanged)) {
                *result = 24;
                return;
            }
        }
        {
            typedef void (QQuickStyleItem::*_t)(int );
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&QQuickStyleItem::textureHeightChanged)) {
                *result = 25;
                return;
            }
        }
    } else if (_c == QMetaObject::RegisterPropertyMetaType) {
        switch (_id) {
        default: *reinterpret_cast<int*>(_a[0]) = -1; break;
        case 0:
            *reinterpret_cast<int*>(_a[0]) = qRegisterMetaType< QQuickPadding* >(); break;
        }
    }

#ifndef QT_NO_PROPERTIES
    else if (_c == QMetaObject::ReadProperty) {
        QQuickStyleItem *_t = static_cast<QQuickStyleItem *>(_o);
        Q_UNUSED(_t)
        void *_v = _a[0];
        switch (_id) {
        case 0: *reinterpret_cast< QQuickPadding**>(_v) = _t->border(); break;
        case 1: *reinterpret_cast< bool*>(_v) = _t->sunken(); break;
        case 2: *reinterpret_cast< bool*>(_v) = _t->raised(); break;
        case 3: *reinterpret_cast< bool*>(_v) = _t->active(); break;
        case 4: *reinterpret_cast< bool*>(_v) = _t->selected(); break;
        case 5: *reinterpret_cast< bool*>(_v) = _t->hasFocus(); break;
        case 6: *reinterpret_cast< bool*>(_v) = _t->on(); break;
        case 7: *reinterpret_cast< bool*>(_v) = _t->hover(); break;
        case 8: *reinterpret_cast< bool*>(_v) = _t->horizontal(); break;
        case 9: *reinterpret_cast< bool*>(_v) = _t->isTransient(); break;
        case 10: *reinterpret_cast< QString*>(_v) = _t->elementType(); break;
        case 11: *reinterpret_cast< QString*>(_v) = _t->text(); break;
        case 12: *reinterpret_cast< QString*>(_v) = _t->activeControl(); break;
        case 13: *reinterpret_cast< QString*>(_v) = _t->style(); break;
        case 14: *reinterpret_cast< QVariantMap*>(_v) = _t->hints(); break;
        case 15: *reinterpret_cast< QVariantMap*>(_v) = _t->properties(); break;
        case 16: *reinterpret_cast< QFont*>(_v) = _t->font(); break;
        case 17: *reinterpret_cast< int*>(_v) = _t->minimum(); break;
        case 18: *reinterpret_cast< int*>(_v) = _t->maximum(); break;
        case 19: *reinterpret_cast< int*>(_v) = _t->value(); break;
        case 20: *reinterpret_cast< int*>(_v) = _t->step(); break;
        case 21: *reinterpret_cast< int*>(_v) = _t->paintMargins(); break;
        case 22: *reinterpret_cast< int*>(_v) = _t->contentWidth(); break;
        case 23: *reinterpret_cast< int*>(_v) = _t->contentHeight(); break;
        case 24: *reinterpret_cast< int*>(_v) = _t->textureWidth(); break;
        case 25: *reinterpret_cast< int*>(_v) = _t->textureHeight(); break;
        default: break;
        }
    } else if (_c == QMetaObject::WriteProperty) {
        QQuickStyleItem *_t = static_cast<QQuickStyleItem *>(_o);
        Q_UNUSED(_t)
        void *_v = _a[0];
        switch (_id) {
        case 1: _t->setSunken(*reinterpret_cast< bool*>(_v)); break;
        case 2: _t->setRaised(*reinterpret_cast< bool*>(_v)); break;
        case 3: _t->setActive(*reinterpret_cast< bool*>(_v)); break;
        case 4: _t->setSelected(*reinterpret_cast< bool*>(_v)); break;
        case 5: _t->sethasFocus(*reinterpret_cast< bool*>(_v)); break;
        case 6: _t->setOn(*reinterpret_cast< bool*>(_v)); break;
        case 7: _t->setHover(*reinterpret_cast< bool*>(_v)); break;
        case 8: _t->setHorizontal(*reinterpret_cast< bool*>(_v)); break;
        case 9: _t->setTransient(*reinterpret_cast< bool*>(_v)); break;
        case 10: _t->setElementType(*reinterpret_cast< QString*>(_v)); break;
        case 11: _t->setText(*reinterpret_cast< QString*>(_v)); break;
        case 12: _t->setActiveControl(*reinterpret_cast< QString*>(_v)); break;
        case 14: _t->setHints(*reinterpret_cast< QVariantMap*>(_v)); break;
        case 15: _t->setProperties(*reinterpret_cast< QVariantMap*>(_v)); break;
        case 17: _t->setMinimum(*reinterpret_cast< int*>(_v)); break;
        case 18: _t->setMaximum(*reinterpret_cast< int*>(_v)); break;
        case 19: _t->setValue(*reinterpret_cast< int*>(_v)); break;
        case 20: _t->setStep(*reinterpret_cast< int*>(_v)); break;
        case 21: _t->setPaintMargins(*reinterpret_cast< int*>(_v)); break;
        case 22: _t->setContentWidth(*reinterpret_cast< int*>(_v)); break;
        case 23: _t->setContentHeight(*reinterpret_cast< int*>(_v)); break;
        case 24: _t->setTextureWidth(*reinterpret_cast< int*>(_v)); break;
        case 25: _t->setTextureHeight(*reinterpret_cast< int*>(_v)); break;
        default: break;
        }
    } else if (_c == QMetaObject::ResetProperty) {
        QQuickStyleItem *_t = static_cast<QQuickStyleItem *>(_o);
        Q_UNUSED(_t)
        switch (_id) {
        case 14: _t->resetHints(); break;
        default: break;
        }
    }
#endif // QT_NO_PROPERTIES
}

const QMetaObject QQuickStyleItem::staticMetaObject = {
    { &QQuickItem::staticMetaObject, qt_meta_stringdata_QQuickStyleItem.data,
      qt_meta_data_QQuickStyleItem,  qt_static_metacall, Q_NULLPTR, Q_NULLPTR}
};


const QMetaObject *QQuickStyleItem::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->dynamicMetaObject() : &staticMetaObject;
}

void *QQuickStyleItem::qt_metacast(const char *_clname)
{
    if (!_clname) return Q_NULLPTR;
    if (!strcmp(_clname, qt_meta_stringdata_QQuickStyleItem.stringdata0))
        return static_cast<void*>(const_cast< QQuickStyleItem*>(this));
    return QQuickItem::qt_metacast(_clname);
}

int QQuickStyleItem::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QQuickItem::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 38)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 38;
    } else if (_c == QMetaObject::RegisterMethodArgumentMetaType) {
        if (_id < 38)
            *reinterpret_cast<int*>(_a[0]) = -1;
        _id -= 38;
    }
#ifndef QT_NO_PROPERTIES
   else if (_c == QMetaObject::ReadProperty || _c == QMetaObject::WriteProperty
            || _c == QMetaObject::ResetProperty || _c == QMetaObject::RegisterPropertyMetaType) {
        qt_static_metacall(this, _c, _id, _a);
        _id -= 26;
    } else if (_c == QMetaObject::QueryPropertyDesignable) {
        _id -= 26;
    } else if (_c == QMetaObject::QueryPropertyScriptable) {
        _id -= 26;
    } else if (_c == QMetaObject::QueryPropertyStored) {
        _id -= 26;
    } else if (_c == QMetaObject::QueryPropertyEditable) {
        _id -= 26;
    } else if (_c == QMetaObject::QueryPropertyUser) {
        _id -= 26;
    }
#endif // QT_NO_PROPERTIES
    return _id;
}

// SIGNAL 0
void QQuickStyleItem::elementTypeChanged()
{
    QMetaObject::activate(this, &staticMetaObject, 0, Q_NULLPTR);
}

// SIGNAL 1
void QQuickStyleItem::textChanged()
{
    QMetaObject::activate(this, &staticMetaObject, 1, Q_NULLPTR);
}

// SIGNAL 2
void QQuickStyleItem::sunkenChanged()
{
    QMetaObject::activate(this, &staticMetaObject, 2, Q_NULLPTR);
}

// SIGNAL 3
void QQuickStyleItem::raisedChanged()
{
    QMetaObject::activate(this, &staticMetaObject, 3, Q_NULLPTR);
}

// SIGNAL 4
void QQuickStyleItem::activeChanged()
{
    QMetaObject::activate(this, &staticMetaObject, 4, Q_NULLPTR);
}

// SIGNAL 5
void QQuickStyleItem::selectedChanged()
{
    QMetaObject::activate(this, &staticMetaObject, 5, Q_NULLPTR);
}

// SIGNAL 6
void QQuickStyleItem::hasFocusChanged()
{
    QMetaObject::activate(this, &staticMetaObject, 6, Q_NULLPTR);
}

// SIGNAL 7
void QQuickStyleItem::onChanged()
{
    QMetaObject::activate(this, &staticMetaObject, 7, Q_NULLPTR);
}

// SIGNAL 8
void QQuickStyleItem::hoverChanged()
{
    QMetaObject::activate(this, &staticMetaObject, 8, Q_NULLPTR);
}

// SIGNAL 9
void QQuickStyleItem::horizontalChanged()
{
    QMetaObject::activate(this, &staticMetaObject, 9, Q_NULLPTR);
}

// SIGNAL 10
void QQuickStyleItem::transientChanged()
{
    QMetaObject::activate(this, &staticMetaObject, 10, Q_NULLPTR);
}

// SIGNAL 11
void QQuickStyleItem::minimumChanged()
{
    QMetaObject::activate(this, &staticMetaObject, 11, Q_NULLPTR);
}

// SIGNAL 12
void QQuickStyleItem::maximumChanged()
{
    QMetaObject::activate(this, &staticMetaObject, 12, Q_NULLPTR);
}

// SIGNAL 13
void QQuickStyleItem::stepChanged()
{
    QMetaObject::activate(this, &staticMetaObject, 13, Q_NULLPTR);
}

// SIGNAL 14
void QQuickStyleItem::valueChanged()
{
    QMetaObject::activate(this, &staticMetaObject, 14, Q_NULLPTR);
}

// SIGNAL 15
void QQuickStyleItem::activeControlChanged()
{
    QMetaObject::activate(this, &staticMetaObject, 15, Q_NULLPTR);
}

// SIGNAL 16
void QQuickStyleItem::infoChanged()
{
    QMetaObject::activate(this, &staticMetaObject, 16, Q_NULLPTR);
}

// SIGNAL 17
void QQuickStyleItem::styleChanged()
{
    QMetaObject::activate(this, &staticMetaObject, 17, Q_NULLPTR);
}

// SIGNAL 18
void QQuickStyleItem::paintMarginsChanged()
{
    QMetaObject::activate(this, &staticMetaObject, 18, Q_NULLPTR);
}

// SIGNAL 19
void QQuickStyleItem::hintChanged()
{
    QMetaObject::activate(this, &staticMetaObject, 19, Q_NULLPTR);
}

// SIGNAL 20
void QQuickStyleItem::propertiesChanged()
{
    QMetaObject::activate(this, &staticMetaObject, 20, Q_NULLPTR);
}

// SIGNAL 21
void QQuickStyleItem::fontChanged()
{
    QMetaObject::activate(this, &staticMetaObject, 21, Q_NULLPTR);
}

// SIGNAL 22
void QQuickStyleItem::contentWidthChanged(int _t1)
{
    void *_a[] = { Q_NULLPTR, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 22, _a);
}

// SIGNAL 23
void QQuickStyleItem::contentHeightChanged(int _t1)
{
    void *_a[] = { Q_NULLPTR, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 23, _a);
}

// SIGNAL 24
void QQuickStyleItem::textureWidthChanged(int _t1)
{
    void *_a[] = { Q_NULLPTR, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 24, _a);
}

// SIGNAL 25
void QQuickStyleItem::textureHeightChanged(int _t1)
{
    void *_a[] = { Q_NULLPTR, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 25, _a);
}
QT_END_MOC_NAMESPACE
