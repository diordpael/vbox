/****************************************************************************
** Meta object code from reading C++ file 'qquickabstractfiledialog_p.h'
**
** Created by: The Qt Meta Object Compiler version 67 (Qt 5.6.3)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "../../qquickabstractfiledialog_p.h"
#include <QtCore/qbytearray.h>
#include <QtCore/qmetatype.h>
#include <QtCore/QList>
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'qquickabstractfiledialog_p.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 67
#error "This file was generated using the moc from 5.6.3. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
struct qt_meta_stringdata_QQuickAbstractFileDialog_t {
    QByteArrayData data[40];
    char stringdata0[540];
};
#define QT_MOC_LITERAL(idx, ofs, len) \
    Q_STATIC_BYTE_ARRAY_DATA_HEADER_INITIALIZER_WITH_OFFSET(len, \
    qptrdiff(offsetof(qt_meta_stringdata_QQuickAbstractFileDialog_t, stringdata0) + ofs \
        - idx * sizeof(QByteArrayData)) \
    )
static const qt_meta_stringdata_QQuickAbstractFileDialog_t qt_meta_stringdata_QQuickAbstractFileDialog = {
    {
QT_MOC_LITERAL(0, 0, 24), // "QQuickAbstractFileDialog"
QT_MOC_LITERAL(1, 25, 13), // "folderChanged"
QT_MOC_LITERAL(2, 39, 0), // ""
QT_MOC_LITERAL(3, 40, 18), // "nameFiltersChanged"
QT_MOC_LITERAL(4, 59, 14), // "filterSelected"
QT_MOC_LITERAL(5, 74, 15), // "fileModeChanged"
QT_MOC_LITERAL(6, 90, 17), // "selectionAccepted"
QT_MOC_LITERAL(7, 108, 21), // "sidebarVisibleChanged"
QT_MOC_LITERAL(8, 130, 16), // "shortcutsChanged"
QT_MOC_LITERAL(9, 147, 10), // "setVisible"
QT_MOC_LITERAL(10, 158, 1), // "v"
QT_MOC_LITERAL(11, 160, 8), // "setTitle"
QT_MOC_LITERAL(12, 169, 1), // "t"
QT_MOC_LITERAL(13, 171, 17), // "setSelectExisting"
QT_MOC_LITERAL(14, 189, 1), // "s"
QT_MOC_LITERAL(15, 191, 17), // "setSelectMultiple"
QT_MOC_LITERAL(16, 209, 15), // "setSelectFolder"
QT_MOC_LITERAL(17, 225, 9), // "setFolder"
QT_MOC_LITERAL(18, 235, 1), // "f"
QT_MOC_LITERAL(19, 237, 14), // "setNameFilters"
QT_MOC_LITERAL(20, 252, 16), // "selectNameFilter"
QT_MOC_LITERAL(21, 269, 26), // "setSelectedNameFilterIndex"
QT_MOC_LITERAL(22, 296, 3), // "idx"
QT_MOC_LITERAL(23, 300, 17), // "setSidebarVisible"
QT_MOC_LITERAL(24, 318, 12), // "updateFolder"
QT_MOC_LITERAL(25, 331, 14), // "selectExisting"
QT_MOC_LITERAL(26, 346, 14), // "selectMultiple"
QT_MOC_LITERAL(27, 361, 12), // "selectFolder"
QT_MOC_LITERAL(28, 374, 6), // "folder"
QT_MOC_LITERAL(29, 381, 11), // "nameFilters"
QT_MOC_LITERAL(30, 393, 18), // "selectedNameFilter"
QT_MOC_LITERAL(31, 412, 28), // "selectedNameFilterExtensions"
QT_MOC_LITERAL(32, 441, 23), // "selectedNameFilterIndex"
QT_MOC_LITERAL(33, 465, 7), // "fileUrl"
QT_MOC_LITERAL(34, 473, 8), // "fileUrls"
QT_MOC_LITERAL(35, 482, 11), // "QList<QUrl>"
QT_MOC_LITERAL(36, 494, 14), // "sidebarVisible"
QT_MOC_LITERAL(37, 509, 9), // "shortcuts"
QT_MOC_LITERAL(38, 519, 8), // "QJSValue"
QT_MOC_LITERAL(39, 528, 11) // "__shortcuts"

    },
    "QQuickAbstractFileDialog\0folderChanged\0"
    "\0nameFiltersChanged\0filterSelected\0"
    "fileModeChanged\0selectionAccepted\0"
    "sidebarVisibleChanged\0shortcutsChanged\0"
    "setVisible\0v\0setTitle\0t\0setSelectExisting\0"
    "s\0setSelectMultiple\0setSelectFolder\0"
    "setFolder\0f\0setNameFilters\0selectNameFilter\0"
    "setSelectedNameFilterIndex\0idx\0"
    "setSidebarVisible\0updateFolder\0"
    "selectExisting\0selectMultiple\0"
    "selectFolder\0folder\0nameFilters\0"
    "selectedNameFilter\0selectedNameFilterExtensions\0"
    "selectedNameFilterIndex\0fileUrl\0"
    "fileUrls\0QList<QUrl>\0sidebarVisible\0"
    "shortcuts\0QJSValue\0__shortcuts"
};
#undef QT_MOC_LITERAL

static const uint qt_meta_data_QQuickAbstractFileDialog[] = {

 // content:
       7,       // revision
       0,       // classname
       0,    0, // classinfo
      18,   14, // methods
      13,  144, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       7,       // signalCount

 // signals: name, argc, parameters, tag, flags
       1,    0,  104,    2, 0x06 /* Public */,
       3,    0,  105,    2, 0x06 /* Public */,
       4,    0,  106,    2, 0x06 /* Public */,
       5,    0,  107,    2, 0x06 /* Public */,
       6,    0,  108,    2, 0x06 /* Public */,
       7,    0,  109,    2, 0x06 /* Public */,
       8,    0,  110,    2, 0x06 /* Public */,

 // slots: name, argc, parameters, tag, flags
       9,    1,  111,    2, 0x0a /* Public */,
      11,    1,  114,    2, 0x0a /* Public */,
      13,    1,  117,    2, 0x0a /* Public */,
      15,    1,  120,    2, 0x0a /* Public */,
      16,    1,  123,    2, 0x0a /* Public */,
      17,    1,  126,    2, 0x0a /* Public */,
      19,    1,  129,    2, 0x0a /* Public */,
      20,    1,  132,    2, 0x0a /* Public */,
      21,    1,  135,    2, 0x0a /* Public */,
      23,    1,  138,    2, 0x0a /* Public */,
      24,    1,  141,    2, 0x09 /* Protected */,

 // signals: parameters
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,

 // slots: parameters
    QMetaType::Void, QMetaType::Bool,   10,
    QMetaType::Void, QMetaType::QString,   12,
    QMetaType::Void, QMetaType::Bool,   14,
    QMetaType::Void, QMetaType::Bool,   14,
    QMetaType::Void, QMetaType::Bool,   14,
    QMetaType::Void, QMetaType::QUrl,   18,
    QMetaType::Void, QMetaType::QStringList,   18,
    QMetaType::Void, QMetaType::QString,   18,
    QMetaType::Void, QMetaType::Int,   22,
    QMetaType::Void, QMetaType::Bool,   14,
    QMetaType::Void, QMetaType::QUrl,   18,

 // properties: name, type, flags
      25, QMetaType::Bool, 0x00495103,
      26, QMetaType::Bool, 0x00495103,
      27, QMetaType::Bool, 0x00495103,
      28, QMetaType::QUrl, 0x00495103,
      29, QMetaType::QStringList, 0x00495103,
      30, QMetaType::QString, 0x00495003,
      31, QMetaType::QStringList, 0x00495001,
      32, QMetaType::Int, 0x00495103,
      33, QMetaType::QUrl, 0x00495001,
      34, 0x80000000 | 35, 0x00495009,
      36, QMetaType::Bool, 0x00495103,
      37, 0x80000000 | 38, 0x00495009,
      39, 0x80000000 | 38, 0x00495009,

 // properties: notify_signal_id
       3,
       3,
       3,
       0,
       1,
       2,
       2,
       2,
       4,
       4,
       5,
       6,
       6,

       0        // eod
};

void QQuickAbstractFileDialog::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        QQuickAbstractFileDialog *_t = static_cast<QQuickAbstractFileDialog *>(_o);
        Q_UNUSED(_t)
        switch (_id) {
        case 0: _t->folderChanged(); break;
        case 1: _t->nameFiltersChanged(); break;
        case 2: _t->filterSelected(); break;
        case 3: _t->fileModeChanged(); break;
        case 4: _t->selectionAccepted(); break;
        case 5: _t->sidebarVisibleChanged(); break;
        case 6: _t->shortcutsChanged(); break;
        case 7: _t->setVisible((*reinterpret_cast< bool(*)>(_a[1]))); break;
        case 8: _t->setTitle((*reinterpret_cast< const QString(*)>(_a[1]))); break;
        case 9: _t->setSelectExisting((*reinterpret_cast< bool(*)>(_a[1]))); break;
        case 10: _t->setSelectMultiple((*reinterpret_cast< bool(*)>(_a[1]))); break;
        case 11: _t->setSelectFolder((*reinterpret_cast< bool(*)>(_a[1]))); break;
        case 12: _t->setFolder((*reinterpret_cast< const QUrl(*)>(_a[1]))); break;
        case 13: _t->setNameFilters((*reinterpret_cast< const QStringList(*)>(_a[1]))); break;
        case 14: _t->selectNameFilter((*reinterpret_cast< const QString(*)>(_a[1]))); break;
        case 15: _t->setSelectedNameFilterIndex((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 16: _t->setSidebarVisible((*reinterpret_cast< bool(*)>(_a[1]))); break;
        case 17: _t->updateFolder((*reinterpret_cast< const QUrl(*)>(_a[1]))); break;
        default: ;
        }
    } else if (_c == QMetaObject::IndexOfMethod) {
        int *result = reinterpret_cast<int *>(_a[0]);
        void **func = reinterpret_cast<void **>(_a[1]);
        {
            typedef void (QQuickAbstractFileDialog::*_t)();
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&QQuickAbstractFileDialog::folderChanged)) {
                *result = 0;
                return;
            }
        }
        {
            typedef void (QQuickAbstractFileDialog::*_t)();
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&QQuickAbstractFileDialog::nameFiltersChanged)) {
                *result = 1;
                return;
            }
        }
        {
            typedef void (QQuickAbstractFileDialog::*_t)();
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&QQuickAbstractFileDialog::filterSelected)) {
                *result = 2;
                return;
            }
        }
        {
            typedef void (QQuickAbstractFileDialog::*_t)();
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&QQuickAbstractFileDialog::fileModeChanged)) {
                *result = 3;
                return;
            }
        }
        {
            typedef void (QQuickAbstractFileDialog::*_t)();
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&QQuickAbstractFileDialog::selectionAccepted)) {
                *result = 4;
                return;
            }
        }
        {
            typedef void (QQuickAbstractFileDialog::*_t)();
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&QQuickAbstractFileDialog::sidebarVisibleChanged)) {
                *result = 5;
                return;
            }
        }
        {
            typedef void (QQuickAbstractFileDialog::*_t)();
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&QQuickAbstractFileDialog::shortcutsChanged)) {
                *result = 6;
                return;
            }
        }
    } else if (_c == QMetaObject::RegisterPropertyMetaType) {
        switch (_id) {
        default: *reinterpret_cast<int*>(_a[0]) = -1; break;
        case 12:
        case 11:
            *reinterpret_cast<int*>(_a[0]) = qRegisterMetaType< QJSValue >(); break;
        case 9:
            *reinterpret_cast<int*>(_a[0]) = qRegisterMetaType< QList<QUrl> >(); break;
        }
    }

#ifndef QT_NO_PROPERTIES
    else if (_c == QMetaObject::ReadProperty) {
        QQuickAbstractFileDialog *_t = static_cast<QQuickAbstractFileDialog *>(_o);
        Q_UNUSED(_t)
        void *_v = _a[0];
        switch (_id) {
        case 0: *reinterpret_cast< bool*>(_v) = _t->selectExisting(); break;
        case 1: *reinterpret_cast< bool*>(_v) = _t->selectMultiple(); break;
        case 2: *reinterpret_cast< bool*>(_v) = _t->selectFolder(); break;
        case 3: *reinterpret_cast< QUrl*>(_v) = _t->folder(); break;
        case 4: *reinterpret_cast< QStringList*>(_v) = _t->nameFilters(); break;
        case 5: *reinterpret_cast< QString*>(_v) = _t->selectedNameFilter(); break;
        case 6: *reinterpret_cast< QStringList*>(_v) = _t->selectedNameFilterExtensions(); break;
        case 7: *reinterpret_cast< int*>(_v) = _t->selectedNameFilterIndex(); break;
        case 8: *reinterpret_cast< QUrl*>(_v) = _t->fileUrl(); break;
        case 9: *reinterpret_cast< QList<QUrl>*>(_v) = _t->fileUrls(); break;
        case 10: *reinterpret_cast< bool*>(_v) = _t->sidebarVisible(); break;
        case 11: *reinterpret_cast< QJSValue*>(_v) = _t->shortcuts(); break;
        case 12: *reinterpret_cast< QJSValue*>(_v) = _t->__shortcuts(); break;
        default: break;
        }
    } else if (_c == QMetaObject::WriteProperty) {
        QQuickAbstractFileDialog *_t = static_cast<QQuickAbstractFileDialog *>(_o);
        Q_UNUSED(_t)
        void *_v = _a[0];
        switch (_id) {
        case 0: _t->setSelectExisting(*reinterpret_cast< bool*>(_v)); break;
        case 1: _t->setSelectMultiple(*reinterpret_cast< bool*>(_v)); break;
        case 2: _t->setSelectFolder(*reinterpret_cast< bool*>(_v)); break;
        case 3: _t->setFolder(*reinterpret_cast< QUrl*>(_v)); break;
        case 4: _t->setNameFilters(*reinterpret_cast< QStringList*>(_v)); break;
        case 5: _t->selectNameFilter(*reinterpret_cast< QString*>(_v)); break;
        case 7: _t->setSelectedNameFilterIndex(*reinterpret_cast< int*>(_v)); break;
        case 10: _t->setSidebarVisible(*reinterpret_cast< bool*>(_v)); break;
        default: break;
        }
    } else if (_c == QMetaObject::ResetProperty) {
    }
#endif // QT_NO_PROPERTIES
}

const QMetaObject QQuickAbstractFileDialog::staticMetaObject = {
    { &QQuickAbstractDialog::staticMetaObject, qt_meta_stringdata_QQuickAbstractFileDialog.data,
      qt_meta_data_QQuickAbstractFileDialog,  qt_static_metacall, Q_NULLPTR, Q_NULLPTR}
};


const QMetaObject *QQuickAbstractFileDialog::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->dynamicMetaObject() : &staticMetaObject;
}

void *QQuickAbstractFileDialog::qt_metacast(const char *_clname)
{
    if (!_clname) return Q_NULLPTR;
    if (!strcmp(_clname, qt_meta_stringdata_QQuickAbstractFileDialog.stringdata0))
        return static_cast<void*>(const_cast< QQuickAbstractFileDialog*>(this));
    return QQuickAbstractDialog::qt_metacast(_clname);
}

int QQuickAbstractFileDialog::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QQuickAbstractDialog::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 18)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 18;
    } else if (_c == QMetaObject::RegisterMethodArgumentMetaType) {
        if (_id < 18)
            *reinterpret_cast<int*>(_a[0]) = -1;
        _id -= 18;
    }
#ifndef QT_NO_PROPERTIES
   else if (_c == QMetaObject::ReadProperty || _c == QMetaObject::WriteProperty
            || _c == QMetaObject::ResetProperty || _c == QMetaObject::RegisterPropertyMetaType) {
        qt_static_metacall(this, _c, _id, _a);
        _id -= 13;
    } else if (_c == QMetaObject::QueryPropertyDesignable) {
        _id -= 13;
    } else if (_c == QMetaObject::QueryPropertyScriptable) {
        _id -= 13;
    } else if (_c == QMetaObject::QueryPropertyStored) {
        _id -= 13;
    } else if (_c == QMetaObject::QueryPropertyEditable) {
        _id -= 13;
    } else if (_c == QMetaObject::QueryPropertyUser) {
        _id -= 13;
    }
#endif // QT_NO_PROPERTIES
    return _id;
}

// SIGNAL 0
void QQuickAbstractFileDialog::folderChanged()
{
    QMetaObject::activate(this, &staticMetaObject, 0, Q_NULLPTR);
}

// SIGNAL 1
void QQuickAbstractFileDialog::nameFiltersChanged()
{
    QMetaObject::activate(this, &staticMetaObject, 1, Q_NULLPTR);
}

// SIGNAL 2
void QQuickAbstractFileDialog::filterSelected()
{
    QMetaObject::activate(this, &staticMetaObject, 2, Q_NULLPTR);
}

// SIGNAL 3
void QQuickAbstractFileDialog::fileModeChanged()
{
    QMetaObject::activate(this, &staticMetaObject, 3, Q_NULLPTR);
}

// SIGNAL 4
void QQuickAbstractFileDialog::selectionAccepted()
{
    QMetaObject::activate(this, &staticMetaObject, 4, Q_NULLPTR);
}

// SIGNAL 5
void QQuickAbstractFileDialog::sidebarVisibleChanged()
{
    QMetaObject::activate(this, &staticMetaObject, 5, Q_NULLPTR);
}

// SIGNAL 6
void QQuickAbstractFileDialog::shortcutsChanged()
{
    QMetaObject::activate(this, &staticMetaObject, 6, Q_NULLPTR);
}
QT_END_MOC_NAMESPACE
