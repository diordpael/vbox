/****************************************************************************
** Meta object code from reading C++ file 'qquickdialog_p.h'
**
** Created by: The Qt Meta Object Compiler version 67 (Qt 5.6.3)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "../../qquickdialog_p.h"
#include <QtCore/qbytearray.h>
#include <QtCore/qmetatype.h>
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'qquickdialog_p.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 67
#error "This file was generated using the moc from 5.6.3. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
struct qt_meta_stringdata_QQuickDialog_t {
    QByteArrayData data[31];
    char stringdata0[394];
};
#define QT_MOC_LITERAL(idx, ofs, len) \
    Q_STATIC_BYTE_ARRAY_DATA_HEADER_INITIALIZER_WITH_OFFSET(len, \
    qptrdiff(offsetof(qt_meta_stringdata_QQuickDialog_t, stringdata0) + ofs \
        - idx * sizeof(QByteArrayData)) \
    )
static const qt_meta_stringdata_QQuickDialog_t qt_meta_stringdata_QQuickDialog = {
    {
QT_MOC_LITERAL(0, 0, 12), // "QQuickDialog"
QT_MOC_LITERAL(1, 13, 15), // "DefaultProperty"
QT_MOC_LITERAL(2, 29, 11), // "contentItem"
QT_MOC_LITERAL(3, 41, 12), // "titleChanged"
QT_MOC_LITERAL(4, 54, 0), // ""
QT_MOC_LITERAL(5, 55, 22), // "standardButtonsChanged"
QT_MOC_LITERAL(6, 78, 13), // "buttonClicked"
QT_MOC_LITERAL(7, 92, 7), // "discard"
QT_MOC_LITERAL(8, 100, 4), // "help"
QT_MOC_LITERAL(9, 105, 3), // "yes"
QT_MOC_LITERAL(10, 109, 2), // "no"
QT_MOC_LITERAL(11, 112, 5), // "apply"
QT_MOC_LITERAL(12, 118, 5), // "reset"
QT_MOC_LITERAL(13, 124, 8), // "setTitle"
QT_MOC_LITERAL(14, 133, 3), // "arg"
QT_MOC_LITERAL(15, 137, 18), // "setStandardButtons"
QT_MOC_LITERAL(16, 156, 15), // "StandardButtons"
QT_MOC_LITERAL(17, 172, 7), // "buttons"
QT_MOC_LITERAL(18, 180, 5), // "click"
QT_MOC_LITERAL(19, 186, 36), // "QQuickAbstractDialog::Standar..."
QT_MOC_LITERAL(20, 223, 6), // "button"
QT_MOC_LITERAL(21, 230, 6), // "accept"
QT_MOC_LITERAL(22, 237, 6), // "reject"
QT_MOC_LITERAL(23, 244, 26), // "__standardButtonsLeftModel"
QT_MOC_LITERAL(24, 271, 8), // "QJSValue"
QT_MOC_LITERAL(25, 280, 27), // "__standardButtonsRightModel"
QT_MOC_LITERAL(26, 308, 5), // "title"
QT_MOC_LITERAL(27, 314, 15), // "standardButtons"
QT_MOC_LITERAL(28, 330, 37), // "QQuickAbstractDialog::Standar..."
QT_MOC_LITERAL(29, 368, 13), // "clickedButton"
QT_MOC_LITERAL(30, 382, 11) // "QQuickItem*"

    },
    "QQuickDialog\0DefaultProperty\0contentItem\0"
    "titleChanged\0\0standardButtonsChanged\0"
    "buttonClicked\0discard\0help\0yes\0no\0"
    "apply\0reset\0setTitle\0arg\0setStandardButtons\0"
    "StandardButtons\0buttons\0click\0"
    "QQuickAbstractDialog::StandardButton\0"
    "button\0accept\0reject\0__standardButtonsLeftModel\0"
    "QJSValue\0__standardButtonsRightModel\0"
    "title\0standardButtons\0"
    "QQuickAbstractDialog::StandardButtons\0"
    "clickedButton\0QQuickItem*"
};
#undef QT_MOC_LITERAL

static const uint qt_meta_data_QQuickDialog[] = {

 // content:
       7,       // revision
       0,       // classname
       1,   14, // classinfo
      16,   16, // methods
       4,  118, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       9,       // signalCount

 // classinfo: key, value
       1,    2,

 // signals: name, argc, parameters, tag, flags
       3,    0,   96,    4, 0x06 /* Public */,
       5,    0,   97,    4, 0x06 /* Public */,
       6,    0,   98,    4, 0x06 /* Public */,
       7,    0,   99,    4, 0x06 /* Public */,
       8,    0,  100,    4, 0x06 /* Public */,
       9,    0,  101,    4, 0x06 /* Public */,
      10,    0,  102,    4, 0x06 /* Public */,
      11,    0,  103,    4, 0x06 /* Public */,
      12,    0,  104,    4, 0x06 /* Public */,

 // slots: name, argc, parameters, tag, flags
      13,    1,  105,    4, 0x0a /* Public */,
      15,    1,  108,    4, 0x0a /* Public */,
      18,    1,  111,    4, 0x0a /* Public */,
      21,    0,  114,    4, 0x09 /* Protected */,
      22,    0,  115,    4, 0x09 /* Protected */,

 // methods: name, argc, parameters, tag, flags
      23,    0,  116,    4, 0x02 /* Public */,
      25,    0,  117,    4, 0x02 /* Public */,

 // signals: parameters
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,

 // slots: parameters
    QMetaType::Void, QMetaType::QString,   14,
    QMetaType::Void, 0x80000000 | 16,   17,
    QMetaType::Void, 0x80000000 | 19,   20,
    QMetaType::Void,
    QMetaType::Void,

 // methods: parameters
    0x80000000 | 24,
    0x80000000 | 24,

 // properties: name, type, flags
      26, QMetaType::QString, 0x00495103,
      27, 0x80000000 | 28, 0x0049510b,
      29, 0x80000000 | 19, 0x00495009,
       2, 0x80000000 | 30, 0x0009410b,

 // properties: notify_signal_id
       0,
       1,
       2,
       0,

 // enums: name, flags, count, data

 // enum data: key, value

       0        // eod
};

void QQuickDialog::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        QQuickDialog *_t = static_cast<QQuickDialog *>(_o);
        Q_UNUSED(_t)
        switch (_id) {
        case 0: _t->titleChanged(); break;
        case 1: _t->standardButtonsChanged(); break;
        case 2: _t->buttonClicked(); break;
        case 3: _t->discard(); break;
        case 4: _t->help(); break;
        case 5: _t->yes(); break;
        case 6: _t->no(); break;
        case 7: _t->apply(); break;
        case 8: _t->reset(); break;
        case 9: _t->setTitle((*reinterpret_cast< const QString(*)>(_a[1]))); break;
        case 10: _t->setStandardButtons((*reinterpret_cast< StandardButtons(*)>(_a[1]))); break;
        case 11: _t->click((*reinterpret_cast< QQuickAbstractDialog::StandardButton(*)>(_a[1]))); break;
        case 12: _t->accept(); break;
        case 13: _t->reject(); break;
        case 14: { QJSValue _r = _t->__standardButtonsLeftModel();
            if (_a[0]) *reinterpret_cast< QJSValue*>(_a[0]) = _r; }  break;
        case 15: { QJSValue _r = _t->__standardButtonsRightModel();
            if (_a[0]) *reinterpret_cast< QJSValue*>(_a[0]) = _r; }  break;
        default: ;
        }
    } else if (_c == QMetaObject::IndexOfMethod) {
        int *result = reinterpret_cast<int *>(_a[0]);
        void **func = reinterpret_cast<void **>(_a[1]);
        {
            typedef void (QQuickDialog::*_t)();
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&QQuickDialog::titleChanged)) {
                *result = 0;
                return;
            }
        }
        {
            typedef void (QQuickDialog::*_t)();
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&QQuickDialog::standardButtonsChanged)) {
                *result = 1;
                return;
            }
        }
        {
            typedef void (QQuickDialog::*_t)();
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&QQuickDialog::buttonClicked)) {
                *result = 2;
                return;
            }
        }
        {
            typedef void (QQuickDialog::*_t)();
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&QQuickDialog::discard)) {
                *result = 3;
                return;
            }
        }
        {
            typedef void (QQuickDialog::*_t)();
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&QQuickDialog::help)) {
                *result = 4;
                return;
            }
        }
        {
            typedef void (QQuickDialog::*_t)();
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&QQuickDialog::yes)) {
                *result = 5;
                return;
            }
        }
        {
            typedef void (QQuickDialog::*_t)();
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&QQuickDialog::no)) {
                *result = 6;
                return;
            }
        }
        {
            typedef void (QQuickDialog::*_t)();
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&QQuickDialog::apply)) {
                *result = 7;
                return;
            }
        }
        {
            typedef void (QQuickDialog::*_t)();
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&QQuickDialog::reset)) {
                *result = 8;
                return;
            }
        }
    }
#ifndef QT_NO_PROPERTIES
    else if (_c == QMetaObject::ReadProperty) {
        QQuickDialog *_t = static_cast<QQuickDialog *>(_o);
        Q_UNUSED(_t)
        void *_v = _a[0];
        switch (_id) {
        case 0: *reinterpret_cast< QString*>(_v) = _t->title(); break;
        case 1: *reinterpret_cast< QQuickAbstractDialog::StandardButtons*>(_v) = _t->standardButtons(); break;
        case 2: *reinterpret_cast< QQuickAbstractDialog::StandardButton*>(_v) = _t->clickedButton(); break;
        case 3: *reinterpret_cast< QQuickItem**>(_v) = _t->contentItem(); break;
        default: break;
        }
    } else if (_c == QMetaObject::WriteProperty) {
        QQuickDialog *_t = static_cast<QQuickDialog *>(_o);
        Q_UNUSED(_t)
        void *_v = _a[0];
        switch (_id) {
        case 0: _t->setTitle(*reinterpret_cast< QString*>(_v)); break;
        case 1: _t->setStandardButtons(*reinterpret_cast< QQuickAbstractDialog::StandardButtons*>(_v)); break;
        case 3: _t->setContentItem(*reinterpret_cast< QQuickItem**>(_v)); break;
        default: break;
        }
    } else if (_c == QMetaObject::ResetProperty) {
    }
#endif // QT_NO_PROPERTIES
}

static const QMetaObject * const qt_meta_extradata_QQuickDialog[] = {
        &QQuickAbstractDialog::staticMetaObject,
    Q_NULLPTR
};

const QMetaObject QQuickDialog::staticMetaObject = {
    { &QQuickAbstractDialog::staticMetaObject, qt_meta_stringdata_QQuickDialog.data,
      qt_meta_data_QQuickDialog,  qt_static_metacall, qt_meta_extradata_QQuickDialog, Q_NULLPTR}
};


const QMetaObject *QQuickDialog::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->dynamicMetaObject() : &staticMetaObject;
}

void *QQuickDialog::qt_metacast(const char *_clname)
{
    if (!_clname) return Q_NULLPTR;
    if (!strcmp(_clname, qt_meta_stringdata_QQuickDialog.stringdata0))
        return static_cast<void*>(const_cast< QQuickDialog*>(this));
    return QQuickAbstractDialog::qt_metacast(_clname);
}

int QQuickDialog::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QQuickAbstractDialog::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 16)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 16;
    } else if (_c == QMetaObject::RegisterMethodArgumentMetaType) {
        if (_id < 16)
            *reinterpret_cast<int*>(_a[0]) = -1;
        _id -= 16;
    }
#ifndef QT_NO_PROPERTIES
   else if (_c == QMetaObject::ReadProperty || _c == QMetaObject::WriteProperty
            || _c == QMetaObject::ResetProperty || _c == QMetaObject::RegisterPropertyMetaType) {
        qt_static_metacall(this, _c, _id, _a);
        _id -= 4;
    } else if (_c == QMetaObject::QueryPropertyDesignable) {
        _id -= 4;
    } else if (_c == QMetaObject::QueryPropertyScriptable) {
        _id -= 4;
    } else if (_c == QMetaObject::QueryPropertyStored) {
        _id -= 4;
    } else if (_c == QMetaObject::QueryPropertyEditable) {
        _id -= 4;
    } else if (_c == QMetaObject::QueryPropertyUser) {
        _id -= 4;
    }
#endif // QT_NO_PROPERTIES
    return _id;
}

// SIGNAL 0
void QQuickDialog::titleChanged()
{
    QMetaObject::activate(this, &staticMetaObject, 0, Q_NULLPTR);
}

// SIGNAL 1
void QQuickDialog::standardButtonsChanged()
{
    QMetaObject::activate(this, &staticMetaObject, 1, Q_NULLPTR);
}

// SIGNAL 2
void QQuickDialog::buttonClicked()
{
    QMetaObject::activate(this, &staticMetaObject, 2, Q_NULLPTR);
}

// SIGNAL 3
void QQuickDialog::discard()
{
    QMetaObject::activate(this, &staticMetaObject, 3, Q_NULLPTR);
}

// SIGNAL 4
void QQuickDialog::help()
{
    QMetaObject::activate(this, &staticMetaObject, 4, Q_NULLPTR);
}

// SIGNAL 5
void QQuickDialog::yes()
{
    QMetaObject::activate(this, &staticMetaObject, 5, Q_NULLPTR);
}

// SIGNAL 6
void QQuickDialog::no()
{
    QMetaObject::activate(this, &staticMetaObject, 6, Q_NULLPTR);
}

// SIGNAL 7
void QQuickDialog::apply()
{
    QMetaObject::activate(this, &staticMetaObject, 7, Q_NULLPTR);
}

// SIGNAL 8
void QQuickDialog::reset()
{
    QMetaObject::activate(this, &staticMetaObject, 8, Q_NULLPTR);
}
QT_END_MOC_NAMESPACE
