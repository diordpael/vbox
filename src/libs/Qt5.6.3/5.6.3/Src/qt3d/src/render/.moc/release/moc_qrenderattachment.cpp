/****************************************************************************
** Meta object code from reading C++ file 'qrenderattachment.h'
**
** Created by: The Qt Meta Object Compiler version 67 (Qt 5.6.3)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "../../frontend/qrenderattachment.h"
#include <QtCore/qbytearray.h>
#include <QtCore/qmetatype.h>
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'qrenderattachment.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 67
#error "This file was generated using the moc from 5.6.3. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
struct qt_meta_stringdata_Qt3DRender__QRenderAttachment_t {
    QByteArrayData data[50];
    char stringdata0[745];
};
#define QT_MOC_LITERAL(idx, ofs, len) \
    Q_STATIC_BYTE_ARRAY_DATA_HEADER_INITIALIZER_WITH_OFFSET(len, \
    qptrdiff(offsetof(qt_meta_stringdata_Qt3DRender__QRenderAttachment_t, stringdata0) + ofs \
        - idx * sizeof(QByteArrayData)) \
    )
static const qt_meta_stringdata_Qt3DRender__QRenderAttachment_t qt_meta_stringdata_Qt3DRender__QRenderAttachment = {
    {
QT_MOC_LITERAL(0, 0, 29), // "Qt3DRender::QRenderAttachment"
QT_MOC_LITERAL(1, 30, 11), // "typeChanged"
QT_MOC_LITERAL(2, 42, 0), // ""
QT_MOC_LITERAL(3, 43, 20), // "RenderAttachmentType"
QT_MOC_LITERAL(4, 64, 4), // "type"
QT_MOC_LITERAL(5, 69, 14), // "textureChanged"
QT_MOC_LITERAL(6, 84, 25), // "QAbstractTextureProvider*"
QT_MOC_LITERAL(7, 110, 7), // "texture"
QT_MOC_LITERAL(8, 118, 15), // "mipLevelChanged"
QT_MOC_LITERAL(9, 134, 8), // "mipLevel"
QT_MOC_LITERAL(10, 143, 12), // "layerChanged"
QT_MOC_LITERAL(11, 156, 5), // "layer"
QT_MOC_LITERAL(12, 162, 11), // "faceChanged"
QT_MOC_LITERAL(13, 174, 11), // "CubeMapFace"
QT_MOC_LITERAL(14, 186, 4), // "face"
QT_MOC_LITERAL(15, 191, 11), // "nameChanged"
QT_MOC_LITERAL(16, 203, 4), // "name"
QT_MOC_LITERAL(17, 208, 7), // "setType"
QT_MOC_LITERAL(18, 216, 10), // "setTexture"
QT_MOC_LITERAL(19, 227, 11), // "setMipLevel"
QT_MOC_LITERAL(20, 239, 5), // "level"
QT_MOC_LITERAL(21, 245, 8), // "setLayer"
QT_MOC_LITERAL(22, 254, 7), // "setFace"
QT_MOC_LITERAL(23, 262, 7), // "setName"
QT_MOC_LITERAL(24, 270, 37), // "Qt3DRender::QAbstractTextureP..."
QT_MOC_LITERAL(25, 308, 16), // "ColorAttachment0"
QT_MOC_LITERAL(26, 325, 16), // "ColorAttachment1"
QT_MOC_LITERAL(27, 342, 16), // "ColorAttachment2"
QT_MOC_LITERAL(28, 359, 16), // "ColorAttachment3"
QT_MOC_LITERAL(29, 376, 16), // "ColorAttachment4"
QT_MOC_LITERAL(30, 393, 16), // "ColorAttachment5"
QT_MOC_LITERAL(31, 410, 16), // "ColorAttachment6"
QT_MOC_LITERAL(32, 427, 16), // "ColorAttachment7"
QT_MOC_LITERAL(33, 444, 16), // "ColorAttachment8"
QT_MOC_LITERAL(34, 461, 16), // "ColorAttachment9"
QT_MOC_LITERAL(35, 478, 17), // "ColorAttachment10"
QT_MOC_LITERAL(36, 496, 17), // "ColorAttachment11"
QT_MOC_LITERAL(37, 514, 17), // "ColorAttachment12"
QT_MOC_LITERAL(38, 532, 17), // "ColorAttachment13"
QT_MOC_LITERAL(39, 550, 17), // "ColorAttachment14"
QT_MOC_LITERAL(40, 568, 17), // "ColorAttachment15"
QT_MOC_LITERAL(41, 586, 15), // "DepthAttachment"
QT_MOC_LITERAL(42, 602, 17), // "StencilAttachment"
QT_MOC_LITERAL(43, 620, 22), // "DepthStencilAttachment"
QT_MOC_LITERAL(44, 643, 16), // "CubeMapPositiveX"
QT_MOC_LITERAL(45, 660, 16), // "CubeMapNegativeX"
QT_MOC_LITERAL(46, 677, 16), // "CubeMapPositiveY"
QT_MOC_LITERAL(47, 694, 16), // "CubeMapNegativeY"
QT_MOC_LITERAL(48, 711, 16), // "CubeMapPositiveZ"
QT_MOC_LITERAL(49, 728, 16) // "CubeMapNegativeZ"

    },
    "Qt3DRender::QRenderAttachment\0typeChanged\0"
    "\0RenderAttachmentType\0type\0textureChanged\0"
    "QAbstractTextureProvider*\0texture\0"
    "mipLevelChanged\0mipLevel\0layerChanged\0"
    "layer\0faceChanged\0CubeMapFace\0face\0"
    "nameChanged\0name\0setType\0setTexture\0"
    "setMipLevel\0level\0setLayer\0setFace\0"
    "setName\0Qt3DRender::QAbstractTextureProvider*\0"
    "ColorAttachment0\0ColorAttachment1\0"
    "ColorAttachment2\0ColorAttachment3\0"
    "ColorAttachment4\0ColorAttachment5\0"
    "ColorAttachment6\0ColorAttachment7\0"
    "ColorAttachment8\0ColorAttachment9\0"
    "ColorAttachment10\0ColorAttachment11\0"
    "ColorAttachment12\0ColorAttachment13\0"
    "ColorAttachment14\0ColorAttachment15\0"
    "DepthAttachment\0StencilAttachment\0"
    "DepthStencilAttachment\0CubeMapPositiveX\0"
    "CubeMapNegativeX\0CubeMapPositiveY\0"
    "CubeMapNegativeY\0CubeMapPositiveZ\0"
    "CubeMapNegativeZ"
};
#undef QT_MOC_LITERAL

static const uint qt_meta_data_Qt3DRender__QRenderAttachment[] = {

 // content:
       7,       // revision
       0,       // classname
       0,    0, // classinfo
      12,   14, // methods
       6,  110, // properties
       2,  134, // enums/sets
       0,    0, // constructors
       0,       // flags
       6,       // signalCount

 // signals: name, argc, parameters, tag, flags
       1,    1,   74,    2, 0x06 /* Public */,
       5,    1,   77,    2, 0x06 /* Public */,
       8,    1,   80,    2, 0x06 /* Public */,
      10,    1,   83,    2, 0x06 /* Public */,
      12,    1,   86,    2, 0x06 /* Public */,
      15,    1,   89,    2, 0x06 /* Public */,

 // slots: name, argc, parameters, tag, flags
      17,    1,   92,    2, 0x0a /* Public */,
      18,    1,   95,    2, 0x0a /* Public */,
      19,    1,   98,    2, 0x0a /* Public */,
      21,    1,  101,    2, 0x0a /* Public */,
      22,    1,  104,    2, 0x0a /* Public */,
      23,    1,  107,    2, 0x0a /* Public */,

 // signals: parameters
    QMetaType::Void, 0x80000000 | 3,    4,
    QMetaType::Void, 0x80000000 | 6,    7,
    QMetaType::Void, QMetaType::Int,    9,
    QMetaType::Void, QMetaType::Int,   11,
    QMetaType::Void, 0x80000000 | 13,   14,
    QMetaType::Void, QMetaType::QString,   16,

 // slots: parameters
    QMetaType::Void, 0x80000000 | 3,    4,
    QMetaType::Void, 0x80000000 | 6,    7,
    QMetaType::Void, QMetaType::Int,   20,
    QMetaType::Void, QMetaType::Int,   11,
    QMetaType::Void, 0x80000000 | 13,   14,
    QMetaType::Void, QMetaType::QString,   16,

 // properties: name, type, flags
       4, 0x80000000 | 3, 0x0049510b,
       7, 0x80000000 | 24, 0x0049510b,
       9, QMetaType::Int, 0x00495103,
      11, QMetaType::Int, 0x00495103,
      14, 0x80000000 | 13, 0x0049510b,
      16, QMetaType::QString, 0x00495103,

 // properties: notify_signal_id
       0,
       1,
       2,
       3,
       4,
       5,

 // enums: name, flags, count, data
       3, 0x0,   19,  142,
      13, 0x0,    6,  180,

 // enum data: key, value
      25, uint(Qt3DRender::QRenderAttachment::ColorAttachment0),
      26, uint(Qt3DRender::QRenderAttachment::ColorAttachment1),
      27, uint(Qt3DRender::QRenderAttachment::ColorAttachment2),
      28, uint(Qt3DRender::QRenderAttachment::ColorAttachment3),
      29, uint(Qt3DRender::QRenderAttachment::ColorAttachment4),
      30, uint(Qt3DRender::QRenderAttachment::ColorAttachment5),
      31, uint(Qt3DRender::QRenderAttachment::ColorAttachment6),
      32, uint(Qt3DRender::QRenderAttachment::ColorAttachment7),
      33, uint(Qt3DRender::QRenderAttachment::ColorAttachment8),
      34, uint(Qt3DRender::QRenderAttachment::ColorAttachment9),
      35, uint(Qt3DRender::QRenderAttachment::ColorAttachment10),
      36, uint(Qt3DRender::QRenderAttachment::ColorAttachment11),
      37, uint(Qt3DRender::QRenderAttachment::ColorAttachment12),
      38, uint(Qt3DRender::QRenderAttachment::ColorAttachment13),
      39, uint(Qt3DRender::QRenderAttachment::ColorAttachment14),
      40, uint(Qt3DRender::QRenderAttachment::ColorAttachment15),
      41, uint(Qt3DRender::QRenderAttachment::DepthAttachment),
      42, uint(Qt3DRender::QRenderAttachment::StencilAttachment),
      43, uint(Qt3DRender::QRenderAttachment::DepthStencilAttachment),
      44, uint(Qt3DRender::QRenderAttachment::CubeMapPositiveX),
      45, uint(Qt3DRender::QRenderAttachment::CubeMapNegativeX),
      46, uint(Qt3DRender::QRenderAttachment::CubeMapPositiveY),
      47, uint(Qt3DRender::QRenderAttachment::CubeMapNegativeY),
      48, uint(Qt3DRender::QRenderAttachment::CubeMapPositiveZ),
      49, uint(Qt3DRender::QRenderAttachment::CubeMapNegativeZ),

       0        // eod
};

void Qt3DRender::QRenderAttachment::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        QRenderAttachment *_t = static_cast<QRenderAttachment *>(_o);
        Q_UNUSED(_t)
        switch (_id) {
        case 0: _t->typeChanged((*reinterpret_cast< RenderAttachmentType(*)>(_a[1]))); break;
        case 1: _t->textureChanged((*reinterpret_cast< QAbstractTextureProvider*(*)>(_a[1]))); break;
        case 2: _t->mipLevelChanged((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 3: _t->layerChanged((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 4: _t->faceChanged((*reinterpret_cast< CubeMapFace(*)>(_a[1]))); break;
        case 5: _t->nameChanged((*reinterpret_cast< const QString(*)>(_a[1]))); break;
        case 6: _t->setType((*reinterpret_cast< RenderAttachmentType(*)>(_a[1]))); break;
        case 7: _t->setTexture((*reinterpret_cast< QAbstractTextureProvider*(*)>(_a[1]))); break;
        case 8: _t->setMipLevel((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 9: _t->setLayer((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 10: _t->setFace((*reinterpret_cast< CubeMapFace(*)>(_a[1]))); break;
        case 11: _t->setName((*reinterpret_cast< const QString(*)>(_a[1]))); break;
        default: ;
        }
    } else if (_c == QMetaObject::IndexOfMethod) {
        int *result = reinterpret_cast<int *>(_a[0]);
        void **func = reinterpret_cast<void **>(_a[1]);
        {
            typedef void (QRenderAttachment::*_t)(RenderAttachmentType );
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&QRenderAttachment::typeChanged)) {
                *result = 0;
                return;
            }
        }
        {
            typedef void (QRenderAttachment::*_t)(QAbstractTextureProvider * );
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&QRenderAttachment::textureChanged)) {
                *result = 1;
                return;
            }
        }
        {
            typedef void (QRenderAttachment::*_t)(int );
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&QRenderAttachment::mipLevelChanged)) {
                *result = 2;
                return;
            }
        }
        {
            typedef void (QRenderAttachment::*_t)(int );
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&QRenderAttachment::layerChanged)) {
                *result = 3;
                return;
            }
        }
        {
            typedef void (QRenderAttachment::*_t)(CubeMapFace );
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&QRenderAttachment::faceChanged)) {
                *result = 4;
                return;
            }
        }
        {
            typedef void (QRenderAttachment::*_t)(const QString & );
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&QRenderAttachment::nameChanged)) {
                *result = 5;
                return;
            }
        }
    }
#ifndef QT_NO_PROPERTIES
    else if (_c == QMetaObject::ReadProperty) {
        QRenderAttachment *_t = static_cast<QRenderAttachment *>(_o);
        Q_UNUSED(_t)
        void *_v = _a[0];
        switch (_id) {
        case 0: *reinterpret_cast< RenderAttachmentType*>(_v) = _t->type(); break;
        case 1: *reinterpret_cast< Qt3DRender::QAbstractTextureProvider**>(_v) = _t->texture(); break;
        case 2: *reinterpret_cast< int*>(_v) = _t->mipLevel(); break;
        case 3: *reinterpret_cast< int*>(_v) = _t->layer(); break;
        case 4: *reinterpret_cast< CubeMapFace*>(_v) = _t->face(); break;
        case 5: *reinterpret_cast< QString*>(_v) = _t->name(); break;
        default: break;
        }
    } else if (_c == QMetaObject::WriteProperty) {
        QRenderAttachment *_t = static_cast<QRenderAttachment *>(_o);
        Q_UNUSED(_t)
        void *_v = _a[0];
        switch (_id) {
        case 0: _t->setType(*reinterpret_cast< RenderAttachmentType*>(_v)); break;
        case 1: _t->setTexture(*reinterpret_cast< Qt3DRender::QAbstractTextureProvider**>(_v)); break;
        case 2: _t->setMipLevel(*reinterpret_cast< int*>(_v)); break;
        case 3: _t->setLayer(*reinterpret_cast< int*>(_v)); break;
        case 4: _t->setFace(*reinterpret_cast< CubeMapFace*>(_v)); break;
        case 5: _t->setName(*reinterpret_cast< QString*>(_v)); break;
        default: break;
        }
    } else if (_c == QMetaObject::ResetProperty) {
    }
#endif // QT_NO_PROPERTIES
}

const QMetaObject Qt3DRender::QRenderAttachment::staticMetaObject = {
    { &Qt3DCore::QNode::staticMetaObject, qt_meta_stringdata_Qt3DRender__QRenderAttachment.data,
      qt_meta_data_Qt3DRender__QRenderAttachment,  qt_static_metacall, Q_NULLPTR, Q_NULLPTR}
};


const QMetaObject *Qt3DRender::QRenderAttachment::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->dynamicMetaObject() : &staticMetaObject;
}

void *Qt3DRender::QRenderAttachment::qt_metacast(const char *_clname)
{
    if (!_clname) return Q_NULLPTR;
    if (!strcmp(_clname, qt_meta_stringdata_Qt3DRender__QRenderAttachment.stringdata0))
        return static_cast<void*>(const_cast< QRenderAttachment*>(this));
    return Qt3DCore::QNode::qt_metacast(_clname);
}

int Qt3DRender::QRenderAttachment::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = Qt3DCore::QNode::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 12)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 12;
    } else if (_c == QMetaObject::RegisterMethodArgumentMetaType) {
        if (_id < 12)
            *reinterpret_cast<int*>(_a[0]) = -1;
        _id -= 12;
    }
#ifndef QT_NO_PROPERTIES
   else if (_c == QMetaObject::ReadProperty || _c == QMetaObject::WriteProperty
            || _c == QMetaObject::ResetProperty || _c == QMetaObject::RegisterPropertyMetaType) {
        qt_static_metacall(this, _c, _id, _a);
        _id -= 6;
    } else if (_c == QMetaObject::QueryPropertyDesignable) {
        _id -= 6;
    } else if (_c == QMetaObject::QueryPropertyScriptable) {
        _id -= 6;
    } else if (_c == QMetaObject::QueryPropertyStored) {
        _id -= 6;
    } else if (_c == QMetaObject::QueryPropertyEditable) {
        _id -= 6;
    } else if (_c == QMetaObject::QueryPropertyUser) {
        _id -= 6;
    }
#endif // QT_NO_PROPERTIES
    return _id;
}

// SIGNAL 0
void Qt3DRender::QRenderAttachment::typeChanged(RenderAttachmentType _t1)
{
    void *_a[] = { Q_NULLPTR, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 0, _a);
}

// SIGNAL 1
void Qt3DRender::QRenderAttachment::textureChanged(QAbstractTextureProvider * _t1)
{
    void *_a[] = { Q_NULLPTR, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 1, _a);
}

// SIGNAL 2
void Qt3DRender::QRenderAttachment::mipLevelChanged(int _t1)
{
    void *_a[] = { Q_NULLPTR, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 2, _a);
}

// SIGNAL 3
void Qt3DRender::QRenderAttachment::layerChanged(int _t1)
{
    void *_a[] = { Q_NULLPTR, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 3, _a);
}

// SIGNAL 4
void Qt3DRender::QRenderAttachment::faceChanged(CubeMapFace _t1)
{
    void *_a[] = { Q_NULLPTR, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 4, _a);
}

// SIGNAL 5
void Qt3DRender::QRenderAttachment::nameChanged(const QString & _t1)
{
    void *_a[] = { Q_NULLPTR, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 5, _a);
}
QT_END_MOC_NAMESPACE
