/****************************************************************************
** Meta object code from reading C++ file 'qgeometryrenderer.h'
**
** Created by: The Qt Meta Object Compiler version 67 (Qt 5.6.3)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "../../geometry/qgeometryrenderer.h"
#include <QtCore/qbytearray.h>
#include <QtCore/qmetatype.h>
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'qgeometryrenderer.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 67
#error "This file was generated using the moc from 5.6.3. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
struct qt_meta_stringdata_Qt3DRender__QGeometryRenderer_t {
    QByteArrayData data[43];
    char stringdata0[643];
};
#define QT_MOC_LITERAL(idx, ofs, len) \
    Q_STATIC_BYTE_ARRAY_DATA_HEADER_INITIALIZER_WITH_OFFSET(len, \
    qptrdiff(offsetof(qt_meta_stringdata_Qt3DRender__QGeometryRenderer_t, stringdata0) + ofs \
        - idx * sizeof(QByteArrayData)) \
    )
static const qt_meta_stringdata_Qt3DRender__QGeometryRenderer_t qt_meta_stringdata_Qt3DRender__QGeometryRenderer = {
    {
QT_MOC_LITERAL(0, 0, 29), // "Qt3DRender::QGeometryRenderer"
QT_MOC_LITERAL(1, 30, 20), // "instanceCountChanged"
QT_MOC_LITERAL(2, 51, 0), // ""
QT_MOC_LITERAL(3, 52, 13), // "instanceCount"
QT_MOC_LITERAL(4, 66, 21), // "primitiveCountChanged"
QT_MOC_LITERAL(5, 88, 14), // "primitiveCount"
QT_MOC_LITERAL(6, 103, 17), // "baseVertexChanged"
QT_MOC_LITERAL(7, 121, 10), // "baseVertex"
QT_MOC_LITERAL(8, 132, 19), // "baseInstanceChanged"
QT_MOC_LITERAL(9, 152, 12), // "baseInstance"
QT_MOC_LITERAL(10, 165, 19), // "restartIndexChanged"
QT_MOC_LITERAL(11, 185, 12), // "restartIndex"
QT_MOC_LITERAL(12, 198, 23), // "primitiveRestartChanged"
QT_MOC_LITERAL(13, 222, 16), // "primitiveRestart"
QT_MOC_LITERAL(14, 239, 15), // "geometryChanged"
QT_MOC_LITERAL(15, 255, 10), // "QGeometry*"
QT_MOC_LITERAL(16, 266, 8), // "geometry"
QT_MOC_LITERAL(17, 275, 20), // "primitiveTypeChanged"
QT_MOC_LITERAL(18, 296, 13), // "PrimitiveType"
QT_MOC_LITERAL(19, 310, 13), // "primitiveType"
QT_MOC_LITERAL(20, 324, 16), // "setInstanceCount"
QT_MOC_LITERAL(21, 341, 17), // "setPrimitiveCount"
QT_MOC_LITERAL(22, 359, 13), // "setBaseVertex"
QT_MOC_LITERAL(23, 373, 15), // "setBaseInstance"
QT_MOC_LITERAL(24, 389, 15), // "setRestartIndex"
QT_MOC_LITERAL(25, 405, 5), // "index"
QT_MOC_LITERAL(26, 411, 19), // "setPrimitiveRestart"
QT_MOC_LITERAL(27, 431, 7), // "enabled"
QT_MOC_LITERAL(28, 439, 11), // "setGeometry"
QT_MOC_LITERAL(29, 451, 16), // "setPrimitiveType"
QT_MOC_LITERAL(30, 468, 22), // "Qt3DRender::QGeometry*"
QT_MOC_LITERAL(31, 491, 6), // "Points"
QT_MOC_LITERAL(32, 498, 5), // "Lines"
QT_MOC_LITERAL(33, 504, 8), // "LineLoop"
QT_MOC_LITERAL(34, 513, 9), // "LineStrip"
QT_MOC_LITERAL(35, 523, 9), // "Triangles"
QT_MOC_LITERAL(36, 533, 13), // "TriangleStrip"
QT_MOC_LITERAL(37, 547, 11), // "TriangleFan"
QT_MOC_LITERAL(38, 559, 14), // "LinesAdjacency"
QT_MOC_LITERAL(39, 574, 18), // "TrianglesAdjacency"
QT_MOC_LITERAL(40, 593, 18), // "LineStripAdjacency"
QT_MOC_LITERAL(41, 612, 22), // "TriangleStripAdjacency"
QT_MOC_LITERAL(42, 635, 7) // "Patches"

    },
    "Qt3DRender::QGeometryRenderer\0"
    "instanceCountChanged\0\0instanceCount\0"
    "primitiveCountChanged\0primitiveCount\0"
    "baseVertexChanged\0baseVertex\0"
    "baseInstanceChanged\0baseInstance\0"
    "restartIndexChanged\0restartIndex\0"
    "primitiveRestartChanged\0primitiveRestart\0"
    "geometryChanged\0QGeometry*\0geometry\0"
    "primitiveTypeChanged\0PrimitiveType\0"
    "primitiveType\0setInstanceCount\0"
    "setPrimitiveCount\0setBaseVertex\0"
    "setBaseInstance\0setRestartIndex\0index\0"
    "setPrimitiveRestart\0enabled\0setGeometry\0"
    "setPrimitiveType\0Qt3DRender::QGeometry*\0"
    "Points\0Lines\0LineLoop\0LineStrip\0"
    "Triangles\0TriangleStrip\0TriangleFan\0"
    "LinesAdjacency\0TrianglesAdjacency\0"
    "LineStripAdjacency\0TriangleStripAdjacency\0"
    "Patches"
};
#undef QT_MOC_LITERAL

static const uint qt_meta_data_Qt3DRender__QGeometryRenderer[] = {

 // content:
       7,       // revision
       0,       // classname
       0,    0, // classinfo
      16,   14, // methods
       8,  142, // properties
       1,  174, // enums/sets
       0,    0, // constructors
       0,       // flags
       8,       // signalCount

 // signals: name, argc, parameters, tag, flags
       1,    1,   94,    2, 0x06 /* Public */,
       4,    1,   97,    2, 0x06 /* Public */,
       6,    1,  100,    2, 0x06 /* Public */,
       8,    1,  103,    2, 0x06 /* Public */,
      10,    1,  106,    2, 0x06 /* Public */,
      12,    1,  109,    2, 0x06 /* Public */,
      14,    1,  112,    2, 0x06 /* Public */,
      17,    1,  115,    2, 0x06 /* Public */,

 // slots: name, argc, parameters, tag, flags
      20,    1,  118,    2, 0x0a /* Public */,
      21,    1,  121,    2, 0x0a /* Public */,
      22,    1,  124,    2, 0x0a /* Public */,
      23,    1,  127,    2, 0x0a /* Public */,
      24,    1,  130,    2, 0x0a /* Public */,
      26,    1,  133,    2, 0x0a /* Public */,
      28,    1,  136,    2, 0x0a /* Public */,
      29,    1,  139,    2, 0x0a /* Public */,

 // signals: parameters
    QMetaType::Void, QMetaType::Int,    3,
    QMetaType::Void, QMetaType::Int,    5,
    QMetaType::Void, QMetaType::Int,    7,
    QMetaType::Void, QMetaType::Int,    9,
    QMetaType::Void, QMetaType::Int,   11,
    QMetaType::Void, QMetaType::Bool,   13,
    QMetaType::Void, 0x80000000 | 15,   16,
    QMetaType::Void, 0x80000000 | 18,   19,

 // slots: parameters
    QMetaType::Void, QMetaType::Int,    3,
    QMetaType::Void, QMetaType::Int,    5,
    QMetaType::Void, QMetaType::Int,    7,
    QMetaType::Void, QMetaType::Int,    9,
    QMetaType::Void, QMetaType::Int,   25,
    QMetaType::Void, QMetaType::Bool,   27,
    QMetaType::Void, 0x80000000 | 15,   16,
    QMetaType::Void, 0x80000000 | 18,   19,

 // properties: name, type, flags
       3, QMetaType::Int, 0x00495103,
       5, QMetaType::Int, 0x00495103,
       7, QMetaType::Int, 0x00495103,
       9, QMetaType::Int, 0x00495103,
      11, QMetaType::Int, 0x00495103,
      13, QMetaType::Bool, 0x00495103,
      16, 0x80000000 | 30, 0x0049510b,
      19, 0x80000000 | 18, 0x0049510b,

 // properties: notify_signal_id
       0,
       1,
       2,
       3,
       4,
       5,
       6,
       7,

 // enums: name, flags, count, data
      18, 0x0,   12,  178,

 // enum data: key, value
      31, uint(Qt3DRender::QGeometryRenderer::Points),
      32, uint(Qt3DRender::QGeometryRenderer::Lines),
      33, uint(Qt3DRender::QGeometryRenderer::LineLoop),
      34, uint(Qt3DRender::QGeometryRenderer::LineStrip),
      35, uint(Qt3DRender::QGeometryRenderer::Triangles),
      36, uint(Qt3DRender::QGeometryRenderer::TriangleStrip),
      37, uint(Qt3DRender::QGeometryRenderer::TriangleFan),
      38, uint(Qt3DRender::QGeometryRenderer::LinesAdjacency),
      39, uint(Qt3DRender::QGeometryRenderer::TrianglesAdjacency),
      40, uint(Qt3DRender::QGeometryRenderer::LineStripAdjacency),
      41, uint(Qt3DRender::QGeometryRenderer::TriangleStripAdjacency),
      42, uint(Qt3DRender::QGeometryRenderer::Patches),

       0        // eod
};

void Qt3DRender::QGeometryRenderer::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        QGeometryRenderer *_t = static_cast<QGeometryRenderer *>(_o);
        Q_UNUSED(_t)
        switch (_id) {
        case 0: _t->instanceCountChanged((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 1: _t->primitiveCountChanged((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 2: _t->baseVertexChanged((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 3: _t->baseInstanceChanged((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 4: _t->restartIndexChanged((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 5: _t->primitiveRestartChanged((*reinterpret_cast< bool(*)>(_a[1]))); break;
        case 6: _t->geometryChanged((*reinterpret_cast< QGeometry*(*)>(_a[1]))); break;
        case 7: _t->primitiveTypeChanged((*reinterpret_cast< PrimitiveType(*)>(_a[1]))); break;
        case 8: _t->setInstanceCount((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 9: _t->setPrimitiveCount((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 10: _t->setBaseVertex((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 11: _t->setBaseInstance((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 12: _t->setRestartIndex((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 13: _t->setPrimitiveRestart((*reinterpret_cast< bool(*)>(_a[1]))); break;
        case 14: _t->setGeometry((*reinterpret_cast< QGeometry*(*)>(_a[1]))); break;
        case 15: _t->setPrimitiveType((*reinterpret_cast< PrimitiveType(*)>(_a[1]))); break;
        default: ;
        }
    } else if (_c == QMetaObject::RegisterMethodArgumentMetaType) {
        switch (_id) {
        default: *reinterpret_cast<int*>(_a[0]) = -1; break;
        case 6:
            switch (*reinterpret_cast<int*>(_a[1])) {
            default: *reinterpret_cast<int*>(_a[0]) = -1; break;
            case 0:
                *reinterpret_cast<int*>(_a[0]) = qRegisterMetaType< QGeometry* >(); break;
            }
            break;
        case 14:
            switch (*reinterpret_cast<int*>(_a[1])) {
            default: *reinterpret_cast<int*>(_a[0]) = -1; break;
            case 0:
                *reinterpret_cast<int*>(_a[0]) = qRegisterMetaType< QGeometry* >(); break;
            }
            break;
        }
    } else if (_c == QMetaObject::IndexOfMethod) {
        int *result = reinterpret_cast<int *>(_a[0]);
        void **func = reinterpret_cast<void **>(_a[1]);
        {
            typedef void (QGeometryRenderer::*_t)(int );
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&QGeometryRenderer::instanceCountChanged)) {
                *result = 0;
                return;
            }
        }
        {
            typedef void (QGeometryRenderer::*_t)(int );
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&QGeometryRenderer::primitiveCountChanged)) {
                *result = 1;
                return;
            }
        }
        {
            typedef void (QGeometryRenderer::*_t)(int );
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&QGeometryRenderer::baseVertexChanged)) {
                *result = 2;
                return;
            }
        }
        {
            typedef void (QGeometryRenderer::*_t)(int );
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&QGeometryRenderer::baseInstanceChanged)) {
                *result = 3;
                return;
            }
        }
        {
            typedef void (QGeometryRenderer::*_t)(int );
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&QGeometryRenderer::restartIndexChanged)) {
                *result = 4;
                return;
            }
        }
        {
            typedef void (QGeometryRenderer::*_t)(bool );
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&QGeometryRenderer::primitiveRestartChanged)) {
                *result = 5;
                return;
            }
        }
        {
            typedef void (QGeometryRenderer::*_t)(QGeometry * );
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&QGeometryRenderer::geometryChanged)) {
                *result = 6;
                return;
            }
        }
        {
            typedef void (QGeometryRenderer::*_t)(PrimitiveType );
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&QGeometryRenderer::primitiveTypeChanged)) {
                *result = 7;
                return;
            }
        }
    } else if (_c == QMetaObject::RegisterPropertyMetaType) {
        switch (_id) {
        default: *reinterpret_cast<int*>(_a[0]) = -1; break;
        case 6:
            *reinterpret_cast<int*>(_a[0]) = qRegisterMetaType< Qt3DRender::QGeometry* >(); break;
        }
    }

#ifndef QT_NO_PROPERTIES
    else if (_c == QMetaObject::ReadProperty) {
        QGeometryRenderer *_t = static_cast<QGeometryRenderer *>(_o);
        Q_UNUSED(_t)
        void *_v = _a[0];
        switch (_id) {
        case 0: *reinterpret_cast< int*>(_v) = _t->instanceCount(); break;
        case 1: *reinterpret_cast< int*>(_v) = _t->primitiveCount(); break;
        case 2: *reinterpret_cast< int*>(_v) = _t->baseVertex(); break;
        case 3: *reinterpret_cast< int*>(_v) = _t->baseInstance(); break;
        case 4: *reinterpret_cast< int*>(_v) = _t->restartIndex(); break;
        case 5: *reinterpret_cast< bool*>(_v) = _t->primitiveRestart(); break;
        case 6: *reinterpret_cast< Qt3DRender::QGeometry**>(_v) = _t->geometry(); break;
        case 7: *reinterpret_cast< PrimitiveType*>(_v) = _t->primitiveType(); break;
        default: break;
        }
    } else if (_c == QMetaObject::WriteProperty) {
        QGeometryRenderer *_t = static_cast<QGeometryRenderer *>(_o);
        Q_UNUSED(_t)
        void *_v = _a[0];
        switch (_id) {
        case 0: _t->setInstanceCount(*reinterpret_cast< int*>(_v)); break;
        case 1: _t->setPrimitiveCount(*reinterpret_cast< int*>(_v)); break;
        case 2: _t->setBaseVertex(*reinterpret_cast< int*>(_v)); break;
        case 3: _t->setBaseInstance(*reinterpret_cast< int*>(_v)); break;
        case 4: _t->setRestartIndex(*reinterpret_cast< int*>(_v)); break;
        case 5: _t->setPrimitiveRestart(*reinterpret_cast< bool*>(_v)); break;
        case 6: _t->setGeometry(*reinterpret_cast< Qt3DRender::QGeometry**>(_v)); break;
        case 7: _t->setPrimitiveType(*reinterpret_cast< PrimitiveType*>(_v)); break;
        default: break;
        }
    } else if (_c == QMetaObject::ResetProperty) {
    }
#endif // QT_NO_PROPERTIES
}

const QMetaObject Qt3DRender::QGeometryRenderer::staticMetaObject = {
    { &Qt3DCore::QComponent::staticMetaObject, qt_meta_stringdata_Qt3DRender__QGeometryRenderer.data,
      qt_meta_data_Qt3DRender__QGeometryRenderer,  qt_static_metacall, Q_NULLPTR, Q_NULLPTR}
};


const QMetaObject *Qt3DRender::QGeometryRenderer::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->dynamicMetaObject() : &staticMetaObject;
}

void *Qt3DRender::QGeometryRenderer::qt_metacast(const char *_clname)
{
    if (!_clname) return Q_NULLPTR;
    if (!strcmp(_clname, qt_meta_stringdata_Qt3DRender__QGeometryRenderer.stringdata0))
        return static_cast<void*>(const_cast< QGeometryRenderer*>(this));
    return Qt3DCore::QComponent::qt_metacast(_clname);
}

int Qt3DRender::QGeometryRenderer::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = Qt3DCore::QComponent::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 16)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 16;
    } else if (_c == QMetaObject::RegisterMethodArgumentMetaType) {
        if (_id < 16)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 16;
    }
#ifndef QT_NO_PROPERTIES
   else if (_c == QMetaObject::ReadProperty || _c == QMetaObject::WriteProperty
            || _c == QMetaObject::ResetProperty || _c == QMetaObject::RegisterPropertyMetaType) {
        qt_static_metacall(this, _c, _id, _a);
        _id -= 8;
    } else if (_c == QMetaObject::QueryPropertyDesignable) {
        _id -= 8;
    } else if (_c == QMetaObject::QueryPropertyScriptable) {
        _id -= 8;
    } else if (_c == QMetaObject::QueryPropertyStored) {
        _id -= 8;
    } else if (_c == QMetaObject::QueryPropertyEditable) {
        _id -= 8;
    } else if (_c == QMetaObject::QueryPropertyUser) {
        _id -= 8;
    }
#endif // QT_NO_PROPERTIES
    return _id;
}

// SIGNAL 0
void Qt3DRender::QGeometryRenderer::instanceCountChanged(int _t1)
{
    void *_a[] = { Q_NULLPTR, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 0, _a);
}

// SIGNAL 1
void Qt3DRender::QGeometryRenderer::primitiveCountChanged(int _t1)
{
    void *_a[] = { Q_NULLPTR, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 1, _a);
}

// SIGNAL 2
void Qt3DRender::QGeometryRenderer::baseVertexChanged(int _t1)
{
    void *_a[] = { Q_NULLPTR, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 2, _a);
}

// SIGNAL 3
void Qt3DRender::QGeometryRenderer::baseInstanceChanged(int _t1)
{
    void *_a[] = { Q_NULLPTR, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 3, _a);
}

// SIGNAL 4
void Qt3DRender::QGeometryRenderer::restartIndexChanged(int _t1)
{
    void *_a[] = { Q_NULLPTR, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 4, _a);
}

// SIGNAL 5
void Qt3DRender::QGeometryRenderer::primitiveRestartChanged(bool _t1)
{
    void *_a[] = { Q_NULLPTR, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 5, _a);
}

// SIGNAL 6
void Qt3DRender::QGeometryRenderer::geometryChanged(QGeometry * _t1)
{
    void *_a[] = { Q_NULLPTR, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 6, _a);
}

// SIGNAL 7
void Qt3DRender::QGeometryRenderer::primitiveTypeChanged(PrimitiveType _t1)
{
    void *_a[] = { Q_NULLPTR, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 7, _a);
}
QT_END_MOC_NAMESPACE
