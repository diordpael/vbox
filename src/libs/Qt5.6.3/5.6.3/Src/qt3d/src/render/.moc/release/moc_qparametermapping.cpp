/****************************************************************************
** Meta object code from reading C++ file 'qparametermapping.h'
**
** Created by: The Qt Meta Object Compiler version 67 (Qt 5.6.3)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "../../materialsystem/qparametermapping.h"
#include <QtCore/qbytearray.h>
#include <QtCore/qmetatype.h>
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'qparametermapping.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 67
#error "This file was generated using the moc from 5.6.3. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
struct qt_meta_stringdata_Qt3DRender__QParameterMapping_t {
    QByteArrayData data[20];
    char stringdata0[309];
};
#define QT_MOC_LITERAL(idx, ofs, len) \
    Q_STATIC_BYTE_ARRAY_DATA_HEADER_INITIALIZER_WITH_OFFSET(len, \
    qptrdiff(offsetof(qt_meta_stringdata_Qt3DRender__QParameterMapping_t, stringdata0) + ofs \
        - idx * sizeof(QByteArrayData)) \
    )
static const qt_meta_stringdata_Qt3DRender__QParameterMapping_t qt_meta_stringdata_Qt3DRender__QParameterMapping = {
    {
QT_MOC_LITERAL(0, 0, 29), // "Qt3DRender::QParameterMapping"
QT_MOC_LITERAL(1, 30, 20), // "parameterNameChanged"
QT_MOC_LITERAL(2, 51, 0), // ""
QT_MOC_LITERAL(3, 52, 13), // "parameterName"
QT_MOC_LITERAL(4, 66, 25), // "shaderVariableNameChanged"
QT_MOC_LITERAL(5, 92, 18), // "shaderVariableName"
QT_MOC_LITERAL(6, 111, 18), // "bindingTypeChanged"
QT_MOC_LITERAL(7, 130, 7), // "Binding"
QT_MOC_LITERAL(8, 138, 11), // "bindingType"
QT_MOC_LITERAL(9, 150, 16), // "setParameterName"
QT_MOC_LITERAL(10, 167, 4), // "name"
QT_MOC_LITERAL(11, 172, 21), // "setShaderVariableName"
QT_MOC_LITERAL(12, 194, 14), // "setBindingType"
QT_MOC_LITERAL(13, 209, 4), // "type"
QT_MOC_LITERAL(14, 214, 7), // "Uniform"
QT_MOC_LITERAL(15, 222, 9), // "Attribute"
QT_MOC_LITERAL(16, 232, 15), // "StandardUniform"
QT_MOC_LITERAL(17, 248, 14), // "FragmentOutput"
QT_MOC_LITERAL(18, 263, 19), // "UniformBufferObject"
QT_MOC_LITERAL(19, 283, 25) // "ShaderStorageBufferObject"

    },
    "Qt3DRender::QParameterMapping\0"
    "parameterNameChanged\0\0parameterName\0"
    "shaderVariableNameChanged\0shaderVariableName\0"
    "bindingTypeChanged\0Binding\0bindingType\0"
    "setParameterName\0name\0setShaderVariableName\0"
    "setBindingType\0type\0Uniform\0Attribute\0"
    "StandardUniform\0FragmentOutput\0"
    "UniformBufferObject\0ShaderStorageBufferObject"
};
#undef QT_MOC_LITERAL

static const uint qt_meta_data_Qt3DRender__QParameterMapping[] = {

 // content:
       7,       // revision
       0,       // classname
       0,    0, // classinfo
       6,   14, // methods
       3,   62, // properties
       1,   74, // enums/sets
       0,    0, // constructors
       0,       // flags
       3,       // signalCount

 // signals: name, argc, parameters, tag, flags
       1,    1,   44,    2, 0x06 /* Public */,
       4,    1,   47,    2, 0x06 /* Public */,
       6,    1,   50,    2, 0x06 /* Public */,

 // slots: name, argc, parameters, tag, flags
       9,    1,   53,    2, 0x0a /* Public */,
      11,    1,   56,    2, 0x0a /* Public */,
      12,    1,   59,    2, 0x0a /* Public */,

 // signals: parameters
    QMetaType::Void, QMetaType::QString,    3,
    QMetaType::Void, QMetaType::QString,    5,
    QMetaType::Void, 0x80000000 | 7,    8,

 // slots: parameters
    QMetaType::Void, QMetaType::QString,   10,
    QMetaType::Void, QMetaType::QString,   10,
    QMetaType::Void, 0x80000000 | 7,   13,

 // properties: name, type, flags
       3, QMetaType::QString, 0x00495103,
       5, QMetaType::QString, 0x00495103,
       8, 0x80000000 | 7, 0x0049510b,

 // properties: notify_signal_id
       0,
       1,
       2,

 // enums: name, flags, count, data
       7, 0x0,    6,   78,

 // enum data: key, value
      14, uint(Qt3DRender::QParameterMapping::Uniform),
      15, uint(Qt3DRender::QParameterMapping::Attribute),
      16, uint(Qt3DRender::QParameterMapping::StandardUniform),
      17, uint(Qt3DRender::QParameterMapping::FragmentOutput),
      18, uint(Qt3DRender::QParameterMapping::UniformBufferObject),
      19, uint(Qt3DRender::QParameterMapping::ShaderStorageBufferObject),

       0        // eod
};

void Qt3DRender::QParameterMapping::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        QParameterMapping *_t = static_cast<QParameterMapping *>(_o);
        Q_UNUSED(_t)
        switch (_id) {
        case 0: _t->parameterNameChanged((*reinterpret_cast< const QString(*)>(_a[1]))); break;
        case 1: _t->shaderVariableNameChanged((*reinterpret_cast< const QString(*)>(_a[1]))); break;
        case 2: _t->bindingTypeChanged((*reinterpret_cast< Binding(*)>(_a[1]))); break;
        case 3: _t->setParameterName((*reinterpret_cast< const QString(*)>(_a[1]))); break;
        case 4: _t->setShaderVariableName((*reinterpret_cast< const QString(*)>(_a[1]))); break;
        case 5: _t->setBindingType((*reinterpret_cast< Binding(*)>(_a[1]))); break;
        default: ;
        }
    } else if (_c == QMetaObject::IndexOfMethod) {
        int *result = reinterpret_cast<int *>(_a[0]);
        void **func = reinterpret_cast<void **>(_a[1]);
        {
            typedef void (QParameterMapping::*_t)(const QString & );
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&QParameterMapping::parameterNameChanged)) {
                *result = 0;
                return;
            }
        }
        {
            typedef void (QParameterMapping::*_t)(const QString & );
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&QParameterMapping::shaderVariableNameChanged)) {
                *result = 1;
                return;
            }
        }
        {
            typedef void (QParameterMapping::*_t)(Binding );
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&QParameterMapping::bindingTypeChanged)) {
                *result = 2;
                return;
            }
        }
    }
#ifndef QT_NO_PROPERTIES
    else if (_c == QMetaObject::ReadProperty) {
        QParameterMapping *_t = static_cast<QParameterMapping *>(_o);
        Q_UNUSED(_t)
        void *_v = _a[0];
        switch (_id) {
        case 0: *reinterpret_cast< QString*>(_v) = _t->parameterName(); break;
        case 1: *reinterpret_cast< QString*>(_v) = _t->shaderVariableName(); break;
        case 2: *reinterpret_cast< Binding*>(_v) = _t->bindingType(); break;
        default: break;
        }
    } else if (_c == QMetaObject::WriteProperty) {
        QParameterMapping *_t = static_cast<QParameterMapping *>(_o);
        Q_UNUSED(_t)
        void *_v = _a[0];
        switch (_id) {
        case 0: _t->setParameterName(*reinterpret_cast< QString*>(_v)); break;
        case 1: _t->setShaderVariableName(*reinterpret_cast< QString*>(_v)); break;
        case 2: _t->setBindingType(*reinterpret_cast< Binding*>(_v)); break;
        default: break;
        }
    } else if (_c == QMetaObject::ResetProperty) {
    }
#endif // QT_NO_PROPERTIES
}

const QMetaObject Qt3DRender::QParameterMapping::staticMetaObject = {
    { &Qt3DCore::QNode::staticMetaObject, qt_meta_stringdata_Qt3DRender__QParameterMapping.data,
      qt_meta_data_Qt3DRender__QParameterMapping,  qt_static_metacall, Q_NULLPTR, Q_NULLPTR}
};


const QMetaObject *Qt3DRender::QParameterMapping::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->dynamicMetaObject() : &staticMetaObject;
}

void *Qt3DRender::QParameterMapping::qt_metacast(const char *_clname)
{
    if (!_clname) return Q_NULLPTR;
    if (!strcmp(_clname, qt_meta_stringdata_Qt3DRender__QParameterMapping.stringdata0))
        return static_cast<void*>(const_cast< QParameterMapping*>(this));
    return Qt3DCore::QNode::qt_metacast(_clname);
}

int Qt3DRender::QParameterMapping::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = Qt3DCore::QNode::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 6)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 6;
    } else if (_c == QMetaObject::RegisterMethodArgumentMetaType) {
        if (_id < 6)
            *reinterpret_cast<int*>(_a[0]) = -1;
        _id -= 6;
    }
#ifndef QT_NO_PROPERTIES
   else if (_c == QMetaObject::ReadProperty || _c == QMetaObject::WriteProperty
            || _c == QMetaObject::ResetProperty || _c == QMetaObject::RegisterPropertyMetaType) {
        qt_static_metacall(this, _c, _id, _a);
        _id -= 3;
    } else if (_c == QMetaObject::QueryPropertyDesignable) {
        _id -= 3;
    } else if (_c == QMetaObject::QueryPropertyScriptable) {
        _id -= 3;
    } else if (_c == QMetaObject::QueryPropertyStored) {
        _id -= 3;
    } else if (_c == QMetaObject::QueryPropertyEditable) {
        _id -= 3;
    } else if (_c == QMetaObject::QueryPropertyUser) {
        _id -= 3;
    }
#endif // QT_NO_PROPERTIES
    return _id;
}

// SIGNAL 0
void Qt3DRender::QParameterMapping::parameterNameChanged(const QString & _t1)
{
    void *_a[] = { Q_NULLPTR, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 0, _a);
}

// SIGNAL 1
void Qt3DRender::QParameterMapping::shaderVariableNameChanged(const QString & _t1)
{
    void *_a[] = { Q_NULLPTR, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 1, _a);
}

// SIGNAL 2
void Qt3DRender::QParameterMapping::bindingTypeChanged(Binding _t1)
{
    void *_a[] = { Q_NULLPTR, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 2, _a);
}
QT_END_MOC_NAMESPACE
