/****************************************************************************
** Meta object code from reading C++ file 'qshaderprogram.h'
**
** Created by: The Qt Meta Object Compiler version 67 (Qt 5.6.3)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "../../materialsystem/qshaderprogram.h"
#include <QtCore/qbytearray.h>
#include <QtCore/qmetatype.h>
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'qshaderprogram.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 67
#error "This file was generated using the moc from 5.6.3. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
struct qt_meta_stringdata_Qt3DRender__QShaderProgram_t {
    QByteArrayData data[29];
    char stringdata0[604];
};
#define QT_MOC_LITERAL(idx, ofs, len) \
    Q_STATIC_BYTE_ARRAY_DATA_HEADER_INITIALIZER_WITH_OFFSET(len, \
    qptrdiff(offsetof(qt_meta_stringdata_Qt3DRender__QShaderProgram_t, stringdata0) + ofs \
        - idx * sizeof(QByteArrayData)) \
    )
static const qt_meta_stringdata_Qt3DRender__QShaderProgram_t qt_meta_stringdata_Qt3DRender__QShaderProgram = {
    {
QT_MOC_LITERAL(0, 0, 26), // "Qt3DRender::QShaderProgram"
QT_MOC_LITERAL(1, 27, 23), // "vertexShaderCodeChanged"
QT_MOC_LITERAL(2, 51, 0), // ""
QT_MOC_LITERAL(3, 52, 16), // "vertexShaderCode"
QT_MOC_LITERAL(4, 69, 36), // "tessellationControlShaderCode..."
QT_MOC_LITERAL(5, 106, 29), // "tessellationControlShaderCode"
QT_MOC_LITERAL(6, 136, 39), // "tessellationEvaluationShaderC..."
QT_MOC_LITERAL(7, 176, 32), // "tessellationEvaluationShaderCode"
QT_MOC_LITERAL(8, 209, 25), // "geometryShaderCodeChanged"
QT_MOC_LITERAL(9, 235, 18), // "geometryShaderCode"
QT_MOC_LITERAL(10, 254, 25), // "fragmentShaderCodeChanged"
QT_MOC_LITERAL(11, 280, 18), // "fragmentShaderCode"
QT_MOC_LITERAL(12, 299, 24), // "computeShaderCodeChanged"
QT_MOC_LITERAL(13, 324, 17), // "computeShaderCode"
QT_MOC_LITERAL(14, 342, 19), // "setVertexShaderCode"
QT_MOC_LITERAL(15, 362, 32), // "setTessellationControlShaderCode"
QT_MOC_LITERAL(16, 395, 35), // "setTessellationEvaluationShad..."
QT_MOC_LITERAL(17, 431, 21), // "setGeometryShaderCode"
QT_MOC_LITERAL(18, 453, 21), // "setFragmentShaderCode"
QT_MOC_LITERAL(19, 475, 20), // "setComputeShaderCode"
QT_MOC_LITERAL(20, 496, 10), // "loadSource"
QT_MOC_LITERAL(21, 507, 9), // "sourceUrl"
QT_MOC_LITERAL(22, 517, 10), // "ShaderType"
QT_MOC_LITERAL(23, 528, 6), // "Vertex"
QT_MOC_LITERAL(24, 535, 8), // "Fragment"
QT_MOC_LITERAL(25, 544, 19), // "TessellationControl"
QT_MOC_LITERAL(26, 564, 22), // "TessellationEvaluation"
QT_MOC_LITERAL(27, 587, 8), // "Geometry"
QT_MOC_LITERAL(28, 596, 7) // "Compute"

    },
    "Qt3DRender::QShaderProgram\0"
    "vertexShaderCodeChanged\0\0vertexShaderCode\0"
    "tessellationControlShaderCodeChanged\0"
    "tessellationControlShaderCode\0"
    "tessellationEvaluationShaderCodeChanged\0"
    "tessellationEvaluationShaderCode\0"
    "geometryShaderCodeChanged\0geometryShaderCode\0"
    "fragmentShaderCodeChanged\0fragmentShaderCode\0"
    "computeShaderCodeChanged\0computeShaderCode\0"
    "setVertexShaderCode\0"
    "setTessellationControlShaderCode\0"
    "setTessellationEvaluationShaderCode\0"
    "setGeometryShaderCode\0setFragmentShaderCode\0"
    "setComputeShaderCode\0loadSource\0"
    "sourceUrl\0ShaderType\0Vertex\0Fragment\0"
    "TessellationControl\0TessellationEvaluation\0"
    "Geometry\0Compute"
};
#undef QT_MOC_LITERAL

static const uint qt_meta_data_Qt3DRender__QShaderProgram[] = {

 // content:
       7,       // revision
       0,       // classname
       0,    0, // classinfo
      13,   14, // methods
       6,  118, // properties
       1,  142, // enums/sets
       0,    0, // constructors
       0,       // flags
       6,       // signalCount

 // signals: name, argc, parameters, tag, flags
       1,    1,   79,    2, 0x06 /* Public */,
       4,    1,   82,    2, 0x06 /* Public */,
       6,    1,   85,    2, 0x06 /* Public */,
       8,    1,   88,    2, 0x06 /* Public */,
      10,    1,   91,    2, 0x06 /* Public */,
      12,    1,   94,    2, 0x06 /* Public */,

 // slots: name, argc, parameters, tag, flags
      14,    1,   97,    2, 0x0a /* Public */,
      15,    1,  100,    2, 0x0a /* Public */,
      16,    1,  103,    2, 0x0a /* Public */,
      17,    1,  106,    2, 0x0a /* Public */,
      18,    1,  109,    2, 0x0a /* Public */,
      19,    1,  112,    2, 0x0a /* Public */,

 // methods: name, argc, parameters, tag, flags
      20,    1,  115,    2, 0x02 /* Public */,

 // signals: parameters
    QMetaType::Void, QMetaType::QByteArray,    3,
    QMetaType::Void, QMetaType::QByteArray,    5,
    QMetaType::Void, QMetaType::QByteArray,    7,
    QMetaType::Void, QMetaType::QByteArray,    9,
    QMetaType::Void, QMetaType::QByteArray,   11,
    QMetaType::Void, QMetaType::QByteArray,   13,

 // slots: parameters
    QMetaType::Void, QMetaType::QByteArray,    3,
    QMetaType::Void, QMetaType::QByteArray,    5,
    QMetaType::Void, QMetaType::QByteArray,    7,
    QMetaType::Void, QMetaType::QByteArray,    9,
    QMetaType::Void, QMetaType::QByteArray,   11,
    QMetaType::Void, QMetaType::QByteArray,   13,

 // methods: parameters
    QMetaType::QByteArray, QMetaType::QUrl,   21,

 // properties: name, type, flags
       3, QMetaType::QByteArray, 0x00495103,
       5, QMetaType::QByteArray, 0x00495103,
       7, QMetaType::QByteArray, 0x00495103,
       9, QMetaType::QByteArray, 0x00495103,
      11, QMetaType::QByteArray, 0x00495103,
      13, QMetaType::QByteArray, 0x00495103,

 // properties: notify_signal_id
       0,
       1,
       2,
       3,
       4,
       5,

 // enums: name, flags, count, data
      22, 0x0,    6,  146,

 // enum data: key, value
      23, uint(Qt3DRender::QShaderProgram::Vertex),
      24, uint(Qt3DRender::QShaderProgram::Fragment),
      25, uint(Qt3DRender::QShaderProgram::TessellationControl),
      26, uint(Qt3DRender::QShaderProgram::TessellationEvaluation),
      27, uint(Qt3DRender::QShaderProgram::Geometry),
      28, uint(Qt3DRender::QShaderProgram::Compute),

       0        // eod
};

void Qt3DRender::QShaderProgram::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        QShaderProgram *_t = static_cast<QShaderProgram *>(_o);
        Q_UNUSED(_t)
        switch (_id) {
        case 0: _t->vertexShaderCodeChanged((*reinterpret_cast< const QByteArray(*)>(_a[1]))); break;
        case 1: _t->tessellationControlShaderCodeChanged((*reinterpret_cast< const QByteArray(*)>(_a[1]))); break;
        case 2: _t->tessellationEvaluationShaderCodeChanged((*reinterpret_cast< const QByteArray(*)>(_a[1]))); break;
        case 3: _t->geometryShaderCodeChanged((*reinterpret_cast< const QByteArray(*)>(_a[1]))); break;
        case 4: _t->fragmentShaderCodeChanged((*reinterpret_cast< const QByteArray(*)>(_a[1]))); break;
        case 5: _t->computeShaderCodeChanged((*reinterpret_cast< const QByteArray(*)>(_a[1]))); break;
        case 6: _t->setVertexShaderCode((*reinterpret_cast< const QByteArray(*)>(_a[1]))); break;
        case 7: _t->setTessellationControlShaderCode((*reinterpret_cast< const QByteArray(*)>(_a[1]))); break;
        case 8: _t->setTessellationEvaluationShaderCode((*reinterpret_cast< const QByteArray(*)>(_a[1]))); break;
        case 9: _t->setGeometryShaderCode((*reinterpret_cast< const QByteArray(*)>(_a[1]))); break;
        case 10: _t->setFragmentShaderCode((*reinterpret_cast< const QByteArray(*)>(_a[1]))); break;
        case 11: _t->setComputeShaderCode((*reinterpret_cast< const QByteArray(*)>(_a[1]))); break;
        case 12: { QByteArray _r = _t->loadSource((*reinterpret_cast< const QUrl(*)>(_a[1])));
            if (_a[0]) *reinterpret_cast< QByteArray*>(_a[0]) = _r; }  break;
        default: ;
        }
    } else if (_c == QMetaObject::IndexOfMethod) {
        int *result = reinterpret_cast<int *>(_a[0]);
        void **func = reinterpret_cast<void **>(_a[1]);
        {
            typedef void (QShaderProgram::*_t)(const QByteArray & );
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&QShaderProgram::vertexShaderCodeChanged)) {
                *result = 0;
                return;
            }
        }
        {
            typedef void (QShaderProgram::*_t)(const QByteArray & );
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&QShaderProgram::tessellationControlShaderCodeChanged)) {
                *result = 1;
                return;
            }
        }
        {
            typedef void (QShaderProgram::*_t)(const QByteArray & );
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&QShaderProgram::tessellationEvaluationShaderCodeChanged)) {
                *result = 2;
                return;
            }
        }
        {
            typedef void (QShaderProgram::*_t)(const QByteArray & );
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&QShaderProgram::geometryShaderCodeChanged)) {
                *result = 3;
                return;
            }
        }
        {
            typedef void (QShaderProgram::*_t)(const QByteArray & );
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&QShaderProgram::fragmentShaderCodeChanged)) {
                *result = 4;
                return;
            }
        }
        {
            typedef void (QShaderProgram::*_t)(const QByteArray & );
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&QShaderProgram::computeShaderCodeChanged)) {
                *result = 5;
                return;
            }
        }
    }
#ifndef QT_NO_PROPERTIES
    else if (_c == QMetaObject::ReadProperty) {
        QShaderProgram *_t = static_cast<QShaderProgram *>(_o);
        Q_UNUSED(_t)
        void *_v = _a[0];
        switch (_id) {
        case 0: *reinterpret_cast< QByteArray*>(_v) = _t->vertexShaderCode(); break;
        case 1: *reinterpret_cast< QByteArray*>(_v) = _t->tessellationControlShaderCode(); break;
        case 2: *reinterpret_cast< QByteArray*>(_v) = _t->tessellationEvaluationShaderCode(); break;
        case 3: *reinterpret_cast< QByteArray*>(_v) = _t->geometryShaderCode(); break;
        case 4: *reinterpret_cast< QByteArray*>(_v) = _t->fragmentShaderCode(); break;
        case 5: *reinterpret_cast< QByteArray*>(_v) = _t->computeShaderCode(); break;
        default: break;
        }
    } else if (_c == QMetaObject::WriteProperty) {
        QShaderProgram *_t = static_cast<QShaderProgram *>(_o);
        Q_UNUSED(_t)
        void *_v = _a[0];
        switch (_id) {
        case 0: _t->setVertexShaderCode(*reinterpret_cast< QByteArray*>(_v)); break;
        case 1: _t->setTessellationControlShaderCode(*reinterpret_cast< QByteArray*>(_v)); break;
        case 2: _t->setTessellationEvaluationShaderCode(*reinterpret_cast< QByteArray*>(_v)); break;
        case 3: _t->setGeometryShaderCode(*reinterpret_cast< QByteArray*>(_v)); break;
        case 4: _t->setFragmentShaderCode(*reinterpret_cast< QByteArray*>(_v)); break;
        case 5: _t->setComputeShaderCode(*reinterpret_cast< QByteArray*>(_v)); break;
        default: break;
        }
    } else if (_c == QMetaObject::ResetProperty) {
    }
#endif // QT_NO_PROPERTIES
}

const QMetaObject Qt3DRender::QShaderProgram::staticMetaObject = {
    { &Qt3DCore::QNode::staticMetaObject, qt_meta_stringdata_Qt3DRender__QShaderProgram.data,
      qt_meta_data_Qt3DRender__QShaderProgram,  qt_static_metacall, Q_NULLPTR, Q_NULLPTR}
};


const QMetaObject *Qt3DRender::QShaderProgram::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->dynamicMetaObject() : &staticMetaObject;
}

void *Qt3DRender::QShaderProgram::qt_metacast(const char *_clname)
{
    if (!_clname) return Q_NULLPTR;
    if (!strcmp(_clname, qt_meta_stringdata_Qt3DRender__QShaderProgram.stringdata0))
        return static_cast<void*>(const_cast< QShaderProgram*>(this));
    return Qt3DCore::QNode::qt_metacast(_clname);
}

int Qt3DRender::QShaderProgram::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = Qt3DCore::QNode::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 13)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 13;
    } else if (_c == QMetaObject::RegisterMethodArgumentMetaType) {
        if (_id < 13)
            *reinterpret_cast<int*>(_a[0]) = -1;
        _id -= 13;
    }
#ifndef QT_NO_PROPERTIES
   else if (_c == QMetaObject::ReadProperty || _c == QMetaObject::WriteProperty
            || _c == QMetaObject::ResetProperty || _c == QMetaObject::RegisterPropertyMetaType) {
        qt_static_metacall(this, _c, _id, _a);
        _id -= 6;
    } else if (_c == QMetaObject::QueryPropertyDesignable) {
        _id -= 6;
    } else if (_c == QMetaObject::QueryPropertyScriptable) {
        _id -= 6;
    } else if (_c == QMetaObject::QueryPropertyStored) {
        _id -= 6;
    } else if (_c == QMetaObject::QueryPropertyEditable) {
        _id -= 6;
    } else if (_c == QMetaObject::QueryPropertyUser) {
        _id -= 6;
    }
#endif // QT_NO_PROPERTIES
    return _id;
}

// SIGNAL 0
void Qt3DRender::QShaderProgram::vertexShaderCodeChanged(const QByteArray & _t1)
{
    void *_a[] = { Q_NULLPTR, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 0, _a);
}

// SIGNAL 1
void Qt3DRender::QShaderProgram::tessellationControlShaderCodeChanged(const QByteArray & _t1)
{
    void *_a[] = { Q_NULLPTR, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 1, _a);
}

// SIGNAL 2
void Qt3DRender::QShaderProgram::tessellationEvaluationShaderCodeChanged(const QByteArray & _t1)
{
    void *_a[] = { Q_NULLPTR, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 2, _a);
}

// SIGNAL 3
void Qt3DRender::QShaderProgram::geometryShaderCodeChanged(const QByteArray & _t1)
{
    void *_a[] = { Q_NULLPTR, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 3, _a);
}

// SIGNAL 4
void Qt3DRender::QShaderProgram::fragmentShaderCodeChanged(const QByteArray & _t1)
{
    void *_a[] = { Q_NULLPTR, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 4, _a);
}

// SIGNAL 5
void Qt3DRender::QShaderProgram::computeShaderCodeChanged(const QByteArray & _t1)
{
    void *_a[] = { Q_NULLPTR, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 5, _a);
}
QT_END_MOC_NAMESPACE
