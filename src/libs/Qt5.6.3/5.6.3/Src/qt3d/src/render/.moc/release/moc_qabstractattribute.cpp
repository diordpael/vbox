/****************************************************************************
** Meta object code from reading C++ file 'qabstractattribute.h'
**
** Created by: The Qt Meta Object Compiler version 67 (Qt 5.6.3)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "../../geometry/qabstractattribute.h"
#include <QtCore/qbytearray.h>
#include <QtCore/qmetatype.h>
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'qabstractattribute.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 67
#error "This file was generated using the moc from 5.6.3. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
struct qt_meta_stringdata_Qt3DRender__QAbstractAttribute_t {
    QByteArrayData data[46];
    char stringdata0[549];
};
#define QT_MOC_LITERAL(idx, ofs, len) \
    Q_STATIC_BYTE_ARRAY_DATA_HEADER_INITIALIZER_WITH_OFFSET(len, \
    qptrdiff(offsetof(qt_meta_stringdata_Qt3DRender__QAbstractAttribute_t, stringdata0) + ofs \
        - idx * sizeof(QByteArrayData)) \
    )
static const qt_meta_stringdata_Qt3DRender__QAbstractAttribute_t qt_meta_stringdata_Qt3DRender__QAbstractAttribute = {
    {
QT_MOC_LITERAL(0, 0, 30), // "Qt3DRender::QAbstractAttribute"
QT_MOC_LITERAL(1, 31, 13), // "bufferChanged"
QT_MOC_LITERAL(2, 45, 0), // ""
QT_MOC_LITERAL(3, 46, 16), // "QAbstractBuffer*"
QT_MOC_LITERAL(4, 63, 6), // "buffer"
QT_MOC_LITERAL(5, 70, 11), // "nameChanged"
QT_MOC_LITERAL(6, 82, 4), // "name"
QT_MOC_LITERAL(7, 87, 15), // "dataTypeChanged"
QT_MOC_LITERAL(8, 103, 8), // "DataType"
QT_MOC_LITERAL(9, 112, 8), // "dataType"
QT_MOC_LITERAL(10, 121, 15), // "dataSizeChanged"
QT_MOC_LITERAL(11, 137, 8), // "dataSize"
QT_MOC_LITERAL(12, 146, 12), // "countChanged"
QT_MOC_LITERAL(13, 159, 5), // "count"
QT_MOC_LITERAL(14, 165, 17), // "byteStrideChanged"
QT_MOC_LITERAL(15, 183, 10), // "byteStride"
QT_MOC_LITERAL(16, 194, 17), // "byteOffsetChanged"
QT_MOC_LITERAL(17, 212, 10), // "byteOffset"
QT_MOC_LITERAL(18, 223, 14), // "divisorChanged"
QT_MOC_LITERAL(19, 238, 7), // "divisor"
QT_MOC_LITERAL(20, 246, 20), // "attributeTypeChanged"
QT_MOC_LITERAL(21, 267, 13), // "AttributeType"
QT_MOC_LITERAL(22, 281, 13), // "attributeType"
QT_MOC_LITERAL(23, 295, 9), // "setBuffer"
QT_MOC_LITERAL(24, 305, 7), // "setName"
QT_MOC_LITERAL(25, 313, 11), // "setDataType"
QT_MOC_LITERAL(26, 325, 4), // "type"
QT_MOC_LITERAL(27, 330, 11), // "setDataSize"
QT_MOC_LITERAL(28, 342, 4), // "size"
QT_MOC_LITERAL(29, 347, 8), // "setCount"
QT_MOC_LITERAL(30, 356, 13), // "setByteStride"
QT_MOC_LITERAL(31, 370, 13), // "setByteOffset"
QT_MOC_LITERAL(32, 384, 10), // "setDivisor"
QT_MOC_LITERAL(33, 395, 16), // "setAttributeType"
QT_MOC_LITERAL(34, 412, 28), // "Qt3DRender::QAbstractBuffer*"
QT_MOC_LITERAL(35, 441, 15), // "VertexAttribute"
QT_MOC_LITERAL(36, 457, 14), // "IndexAttribute"
QT_MOC_LITERAL(37, 472, 4), // "Byte"
QT_MOC_LITERAL(38, 477, 12), // "UnsignedByte"
QT_MOC_LITERAL(39, 490, 5), // "Short"
QT_MOC_LITERAL(40, 496, 13), // "UnsignedShort"
QT_MOC_LITERAL(41, 510, 3), // "Int"
QT_MOC_LITERAL(42, 514, 11), // "UnsignedInt"
QT_MOC_LITERAL(43, 526, 9), // "HalfFloat"
QT_MOC_LITERAL(44, 536, 5), // "Float"
QT_MOC_LITERAL(45, 542, 6) // "Double"

    },
    "Qt3DRender::QAbstractAttribute\0"
    "bufferChanged\0\0QAbstractBuffer*\0buffer\0"
    "nameChanged\0name\0dataTypeChanged\0"
    "DataType\0dataType\0dataSizeChanged\0"
    "dataSize\0countChanged\0count\0"
    "byteStrideChanged\0byteStride\0"
    "byteOffsetChanged\0byteOffset\0"
    "divisorChanged\0divisor\0attributeTypeChanged\0"
    "AttributeType\0attributeType\0setBuffer\0"
    "setName\0setDataType\0type\0setDataSize\0"
    "size\0setCount\0setByteStride\0setByteOffset\0"
    "setDivisor\0setAttributeType\0"
    "Qt3DRender::QAbstractBuffer*\0"
    "VertexAttribute\0IndexAttribute\0Byte\0"
    "UnsignedByte\0Short\0UnsignedShort\0Int\0"
    "UnsignedInt\0HalfFloat\0Float\0Double"
};
#undef QT_MOC_LITERAL

static const uint qt_meta_data_Qt3DRender__QAbstractAttribute[] = {

 // content:
       7,       // revision
       0,       // classname
       0,    0, // classinfo
      18,   14, // methods
       9,  158, // properties
       2,  194, // enums/sets
       0,    0, // constructors
       0,       // flags
       9,       // signalCount

 // signals: name, argc, parameters, tag, flags
       1,    1,  104,    2, 0x06 /* Public */,
       5,    1,  107,    2, 0x06 /* Public */,
       7,    1,  110,    2, 0x06 /* Public */,
      10,    1,  113,    2, 0x06 /* Public */,
      12,    1,  116,    2, 0x06 /* Public */,
      14,    1,  119,    2, 0x06 /* Public */,
      16,    1,  122,    2, 0x06 /* Public */,
      18,    1,  125,    2, 0x06 /* Public */,
      20,    1,  128,    2, 0x06 /* Public */,

 // slots: name, argc, parameters, tag, flags
      23,    1,  131,    2, 0x0a /* Public */,
      24,    1,  134,    2, 0x0a /* Public */,
      25,    1,  137,    2, 0x0a /* Public */,
      27,    1,  140,    2, 0x0a /* Public */,
      29,    1,  143,    2, 0x0a /* Public */,
      30,    1,  146,    2, 0x0a /* Public */,
      31,    1,  149,    2, 0x0a /* Public */,
      32,    1,  152,    2, 0x0a /* Public */,
      33,    1,  155,    2, 0x0a /* Public */,

 // signals: parameters
    QMetaType::Void, 0x80000000 | 3,    4,
    QMetaType::Void, QMetaType::QString,    6,
    QMetaType::Void, 0x80000000 | 8,    9,
    QMetaType::Void, QMetaType::UInt,   11,
    QMetaType::Void, QMetaType::UInt,   13,
    QMetaType::Void, QMetaType::UInt,   15,
    QMetaType::Void, QMetaType::UInt,   17,
    QMetaType::Void, QMetaType::UInt,   19,
    QMetaType::Void, 0x80000000 | 21,   22,

 // slots: parameters
    QMetaType::Void, 0x80000000 | 3,    4,
    QMetaType::Void, QMetaType::QString,    6,
    QMetaType::Void, 0x80000000 | 8,   26,
    QMetaType::Void, QMetaType::UInt,   28,
    QMetaType::Void, QMetaType::UInt,   13,
    QMetaType::Void, QMetaType::UInt,   15,
    QMetaType::Void, QMetaType::UInt,   17,
    QMetaType::Void, QMetaType::UInt,   19,
    QMetaType::Void, 0x80000000 | 21,   22,

 // properties: name, type, flags
       4, 0x80000000 | 34, 0x0049510b,
       6, QMetaType::QString, 0x00495103,
       9, 0x80000000 | 8, 0x0049510b,
      11, QMetaType::UInt, 0x00495103,
      13, QMetaType::UInt, 0x00495103,
      15, QMetaType::UInt, 0x00495103,
      17, QMetaType::UInt, 0x00495103,
      19, QMetaType::UInt, 0x00495103,
      22, 0x80000000 | 21, 0x0049510b,

 // properties: notify_signal_id
       0,
       1,
       2,
       3,
       4,
       5,
       6,
       7,
       8,

 // enums: name, flags, count, data
      21, 0x0,    2,  202,
       8, 0x0,    9,  206,

 // enum data: key, value
      35, uint(Qt3DRender::QAbstractAttribute::VertexAttribute),
      36, uint(Qt3DRender::QAbstractAttribute::IndexAttribute),
      37, uint(Qt3DRender::QAbstractAttribute::Byte),
      38, uint(Qt3DRender::QAbstractAttribute::UnsignedByte),
      39, uint(Qt3DRender::QAbstractAttribute::Short),
      40, uint(Qt3DRender::QAbstractAttribute::UnsignedShort),
      41, uint(Qt3DRender::QAbstractAttribute::Int),
      42, uint(Qt3DRender::QAbstractAttribute::UnsignedInt),
      43, uint(Qt3DRender::QAbstractAttribute::HalfFloat),
      44, uint(Qt3DRender::QAbstractAttribute::Float),
      45, uint(Qt3DRender::QAbstractAttribute::Double),

       0        // eod
};

void Qt3DRender::QAbstractAttribute::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        QAbstractAttribute *_t = static_cast<QAbstractAttribute *>(_o);
        Q_UNUSED(_t)
        switch (_id) {
        case 0: _t->bufferChanged((*reinterpret_cast< QAbstractBuffer*(*)>(_a[1]))); break;
        case 1: _t->nameChanged((*reinterpret_cast< const QString(*)>(_a[1]))); break;
        case 2: _t->dataTypeChanged((*reinterpret_cast< DataType(*)>(_a[1]))); break;
        case 3: _t->dataSizeChanged((*reinterpret_cast< uint(*)>(_a[1]))); break;
        case 4: _t->countChanged((*reinterpret_cast< uint(*)>(_a[1]))); break;
        case 5: _t->byteStrideChanged((*reinterpret_cast< uint(*)>(_a[1]))); break;
        case 6: _t->byteOffsetChanged((*reinterpret_cast< uint(*)>(_a[1]))); break;
        case 7: _t->divisorChanged((*reinterpret_cast< uint(*)>(_a[1]))); break;
        case 8: _t->attributeTypeChanged((*reinterpret_cast< AttributeType(*)>(_a[1]))); break;
        case 9: _t->setBuffer((*reinterpret_cast< QAbstractBuffer*(*)>(_a[1]))); break;
        case 10: _t->setName((*reinterpret_cast< const QString(*)>(_a[1]))); break;
        case 11: _t->setDataType((*reinterpret_cast< DataType(*)>(_a[1]))); break;
        case 12: _t->setDataSize((*reinterpret_cast< uint(*)>(_a[1]))); break;
        case 13: _t->setCount((*reinterpret_cast< uint(*)>(_a[1]))); break;
        case 14: _t->setByteStride((*reinterpret_cast< uint(*)>(_a[1]))); break;
        case 15: _t->setByteOffset((*reinterpret_cast< uint(*)>(_a[1]))); break;
        case 16: _t->setDivisor((*reinterpret_cast< uint(*)>(_a[1]))); break;
        case 17: _t->setAttributeType((*reinterpret_cast< AttributeType(*)>(_a[1]))); break;
        default: ;
        }
    } else if (_c == QMetaObject::IndexOfMethod) {
        int *result = reinterpret_cast<int *>(_a[0]);
        void **func = reinterpret_cast<void **>(_a[1]);
        {
            typedef void (QAbstractAttribute::*_t)(QAbstractBuffer * );
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&QAbstractAttribute::bufferChanged)) {
                *result = 0;
                return;
            }
        }
        {
            typedef void (QAbstractAttribute::*_t)(const QString & );
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&QAbstractAttribute::nameChanged)) {
                *result = 1;
                return;
            }
        }
        {
            typedef void (QAbstractAttribute::*_t)(DataType );
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&QAbstractAttribute::dataTypeChanged)) {
                *result = 2;
                return;
            }
        }
        {
            typedef void (QAbstractAttribute::*_t)(uint );
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&QAbstractAttribute::dataSizeChanged)) {
                *result = 3;
                return;
            }
        }
        {
            typedef void (QAbstractAttribute::*_t)(uint );
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&QAbstractAttribute::countChanged)) {
                *result = 4;
                return;
            }
        }
        {
            typedef void (QAbstractAttribute::*_t)(uint );
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&QAbstractAttribute::byteStrideChanged)) {
                *result = 5;
                return;
            }
        }
        {
            typedef void (QAbstractAttribute::*_t)(uint );
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&QAbstractAttribute::byteOffsetChanged)) {
                *result = 6;
                return;
            }
        }
        {
            typedef void (QAbstractAttribute::*_t)(uint );
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&QAbstractAttribute::divisorChanged)) {
                *result = 7;
                return;
            }
        }
        {
            typedef void (QAbstractAttribute::*_t)(AttributeType );
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&QAbstractAttribute::attributeTypeChanged)) {
                *result = 8;
                return;
            }
        }
    }
#ifndef QT_NO_PROPERTIES
    else if (_c == QMetaObject::ReadProperty) {
        QAbstractAttribute *_t = static_cast<QAbstractAttribute *>(_o);
        Q_UNUSED(_t)
        void *_v = _a[0];
        switch (_id) {
        case 0: *reinterpret_cast< Qt3DRender::QAbstractBuffer**>(_v) = _t->buffer(); break;
        case 1: *reinterpret_cast< QString*>(_v) = _t->name(); break;
        case 2: *reinterpret_cast< DataType*>(_v) = _t->dataType(); break;
        case 3: *reinterpret_cast< uint*>(_v) = _t->dataSize(); break;
        case 4: *reinterpret_cast< uint*>(_v) = _t->count(); break;
        case 5: *reinterpret_cast< uint*>(_v) = _t->byteStride(); break;
        case 6: *reinterpret_cast< uint*>(_v) = _t->byteOffset(); break;
        case 7: *reinterpret_cast< uint*>(_v) = _t->divisor(); break;
        case 8: *reinterpret_cast< AttributeType*>(_v) = _t->attributeType(); break;
        default: break;
        }
    } else if (_c == QMetaObject::WriteProperty) {
        QAbstractAttribute *_t = static_cast<QAbstractAttribute *>(_o);
        Q_UNUSED(_t)
        void *_v = _a[0];
        switch (_id) {
        case 0: _t->setBuffer(*reinterpret_cast< Qt3DRender::QAbstractBuffer**>(_v)); break;
        case 1: _t->setName(*reinterpret_cast< QString*>(_v)); break;
        case 2: _t->setDataType(*reinterpret_cast< DataType*>(_v)); break;
        case 3: _t->setDataSize(*reinterpret_cast< uint*>(_v)); break;
        case 4: _t->setCount(*reinterpret_cast< uint*>(_v)); break;
        case 5: _t->setByteStride(*reinterpret_cast< uint*>(_v)); break;
        case 6: _t->setByteOffset(*reinterpret_cast< uint*>(_v)); break;
        case 7: _t->setDivisor(*reinterpret_cast< uint*>(_v)); break;
        case 8: _t->setAttributeType(*reinterpret_cast< AttributeType*>(_v)); break;
        default: break;
        }
    } else if (_c == QMetaObject::ResetProperty) {
    }
#endif // QT_NO_PROPERTIES
}

const QMetaObject Qt3DRender::QAbstractAttribute::staticMetaObject = {
    { &Qt3DCore::QNode::staticMetaObject, qt_meta_stringdata_Qt3DRender__QAbstractAttribute.data,
      qt_meta_data_Qt3DRender__QAbstractAttribute,  qt_static_metacall, Q_NULLPTR, Q_NULLPTR}
};


const QMetaObject *Qt3DRender::QAbstractAttribute::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->dynamicMetaObject() : &staticMetaObject;
}

void *Qt3DRender::QAbstractAttribute::qt_metacast(const char *_clname)
{
    if (!_clname) return Q_NULLPTR;
    if (!strcmp(_clname, qt_meta_stringdata_Qt3DRender__QAbstractAttribute.stringdata0))
        return static_cast<void*>(const_cast< QAbstractAttribute*>(this));
    return Qt3DCore::QNode::qt_metacast(_clname);
}

int Qt3DRender::QAbstractAttribute::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = Qt3DCore::QNode::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 18)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 18;
    } else if (_c == QMetaObject::RegisterMethodArgumentMetaType) {
        if (_id < 18)
            *reinterpret_cast<int*>(_a[0]) = -1;
        _id -= 18;
    }
#ifndef QT_NO_PROPERTIES
   else if (_c == QMetaObject::ReadProperty || _c == QMetaObject::WriteProperty
            || _c == QMetaObject::ResetProperty || _c == QMetaObject::RegisterPropertyMetaType) {
        qt_static_metacall(this, _c, _id, _a);
        _id -= 9;
    } else if (_c == QMetaObject::QueryPropertyDesignable) {
        _id -= 9;
    } else if (_c == QMetaObject::QueryPropertyScriptable) {
        _id -= 9;
    } else if (_c == QMetaObject::QueryPropertyStored) {
        _id -= 9;
    } else if (_c == QMetaObject::QueryPropertyEditable) {
        _id -= 9;
    } else if (_c == QMetaObject::QueryPropertyUser) {
        _id -= 9;
    }
#endif // QT_NO_PROPERTIES
    return _id;
}

// SIGNAL 0
void Qt3DRender::QAbstractAttribute::bufferChanged(QAbstractBuffer * _t1)
{
    void *_a[] = { Q_NULLPTR, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 0, _a);
}

// SIGNAL 1
void Qt3DRender::QAbstractAttribute::nameChanged(const QString & _t1)
{
    void *_a[] = { Q_NULLPTR, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 1, _a);
}

// SIGNAL 2
void Qt3DRender::QAbstractAttribute::dataTypeChanged(DataType _t1)
{
    void *_a[] = { Q_NULLPTR, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 2, _a);
}

// SIGNAL 3
void Qt3DRender::QAbstractAttribute::dataSizeChanged(uint _t1)
{
    void *_a[] = { Q_NULLPTR, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 3, _a);
}

// SIGNAL 4
void Qt3DRender::QAbstractAttribute::countChanged(uint _t1)
{
    void *_a[] = { Q_NULLPTR, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 4, _a);
}

// SIGNAL 5
void Qt3DRender::QAbstractAttribute::byteStrideChanged(uint _t1)
{
    void *_a[] = { Q_NULLPTR, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 5, _a);
}

// SIGNAL 6
void Qt3DRender::QAbstractAttribute::byteOffsetChanged(uint _t1)
{
    void *_a[] = { Q_NULLPTR, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 6, _a);
}

// SIGNAL 7
void Qt3DRender::QAbstractAttribute::divisorChanged(uint _t1)
{
    void *_a[] = { Q_NULLPTR, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 7, _a);
}

// SIGNAL 8
void Qt3DRender::QAbstractAttribute::attributeTypeChanged(AttributeType _t1)
{
    void *_a[] = { Q_NULLPTR, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 8, _a);
}
QT_END_MOC_NAMESPACE
