/****************************************************************************
** Meta object code from reading C++ file 'qstenciltestseparate.h'
**
** Created by: The Qt Meta Object Compiler version 67 (Qt 5.6.3)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "../../renderstates/qstenciltestseparate.h"
#include <QtCore/qbytearray.h>
#include <QtCore/qmetatype.h>
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'qstenciltestseparate.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 67
#error "This file was generated using the moc from 5.6.3. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
struct qt_meta_stringdata_Qt3DRender__QStencilTestSeparate_t {
    QByteArrayData data[26];
    char stringdata0[251];
};
#define QT_MOC_LITERAL(idx, ofs, len) \
    Q_STATIC_BYTE_ARRAY_DATA_HEADER_INITIALIZER_WITH_OFFSET(len, \
    qptrdiff(offsetof(qt_meta_stringdata_Qt3DRender__QStencilTestSeparate_t, stringdata0) + ofs \
        - idx * sizeof(QByteArrayData)) \
    )
static const qt_meta_stringdata_Qt3DRender__QStencilTestSeparate_t qt_meta_stringdata_Qt3DRender__QStencilTestSeparate = {
    {
QT_MOC_LITERAL(0, 0, 32), // "Qt3DRender::QStencilTestSeparate"
QT_MOC_LITERAL(1, 33, 11), // "maskChanged"
QT_MOC_LITERAL(2, 45, 0), // ""
QT_MOC_LITERAL(3, 46, 4), // "mask"
QT_MOC_LITERAL(4, 51, 11), // "funcChanged"
QT_MOC_LITERAL(5, 63, 11), // "StencilFunc"
QT_MOC_LITERAL(6, 75, 4), // "func"
QT_MOC_LITERAL(7, 80, 10), // "refChanged"
QT_MOC_LITERAL(8, 91, 3), // "ref"
QT_MOC_LITERAL(9, 95, 15), // "faceModeChanged"
QT_MOC_LITERAL(10, 111, 15), // "StencilFaceMode"
QT_MOC_LITERAL(11, 127, 8), // "faceMode"
QT_MOC_LITERAL(12, 136, 7), // "setMask"
QT_MOC_LITERAL(13, 144, 6), // "setRef"
QT_MOC_LITERAL(14, 151, 7), // "setFunc"
QT_MOC_LITERAL(15, 159, 5), // "Front"
QT_MOC_LITERAL(16, 165, 4), // "Back"
QT_MOC_LITERAL(17, 170, 12), // "FrontAndBack"
QT_MOC_LITERAL(18, 183, 5), // "Never"
QT_MOC_LITERAL(19, 189, 6), // "Always"
QT_MOC_LITERAL(20, 196, 4), // "Less"
QT_MOC_LITERAL(21, 201, 11), // "LessOrEqual"
QT_MOC_LITERAL(22, 213, 5), // "Equal"
QT_MOC_LITERAL(23, 219, 14), // "GreaterOrEqual"
QT_MOC_LITERAL(24, 234, 7), // "Greater"
QT_MOC_LITERAL(25, 242, 8) // "NotEqual"

    },
    "Qt3DRender::QStencilTestSeparate\0"
    "maskChanged\0\0mask\0funcChanged\0StencilFunc\0"
    "func\0refChanged\0ref\0faceModeChanged\0"
    "StencilFaceMode\0faceMode\0setMask\0"
    "setRef\0setFunc\0Front\0Back\0FrontAndBack\0"
    "Never\0Always\0Less\0LessOrEqual\0Equal\0"
    "GreaterOrEqual\0Greater\0NotEqual"
};
#undef QT_MOC_LITERAL

static const uint qt_meta_data_Qt3DRender__QStencilTestSeparate[] = {

 // content:
       7,       // revision
       0,       // classname
       0,    0, // classinfo
       7,   14, // methods
       4,   70, // properties
       2,   86, // enums/sets
       0,    0, // constructors
       0,       // flags
       4,       // signalCount

 // signals: name, argc, parameters, tag, flags
       1,    1,   49,    2, 0x06 /* Public */,
       4,    1,   52,    2, 0x06 /* Public */,
       7,    1,   55,    2, 0x06 /* Public */,
       9,    1,   58,    2, 0x06 /* Public */,

 // slots: name, argc, parameters, tag, flags
      12,    1,   61,    2, 0x0a /* Public */,
      13,    1,   64,    2, 0x0a /* Public */,
      14,    1,   67,    2, 0x0a /* Public */,

 // signals: parameters
    QMetaType::Void, QMetaType::UInt,    3,
    QMetaType::Void, 0x80000000 | 5,    6,
    QMetaType::Void, QMetaType::Int,    8,
    QMetaType::Void, 0x80000000 | 10,   11,

 // slots: parameters
    QMetaType::Void, QMetaType::UInt,    3,
    QMetaType::Void, QMetaType::Int,    8,
    QMetaType::Void, 0x80000000 | 5,    6,

 // properties: name, type, flags
      11, 0x80000000 | 10, 0x00495009,
       3, QMetaType::UInt, 0x00495103,
       8, QMetaType::Int, 0x00495103,
       6, 0x80000000 | 5, 0x0049510b,

 // properties: notify_signal_id
       3,
       0,
       2,
       1,

 // enums: name, flags, count, data
      10, 0x0,    3,   94,
       5, 0x0,    8,  100,

 // enum data: key, value
      15, uint(Qt3DRender::QStencilTestSeparate::Front),
      16, uint(Qt3DRender::QStencilTestSeparate::Back),
      17, uint(Qt3DRender::QStencilTestSeparate::FrontAndBack),
      18, uint(Qt3DRender::QStencilTestSeparate::Never),
      19, uint(Qt3DRender::QStencilTestSeparate::Always),
      20, uint(Qt3DRender::QStencilTestSeparate::Less),
      21, uint(Qt3DRender::QStencilTestSeparate::LessOrEqual),
      22, uint(Qt3DRender::QStencilTestSeparate::Equal),
      23, uint(Qt3DRender::QStencilTestSeparate::GreaterOrEqual),
      24, uint(Qt3DRender::QStencilTestSeparate::Greater),
      25, uint(Qt3DRender::QStencilTestSeparate::NotEqual),

       0        // eod
};

void Qt3DRender::QStencilTestSeparate::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        QStencilTestSeparate *_t = static_cast<QStencilTestSeparate *>(_o);
        Q_UNUSED(_t)
        switch (_id) {
        case 0: _t->maskChanged((*reinterpret_cast< uint(*)>(_a[1]))); break;
        case 1: _t->funcChanged((*reinterpret_cast< StencilFunc(*)>(_a[1]))); break;
        case 2: _t->refChanged((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 3: _t->faceModeChanged((*reinterpret_cast< StencilFaceMode(*)>(_a[1]))); break;
        case 4: _t->setMask((*reinterpret_cast< uint(*)>(_a[1]))); break;
        case 5: _t->setRef((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 6: _t->setFunc((*reinterpret_cast< StencilFunc(*)>(_a[1]))); break;
        default: ;
        }
    } else if (_c == QMetaObject::IndexOfMethod) {
        int *result = reinterpret_cast<int *>(_a[0]);
        void **func = reinterpret_cast<void **>(_a[1]);
        {
            typedef void (QStencilTestSeparate::*_t)(uint );
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&QStencilTestSeparate::maskChanged)) {
                *result = 0;
                return;
            }
        }
        {
            typedef void (QStencilTestSeparate::*_t)(StencilFunc );
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&QStencilTestSeparate::funcChanged)) {
                *result = 1;
                return;
            }
        }
        {
            typedef void (QStencilTestSeparate::*_t)(int );
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&QStencilTestSeparate::refChanged)) {
                *result = 2;
                return;
            }
        }
        {
            typedef void (QStencilTestSeparate::*_t)(StencilFaceMode );
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&QStencilTestSeparate::faceModeChanged)) {
                *result = 3;
                return;
            }
        }
    }
#ifndef QT_NO_PROPERTIES
    else if (_c == QMetaObject::ReadProperty) {
        QStencilTestSeparate *_t = static_cast<QStencilTestSeparate *>(_o);
        Q_UNUSED(_t)
        void *_v = _a[0];
        switch (_id) {
        case 0: *reinterpret_cast< StencilFaceMode*>(_v) = _t->faceMode(); break;
        case 1: *reinterpret_cast< uint*>(_v) = _t->mask(); break;
        case 2: *reinterpret_cast< int*>(_v) = _t->ref(); break;
        case 3: *reinterpret_cast< StencilFunc*>(_v) = _t->func(); break;
        default: break;
        }
    } else if (_c == QMetaObject::WriteProperty) {
        QStencilTestSeparate *_t = static_cast<QStencilTestSeparate *>(_o);
        Q_UNUSED(_t)
        void *_v = _a[0];
        switch (_id) {
        case 1: _t->setMask(*reinterpret_cast< uint*>(_v)); break;
        case 2: _t->setRef(*reinterpret_cast< int*>(_v)); break;
        case 3: _t->setFunc(*reinterpret_cast< StencilFunc*>(_v)); break;
        default: break;
        }
    } else if (_c == QMetaObject::ResetProperty) {
    }
#endif // QT_NO_PROPERTIES
}

const QMetaObject Qt3DRender::QStencilTestSeparate::staticMetaObject = {
    { &QObject::staticMetaObject, qt_meta_stringdata_Qt3DRender__QStencilTestSeparate.data,
      qt_meta_data_Qt3DRender__QStencilTestSeparate,  qt_static_metacall, Q_NULLPTR, Q_NULLPTR}
};


const QMetaObject *Qt3DRender::QStencilTestSeparate::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->dynamicMetaObject() : &staticMetaObject;
}

void *Qt3DRender::QStencilTestSeparate::qt_metacast(const char *_clname)
{
    if (!_clname) return Q_NULLPTR;
    if (!strcmp(_clname, qt_meta_stringdata_Qt3DRender__QStencilTestSeparate.stringdata0))
        return static_cast<void*>(const_cast< QStencilTestSeparate*>(this));
    return QObject::qt_metacast(_clname);
}

int Qt3DRender::QStencilTestSeparate::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QObject::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 7)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 7;
    } else if (_c == QMetaObject::RegisterMethodArgumentMetaType) {
        if (_id < 7)
            *reinterpret_cast<int*>(_a[0]) = -1;
        _id -= 7;
    }
#ifndef QT_NO_PROPERTIES
   else if (_c == QMetaObject::ReadProperty || _c == QMetaObject::WriteProperty
            || _c == QMetaObject::ResetProperty || _c == QMetaObject::RegisterPropertyMetaType) {
        qt_static_metacall(this, _c, _id, _a);
        _id -= 4;
    } else if (_c == QMetaObject::QueryPropertyDesignable) {
        _id -= 4;
    } else if (_c == QMetaObject::QueryPropertyScriptable) {
        _id -= 4;
    } else if (_c == QMetaObject::QueryPropertyStored) {
        _id -= 4;
    } else if (_c == QMetaObject::QueryPropertyEditable) {
        _id -= 4;
    } else if (_c == QMetaObject::QueryPropertyUser) {
        _id -= 4;
    }
#endif // QT_NO_PROPERTIES
    return _id;
}

// SIGNAL 0
void Qt3DRender::QStencilTestSeparate::maskChanged(uint _t1)
{
    void *_a[] = { Q_NULLPTR, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 0, _a);
}

// SIGNAL 1
void Qt3DRender::QStencilTestSeparate::funcChanged(StencilFunc _t1)
{
    void *_a[] = { Q_NULLPTR, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 1, _a);
}

// SIGNAL 2
void Qt3DRender::QStencilTestSeparate::refChanged(int _t1)
{
    void *_a[] = { Q_NULLPTR, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 2, _a);
}

// SIGNAL 3
void Qt3DRender::QStencilTestSeparate::faceModeChanged(StencilFaceMode _t1)
{
    void *_a[] = { Q_NULLPTR, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 3, _a);
}
QT_END_MOC_NAMESPACE
