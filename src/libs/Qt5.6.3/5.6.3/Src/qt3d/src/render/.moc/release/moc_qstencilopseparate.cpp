/****************************************************************************
** Meta object code from reading C++ file 'qstencilopseparate.h'
**
** Created by: The Qt Meta Object Compiler version 67 (Qt 5.6.3)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "../../renderstates/qstencilopseparate.h"
#include <QtCore/qbytearray.h>
#include <QtCore/qmetatype.h>
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'qstencilopseparate.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 67
#error "This file was generated using the moc from 5.6.3. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
struct qt_meta_stringdata_Qt3DRender__QStencilOpSeparate_t {
    QByteArrayData data[27];
    char stringdata0[310];
};
#define QT_MOC_LITERAL(idx, ofs, len) \
    Q_STATIC_BYTE_ARRAY_DATA_HEADER_INITIALIZER_WITH_OFFSET(len, \
    qptrdiff(offsetof(qt_meta_stringdata_Qt3DRender__QStencilOpSeparate_t, stringdata0) + ofs \
        - idx * sizeof(QByteArrayData)) \
    )
static const qt_meta_stringdata_Qt3DRender__QStencilOpSeparate_t qt_meta_stringdata_Qt3DRender__QStencilOpSeparate = {
    {
QT_MOC_LITERAL(0, 0, 30), // "Qt3DRender::QStencilOpSeparate"
QT_MOC_LITERAL(1, 31, 18), // "stencilFailChanged"
QT_MOC_LITERAL(2, 50, 0), // ""
QT_MOC_LITERAL(3, 51, 9), // "StencilOp"
QT_MOC_LITERAL(4, 61, 11), // "stencilFail"
QT_MOC_LITERAL(5, 73, 16), // "depthFailChanged"
QT_MOC_LITERAL(6, 90, 9), // "depthFail"
QT_MOC_LITERAL(7, 100, 23), // "stencilDepthPassChanged"
QT_MOC_LITERAL(8, 124, 16), // "stencilDepthPass"
QT_MOC_LITERAL(9, 141, 15), // "faceModeChanged"
QT_MOC_LITERAL(10, 157, 15), // "StencilFaceMode"
QT_MOC_LITERAL(11, 173, 8), // "faceMode"
QT_MOC_LITERAL(12, 182, 14), // "setStencilFail"
QT_MOC_LITERAL(13, 197, 2), // "op"
QT_MOC_LITERAL(14, 200, 12), // "setDepthFail"
QT_MOC_LITERAL(15, 213, 19), // "setStencilDepthPass"
QT_MOC_LITERAL(16, 233, 5), // "Front"
QT_MOC_LITERAL(17, 239, 4), // "Back"
QT_MOC_LITERAL(18, 244, 12), // "FrontAndBack"
QT_MOC_LITERAL(19, 257, 4), // "Zero"
QT_MOC_LITERAL(20, 262, 4), // "Keep"
QT_MOC_LITERAL(21, 267, 7), // "Replace"
QT_MOC_LITERAL(22, 275, 4), // "Incr"
QT_MOC_LITERAL(23, 280, 4), // "Decr"
QT_MOC_LITERAL(24, 285, 8), // "IncrWrap"
QT_MOC_LITERAL(25, 294, 8), // "DecrWrap"
QT_MOC_LITERAL(26, 303, 6) // "Invert"

    },
    "Qt3DRender::QStencilOpSeparate\0"
    "stencilFailChanged\0\0StencilOp\0stencilFail\0"
    "depthFailChanged\0depthFail\0"
    "stencilDepthPassChanged\0stencilDepthPass\0"
    "faceModeChanged\0StencilFaceMode\0"
    "faceMode\0setStencilFail\0op\0setDepthFail\0"
    "setStencilDepthPass\0Front\0Back\0"
    "FrontAndBack\0Zero\0Keep\0Replace\0Incr\0"
    "Decr\0IncrWrap\0DecrWrap\0Invert"
};
#undef QT_MOC_LITERAL

static const uint qt_meta_data_Qt3DRender__QStencilOpSeparate[] = {

 // content:
       7,       // revision
       0,       // classname
       0,    0, // classinfo
       7,   14, // methods
       4,   70, // properties
       2,   86, // enums/sets
       0,    0, // constructors
       0,       // flags
       4,       // signalCount

 // signals: name, argc, parameters, tag, flags
       1,    1,   49,    2, 0x06 /* Public */,
       5,    1,   52,    2, 0x06 /* Public */,
       7,    1,   55,    2, 0x06 /* Public */,
       9,    1,   58,    2, 0x06 /* Public */,

 // slots: name, argc, parameters, tag, flags
      12,    1,   61,    2, 0x0a /* Public */,
      14,    1,   64,    2, 0x0a /* Public */,
      15,    1,   67,    2, 0x0a /* Public */,

 // signals: parameters
    QMetaType::Void, 0x80000000 | 3,    4,
    QMetaType::Void, 0x80000000 | 3,    6,
    QMetaType::Void, 0x80000000 | 3,    8,
    QMetaType::Void, 0x80000000 | 10,   11,

 // slots: parameters
    QMetaType::Void, 0x80000000 | 3,   13,
    QMetaType::Void, 0x80000000 | 3,   13,
    QMetaType::Void, 0x80000000 | 3,   13,

 // properties: name, type, flags
      11, 0x80000000 | 10, 0x00495009,
       4, 0x80000000 | 3, 0x0049510b,
       6, 0x80000000 | 3, 0x0049510b,
       8, 0x80000000 | 3, 0x0049510b,

 // properties: notify_signal_id
       3,
       0,
       1,
       2,

 // enums: name, flags, count, data
      10, 0x0,    3,   94,
       3, 0x0,    8,  100,

 // enum data: key, value
      16, uint(Qt3DRender::QStencilOpSeparate::Front),
      17, uint(Qt3DRender::QStencilOpSeparate::Back),
      18, uint(Qt3DRender::QStencilOpSeparate::FrontAndBack),
      19, uint(Qt3DRender::QStencilOpSeparate::Zero),
      20, uint(Qt3DRender::QStencilOpSeparate::Keep),
      21, uint(Qt3DRender::QStencilOpSeparate::Replace),
      22, uint(Qt3DRender::QStencilOpSeparate::Incr),
      23, uint(Qt3DRender::QStencilOpSeparate::Decr),
      24, uint(Qt3DRender::QStencilOpSeparate::IncrWrap),
      25, uint(Qt3DRender::QStencilOpSeparate::DecrWrap),
      26, uint(Qt3DRender::QStencilOpSeparate::Invert),

       0        // eod
};

void Qt3DRender::QStencilOpSeparate::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        QStencilOpSeparate *_t = static_cast<QStencilOpSeparate *>(_o);
        Q_UNUSED(_t)
        switch (_id) {
        case 0: _t->stencilFailChanged((*reinterpret_cast< StencilOp(*)>(_a[1]))); break;
        case 1: _t->depthFailChanged((*reinterpret_cast< StencilOp(*)>(_a[1]))); break;
        case 2: _t->stencilDepthPassChanged((*reinterpret_cast< StencilOp(*)>(_a[1]))); break;
        case 3: _t->faceModeChanged((*reinterpret_cast< StencilFaceMode(*)>(_a[1]))); break;
        case 4: _t->setStencilFail((*reinterpret_cast< StencilOp(*)>(_a[1]))); break;
        case 5: _t->setDepthFail((*reinterpret_cast< StencilOp(*)>(_a[1]))); break;
        case 6: _t->setStencilDepthPass((*reinterpret_cast< StencilOp(*)>(_a[1]))); break;
        default: ;
        }
    } else if (_c == QMetaObject::IndexOfMethod) {
        int *result = reinterpret_cast<int *>(_a[0]);
        void **func = reinterpret_cast<void **>(_a[1]);
        {
            typedef void (QStencilOpSeparate::*_t)(StencilOp );
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&QStencilOpSeparate::stencilFailChanged)) {
                *result = 0;
                return;
            }
        }
        {
            typedef void (QStencilOpSeparate::*_t)(StencilOp );
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&QStencilOpSeparate::depthFailChanged)) {
                *result = 1;
                return;
            }
        }
        {
            typedef void (QStencilOpSeparate::*_t)(StencilOp );
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&QStencilOpSeparate::stencilDepthPassChanged)) {
                *result = 2;
                return;
            }
        }
        {
            typedef void (QStencilOpSeparate::*_t)(StencilFaceMode );
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&QStencilOpSeparate::faceModeChanged)) {
                *result = 3;
                return;
            }
        }
    }
#ifndef QT_NO_PROPERTIES
    else if (_c == QMetaObject::ReadProperty) {
        QStencilOpSeparate *_t = static_cast<QStencilOpSeparate *>(_o);
        Q_UNUSED(_t)
        void *_v = _a[0];
        switch (_id) {
        case 0: *reinterpret_cast< StencilFaceMode*>(_v) = _t->faceMode(); break;
        case 1: *reinterpret_cast< StencilOp*>(_v) = _t->stencilFail(); break;
        case 2: *reinterpret_cast< StencilOp*>(_v) = _t->depthFail(); break;
        case 3: *reinterpret_cast< StencilOp*>(_v) = _t->stencilDepthPass(); break;
        default: break;
        }
    } else if (_c == QMetaObject::WriteProperty) {
        QStencilOpSeparate *_t = static_cast<QStencilOpSeparate *>(_o);
        Q_UNUSED(_t)
        void *_v = _a[0];
        switch (_id) {
        case 1: _t->setStencilFail(*reinterpret_cast< StencilOp*>(_v)); break;
        case 2: _t->setDepthFail(*reinterpret_cast< StencilOp*>(_v)); break;
        case 3: _t->setStencilDepthPass(*reinterpret_cast< StencilOp*>(_v)); break;
        default: break;
        }
    } else if (_c == QMetaObject::ResetProperty) {
    }
#endif // QT_NO_PROPERTIES
}

const QMetaObject Qt3DRender::QStencilOpSeparate::staticMetaObject = {
    { &QObject::staticMetaObject, qt_meta_stringdata_Qt3DRender__QStencilOpSeparate.data,
      qt_meta_data_Qt3DRender__QStencilOpSeparate,  qt_static_metacall, Q_NULLPTR, Q_NULLPTR}
};


const QMetaObject *Qt3DRender::QStencilOpSeparate::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->dynamicMetaObject() : &staticMetaObject;
}

void *Qt3DRender::QStencilOpSeparate::qt_metacast(const char *_clname)
{
    if (!_clname) return Q_NULLPTR;
    if (!strcmp(_clname, qt_meta_stringdata_Qt3DRender__QStencilOpSeparate.stringdata0))
        return static_cast<void*>(const_cast< QStencilOpSeparate*>(this));
    return QObject::qt_metacast(_clname);
}

int Qt3DRender::QStencilOpSeparate::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QObject::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 7)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 7;
    } else if (_c == QMetaObject::RegisterMethodArgumentMetaType) {
        if (_id < 7)
            *reinterpret_cast<int*>(_a[0]) = -1;
        _id -= 7;
    }
#ifndef QT_NO_PROPERTIES
   else if (_c == QMetaObject::ReadProperty || _c == QMetaObject::WriteProperty
            || _c == QMetaObject::ResetProperty || _c == QMetaObject::RegisterPropertyMetaType) {
        qt_static_metacall(this, _c, _id, _a);
        _id -= 4;
    } else if (_c == QMetaObject::QueryPropertyDesignable) {
        _id -= 4;
    } else if (_c == QMetaObject::QueryPropertyScriptable) {
        _id -= 4;
    } else if (_c == QMetaObject::QueryPropertyStored) {
        _id -= 4;
    } else if (_c == QMetaObject::QueryPropertyEditable) {
        _id -= 4;
    } else if (_c == QMetaObject::QueryPropertyUser) {
        _id -= 4;
    }
#endif // QT_NO_PROPERTIES
    return _id;
}

// SIGNAL 0
void Qt3DRender::QStencilOpSeparate::stencilFailChanged(StencilOp _t1)
{
    void *_a[] = { Q_NULLPTR, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 0, _a);
}

// SIGNAL 1
void Qt3DRender::QStencilOpSeparate::depthFailChanged(StencilOp _t1)
{
    void *_a[] = { Q_NULLPTR, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 1, _a);
}

// SIGNAL 2
void Qt3DRender::QStencilOpSeparate::stencilDepthPassChanged(StencilOp _t1)
{
    void *_a[] = { Q_NULLPTR, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 2, _a);
}

// SIGNAL 3
void Qt3DRender::QStencilOpSeparate::faceModeChanged(StencilFaceMode _t1)
{
    void *_a[] = { Q_NULLPTR, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 3, _a);
}
QT_END_MOC_NAMESPACE
