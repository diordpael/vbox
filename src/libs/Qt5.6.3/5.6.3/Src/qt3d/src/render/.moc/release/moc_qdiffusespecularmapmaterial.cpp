/****************************************************************************
** Meta object code from reading C++ file 'qdiffusespecularmapmaterial.h'
**
** Created by: The Qt Meta Object Compiler version 67 (Qt 5.6.3)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "../../defaults/qdiffusespecularmapmaterial.h"
#include <QtCore/qbytearray.h>
#include <QtCore/qmetatype.h>
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'qdiffusespecularmapmaterial.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 67
#error "This file was generated using the moc from 5.6.3. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
struct qt_meta_stringdata_Qt3DRender__QDiffuseSpecularMapMaterial_t {
    QByteArrayData data[19];
    char stringdata0[299];
};
#define QT_MOC_LITERAL(idx, ofs, len) \
    Q_STATIC_BYTE_ARRAY_DATA_HEADER_INITIALIZER_WITH_OFFSET(len, \
    qptrdiff(offsetof(qt_meta_stringdata_Qt3DRender__QDiffuseSpecularMapMaterial_t, stringdata0) + ofs \
        - idx * sizeof(QByteArrayData)) \
    )
static const qt_meta_stringdata_Qt3DRender__QDiffuseSpecularMapMaterial_t qt_meta_stringdata_Qt3DRender__QDiffuseSpecularMapMaterial = {
    {
QT_MOC_LITERAL(0, 0, 39), // "Qt3DRender::QDiffuseSpecularM..."
QT_MOC_LITERAL(1, 40, 14), // "ambientChanged"
QT_MOC_LITERAL(2, 55, 0), // ""
QT_MOC_LITERAL(3, 56, 7), // "ambient"
QT_MOC_LITERAL(4, 64, 14), // "diffuseChanged"
QT_MOC_LITERAL(5, 79, 25), // "QAbstractTextureProvider*"
QT_MOC_LITERAL(6, 105, 7), // "diffuse"
QT_MOC_LITERAL(7, 113, 15), // "specularChanged"
QT_MOC_LITERAL(8, 129, 8), // "specular"
QT_MOC_LITERAL(9, 138, 16), // "shininessChanged"
QT_MOC_LITERAL(10, 155, 9), // "shininess"
QT_MOC_LITERAL(11, 165, 19), // "textureScaleChanged"
QT_MOC_LITERAL(12, 185, 12), // "textureScale"
QT_MOC_LITERAL(13, 198, 10), // "setAmbient"
QT_MOC_LITERAL(14, 209, 10), // "setDiffuse"
QT_MOC_LITERAL(15, 220, 11), // "setSpecular"
QT_MOC_LITERAL(16, 232, 12), // "setShininess"
QT_MOC_LITERAL(17, 245, 15), // "setTextureScale"
QT_MOC_LITERAL(18, 261, 37) // "Qt3DRender::QAbstractTextureP..."

    },
    "Qt3DRender::QDiffuseSpecularMapMaterial\0"
    "ambientChanged\0\0ambient\0diffuseChanged\0"
    "QAbstractTextureProvider*\0diffuse\0"
    "specularChanged\0specular\0shininessChanged\0"
    "shininess\0textureScaleChanged\0"
    "textureScale\0setAmbient\0setDiffuse\0"
    "setSpecular\0setShininess\0setTextureScale\0"
    "Qt3DRender::QAbstractTextureProvider*"
};
#undef QT_MOC_LITERAL

static const uint qt_meta_data_Qt3DRender__QDiffuseSpecularMapMaterial[] = {

 // content:
       7,       // revision
       0,       // classname
       0,    0, // classinfo
      10,   14, // methods
       5,   94, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       5,       // signalCount

 // signals: name, argc, parameters, tag, flags
       1,    1,   64,    2, 0x06 /* Public */,
       4,    1,   67,    2, 0x06 /* Public */,
       7,    1,   70,    2, 0x06 /* Public */,
       9,    1,   73,    2, 0x06 /* Public */,
      11,    1,   76,    2, 0x06 /* Public */,

 // slots: name, argc, parameters, tag, flags
      13,    1,   79,    2, 0x0a /* Public */,
      14,    1,   82,    2, 0x0a /* Public */,
      15,    1,   85,    2, 0x0a /* Public */,
      16,    1,   88,    2, 0x0a /* Public */,
      17,    1,   91,    2, 0x0a /* Public */,

 // signals: parameters
    QMetaType::Void, QMetaType::QColor,    3,
    QMetaType::Void, 0x80000000 | 5,    6,
    QMetaType::Void, 0x80000000 | 5,    8,
    QMetaType::Void, QMetaType::Float,   10,
    QMetaType::Void, QMetaType::Float,   12,

 // slots: parameters
    QMetaType::Void, QMetaType::QColor,    3,
    QMetaType::Void, 0x80000000 | 5,    6,
    QMetaType::Void, 0x80000000 | 5,    8,
    QMetaType::Void, QMetaType::Float,   10,
    QMetaType::Void, QMetaType::Float,   12,

 // properties: name, type, flags
       3, QMetaType::QColor, 0x00495103,
      10, QMetaType::Float, 0x00495103,
       8, 0x80000000 | 18, 0x0049510b,
       6, 0x80000000 | 18, 0x0049510b,
      12, QMetaType::Float, 0x00495103,

 // properties: notify_signal_id
       0,
       3,
       2,
       1,
       4,

       0        // eod
};

void Qt3DRender::QDiffuseSpecularMapMaterial::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        QDiffuseSpecularMapMaterial *_t = static_cast<QDiffuseSpecularMapMaterial *>(_o);
        Q_UNUSED(_t)
        switch (_id) {
        case 0: _t->ambientChanged((*reinterpret_cast< const QColor(*)>(_a[1]))); break;
        case 1: _t->diffuseChanged((*reinterpret_cast< QAbstractTextureProvider*(*)>(_a[1]))); break;
        case 2: _t->specularChanged((*reinterpret_cast< QAbstractTextureProvider*(*)>(_a[1]))); break;
        case 3: _t->shininessChanged((*reinterpret_cast< float(*)>(_a[1]))); break;
        case 4: _t->textureScaleChanged((*reinterpret_cast< float(*)>(_a[1]))); break;
        case 5: _t->setAmbient((*reinterpret_cast< const QColor(*)>(_a[1]))); break;
        case 6: _t->setDiffuse((*reinterpret_cast< QAbstractTextureProvider*(*)>(_a[1]))); break;
        case 7: _t->setSpecular((*reinterpret_cast< QAbstractTextureProvider*(*)>(_a[1]))); break;
        case 8: _t->setShininess((*reinterpret_cast< float(*)>(_a[1]))); break;
        case 9: _t->setTextureScale((*reinterpret_cast< float(*)>(_a[1]))); break;
        default: ;
        }
    } else if (_c == QMetaObject::IndexOfMethod) {
        int *result = reinterpret_cast<int *>(_a[0]);
        void **func = reinterpret_cast<void **>(_a[1]);
        {
            typedef void (QDiffuseSpecularMapMaterial::*_t)(const QColor & );
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&QDiffuseSpecularMapMaterial::ambientChanged)) {
                *result = 0;
                return;
            }
        }
        {
            typedef void (QDiffuseSpecularMapMaterial::*_t)(QAbstractTextureProvider * );
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&QDiffuseSpecularMapMaterial::diffuseChanged)) {
                *result = 1;
                return;
            }
        }
        {
            typedef void (QDiffuseSpecularMapMaterial::*_t)(QAbstractTextureProvider * );
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&QDiffuseSpecularMapMaterial::specularChanged)) {
                *result = 2;
                return;
            }
        }
        {
            typedef void (QDiffuseSpecularMapMaterial::*_t)(float );
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&QDiffuseSpecularMapMaterial::shininessChanged)) {
                *result = 3;
                return;
            }
        }
        {
            typedef void (QDiffuseSpecularMapMaterial::*_t)(float );
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&QDiffuseSpecularMapMaterial::textureScaleChanged)) {
                *result = 4;
                return;
            }
        }
    }
#ifndef QT_NO_PROPERTIES
    else if (_c == QMetaObject::ReadProperty) {
        QDiffuseSpecularMapMaterial *_t = static_cast<QDiffuseSpecularMapMaterial *>(_o);
        Q_UNUSED(_t)
        void *_v = _a[0];
        switch (_id) {
        case 0: *reinterpret_cast< QColor*>(_v) = _t->ambient(); break;
        case 1: *reinterpret_cast< float*>(_v) = _t->shininess(); break;
        case 2: *reinterpret_cast< Qt3DRender::QAbstractTextureProvider**>(_v) = _t->specular(); break;
        case 3: *reinterpret_cast< Qt3DRender::QAbstractTextureProvider**>(_v) = _t->diffuse(); break;
        case 4: *reinterpret_cast< float*>(_v) = _t->textureScale(); break;
        default: break;
        }
    } else if (_c == QMetaObject::WriteProperty) {
        QDiffuseSpecularMapMaterial *_t = static_cast<QDiffuseSpecularMapMaterial *>(_o);
        Q_UNUSED(_t)
        void *_v = _a[0];
        switch (_id) {
        case 0: _t->setAmbient(*reinterpret_cast< QColor*>(_v)); break;
        case 1: _t->setShininess(*reinterpret_cast< float*>(_v)); break;
        case 2: _t->setSpecular(*reinterpret_cast< Qt3DRender::QAbstractTextureProvider**>(_v)); break;
        case 3: _t->setDiffuse(*reinterpret_cast< Qt3DRender::QAbstractTextureProvider**>(_v)); break;
        case 4: _t->setTextureScale(*reinterpret_cast< float*>(_v)); break;
        default: break;
        }
    } else if (_c == QMetaObject::ResetProperty) {
    }
#endif // QT_NO_PROPERTIES
}

const QMetaObject Qt3DRender::QDiffuseSpecularMapMaterial::staticMetaObject = {
    { &QMaterial::staticMetaObject, qt_meta_stringdata_Qt3DRender__QDiffuseSpecularMapMaterial.data,
      qt_meta_data_Qt3DRender__QDiffuseSpecularMapMaterial,  qt_static_metacall, Q_NULLPTR, Q_NULLPTR}
};


const QMetaObject *Qt3DRender::QDiffuseSpecularMapMaterial::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->dynamicMetaObject() : &staticMetaObject;
}

void *Qt3DRender::QDiffuseSpecularMapMaterial::qt_metacast(const char *_clname)
{
    if (!_clname) return Q_NULLPTR;
    if (!strcmp(_clname, qt_meta_stringdata_Qt3DRender__QDiffuseSpecularMapMaterial.stringdata0))
        return static_cast<void*>(const_cast< QDiffuseSpecularMapMaterial*>(this));
    return QMaterial::qt_metacast(_clname);
}

int Qt3DRender::QDiffuseSpecularMapMaterial::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QMaterial::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 10)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 10;
    } else if (_c == QMetaObject::RegisterMethodArgumentMetaType) {
        if (_id < 10)
            *reinterpret_cast<int*>(_a[0]) = -1;
        _id -= 10;
    }
#ifndef QT_NO_PROPERTIES
   else if (_c == QMetaObject::ReadProperty || _c == QMetaObject::WriteProperty
            || _c == QMetaObject::ResetProperty || _c == QMetaObject::RegisterPropertyMetaType) {
        qt_static_metacall(this, _c, _id, _a);
        _id -= 5;
    } else if (_c == QMetaObject::QueryPropertyDesignable) {
        _id -= 5;
    } else if (_c == QMetaObject::QueryPropertyScriptable) {
        _id -= 5;
    } else if (_c == QMetaObject::QueryPropertyStored) {
        _id -= 5;
    } else if (_c == QMetaObject::QueryPropertyEditable) {
        _id -= 5;
    } else if (_c == QMetaObject::QueryPropertyUser) {
        _id -= 5;
    }
#endif // QT_NO_PROPERTIES
    return _id;
}

// SIGNAL 0
void Qt3DRender::QDiffuseSpecularMapMaterial::ambientChanged(const QColor & _t1)
{
    void *_a[] = { Q_NULLPTR, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 0, _a);
}

// SIGNAL 1
void Qt3DRender::QDiffuseSpecularMapMaterial::diffuseChanged(QAbstractTextureProvider * _t1)
{
    void *_a[] = { Q_NULLPTR, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 1, _a);
}

// SIGNAL 2
void Qt3DRender::QDiffuseSpecularMapMaterial::specularChanged(QAbstractTextureProvider * _t1)
{
    void *_a[] = { Q_NULLPTR, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 2, _a);
}

// SIGNAL 3
void Qt3DRender::QDiffuseSpecularMapMaterial::shininessChanged(float _t1)
{
    void *_a[] = { Q_NULLPTR, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 3, _a);
}

// SIGNAL 4
void Qt3DRender::QDiffuseSpecularMapMaterial::textureScaleChanged(float _t1)
{
    void *_a[] = { Q_NULLPTR, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 4, _a);
}
QT_END_MOC_NAMESPACE
