/****************************************************************************
** Meta object code from reading C++ file 'qframegraph.h'
**
** Created by: The Qt Meta Object Compiler version 67 (Qt 5.6.3)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "../../framegraph/qframegraph.h"
#include <QtCore/qbytearray.h>
#include <QtCore/qmetatype.h>
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'qframegraph.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 67
#error "This file was generated using the moc from 5.6.3. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
struct qt_meta_stringdata_Qt3DRender__QFrameGraph_t {
    QByteArrayData data[8];
    char stringdata0[148];
};
#define QT_MOC_LITERAL(idx, ofs, len) \
    Q_STATIC_BYTE_ARRAY_DATA_HEADER_INITIALIZER_WITH_OFFSET(len, \
    qptrdiff(offsetof(qt_meta_stringdata_Qt3DRender__QFrameGraph_t, stringdata0) + ofs \
        - idx * sizeof(QByteArrayData)) \
    )
static const qt_meta_stringdata_Qt3DRender__QFrameGraph_t qt_meta_stringdata_Qt3DRender__QFrameGraph = {
    {
QT_MOC_LITERAL(0, 0, 23), // "Qt3DRender::QFrameGraph"
QT_MOC_LITERAL(1, 24, 15), // "DefaultProperty"
QT_MOC_LITERAL(2, 40, 16), // "activeFrameGraph"
QT_MOC_LITERAL(3, 57, 23), // "activeFrameGraphChanged"
QT_MOC_LITERAL(4, 81, 0), // ""
QT_MOC_LITERAL(5, 82, 16), // "QFrameGraphNode*"
QT_MOC_LITERAL(6, 99, 19), // "setActiveFrameGraph"
QT_MOC_LITERAL(7, 119, 28) // "Qt3DRender::QFrameGraphNode*"

    },
    "Qt3DRender::QFrameGraph\0DefaultProperty\0"
    "activeFrameGraph\0activeFrameGraphChanged\0"
    "\0QFrameGraphNode*\0setActiveFrameGraph\0"
    "Qt3DRender::QFrameGraphNode*"
};
#undef QT_MOC_LITERAL

static const uint qt_meta_data_Qt3DRender__QFrameGraph[] = {

 // content:
       7,       // revision
       0,       // classname
       1,   14, // classinfo
       2,   16, // methods
       1,   32, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       1,       // signalCount

 // classinfo: key, value
       1,    2,

 // signals: name, argc, parameters, tag, flags
       3,    1,   26,    4, 0x06 /* Public */,

 // slots: name, argc, parameters, tag, flags
       6,    1,   29,    4, 0x0a /* Public */,

 // signals: parameters
    QMetaType::Void, 0x80000000 | 5,    2,

 // slots: parameters
    QMetaType::Void, 0x80000000 | 5,    2,

 // properties: name, type, flags
       2, 0x80000000 | 7, 0x0049510b,

 // properties: notify_signal_id
       0,

       0        // eod
};

void Qt3DRender::QFrameGraph::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        QFrameGraph *_t = static_cast<QFrameGraph *>(_o);
        Q_UNUSED(_t)
        switch (_id) {
        case 0: _t->activeFrameGraphChanged((*reinterpret_cast< QFrameGraphNode*(*)>(_a[1]))); break;
        case 1: _t->setActiveFrameGraph((*reinterpret_cast< QFrameGraphNode*(*)>(_a[1]))); break;
        default: ;
        }
    } else if (_c == QMetaObject::IndexOfMethod) {
        int *result = reinterpret_cast<int *>(_a[0]);
        void **func = reinterpret_cast<void **>(_a[1]);
        {
            typedef void (QFrameGraph::*_t)(QFrameGraphNode * );
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&QFrameGraph::activeFrameGraphChanged)) {
                *result = 0;
                return;
            }
        }
    }
#ifndef QT_NO_PROPERTIES
    else if (_c == QMetaObject::ReadProperty) {
        QFrameGraph *_t = static_cast<QFrameGraph *>(_o);
        Q_UNUSED(_t)
        void *_v = _a[0];
        switch (_id) {
        case 0: *reinterpret_cast< Qt3DRender::QFrameGraphNode**>(_v) = _t->activeFrameGraph(); break;
        default: break;
        }
    } else if (_c == QMetaObject::WriteProperty) {
        QFrameGraph *_t = static_cast<QFrameGraph *>(_o);
        Q_UNUSED(_t)
        void *_v = _a[0];
        switch (_id) {
        case 0: _t->setActiveFrameGraph(*reinterpret_cast< Qt3DRender::QFrameGraphNode**>(_v)); break;
        default: break;
        }
    } else if (_c == QMetaObject::ResetProperty) {
    }
#endif // QT_NO_PROPERTIES
}

const QMetaObject Qt3DRender::QFrameGraph::staticMetaObject = {
    { &Qt3DCore::QComponent::staticMetaObject, qt_meta_stringdata_Qt3DRender__QFrameGraph.data,
      qt_meta_data_Qt3DRender__QFrameGraph,  qt_static_metacall, Q_NULLPTR, Q_NULLPTR}
};


const QMetaObject *Qt3DRender::QFrameGraph::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->dynamicMetaObject() : &staticMetaObject;
}

void *Qt3DRender::QFrameGraph::qt_metacast(const char *_clname)
{
    if (!_clname) return Q_NULLPTR;
    if (!strcmp(_clname, qt_meta_stringdata_Qt3DRender__QFrameGraph.stringdata0))
        return static_cast<void*>(const_cast< QFrameGraph*>(this));
    return Qt3DCore::QComponent::qt_metacast(_clname);
}

int Qt3DRender::QFrameGraph::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = Qt3DCore::QComponent::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 2)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 2;
    } else if (_c == QMetaObject::RegisterMethodArgumentMetaType) {
        if (_id < 2)
            *reinterpret_cast<int*>(_a[0]) = -1;
        _id -= 2;
    }
#ifndef QT_NO_PROPERTIES
   else if (_c == QMetaObject::ReadProperty || _c == QMetaObject::WriteProperty
            || _c == QMetaObject::ResetProperty || _c == QMetaObject::RegisterPropertyMetaType) {
        qt_static_metacall(this, _c, _id, _a);
        _id -= 1;
    } else if (_c == QMetaObject::QueryPropertyDesignable) {
        _id -= 1;
    } else if (_c == QMetaObject::QueryPropertyScriptable) {
        _id -= 1;
    } else if (_c == QMetaObject::QueryPropertyStored) {
        _id -= 1;
    } else if (_c == QMetaObject::QueryPropertyEditable) {
        _id -= 1;
    } else if (_c == QMetaObject::QueryPropertyUser) {
        _id -= 1;
    }
#endif // QT_NO_PROPERTIES
    return _id;
}

// SIGNAL 0
void Qt3DRender::QFrameGraph::activeFrameGraphChanged(QFrameGraphNode * _t1)
{
    void *_a[] = { Q_NULLPTR, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 0, _a);
}
QT_END_MOC_NAMESPACE
