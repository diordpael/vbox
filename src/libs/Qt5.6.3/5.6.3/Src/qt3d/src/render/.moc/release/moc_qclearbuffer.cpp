/****************************************************************************
** Meta object code from reading C++ file 'qclearbuffer.h'
**
** Created by: The Qt Meta Object Compiler version 67 (Qt 5.6.3)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "../../framegraph/qclearbuffer.h"
#include <QtCore/qbytearray.h>
#include <QtCore/qmetatype.h>
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'qclearbuffer.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 67
#error "This file was generated using the moc from 5.6.3. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
struct qt_meta_stringdata_Qt3DRender__QClearBuffer_t {
    QByteArrayData data[14];
    char stringdata0[185];
};
#define QT_MOC_LITERAL(idx, ofs, len) \
    Q_STATIC_BYTE_ARRAY_DATA_HEADER_INITIALIZER_WITH_OFFSET(len, \
    qptrdiff(offsetof(qt_meta_stringdata_Qt3DRender__QClearBuffer_t, stringdata0) + ofs \
        - idx * sizeof(QByteArrayData)) \
    )
static const qt_meta_stringdata_Qt3DRender__QClearBuffer_t qt_meta_stringdata_Qt3DRender__QClearBuffer = {
    {
QT_MOC_LITERAL(0, 0, 24), // "Qt3DRender::QClearBuffer"
QT_MOC_LITERAL(1, 25, 14), // "buffersChanged"
QT_MOC_LITERAL(2, 40, 0), // ""
QT_MOC_LITERAL(3, 41, 10), // "BufferType"
QT_MOC_LITERAL(4, 52, 7), // "buffers"
QT_MOC_LITERAL(5, 60, 10), // "setBuffers"
QT_MOC_LITERAL(6, 71, 4), // "None"
QT_MOC_LITERAL(7, 76, 11), // "ColorBuffer"
QT_MOC_LITERAL(8, 88, 11), // "DepthBuffer"
QT_MOC_LITERAL(9, 100, 13), // "StencilBuffer"
QT_MOC_LITERAL(10, 114, 18), // "DepthStencilBuffer"
QT_MOC_LITERAL(11, 133, 16), // "ColorDepthBuffer"
QT_MOC_LITERAL(12, 150, 23), // "ColorDepthStencilBuffer"
QT_MOC_LITERAL(13, 174, 10) // "AllBuffers"

    },
    "Qt3DRender::QClearBuffer\0buffersChanged\0"
    "\0BufferType\0buffers\0setBuffers\0None\0"
    "ColorBuffer\0DepthBuffer\0StencilBuffer\0"
    "DepthStencilBuffer\0ColorDepthBuffer\0"
    "ColorDepthStencilBuffer\0AllBuffers"
};
#undef QT_MOC_LITERAL

static const uint qt_meta_data_Qt3DRender__QClearBuffer[] = {

 // content:
       7,       // revision
       0,       // classname
       0,    0, // classinfo
       2,   14, // methods
       1,   30, // properties
       1,   34, // enums/sets
       0,    0, // constructors
       0,       // flags
       1,       // signalCount

 // signals: name, argc, parameters, tag, flags
       1,    1,   24,    2, 0x06 /* Public */,

 // slots: name, argc, parameters, tag, flags
       5,    1,   27,    2, 0x0a /* Public */,

 // signals: parameters
    QMetaType::Void, 0x80000000 | 3,    4,

 // slots: parameters
    QMetaType::Void, 0x80000000 | 3,    4,

 // properties: name, type, flags
       4, 0x80000000 | 3, 0x0049510b,

 // properties: notify_signal_id
       0,

 // enums: name, flags, count, data
       3, 0x0,    8,   38,

 // enum data: key, value
       6, uint(Qt3DRender::QClearBuffer::None),
       7, uint(Qt3DRender::QClearBuffer::ColorBuffer),
       8, uint(Qt3DRender::QClearBuffer::DepthBuffer),
       9, uint(Qt3DRender::QClearBuffer::StencilBuffer),
      10, uint(Qt3DRender::QClearBuffer::DepthStencilBuffer),
      11, uint(Qt3DRender::QClearBuffer::ColorDepthBuffer),
      12, uint(Qt3DRender::QClearBuffer::ColorDepthStencilBuffer),
      13, uint(Qt3DRender::QClearBuffer::AllBuffers),

       0        // eod
};

void Qt3DRender::QClearBuffer::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        QClearBuffer *_t = static_cast<QClearBuffer *>(_o);
        Q_UNUSED(_t)
        switch (_id) {
        case 0: _t->buffersChanged((*reinterpret_cast< BufferType(*)>(_a[1]))); break;
        case 1: _t->setBuffers((*reinterpret_cast< BufferType(*)>(_a[1]))); break;
        default: ;
        }
    } else if (_c == QMetaObject::IndexOfMethod) {
        int *result = reinterpret_cast<int *>(_a[0]);
        void **func = reinterpret_cast<void **>(_a[1]);
        {
            typedef void (QClearBuffer::*_t)(BufferType );
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&QClearBuffer::buffersChanged)) {
                *result = 0;
                return;
            }
        }
    }
#ifndef QT_NO_PROPERTIES
    else if (_c == QMetaObject::ReadProperty) {
        QClearBuffer *_t = static_cast<QClearBuffer *>(_o);
        Q_UNUSED(_t)
        void *_v = _a[0];
        switch (_id) {
        case 0: *reinterpret_cast< BufferType*>(_v) = _t->buffers(); break;
        default: break;
        }
    } else if (_c == QMetaObject::WriteProperty) {
        QClearBuffer *_t = static_cast<QClearBuffer *>(_o);
        Q_UNUSED(_t)
        void *_v = _a[0];
        switch (_id) {
        case 0: _t->setBuffers(*reinterpret_cast< BufferType*>(_v)); break;
        default: break;
        }
    } else if (_c == QMetaObject::ResetProperty) {
    }
#endif // QT_NO_PROPERTIES
}

const QMetaObject Qt3DRender::QClearBuffer::staticMetaObject = {
    { &QFrameGraphNode::staticMetaObject, qt_meta_stringdata_Qt3DRender__QClearBuffer.data,
      qt_meta_data_Qt3DRender__QClearBuffer,  qt_static_metacall, Q_NULLPTR, Q_NULLPTR}
};


const QMetaObject *Qt3DRender::QClearBuffer::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->dynamicMetaObject() : &staticMetaObject;
}

void *Qt3DRender::QClearBuffer::qt_metacast(const char *_clname)
{
    if (!_clname) return Q_NULLPTR;
    if (!strcmp(_clname, qt_meta_stringdata_Qt3DRender__QClearBuffer.stringdata0))
        return static_cast<void*>(const_cast< QClearBuffer*>(this));
    return QFrameGraphNode::qt_metacast(_clname);
}

int Qt3DRender::QClearBuffer::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QFrameGraphNode::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 2)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 2;
    } else if (_c == QMetaObject::RegisterMethodArgumentMetaType) {
        if (_id < 2)
            *reinterpret_cast<int*>(_a[0]) = -1;
        _id -= 2;
    }
#ifndef QT_NO_PROPERTIES
   else if (_c == QMetaObject::ReadProperty || _c == QMetaObject::WriteProperty
            || _c == QMetaObject::ResetProperty || _c == QMetaObject::RegisterPropertyMetaType) {
        qt_static_metacall(this, _c, _id, _a);
        _id -= 1;
    } else if (_c == QMetaObject::QueryPropertyDesignable) {
        _id -= 1;
    } else if (_c == QMetaObject::QueryPropertyScriptable) {
        _id -= 1;
    } else if (_c == QMetaObject::QueryPropertyStored) {
        _id -= 1;
    } else if (_c == QMetaObject::QueryPropertyEditable) {
        _id -= 1;
    } else if (_c == QMetaObject::QueryPropertyUser) {
        _id -= 1;
    }
#endif // QT_NO_PROPERTIES
    return _id;
}

// SIGNAL 0
void Qt3DRender::QClearBuffer::buffersChanged(BufferType _t1)
{
    void *_a[] = { Q_NULLPTR, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 0, _a);
}
QT_END_MOC_NAMESPACE
