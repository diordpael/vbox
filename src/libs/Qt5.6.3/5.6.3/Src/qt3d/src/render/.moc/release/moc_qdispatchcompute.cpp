/****************************************************************************
** Meta object code from reading C++ file 'qdispatchcompute.h'
**
** Created by: The Qt Meta Object Compiler version 67 (Qt 5.6.3)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "../../framegraph/qdispatchcompute.h"
#include <QtCore/qbytearray.h>
#include <QtCore/qmetatype.h>
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'qdispatchcompute.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 67
#error "This file was generated using the moc from 5.6.3. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
struct qt_meta_stringdata_Qt3DRender__QDispatchCompute_t {
    QByteArrayData data[8];
    char stringdata0[117];
};
#define QT_MOC_LITERAL(idx, ofs, len) \
    Q_STATIC_BYTE_ARRAY_DATA_HEADER_INITIALIZER_WITH_OFFSET(len, \
    qptrdiff(offsetof(qt_meta_stringdata_Qt3DRender__QDispatchCompute_t, stringdata0) + ofs \
        - idx * sizeof(QByteArrayData)) \
    )
static const qt_meta_stringdata_Qt3DRender__QDispatchCompute_t qt_meta_stringdata_Qt3DRender__QDispatchCompute = {
    {
QT_MOC_LITERAL(0, 0, 28), // "Qt3DRender::QDispatchCompute"
QT_MOC_LITERAL(1, 29, 17), // "workGroupXChanged"
QT_MOC_LITERAL(2, 47, 0), // ""
QT_MOC_LITERAL(3, 48, 17), // "workGroupYChanged"
QT_MOC_LITERAL(4, 66, 17), // "workGroupZChanged"
QT_MOC_LITERAL(5, 84, 10), // "workGroupX"
QT_MOC_LITERAL(6, 95, 10), // "workGroupY"
QT_MOC_LITERAL(7, 106, 10) // "workGroupZ"

    },
    "Qt3DRender::QDispatchCompute\0"
    "workGroupXChanged\0\0workGroupYChanged\0"
    "workGroupZChanged\0workGroupX\0workGroupY\0"
    "workGroupZ"
};
#undef QT_MOC_LITERAL

static const uint qt_meta_data_Qt3DRender__QDispatchCompute[] = {

 // content:
       7,       // revision
       0,       // classname
       0,    0, // classinfo
       3,   14, // methods
       3,   32, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       3,       // signalCount

 // signals: name, argc, parameters, tag, flags
       1,    0,   29,    2, 0x06 /* Public */,
       3,    0,   30,    2, 0x06 /* Public */,
       4,    0,   31,    2, 0x06 /* Public */,

 // signals: parameters
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,

 // properties: name, type, flags
       5, QMetaType::Int, 0x00495103,
       6, QMetaType::Int, 0x00495103,
       7, QMetaType::Int, 0x00495103,

 // properties: notify_signal_id
       0,
       1,
       2,

       0        // eod
};

void Qt3DRender::QDispatchCompute::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        QDispatchCompute *_t = static_cast<QDispatchCompute *>(_o);
        Q_UNUSED(_t)
        switch (_id) {
        case 0: _t->workGroupXChanged(); break;
        case 1: _t->workGroupYChanged(); break;
        case 2: _t->workGroupZChanged(); break;
        default: ;
        }
    } else if (_c == QMetaObject::IndexOfMethod) {
        int *result = reinterpret_cast<int *>(_a[0]);
        void **func = reinterpret_cast<void **>(_a[1]);
        {
            typedef void (QDispatchCompute::*_t)();
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&QDispatchCompute::workGroupXChanged)) {
                *result = 0;
                return;
            }
        }
        {
            typedef void (QDispatchCompute::*_t)();
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&QDispatchCompute::workGroupYChanged)) {
                *result = 1;
                return;
            }
        }
        {
            typedef void (QDispatchCompute::*_t)();
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&QDispatchCompute::workGroupZChanged)) {
                *result = 2;
                return;
            }
        }
    }
#ifndef QT_NO_PROPERTIES
    else if (_c == QMetaObject::ReadProperty) {
        QDispatchCompute *_t = static_cast<QDispatchCompute *>(_o);
        Q_UNUSED(_t)
        void *_v = _a[0];
        switch (_id) {
        case 0: *reinterpret_cast< int*>(_v) = _t->workGroupX(); break;
        case 1: *reinterpret_cast< int*>(_v) = _t->workGroupY(); break;
        case 2: *reinterpret_cast< int*>(_v) = _t->workGroupZ(); break;
        default: break;
        }
    } else if (_c == QMetaObject::WriteProperty) {
        QDispatchCompute *_t = static_cast<QDispatchCompute *>(_o);
        Q_UNUSED(_t)
        void *_v = _a[0];
        switch (_id) {
        case 0: _t->setWorkGroupX(*reinterpret_cast< int*>(_v)); break;
        case 1: _t->setWorkGroupY(*reinterpret_cast< int*>(_v)); break;
        case 2: _t->setWorkGroupZ(*reinterpret_cast< int*>(_v)); break;
        default: break;
        }
    } else if (_c == QMetaObject::ResetProperty) {
    }
#endif // QT_NO_PROPERTIES
    Q_UNUSED(_a);
}

const QMetaObject Qt3DRender::QDispatchCompute::staticMetaObject = {
    { &QFrameGraphNode::staticMetaObject, qt_meta_stringdata_Qt3DRender__QDispatchCompute.data,
      qt_meta_data_Qt3DRender__QDispatchCompute,  qt_static_metacall, Q_NULLPTR, Q_NULLPTR}
};


const QMetaObject *Qt3DRender::QDispatchCompute::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->dynamicMetaObject() : &staticMetaObject;
}

void *Qt3DRender::QDispatchCompute::qt_metacast(const char *_clname)
{
    if (!_clname) return Q_NULLPTR;
    if (!strcmp(_clname, qt_meta_stringdata_Qt3DRender__QDispatchCompute.stringdata0))
        return static_cast<void*>(const_cast< QDispatchCompute*>(this));
    return QFrameGraphNode::qt_metacast(_clname);
}

int Qt3DRender::QDispatchCompute::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QFrameGraphNode::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 3)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 3;
    } else if (_c == QMetaObject::RegisterMethodArgumentMetaType) {
        if (_id < 3)
            *reinterpret_cast<int*>(_a[0]) = -1;
        _id -= 3;
    }
#ifndef QT_NO_PROPERTIES
   else if (_c == QMetaObject::ReadProperty || _c == QMetaObject::WriteProperty
            || _c == QMetaObject::ResetProperty || _c == QMetaObject::RegisterPropertyMetaType) {
        qt_static_metacall(this, _c, _id, _a);
        _id -= 3;
    } else if (_c == QMetaObject::QueryPropertyDesignable) {
        _id -= 3;
    } else if (_c == QMetaObject::QueryPropertyScriptable) {
        _id -= 3;
    } else if (_c == QMetaObject::QueryPropertyStored) {
        _id -= 3;
    } else if (_c == QMetaObject::QueryPropertyEditable) {
        _id -= 3;
    } else if (_c == QMetaObject::QueryPropertyUser) {
        _id -= 3;
    }
#endif // QT_NO_PROPERTIES
    return _id;
}

// SIGNAL 0
void Qt3DRender::QDispatchCompute::workGroupXChanged()
{
    QMetaObject::activate(this, &staticMetaObject, 0, Q_NULLPTR);
}

// SIGNAL 1
void Qt3DRender::QDispatchCompute::workGroupYChanged()
{
    QMetaObject::activate(this, &staticMetaObject, 1, Q_NULLPTR);
}

// SIGNAL 2
void Qt3DRender::QDispatchCompute::workGroupZChanged()
{
    QMetaObject::activate(this, &staticMetaObject, 2, Q_NULLPTR);
}
QT_END_MOC_NAMESPACE
