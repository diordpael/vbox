/****************************************************************************
** Meta object code from reading C++ file 'qabstractsceneparser.h'
**
** Created by: The Qt Meta Object Compiler version 67 (Qt 5.6.3)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "../../io/qabstractsceneparser.h"
#include <QtCore/qbytearray.h>
#include <QtCore/qmetatype.h>
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'qabstractsceneparser.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 67
#error "This file was generated using the moc from 5.6.3. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
struct qt_meta_stringdata_Qt3DRender__QAbstractSceneParser_t {
    QByteArrayData data[11];
    char stringdata0[128];
};
#define QT_MOC_LITERAL(idx, ofs, len) \
    Q_STATIC_BYTE_ARRAY_DATA_HEADER_INITIALIZER_WITH_OFFSET(len, \
    qptrdiff(offsetof(qt_meta_stringdata_Qt3DRender__QAbstractSceneParser_t, stringdata0) + ofs \
        - idx * sizeof(QByteArrayData)) \
    )
static const qt_meta_stringdata_Qt3DRender__QAbstractSceneParser_t qt_meta_stringdata_Qt3DRender__QAbstractSceneParser = {
    {
QT_MOC_LITERAL(0, 0, 32), // "Qt3DRender::QAbstractSceneParser"
QT_MOC_LITERAL(1, 33, 19), // "parserStatusChanged"
QT_MOC_LITERAL(2, 53, 0), // ""
QT_MOC_LITERAL(3, 54, 12), // "ParserStatus"
QT_MOC_LITERAL(4, 67, 12), // "parserStatus"
QT_MOC_LITERAL(5, 80, 13), // "errorsChanged"
QT_MOC_LITERAL(6, 94, 6), // "errors"
QT_MOC_LITERAL(7, 101, 5), // "Empty"
QT_MOC_LITERAL(8, 107, 7), // "Loading"
QT_MOC_LITERAL(9, 115, 6), // "Loaded"
QT_MOC_LITERAL(10, 122, 5) // "Error"

    },
    "Qt3DRender::QAbstractSceneParser\0"
    "parserStatusChanged\0\0ParserStatus\0"
    "parserStatus\0errorsChanged\0errors\0"
    "Empty\0Loading\0Loaded\0Error"
};
#undef QT_MOC_LITERAL

static const uint qt_meta_data_Qt3DRender__QAbstractSceneParser[] = {

 // content:
       7,       // revision
       0,       // classname
       0,    0, // classinfo
       2,   14, // methods
       2,   30, // properties
       1,   38, // enums/sets
       0,    0, // constructors
       0,       // flags
       2,       // signalCount

 // signals: name, argc, parameters, tag, flags
       1,    1,   24,    2, 0x06 /* Public */,
       5,    1,   27,    2, 0x06 /* Public */,

 // signals: parameters
    QMetaType::Void, 0x80000000 | 3,    4,
    QMetaType::Void, QMetaType::QStringList,    6,

 // properties: name, type, flags
       4, 0x80000000 | 3, 0x00495009,
       6, QMetaType::QStringList, 0x00495001,

 // properties: notify_signal_id
       0,
       1,

 // enums: name, flags, count, data
       3, 0x0,    4,   42,

 // enum data: key, value
       7, uint(Qt3DRender::QAbstractSceneParser::Empty),
       8, uint(Qt3DRender::QAbstractSceneParser::Loading),
       9, uint(Qt3DRender::QAbstractSceneParser::Loaded),
      10, uint(Qt3DRender::QAbstractSceneParser::Error),

       0        // eod
};

void Qt3DRender::QAbstractSceneParser::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        QAbstractSceneParser *_t = static_cast<QAbstractSceneParser *>(_o);
        Q_UNUSED(_t)
        switch (_id) {
        case 0: _t->parserStatusChanged((*reinterpret_cast< ParserStatus(*)>(_a[1]))); break;
        case 1: _t->errorsChanged((*reinterpret_cast< const QStringList(*)>(_a[1]))); break;
        default: ;
        }
    } else if (_c == QMetaObject::IndexOfMethod) {
        int *result = reinterpret_cast<int *>(_a[0]);
        void **func = reinterpret_cast<void **>(_a[1]);
        {
            typedef void (QAbstractSceneParser::*_t)(ParserStatus );
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&QAbstractSceneParser::parserStatusChanged)) {
                *result = 0;
                return;
            }
        }
        {
            typedef void (QAbstractSceneParser::*_t)(const QStringList & );
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&QAbstractSceneParser::errorsChanged)) {
                *result = 1;
                return;
            }
        }
    }
#ifndef QT_NO_PROPERTIES
    else if (_c == QMetaObject::ReadProperty) {
        QAbstractSceneParser *_t = static_cast<QAbstractSceneParser *>(_o);
        Q_UNUSED(_t)
        void *_v = _a[0];
        switch (_id) {
        case 0: *reinterpret_cast< ParserStatus*>(_v) = _t->parserStatus(); break;
        case 1: *reinterpret_cast< QStringList*>(_v) = _t->errors(); break;
        default: break;
        }
    } else if (_c == QMetaObject::WriteProperty) {
    } else if (_c == QMetaObject::ResetProperty) {
    }
#endif // QT_NO_PROPERTIES
}

const QMetaObject Qt3DRender::QAbstractSceneParser::staticMetaObject = {
    { &QObject::staticMetaObject, qt_meta_stringdata_Qt3DRender__QAbstractSceneParser.data,
      qt_meta_data_Qt3DRender__QAbstractSceneParser,  qt_static_metacall, Q_NULLPTR, Q_NULLPTR}
};


const QMetaObject *Qt3DRender::QAbstractSceneParser::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->dynamicMetaObject() : &staticMetaObject;
}

void *Qt3DRender::QAbstractSceneParser::qt_metacast(const char *_clname)
{
    if (!_clname) return Q_NULLPTR;
    if (!strcmp(_clname, qt_meta_stringdata_Qt3DRender__QAbstractSceneParser.stringdata0))
        return static_cast<void*>(const_cast< QAbstractSceneParser*>(this));
    return QObject::qt_metacast(_clname);
}

int Qt3DRender::QAbstractSceneParser::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QObject::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 2)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 2;
    } else if (_c == QMetaObject::RegisterMethodArgumentMetaType) {
        if (_id < 2)
            *reinterpret_cast<int*>(_a[0]) = -1;
        _id -= 2;
    }
#ifndef QT_NO_PROPERTIES
   else if (_c == QMetaObject::ReadProperty || _c == QMetaObject::WriteProperty
            || _c == QMetaObject::ResetProperty || _c == QMetaObject::RegisterPropertyMetaType) {
        qt_static_metacall(this, _c, _id, _a);
        _id -= 2;
    } else if (_c == QMetaObject::QueryPropertyDesignable) {
        _id -= 2;
    } else if (_c == QMetaObject::QueryPropertyScriptable) {
        _id -= 2;
    } else if (_c == QMetaObject::QueryPropertyStored) {
        _id -= 2;
    } else if (_c == QMetaObject::QueryPropertyEditable) {
        _id -= 2;
    } else if (_c == QMetaObject::QueryPropertyUser) {
        _id -= 2;
    }
#endif // QT_NO_PROPERTIES
    return _id;
}

// SIGNAL 0
void Qt3DRender::QAbstractSceneParser::parserStatusChanged(ParserStatus _t1)
{
    void *_a[] = { Q_NULLPTR, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 0, _a);
}

// SIGNAL 1
void Qt3DRender::QAbstractSceneParser::errorsChanged(const QStringList & _t1)
{
    void *_a[] = { Q_NULLPTR, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 1, _a);
}
QT_END_MOC_NAMESPACE
