/****************************************************************************
** Meta object code from reading C++ file 'qforwardrenderer.h'
**
** Created by: The Qt Meta Object Compiler version 67 (Qt 5.6.3)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "../../defaults/qforwardrenderer.h"
#include <QtCore/qbytearray.h>
#include <QtCore/qmetatype.h>
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'qforwardrenderer.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 67
#error "This file was generated using the moc from 5.6.3. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
struct qt_meta_stringdata_Qt3DRender__QForwardRenderer_t {
    QByteArrayData data[12];
    char stringdata0[172];
};
#define QT_MOC_LITERAL(idx, ofs, len) \
    Q_STATIC_BYTE_ARRAY_DATA_HEADER_INITIALIZER_WITH_OFFSET(len, \
    qptrdiff(offsetof(qt_meta_stringdata_Qt3DRender__QForwardRenderer_t, stringdata0) + ofs \
        - idx * sizeof(QByteArrayData)) \
    )
static const qt_meta_stringdata_Qt3DRender__QForwardRenderer_t qt_meta_stringdata_Qt3DRender__QForwardRenderer = {
    {
QT_MOC_LITERAL(0, 0, 28), // "Qt3DRender::QForwardRenderer"
QT_MOC_LITERAL(1, 29, 19), // "viewportRectChanged"
QT_MOC_LITERAL(2, 49, 0), // ""
QT_MOC_LITERAL(3, 50, 12), // "viewportRect"
QT_MOC_LITERAL(4, 63, 17), // "clearColorChanged"
QT_MOC_LITERAL(5, 81, 10), // "clearColor"
QT_MOC_LITERAL(6, 92, 13), // "cameraChanged"
QT_MOC_LITERAL(7, 106, 18), // "Qt3DCore::QEntity*"
QT_MOC_LITERAL(8, 125, 6), // "camera"
QT_MOC_LITERAL(9, 132, 15), // "setViewportRect"
QT_MOC_LITERAL(10, 148, 13), // "setClearColor"
QT_MOC_LITERAL(11, 162, 9) // "setCamera"

    },
    "Qt3DRender::QForwardRenderer\0"
    "viewportRectChanged\0\0viewportRect\0"
    "clearColorChanged\0clearColor\0cameraChanged\0"
    "Qt3DCore::QEntity*\0camera\0setViewportRect\0"
    "setClearColor\0setCamera"
};
#undef QT_MOC_LITERAL

static const uint qt_meta_data_Qt3DRender__QForwardRenderer[] = {

 // content:
       7,       // revision
       0,       // classname
       0,    0, // classinfo
       6,   14, // methods
       3,   62, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       3,       // signalCount

 // signals: name, argc, parameters, tag, flags
       1,    1,   44,    2, 0x06 /* Public */,
       4,    1,   47,    2, 0x06 /* Public */,
       6,    1,   50,    2, 0x06 /* Public */,

 // slots: name, argc, parameters, tag, flags
       9,    1,   53,    2, 0x0a /* Public */,
      10,    1,   56,    2, 0x0a /* Public */,
      11,    1,   59,    2, 0x0a /* Public */,

 // signals: parameters
    QMetaType::Void, QMetaType::QRectF,    3,
    QMetaType::Void, QMetaType::QColor,    5,
    QMetaType::Void, 0x80000000 | 7,    8,

 // slots: parameters
    QMetaType::Void, QMetaType::QRectF,    3,
    QMetaType::Void, QMetaType::QColor,    5,
    QMetaType::Void, 0x80000000 | 7,    8,

 // properties: name, type, flags
       3, QMetaType::QRectF, 0x00495103,
       5, QMetaType::QColor, 0x00495103,
       8, 0x80000000 | 7, 0x0049510b,

 // properties: notify_signal_id
       0,
       1,
       2,

       0        // eod
};

void Qt3DRender::QForwardRenderer::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        QForwardRenderer *_t = static_cast<QForwardRenderer *>(_o);
        Q_UNUSED(_t)
        switch (_id) {
        case 0: _t->viewportRectChanged((*reinterpret_cast< const QRectF(*)>(_a[1]))); break;
        case 1: _t->clearColorChanged((*reinterpret_cast< const QColor(*)>(_a[1]))); break;
        case 2: _t->cameraChanged((*reinterpret_cast< Qt3DCore::QEntity*(*)>(_a[1]))); break;
        case 3: _t->setViewportRect((*reinterpret_cast< const QRectF(*)>(_a[1]))); break;
        case 4: _t->setClearColor((*reinterpret_cast< const QColor(*)>(_a[1]))); break;
        case 5: _t->setCamera((*reinterpret_cast< Qt3DCore::QEntity*(*)>(_a[1]))); break;
        default: ;
        }
    } else if (_c == QMetaObject::IndexOfMethod) {
        int *result = reinterpret_cast<int *>(_a[0]);
        void **func = reinterpret_cast<void **>(_a[1]);
        {
            typedef void (QForwardRenderer::*_t)(const QRectF & );
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&QForwardRenderer::viewportRectChanged)) {
                *result = 0;
                return;
            }
        }
        {
            typedef void (QForwardRenderer::*_t)(const QColor & );
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&QForwardRenderer::clearColorChanged)) {
                *result = 1;
                return;
            }
        }
        {
            typedef void (QForwardRenderer::*_t)(Qt3DCore::QEntity * );
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&QForwardRenderer::cameraChanged)) {
                *result = 2;
                return;
            }
        }
    }
#ifndef QT_NO_PROPERTIES
    else if (_c == QMetaObject::ReadProperty) {
        QForwardRenderer *_t = static_cast<QForwardRenderer *>(_o);
        Q_UNUSED(_t)
        void *_v = _a[0];
        switch (_id) {
        case 0: *reinterpret_cast< QRectF*>(_v) = _t->viewportRect(); break;
        case 1: *reinterpret_cast< QColor*>(_v) = _t->clearColor(); break;
        case 2: *reinterpret_cast< Qt3DCore::QEntity**>(_v) = _t->camera(); break;
        default: break;
        }
    } else if (_c == QMetaObject::WriteProperty) {
        QForwardRenderer *_t = static_cast<QForwardRenderer *>(_o);
        Q_UNUSED(_t)
        void *_v = _a[0];
        switch (_id) {
        case 0: _t->setViewportRect(*reinterpret_cast< QRectF*>(_v)); break;
        case 1: _t->setClearColor(*reinterpret_cast< QColor*>(_v)); break;
        case 2: _t->setCamera(*reinterpret_cast< Qt3DCore::QEntity**>(_v)); break;
        default: break;
        }
    } else if (_c == QMetaObject::ResetProperty) {
    }
#endif // QT_NO_PROPERTIES
}

const QMetaObject Qt3DRender::QForwardRenderer::staticMetaObject = {
    { &QTechniqueFilter::staticMetaObject, qt_meta_stringdata_Qt3DRender__QForwardRenderer.data,
      qt_meta_data_Qt3DRender__QForwardRenderer,  qt_static_metacall, Q_NULLPTR, Q_NULLPTR}
};


const QMetaObject *Qt3DRender::QForwardRenderer::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->dynamicMetaObject() : &staticMetaObject;
}

void *Qt3DRender::QForwardRenderer::qt_metacast(const char *_clname)
{
    if (!_clname) return Q_NULLPTR;
    if (!strcmp(_clname, qt_meta_stringdata_Qt3DRender__QForwardRenderer.stringdata0))
        return static_cast<void*>(const_cast< QForwardRenderer*>(this));
    return QTechniqueFilter::qt_metacast(_clname);
}

int Qt3DRender::QForwardRenderer::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QTechniqueFilter::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 6)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 6;
    } else if (_c == QMetaObject::RegisterMethodArgumentMetaType) {
        if (_id < 6)
            *reinterpret_cast<int*>(_a[0]) = -1;
        _id -= 6;
    }
#ifndef QT_NO_PROPERTIES
   else if (_c == QMetaObject::ReadProperty || _c == QMetaObject::WriteProperty
            || _c == QMetaObject::ResetProperty || _c == QMetaObject::RegisterPropertyMetaType) {
        qt_static_metacall(this, _c, _id, _a);
        _id -= 3;
    } else if (_c == QMetaObject::QueryPropertyDesignable) {
        _id -= 3;
    } else if (_c == QMetaObject::QueryPropertyScriptable) {
        _id -= 3;
    } else if (_c == QMetaObject::QueryPropertyStored) {
        _id -= 3;
    } else if (_c == QMetaObject::QueryPropertyEditable) {
        _id -= 3;
    } else if (_c == QMetaObject::QueryPropertyUser) {
        _id -= 3;
    }
#endif // QT_NO_PROPERTIES
    return _id;
}

// SIGNAL 0
void Qt3DRender::QForwardRenderer::viewportRectChanged(const QRectF & _t1)
{
    void *_a[] = { Q_NULLPTR, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 0, _a);
}

// SIGNAL 1
void Qt3DRender::QForwardRenderer::clearColorChanged(const QColor & _t1)
{
    void *_a[] = { Q_NULLPTR, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 1, _a);
}

// SIGNAL 2
void Qt3DRender::QForwardRenderer::cameraChanged(Qt3DCore::QEntity * _t1)
{
    void *_a[] = { Q_NULLPTR, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 2, _a);
}
QT_END_MOC_NAMESPACE
