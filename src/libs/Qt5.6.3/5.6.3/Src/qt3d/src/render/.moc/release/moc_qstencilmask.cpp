/****************************************************************************
** Meta object code from reading C++ file 'qstencilmask.h'
**
** Created by: The Qt Meta Object Compiler version 67 (Qt 5.6.3)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "../../renderstates/qstencilmask.h"
#include <QtCore/qbytearray.h>
#include <QtCore/qmetatype.h>
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'qstencilmask.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 67
#error "This file was generated using the moc from 5.6.3. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
struct qt_meta_stringdata_Qt3DRender__QStencilMask_t {
    QByteArrayData data[9];
    char stringdata0[108];
};
#define QT_MOC_LITERAL(idx, ofs, len) \
    Q_STATIC_BYTE_ARRAY_DATA_HEADER_INITIALIZER_WITH_OFFSET(len, \
    qptrdiff(offsetof(qt_meta_stringdata_Qt3DRender__QStencilMask_t, stringdata0) + ofs \
        - idx * sizeof(QByteArrayData)) \
    )
static const qt_meta_stringdata_Qt3DRender__QStencilMask_t qt_meta_stringdata_Qt3DRender__QStencilMask = {
    {
QT_MOC_LITERAL(0, 0, 24), // "Qt3DRender::QStencilMask"
QT_MOC_LITERAL(1, 25, 16), // "frontMaskChanged"
QT_MOC_LITERAL(2, 42, 0), // ""
QT_MOC_LITERAL(3, 43, 9), // "frontMask"
QT_MOC_LITERAL(4, 53, 15), // "backMaskChanged"
QT_MOC_LITERAL(5, 69, 8), // "backMask"
QT_MOC_LITERAL(6, 78, 12), // "setFrontMask"
QT_MOC_LITERAL(7, 91, 4), // "mask"
QT_MOC_LITERAL(8, 96, 11) // "setBackMask"

    },
    "Qt3DRender::QStencilMask\0frontMaskChanged\0"
    "\0frontMask\0backMaskChanged\0backMask\0"
    "setFrontMask\0mask\0setBackMask"
};
#undef QT_MOC_LITERAL

static const uint qt_meta_data_Qt3DRender__QStencilMask[] = {

 // content:
       7,       // revision
       0,       // classname
       0,    0, // classinfo
       4,   14, // methods
       2,   46, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       2,       // signalCount

 // signals: name, argc, parameters, tag, flags
       1,    1,   34,    2, 0x06 /* Public */,
       4,    1,   37,    2, 0x06 /* Public */,

 // slots: name, argc, parameters, tag, flags
       6,    1,   40,    2, 0x0a /* Public */,
       8,    1,   43,    2, 0x0a /* Public */,

 // signals: parameters
    QMetaType::Void, QMetaType::UInt,    3,
    QMetaType::Void, QMetaType::UInt,    5,

 // slots: parameters
    QMetaType::Void, QMetaType::UInt,    7,
    QMetaType::Void, QMetaType::UInt,    7,

 // properties: name, type, flags
       3, QMetaType::UInt, 0x00495103,
       5, QMetaType::UInt, 0x00495103,

 // properties: notify_signal_id
       0,
       1,

       0        // eod
};

void Qt3DRender::QStencilMask::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        QStencilMask *_t = static_cast<QStencilMask *>(_o);
        Q_UNUSED(_t)
        switch (_id) {
        case 0: _t->frontMaskChanged((*reinterpret_cast< uint(*)>(_a[1]))); break;
        case 1: _t->backMaskChanged((*reinterpret_cast< uint(*)>(_a[1]))); break;
        case 2: _t->setFrontMask((*reinterpret_cast< uint(*)>(_a[1]))); break;
        case 3: _t->setBackMask((*reinterpret_cast< uint(*)>(_a[1]))); break;
        default: ;
        }
    } else if (_c == QMetaObject::IndexOfMethod) {
        int *result = reinterpret_cast<int *>(_a[0]);
        void **func = reinterpret_cast<void **>(_a[1]);
        {
            typedef void (QStencilMask::*_t)(uint );
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&QStencilMask::frontMaskChanged)) {
                *result = 0;
                return;
            }
        }
        {
            typedef void (QStencilMask::*_t)(uint );
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&QStencilMask::backMaskChanged)) {
                *result = 1;
                return;
            }
        }
    }
#ifndef QT_NO_PROPERTIES
    else if (_c == QMetaObject::ReadProperty) {
        QStencilMask *_t = static_cast<QStencilMask *>(_o);
        Q_UNUSED(_t)
        void *_v = _a[0];
        switch (_id) {
        case 0: *reinterpret_cast< uint*>(_v) = _t->frontMask(); break;
        case 1: *reinterpret_cast< uint*>(_v) = _t->backMask(); break;
        default: break;
        }
    } else if (_c == QMetaObject::WriteProperty) {
        QStencilMask *_t = static_cast<QStencilMask *>(_o);
        Q_UNUSED(_t)
        void *_v = _a[0];
        switch (_id) {
        case 0: _t->setFrontMask(*reinterpret_cast< uint*>(_v)); break;
        case 1: _t->setBackMask(*reinterpret_cast< uint*>(_v)); break;
        default: break;
        }
    } else if (_c == QMetaObject::ResetProperty) {
    }
#endif // QT_NO_PROPERTIES
}

const QMetaObject Qt3DRender::QStencilMask::staticMetaObject = {
    { &QRenderState::staticMetaObject, qt_meta_stringdata_Qt3DRender__QStencilMask.data,
      qt_meta_data_Qt3DRender__QStencilMask,  qt_static_metacall, Q_NULLPTR, Q_NULLPTR}
};


const QMetaObject *Qt3DRender::QStencilMask::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->dynamicMetaObject() : &staticMetaObject;
}

void *Qt3DRender::QStencilMask::qt_metacast(const char *_clname)
{
    if (!_clname) return Q_NULLPTR;
    if (!strcmp(_clname, qt_meta_stringdata_Qt3DRender__QStencilMask.stringdata0))
        return static_cast<void*>(const_cast< QStencilMask*>(this));
    return QRenderState::qt_metacast(_clname);
}

int Qt3DRender::QStencilMask::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QRenderState::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 4)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 4;
    } else if (_c == QMetaObject::RegisterMethodArgumentMetaType) {
        if (_id < 4)
            *reinterpret_cast<int*>(_a[0]) = -1;
        _id -= 4;
    }
#ifndef QT_NO_PROPERTIES
   else if (_c == QMetaObject::ReadProperty || _c == QMetaObject::WriteProperty
            || _c == QMetaObject::ResetProperty || _c == QMetaObject::RegisterPropertyMetaType) {
        qt_static_metacall(this, _c, _id, _a);
        _id -= 2;
    } else if (_c == QMetaObject::QueryPropertyDesignable) {
        _id -= 2;
    } else if (_c == QMetaObject::QueryPropertyScriptable) {
        _id -= 2;
    } else if (_c == QMetaObject::QueryPropertyStored) {
        _id -= 2;
    } else if (_c == QMetaObject::QueryPropertyEditable) {
        _id -= 2;
    } else if (_c == QMetaObject::QueryPropertyUser) {
        _id -= 2;
    }
#endif // QT_NO_PROPERTIES
    return _id;
}

// SIGNAL 0
void Qt3DRender::QStencilMask::frontMaskChanged(uint _t1)
{
    void *_a[] = { Q_NULLPTR, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 0, _a);
}

// SIGNAL 1
void Qt3DRender::QStencilMask::backMaskChanged(uint _t1)
{
    void *_a[] = { Q_NULLPTR, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 1, _a);
}
QT_END_MOC_NAMESPACE
