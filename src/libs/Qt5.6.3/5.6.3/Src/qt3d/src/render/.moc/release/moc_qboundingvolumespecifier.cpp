/****************************************************************************
** Meta object code from reading C++ file 'qboundingvolumespecifier.h'
**
** Created by: The Qt Meta Object Compiler version 67 (Qt 5.6.3)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "../../geometry/qboundingvolumespecifier.h"
#include <QtCore/qbytearray.h>
#include <QtCore/qmetatype.h>
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'qboundingvolumespecifier.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 67
#error "This file was generated using the moc from 5.6.3. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
struct qt_meta_stringdata_Qt3DRender__QBoundingVolumeSpecifier_t {
    QByteArrayData data[7];
    char stringdata0[154];
};
#define QT_MOC_LITERAL(idx, ofs, len) \
    Q_STATIC_BYTE_ARRAY_DATA_HEADER_INITIALIZER_WITH_OFFSET(len, \
    qptrdiff(offsetof(qt_meta_stringdata_Qt3DRender__QBoundingVolumeSpecifier_t, stringdata0) + ofs \
        - idx * sizeof(QByteArrayData)) \
    )
static const qt_meta_stringdata_Qt3DRender__QBoundingVolumeSpecifier_t qt_meta_stringdata_Qt3DRender__QBoundingVolumeSpecifier = {
    {
QT_MOC_LITERAL(0, 0, 36), // "Qt3DRender::QBoundingVolumeSp..."
QT_MOC_LITERAL(1, 37, 24), // "positionAttributeChanged"
QT_MOC_LITERAL(2, 62, 0), // ""
QT_MOC_LITERAL(3, 63, 19), // "QAbstractAttribute*"
QT_MOC_LITERAL(4, 83, 17), // "positionAttribute"
QT_MOC_LITERAL(5, 101, 20), // "setPositionAttribute"
QT_MOC_LITERAL(6, 122, 31) // "Qt3DRender::QAbstractAttribute*"

    },
    "Qt3DRender::QBoundingVolumeSpecifier\0"
    "positionAttributeChanged\0\0QAbstractAttribute*\0"
    "positionAttribute\0setPositionAttribute\0"
    "Qt3DRender::QAbstractAttribute*"
};
#undef QT_MOC_LITERAL

static const uint qt_meta_data_Qt3DRender__QBoundingVolumeSpecifier[] = {

 // content:
       7,       // revision
       0,       // classname
       0,    0, // classinfo
       2,   14, // methods
       1,   30, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       1,       // signalCount

 // signals: name, argc, parameters, tag, flags
       1,    1,   24,    2, 0x06 /* Public */,

 // slots: name, argc, parameters, tag, flags
       5,    1,   27,    2, 0x0a /* Public */,

 // signals: parameters
    QMetaType::Void, 0x80000000 | 3,    4,

 // slots: parameters
    QMetaType::Void, 0x80000000 | 3,    4,

 // properties: name, type, flags
       4, 0x80000000 | 6, 0x0049510b,

 // properties: notify_signal_id
       0,

       0        // eod
};

void Qt3DRender::QBoundingVolumeSpecifier::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        QBoundingVolumeSpecifier *_t = static_cast<QBoundingVolumeSpecifier *>(_o);
        Q_UNUSED(_t)
        switch (_id) {
        case 0: _t->positionAttributeChanged((*reinterpret_cast< QAbstractAttribute*(*)>(_a[1]))); break;
        case 1: _t->setPositionAttribute((*reinterpret_cast< QAbstractAttribute*(*)>(_a[1]))); break;
        default: ;
        }
    } else if (_c == QMetaObject::IndexOfMethod) {
        int *result = reinterpret_cast<int *>(_a[0]);
        void **func = reinterpret_cast<void **>(_a[1]);
        {
            typedef void (QBoundingVolumeSpecifier::*_t)(QAbstractAttribute * );
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&QBoundingVolumeSpecifier::positionAttributeChanged)) {
                *result = 0;
                return;
            }
        }
    }
#ifndef QT_NO_PROPERTIES
    else if (_c == QMetaObject::ReadProperty) {
        QBoundingVolumeSpecifier *_t = static_cast<QBoundingVolumeSpecifier *>(_o);
        Q_UNUSED(_t)
        void *_v = _a[0];
        switch (_id) {
        case 0: *reinterpret_cast< Qt3DRender::QAbstractAttribute**>(_v) = _t->positionAttribute(); break;
        default: break;
        }
    } else if (_c == QMetaObject::WriteProperty) {
        QBoundingVolumeSpecifier *_t = static_cast<QBoundingVolumeSpecifier *>(_o);
        Q_UNUSED(_t)
        void *_v = _a[0];
        switch (_id) {
        case 0: _t->setPositionAttribute(*reinterpret_cast< Qt3DRender::QAbstractAttribute**>(_v)); break;
        default: break;
        }
    } else if (_c == QMetaObject::ResetProperty) {
    }
#endif // QT_NO_PROPERTIES
}

const QMetaObject Qt3DRender::QBoundingVolumeSpecifier::staticMetaObject = {
    { &QObject::staticMetaObject, qt_meta_stringdata_Qt3DRender__QBoundingVolumeSpecifier.data,
      qt_meta_data_Qt3DRender__QBoundingVolumeSpecifier,  qt_static_metacall, Q_NULLPTR, Q_NULLPTR}
};


const QMetaObject *Qt3DRender::QBoundingVolumeSpecifier::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->dynamicMetaObject() : &staticMetaObject;
}

void *Qt3DRender::QBoundingVolumeSpecifier::qt_metacast(const char *_clname)
{
    if (!_clname) return Q_NULLPTR;
    if (!strcmp(_clname, qt_meta_stringdata_Qt3DRender__QBoundingVolumeSpecifier.stringdata0))
        return static_cast<void*>(const_cast< QBoundingVolumeSpecifier*>(this));
    return QObject::qt_metacast(_clname);
}

int Qt3DRender::QBoundingVolumeSpecifier::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QObject::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 2)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 2;
    } else if (_c == QMetaObject::RegisterMethodArgumentMetaType) {
        if (_id < 2)
            *reinterpret_cast<int*>(_a[0]) = -1;
        _id -= 2;
    }
#ifndef QT_NO_PROPERTIES
   else if (_c == QMetaObject::ReadProperty || _c == QMetaObject::WriteProperty
            || _c == QMetaObject::ResetProperty || _c == QMetaObject::RegisterPropertyMetaType) {
        qt_static_metacall(this, _c, _id, _a);
        _id -= 1;
    } else if (_c == QMetaObject::QueryPropertyDesignable) {
        _id -= 1;
    } else if (_c == QMetaObject::QueryPropertyScriptable) {
        _id -= 1;
    } else if (_c == QMetaObject::QueryPropertyStored) {
        _id -= 1;
    } else if (_c == QMetaObject::QueryPropertyEditable) {
        _id -= 1;
    } else if (_c == QMetaObject::QueryPropertyUser) {
        _id -= 1;
    }
#endif // QT_NO_PROPERTIES
    return _id;
}

// SIGNAL 0
void Qt3DRender::QBoundingVolumeSpecifier::positionAttributeChanged(QAbstractAttribute * _t1)
{
    void *_a[] = { Q_NULLPTR, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 0, _a);
}
QT_END_MOC_NAMESPACE
