/****************************************************************************
** Meta object code from reading C++ file 'qbuffer.h'
**
** Created by: The Qt Meta Object Compiler version 67 (Qt 5.6.3)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "../../geometry/qbuffer.h"
#include <QtCore/qbytearray.h>
#include <QtCore/qmetatype.h>
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'qbuffer.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 67
#error "This file was generated using the moc from 5.6.3. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
struct qt_meta_stringdata_Qt3DRender__QBuffer_t {
    QByteArrayData data[26];
    char stringdata0[281];
};
#define QT_MOC_LITERAL(idx, ofs, len) \
    Q_STATIC_BYTE_ARRAY_DATA_HEADER_INITIALIZER_WITH_OFFSET(len, \
    qptrdiff(offsetof(qt_meta_stringdata_Qt3DRender__QBuffer_t, stringdata0) + ofs \
        - idx * sizeof(QByteArrayData)) \
    )
static const qt_meta_stringdata_Qt3DRender__QBuffer_t qt_meta_stringdata_Qt3DRender__QBuffer = {
    {
QT_MOC_LITERAL(0, 0, 19), // "Qt3DRender::QBuffer"
QT_MOC_LITERAL(1, 20, 11), // "typeChanged"
QT_MOC_LITERAL(2, 32, 0), // ""
QT_MOC_LITERAL(3, 33, 10), // "BufferType"
QT_MOC_LITERAL(4, 44, 4), // "type"
QT_MOC_LITERAL(5, 49, 12), // "usageChanged"
QT_MOC_LITERAL(6, 62, 9), // "UsageType"
QT_MOC_LITERAL(7, 72, 5), // "usage"
QT_MOC_LITERAL(8, 78, 11), // "syncChanged"
QT_MOC_LITERAL(9, 90, 4), // "sync"
QT_MOC_LITERAL(10, 95, 7), // "setType"
QT_MOC_LITERAL(11, 103, 8), // "setUsage"
QT_MOC_LITERAL(12, 112, 7), // "setSync"
QT_MOC_LITERAL(13, 120, 12), // "VertexBuffer"
QT_MOC_LITERAL(14, 133, 11), // "IndexBuffer"
QT_MOC_LITERAL(15, 145, 15), // "PixelPackBuffer"
QT_MOC_LITERAL(16, 161, 17), // "PixelUnpackBuffer"
QT_MOC_LITERAL(17, 179, 10), // "StreamDraw"
QT_MOC_LITERAL(18, 190, 10), // "StreamRead"
QT_MOC_LITERAL(19, 201, 10), // "StreamCopy"
QT_MOC_LITERAL(20, 212, 10), // "StaticDraw"
QT_MOC_LITERAL(21, 223, 10), // "StaticRead"
QT_MOC_LITERAL(22, 234, 10), // "StaticCopy"
QT_MOC_LITERAL(23, 245, 11), // "DynamicDraw"
QT_MOC_LITERAL(24, 257, 11), // "DynamicRead"
QT_MOC_LITERAL(25, 269, 11) // "DynamicCopy"

    },
    "Qt3DRender::QBuffer\0typeChanged\0\0"
    "BufferType\0type\0usageChanged\0UsageType\0"
    "usage\0syncChanged\0sync\0setType\0setUsage\0"
    "setSync\0VertexBuffer\0IndexBuffer\0"
    "PixelPackBuffer\0PixelUnpackBuffer\0"
    "StreamDraw\0StreamRead\0StreamCopy\0"
    "StaticDraw\0StaticRead\0StaticCopy\0"
    "DynamicDraw\0DynamicRead\0DynamicCopy"
};
#undef QT_MOC_LITERAL

static const uint qt_meta_data_Qt3DRender__QBuffer[] = {

 // content:
       7,       // revision
       0,       // classname
       0,    0, // classinfo
       6,   14, // methods
       3,   62, // properties
       2,   74, // enums/sets
       0,    0, // constructors
       0,       // flags
       3,       // signalCount

 // signals: name, argc, parameters, tag, flags
       1,    1,   44,    2, 0x06 /* Public */,
       5,    1,   47,    2, 0x06 /* Public */,
       8,    1,   50,    2, 0x06 /* Public */,

 // slots: name, argc, parameters, tag, flags
      10,    1,   53,    2, 0x0a /* Public */,
      11,    1,   56,    2, 0x0a /* Public */,
      12,    1,   59,    2, 0x0a /* Public */,

 // signals: parameters
    QMetaType::Void, 0x80000000 | 3,    4,
    QMetaType::Void, 0x80000000 | 6,    7,
    QMetaType::Void, QMetaType::Bool,    9,

 // slots: parameters
    QMetaType::Void, 0x80000000 | 3,    4,
    QMetaType::Void, 0x80000000 | 6,    7,
    QMetaType::Void, QMetaType::Bool,    9,

 // properties: name, type, flags
       4, 0x80000000 | 3, 0x0049510b,
       7, 0x80000000 | 6, 0x0049510b,
       9, QMetaType::Bool, 0x00495103,

 // properties: notify_signal_id
       0,
       1,
       2,

 // enums: name, flags, count, data
       3, 0x0,    4,   82,
       6, 0x0,    9,   90,

 // enum data: key, value
      13, uint(Qt3DRender::QBuffer::VertexBuffer),
      14, uint(Qt3DRender::QBuffer::IndexBuffer),
      15, uint(Qt3DRender::QBuffer::PixelPackBuffer),
      16, uint(Qt3DRender::QBuffer::PixelUnpackBuffer),
      17, uint(Qt3DRender::QBuffer::StreamDraw),
      18, uint(Qt3DRender::QBuffer::StreamRead),
      19, uint(Qt3DRender::QBuffer::StreamCopy),
      20, uint(Qt3DRender::QBuffer::StaticDraw),
      21, uint(Qt3DRender::QBuffer::StaticRead),
      22, uint(Qt3DRender::QBuffer::StaticCopy),
      23, uint(Qt3DRender::QBuffer::DynamicDraw),
      24, uint(Qt3DRender::QBuffer::DynamicRead),
      25, uint(Qt3DRender::QBuffer::DynamicCopy),

       0        // eod
};

void Qt3DRender::QBuffer::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        QBuffer *_t = static_cast<QBuffer *>(_o);
        Q_UNUSED(_t)
        switch (_id) {
        case 0: _t->typeChanged((*reinterpret_cast< BufferType(*)>(_a[1]))); break;
        case 1: _t->usageChanged((*reinterpret_cast< UsageType(*)>(_a[1]))); break;
        case 2: _t->syncChanged((*reinterpret_cast< bool(*)>(_a[1]))); break;
        case 3: _t->setType((*reinterpret_cast< BufferType(*)>(_a[1]))); break;
        case 4: _t->setUsage((*reinterpret_cast< UsageType(*)>(_a[1]))); break;
        case 5: _t->setSync((*reinterpret_cast< bool(*)>(_a[1]))); break;
        default: ;
        }
    } else if (_c == QMetaObject::IndexOfMethod) {
        int *result = reinterpret_cast<int *>(_a[0]);
        void **func = reinterpret_cast<void **>(_a[1]);
        {
            typedef void (QBuffer::*_t)(BufferType );
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&QBuffer::typeChanged)) {
                *result = 0;
                return;
            }
        }
        {
            typedef void (QBuffer::*_t)(UsageType );
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&QBuffer::usageChanged)) {
                *result = 1;
                return;
            }
        }
        {
            typedef void (QBuffer::*_t)(bool );
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&QBuffer::syncChanged)) {
                *result = 2;
                return;
            }
        }
    }
#ifndef QT_NO_PROPERTIES
    else if (_c == QMetaObject::ReadProperty) {
        QBuffer *_t = static_cast<QBuffer *>(_o);
        Q_UNUSED(_t)
        void *_v = _a[0];
        switch (_id) {
        case 0: *reinterpret_cast< BufferType*>(_v) = _t->type(); break;
        case 1: *reinterpret_cast< UsageType*>(_v) = _t->usage(); break;
        case 2: *reinterpret_cast< bool*>(_v) = _t->isSync(); break;
        default: break;
        }
    } else if (_c == QMetaObject::WriteProperty) {
        QBuffer *_t = static_cast<QBuffer *>(_o);
        Q_UNUSED(_t)
        void *_v = _a[0];
        switch (_id) {
        case 0: _t->setType(*reinterpret_cast< BufferType*>(_v)); break;
        case 1: _t->setUsage(*reinterpret_cast< UsageType*>(_v)); break;
        case 2: _t->setSync(*reinterpret_cast< bool*>(_v)); break;
        default: break;
        }
    } else if (_c == QMetaObject::ResetProperty) {
    }
#endif // QT_NO_PROPERTIES
}

const QMetaObject Qt3DRender::QBuffer::staticMetaObject = {
    { &QAbstractBuffer::staticMetaObject, qt_meta_stringdata_Qt3DRender__QBuffer.data,
      qt_meta_data_Qt3DRender__QBuffer,  qt_static_metacall, Q_NULLPTR, Q_NULLPTR}
};


const QMetaObject *Qt3DRender::QBuffer::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->dynamicMetaObject() : &staticMetaObject;
}

void *Qt3DRender::QBuffer::qt_metacast(const char *_clname)
{
    if (!_clname) return Q_NULLPTR;
    if (!strcmp(_clname, qt_meta_stringdata_Qt3DRender__QBuffer.stringdata0))
        return static_cast<void*>(const_cast< QBuffer*>(this));
    return QAbstractBuffer::qt_metacast(_clname);
}

int Qt3DRender::QBuffer::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QAbstractBuffer::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 6)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 6;
    } else if (_c == QMetaObject::RegisterMethodArgumentMetaType) {
        if (_id < 6)
            *reinterpret_cast<int*>(_a[0]) = -1;
        _id -= 6;
    }
#ifndef QT_NO_PROPERTIES
   else if (_c == QMetaObject::ReadProperty || _c == QMetaObject::WriteProperty
            || _c == QMetaObject::ResetProperty || _c == QMetaObject::RegisterPropertyMetaType) {
        qt_static_metacall(this, _c, _id, _a);
        _id -= 3;
    } else if (_c == QMetaObject::QueryPropertyDesignable) {
        _id -= 3;
    } else if (_c == QMetaObject::QueryPropertyScriptable) {
        _id -= 3;
    } else if (_c == QMetaObject::QueryPropertyStored) {
        _id -= 3;
    } else if (_c == QMetaObject::QueryPropertyEditable) {
        _id -= 3;
    } else if (_c == QMetaObject::QueryPropertyUser) {
        _id -= 3;
    }
#endif // QT_NO_PROPERTIES
    return _id;
}

// SIGNAL 0
void Qt3DRender::QBuffer::typeChanged(BufferType _t1)
{
    void *_a[] = { Q_NULLPTR, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 0, _a);
}

// SIGNAL 1
void Qt3DRender::QBuffer::usageChanged(UsageType _t1)
{
    void *_a[] = { Q_NULLPTR, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 1, _a);
}

// SIGNAL 2
void Qt3DRender::QBuffer::syncChanged(bool _t1)
{
    void *_a[] = { Q_NULLPTR, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 2, _a);
}
QT_END_MOC_NAMESPACE
