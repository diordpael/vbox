/****************************************************************************
** Meta object code from reading C++ file 'qcolormask.h'
**
** Created by: The Qt Meta Object Compiler version 67 (Qt 5.6.3)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "../../renderstates/qcolormask.h"
#include <QtCore/qbytearray.h>
#include <QtCore/qmetatype.h>
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'qcolormask.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 67
#error "This file was generated using the moc from 5.6.3. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
struct qt_meta_stringdata_Qt3DRender__QColorMask_t {
    QByteArrayData data[14];
    char stringdata0[127];
};
#define QT_MOC_LITERAL(idx, ofs, len) \
    Q_STATIC_BYTE_ARRAY_DATA_HEADER_INITIALIZER_WITH_OFFSET(len, \
    qptrdiff(offsetof(qt_meta_stringdata_Qt3DRender__QColorMask_t, stringdata0) + ofs \
        - idx * sizeof(QByteArrayData)) \
    )
static const qt_meta_stringdata_Qt3DRender__QColorMask_t qt_meta_stringdata_Qt3DRender__QColorMask = {
    {
QT_MOC_LITERAL(0, 0, 22), // "Qt3DRender::QColorMask"
QT_MOC_LITERAL(1, 23, 10), // "redChanged"
QT_MOC_LITERAL(2, 34, 0), // ""
QT_MOC_LITERAL(3, 35, 3), // "red"
QT_MOC_LITERAL(4, 39, 12), // "greenChanged"
QT_MOC_LITERAL(5, 52, 5), // "green"
QT_MOC_LITERAL(6, 58, 11), // "blueChanged"
QT_MOC_LITERAL(7, 70, 4), // "blue"
QT_MOC_LITERAL(8, 75, 12), // "alphaChanged"
QT_MOC_LITERAL(9, 88, 5), // "alpha"
QT_MOC_LITERAL(10, 94, 6), // "setRed"
QT_MOC_LITERAL(11, 101, 8), // "setGreen"
QT_MOC_LITERAL(12, 110, 7), // "setBlue"
QT_MOC_LITERAL(13, 118, 8) // "setAlpha"

    },
    "Qt3DRender::QColorMask\0redChanged\0\0"
    "red\0greenChanged\0green\0blueChanged\0"
    "blue\0alphaChanged\0alpha\0setRed\0setGreen\0"
    "setBlue\0setAlpha"
};
#undef QT_MOC_LITERAL

static const uint qt_meta_data_Qt3DRender__QColorMask[] = {

 // content:
       7,       // revision
       0,       // classname
       0,    0, // classinfo
       8,   14, // methods
       4,   78, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       4,       // signalCount

 // signals: name, argc, parameters, tag, flags
       1,    1,   54,    2, 0x06 /* Public */,
       4,    1,   57,    2, 0x06 /* Public */,
       6,    1,   60,    2, 0x06 /* Public */,
       8,    1,   63,    2, 0x06 /* Public */,

 // slots: name, argc, parameters, tag, flags
      10,    1,   66,    2, 0x0a /* Public */,
      11,    1,   69,    2, 0x0a /* Public */,
      12,    1,   72,    2, 0x0a /* Public */,
      13,    1,   75,    2, 0x0a /* Public */,

 // signals: parameters
    QMetaType::Void, QMetaType::Bool,    3,
    QMetaType::Void, QMetaType::Bool,    5,
    QMetaType::Void, QMetaType::Bool,    7,
    QMetaType::Void, QMetaType::Bool,    9,

 // slots: parameters
    QMetaType::Void, QMetaType::Bool,    3,
    QMetaType::Void, QMetaType::Bool,    5,
    QMetaType::Void, QMetaType::Bool,    7,
    QMetaType::Void, QMetaType::Bool,    9,

 // properties: name, type, flags
       3, QMetaType::Bool, 0x00495103,
       5, QMetaType::Bool, 0x00495103,
       7, QMetaType::Bool, 0x00495103,
       9, QMetaType::Bool, 0x00495103,

 // properties: notify_signal_id
       0,
       1,
       2,
       3,

       0        // eod
};

void Qt3DRender::QColorMask::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        QColorMask *_t = static_cast<QColorMask *>(_o);
        Q_UNUSED(_t)
        switch (_id) {
        case 0: _t->redChanged((*reinterpret_cast< bool(*)>(_a[1]))); break;
        case 1: _t->greenChanged((*reinterpret_cast< bool(*)>(_a[1]))); break;
        case 2: _t->blueChanged((*reinterpret_cast< bool(*)>(_a[1]))); break;
        case 3: _t->alphaChanged((*reinterpret_cast< bool(*)>(_a[1]))); break;
        case 4: _t->setRed((*reinterpret_cast< bool(*)>(_a[1]))); break;
        case 5: _t->setGreen((*reinterpret_cast< bool(*)>(_a[1]))); break;
        case 6: _t->setBlue((*reinterpret_cast< bool(*)>(_a[1]))); break;
        case 7: _t->setAlpha((*reinterpret_cast< bool(*)>(_a[1]))); break;
        default: ;
        }
    } else if (_c == QMetaObject::IndexOfMethod) {
        int *result = reinterpret_cast<int *>(_a[0]);
        void **func = reinterpret_cast<void **>(_a[1]);
        {
            typedef void (QColorMask::*_t)(bool );
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&QColorMask::redChanged)) {
                *result = 0;
                return;
            }
        }
        {
            typedef void (QColorMask::*_t)(bool );
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&QColorMask::greenChanged)) {
                *result = 1;
                return;
            }
        }
        {
            typedef void (QColorMask::*_t)(bool );
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&QColorMask::blueChanged)) {
                *result = 2;
                return;
            }
        }
        {
            typedef void (QColorMask::*_t)(bool );
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&QColorMask::alphaChanged)) {
                *result = 3;
                return;
            }
        }
    }
#ifndef QT_NO_PROPERTIES
    else if (_c == QMetaObject::ReadProperty) {
        QColorMask *_t = static_cast<QColorMask *>(_o);
        Q_UNUSED(_t)
        void *_v = _a[0];
        switch (_id) {
        case 0: *reinterpret_cast< bool*>(_v) = _t->isRed(); break;
        case 1: *reinterpret_cast< bool*>(_v) = _t->isGreen(); break;
        case 2: *reinterpret_cast< bool*>(_v) = _t->isBlue(); break;
        case 3: *reinterpret_cast< bool*>(_v) = _t->isAlpha(); break;
        default: break;
        }
    } else if (_c == QMetaObject::WriteProperty) {
        QColorMask *_t = static_cast<QColorMask *>(_o);
        Q_UNUSED(_t)
        void *_v = _a[0];
        switch (_id) {
        case 0: _t->setRed(*reinterpret_cast< bool*>(_v)); break;
        case 1: _t->setGreen(*reinterpret_cast< bool*>(_v)); break;
        case 2: _t->setBlue(*reinterpret_cast< bool*>(_v)); break;
        case 3: _t->setAlpha(*reinterpret_cast< bool*>(_v)); break;
        default: break;
        }
    } else if (_c == QMetaObject::ResetProperty) {
    }
#endif // QT_NO_PROPERTIES
}

const QMetaObject Qt3DRender::QColorMask::staticMetaObject = {
    { &QRenderState::staticMetaObject, qt_meta_stringdata_Qt3DRender__QColorMask.data,
      qt_meta_data_Qt3DRender__QColorMask,  qt_static_metacall, Q_NULLPTR, Q_NULLPTR}
};


const QMetaObject *Qt3DRender::QColorMask::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->dynamicMetaObject() : &staticMetaObject;
}

void *Qt3DRender::QColorMask::qt_metacast(const char *_clname)
{
    if (!_clname) return Q_NULLPTR;
    if (!strcmp(_clname, qt_meta_stringdata_Qt3DRender__QColorMask.stringdata0))
        return static_cast<void*>(const_cast< QColorMask*>(this));
    return QRenderState::qt_metacast(_clname);
}

int Qt3DRender::QColorMask::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QRenderState::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 8)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 8;
    } else if (_c == QMetaObject::RegisterMethodArgumentMetaType) {
        if (_id < 8)
            *reinterpret_cast<int*>(_a[0]) = -1;
        _id -= 8;
    }
#ifndef QT_NO_PROPERTIES
   else if (_c == QMetaObject::ReadProperty || _c == QMetaObject::WriteProperty
            || _c == QMetaObject::ResetProperty || _c == QMetaObject::RegisterPropertyMetaType) {
        qt_static_metacall(this, _c, _id, _a);
        _id -= 4;
    } else if (_c == QMetaObject::QueryPropertyDesignable) {
        _id -= 4;
    } else if (_c == QMetaObject::QueryPropertyScriptable) {
        _id -= 4;
    } else if (_c == QMetaObject::QueryPropertyStored) {
        _id -= 4;
    } else if (_c == QMetaObject::QueryPropertyEditable) {
        _id -= 4;
    } else if (_c == QMetaObject::QueryPropertyUser) {
        _id -= 4;
    }
#endif // QT_NO_PROPERTIES
    return _id;
}

// SIGNAL 0
void Qt3DRender::QColorMask::redChanged(bool _t1)
{
    void *_a[] = { Q_NULLPTR, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 0, _a);
}

// SIGNAL 1
void Qt3DRender::QColorMask::greenChanged(bool _t1)
{
    void *_a[] = { Q_NULLPTR, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 1, _a);
}

// SIGNAL 2
void Qt3DRender::QColorMask::blueChanged(bool _t1)
{
    void *_a[] = { Q_NULLPTR, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 2, _a);
}

// SIGNAL 3
void Qt3DRender::QColorMask::alphaChanged(bool _t1)
{
    void *_a[] = { Q_NULLPTR, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 3, _a);
}
QT_END_MOC_NAMESPACE
