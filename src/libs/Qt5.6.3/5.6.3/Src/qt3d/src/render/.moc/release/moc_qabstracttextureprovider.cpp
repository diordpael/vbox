/****************************************************************************
** Meta object code from reading C++ file 'qabstracttextureprovider.h'
**
** Created by: The Qt Meta Object Compiler version 67 (Qt 5.6.3)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "../../texture/qabstracttextureprovider.h"
#include <QtCore/qbytearray.h>
#include <QtCore/qmetatype.h>
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'qabstracttextureprovider.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 67
#error "This file was generated using the moc from 5.6.3. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
struct qt_meta_stringdata_Qt3DRender__QAbstractTextureProvider_t {
    QByteArrayData data[189];
    char stringdata0[2322];
};
#define QT_MOC_LITERAL(idx, ofs, len) \
    Q_STATIC_BYTE_ARRAY_DATA_HEADER_INITIALIZER_WITH_OFFSET(len, \
    qptrdiff(offsetof(qt_meta_stringdata_Qt3DRender__QAbstractTextureProvider_t, stringdata0) + ofs \
        - idx * sizeof(QByteArrayData)) \
    )
static const qt_meta_stringdata_Qt3DRender__QAbstractTextureProvider_t qt_meta_stringdata_Qt3DRender__QAbstractTextureProvider = {
    {
QT_MOC_LITERAL(0, 0, 36), // "Qt3DRender::QAbstractTextureP..."
QT_MOC_LITERAL(1, 37, 13), // "formatChanged"
QT_MOC_LITERAL(2, 51, 0), // ""
QT_MOC_LITERAL(3, 52, 13), // "TextureFormat"
QT_MOC_LITERAL(4, 66, 6), // "format"
QT_MOC_LITERAL(5, 73, 13), // "statusChanged"
QT_MOC_LITERAL(6, 87, 6), // "Status"
QT_MOC_LITERAL(7, 94, 6), // "status"
QT_MOC_LITERAL(8, 101, 22), // "generateMipMapsChanged"
QT_MOC_LITERAL(9, 124, 15), // "generateMipMaps"
QT_MOC_LITERAL(10, 140, 12), // "widthChanged"
QT_MOC_LITERAL(11, 153, 5), // "width"
QT_MOC_LITERAL(12, 159, 13), // "heightChanged"
QT_MOC_LITERAL(13, 173, 6), // "height"
QT_MOC_LITERAL(14, 180, 12), // "depthChanged"
QT_MOC_LITERAL(15, 193, 5), // "depth"
QT_MOC_LITERAL(16, 199, 26), // "magnificationFilterChanged"
QT_MOC_LITERAL(17, 226, 6), // "Filter"
QT_MOC_LITERAL(18, 233, 19), // "magnificationFilter"
QT_MOC_LITERAL(19, 253, 25), // "minificationFilterChanged"
QT_MOC_LITERAL(20, 279, 18), // "minificationFilter"
QT_MOC_LITERAL(21, 298, 24), // "maximumAnisotropyChanged"
QT_MOC_LITERAL(22, 323, 17), // "maximumAnisotropy"
QT_MOC_LITERAL(23, 341, 25), // "comparisonFunctionChanged"
QT_MOC_LITERAL(24, 367, 18), // "ComparisonFunction"
QT_MOC_LITERAL(25, 386, 18), // "comparisonFunction"
QT_MOC_LITERAL(26, 405, 21), // "comparisonModeChanged"
QT_MOC_LITERAL(27, 427, 14), // "ComparisonMode"
QT_MOC_LITERAL(28, 442, 14), // "comparisonMode"
QT_MOC_LITERAL(29, 457, 20), // "maximumLayersChanged"
QT_MOC_LITERAL(30, 478, 13), // "maximumLayers"
QT_MOC_LITERAL(31, 492, 13), // "uniqueChanged"
QT_MOC_LITERAL(32, 506, 6), // "unique"
QT_MOC_LITERAL(33, 513, 9), // "setFormat"
QT_MOC_LITERAL(34, 523, 18), // "setGenerateMipMaps"
QT_MOC_LITERAL(35, 542, 3), // "gen"
QT_MOC_LITERAL(36, 546, 8), // "setWidth"
QT_MOC_LITERAL(37, 555, 9), // "setHeight"
QT_MOC_LITERAL(38, 565, 8), // "setDepth"
QT_MOC_LITERAL(39, 574, 21), // "setMinificationFilter"
QT_MOC_LITERAL(40, 596, 1), // "f"
QT_MOC_LITERAL(41, 598, 22), // "setMagnificationFilter"
QT_MOC_LITERAL(42, 621, 20), // "setMaximumAnisotropy"
QT_MOC_LITERAL(43, 642, 10), // "anisotropy"
QT_MOC_LITERAL(44, 653, 21), // "setComparisonFunction"
QT_MOC_LITERAL(45, 675, 8), // "function"
QT_MOC_LITERAL(46, 684, 17), // "setComparisonMode"
QT_MOC_LITERAL(47, 702, 4), // "mode"
QT_MOC_LITERAL(48, 707, 16), // "setMaximumLayers"
QT_MOC_LITERAL(49, 724, 9), // "setUnique"
QT_MOC_LITERAL(50, 734, 6), // "target"
QT_MOC_LITERAL(51, 741, 6), // "Target"
QT_MOC_LITERAL(52, 748, 8), // "wrapMode"
QT_MOC_LITERAL(53, 757, 29), // "Qt3DRender::QTextureWrapMode*"
QT_MOC_LITERAL(54, 787, 7), // "Loading"
QT_MOC_LITERAL(55, 795, 6), // "Loaded"
QT_MOC_LITERAL(56, 802, 5), // "Error"
QT_MOC_LITERAL(57, 808, 8), // "Target1D"
QT_MOC_LITERAL(58, 817, 13), // "Target1DArray"
QT_MOC_LITERAL(59, 831, 8), // "Target2D"
QT_MOC_LITERAL(60, 840, 13), // "Target2DArray"
QT_MOC_LITERAL(61, 854, 8), // "Target3D"
QT_MOC_LITERAL(62, 863, 13), // "TargetCubeMap"
QT_MOC_LITERAL(63, 877, 18), // "TargetCubeMapArray"
QT_MOC_LITERAL(64, 896, 19), // "Target2DMultisample"
QT_MOC_LITERAL(65, 916, 24), // "Target2DMultisampleArray"
QT_MOC_LITERAL(66, 941, 15), // "TargetRectangle"
QT_MOC_LITERAL(67, 957, 12), // "TargetBuffer"
QT_MOC_LITERAL(68, 970, 8), // "NoFormat"
QT_MOC_LITERAL(69, 979, 9), // "Automatic"
QT_MOC_LITERAL(70, 989, 8), // "R8_UNorm"
QT_MOC_LITERAL(71, 998, 9), // "RG8_UNorm"
QT_MOC_LITERAL(72, 1008, 10), // "RGB8_UNorm"
QT_MOC_LITERAL(73, 1019, 11), // "RGBA8_UNorm"
QT_MOC_LITERAL(74, 1031, 9), // "R16_UNorm"
QT_MOC_LITERAL(75, 1041, 10), // "RG16_UNorm"
QT_MOC_LITERAL(76, 1052, 11), // "RGB16_UNorm"
QT_MOC_LITERAL(77, 1064, 12), // "RGBA16_UNorm"
QT_MOC_LITERAL(78, 1077, 8), // "R8_SNorm"
QT_MOC_LITERAL(79, 1086, 9), // "RG8_SNorm"
QT_MOC_LITERAL(80, 1096, 10), // "RGB8_SNorm"
QT_MOC_LITERAL(81, 1107, 11), // "RGBA8_SNorm"
QT_MOC_LITERAL(82, 1119, 9), // "R16_SNorm"
QT_MOC_LITERAL(83, 1129, 10), // "RG16_SNorm"
QT_MOC_LITERAL(84, 1140, 11), // "RGB16_SNorm"
QT_MOC_LITERAL(85, 1152, 12), // "RGBA16_SNorm"
QT_MOC_LITERAL(86, 1165, 3), // "R8U"
QT_MOC_LITERAL(87, 1169, 4), // "RG8U"
QT_MOC_LITERAL(88, 1174, 5), // "RGB8U"
QT_MOC_LITERAL(89, 1180, 6), // "RGBA8U"
QT_MOC_LITERAL(90, 1187, 4), // "R16U"
QT_MOC_LITERAL(91, 1192, 5), // "RG16U"
QT_MOC_LITERAL(92, 1198, 6), // "RGB16U"
QT_MOC_LITERAL(93, 1205, 7), // "RGBA16U"
QT_MOC_LITERAL(94, 1213, 4), // "R32U"
QT_MOC_LITERAL(95, 1218, 5), // "RG32U"
QT_MOC_LITERAL(96, 1224, 6), // "RGB32U"
QT_MOC_LITERAL(97, 1231, 7), // "RGBA32U"
QT_MOC_LITERAL(98, 1239, 3), // "R8I"
QT_MOC_LITERAL(99, 1243, 4), // "RG8I"
QT_MOC_LITERAL(100, 1248, 5), // "RGB8I"
QT_MOC_LITERAL(101, 1254, 6), // "RGBA8I"
QT_MOC_LITERAL(102, 1261, 4), // "R16I"
QT_MOC_LITERAL(103, 1266, 5), // "RG16I"
QT_MOC_LITERAL(104, 1272, 6), // "RGB16I"
QT_MOC_LITERAL(105, 1279, 7), // "RGBA16I"
QT_MOC_LITERAL(106, 1287, 4), // "R32I"
QT_MOC_LITERAL(107, 1292, 5), // "RG32I"
QT_MOC_LITERAL(108, 1298, 6), // "RGB32I"
QT_MOC_LITERAL(109, 1305, 7), // "RGBA32I"
QT_MOC_LITERAL(110, 1313, 4), // "R16F"
QT_MOC_LITERAL(111, 1318, 5), // "RG16F"
QT_MOC_LITERAL(112, 1324, 6), // "RGB16F"
QT_MOC_LITERAL(113, 1331, 7), // "RGBA16F"
QT_MOC_LITERAL(114, 1339, 4), // "R32F"
QT_MOC_LITERAL(115, 1344, 5), // "RG32F"
QT_MOC_LITERAL(116, 1350, 6), // "RGB32F"
QT_MOC_LITERAL(117, 1357, 7), // "RGBA32F"
QT_MOC_LITERAL(118, 1365, 6), // "RGB9E5"
QT_MOC_LITERAL(119, 1372, 8), // "RG11B10F"
QT_MOC_LITERAL(120, 1381, 5), // "RG3B2"
QT_MOC_LITERAL(121, 1387, 6), // "R5G6B5"
QT_MOC_LITERAL(122, 1394, 6), // "RGB5A1"
QT_MOC_LITERAL(123, 1401, 5), // "RGBA4"
QT_MOC_LITERAL(124, 1407, 7), // "RGB10A2"
QT_MOC_LITERAL(125, 1415, 3), // "D16"
QT_MOC_LITERAL(126, 1419, 3), // "D24"
QT_MOC_LITERAL(127, 1423, 5), // "D24S8"
QT_MOC_LITERAL(128, 1429, 3), // "D32"
QT_MOC_LITERAL(129, 1433, 4), // "D32F"
QT_MOC_LITERAL(130, 1438, 9), // "D32FS8X24"
QT_MOC_LITERAL(131, 1448, 8), // "RGB_DXT1"
QT_MOC_LITERAL(132, 1457, 9), // "RGBA_DXT1"
QT_MOC_LITERAL(133, 1467, 9), // "RGBA_DXT3"
QT_MOC_LITERAL(134, 1477, 9), // "RGBA_DXT5"
QT_MOC_LITERAL(135, 1487, 13), // "R_ATI1N_UNorm"
QT_MOC_LITERAL(136, 1501, 13), // "R_ATI1N_SNorm"
QT_MOC_LITERAL(137, 1515, 14), // "RG_ATI2N_UNorm"
QT_MOC_LITERAL(138, 1530, 14), // "RG_ATI2N_SNorm"
QT_MOC_LITERAL(139, 1545, 21), // "RGB_BP_UNSIGNED_FLOAT"
QT_MOC_LITERAL(140, 1567, 19), // "RGB_BP_SIGNED_FLOAT"
QT_MOC_LITERAL(141, 1587, 12), // "RGB_BP_UNorm"
QT_MOC_LITERAL(142, 1600, 13), // "R11_EAC_UNorm"
QT_MOC_LITERAL(143, 1614, 13), // "R11_EAC_SNorm"
QT_MOC_LITERAL(144, 1628, 14), // "RG11_EAC_UNorm"
QT_MOC_LITERAL(145, 1643, 14), // "RG11_EAC_SNorm"
QT_MOC_LITERAL(146, 1658, 9), // "RGB8_ETC2"
QT_MOC_LITERAL(147, 1668, 10), // "SRGB8_ETC2"
QT_MOC_LITERAL(148, 1679, 29), // "RGB8_PunchThrough_Alpha1_ETC2"
QT_MOC_LITERAL(149, 1709, 30), // "SRGB8_PunchThrough_Alpha1_ETC2"
QT_MOC_LITERAL(150, 1740, 14), // "RGBA8_ETC2_EAC"
QT_MOC_LITERAL(151, 1755, 21), // "SRGB8_Alpha8_ETC2_EAC"
QT_MOC_LITERAL(152, 1777, 9), // "RGB8_ETC1"
QT_MOC_LITERAL(153, 1787, 5), // "SRGB8"
QT_MOC_LITERAL(154, 1793, 12), // "SRGB8_Alpha8"
QT_MOC_LITERAL(155, 1806, 9), // "SRGB_DXT1"
QT_MOC_LITERAL(156, 1816, 15), // "SRGB_Alpha_DXT1"
QT_MOC_LITERAL(157, 1832, 15), // "SRGB_Alpha_DXT3"
QT_MOC_LITERAL(158, 1848, 15), // "SRGB_Alpha_DXT5"
QT_MOC_LITERAL(159, 1864, 13), // "SRGB_BP_UNorm"
QT_MOC_LITERAL(160, 1878, 11), // "DepthFormat"
QT_MOC_LITERAL(161, 1890, 11), // "AlphaFormat"
QT_MOC_LITERAL(162, 1902, 9), // "RGBFormat"
QT_MOC_LITERAL(163, 1912, 10), // "RGBAFormat"
QT_MOC_LITERAL(164, 1923, 15), // "LuminanceFormat"
QT_MOC_LITERAL(165, 1939, 20), // "LuminanceAlphaFormat"
QT_MOC_LITERAL(166, 1960, 7), // "Nearest"
QT_MOC_LITERAL(167, 1968, 6), // "Linear"
QT_MOC_LITERAL(168, 1975, 20), // "NearestMipMapNearest"
QT_MOC_LITERAL(169, 1996, 19), // "NearestMipMapLinear"
QT_MOC_LITERAL(170, 2016, 19), // "LinearMipMapNearest"
QT_MOC_LITERAL(171, 2036, 18), // "LinearMipMapLinear"
QT_MOC_LITERAL(172, 2055, 11), // "CubeMapFace"
QT_MOC_LITERAL(173, 2067, 16), // "CubeMapPositiveX"
QT_MOC_LITERAL(174, 2084, 16), // "CubeMapNegativeX"
QT_MOC_LITERAL(175, 2101, 16), // "CubeMapPositiveY"
QT_MOC_LITERAL(176, 2118, 16), // "CubeMapNegativeY"
QT_MOC_LITERAL(177, 2135, 16), // "CubeMapPositiveZ"
QT_MOC_LITERAL(178, 2152, 16), // "CubeMapNegativeZ"
QT_MOC_LITERAL(179, 2169, 16), // "CompareLessEqual"
QT_MOC_LITERAL(180, 2186, 19), // "CompareGreaterEqual"
QT_MOC_LITERAL(181, 2206, 11), // "CompareLess"
QT_MOC_LITERAL(182, 2218, 14), // "CompareGreater"
QT_MOC_LITERAL(183, 2233, 12), // "CompareEqual"
QT_MOC_LITERAL(184, 2246, 16), // "CommpareNotEqual"
QT_MOC_LITERAL(185, 2263, 13), // "CompareAlways"
QT_MOC_LITERAL(186, 2277, 12), // "CompareNever"
QT_MOC_LITERAL(187, 2290, 19), // "CompareRefToTexture"
QT_MOC_LITERAL(188, 2310, 11) // "CompareNone"

    },
    "Qt3DRender::QAbstractTextureProvider\0"
    "formatChanged\0\0TextureFormat\0format\0"
    "statusChanged\0Status\0status\0"
    "generateMipMapsChanged\0generateMipMaps\0"
    "widthChanged\0width\0heightChanged\0"
    "height\0depthChanged\0depth\0"
    "magnificationFilterChanged\0Filter\0"
    "magnificationFilter\0minificationFilterChanged\0"
    "minificationFilter\0maximumAnisotropyChanged\0"
    "maximumAnisotropy\0comparisonFunctionChanged\0"
    "ComparisonFunction\0comparisonFunction\0"
    "comparisonModeChanged\0ComparisonMode\0"
    "comparisonMode\0maximumLayersChanged\0"
    "maximumLayers\0uniqueChanged\0unique\0"
    "setFormat\0setGenerateMipMaps\0gen\0"
    "setWidth\0setHeight\0setDepth\0"
    "setMinificationFilter\0f\0setMagnificationFilter\0"
    "setMaximumAnisotropy\0anisotropy\0"
    "setComparisonFunction\0function\0"
    "setComparisonMode\0mode\0setMaximumLayers\0"
    "setUnique\0target\0Target\0wrapMode\0"
    "Qt3DRender::QTextureWrapMode*\0Loading\0"
    "Loaded\0Error\0Target1D\0Target1DArray\0"
    "Target2D\0Target2DArray\0Target3D\0"
    "TargetCubeMap\0TargetCubeMapArray\0"
    "Target2DMultisample\0Target2DMultisampleArray\0"
    "TargetRectangle\0TargetBuffer\0NoFormat\0"
    "Automatic\0R8_UNorm\0RG8_UNorm\0RGB8_UNorm\0"
    "RGBA8_UNorm\0R16_UNorm\0RG16_UNorm\0"
    "RGB16_UNorm\0RGBA16_UNorm\0R8_SNorm\0"
    "RG8_SNorm\0RGB8_SNorm\0RGBA8_SNorm\0"
    "R16_SNorm\0RG16_SNorm\0RGB16_SNorm\0"
    "RGBA16_SNorm\0R8U\0RG8U\0RGB8U\0RGBA8U\0"
    "R16U\0RG16U\0RGB16U\0RGBA16U\0R32U\0RG32U\0"
    "RGB32U\0RGBA32U\0R8I\0RG8I\0RGB8I\0RGBA8I\0"
    "R16I\0RG16I\0RGB16I\0RGBA16I\0R32I\0RG32I\0"
    "RGB32I\0RGBA32I\0R16F\0RG16F\0RGB16F\0"
    "RGBA16F\0R32F\0RG32F\0RGB32F\0RGBA32F\0"
    "RGB9E5\0RG11B10F\0RG3B2\0R5G6B5\0RGB5A1\0"
    "RGBA4\0RGB10A2\0D16\0D24\0D24S8\0D32\0D32F\0"
    "D32FS8X24\0RGB_DXT1\0RGBA_DXT1\0RGBA_DXT3\0"
    "RGBA_DXT5\0R_ATI1N_UNorm\0R_ATI1N_SNorm\0"
    "RG_ATI2N_UNorm\0RG_ATI2N_SNorm\0"
    "RGB_BP_UNSIGNED_FLOAT\0RGB_BP_SIGNED_FLOAT\0"
    "RGB_BP_UNorm\0R11_EAC_UNorm\0R11_EAC_SNorm\0"
    "RG11_EAC_UNorm\0RG11_EAC_SNorm\0RGB8_ETC2\0"
    "SRGB8_ETC2\0RGB8_PunchThrough_Alpha1_ETC2\0"
    "SRGB8_PunchThrough_Alpha1_ETC2\0"
    "RGBA8_ETC2_EAC\0SRGB8_Alpha8_ETC2_EAC\0"
    "RGB8_ETC1\0SRGB8\0SRGB8_Alpha8\0SRGB_DXT1\0"
    "SRGB_Alpha_DXT1\0SRGB_Alpha_DXT3\0"
    "SRGB_Alpha_DXT5\0SRGB_BP_UNorm\0DepthFormat\0"
    "AlphaFormat\0RGBFormat\0RGBAFormat\0"
    "LuminanceFormat\0LuminanceAlphaFormat\0"
    "Nearest\0Linear\0NearestMipMapNearest\0"
    "NearestMipMapLinear\0LinearMipMapNearest\0"
    "LinearMipMapLinear\0CubeMapFace\0"
    "CubeMapPositiveX\0CubeMapNegativeX\0"
    "CubeMapPositiveY\0CubeMapNegativeY\0"
    "CubeMapPositiveZ\0CubeMapNegativeZ\0"
    "CompareLessEqual\0CompareGreaterEqual\0"
    "CompareLess\0CompareGreater\0CompareEqual\0"
    "CommpareNotEqual\0CompareAlways\0"
    "CompareNever\0CompareRefToTexture\0"
    "CompareNone"
};
#undef QT_MOC_LITERAL

static const uint qt_meta_data_Qt3DRender__QAbstractTextureProvider[] = {

 // content:
       7,       // revision
       0,       // classname
       0,    0, // classinfo
      25,   14, // methods
      15,  214, // properties
       7,  274, // enums/sets
       0,    0, // constructors
       0,       // flags
      13,       // signalCount

 // signals: name, argc, parameters, tag, flags
       1,    1,  139,    2, 0x06 /* Public */,
       5,    1,  142,    2, 0x06 /* Public */,
       8,    1,  145,    2, 0x06 /* Public */,
      10,    1,  148,    2, 0x06 /* Public */,
      12,    1,  151,    2, 0x06 /* Public */,
      14,    1,  154,    2, 0x06 /* Public */,
      16,    1,  157,    2, 0x06 /* Public */,
      19,    1,  160,    2, 0x06 /* Public */,
      21,    1,  163,    2, 0x06 /* Public */,
      23,    1,  166,    2, 0x06 /* Public */,
      26,    1,  169,    2, 0x06 /* Public */,
      29,    1,  172,    2, 0x06 /* Public */,
      31,    1,  175,    2, 0x06 /* Public */,

 // slots: name, argc, parameters, tag, flags
      33,    1,  178,    2, 0x0a /* Public */,
      34,    1,  181,    2, 0x0a /* Public */,
      36,    1,  184,    2, 0x0a /* Public */,
      37,    1,  187,    2, 0x0a /* Public */,
      38,    1,  190,    2, 0x0a /* Public */,
      39,    1,  193,    2, 0x0a /* Public */,
      41,    1,  196,    2, 0x0a /* Public */,
      42,    1,  199,    2, 0x0a /* Public */,
      44,    1,  202,    2, 0x0a /* Public */,
      46,    1,  205,    2, 0x0a /* Public */,
      48,    1,  208,    2, 0x0a /* Public */,
      49,    1,  211,    2, 0x0a /* Public */,

 // signals: parameters
    QMetaType::Void, 0x80000000 | 3,    4,
    QMetaType::Void, 0x80000000 | 6,    7,
    QMetaType::Void, QMetaType::Bool,    9,
    QMetaType::Void, QMetaType::Int,   11,
    QMetaType::Void, QMetaType::Int,   13,
    QMetaType::Void, QMetaType::Int,   15,
    QMetaType::Void, 0x80000000 | 17,   18,
    QMetaType::Void, 0x80000000 | 17,   20,
    QMetaType::Void, QMetaType::Float,   22,
    QMetaType::Void, 0x80000000 | 24,   25,
    QMetaType::Void, 0x80000000 | 27,   28,
    QMetaType::Void, QMetaType::Int,   30,
    QMetaType::Void, QMetaType::Bool,   32,

 // slots: parameters
    QMetaType::Void, 0x80000000 | 3,    4,
    QMetaType::Void, QMetaType::Bool,   35,
    QMetaType::Void, QMetaType::Int,   11,
    QMetaType::Void, QMetaType::Int,   13,
    QMetaType::Void, QMetaType::Int,   15,
    QMetaType::Void, 0x80000000 | 17,   40,
    QMetaType::Void, 0x80000000 | 17,   40,
    QMetaType::Void, QMetaType::Float,   43,
    QMetaType::Void, 0x80000000 | 24,   45,
    QMetaType::Void, 0x80000000 | 27,   47,
    QMetaType::Void, QMetaType::Int,   30,
    QMetaType::Void, QMetaType::Bool,   32,

 // properties: name, type, flags
      50, 0x80000000 | 51, 0x00095409,
       4, 0x80000000 | 3, 0x0049510b,
       9, QMetaType::Bool, 0x00495103,
      52, 0x80000000 | 53, 0x00095409,
       7, 0x80000000 | 6, 0x00495009,
      11, QMetaType::Int, 0x00495103,
      13, QMetaType::Int, 0x00495103,
      15, QMetaType::Int, 0x00495103,
      18, 0x80000000 | 17, 0x0049510b,
      20, 0x80000000 | 17, 0x0049510b,
      22, QMetaType::Float, 0x00495103,
      25, 0x80000000 | 24, 0x0049510b,
      28, 0x80000000 | 27, 0x0049510b,
      30, QMetaType::Int, 0x00495103,
      32, QMetaType::Bool, 0x00495103,

 // properties: notify_signal_id
       0,
       0,
       2,
       0,
       1,
       3,
       4,
       5,
       6,
       7,
       8,
       9,
      10,
      11,
      12,

 // enums: name, flags, count, data
       6, 0x0,    3,  302,
      51, 0x0,   11,  308,
       3, 0x0,   98,  330,
      17, 0x0,    6,  526,
     172, 0x0,    6,  538,
      24, 0x0,    8,  550,
      27, 0x0,    2,  566,

 // enum data: key, value
      54, uint(Qt3DRender::QAbstractTextureProvider::Loading),
      55, uint(Qt3DRender::QAbstractTextureProvider::Loaded),
      56, uint(Qt3DRender::QAbstractTextureProvider::Error),
      57, uint(Qt3DRender::QAbstractTextureProvider::Target1D),
      58, uint(Qt3DRender::QAbstractTextureProvider::Target1DArray),
      59, uint(Qt3DRender::QAbstractTextureProvider::Target2D),
      60, uint(Qt3DRender::QAbstractTextureProvider::Target2DArray),
      61, uint(Qt3DRender::QAbstractTextureProvider::Target3D),
      62, uint(Qt3DRender::QAbstractTextureProvider::TargetCubeMap),
      63, uint(Qt3DRender::QAbstractTextureProvider::TargetCubeMapArray),
      64, uint(Qt3DRender::QAbstractTextureProvider::Target2DMultisample),
      65, uint(Qt3DRender::QAbstractTextureProvider::Target2DMultisampleArray),
      66, uint(Qt3DRender::QAbstractTextureProvider::TargetRectangle),
      67, uint(Qt3DRender::QAbstractTextureProvider::TargetBuffer),
      68, uint(Qt3DRender::QAbstractTextureProvider::NoFormat),
      69, uint(Qt3DRender::QAbstractTextureProvider::Automatic),
      70, uint(Qt3DRender::QAbstractTextureProvider::R8_UNorm),
      71, uint(Qt3DRender::QAbstractTextureProvider::RG8_UNorm),
      72, uint(Qt3DRender::QAbstractTextureProvider::RGB8_UNorm),
      73, uint(Qt3DRender::QAbstractTextureProvider::RGBA8_UNorm),
      74, uint(Qt3DRender::QAbstractTextureProvider::R16_UNorm),
      75, uint(Qt3DRender::QAbstractTextureProvider::RG16_UNorm),
      76, uint(Qt3DRender::QAbstractTextureProvider::RGB16_UNorm),
      77, uint(Qt3DRender::QAbstractTextureProvider::RGBA16_UNorm),
      78, uint(Qt3DRender::QAbstractTextureProvider::R8_SNorm),
      79, uint(Qt3DRender::QAbstractTextureProvider::RG8_SNorm),
      80, uint(Qt3DRender::QAbstractTextureProvider::RGB8_SNorm),
      81, uint(Qt3DRender::QAbstractTextureProvider::RGBA8_SNorm),
      82, uint(Qt3DRender::QAbstractTextureProvider::R16_SNorm),
      83, uint(Qt3DRender::QAbstractTextureProvider::RG16_SNorm),
      84, uint(Qt3DRender::QAbstractTextureProvider::RGB16_SNorm),
      85, uint(Qt3DRender::QAbstractTextureProvider::RGBA16_SNorm),
      86, uint(Qt3DRender::QAbstractTextureProvider::R8U),
      87, uint(Qt3DRender::QAbstractTextureProvider::RG8U),
      88, uint(Qt3DRender::QAbstractTextureProvider::RGB8U),
      89, uint(Qt3DRender::QAbstractTextureProvider::RGBA8U),
      90, uint(Qt3DRender::QAbstractTextureProvider::R16U),
      91, uint(Qt3DRender::QAbstractTextureProvider::RG16U),
      92, uint(Qt3DRender::QAbstractTextureProvider::RGB16U),
      93, uint(Qt3DRender::QAbstractTextureProvider::RGBA16U),
      94, uint(Qt3DRender::QAbstractTextureProvider::R32U),
      95, uint(Qt3DRender::QAbstractTextureProvider::RG32U),
      96, uint(Qt3DRender::QAbstractTextureProvider::RGB32U),
      97, uint(Qt3DRender::QAbstractTextureProvider::RGBA32U),
      98, uint(Qt3DRender::QAbstractTextureProvider::R8I),
      99, uint(Qt3DRender::QAbstractTextureProvider::RG8I),
     100, uint(Qt3DRender::QAbstractTextureProvider::RGB8I),
     101, uint(Qt3DRender::QAbstractTextureProvider::RGBA8I),
     102, uint(Qt3DRender::QAbstractTextureProvider::R16I),
     103, uint(Qt3DRender::QAbstractTextureProvider::RG16I),
     104, uint(Qt3DRender::QAbstractTextureProvider::RGB16I),
     105, uint(Qt3DRender::QAbstractTextureProvider::RGBA16I),
     106, uint(Qt3DRender::QAbstractTextureProvider::R32I),
     107, uint(Qt3DRender::QAbstractTextureProvider::RG32I),
     108, uint(Qt3DRender::QAbstractTextureProvider::RGB32I),
     109, uint(Qt3DRender::QAbstractTextureProvider::RGBA32I),
     110, uint(Qt3DRender::QAbstractTextureProvider::R16F),
     111, uint(Qt3DRender::QAbstractTextureProvider::RG16F),
     112, uint(Qt3DRender::QAbstractTextureProvider::RGB16F),
     113, uint(Qt3DRender::QAbstractTextureProvider::RGBA16F),
     114, uint(Qt3DRender::QAbstractTextureProvider::R32F),
     115, uint(Qt3DRender::QAbstractTextureProvider::RG32F),
     116, uint(Qt3DRender::QAbstractTextureProvider::RGB32F),
     117, uint(Qt3DRender::QAbstractTextureProvider::RGBA32F),
     118, uint(Qt3DRender::QAbstractTextureProvider::RGB9E5),
     119, uint(Qt3DRender::QAbstractTextureProvider::RG11B10F),
     120, uint(Qt3DRender::QAbstractTextureProvider::RG3B2),
     121, uint(Qt3DRender::QAbstractTextureProvider::R5G6B5),
     122, uint(Qt3DRender::QAbstractTextureProvider::RGB5A1),
     123, uint(Qt3DRender::QAbstractTextureProvider::RGBA4),
     124, uint(Qt3DRender::QAbstractTextureProvider::RGB10A2),
     125, uint(Qt3DRender::QAbstractTextureProvider::D16),
     126, uint(Qt3DRender::QAbstractTextureProvider::D24),
     127, uint(Qt3DRender::QAbstractTextureProvider::D24S8),
     128, uint(Qt3DRender::QAbstractTextureProvider::D32),
     129, uint(Qt3DRender::QAbstractTextureProvider::D32F),
     130, uint(Qt3DRender::QAbstractTextureProvider::D32FS8X24),
     131, uint(Qt3DRender::QAbstractTextureProvider::RGB_DXT1),
     132, uint(Qt3DRender::QAbstractTextureProvider::RGBA_DXT1),
     133, uint(Qt3DRender::QAbstractTextureProvider::RGBA_DXT3),
     134, uint(Qt3DRender::QAbstractTextureProvider::RGBA_DXT5),
     135, uint(Qt3DRender::QAbstractTextureProvider::R_ATI1N_UNorm),
     136, uint(Qt3DRender::QAbstractTextureProvider::R_ATI1N_SNorm),
     137, uint(Qt3DRender::QAbstractTextureProvider::RG_ATI2N_UNorm),
     138, uint(Qt3DRender::QAbstractTextureProvider::RG_ATI2N_SNorm),
     139, uint(Qt3DRender::QAbstractTextureProvider::RGB_BP_UNSIGNED_FLOAT),
     140, uint(Qt3DRender::QAbstractTextureProvider::RGB_BP_SIGNED_FLOAT),
     141, uint(Qt3DRender::QAbstractTextureProvider::RGB_BP_UNorm),
     142, uint(Qt3DRender::QAbstractTextureProvider::R11_EAC_UNorm),
     143, uint(Qt3DRender::QAbstractTextureProvider::R11_EAC_SNorm),
     144, uint(Qt3DRender::QAbstractTextureProvider::RG11_EAC_UNorm),
     145, uint(Qt3DRender::QAbstractTextureProvider::RG11_EAC_SNorm),
     146, uint(Qt3DRender::QAbstractTextureProvider::RGB8_ETC2),
     147, uint(Qt3DRender::QAbstractTextureProvider::SRGB8_ETC2),
     148, uint(Qt3DRender::QAbstractTextureProvider::RGB8_PunchThrough_Alpha1_ETC2),
     149, uint(Qt3DRender::QAbstractTextureProvider::SRGB8_PunchThrough_Alpha1_ETC2),
     150, uint(Qt3DRender::QAbstractTextureProvider::RGBA8_ETC2_EAC),
     151, uint(Qt3DRender::QAbstractTextureProvider::SRGB8_Alpha8_ETC2_EAC),
     152, uint(Qt3DRender::QAbstractTextureProvider::RGB8_ETC1),
     153, uint(Qt3DRender::QAbstractTextureProvider::SRGB8),
     154, uint(Qt3DRender::QAbstractTextureProvider::SRGB8_Alpha8),
     155, uint(Qt3DRender::QAbstractTextureProvider::SRGB_DXT1),
     156, uint(Qt3DRender::QAbstractTextureProvider::SRGB_Alpha_DXT1),
     157, uint(Qt3DRender::QAbstractTextureProvider::SRGB_Alpha_DXT3),
     158, uint(Qt3DRender::QAbstractTextureProvider::SRGB_Alpha_DXT5),
     159, uint(Qt3DRender::QAbstractTextureProvider::SRGB_BP_UNorm),
     160, uint(Qt3DRender::QAbstractTextureProvider::DepthFormat),
     161, uint(Qt3DRender::QAbstractTextureProvider::AlphaFormat),
     162, uint(Qt3DRender::QAbstractTextureProvider::RGBFormat),
     163, uint(Qt3DRender::QAbstractTextureProvider::RGBAFormat),
     164, uint(Qt3DRender::QAbstractTextureProvider::LuminanceFormat),
     165, uint(Qt3DRender::QAbstractTextureProvider::LuminanceAlphaFormat),
     166, uint(Qt3DRender::QAbstractTextureProvider::Nearest),
     167, uint(Qt3DRender::QAbstractTextureProvider::Linear),
     168, uint(Qt3DRender::QAbstractTextureProvider::NearestMipMapNearest),
     169, uint(Qt3DRender::QAbstractTextureProvider::NearestMipMapLinear),
     170, uint(Qt3DRender::QAbstractTextureProvider::LinearMipMapNearest),
     171, uint(Qt3DRender::QAbstractTextureProvider::LinearMipMapLinear),
     173, uint(Qt3DRender::QAbstractTextureProvider::CubeMapPositiveX),
     174, uint(Qt3DRender::QAbstractTextureProvider::CubeMapNegativeX),
     175, uint(Qt3DRender::QAbstractTextureProvider::CubeMapPositiveY),
     176, uint(Qt3DRender::QAbstractTextureProvider::CubeMapNegativeY),
     177, uint(Qt3DRender::QAbstractTextureProvider::CubeMapPositiveZ),
     178, uint(Qt3DRender::QAbstractTextureProvider::CubeMapNegativeZ),
     179, uint(Qt3DRender::QAbstractTextureProvider::CompareLessEqual),
     180, uint(Qt3DRender::QAbstractTextureProvider::CompareGreaterEqual),
     181, uint(Qt3DRender::QAbstractTextureProvider::CompareLess),
     182, uint(Qt3DRender::QAbstractTextureProvider::CompareGreater),
     183, uint(Qt3DRender::QAbstractTextureProvider::CompareEqual),
     184, uint(Qt3DRender::QAbstractTextureProvider::CommpareNotEqual),
     185, uint(Qt3DRender::QAbstractTextureProvider::CompareAlways),
     186, uint(Qt3DRender::QAbstractTextureProvider::CompareNever),
     187, uint(Qt3DRender::QAbstractTextureProvider::CompareRefToTexture),
     188, uint(Qt3DRender::QAbstractTextureProvider::CompareNone),

       0        // eod
};

void Qt3DRender::QAbstractTextureProvider::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        QAbstractTextureProvider *_t = static_cast<QAbstractTextureProvider *>(_o);
        Q_UNUSED(_t)
        switch (_id) {
        case 0: _t->formatChanged((*reinterpret_cast< TextureFormat(*)>(_a[1]))); break;
        case 1: _t->statusChanged((*reinterpret_cast< Status(*)>(_a[1]))); break;
        case 2: _t->generateMipMapsChanged((*reinterpret_cast< bool(*)>(_a[1]))); break;
        case 3: _t->widthChanged((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 4: _t->heightChanged((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 5: _t->depthChanged((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 6: _t->magnificationFilterChanged((*reinterpret_cast< Filter(*)>(_a[1]))); break;
        case 7: _t->minificationFilterChanged((*reinterpret_cast< Filter(*)>(_a[1]))); break;
        case 8: _t->maximumAnisotropyChanged((*reinterpret_cast< float(*)>(_a[1]))); break;
        case 9: _t->comparisonFunctionChanged((*reinterpret_cast< ComparisonFunction(*)>(_a[1]))); break;
        case 10: _t->comparisonModeChanged((*reinterpret_cast< ComparisonMode(*)>(_a[1]))); break;
        case 11: _t->maximumLayersChanged((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 12: _t->uniqueChanged((*reinterpret_cast< bool(*)>(_a[1]))); break;
        case 13: _t->setFormat((*reinterpret_cast< TextureFormat(*)>(_a[1]))); break;
        case 14: _t->setGenerateMipMaps((*reinterpret_cast< bool(*)>(_a[1]))); break;
        case 15: _t->setWidth((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 16: _t->setHeight((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 17: _t->setDepth((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 18: _t->setMinificationFilter((*reinterpret_cast< Filter(*)>(_a[1]))); break;
        case 19: _t->setMagnificationFilter((*reinterpret_cast< Filter(*)>(_a[1]))); break;
        case 20: _t->setMaximumAnisotropy((*reinterpret_cast< float(*)>(_a[1]))); break;
        case 21: _t->setComparisonFunction((*reinterpret_cast< ComparisonFunction(*)>(_a[1]))); break;
        case 22: _t->setComparisonMode((*reinterpret_cast< ComparisonMode(*)>(_a[1]))); break;
        case 23: _t->setMaximumLayers((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 24: _t->setUnique((*reinterpret_cast< bool(*)>(_a[1]))); break;
        default: ;
        }
    } else if (_c == QMetaObject::IndexOfMethod) {
        int *result = reinterpret_cast<int *>(_a[0]);
        void **func = reinterpret_cast<void **>(_a[1]);
        {
            typedef void (QAbstractTextureProvider::*_t)(TextureFormat );
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&QAbstractTextureProvider::formatChanged)) {
                *result = 0;
                return;
            }
        }
        {
            typedef void (QAbstractTextureProvider::*_t)(Status );
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&QAbstractTextureProvider::statusChanged)) {
                *result = 1;
                return;
            }
        }
        {
            typedef void (QAbstractTextureProvider::*_t)(bool );
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&QAbstractTextureProvider::generateMipMapsChanged)) {
                *result = 2;
                return;
            }
        }
        {
            typedef void (QAbstractTextureProvider::*_t)(int );
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&QAbstractTextureProvider::widthChanged)) {
                *result = 3;
                return;
            }
        }
        {
            typedef void (QAbstractTextureProvider::*_t)(int );
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&QAbstractTextureProvider::heightChanged)) {
                *result = 4;
                return;
            }
        }
        {
            typedef void (QAbstractTextureProvider::*_t)(int );
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&QAbstractTextureProvider::depthChanged)) {
                *result = 5;
                return;
            }
        }
        {
            typedef void (QAbstractTextureProvider::*_t)(Filter );
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&QAbstractTextureProvider::magnificationFilterChanged)) {
                *result = 6;
                return;
            }
        }
        {
            typedef void (QAbstractTextureProvider::*_t)(Filter );
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&QAbstractTextureProvider::minificationFilterChanged)) {
                *result = 7;
                return;
            }
        }
        {
            typedef void (QAbstractTextureProvider::*_t)(float );
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&QAbstractTextureProvider::maximumAnisotropyChanged)) {
                *result = 8;
                return;
            }
        }
        {
            typedef void (QAbstractTextureProvider::*_t)(ComparisonFunction );
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&QAbstractTextureProvider::comparisonFunctionChanged)) {
                *result = 9;
                return;
            }
        }
        {
            typedef void (QAbstractTextureProvider::*_t)(ComparisonMode );
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&QAbstractTextureProvider::comparisonModeChanged)) {
                *result = 10;
                return;
            }
        }
        {
            typedef void (QAbstractTextureProvider::*_t)(int );
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&QAbstractTextureProvider::maximumLayersChanged)) {
                *result = 11;
                return;
            }
        }
        {
            typedef void (QAbstractTextureProvider::*_t)(bool );
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&QAbstractTextureProvider::uniqueChanged)) {
                *result = 12;
                return;
            }
        }
    }
#ifndef QT_NO_PROPERTIES
    else if (_c == QMetaObject::ReadProperty) {
        QAbstractTextureProvider *_t = static_cast<QAbstractTextureProvider *>(_o);
        Q_UNUSED(_t)
        void *_v = _a[0];
        switch (_id) {
        case 0: *reinterpret_cast< Target*>(_v) = _t->target(); break;
        case 1: *reinterpret_cast< TextureFormat*>(_v) = _t->format(); break;
        case 2: *reinterpret_cast< bool*>(_v) = _t->generateMipMaps(); break;
        case 3: *reinterpret_cast< Qt3DRender::QTextureWrapMode**>(_v) = _t->wrapMode(); break;
        case 4: *reinterpret_cast< Status*>(_v) = _t->status(); break;
        case 5: *reinterpret_cast< int*>(_v) = _t->width(); break;
        case 6: *reinterpret_cast< int*>(_v) = _t->height(); break;
        case 7: *reinterpret_cast< int*>(_v) = _t->depth(); break;
        case 8: *reinterpret_cast< Filter*>(_v) = _t->magnificationFilter(); break;
        case 9: *reinterpret_cast< Filter*>(_v) = _t->minificationFilter(); break;
        case 10: *reinterpret_cast< float*>(_v) = _t->maximumAnisotropy(); break;
        case 11: *reinterpret_cast< ComparisonFunction*>(_v) = _t->comparisonFunction(); break;
        case 12: *reinterpret_cast< ComparisonMode*>(_v) = _t->comparisonMode(); break;
        case 13: *reinterpret_cast< int*>(_v) = _t->maximumLayers(); break;
        case 14: *reinterpret_cast< bool*>(_v) = _t->isUnique(); break;
        default: break;
        }
    } else if (_c == QMetaObject::WriteProperty) {
        QAbstractTextureProvider *_t = static_cast<QAbstractTextureProvider *>(_o);
        Q_UNUSED(_t)
        void *_v = _a[0];
        switch (_id) {
        case 1: _t->setFormat(*reinterpret_cast< TextureFormat*>(_v)); break;
        case 2: _t->setGenerateMipMaps(*reinterpret_cast< bool*>(_v)); break;
        case 5: _t->setWidth(*reinterpret_cast< int*>(_v)); break;
        case 6: _t->setHeight(*reinterpret_cast< int*>(_v)); break;
        case 7: _t->setDepth(*reinterpret_cast< int*>(_v)); break;
        case 8: _t->setMagnificationFilter(*reinterpret_cast< Filter*>(_v)); break;
        case 9: _t->setMinificationFilter(*reinterpret_cast< Filter*>(_v)); break;
        case 10: _t->setMaximumAnisotropy(*reinterpret_cast< float*>(_v)); break;
        case 11: _t->setComparisonFunction(*reinterpret_cast< ComparisonFunction*>(_v)); break;
        case 12: _t->setComparisonMode(*reinterpret_cast< ComparisonMode*>(_v)); break;
        case 13: _t->setMaximumLayers(*reinterpret_cast< int*>(_v)); break;
        case 14: _t->setUnique(*reinterpret_cast< bool*>(_v)); break;
        default: break;
        }
    } else if (_c == QMetaObject::ResetProperty) {
    }
#endif // QT_NO_PROPERTIES
}

const QMetaObject Qt3DRender::QAbstractTextureProvider::staticMetaObject = {
    { &Qt3DCore::QNode::staticMetaObject, qt_meta_stringdata_Qt3DRender__QAbstractTextureProvider.data,
      qt_meta_data_Qt3DRender__QAbstractTextureProvider,  qt_static_metacall, Q_NULLPTR, Q_NULLPTR}
};


const QMetaObject *Qt3DRender::QAbstractTextureProvider::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->dynamicMetaObject() : &staticMetaObject;
}

void *Qt3DRender::QAbstractTextureProvider::qt_metacast(const char *_clname)
{
    if (!_clname) return Q_NULLPTR;
    if (!strcmp(_clname, qt_meta_stringdata_Qt3DRender__QAbstractTextureProvider.stringdata0))
        return static_cast<void*>(const_cast< QAbstractTextureProvider*>(this));
    return Qt3DCore::QNode::qt_metacast(_clname);
}

int Qt3DRender::QAbstractTextureProvider::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = Qt3DCore::QNode::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 25)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 25;
    } else if (_c == QMetaObject::RegisterMethodArgumentMetaType) {
        if (_id < 25)
            *reinterpret_cast<int*>(_a[0]) = -1;
        _id -= 25;
    }
#ifndef QT_NO_PROPERTIES
   else if (_c == QMetaObject::ReadProperty || _c == QMetaObject::WriteProperty
            || _c == QMetaObject::ResetProperty || _c == QMetaObject::RegisterPropertyMetaType) {
        qt_static_metacall(this, _c, _id, _a);
        _id -= 15;
    } else if (_c == QMetaObject::QueryPropertyDesignable) {
        _id -= 15;
    } else if (_c == QMetaObject::QueryPropertyScriptable) {
        _id -= 15;
    } else if (_c == QMetaObject::QueryPropertyStored) {
        _id -= 15;
    } else if (_c == QMetaObject::QueryPropertyEditable) {
        _id -= 15;
    } else if (_c == QMetaObject::QueryPropertyUser) {
        _id -= 15;
    }
#endif // QT_NO_PROPERTIES
    return _id;
}

// SIGNAL 0
void Qt3DRender::QAbstractTextureProvider::formatChanged(TextureFormat _t1)
{
    void *_a[] = { Q_NULLPTR, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 0, _a);
}

// SIGNAL 1
void Qt3DRender::QAbstractTextureProvider::statusChanged(Status _t1)
{
    void *_a[] = { Q_NULLPTR, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 1, _a);
}

// SIGNAL 2
void Qt3DRender::QAbstractTextureProvider::generateMipMapsChanged(bool _t1)
{
    void *_a[] = { Q_NULLPTR, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 2, _a);
}

// SIGNAL 3
void Qt3DRender::QAbstractTextureProvider::widthChanged(int _t1)
{
    void *_a[] = { Q_NULLPTR, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 3, _a);
}

// SIGNAL 4
void Qt3DRender::QAbstractTextureProvider::heightChanged(int _t1)
{
    void *_a[] = { Q_NULLPTR, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 4, _a);
}

// SIGNAL 5
void Qt3DRender::QAbstractTextureProvider::depthChanged(int _t1)
{
    void *_a[] = { Q_NULLPTR, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 5, _a);
}

// SIGNAL 6
void Qt3DRender::QAbstractTextureProvider::magnificationFilterChanged(Filter _t1)
{
    void *_a[] = { Q_NULLPTR, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 6, _a);
}

// SIGNAL 7
void Qt3DRender::QAbstractTextureProvider::minificationFilterChanged(Filter _t1)
{
    void *_a[] = { Q_NULLPTR, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 7, _a);
}

// SIGNAL 8
void Qt3DRender::QAbstractTextureProvider::maximumAnisotropyChanged(float _t1)
{
    void *_a[] = { Q_NULLPTR, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 8, _a);
}

// SIGNAL 9
void Qt3DRender::QAbstractTextureProvider::comparisonFunctionChanged(ComparisonFunction _t1)
{
    void *_a[] = { Q_NULLPTR, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 9, _a);
}

// SIGNAL 10
void Qt3DRender::QAbstractTextureProvider::comparisonModeChanged(ComparisonMode _t1)
{
    void *_a[] = { Q_NULLPTR, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 10, _a);
}

// SIGNAL 11
void Qt3DRender::QAbstractTextureProvider::maximumLayersChanged(int _t1)
{
    void *_a[] = { Q_NULLPTR, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 11, _a);
}

// SIGNAL 12
void Qt3DRender::QAbstractTextureProvider::uniqueChanged(bool _t1)
{
    void *_a[] = { Q_NULLPTR, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 12, _a);
}
QT_END_MOC_NAMESPACE
