/****************************************************************************
** Meta object code from reading C++ file 'qnormaldiffusespecularmapmaterial.h'
**
** Created by: The Qt Meta Object Compiler version 67 (Qt 5.6.3)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "../../defaults/qnormaldiffusespecularmapmaterial.h"
#include <QtCore/qbytearray.h>
#include <QtCore/qmetatype.h>
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'qnormaldiffusespecularmapmaterial.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 67
#error "This file was generated using the moc from 5.6.3. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
struct qt_meta_stringdata_Qt3DRender__QNormalDiffuseSpecularMapMaterial_t {
    QByteArrayData data[22];
    char stringdata0[336];
};
#define QT_MOC_LITERAL(idx, ofs, len) \
    Q_STATIC_BYTE_ARRAY_DATA_HEADER_INITIALIZER_WITH_OFFSET(len, \
    qptrdiff(offsetof(qt_meta_stringdata_Qt3DRender__QNormalDiffuseSpecularMapMaterial_t, stringdata0) + ofs \
        - idx * sizeof(QByteArrayData)) \
    )
static const qt_meta_stringdata_Qt3DRender__QNormalDiffuseSpecularMapMaterial_t qt_meta_stringdata_Qt3DRender__QNormalDiffuseSpecularMapMaterial = {
    {
QT_MOC_LITERAL(0, 0, 45), // "Qt3DRender::QNormalDiffuseSpe..."
QT_MOC_LITERAL(1, 46, 14), // "ambientChanged"
QT_MOC_LITERAL(2, 61, 0), // ""
QT_MOC_LITERAL(3, 62, 7), // "ambient"
QT_MOC_LITERAL(4, 70, 14), // "diffuseChanged"
QT_MOC_LITERAL(5, 85, 25), // "QAbstractTextureProvider*"
QT_MOC_LITERAL(6, 111, 7), // "diffuse"
QT_MOC_LITERAL(7, 119, 13), // "normalChanged"
QT_MOC_LITERAL(8, 133, 6), // "normal"
QT_MOC_LITERAL(9, 140, 15), // "specularChanged"
QT_MOC_LITERAL(10, 156, 8), // "specular"
QT_MOC_LITERAL(11, 165, 16), // "shininessChanged"
QT_MOC_LITERAL(12, 182, 9), // "shininess"
QT_MOC_LITERAL(13, 192, 19), // "textureScaleChanged"
QT_MOC_LITERAL(14, 212, 12), // "textureScale"
QT_MOC_LITERAL(15, 225, 10), // "setAmbient"
QT_MOC_LITERAL(16, 236, 10), // "setDiffuse"
QT_MOC_LITERAL(17, 247, 9), // "setNormal"
QT_MOC_LITERAL(18, 257, 11), // "setSpecular"
QT_MOC_LITERAL(19, 269, 12), // "setShininess"
QT_MOC_LITERAL(20, 282, 15), // "setTextureScale"
QT_MOC_LITERAL(21, 298, 37) // "Qt3DRender::QAbstractTextureP..."

    },
    "Qt3DRender::QNormalDiffuseSpecularMapMaterial\0"
    "ambientChanged\0\0ambient\0diffuseChanged\0"
    "QAbstractTextureProvider*\0diffuse\0"
    "normalChanged\0normal\0specularChanged\0"
    "specular\0shininessChanged\0shininess\0"
    "textureScaleChanged\0textureScale\0"
    "setAmbient\0setDiffuse\0setNormal\0"
    "setSpecular\0setShininess\0setTextureScale\0"
    "Qt3DRender::QAbstractTextureProvider*"
};
#undef QT_MOC_LITERAL

static const uint qt_meta_data_Qt3DRender__QNormalDiffuseSpecularMapMaterial[] = {

 // content:
       7,       // revision
       0,       // classname
       0,    0, // classinfo
      12,   14, // methods
       6,  110, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       6,       // signalCount

 // signals: name, argc, parameters, tag, flags
       1,    1,   74,    2, 0x06 /* Public */,
       4,    1,   77,    2, 0x06 /* Public */,
       7,    1,   80,    2, 0x06 /* Public */,
       9,    1,   83,    2, 0x06 /* Public */,
      11,    1,   86,    2, 0x06 /* Public */,
      13,    1,   89,    2, 0x06 /* Public */,

 // slots: name, argc, parameters, tag, flags
      15,    1,   92,    2, 0x0a /* Public */,
      16,    1,   95,    2, 0x0a /* Public */,
      17,    1,   98,    2, 0x0a /* Public */,
      18,    1,  101,    2, 0x0a /* Public */,
      19,    1,  104,    2, 0x0a /* Public */,
      20,    1,  107,    2, 0x0a /* Public */,

 // signals: parameters
    QMetaType::Void, QMetaType::QColor,    3,
    QMetaType::Void, 0x80000000 | 5,    6,
    QMetaType::Void, 0x80000000 | 5,    8,
    QMetaType::Void, 0x80000000 | 5,   10,
    QMetaType::Void, QMetaType::Float,   12,
    QMetaType::Void, QMetaType::Float,   14,

 // slots: parameters
    QMetaType::Void, QMetaType::QColor,    3,
    QMetaType::Void, 0x80000000 | 5,    6,
    QMetaType::Void, 0x80000000 | 5,    8,
    QMetaType::Void, 0x80000000 | 5,   10,
    QMetaType::Void, QMetaType::Float,   12,
    QMetaType::Void, QMetaType::Float,   14,

 // properties: name, type, flags
       3, QMetaType::QColor, 0x00495103,
       6, 0x80000000 | 21, 0x0049510b,
       8, 0x80000000 | 21, 0x0049510b,
      10, 0x80000000 | 21, 0x0049510b,
      12, QMetaType::Float, 0x00495103,
      14, QMetaType::Float, 0x00495103,

 // properties: notify_signal_id
       0,
       1,
       2,
       3,
       4,
       5,

       0        // eod
};

void Qt3DRender::QNormalDiffuseSpecularMapMaterial::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        QNormalDiffuseSpecularMapMaterial *_t = static_cast<QNormalDiffuseSpecularMapMaterial *>(_o);
        Q_UNUSED(_t)
        switch (_id) {
        case 0: _t->ambientChanged((*reinterpret_cast< const QColor(*)>(_a[1]))); break;
        case 1: _t->diffuseChanged((*reinterpret_cast< QAbstractTextureProvider*(*)>(_a[1]))); break;
        case 2: _t->normalChanged((*reinterpret_cast< QAbstractTextureProvider*(*)>(_a[1]))); break;
        case 3: _t->specularChanged((*reinterpret_cast< QAbstractTextureProvider*(*)>(_a[1]))); break;
        case 4: _t->shininessChanged((*reinterpret_cast< float(*)>(_a[1]))); break;
        case 5: _t->textureScaleChanged((*reinterpret_cast< float(*)>(_a[1]))); break;
        case 6: _t->setAmbient((*reinterpret_cast< const QColor(*)>(_a[1]))); break;
        case 7: _t->setDiffuse((*reinterpret_cast< QAbstractTextureProvider*(*)>(_a[1]))); break;
        case 8: _t->setNormal((*reinterpret_cast< QAbstractTextureProvider*(*)>(_a[1]))); break;
        case 9: _t->setSpecular((*reinterpret_cast< QAbstractTextureProvider*(*)>(_a[1]))); break;
        case 10: _t->setShininess((*reinterpret_cast< float(*)>(_a[1]))); break;
        case 11: _t->setTextureScale((*reinterpret_cast< float(*)>(_a[1]))); break;
        default: ;
        }
    } else if (_c == QMetaObject::IndexOfMethod) {
        int *result = reinterpret_cast<int *>(_a[0]);
        void **func = reinterpret_cast<void **>(_a[1]);
        {
            typedef void (QNormalDiffuseSpecularMapMaterial::*_t)(const QColor & );
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&QNormalDiffuseSpecularMapMaterial::ambientChanged)) {
                *result = 0;
                return;
            }
        }
        {
            typedef void (QNormalDiffuseSpecularMapMaterial::*_t)(QAbstractTextureProvider * );
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&QNormalDiffuseSpecularMapMaterial::diffuseChanged)) {
                *result = 1;
                return;
            }
        }
        {
            typedef void (QNormalDiffuseSpecularMapMaterial::*_t)(QAbstractTextureProvider * );
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&QNormalDiffuseSpecularMapMaterial::normalChanged)) {
                *result = 2;
                return;
            }
        }
        {
            typedef void (QNormalDiffuseSpecularMapMaterial::*_t)(QAbstractTextureProvider * );
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&QNormalDiffuseSpecularMapMaterial::specularChanged)) {
                *result = 3;
                return;
            }
        }
        {
            typedef void (QNormalDiffuseSpecularMapMaterial::*_t)(float );
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&QNormalDiffuseSpecularMapMaterial::shininessChanged)) {
                *result = 4;
                return;
            }
        }
        {
            typedef void (QNormalDiffuseSpecularMapMaterial::*_t)(float );
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&QNormalDiffuseSpecularMapMaterial::textureScaleChanged)) {
                *result = 5;
                return;
            }
        }
    }
#ifndef QT_NO_PROPERTIES
    else if (_c == QMetaObject::ReadProperty) {
        QNormalDiffuseSpecularMapMaterial *_t = static_cast<QNormalDiffuseSpecularMapMaterial *>(_o);
        Q_UNUSED(_t)
        void *_v = _a[0];
        switch (_id) {
        case 0: *reinterpret_cast< QColor*>(_v) = _t->ambient(); break;
        case 1: *reinterpret_cast< Qt3DRender::QAbstractTextureProvider**>(_v) = _t->diffuse(); break;
        case 2: *reinterpret_cast< Qt3DRender::QAbstractTextureProvider**>(_v) = _t->normal(); break;
        case 3: *reinterpret_cast< Qt3DRender::QAbstractTextureProvider**>(_v) = _t->specular(); break;
        case 4: *reinterpret_cast< float*>(_v) = _t->shininess(); break;
        case 5: *reinterpret_cast< float*>(_v) = _t->textureScale(); break;
        default: break;
        }
    } else if (_c == QMetaObject::WriteProperty) {
        QNormalDiffuseSpecularMapMaterial *_t = static_cast<QNormalDiffuseSpecularMapMaterial *>(_o);
        Q_UNUSED(_t)
        void *_v = _a[0];
        switch (_id) {
        case 0: _t->setAmbient(*reinterpret_cast< QColor*>(_v)); break;
        case 1: _t->setDiffuse(*reinterpret_cast< Qt3DRender::QAbstractTextureProvider**>(_v)); break;
        case 2: _t->setNormal(*reinterpret_cast< Qt3DRender::QAbstractTextureProvider**>(_v)); break;
        case 3: _t->setSpecular(*reinterpret_cast< Qt3DRender::QAbstractTextureProvider**>(_v)); break;
        case 4: _t->setShininess(*reinterpret_cast< float*>(_v)); break;
        case 5: _t->setTextureScale(*reinterpret_cast< float*>(_v)); break;
        default: break;
        }
    } else if (_c == QMetaObject::ResetProperty) {
    }
#endif // QT_NO_PROPERTIES
}

const QMetaObject Qt3DRender::QNormalDiffuseSpecularMapMaterial::staticMetaObject = {
    { &QMaterial::staticMetaObject, qt_meta_stringdata_Qt3DRender__QNormalDiffuseSpecularMapMaterial.data,
      qt_meta_data_Qt3DRender__QNormalDiffuseSpecularMapMaterial,  qt_static_metacall, Q_NULLPTR, Q_NULLPTR}
};


const QMetaObject *Qt3DRender::QNormalDiffuseSpecularMapMaterial::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->dynamicMetaObject() : &staticMetaObject;
}

void *Qt3DRender::QNormalDiffuseSpecularMapMaterial::qt_metacast(const char *_clname)
{
    if (!_clname) return Q_NULLPTR;
    if (!strcmp(_clname, qt_meta_stringdata_Qt3DRender__QNormalDiffuseSpecularMapMaterial.stringdata0))
        return static_cast<void*>(const_cast< QNormalDiffuseSpecularMapMaterial*>(this));
    return QMaterial::qt_metacast(_clname);
}

int Qt3DRender::QNormalDiffuseSpecularMapMaterial::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QMaterial::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 12)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 12;
    } else if (_c == QMetaObject::RegisterMethodArgumentMetaType) {
        if (_id < 12)
            *reinterpret_cast<int*>(_a[0]) = -1;
        _id -= 12;
    }
#ifndef QT_NO_PROPERTIES
   else if (_c == QMetaObject::ReadProperty || _c == QMetaObject::WriteProperty
            || _c == QMetaObject::ResetProperty || _c == QMetaObject::RegisterPropertyMetaType) {
        qt_static_metacall(this, _c, _id, _a);
        _id -= 6;
    } else if (_c == QMetaObject::QueryPropertyDesignable) {
        _id -= 6;
    } else if (_c == QMetaObject::QueryPropertyScriptable) {
        _id -= 6;
    } else if (_c == QMetaObject::QueryPropertyStored) {
        _id -= 6;
    } else if (_c == QMetaObject::QueryPropertyEditable) {
        _id -= 6;
    } else if (_c == QMetaObject::QueryPropertyUser) {
        _id -= 6;
    }
#endif // QT_NO_PROPERTIES
    return _id;
}

// SIGNAL 0
void Qt3DRender::QNormalDiffuseSpecularMapMaterial::ambientChanged(const QColor & _t1)
{
    void *_a[] = { Q_NULLPTR, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 0, _a);
}

// SIGNAL 1
void Qt3DRender::QNormalDiffuseSpecularMapMaterial::diffuseChanged(QAbstractTextureProvider * _t1)
{
    void *_a[] = { Q_NULLPTR, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 1, _a);
}

// SIGNAL 2
void Qt3DRender::QNormalDiffuseSpecularMapMaterial::normalChanged(QAbstractTextureProvider * _t1)
{
    void *_a[] = { Q_NULLPTR, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 2, _a);
}

// SIGNAL 3
void Qt3DRender::QNormalDiffuseSpecularMapMaterial::specularChanged(QAbstractTextureProvider * _t1)
{
    void *_a[] = { Q_NULLPTR, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 3, _a);
}

// SIGNAL 4
void Qt3DRender::QNormalDiffuseSpecularMapMaterial::shininessChanged(float _t1)
{
    void *_a[] = { Q_NULLPTR, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 4, _a);
}

// SIGNAL 5
void Qt3DRender::QNormalDiffuseSpecularMapMaterial::textureScaleChanged(float _t1)
{
    void *_a[] = { Q_NULLPTR, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 5, _a);
}
QT_END_MOC_NAMESPACE
