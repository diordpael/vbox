/****************************************************************************
** Meta object code from reading C++ file 'qgraphicsapifilter.h'
**
** Created by: The Qt Meta Object Compiler version 67 (Qt 5.6.3)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "../../materialsystem/qgraphicsapifilter.h"
#include <QtCore/qbytearray.h>
#include <QtCore/qmetatype.h>
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'qgraphicsapifilter.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 67
#error "This file was generated using the moc from 5.6.3. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
struct qt_meta_stringdata_Qt3DRender__QGraphicsApiFilter_t {
    QByteArrayData data[30];
    char stringdata0[432];
};
#define QT_MOC_LITERAL(idx, ofs, len) \
    Q_STATIC_BYTE_ARRAY_DATA_HEADER_INITIALIZER_WITH_OFFSET(len, \
    qptrdiff(offsetof(qt_meta_stringdata_Qt3DRender__QGraphicsApiFilter_t, stringdata0) + ofs \
        - idx * sizeof(QByteArrayData)) \
    )
static const qt_meta_stringdata_Qt3DRender__QGraphicsApiFilter_t qt_meta_stringdata_Qt3DRender__QGraphicsApiFilter = {
    {
QT_MOC_LITERAL(0, 0, 30), // "Qt3DRender::QGraphicsApiFilter"
QT_MOC_LITERAL(1, 31, 10), // "apiChanged"
QT_MOC_LITERAL(2, 42, 0), // ""
QT_MOC_LITERAL(3, 43, 3), // "Api"
QT_MOC_LITERAL(4, 47, 3), // "api"
QT_MOC_LITERAL(5, 51, 14), // "profileChanged"
QT_MOC_LITERAL(6, 66, 7), // "Profile"
QT_MOC_LITERAL(7, 74, 7), // "profile"
QT_MOC_LITERAL(8, 82, 19), // "minorVersionChanged"
QT_MOC_LITERAL(9, 102, 12), // "minorVersion"
QT_MOC_LITERAL(10, 115, 19), // "majorVersionChanged"
QT_MOC_LITERAL(11, 135, 12), // "majorVersion"
QT_MOC_LITERAL(12, 148, 17), // "extensionsChanged"
QT_MOC_LITERAL(13, 166, 10), // "extensions"
QT_MOC_LITERAL(14, 177, 13), // "vendorChanged"
QT_MOC_LITERAL(15, 191, 6), // "vendor"
QT_MOC_LITERAL(16, 198, 24), // "graphicsApiFilterChanged"
QT_MOC_LITERAL(17, 223, 6), // "setApi"
QT_MOC_LITERAL(18, 230, 10), // "setProfile"
QT_MOC_LITERAL(19, 241, 15), // "setMinorVersion"
QT_MOC_LITERAL(20, 257, 15), // "setMajorVersion"
QT_MOC_LITERAL(21, 273, 13), // "setExtensions"
QT_MOC_LITERAL(22, 287, 9), // "setVendor"
QT_MOC_LITERAL(23, 297, 35), // "Qt3DRender::QGraphicsApiFilte..."
QT_MOC_LITERAL(24, 333, 39), // "Qt3DRender::QGraphicsApiFilte..."
QT_MOC_LITERAL(25, 373, 8), // "OpenGLES"
QT_MOC_LITERAL(26, 382, 6), // "OpenGL"
QT_MOC_LITERAL(27, 389, 9), // "NoProfile"
QT_MOC_LITERAL(28, 399, 11), // "CoreProfile"
QT_MOC_LITERAL(29, 411, 20) // "CompatibilityProfile"

    },
    "Qt3DRender::QGraphicsApiFilter\0"
    "apiChanged\0\0Api\0api\0profileChanged\0"
    "Profile\0profile\0minorVersionChanged\0"
    "minorVersion\0majorVersionChanged\0"
    "majorVersion\0extensionsChanged\0"
    "extensions\0vendorChanged\0vendor\0"
    "graphicsApiFilterChanged\0setApi\0"
    "setProfile\0setMinorVersion\0setMajorVersion\0"
    "setExtensions\0setVendor\0"
    "Qt3DRender::QGraphicsApiFilter::Api\0"
    "Qt3DRender::QGraphicsApiFilter::Profile\0"
    "OpenGLES\0OpenGL\0NoProfile\0CoreProfile\0"
    "CompatibilityProfile"
};
#undef QT_MOC_LITERAL

static const uint qt_meta_data_Qt3DRender__QGraphicsApiFilter[] = {

 // content:
       7,       // revision
       0,       // classname
       0,    0, // classinfo
      13,   14, // methods
       6,  116, // properties
       2,  140, // enums/sets
       0,    0, // constructors
       0,       // flags
       7,       // signalCount

 // signals: name, argc, parameters, tag, flags
       1,    1,   79,    2, 0x06 /* Public */,
       5,    1,   82,    2, 0x06 /* Public */,
       8,    1,   85,    2, 0x06 /* Public */,
      10,    1,   88,    2, 0x06 /* Public */,
      12,    1,   91,    2, 0x06 /* Public */,
      14,    1,   94,    2, 0x06 /* Public */,
      16,    0,   97,    2, 0x06 /* Public */,

 // slots: name, argc, parameters, tag, flags
      17,    1,   98,    2, 0x0a /* Public */,
      18,    1,  101,    2, 0x0a /* Public */,
      19,    1,  104,    2, 0x0a /* Public */,
      20,    1,  107,    2, 0x0a /* Public */,
      21,    1,  110,    2, 0x0a /* Public */,
      22,    1,  113,    2, 0x0a /* Public */,

 // signals: parameters
    QMetaType::Void, 0x80000000 | 3,    4,
    QMetaType::Void, 0x80000000 | 6,    7,
    QMetaType::Void, QMetaType::Int,    9,
    QMetaType::Void, QMetaType::Int,   11,
    QMetaType::Void, QMetaType::QStringList,   13,
    QMetaType::Void, QMetaType::QString,   15,
    QMetaType::Void,

 // slots: parameters
    QMetaType::Void, 0x80000000 | 3,    4,
    QMetaType::Void, 0x80000000 | 6,    7,
    QMetaType::Void, QMetaType::Int,    9,
    QMetaType::Void, QMetaType::Int,   11,
    QMetaType::Void, QMetaType::QStringList,   13,
    QMetaType::Void, QMetaType::QString,   15,

 // properties: name, type, flags
       4, 0x80000000 | 23, 0x0049510b,
       7, 0x80000000 | 24, 0x0049510b,
       9, QMetaType::Int, 0x00495103,
      11, QMetaType::Int, 0x00495103,
      13, QMetaType::QStringList, 0x00495103,
      15, QMetaType::QString, 0x00495103,

 // properties: notify_signal_id
       0,
       1,
       2,
       3,
       4,
       5,

 // enums: name, flags, count, data
       3, 0x0,    2,  148,
       6, 0x0,    3,  152,

 // enum data: key, value
      25, uint(Qt3DRender::QGraphicsApiFilter::OpenGLES),
      26, uint(Qt3DRender::QGraphicsApiFilter::OpenGL),
      27, uint(Qt3DRender::QGraphicsApiFilter::NoProfile),
      28, uint(Qt3DRender::QGraphicsApiFilter::CoreProfile),
      29, uint(Qt3DRender::QGraphicsApiFilter::CompatibilityProfile),

       0        // eod
};

void Qt3DRender::QGraphicsApiFilter::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        QGraphicsApiFilter *_t = static_cast<QGraphicsApiFilter *>(_o);
        Q_UNUSED(_t)
        switch (_id) {
        case 0: _t->apiChanged((*reinterpret_cast< Api(*)>(_a[1]))); break;
        case 1: _t->profileChanged((*reinterpret_cast< Profile(*)>(_a[1]))); break;
        case 2: _t->minorVersionChanged((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 3: _t->majorVersionChanged((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 4: _t->extensionsChanged((*reinterpret_cast< const QStringList(*)>(_a[1]))); break;
        case 5: _t->vendorChanged((*reinterpret_cast< const QString(*)>(_a[1]))); break;
        case 6: _t->graphicsApiFilterChanged(); break;
        case 7: _t->setApi((*reinterpret_cast< Api(*)>(_a[1]))); break;
        case 8: _t->setProfile((*reinterpret_cast< Profile(*)>(_a[1]))); break;
        case 9: _t->setMinorVersion((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 10: _t->setMajorVersion((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 11: _t->setExtensions((*reinterpret_cast< const QStringList(*)>(_a[1]))); break;
        case 12: _t->setVendor((*reinterpret_cast< const QString(*)>(_a[1]))); break;
        default: ;
        }
    } else if (_c == QMetaObject::IndexOfMethod) {
        int *result = reinterpret_cast<int *>(_a[0]);
        void **func = reinterpret_cast<void **>(_a[1]);
        {
            typedef void (QGraphicsApiFilter::*_t)(Api );
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&QGraphicsApiFilter::apiChanged)) {
                *result = 0;
                return;
            }
        }
        {
            typedef void (QGraphicsApiFilter::*_t)(Profile );
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&QGraphicsApiFilter::profileChanged)) {
                *result = 1;
                return;
            }
        }
        {
            typedef void (QGraphicsApiFilter::*_t)(int );
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&QGraphicsApiFilter::minorVersionChanged)) {
                *result = 2;
                return;
            }
        }
        {
            typedef void (QGraphicsApiFilter::*_t)(int );
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&QGraphicsApiFilter::majorVersionChanged)) {
                *result = 3;
                return;
            }
        }
        {
            typedef void (QGraphicsApiFilter::*_t)(const QStringList & );
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&QGraphicsApiFilter::extensionsChanged)) {
                *result = 4;
                return;
            }
        }
        {
            typedef void (QGraphicsApiFilter::*_t)(const QString & );
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&QGraphicsApiFilter::vendorChanged)) {
                *result = 5;
                return;
            }
        }
        {
            typedef void (QGraphicsApiFilter::*_t)();
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&QGraphicsApiFilter::graphicsApiFilterChanged)) {
                *result = 6;
                return;
            }
        }
    }
#ifndef QT_NO_PROPERTIES
    else if (_c == QMetaObject::ReadProperty) {
        QGraphicsApiFilter *_t = static_cast<QGraphicsApiFilter *>(_o);
        Q_UNUSED(_t)
        void *_v = _a[0];
        switch (_id) {
        case 0: *reinterpret_cast< Qt3DRender::QGraphicsApiFilter::Api*>(_v) = _t->api(); break;
        case 1: *reinterpret_cast< Qt3DRender::QGraphicsApiFilter::Profile*>(_v) = _t->profile(); break;
        case 2: *reinterpret_cast< int*>(_v) = _t->minorVersion(); break;
        case 3: *reinterpret_cast< int*>(_v) = _t->majorVersion(); break;
        case 4: *reinterpret_cast< QStringList*>(_v) = _t->extensions(); break;
        case 5: *reinterpret_cast< QString*>(_v) = _t->vendor(); break;
        default: break;
        }
    } else if (_c == QMetaObject::WriteProperty) {
        QGraphicsApiFilter *_t = static_cast<QGraphicsApiFilter *>(_o);
        Q_UNUSED(_t)
        void *_v = _a[0];
        switch (_id) {
        case 0: _t->setApi(*reinterpret_cast< Qt3DRender::QGraphicsApiFilter::Api*>(_v)); break;
        case 1: _t->setProfile(*reinterpret_cast< Qt3DRender::QGraphicsApiFilter::Profile*>(_v)); break;
        case 2: _t->setMinorVersion(*reinterpret_cast< int*>(_v)); break;
        case 3: _t->setMajorVersion(*reinterpret_cast< int*>(_v)); break;
        case 4: _t->setExtensions(*reinterpret_cast< QStringList*>(_v)); break;
        case 5: _t->setVendor(*reinterpret_cast< QString*>(_v)); break;
        default: break;
        }
    } else if (_c == QMetaObject::ResetProperty) {
    }
#endif // QT_NO_PROPERTIES
}

const QMetaObject Qt3DRender::QGraphicsApiFilter::staticMetaObject = {
    { &QObject::staticMetaObject, qt_meta_stringdata_Qt3DRender__QGraphicsApiFilter.data,
      qt_meta_data_Qt3DRender__QGraphicsApiFilter,  qt_static_metacall, Q_NULLPTR, Q_NULLPTR}
};


const QMetaObject *Qt3DRender::QGraphicsApiFilter::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->dynamicMetaObject() : &staticMetaObject;
}

void *Qt3DRender::QGraphicsApiFilter::qt_metacast(const char *_clname)
{
    if (!_clname) return Q_NULLPTR;
    if (!strcmp(_clname, qt_meta_stringdata_Qt3DRender__QGraphicsApiFilter.stringdata0))
        return static_cast<void*>(const_cast< QGraphicsApiFilter*>(this));
    return QObject::qt_metacast(_clname);
}

int Qt3DRender::QGraphicsApiFilter::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QObject::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 13)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 13;
    } else if (_c == QMetaObject::RegisterMethodArgumentMetaType) {
        if (_id < 13)
            *reinterpret_cast<int*>(_a[0]) = -1;
        _id -= 13;
    }
#ifndef QT_NO_PROPERTIES
   else if (_c == QMetaObject::ReadProperty || _c == QMetaObject::WriteProperty
            || _c == QMetaObject::ResetProperty || _c == QMetaObject::RegisterPropertyMetaType) {
        qt_static_metacall(this, _c, _id, _a);
        _id -= 6;
    } else if (_c == QMetaObject::QueryPropertyDesignable) {
        _id -= 6;
    } else if (_c == QMetaObject::QueryPropertyScriptable) {
        _id -= 6;
    } else if (_c == QMetaObject::QueryPropertyStored) {
        _id -= 6;
    } else if (_c == QMetaObject::QueryPropertyEditable) {
        _id -= 6;
    } else if (_c == QMetaObject::QueryPropertyUser) {
        _id -= 6;
    }
#endif // QT_NO_PROPERTIES
    return _id;
}

// SIGNAL 0
void Qt3DRender::QGraphicsApiFilter::apiChanged(Api _t1)
{
    void *_a[] = { Q_NULLPTR, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 0, _a);
}

// SIGNAL 1
void Qt3DRender::QGraphicsApiFilter::profileChanged(Profile _t1)
{
    void *_a[] = { Q_NULLPTR, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 1, _a);
}

// SIGNAL 2
void Qt3DRender::QGraphicsApiFilter::minorVersionChanged(int _t1)
{
    void *_a[] = { Q_NULLPTR, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 2, _a);
}

// SIGNAL 3
void Qt3DRender::QGraphicsApiFilter::majorVersionChanged(int _t1)
{
    void *_a[] = { Q_NULLPTR, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 3, _a);
}

// SIGNAL 4
void Qt3DRender::QGraphicsApiFilter::extensionsChanged(const QStringList & _t1)
{
    void *_a[] = { Q_NULLPTR, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 4, _a);
}

// SIGNAL 5
void Qt3DRender::QGraphicsApiFilter::vendorChanged(const QString & _t1)
{
    void *_a[] = { Q_NULLPTR, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 5, _a);
}

// SIGNAL 6
void Qt3DRender::QGraphicsApiFilter::graphicsApiFilterChanged()
{
    QMetaObject::activate(this, &staticMetaObject, 6, Q_NULLPTR);
}
QT_END_MOC_NAMESPACE
