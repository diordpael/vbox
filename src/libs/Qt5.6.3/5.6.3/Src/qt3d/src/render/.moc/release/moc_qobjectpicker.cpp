/****************************************************************************
** Meta object code from reading C++ file 'qobjectpicker.h'
**
** Created by: The Qt Meta Object Compiler version 67 (Qt 5.6.3)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "../../picking/qobjectpicker.h"
#include <QtCore/qbytearray.h>
#include <QtCore/qmetatype.h>
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'qobjectpicker.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 67
#error "This file was generated using the moc from 5.6.3. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
struct qt_meta_stringdata_Qt3DRender__QObjectPicker_t {
    QByteArrayData data[15];
    char stringdata0[196];
};
#define QT_MOC_LITERAL(idx, ofs, len) \
    Q_STATIC_BYTE_ARRAY_DATA_HEADER_INITIALIZER_WITH_OFFSET(len, \
    qptrdiff(offsetof(qt_meta_stringdata_Qt3DRender__QObjectPicker_t, stringdata0) + ofs \
        - idx * sizeof(QByteArrayData)) \
    )
static const qt_meta_stringdata_Qt3DRender__QObjectPicker_t qt_meta_stringdata_Qt3DRender__QObjectPicker = {
    {
QT_MOC_LITERAL(0, 0, 25), // "Qt3DRender::QObjectPicker"
QT_MOC_LITERAL(1, 26, 7), // "pressed"
QT_MOC_LITERAL(2, 34, 0), // ""
QT_MOC_LITERAL(3, 35, 23), // "Qt3DRender::QPickEvent*"
QT_MOC_LITERAL(4, 59, 5), // "event"
QT_MOC_LITERAL(5, 65, 8), // "released"
QT_MOC_LITERAL(6, 74, 7), // "clicked"
QT_MOC_LITERAL(7, 82, 7), // "entered"
QT_MOC_LITERAL(8, 90, 6), // "exited"
QT_MOC_LITERAL(9, 97, 19), // "hoverEnabledChanged"
QT_MOC_LITERAL(10, 117, 12), // "hoverEnabled"
QT_MOC_LITERAL(11, 130, 14), // "pressedChanged"
QT_MOC_LITERAL(12, 145, 20), // "containsMouseChanged"
QT_MOC_LITERAL(13, 166, 13), // "containsMouse"
QT_MOC_LITERAL(14, 180, 15) // "setHoverEnabled"

    },
    "Qt3DRender::QObjectPicker\0pressed\0\0"
    "Qt3DRender::QPickEvent*\0event\0released\0"
    "clicked\0entered\0exited\0hoverEnabledChanged\0"
    "hoverEnabled\0pressedChanged\0"
    "containsMouseChanged\0containsMouse\0"
    "setHoverEnabled"
};
#undef QT_MOC_LITERAL

static const uint qt_meta_data_Qt3DRender__QObjectPicker[] = {

 // content:
       7,       // revision
       0,       // classname
       0,    0, // classinfo
       9,   14, // methods
       3,   82, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       8,       // signalCount

 // signals: name, argc, parameters, tag, flags
       1,    1,   59,    2, 0x06 /* Public */,
       5,    1,   62,    2, 0x06 /* Public */,
       6,    1,   65,    2, 0x06 /* Public */,
       7,    0,   68,    2, 0x06 /* Public */,
       8,    0,   69,    2, 0x06 /* Public */,
       9,    1,   70,    2, 0x06 /* Public */,
      11,    1,   73,    2, 0x06 /* Public */,
      12,    1,   76,    2, 0x06 /* Public */,

 // slots: name, argc, parameters, tag, flags
      14,    1,   79,    2, 0x0a /* Public */,

 // signals: parameters
    QMetaType::Void, 0x80000000 | 3,    4,
    QMetaType::Void, 0x80000000 | 3,    4,
    QMetaType::Void, 0x80000000 | 3,    4,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void, QMetaType::Bool,   10,
    QMetaType::Void, QMetaType::Bool,    1,
    QMetaType::Void, QMetaType::Bool,   13,

 // slots: parameters
    QMetaType::Void, QMetaType::Bool,   10,

 // properties: name, type, flags
      10, QMetaType::Bool, 0x00495103,
       1, QMetaType::Bool, 0x00495001,
      13, QMetaType::Bool, 0x00495001,

 // properties: notify_signal_id
       5,
       6,
       7,

       0        // eod
};

void Qt3DRender::QObjectPicker::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        QObjectPicker *_t = static_cast<QObjectPicker *>(_o);
        Q_UNUSED(_t)
        switch (_id) {
        case 0: _t->pressed((*reinterpret_cast< Qt3DRender::QPickEvent*(*)>(_a[1]))); break;
        case 1: _t->released((*reinterpret_cast< Qt3DRender::QPickEvent*(*)>(_a[1]))); break;
        case 2: _t->clicked((*reinterpret_cast< Qt3DRender::QPickEvent*(*)>(_a[1]))); break;
        case 3: _t->entered(); break;
        case 4: _t->exited(); break;
        case 5: _t->hoverEnabledChanged((*reinterpret_cast< bool(*)>(_a[1]))); break;
        case 6: _t->pressedChanged((*reinterpret_cast< bool(*)>(_a[1]))); break;
        case 7: _t->containsMouseChanged((*reinterpret_cast< bool(*)>(_a[1]))); break;
        case 8: _t->setHoverEnabled((*reinterpret_cast< bool(*)>(_a[1]))); break;
        default: ;
        }
    } else if (_c == QMetaObject::IndexOfMethod) {
        int *result = reinterpret_cast<int *>(_a[0]);
        void **func = reinterpret_cast<void **>(_a[1]);
        {
            typedef void (QObjectPicker::*_t)(Qt3DRender::QPickEvent * );
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&QObjectPicker::pressed)) {
                *result = 0;
                return;
            }
        }
        {
            typedef void (QObjectPicker::*_t)(Qt3DRender::QPickEvent * );
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&QObjectPicker::released)) {
                *result = 1;
                return;
            }
        }
        {
            typedef void (QObjectPicker::*_t)(Qt3DRender::QPickEvent * );
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&QObjectPicker::clicked)) {
                *result = 2;
                return;
            }
        }
        {
            typedef void (QObjectPicker::*_t)();
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&QObjectPicker::entered)) {
                *result = 3;
                return;
            }
        }
        {
            typedef void (QObjectPicker::*_t)();
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&QObjectPicker::exited)) {
                *result = 4;
                return;
            }
        }
        {
            typedef void (QObjectPicker::*_t)(bool );
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&QObjectPicker::hoverEnabledChanged)) {
                *result = 5;
                return;
            }
        }
        {
            typedef void (QObjectPicker::*_t)(bool );
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&QObjectPicker::pressedChanged)) {
                *result = 6;
                return;
            }
        }
        {
            typedef void (QObjectPicker::*_t)(bool );
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&QObjectPicker::containsMouseChanged)) {
                *result = 7;
                return;
            }
        }
    }
#ifndef QT_NO_PROPERTIES
    else if (_c == QMetaObject::ReadProperty) {
        QObjectPicker *_t = static_cast<QObjectPicker *>(_o);
        Q_UNUSED(_t)
        void *_v = _a[0];
        switch (_id) {
        case 0: *reinterpret_cast< bool*>(_v) = _t->hoverEnabled(); break;
        case 1: *reinterpret_cast< bool*>(_v) = _t->isPressed(); break;
        case 2: *reinterpret_cast< bool*>(_v) = _t->containsMouse(); break;
        default: break;
        }
    } else if (_c == QMetaObject::WriteProperty) {
        QObjectPicker *_t = static_cast<QObjectPicker *>(_o);
        Q_UNUSED(_t)
        void *_v = _a[0];
        switch (_id) {
        case 0: _t->setHoverEnabled(*reinterpret_cast< bool*>(_v)); break;
        default: break;
        }
    } else if (_c == QMetaObject::ResetProperty) {
    }
#endif // QT_NO_PROPERTIES
}

const QMetaObject Qt3DRender::QObjectPicker::staticMetaObject = {
    { &Qt3DCore::QComponent::staticMetaObject, qt_meta_stringdata_Qt3DRender__QObjectPicker.data,
      qt_meta_data_Qt3DRender__QObjectPicker,  qt_static_metacall, Q_NULLPTR, Q_NULLPTR}
};


const QMetaObject *Qt3DRender::QObjectPicker::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->dynamicMetaObject() : &staticMetaObject;
}

void *Qt3DRender::QObjectPicker::qt_metacast(const char *_clname)
{
    if (!_clname) return Q_NULLPTR;
    if (!strcmp(_clname, qt_meta_stringdata_Qt3DRender__QObjectPicker.stringdata0))
        return static_cast<void*>(const_cast< QObjectPicker*>(this));
    return Qt3DCore::QComponent::qt_metacast(_clname);
}

int Qt3DRender::QObjectPicker::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = Qt3DCore::QComponent::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 9)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 9;
    } else if (_c == QMetaObject::RegisterMethodArgumentMetaType) {
        if (_id < 9)
            *reinterpret_cast<int*>(_a[0]) = -1;
        _id -= 9;
    }
#ifndef QT_NO_PROPERTIES
   else if (_c == QMetaObject::ReadProperty || _c == QMetaObject::WriteProperty
            || _c == QMetaObject::ResetProperty || _c == QMetaObject::RegisterPropertyMetaType) {
        qt_static_metacall(this, _c, _id, _a);
        _id -= 3;
    } else if (_c == QMetaObject::QueryPropertyDesignable) {
        _id -= 3;
    } else if (_c == QMetaObject::QueryPropertyScriptable) {
        _id -= 3;
    } else if (_c == QMetaObject::QueryPropertyStored) {
        _id -= 3;
    } else if (_c == QMetaObject::QueryPropertyEditable) {
        _id -= 3;
    } else if (_c == QMetaObject::QueryPropertyUser) {
        _id -= 3;
    }
#endif // QT_NO_PROPERTIES
    return _id;
}

// SIGNAL 0
void Qt3DRender::QObjectPicker::pressed(Qt3DRender::QPickEvent * _t1)
{
    void *_a[] = { Q_NULLPTR, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 0, _a);
}

// SIGNAL 1
void Qt3DRender::QObjectPicker::released(Qt3DRender::QPickEvent * _t1)
{
    void *_a[] = { Q_NULLPTR, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 1, _a);
}

// SIGNAL 2
void Qt3DRender::QObjectPicker::clicked(Qt3DRender::QPickEvent * _t1)
{
    void *_a[] = { Q_NULLPTR, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 2, _a);
}

// SIGNAL 3
void Qt3DRender::QObjectPicker::entered()
{
    QMetaObject::activate(this, &staticMetaObject, 3, Q_NULLPTR);
}

// SIGNAL 4
void Qt3DRender::QObjectPicker::exited()
{
    QMetaObject::activate(this, &staticMetaObject, 4, Q_NULLPTR);
}

// SIGNAL 5
void Qt3DRender::QObjectPicker::hoverEnabledChanged(bool _t1)
{
    void *_a[] = { Q_NULLPTR, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 5, _a);
}

// SIGNAL 6
void Qt3DRender::QObjectPicker::pressedChanged(bool _t1)
{
    void *_a[] = { Q_NULLPTR, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 6, _a);
}

// SIGNAL 7
void Qt3DRender::QObjectPicker::containsMouseChanged(bool _t1)
{
    void *_a[] = { Q_NULLPTR, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 7, _a);
}
QT_END_MOC_NAMESPACE
