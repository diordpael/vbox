/****************************************************************************
** Meta object code from reading C++ file 'qabstracttextureimage.h'
**
** Created by: The Qt Meta Object Compiler version 67 (Qt 5.6.3)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "../../texture/qabstracttextureimage.h"
#include <QtCore/qbytearray.h>
#include <QtCore/qmetatype.h>
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'qabstracttextureimage.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 67
#error "This file was generated using the moc from 5.6.3. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
struct qt_meta_stringdata_Qt3DRender__QAbstractTextureImage_t {
    QByteArrayData data[15];
    char stringdata0[254];
};
#define QT_MOC_LITERAL(idx, ofs, len) \
    Q_STATIC_BYTE_ARRAY_DATA_HEADER_INITIALIZER_WITH_OFFSET(len, \
    qptrdiff(offsetof(qt_meta_stringdata_Qt3DRender__QAbstractTextureImage_t, stringdata0) + ofs \
        - idx * sizeof(QByteArrayData)) \
    )
static const qt_meta_stringdata_Qt3DRender__QAbstractTextureImage_t qt_meta_stringdata_Qt3DRender__QAbstractTextureImage = {
    {
QT_MOC_LITERAL(0, 0, 33), // "Qt3DRender::QAbstractTextureI..."
QT_MOC_LITERAL(1, 34, 18), // "mipmapLevelChanged"
QT_MOC_LITERAL(2, 53, 0), // ""
QT_MOC_LITERAL(3, 54, 11), // "mipmapLevel"
QT_MOC_LITERAL(4, 66, 12), // "layerChanged"
QT_MOC_LITERAL(5, 79, 5), // "layer"
QT_MOC_LITERAL(6, 85, 18), // "cubeMapFaceChanged"
QT_MOC_LITERAL(7, 104, 37), // "QAbstractTextureProvider::Cub..."
QT_MOC_LITERAL(8, 142, 11), // "cubeMapFace"
QT_MOC_LITERAL(9, 154, 14), // "setMipmapLevel"
QT_MOC_LITERAL(10, 169, 5), // "level"
QT_MOC_LITERAL(11, 175, 8), // "setLayer"
QT_MOC_LITERAL(12, 184, 14), // "setCubeMapFace"
QT_MOC_LITERAL(13, 199, 4), // "face"
QT_MOC_LITERAL(14, 204, 49) // "Qt3DRender::QAbstractTextureP..."

    },
    "Qt3DRender::QAbstractTextureImage\0"
    "mipmapLevelChanged\0\0mipmapLevel\0"
    "layerChanged\0layer\0cubeMapFaceChanged\0"
    "QAbstractTextureProvider::CubeMapFace\0"
    "cubeMapFace\0setMipmapLevel\0level\0"
    "setLayer\0setCubeMapFace\0face\0"
    "Qt3DRender::QAbstractTextureProvider::CubeMapFace"
};
#undef QT_MOC_LITERAL

static const uint qt_meta_data_Qt3DRender__QAbstractTextureImage[] = {

 // content:
       7,       // revision
       0,       // classname
       0,    0, // classinfo
       6,   14, // methods
       3,   62, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       3,       // signalCount

 // signals: name, argc, parameters, tag, flags
       1,    1,   44,    2, 0x06 /* Public */,
       4,    1,   47,    2, 0x06 /* Public */,
       6,    1,   50,    2, 0x06 /* Public */,

 // slots: name, argc, parameters, tag, flags
       9,    1,   53,    2, 0x0a /* Public */,
      11,    1,   56,    2, 0x0a /* Public */,
      12,    1,   59,    2, 0x0a /* Public */,

 // signals: parameters
    QMetaType::Void, QMetaType::Int,    3,
    QMetaType::Void, QMetaType::Int,    5,
    QMetaType::Void, 0x80000000 | 7,    8,

 // slots: parameters
    QMetaType::Void, QMetaType::Int,   10,
    QMetaType::Void, QMetaType::Int,    5,
    QMetaType::Void, 0x80000000 | 7,   13,

 // properties: name, type, flags
       3, QMetaType::Int, 0x00495103,
       5, QMetaType::Int, 0x00495103,
       8, 0x80000000 | 14, 0x0049510b,

 // properties: notify_signal_id
       0,
       1,
       2,

       0        // eod
};

void Qt3DRender::QAbstractTextureImage::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        QAbstractTextureImage *_t = static_cast<QAbstractTextureImage *>(_o);
        Q_UNUSED(_t)
        switch (_id) {
        case 0: _t->mipmapLevelChanged((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 1: _t->layerChanged((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 2: _t->cubeMapFaceChanged((*reinterpret_cast< QAbstractTextureProvider::CubeMapFace(*)>(_a[1]))); break;
        case 3: _t->setMipmapLevel((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 4: _t->setLayer((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 5: _t->setCubeMapFace((*reinterpret_cast< QAbstractTextureProvider::CubeMapFace(*)>(_a[1]))); break;
        default: ;
        }
    } else if (_c == QMetaObject::IndexOfMethod) {
        int *result = reinterpret_cast<int *>(_a[0]);
        void **func = reinterpret_cast<void **>(_a[1]);
        {
            typedef void (QAbstractTextureImage::*_t)(int );
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&QAbstractTextureImage::mipmapLevelChanged)) {
                *result = 0;
                return;
            }
        }
        {
            typedef void (QAbstractTextureImage::*_t)(int );
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&QAbstractTextureImage::layerChanged)) {
                *result = 1;
                return;
            }
        }
        {
            typedef void (QAbstractTextureImage::*_t)(QAbstractTextureProvider::CubeMapFace );
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&QAbstractTextureImage::cubeMapFaceChanged)) {
                *result = 2;
                return;
            }
        }
    }
#ifndef QT_NO_PROPERTIES
    else if (_c == QMetaObject::ReadProperty) {
        QAbstractTextureImage *_t = static_cast<QAbstractTextureImage *>(_o);
        Q_UNUSED(_t)
        void *_v = _a[0];
        switch (_id) {
        case 0: *reinterpret_cast< int*>(_v) = _t->mipmapLevel(); break;
        case 1: *reinterpret_cast< int*>(_v) = _t->layer(); break;
        case 2: *reinterpret_cast< Qt3DRender::QAbstractTextureProvider::CubeMapFace*>(_v) = _t->cubeMapFace(); break;
        default: break;
        }
    } else if (_c == QMetaObject::WriteProperty) {
        QAbstractTextureImage *_t = static_cast<QAbstractTextureImage *>(_o);
        Q_UNUSED(_t)
        void *_v = _a[0];
        switch (_id) {
        case 0: _t->setMipmapLevel(*reinterpret_cast< int*>(_v)); break;
        case 1: _t->setLayer(*reinterpret_cast< int*>(_v)); break;
        case 2: _t->setCubeMapFace(*reinterpret_cast< Qt3DRender::QAbstractTextureProvider::CubeMapFace*>(_v)); break;
        default: break;
        }
    } else if (_c == QMetaObject::ResetProperty) {
    }
#endif // QT_NO_PROPERTIES
}

static const QMetaObject * const qt_meta_extradata_Qt3DRender__QAbstractTextureImage[] = {
        &Qt3DRender::QAbstractTextureProvider::staticMetaObject,
    Q_NULLPTR
};

const QMetaObject Qt3DRender::QAbstractTextureImage::staticMetaObject = {
    { &Qt3DCore::QNode::staticMetaObject, qt_meta_stringdata_Qt3DRender__QAbstractTextureImage.data,
      qt_meta_data_Qt3DRender__QAbstractTextureImage,  qt_static_metacall, qt_meta_extradata_Qt3DRender__QAbstractTextureImage, Q_NULLPTR}
};


const QMetaObject *Qt3DRender::QAbstractTextureImage::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->dynamicMetaObject() : &staticMetaObject;
}

void *Qt3DRender::QAbstractTextureImage::qt_metacast(const char *_clname)
{
    if (!_clname) return Q_NULLPTR;
    if (!strcmp(_clname, qt_meta_stringdata_Qt3DRender__QAbstractTextureImage.stringdata0))
        return static_cast<void*>(const_cast< QAbstractTextureImage*>(this));
    return Qt3DCore::QNode::qt_metacast(_clname);
}

int Qt3DRender::QAbstractTextureImage::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = Qt3DCore::QNode::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 6)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 6;
    } else if (_c == QMetaObject::RegisterMethodArgumentMetaType) {
        if (_id < 6)
            *reinterpret_cast<int*>(_a[0]) = -1;
        _id -= 6;
    }
#ifndef QT_NO_PROPERTIES
   else if (_c == QMetaObject::ReadProperty || _c == QMetaObject::WriteProperty
            || _c == QMetaObject::ResetProperty || _c == QMetaObject::RegisterPropertyMetaType) {
        qt_static_metacall(this, _c, _id, _a);
        _id -= 3;
    } else if (_c == QMetaObject::QueryPropertyDesignable) {
        _id -= 3;
    } else if (_c == QMetaObject::QueryPropertyScriptable) {
        _id -= 3;
    } else if (_c == QMetaObject::QueryPropertyStored) {
        _id -= 3;
    } else if (_c == QMetaObject::QueryPropertyEditable) {
        _id -= 3;
    } else if (_c == QMetaObject::QueryPropertyUser) {
        _id -= 3;
    }
#endif // QT_NO_PROPERTIES
    return _id;
}

// SIGNAL 0
void Qt3DRender::QAbstractTextureImage::mipmapLevelChanged(int _t1)
{
    void *_a[] = { Q_NULLPTR, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 0, _a);
}

// SIGNAL 1
void Qt3DRender::QAbstractTextureImage::layerChanged(int _t1)
{
    void *_a[] = { Q_NULLPTR, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 1, _a);
}

// SIGNAL 2
void Qt3DRender::QAbstractTextureImage::cubeMapFaceChanged(QAbstractTextureProvider::CubeMapFace _t1)
{
    void *_a[] = { Q_NULLPTR, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 2, _a);
}
QT_END_MOC_NAMESPACE
