/****************************************************************************
** Meta object code from reading C++ file 'qblendstate.h'
**
** Created by: The Qt Meta Object Compiler version 67 (Qt 5.6.3)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "../../renderstates/qblendstate.h"
#include <QtCore/qbytearray.h>
#include <QtCore/qmetatype.h>
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'qblendstate.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 67
#error "This file was generated using the moc from 5.6.3. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
struct qt_meta_stringdata_Qt3DRender__QBlendState_t {
    QByteArrayData data[34];
    char stringdata0[429];
};
#define QT_MOC_LITERAL(idx, ofs, len) \
    Q_STATIC_BYTE_ARRAY_DATA_HEADER_INITIALIZER_WITH_OFFSET(len, \
    qptrdiff(offsetof(qt_meta_stringdata_Qt3DRender__QBlendState_t, stringdata0) + ofs \
        - idx * sizeof(QByteArrayData)) \
    )
static const qt_meta_stringdata_Qt3DRender__QBlendState_t qt_meta_stringdata_Qt3DRender__QBlendState = {
    {
QT_MOC_LITERAL(0, 0, 23), // "Qt3DRender::QBlendState"
QT_MOC_LITERAL(1, 24, 13), // "srcRGBChanged"
QT_MOC_LITERAL(2, 38, 0), // ""
QT_MOC_LITERAL(3, 39, 8), // "Blending"
QT_MOC_LITERAL(4, 48, 6), // "srcRGB"
QT_MOC_LITERAL(5, 55, 15), // "srcAlphaChanged"
QT_MOC_LITERAL(6, 71, 8), // "srcAlpha"
QT_MOC_LITERAL(7, 80, 13), // "dstRGBChanged"
QT_MOC_LITERAL(8, 94, 6), // "dstRGB"
QT_MOC_LITERAL(9, 101, 15), // "dstAlphaChanged"
QT_MOC_LITERAL(10, 117, 8), // "dstAlpha"
QT_MOC_LITERAL(11, 126, 9), // "setSrcRGB"
QT_MOC_LITERAL(12, 136, 9), // "setDstRGB"
QT_MOC_LITERAL(13, 146, 11), // "setSrcAlpha"
QT_MOC_LITERAL(14, 158, 11), // "setDstAlpha"
QT_MOC_LITERAL(15, 170, 4), // "Zero"
QT_MOC_LITERAL(16, 175, 3), // "One"
QT_MOC_LITERAL(17, 179, 8), // "SrcColor"
QT_MOC_LITERAL(18, 188, 8), // "SrcAlpha"
QT_MOC_LITERAL(19, 197, 9), // "Src1Alpha"
QT_MOC_LITERAL(20, 207, 9), // "Src1Color"
QT_MOC_LITERAL(21, 217, 8), // "DstColor"
QT_MOC_LITERAL(22, 226, 8), // "DstAlpha"
QT_MOC_LITERAL(23, 235, 16), // "SrcAlphaSaturate"
QT_MOC_LITERAL(24, 252, 13), // "ConstantColor"
QT_MOC_LITERAL(25, 266, 13), // "ConstantAlpha"
QT_MOC_LITERAL(26, 280, 16), // "OneMinusSrcColor"
QT_MOC_LITERAL(27, 297, 16), // "OneMinusSrcAlpha"
QT_MOC_LITERAL(28, 314, 16), // "OneMinusDstAlpha"
QT_MOC_LITERAL(29, 331, 16), // "OneMinusDstColor"
QT_MOC_LITERAL(30, 348, 21), // "OneMinusConstantColor"
QT_MOC_LITERAL(31, 370, 21), // "OneMinusConstantAlpha"
QT_MOC_LITERAL(32, 392, 17), // "OneMinusSrc1Alpha"
QT_MOC_LITERAL(33, 410, 18) // "OneMinusSrc1Color0"

    },
    "Qt3DRender::QBlendState\0srcRGBChanged\0"
    "\0Blending\0srcRGB\0srcAlphaChanged\0"
    "srcAlpha\0dstRGBChanged\0dstRGB\0"
    "dstAlphaChanged\0dstAlpha\0setSrcRGB\0"
    "setDstRGB\0setSrcAlpha\0setDstAlpha\0"
    "Zero\0One\0SrcColor\0SrcAlpha\0Src1Alpha\0"
    "Src1Color\0DstColor\0DstAlpha\0"
    "SrcAlphaSaturate\0ConstantColor\0"
    "ConstantAlpha\0OneMinusSrcColor\0"
    "OneMinusSrcAlpha\0OneMinusDstAlpha\0"
    "OneMinusDstColor\0OneMinusConstantColor\0"
    "OneMinusConstantAlpha\0OneMinusSrc1Alpha\0"
    "OneMinusSrc1Color0"
};
#undef QT_MOC_LITERAL

static const uint qt_meta_data_Qt3DRender__QBlendState[] = {

 // content:
       7,       // revision
       0,       // classname
       0,    0, // classinfo
       8,   14, // methods
       4,   78, // properties
       1,   94, // enums/sets
       0,    0, // constructors
       0,       // flags
       4,       // signalCount

 // signals: name, argc, parameters, tag, flags
       1,    1,   54,    2, 0x06 /* Public */,
       5,    1,   57,    2, 0x06 /* Public */,
       7,    1,   60,    2, 0x06 /* Public */,
       9,    1,   63,    2, 0x06 /* Public */,

 // slots: name, argc, parameters, tag, flags
      11,    1,   66,    2, 0x0a /* Public */,
      12,    1,   69,    2, 0x0a /* Public */,
      13,    1,   72,    2, 0x0a /* Public */,
      14,    1,   75,    2, 0x0a /* Public */,

 // signals: parameters
    QMetaType::Void, 0x80000000 | 3,    4,
    QMetaType::Void, 0x80000000 | 3,    6,
    QMetaType::Void, 0x80000000 | 3,    8,
    QMetaType::Void, 0x80000000 | 3,   10,

 // slots: parameters
    QMetaType::Void, 0x80000000 | 3,    4,
    QMetaType::Void, 0x80000000 | 3,    8,
    QMetaType::Void, 0x80000000 | 3,    6,
    QMetaType::Void, 0x80000000 | 3,   10,

 // properties: name, type, flags
       4, 0x80000000 | 3, 0x0049510b,
       6, 0x80000000 | 3, 0x0049510b,
       8, 0x80000000 | 3, 0x0049510b,
      10, 0x80000000 | 3, 0x0049510b,

 // properties: notify_signal_id
       0,
       1,
       2,
       3,

 // enums: name, flags, count, data
       3, 0x0,   19,   98,

 // enum data: key, value
      15, uint(Qt3DRender::QBlendState::Zero),
      16, uint(Qt3DRender::QBlendState::One),
      17, uint(Qt3DRender::QBlendState::SrcColor),
      18, uint(Qt3DRender::QBlendState::SrcAlpha),
      19, uint(Qt3DRender::QBlendState::Src1Alpha),
      20, uint(Qt3DRender::QBlendState::Src1Color),
      21, uint(Qt3DRender::QBlendState::DstColor),
      22, uint(Qt3DRender::QBlendState::DstAlpha),
      23, uint(Qt3DRender::QBlendState::SrcAlphaSaturate),
      24, uint(Qt3DRender::QBlendState::ConstantColor),
      25, uint(Qt3DRender::QBlendState::ConstantAlpha),
      26, uint(Qt3DRender::QBlendState::OneMinusSrcColor),
      27, uint(Qt3DRender::QBlendState::OneMinusSrcAlpha),
      28, uint(Qt3DRender::QBlendState::OneMinusDstAlpha),
      29, uint(Qt3DRender::QBlendState::OneMinusDstColor),
      30, uint(Qt3DRender::QBlendState::OneMinusConstantColor),
      31, uint(Qt3DRender::QBlendState::OneMinusConstantAlpha),
      32, uint(Qt3DRender::QBlendState::OneMinusSrc1Alpha),
      33, uint(Qt3DRender::QBlendState::OneMinusSrc1Color0),

       0        // eod
};

void Qt3DRender::QBlendState::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        QBlendState *_t = static_cast<QBlendState *>(_o);
        Q_UNUSED(_t)
        switch (_id) {
        case 0: _t->srcRGBChanged((*reinterpret_cast< Blending(*)>(_a[1]))); break;
        case 1: _t->srcAlphaChanged((*reinterpret_cast< Blending(*)>(_a[1]))); break;
        case 2: _t->dstRGBChanged((*reinterpret_cast< Blending(*)>(_a[1]))); break;
        case 3: _t->dstAlphaChanged((*reinterpret_cast< Blending(*)>(_a[1]))); break;
        case 4: _t->setSrcRGB((*reinterpret_cast< Blending(*)>(_a[1]))); break;
        case 5: _t->setDstRGB((*reinterpret_cast< Blending(*)>(_a[1]))); break;
        case 6: _t->setSrcAlpha((*reinterpret_cast< Blending(*)>(_a[1]))); break;
        case 7: _t->setDstAlpha((*reinterpret_cast< Blending(*)>(_a[1]))); break;
        default: ;
        }
    } else if (_c == QMetaObject::IndexOfMethod) {
        int *result = reinterpret_cast<int *>(_a[0]);
        void **func = reinterpret_cast<void **>(_a[1]);
        {
            typedef void (QBlendState::*_t)(Blending );
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&QBlendState::srcRGBChanged)) {
                *result = 0;
                return;
            }
        }
        {
            typedef void (QBlendState::*_t)(Blending );
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&QBlendState::srcAlphaChanged)) {
                *result = 1;
                return;
            }
        }
        {
            typedef void (QBlendState::*_t)(Blending );
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&QBlendState::dstRGBChanged)) {
                *result = 2;
                return;
            }
        }
        {
            typedef void (QBlendState::*_t)(Blending );
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&QBlendState::dstAlphaChanged)) {
                *result = 3;
                return;
            }
        }
    }
#ifndef QT_NO_PROPERTIES
    else if (_c == QMetaObject::ReadProperty) {
        QBlendState *_t = static_cast<QBlendState *>(_o);
        Q_UNUSED(_t)
        void *_v = _a[0];
        switch (_id) {
        case 0: *reinterpret_cast< Blending*>(_v) = _t->srcRGB(); break;
        case 1: *reinterpret_cast< Blending*>(_v) = _t->srcAlpha(); break;
        case 2: *reinterpret_cast< Blending*>(_v) = _t->dstRGB(); break;
        case 3: *reinterpret_cast< Blending*>(_v) = _t->dstAlpha(); break;
        default: break;
        }
    } else if (_c == QMetaObject::WriteProperty) {
        QBlendState *_t = static_cast<QBlendState *>(_o);
        Q_UNUSED(_t)
        void *_v = _a[0];
        switch (_id) {
        case 0: _t->setSrcRGB(*reinterpret_cast< Blending*>(_v)); break;
        case 1: _t->setSrcAlpha(*reinterpret_cast< Blending*>(_v)); break;
        case 2: _t->setDstRGB(*reinterpret_cast< Blending*>(_v)); break;
        case 3: _t->setDstAlpha(*reinterpret_cast< Blending*>(_v)); break;
        default: break;
        }
    } else if (_c == QMetaObject::ResetProperty) {
    }
#endif // QT_NO_PROPERTIES
}

const QMetaObject Qt3DRender::QBlendState::staticMetaObject = {
    { &QRenderState::staticMetaObject, qt_meta_stringdata_Qt3DRender__QBlendState.data,
      qt_meta_data_Qt3DRender__QBlendState,  qt_static_metacall, Q_NULLPTR, Q_NULLPTR}
};


const QMetaObject *Qt3DRender::QBlendState::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->dynamicMetaObject() : &staticMetaObject;
}

void *Qt3DRender::QBlendState::qt_metacast(const char *_clname)
{
    if (!_clname) return Q_NULLPTR;
    if (!strcmp(_clname, qt_meta_stringdata_Qt3DRender__QBlendState.stringdata0))
        return static_cast<void*>(const_cast< QBlendState*>(this));
    return QRenderState::qt_metacast(_clname);
}

int Qt3DRender::QBlendState::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QRenderState::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 8)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 8;
    } else if (_c == QMetaObject::RegisterMethodArgumentMetaType) {
        if (_id < 8)
            *reinterpret_cast<int*>(_a[0]) = -1;
        _id -= 8;
    }
#ifndef QT_NO_PROPERTIES
   else if (_c == QMetaObject::ReadProperty || _c == QMetaObject::WriteProperty
            || _c == QMetaObject::ResetProperty || _c == QMetaObject::RegisterPropertyMetaType) {
        qt_static_metacall(this, _c, _id, _a);
        _id -= 4;
    } else if (_c == QMetaObject::QueryPropertyDesignable) {
        _id -= 4;
    } else if (_c == QMetaObject::QueryPropertyScriptable) {
        _id -= 4;
    } else if (_c == QMetaObject::QueryPropertyStored) {
        _id -= 4;
    } else if (_c == QMetaObject::QueryPropertyEditable) {
        _id -= 4;
    } else if (_c == QMetaObject::QueryPropertyUser) {
        _id -= 4;
    }
#endif // QT_NO_PROPERTIES
    return _id;
}

// SIGNAL 0
void Qt3DRender::QBlendState::srcRGBChanged(Blending _t1)
{
    void *_a[] = { Q_NULLPTR, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 0, _a);
}

// SIGNAL 1
void Qt3DRender::QBlendState::srcAlphaChanged(Blending _t1)
{
    void *_a[] = { Q_NULLPTR, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 1, _a);
}

// SIGNAL 2
void Qt3DRender::QBlendState::dstRGBChanged(Blending _t1)
{
    void *_a[] = { Q_NULLPTR, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 2, _a);
}

// SIGNAL 3
void Qt3DRender::QBlendState::dstAlphaChanged(Blending _t1)
{
    void *_a[] = { Q_NULLPTR, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 3, _a);
}
struct qt_meta_stringdata_Qt3DRender__QBlendStateSeparate_t {
    QByteArrayData data[1];
    char stringdata0[32];
};
#define QT_MOC_LITERAL(idx, ofs, len) \
    Q_STATIC_BYTE_ARRAY_DATA_HEADER_INITIALIZER_WITH_OFFSET(len, \
    qptrdiff(offsetof(qt_meta_stringdata_Qt3DRender__QBlendStateSeparate_t, stringdata0) + ofs \
        - idx * sizeof(QByteArrayData)) \
    )
static const qt_meta_stringdata_Qt3DRender__QBlendStateSeparate_t qt_meta_stringdata_Qt3DRender__QBlendStateSeparate = {
    {
QT_MOC_LITERAL(0, 0, 31) // "Qt3DRender::QBlendStateSeparate"

    },
    "Qt3DRender::QBlendStateSeparate"
};
#undef QT_MOC_LITERAL

static const uint qt_meta_data_Qt3DRender__QBlendStateSeparate[] = {

 // content:
       7,       // revision
       0,       // classname
       0,    0, // classinfo
       0,    0, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       0,       // signalCount

       0        // eod
};

void Qt3DRender::QBlendStateSeparate::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    Q_UNUSED(_o);
    Q_UNUSED(_id);
    Q_UNUSED(_c);
    Q_UNUSED(_a);
}

const QMetaObject Qt3DRender::QBlendStateSeparate::staticMetaObject = {
    { &QBlendState::staticMetaObject, qt_meta_stringdata_Qt3DRender__QBlendStateSeparate.data,
      qt_meta_data_Qt3DRender__QBlendStateSeparate,  qt_static_metacall, Q_NULLPTR, Q_NULLPTR}
};


const QMetaObject *Qt3DRender::QBlendStateSeparate::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->dynamicMetaObject() : &staticMetaObject;
}

void *Qt3DRender::QBlendStateSeparate::qt_metacast(const char *_clname)
{
    if (!_clname) return Q_NULLPTR;
    if (!strcmp(_clname, qt_meta_stringdata_Qt3DRender__QBlendStateSeparate.stringdata0))
        return static_cast<void*>(const_cast< QBlendStateSeparate*>(this));
    return QBlendState::qt_metacast(_clname);
}

int Qt3DRender::QBlendStateSeparate::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QBlendState::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    return _id;
}
QT_END_MOC_NAMESPACE
