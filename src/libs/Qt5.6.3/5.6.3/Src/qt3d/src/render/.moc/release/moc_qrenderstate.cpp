/****************************************************************************
** Meta object code from reading C++ file 'qrenderstate.h'
**
** Created by: The Qt Meta Object Compiler version 67 (Qt 5.6.3)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "../../renderstates/qrenderstate.h"
#include <QtCore/qbytearray.h>
#include <QtCore/qmetatype.h>
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'qrenderstate.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 67
#error "This file was generated using the moc from 5.6.3. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
struct qt_meta_stringdata_Qt3DRender__QRenderState_t {
    QByteArrayData data[20];
    char stringdata0[237];
};
#define QT_MOC_LITERAL(idx, ofs, len) \
    Q_STATIC_BYTE_ARRAY_DATA_HEADER_INITIALIZER_WITH_OFFSET(len, \
    qptrdiff(offsetof(qt_meta_stringdata_Qt3DRender__QRenderState_t, stringdata0) + ofs \
        - idx * sizeof(QByteArrayData)) \
    )
static const qt_meta_stringdata_Qt3DRender__QRenderState_t qt_meta_stringdata_Qt3DRender__QRenderState = {
    {
QT_MOC_LITERAL(0, 0, 24), // "Qt3DRender::QRenderState"
QT_MOC_LITERAL(1, 25, 4), // "Type"
QT_MOC_LITERAL(2, 30, 13), // "AlphaCoverage"
QT_MOC_LITERAL(3, 44, 9), // "AlphaTest"
QT_MOC_LITERAL(4, 54, 13), // "BlendEquation"
QT_MOC_LITERAL(5, 68, 10), // "BlendState"
QT_MOC_LITERAL(6, 79, 18), // "BlendStateSeparate"
QT_MOC_LITERAL(7, 98, 9), // "ColorMask"
QT_MOC_LITERAL(8, 108, 8), // "CullFace"
QT_MOC_LITERAL(9, 117, 9), // "DepthMask"
QT_MOC_LITERAL(10, 127, 9), // "DepthTest"
QT_MOC_LITERAL(11, 137, 9), // "Dithering"
QT_MOC_LITERAL(12, 147, 9), // "FrontFace"
QT_MOC_LITERAL(13, 157, 9), // "PointSize"
QT_MOC_LITERAL(14, 167, 13), // "PolygonOffset"
QT_MOC_LITERAL(15, 181, 11), // "ScissorTest"
QT_MOC_LITERAL(16, 193, 11), // "StencilTest"
QT_MOC_LITERAL(17, 205, 11), // "StencilMask"
QT_MOC_LITERAL(18, 217, 9), // "StencilOp"
QT_MOC_LITERAL(19, 227, 9) // "ClipPlane"

    },
    "Qt3DRender::QRenderState\0Type\0"
    "AlphaCoverage\0AlphaTest\0BlendEquation\0"
    "BlendState\0BlendStateSeparate\0ColorMask\0"
    "CullFace\0DepthMask\0DepthTest\0Dithering\0"
    "FrontFace\0PointSize\0PolygonOffset\0"
    "ScissorTest\0StencilTest\0StencilMask\0"
    "StencilOp\0ClipPlane"
};
#undef QT_MOC_LITERAL

static const uint qt_meta_data_Qt3DRender__QRenderState[] = {

 // content:
       7,       // revision
       0,       // classname
       0,    0, // classinfo
       0,    0, // methods
       0,    0, // properties
       1,   14, // enums/sets
       0,    0, // constructors
       0,       // flags
       0,       // signalCount

 // enums: name, flags, count, data
       1, 0x0,   18,   18,

 // enum data: key, value
       2, uint(Qt3DRender::QRenderState::AlphaCoverage),
       3, uint(Qt3DRender::QRenderState::AlphaTest),
       4, uint(Qt3DRender::QRenderState::BlendEquation),
       5, uint(Qt3DRender::QRenderState::BlendState),
       6, uint(Qt3DRender::QRenderState::BlendStateSeparate),
       7, uint(Qt3DRender::QRenderState::ColorMask),
       8, uint(Qt3DRender::QRenderState::CullFace),
       9, uint(Qt3DRender::QRenderState::DepthMask),
      10, uint(Qt3DRender::QRenderState::DepthTest),
      11, uint(Qt3DRender::QRenderState::Dithering),
      12, uint(Qt3DRender::QRenderState::FrontFace),
      13, uint(Qt3DRender::QRenderState::PointSize),
      14, uint(Qt3DRender::QRenderState::PolygonOffset),
      15, uint(Qt3DRender::QRenderState::ScissorTest),
      16, uint(Qt3DRender::QRenderState::StencilTest),
      17, uint(Qt3DRender::QRenderState::StencilMask),
      18, uint(Qt3DRender::QRenderState::StencilOp),
      19, uint(Qt3DRender::QRenderState::ClipPlane),

       0        // eod
};

void Qt3DRender::QRenderState::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    Q_UNUSED(_o);
    Q_UNUSED(_id);
    Q_UNUSED(_c);
    Q_UNUSED(_a);
}

const QMetaObject Qt3DRender::QRenderState::staticMetaObject = {
    { &Qt3DCore::QNode::staticMetaObject, qt_meta_stringdata_Qt3DRender__QRenderState.data,
      qt_meta_data_Qt3DRender__QRenderState,  qt_static_metacall, Q_NULLPTR, Q_NULLPTR}
};


const QMetaObject *Qt3DRender::QRenderState::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->dynamicMetaObject() : &staticMetaObject;
}

void *Qt3DRender::QRenderState::qt_metacast(const char *_clname)
{
    if (!_clname) return Q_NULLPTR;
    if (!strcmp(_clname, qt_meta_stringdata_Qt3DRender__QRenderState.stringdata0))
        return static_cast<void*>(const_cast< QRenderState*>(this));
    return Qt3DCore::QNode::qt_metacast(_clname);
}

int Qt3DRender::QRenderState::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = Qt3DCore::QNode::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    return _id;
}
QT_END_MOC_NAMESPACE
