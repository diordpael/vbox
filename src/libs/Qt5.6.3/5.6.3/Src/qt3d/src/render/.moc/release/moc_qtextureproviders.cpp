/****************************************************************************
** Meta object code from reading C++ file 'qtextureproviders.h'
**
** Created by: The Qt Meta Object Compiler version 67 (Qt 5.6.3)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "../../texture/qtextureproviders.h"
#include <QtCore/qbytearray.h>
#include <QtCore/qmetatype.h>
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'qtextureproviders.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 67
#error "This file was generated using the moc from 5.6.3. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
struct qt_meta_stringdata_Qt3DRender__QTexture1D_t {
    QByteArrayData data[1];
    char stringdata0[23];
};
#define QT_MOC_LITERAL(idx, ofs, len) \
    Q_STATIC_BYTE_ARRAY_DATA_HEADER_INITIALIZER_WITH_OFFSET(len, \
    qptrdiff(offsetof(qt_meta_stringdata_Qt3DRender__QTexture1D_t, stringdata0) + ofs \
        - idx * sizeof(QByteArrayData)) \
    )
static const qt_meta_stringdata_Qt3DRender__QTexture1D_t qt_meta_stringdata_Qt3DRender__QTexture1D = {
    {
QT_MOC_LITERAL(0, 0, 22) // "Qt3DRender::QTexture1D"

    },
    "Qt3DRender::QTexture1D"
};
#undef QT_MOC_LITERAL

static const uint qt_meta_data_Qt3DRender__QTexture1D[] = {

 // content:
       7,       // revision
       0,       // classname
       0,    0, // classinfo
       0,    0, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       0,       // signalCount

       0        // eod
};

void Qt3DRender::QTexture1D::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    Q_UNUSED(_o);
    Q_UNUSED(_id);
    Q_UNUSED(_c);
    Q_UNUSED(_a);
}

const QMetaObject Qt3DRender::QTexture1D::staticMetaObject = {
    { &QAbstractTextureProvider::staticMetaObject, qt_meta_stringdata_Qt3DRender__QTexture1D.data,
      qt_meta_data_Qt3DRender__QTexture1D,  qt_static_metacall, Q_NULLPTR, Q_NULLPTR}
};


const QMetaObject *Qt3DRender::QTexture1D::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->dynamicMetaObject() : &staticMetaObject;
}

void *Qt3DRender::QTexture1D::qt_metacast(const char *_clname)
{
    if (!_clname) return Q_NULLPTR;
    if (!strcmp(_clname, qt_meta_stringdata_Qt3DRender__QTexture1D.stringdata0))
        return static_cast<void*>(const_cast< QTexture1D*>(this));
    return QAbstractTextureProvider::qt_metacast(_clname);
}

int Qt3DRender::QTexture1D::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QAbstractTextureProvider::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    return _id;
}
struct qt_meta_stringdata_Qt3DRender__QTexture1DArray_t {
    QByteArrayData data[1];
    char stringdata0[28];
};
#define QT_MOC_LITERAL(idx, ofs, len) \
    Q_STATIC_BYTE_ARRAY_DATA_HEADER_INITIALIZER_WITH_OFFSET(len, \
    qptrdiff(offsetof(qt_meta_stringdata_Qt3DRender__QTexture1DArray_t, stringdata0) + ofs \
        - idx * sizeof(QByteArrayData)) \
    )
static const qt_meta_stringdata_Qt3DRender__QTexture1DArray_t qt_meta_stringdata_Qt3DRender__QTexture1DArray = {
    {
QT_MOC_LITERAL(0, 0, 27) // "Qt3DRender::QTexture1DArray"

    },
    "Qt3DRender::QTexture1DArray"
};
#undef QT_MOC_LITERAL

static const uint qt_meta_data_Qt3DRender__QTexture1DArray[] = {

 // content:
       7,       // revision
       0,       // classname
       0,    0, // classinfo
       0,    0, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       0,       // signalCount

       0        // eod
};

void Qt3DRender::QTexture1DArray::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    Q_UNUSED(_o);
    Q_UNUSED(_id);
    Q_UNUSED(_c);
    Q_UNUSED(_a);
}

const QMetaObject Qt3DRender::QTexture1DArray::staticMetaObject = {
    { &QAbstractTextureProvider::staticMetaObject, qt_meta_stringdata_Qt3DRender__QTexture1DArray.data,
      qt_meta_data_Qt3DRender__QTexture1DArray,  qt_static_metacall, Q_NULLPTR, Q_NULLPTR}
};


const QMetaObject *Qt3DRender::QTexture1DArray::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->dynamicMetaObject() : &staticMetaObject;
}

void *Qt3DRender::QTexture1DArray::qt_metacast(const char *_clname)
{
    if (!_clname) return Q_NULLPTR;
    if (!strcmp(_clname, qt_meta_stringdata_Qt3DRender__QTexture1DArray.stringdata0))
        return static_cast<void*>(const_cast< QTexture1DArray*>(this));
    return QAbstractTextureProvider::qt_metacast(_clname);
}

int Qt3DRender::QTexture1DArray::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QAbstractTextureProvider::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    return _id;
}
struct qt_meta_stringdata_Qt3DRender__QTexture2D_t {
    QByteArrayData data[1];
    char stringdata0[23];
};
#define QT_MOC_LITERAL(idx, ofs, len) \
    Q_STATIC_BYTE_ARRAY_DATA_HEADER_INITIALIZER_WITH_OFFSET(len, \
    qptrdiff(offsetof(qt_meta_stringdata_Qt3DRender__QTexture2D_t, stringdata0) + ofs \
        - idx * sizeof(QByteArrayData)) \
    )
static const qt_meta_stringdata_Qt3DRender__QTexture2D_t qt_meta_stringdata_Qt3DRender__QTexture2D = {
    {
QT_MOC_LITERAL(0, 0, 22) // "Qt3DRender::QTexture2D"

    },
    "Qt3DRender::QTexture2D"
};
#undef QT_MOC_LITERAL

static const uint qt_meta_data_Qt3DRender__QTexture2D[] = {

 // content:
       7,       // revision
       0,       // classname
       0,    0, // classinfo
       0,    0, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       0,       // signalCount

       0        // eod
};

void Qt3DRender::QTexture2D::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    Q_UNUSED(_o);
    Q_UNUSED(_id);
    Q_UNUSED(_c);
    Q_UNUSED(_a);
}

const QMetaObject Qt3DRender::QTexture2D::staticMetaObject = {
    { &QAbstractTextureProvider::staticMetaObject, qt_meta_stringdata_Qt3DRender__QTexture2D.data,
      qt_meta_data_Qt3DRender__QTexture2D,  qt_static_metacall, Q_NULLPTR, Q_NULLPTR}
};


const QMetaObject *Qt3DRender::QTexture2D::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->dynamicMetaObject() : &staticMetaObject;
}

void *Qt3DRender::QTexture2D::qt_metacast(const char *_clname)
{
    if (!_clname) return Q_NULLPTR;
    if (!strcmp(_clname, qt_meta_stringdata_Qt3DRender__QTexture2D.stringdata0))
        return static_cast<void*>(const_cast< QTexture2D*>(this));
    return QAbstractTextureProvider::qt_metacast(_clname);
}

int Qt3DRender::QTexture2D::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QAbstractTextureProvider::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    return _id;
}
struct qt_meta_stringdata_Qt3DRender__QTexture2DArray_t {
    QByteArrayData data[1];
    char stringdata0[28];
};
#define QT_MOC_LITERAL(idx, ofs, len) \
    Q_STATIC_BYTE_ARRAY_DATA_HEADER_INITIALIZER_WITH_OFFSET(len, \
    qptrdiff(offsetof(qt_meta_stringdata_Qt3DRender__QTexture2DArray_t, stringdata0) + ofs \
        - idx * sizeof(QByteArrayData)) \
    )
static const qt_meta_stringdata_Qt3DRender__QTexture2DArray_t qt_meta_stringdata_Qt3DRender__QTexture2DArray = {
    {
QT_MOC_LITERAL(0, 0, 27) // "Qt3DRender::QTexture2DArray"

    },
    "Qt3DRender::QTexture2DArray"
};
#undef QT_MOC_LITERAL

static const uint qt_meta_data_Qt3DRender__QTexture2DArray[] = {

 // content:
       7,       // revision
       0,       // classname
       0,    0, // classinfo
       0,    0, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       0,       // signalCount

       0        // eod
};

void Qt3DRender::QTexture2DArray::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    Q_UNUSED(_o);
    Q_UNUSED(_id);
    Q_UNUSED(_c);
    Q_UNUSED(_a);
}

const QMetaObject Qt3DRender::QTexture2DArray::staticMetaObject = {
    { &QAbstractTextureProvider::staticMetaObject, qt_meta_stringdata_Qt3DRender__QTexture2DArray.data,
      qt_meta_data_Qt3DRender__QTexture2DArray,  qt_static_metacall, Q_NULLPTR, Q_NULLPTR}
};


const QMetaObject *Qt3DRender::QTexture2DArray::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->dynamicMetaObject() : &staticMetaObject;
}

void *Qt3DRender::QTexture2DArray::qt_metacast(const char *_clname)
{
    if (!_clname) return Q_NULLPTR;
    if (!strcmp(_clname, qt_meta_stringdata_Qt3DRender__QTexture2DArray.stringdata0))
        return static_cast<void*>(const_cast< QTexture2DArray*>(this));
    return QAbstractTextureProvider::qt_metacast(_clname);
}

int Qt3DRender::QTexture2DArray::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QAbstractTextureProvider::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    return _id;
}
struct qt_meta_stringdata_Qt3DRender__QTexture3D_t {
    QByteArrayData data[1];
    char stringdata0[23];
};
#define QT_MOC_LITERAL(idx, ofs, len) \
    Q_STATIC_BYTE_ARRAY_DATA_HEADER_INITIALIZER_WITH_OFFSET(len, \
    qptrdiff(offsetof(qt_meta_stringdata_Qt3DRender__QTexture3D_t, stringdata0) + ofs \
        - idx * sizeof(QByteArrayData)) \
    )
static const qt_meta_stringdata_Qt3DRender__QTexture3D_t qt_meta_stringdata_Qt3DRender__QTexture3D = {
    {
QT_MOC_LITERAL(0, 0, 22) // "Qt3DRender::QTexture3D"

    },
    "Qt3DRender::QTexture3D"
};
#undef QT_MOC_LITERAL

static const uint qt_meta_data_Qt3DRender__QTexture3D[] = {

 // content:
       7,       // revision
       0,       // classname
       0,    0, // classinfo
       0,    0, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       0,       // signalCount

       0        // eod
};

void Qt3DRender::QTexture3D::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    Q_UNUSED(_o);
    Q_UNUSED(_id);
    Q_UNUSED(_c);
    Q_UNUSED(_a);
}

const QMetaObject Qt3DRender::QTexture3D::staticMetaObject = {
    { &QAbstractTextureProvider::staticMetaObject, qt_meta_stringdata_Qt3DRender__QTexture3D.data,
      qt_meta_data_Qt3DRender__QTexture3D,  qt_static_metacall, Q_NULLPTR, Q_NULLPTR}
};


const QMetaObject *Qt3DRender::QTexture3D::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->dynamicMetaObject() : &staticMetaObject;
}

void *Qt3DRender::QTexture3D::qt_metacast(const char *_clname)
{
    if (!_clname) return Q_NULLPTR;
    if (!strcmp(_clname, qt_meta_stringdata_Qt3DRender__QTexture3D.stringdata0))
        return static_cast<void*>(const_cast< QTexture3D*>(this));
    return QAbstractTextureProvider::qt_metacast(_clname);
}

int Qt3DRender::QTexture3D::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QAbstractTextureProvider::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    return _id;
}
struct qt_meta_stringdata_Qt3DRender__QTextureCubeMap_t {
    QByteArrayData data[1];
    char stringdata0[28];
};
#define QT_MOC_LITERAL(idx, ofs, len) \
    Q_STATIC_BYTE_ARRAY_DATA_HEADER_INITIALIZER_WITH_OFFSET(len, \
    qptrdiff(offsetof(qt_meta_stringdata_Qt3DRender__QTextureCubeMap_t, stringdata0) + ofs \
        - idx * sizeof(QByteArrayData)) \
    )
static const qt_meta_stringdata_Qt3DRender__QTextureCubeMap_t qt_meta_stringdata_Qt3DRender__QTextureCubeMap = {
    {
QT_MOC_LITERAL(0, 0, 27) // "Qt3DRender::QTextureCubeMap"

    },
    "Qt3DRender::QTextureCubeMap"
};
#undef QT_MOC_LITERAL

static const uint qt_meta_data_Qt3DRender__QTextureCubeMap[] = {

 // content:
       7,       // revision
       0,       // classname
       0,    0, // classinfo
       0,    0, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       0,       // signalCount

       0        // eod
};

void Qt3DRender::QTextureCubeMap::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    Q_UNUSED(_o);
    Q_UNUSED(_id);
    Q_UNUSED(_c);
    Q_UNUSED(_a);
}

const QMetaObject Qt3DRender::QTextureCubeMap::staticMetaObject = {
    { &QAbstractTextureProvider::staticMetaObject, qt_meta_stringdata_Qt3DRender__QTextureCubeMap.data,
      qt_meta_data_Qt3DRender__QTextureCubeMap,  qt_static_metacall, Q_NULLPTR, Q_NULLPTR}
};


const QMetaObject *Qt3DRender::QTextureCubeMap::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->dynamicMetaObject() : &staticMetaObject;
}

void *Qt3DRender::QTextureCubeMap::qt_metacast(const char *_clname)
{
    if (!_clname) return Q_NULLPTR;
    if (!strcmp(_clname, qt_meta_stringdata_Qt3DRender__QTextureCubeMap.stringdata0))
        return static_cast<void*>(const_cast< QTextureCubeMap*>(this));
    return QAbstractTextureProvider::qt_metacast(_clname);
}

int Qt3DRender::QTextureCubeMap::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QAbstractTextureProvider::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    return _id;
}
struct qt_meta_stringdata_Qt3DRender__QTextureCubeMapArray_t {
    QByteArrayData data[1];
    char stringdata0[33];
};
#define QT_MOC_LITERAL(idx, ofs, len) \
    Q_STATIC_BYTE_ARRAY_DATA_HEADER_INITIALIZER_WITH_OFFSET(len, \
    qptrdiff(offsetof(qt_meta_stringdata_Qt3DRender__QTextureCubeMapArray_t, stringdata0) + ofs \
        - idx * sizeof(QByteArrayData)) \
    )
static const qt_meta_stringdata_Qt3DRender__QTextureCubeMapArray_t qt_meta_stringdata_Qt3DRender__QTextureCubeMapArray = {
    {
QT_MOC_LITERAL(0, 0, 32) // "Qt3DRender::QTextureCubeMapArray"

    },
    "Qt3DRender::QTextureCubeMapArray"
};
#undef QT_MOC_LITERAL

static const uint qt_meta_data_Qt3DRender__QTextureCubeMapArray[] = {

 // content:
       7,       // revision
       0,       // classname
       0,    0, // classinfo
       0,    0, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       0,       // signalCount

       0        // eod
};

void Qt3DRender::QTextureCubeMapArray::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    Q_UNUSED(_o);
    Q_UNUSED(_id);
    Q_UNUSED(_c);
    Q_UNUSED(_a);
}

const QMetaObject Qt3DRender::QTextureCubeMapArray::staticMetaObject = {
    { &QAbstractTextureProvider::staticMetaObject, qt_meta_stringdata_Qt3DRender__QTextureCubeMapArray.data,
      qt_meta_data_Qt3DRender__QTextureCubeMapArray,  qt_static_metacall, Q_NULLPTR, Q_NULLPTR}
};


const QMetaObject *Qt3DRender::QTextureCubeMapArray::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->dynamicMetaObject() : &staticMetaObject;
}

void *Qt3DRender::QTextureCubeMapArray::qt_metacast(const char *_clname)
{
    if (!_clname) return Q_NULLPTR;
    if (!strcmp(_clname, qt_meta_stringdata_Qt3DRender__QTextureCubeMapArray.stringdata0))
        return static_cast<void*>(const_cast< QTextureCubeMapArray*>(this));
    return QAbstractTextureProvider::qt_metacast(_clname);
}

int Qt3DRender::QTextureCubeMapArray::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QAbstractTextureProvider::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    return _id;
}
struct qt_meta_stringdata_Qt3DRender__QTexture2DMultisample_t {
    QByteArrayData data[1];
    char stringdata0[34];
};
#define QT_MOC_LITERAL(idx, ofs, len) \
    Q_STATIC_BYTE_ARRAY_DATA_HEADER_INITIALIZER_WITH_OFFSET(len, \
    qptrdiff(offsetof(qt_meta_stringdata_Qt3DRender__QTexture2DMultisample_t, stringdata0) + ofs \
        - idx * sizeof(QByteArrayData)) \
    )
static const qt_meta_stringdata_Qt3DRender__QTexture2DMultisample_t qt_meta_stringdata_Qt3DRender__QTexture2DMultisample = {
    {
QT_MOC_LITERAL(0, 0, 33) // "Qt3DRender::QTexture2DMultisa..."

    },
    "Qt3DRender::QTexture2DMultisample"
};
#undef QT_MOC_LITERAL

static const uint qt_meta_data_Qt3DRender__QTexture2DMultisample[] = {

 // content:
       7,       // revision
       0,       // classname
       0,    0, // classinfo
       0,    0, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       0,       // signalCount

       0        // eod
};

void Qt3DRender::QTexture2DMultisample::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    Q_UNUSED(_o);
    Q_UNUSED(_id);
    Q_UNUSED(_c);
    Q_UNUSED(_a);
}

const QMetaObject Qt3DRender::QTexture2DMultisample::staticMetaObject = {
    { &QAbstractTextureProvider::staticMetaObject, qt_meta_stringdata_Qt3DRender__QTexture2DMultisample.data,
      qt_meta_data_Qt3DRender__QTexture2DMultisample,  qt_static_metacall, Q_NULLPTR, Q_NULLPTR}
};


const QMetaObject *Qt3DRender::QTexture2DMultisample::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->dynamicMetaObject() : &staticMetaObject;
}

void *Qt3DRender::QTexture2DMultisample::qt_metacast(const char *_clname)
{
    if (!_clname) return Q_NULLPTR;
    if (!strcmp(_clname, qt_meta_stringdata_Qt3DRender__QTexture2DMultisample.stringdata0))
        return static_cast<void*>(const_cast< QTexture2DMultisample*>(this));
    return QAbstractTextureProvider::qt_metacast(_clname);
}

int Qt3DRender::QTexture2DMultisample::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QAbstractTextureProvider::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    return _id;
}
struct qt_meta_stringdata_Qt3DRender__QTexture2DMultisampleArray_t {
    QByteArrayData data[1];
    char stringdata0[39];
};
#define QT_MOC_LITERAL(idx, ofs, len) \
    Q_STATIC_BYTE_ARRAY_DATA_HEADER_INITIALIZER_WITH_OFFSET(len, \
    qptrdiff(offsetof(qt_meta_stringdata_Qt3DRender__QTexture2DMultisampleArray_t, stringdata0) + ofs \
        - idx * sizeof(QByteArrayData)) \
    )
static const qt_meta_stringdata_Qt3DRender__QTexture2DMultisampleArray_t qt_meta_stringdata_Qt3DRender__QTexture2DMultisampleArray = {
    {
QT_MOC_LITERAL(0, 0, 38) // "Qt3DRender::QTexture2DMultisa..."

    },
    "Qt3DRender::QTexture2DMultisampleArray"
};
#undef QT_MOC_LITERAL

static const uint qt_meta_data_Qt3DRender__QTexture2DMultisampleArray[] = {

 // content:
       7,       // revision
       0,       // classname
       0,    0, // classinfo
       0,    0, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       0,       // signalCount

       0        // eod
};

void Qt3DRender::QTexture2DMultisampleArray::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    Q_UNUSED(_o);
    Q_UNUSED(_id);
    Q_UNUSED(_c);
    Q_UNUSED(_a);
}

const QMetaObject Qt3DRender::QTexture2DMultisampleArray::staticMetaObject = {
    { &QAbstractTextureProvider::staticMetaObject, qt_meta_stringdata_Qt3DRender__QTexture2DMultisampleArray.data,
      qt_meta_data_Qt3DRender__QTexture2DMultisampleArray,  qt_static_metacall, Q_NULLPTR, Q_NULLPTR}
};


const QMetaObject *Qt3DRender::QTexture2DMultisampleArray::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->dynamicMetaObject() : &staticMetaObject;
}

void *Qt3DRender::QTexture2DMultisampleArray::qt_metacast(const char *_clname)
{
    if (!_clname) return Q_NULLPTR;
    if (!strcmp(_clname, qt_meta_stringdata_Qt3DRender__QTexture2DMultisampleArray.stringdata0))
        return static_cast<void*>(const_cast< QTexture2DMultisampleArray*>(this));
    return QAbstractTextureProvider::qt_metacast(_clname);
}

int Qt3DRender::QTexture2DMultisampleArray::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QAbstractTextureProvider::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    return _id;
}
struct qt_meta_stringdata_Qt3DRender__QTextureRectangle_t {
    QByteArrayData data[1];
    char stringdata0[30];
};
#define QT_MOC_LITERAL(idx, ofs, len) \
    Q_STATIC_BYTE_ARRAY_DATA_HEADER_INITIALIZER_WITH_OFFSET(len, \
    qptrdiff(offsetof(qt_meta_stringdata_Qt3DRender__QTextureRectangle_t, stringdata0) + ofs \
        - idx * sizeof(QByteArrayData)) \
    )
static const qt_meta_stringdata_Qt3DRender__QTextureRectangle_t qt_meta_stringdata_Qt3DRender__QTextureRectangle = {
    {
QT_MOC_LITERAL(0, 0, 29) // "Qt3DRender::QTextureRectangle"

    },
    "Qt3DRender::QTextureRectangle"
};
#undef QT_MOC_LITERAL

static const uint qt_meta_data_Qt3DRender__QTextureRectangle[] = {

 // content:
       7,       // revision
       0,       // classname
       0,    0, // classinfo
       0,    0, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       0,       // signalCount

       0        // eod
};

void Qt3DRender::QTextureRectangle::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    Q_UNUSED(_o);
    Q_UNUSED(_id);
    Q_UNUSED(_c);
    Q_UNUSED(_a);
}

const QMetaObject Qt3DRender::QTextureRectangle::staticMetaObject = {
    { &QAbstractTextureProvider::staticMetaObject, qt_meta_stringdata_Qt3DRender__QTextureRectangle.data,
      qt_meta_data_Qt3DRender__QTextureRectangle,  qt_static_metacall, Q_NULLPTR, Q_NULLPTR}
};


const QMetaObject *Qt3DRender::QTextureRectangle::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->dynamicMetaObject() : &staticMetaObject;
}

void *Qt3DRender::QTextureRectangle::qt_metacast(const char *_clname)
{
    if (!_clname) return Q_NULLPTR;
    if (!strcmp(_clname, qt_meta_stringdata_Qt3DRender__QTextureRectangle.stringdata0))
        return static_cast<void*>(const_cast< QTextureRectangle*>(this));
    return QAbstractTextureProvider::qt_metacast(_clname);
}

int Qt3DRender::QTextureRectangle::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QAbstractTextureProvider::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    return _id;
}
struct qt_meta_stringdata_Qt3DRender__QTextureBuffer_t {
    QByteArrayData data[1];
    char stringdata0[27];
};
#define QT_MOC_LITERAL(idx, ofs, len) \
    Q_STATIC_BYTE_ARRAY_DATA_HEADER_INITIALIZER_WITH_OFFSET(len, \
    qptrdiff(offsetof(qt_meta_stringdata_Qt3DRender__QTextureBuffer_t, stringdata0) + ofs \
        - idx * sizeof(QByteArrayData)) \
    )
static const qt_meta_stringdata_Qt3DRender__QTextureBuffer_t qt_meta_stringdata_Qt3DRender__QTextureBuffer = {
    {
QT_MOC_LITERAL(0, 0, 26) // "Qt3DRender::QTextureBuffer"

    },
    "Qt3DRender::QTextureBuffer"
};
#undef QT_MOC_LITERAL

static const uint qt_meta_data_Qt3DRender__QTextureBuffer[] = {

 // content:
       7,       // revision
       0,       // classname
       0,    0, // classinfo
       0,    0, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       0,       // signalCount

       0        // eod
};

void Qt3DRender::QTextureBuffer::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    Q_UNUSED(_o);
    Q_UNUSED(_id);
    Q_UNUSED(_c);
    Q_UNUSED(_a);
}

const QMetaObject Qt3DRender::QTextureBuffer::staticMetaObject = {
    { &QAbstractTextureProvider::staticMetaObject, qt_meta_stringdata_Qt3DRender__QTextureBuffer.data,
      qt_meta_data_Qt3DRender__QTextureBuffer,  qt_static_metacall, Q_NULLPTR, Q_NULLPTR}
};


const QMetaObject *Qt3DRender::QTextureBuffer::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->dynamicMetaObject() : &staticMetaObject;
}

void *Qt3DRender::QTextureBuffer::qt_metacast(const char *_clname)
{
    if (!_clname) return Q_NULLPTR;
    if (!strcmp(_clname, qt_meta_stringdata_Qt3DRender__QTextureBuffer.stringdata0))
        return static_cast<void*>(const_cast< QTextureBuffer*>(this));
    return QAbstractTextureProvider::qt_metacast(_clname);
}

int Qt3DRender::QTextureBuffer::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QAbstractTextureProvider::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    return _id;
}
QT_END_MOC_NAMESPACE
