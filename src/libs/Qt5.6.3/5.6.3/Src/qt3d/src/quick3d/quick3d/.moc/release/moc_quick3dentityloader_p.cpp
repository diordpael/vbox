/****************************************************************************
** Meta object code from reading C++ file 'quick3dentityloader_p.h'
**
** Created by: The Qt Meta Object Compiler version 67 (Qt 5.6.3)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "../../items/quick3dentityloader_p.h"
#include <QtCore/qbytearray.h>
#include <QtCore/qmetatype.h>
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'quick3dentityloader_p.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 67
#error "This file was generated using the moc from 5.6.3. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
struct qt_meta_stringdata_Qt3DCore__Quick__Quick3DEntityLoader_t {
    QByteArrayData data[8];
    char stringdata0[128];
};
#define QT_MOC_LITERAL(idx, ofs, len) \
    Q_STATIC_BYTE_ARRAY_DATA_HEADER_INITIALIZER_WITH_OFFSET(len, \
    qptrdiff(offsetof(qt_meta_stringdata_Qt3DCore__Quick__Quick3DEntityLoader_t, stringdata0) + ofs \
        - idx * sizeof(QByteArrayData)) \
    )
static const qt_meta_stringdata_Qt3DCore__Quick__Quick3DEntityLoader_t qt_meta_stringdata_Qt3DCore__Quick__Quick3DEntityLoader = {
    {
QT_MOC_LITERAL(0, 0, 36), // "Qt3DCore::Quick::Quick3DEntit..."
QT_MOC_LITERAL(1, 37, 13), // "entityChanged"
QT_MOC_LITERAL(2, 51, 0), // ""
QT_MOC_LITERAL(3, 52, 13), // "sourceChanged"
QT_MOC_LITERAL(4, 66, 25), // "_q_componentStatusChanged"
QT_MOC_LITERAL(5, 92, 21), // "QQmlComponent::Status"
QT_MOC_LITERAL(6, 114, 6), // "entity"
QT_MOC_LITERAL(7, 121, 6) // "source"

    },
    "Qt3DCore::Quick::Quick3DEntityLoader\0"
    "entityChanged\0\0sourceChanged\0"
    "_q_componentStatusChanged\0"
    "QQmlComponent::Status\0entity\0source"
};
#undef QT_MOC_LITERAL

static const uint qt_meta_data_Qt3DCore__Quick__Quick3DEntityLoader[] = {

 // content:
       7,       // revision
       0,       // classname
       0,    0, // classinfo
       3,   14, // methods
       2,   34, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       2,       // signalCount

 // signals: name, argc, parameters, tag, flags
       1,    0,   29,    2, 0x06 /* Public */,
       3,    0,   30,    2, 0x06 /* Public */,

 // slots: name, argc, parameters, tag, flags
       4,    1,   31,    2, 0x08 /* Private */,

 // signals: parameters
    QMetaType::Void,
    QMetaType::Void,

 // slots: parameters
    QMetaType::Void, 0x80000000 | 5,    2,

 // properties: name, type, flags
       6, QMetaType::QObjectStar, 0x00495001,
       7, QMetaType::QUrl, 0x00495103,

 // properties: notify_signal_id
       0,
       1,

       0        // eod
};

void Qt3DCore::Quick::Quick3DEntityLoader::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        Quick3DEntityLoader *_t = static_cast<Quick3DEntityLoader *>(_o);
        Q_UNUSED(_t)
        switch (_id) {
        case 0: _t->entityChanged(); break;
        case 1: _t->sourceChanged(); break;
        case 2: _t->d_func()->_q_componentStatusChanged((*reinterpret_cast< QQmlComponent::Status(*)>(_a[1]))); break;
        default: ;
        }
    } else if (_c == QMetaObject::IndexOfMethod) {
        int *result = reinterpret_cast<int *>(_a[0]);
        void **func = reinterpret_cast<void **>(_a[1]);
        {
            typedef void (Quick3DEntityLoader::*_t)();
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&Quick3DEntityLoader::entityChanged)) {
                *result = 0;
                return;
            }
        }
        {
            typedef void (Quick3DEntityLoader::*_t)();
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&Quick3DEntityLoader::sourceChanged)) {
                *result = 1;
                return;
            }
        }
    }
#ifndef QT_NO_PROPERTIES
    else if (_c == QMetaObject::ReadProperty) {
        Quick3DEntityLoader *_t = static_cast<Quick3DEntityLoader *>(_o);
        Q_UNUSED(_t)
        void *_v = _a[0];
        switch (_id) {
        case 0: *reinterpret_cast< QObject**>(_v) = _t->entity(); break;
        case 1: *reinterpret_cast< QUrl*>(_v) = _t->source(); break;
        default: break;
        }
    } else if (_c == QMetaObject::WriteProperty) {
        Quick3DEntityLoader *_t = static_cast<Quick3DEntityLoader *>(_o);
        Q_UNUSED(_t)
        void *_v = _a[0];
        switch (_id) {
        case 1: _t->setSource(*reinterpret_cast< QUrl*>(_v)); break;
        default: break;
        }
    } else if (_c == QMetaObject::ResetProperty) {
    }
#endif // QT_NO_PROPERTIES
}

const QMetaObject Qt3DCore::Quick::Quick3DEntityLoader::staticMetaObject = {
    { &QEntity::staticMetaObject, qt_meta_stringdata_Qt3DCore__Quick__Quick3DEntityLoader.data,
      qt_meta_data_Qt3DCore__Quick__Quick3DEntityLoader,  qt_static_metacall, Q_NULLPTR, Q_NULLPTR}
};


const QMetaObject *Qt3DCore::Quick::Quick3DEntityLoader::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->dynamicMetaObject() : &staticMetaObject;
}

void *Qt3DCore::Quick::Quick3DEntityLoader::qt_metacast(const char *_clname)
{
    if (!_clname) return Q_NULLPTR;
    if (!strcmp(_clname, qt_meta_stringdata_Qt3DCore__Quick__Quick3DEntityLoader.stringdata0))
        return static_cast<void*>(const_cast< Quick3DEntityLoader*>(this));
    return QEntity::qt_metacast(_clname);
}

int Qt3DCore::Quick::Quick3DEntityLoader::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QEntity::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 3)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 3;
    } else if (_c == QMetaObject::RegisterMethodArgumentMetaType) {
        if (_id < 3)
            *reinterpret_cast<int*>(_a[0]) = -1;
        _id -= 3;
    }
#ifndef QT_NO_PROPERTIES
   else if (_c == QMetaObject::ReadProperty || _c == QMetaObject::WriteProperty
            || _c == QMetaObject::ResetProperty || _c == QMetaObject::RegisterPropertyMetaType) {
        qt_static_metacall(this, _c, _id, _a);
        _id -= 2;
    } else if (_c == QMetaObject::QueryPropertyDesignable) {
        _id -= 2;
    } else if (_c == QMetaObject::QueryPropertyScriptable) {
        _id -= 2;
    } else if (_c == QMetaObject::QueryPropertyStored) {
        _id -= 2;
    } else if (_c == QMetaObject::QueryPropertyEditable) {
        _id -= 2;
    } else if (_c == QMetaObject::QueryPropertyUser) {
        _id -= 2;
    }
#endif // QT_NO_PROPERTIES
    return _id;
}

// SIGNAL 0
void Qt3DCore::Quick::Quick3DEntityLoader::entityChanged()
{
    QMetaObject::activate(this, &staticMetaObject, 0, Q_NULLPTR);
}

// SIGNAL 1
void Qt3DCore::Quick::Quick3DEntityLoader::sourceChanged()
{
    QMetaObject::activate(this, &staticMetaObject, 1, Q_NULLPTR);
}
QT_END_MOC_NAMESPACE
