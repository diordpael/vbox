/****************************************************************************
** Meta object code from reading C++ file 'quick3drenderpass_p.h'
**
** Created by: The Qt Meta Object Compiler version 67 (Qt 5.6.3)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "../../items/quick3drenderpass_p.h"
#include <QtCore/qbytearray.h>
#include <QtCore/qmetatype.h>
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'quick3drenderpass_p.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 67
#error "This file was generated using the moc from 5.6.3. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
struct qt_meta_stringdata_Qt3DRender__Render__Quick__Quick3DRenderPass_t {
    QByteArrayData data[9];
    char stringdata0[264];
};
#define QT_MOC_LITERAL(idx, ofs, len) \
    Q_STATIC_BYTE_ARRAY_DATA_HEADER_INITIALIZER_WITH_OFFSET(len, \
    qptrdiff(offsetof(qt_meta_stringdata_Qt3DRender__Render__Quick__Quick3DRenderPass_t, stringdata0) + ofs \
        - idx * sizeof(QByteArrayData)) \
    )
static const qt_meta_stringdata_Qt3DRender__Render__Quick__Quick3DRenderPass_t qt_meta_stringdata_Qt3DRender__Render__Quick__Quick3DRenderPass = {
    {
QT_MOC_LITERAL(0, 0, 44), // "Qt3DRender::Render::Quick::Qu..."
QT_MOC_LITERAL(1, 45, 11), // "annotations"
QT_MOC_LITERAL(2, 57, 41), // "QQmlListProperty<Qt3DRender::..."
QT_MOC_LITERAL(3, 99, 8), // "bindings"
QT_MOC_LITERAL(4, 108, 47), // "QQmlListProperty<Qt3DRender::..."
QT_MOC_LITERAL(5, 156, 12), // "renderStates"
QT_MOC_LITERAL(6, 169, 42), // "QQmlListProperty<Qt3DRender::..."
QT_MOC_LITERAL(7, 212, 10), // "parameters"
QT_MOC_LITERAL(8, 223, 40) // "QQmlListProperty<Qt3DRender::..."

    },
    "Qt3DRender::Render::Quick::Quick3DRenderPass\0"
    "annotations\0QQmlListProperty<Qt3DRender::QAnnotation>\0"
    "bindings\0QQmlListProperty<Qt3DRender::QParameterMapping>\0"
    "renderStates\0QQmlListProperty<Qt3DRender::QRenderState>\0"
    "parameters\0QQmlListProperty<Qt3DRender::QParameter>"
};
#undef QT_MOC_LITERAL

static const uint qt_meta_data_Qt3DRender__Render__Quick__Quick3DRenderPass[] = {

 // content:
       7,       // revision
       0,       // classname
       0,    0, // classinfo
       0,    0, // methods
       4,   14, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       0,       // signalCount

 // properties: name, type, flags
       1, 0x80000000 | 2, 0x00095009,
       3, 0x80000000 | 4, 0x00095009,
       5, 0x80000000 | 6, 0x00095009,
       7, 0x80000000 | 8, 0x00095009,

       0        // eod
};

void Qt3DRender::Render::Quick::Quick3DRenderPass::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{

#ifndef QT_NO_PROPERTIES
    if (_c == QMetaObject::ReadProperty) {
        Quick3DRenderPass *_t = static_cast<Quick3DRenderPass *>(_o);
        Q_UNUSED(_t)
        void *_v = _a[0];
        switch (_id) {
        case 0: *reinterpret_cast< QQmlListProperty<Qt3DRender::QAnnotation>*>(_v) = _t->annotationList(); break;
        case 1: *reinterpret_cast< QQmlListProperty<Qt3DRender::QParameterMapping>*>(_v) = _t->bindingList(); break;
        case 2: *reinterpret_cast< QQmlListProperty<Qt3DRender::QRenderState>*>(_v) = _t->renderStateList(); break;
        case 3: *reinterpret_cast< QQmlListProperty<Qt3DRender::QParameter>*>(_v) = _t->parameterList(); break;
        default: break;
        }
    } else if (_c == QMetaObject::WriteProperty) {
    } else if (_c == QMetaObject::ResetProperty) {
    }
#endif // QT_NO_PROPERTIES
    Q_UNUSED(_o);
    Q_UNUSED(_id);
    Q_UNUSED(_c);
    Q_UNUSED(_a);
}

const QMetaObject Qt3DRender::Render::Quick::Quick3DRenderPass::staticMetaObject = {
    { &QObject::staticMetaObject, qt_meta_stringdata_Qt3DRender__Render__Quick__Quick3DRenderPass.data,
      qt_meta_data_Qt3DRender__Render__Quick__Quick3DRenderPass,  qt_static_metacall, Q_NULLPTR, Q_NULLPTR}
};


const QMetaObject *Qt3DRender::Render::Quick::Quick3DRenderPass::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->dynamicMetaObject() : &staticMetaObject;
}

void *Qt3DRender::Render::Quick::Quick3DRenderPass::qt_metacast(const char *_clname)
{
    if (!_clname) return Q_NULLPTR;
    if (!strcmp(_clname, qt_meta_stringdata_Qt3DRender__Render__Quick__Quick3DRenderPass.stringdata0))
        return static_cast<void*>(const_cast< Quick3DRenderPass*>(this));
    return QObject::qt_metacast(_clname);
}

int Qt3DRender::Render::Quick::Quick3DRenderPass::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QObject::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    
#ifndef QT_NO_PROPERTIES
   if (_c == QMetaObject::ReadProperty || _c == QMetaObject::WriteProperty
            || _c == QMetaObject::ResetProperty || _c == QMetaObject::RegisterPropertyMetaType) {
        qt_static_metacall(this, _c, _id, _a);
        _id -= 4;
    } else if (_c == QMetaObject::QueryPropertyDesignable) {
        _id -= 4;
    } else if (_c == QMetaObject::QueryPropertyScriptable) {
        _id -= 4;
    } else if (_c == QMetaObject::QueryPropertyStored) {
        _id -= 4;
    } else if (_c == QMetaObject::QueryPropertyEditable) {
        _id -= 4;
    } else if (_c == QMetaObject::QueryPropertyUser) {
        _id -= 4;
    }
#endif // QT_NO_PROPERTIES
    return _id;
}
QT_END_MOC_NAMESPACE
