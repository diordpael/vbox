/****************************************************************************
** Meta object code from reading C++ file 'scene3ditem_p.h'
**
** Created by: The Qt Meta Object Compiler version 67 (Qt 5.6.3)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "../../scene3ditem_p.h"
#include <QtCore/qbytearray.h>
#include <QtCore/qmetatype.h>
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'scene3ditem_p.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 67
#error "This file was generated using the moc from 5.6.3. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
struct qt_meta_stringdata_Qt3DRender__Scene3DItem_t {
    QByteArrayData data[13];
    char stringdata0[178];
};
#define QT_MOC_LITERAL(idx, ofs, len) \
    Q_STATIC_BYTE_ARRAY_DATA_HEADER_INITIALIZER_WITH_OFFSET(len, \
    qptrdiff(offsetof(qt_meta_stringdata_Qt3DRender__Scene3DItem_t, stringdata0) + ofs \
        - idx * sizeof(QByteArrayData)) \
    )
static const qt_meta_stringdata_Qt3DRender__Scene3DItem_t qt_meta_stringdata_Qt3DRender__Scene3DItem = {
    {
QT_MOC_LITERAL(0, 0, 23), // "Qt3DRender::Scene3DItem"
QT_MOC_LITERAL(1, 24, 15), // "DefaultProperty"
QT_MOC_LITERAL(2, 40, 6), // "entity"
QT_MOC_LITERAL(3, 47, 14), // "aspectsChanged"
QT_MOC_LITERAL(4, 62, 0), // ""
QT_MOC_LITERAL(5, 63, 13), // "entityChanged"
QT_MOC_LITERAL(6, 77, 18), // "multisampleChanged"
QT_MOC_LITERAL(7, 96, 10), // "setAspects"
QT_MOC_LITERAL(8, 107, 7), // "aspects"
QT_MOC_LITERAL(9, 115, 9), // "setEntity"
QT_MOC_LITERAL(10, 125, 18), // "Qt3DCore::QEntity*"
QT_MOC_LITERAL(11, 144, 21), // "applyRootEntityChange"
QT_MOC_LITERAL(12, 166, 11) // "multisample"

    },
    "Qt3DRender::Scene3DItem\0DefaultProperty\0"
    "entity\0aspectsChanged\0\0entityChanged\0"
    "multisampleChanged\0setAspects\0aspects\0"
    "setEntity\0Qt3DCore::QEntity*\0"
    "applyRootEntityChange\0multisample"
};
#undef QT_MOC_LITERAL

static const uint qt_meta_data_Qt3DRender__Scene3DItem[] = {

 // content:
       7,       // revision
       0,       // classname
       1,   14, // classinfo
       6,   16, // methods
       3,   56, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       3,       // signalCount

 // classinfo: key, value
       1,    2,

 // signals: name, argc, parameters, tag, flags
       3,    0,   46,    4, 0x06 /* Public */,
       5,    0,   47,    4, 0x06 /* Public */,
       6,    0,   48,    4, 0x06 /* Public */,

 // slots: name, argc, parameters, tag, flags
       7,    1,   49,    4, 0x0a /* Public */,
       9,    1,   52,    4, 0x0a /* Public */,
      11,    0,   55,    4, 0x08 /* Private */,

 // signals: parameters
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,

 // slots: parameters
    QMetaType::Void, QMetaType::QStringList,    8,
    QMetaType::Void, 0x80000000 | 10,    2,
    QMetaType::Void,

 // properties: name, type, flags
       2, 0x80000000 | 10, 0x0049510b,
       8, QMetaType::QStringList, 0x00495103,
      12, QMetaType::Bool, 0x00495103,

 // properties: notify_signal_id
       1,
       0,
       2,

       0        // eod
};

void Qt3DRender::Scene3DItem::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        Scene3DItem *_t = static_cast<Scene3DItem *>(_o);
        Q_UNUSED(_t)
        switch (_id) {
        case 0: _t->aspectsChanged(); break;
        case 1: _t->entityChanged(); break;
        case 2: _t->multisampleChanged(); break;
        case 3: _t->setAspects((*reinterpret_cast< const QStringList(*)>(_a[1]))); break;
        case 4: _t->setEntity((*reinterpret_cast< Qt3DCore::QEntity*(*)>(_a[1]))); break;
        case 5: _t->applyRootEntityChange(); break;
        default: ;
        }
    } else if (_c == QMetaObject::IndexOfMethod) {
        int *result = reinterpret_cast<int *>(_a[0]);
        void **func = reinterpret_cast<void **>(_a[1]);
        {
            typedef void (Scene3DItem::*_t)();
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&Scene3DItem::aspectsChanged)) {
                *result = 0;
                return;
            }
        }
        {
            typedef void (Scene3DItem::*_t)();
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&Scene3DItem::entityChanged)) {
                *result = 1;
                return;
            }
        }
        {
            typedef void (Scene3DItem::*_t)();
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&Scene3DItem::multisampleChanged)) {
                *result = 2;
                return;
            }
        }
    }
#ifndef QT_NO_PROPERTIES
    else if (_c == QMetaObject::ReadProperty) {
        Scene3DItem *_t = static_cast<Scene3DItem *>(_o);
        Q_UNUSED(_t)
        void *_v = _a[0];
        switch (_id) {
        case 0: *reinterpret_cast< Qt3DCore::QEntity**>(_v) = _t->entity(); break;
        case 1: *reinterpret_cast< QStringList*>(_v) = _t->aspects(); break;
        case 2: *reinterpret_cast< bool*>(_v) = _t->multisample(); break;
        default: break;
        }
    } else if (_c == QMetaObject::WriteProperty) {
        Scene3DItem *_t = static_cast<Scene3DItem *>(_o);
        Q_UNUSED(_t)
        void *_v = _a[0];
        switch (_id) {
        case 0: _t->setEntity(*reinterpret_cast< Qt3DCore::QEntity**>(_v)); break;
        case 1: _t->setAspects(*reinterpret_cast< QStringList*>(_v)); break;
        case 2: _t->setMultisample(*reinterpret_cast< bool*>(_v)); break;
        default: break;
        }
    } else if (_c == QMetaObject::ResetProperty) {
    }
#endif // QT_NO_PROPERTIES
}

const QMetaObject Qt3DRender::Scene3DItem::staticMetaObject = {
    { &QQuickItem::staticMetaObject, qt_meta_stringdata_Qt3DRender__Scene3DItem.data,
      qt_meta_data_Qt3DRender__Scene3DItem,  qt_static_metacall, Q_NULLPTR, Q_NULLPTR}
};


const QMetaObject *Qt3DRender::Scene3DItem::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->dynamicMetaObject() : &staticMetaObject;
}

void *Qt3DRender::Scene3DItem::qt_metacast(const char *_clname)
{
    if (!_clname) return Q_NULLPTR;
    if (!strcmp(_clname, qt_meta_stringdata_Qt3DRender__Scene3DItem.stringdata0))
        return static_cast<void*>(const_cast< Scene3DItem*>(this));
    return QQuickItem::qt_metacast(_clname);
}

int Qt3DRender::Scene3DItem::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QQuickItem::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 6)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 6;
    } else if (_c == QMetaObject::RegisterMethodArgumentMetaType) {
        if (_id < 6)
            *reinterpret_cast<int*>(_a[0]) = -1;
        _id -= 6;
    }
#ifndef QT_NO_PROPERTIES
   else if (_c == QMetaObject::ReadProperty || _c == QMetaObject::WriteProperty
            || _c == QMetaObject::ResetProperty || _c == QMetaObject::RegisterPropertyMetaType) {
        qt_static_metacall(this, _c, _id, _a);
        _id -= 3;
    } else if (_c == QMetaObject::QueryPropertyDesignable) {
        _id -= 3;
    } else if (_c == QMetaObject::QueryPropertyScriptable) {
        _id -= 3;
    } else if (_c == QMetaObject::QueryPropertyStored) {
        _id -= 3;
    } else if (_c == QMetaObject::QueryPropertyEditable) {
        _id -= 3;
    } else if (_c == QMetaObject::QueryPropertyUser) {
        _id -= 3;
    }
#endif // QT_NO_PROPERTIES
    return _id;
}

// SIGNAL 0
void Qt3DRender::Scene3DItem::aspectsChanged()
{
    QMetaObject::activate(this, &staticMetaObject, 0, Q_NULLPTR);
}

// SIGNAL 1
void Qt3DRender::Scene3DItem::entityChanged()
{
    QMetaObject::activate(this, &staticMetaObject, 1, Q_NULLPTR);
}

// SIGNAL 2
void Qt3DRender::Scene3DItem::multisampleChanged()
{
    QMetaObject::activate(this, &staticMetaObject, 2, Q_NULLPTR);
}
QT_END_MOC_NAMESPACE
