/****************************************************************************
** Meta object code from reading C++ file 'qmousecontroller.h'
**
** Created by: The Qt Meta Object Compiler version 67 (Qt 5.6.3)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "../../frontend/qmousecontroller.h"
#include <QtCore/qbytearray.h>
#include <QtCore/qmetatype.h>
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'qmousecontroller.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 67
#error "This file was generated using the moc from 5.6.3. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
struct qt_meta_stringdata_Qt3DInput__QMouseController_t {
    QByteArrayData data[13];
    char stringdata0[115];
};
#define QT_MOC_LITERAL(idx, ofs, len) \
    Q_STATIC_BYTE_ARRAY_DATA_HEADER_INITIALIZER_WITH_OFFSET(len, \
    qptrdiff(offsetof(qt_meta_stringdata_Qt3DInput__QMouseController_t, stringdata0) + ofs \
        - idx * sizeof(QByteArrayData)) \
    )
static const qt_meta_stringdata_Qt3DInput__QMouseController_t qt_meta_stringdata_Qt3DInput__QMouseController = {
    {
QT_MOC_LITERAL(0, 0, 27), // "Qt3DInput::QMouseController"
QT_MOC_LITERAL(1, 28, 18), // "sensitivityChanged"
QT_MOC_LITERAL(2, 47, 0), // ""
QT_MOC_LITERAL(3, 48, 5), // "value"
QT_MOC_LITERAL(4, 54, 14), // "setSensitivity"
QT_MOC_LITERAL(5, 69, 11), // "sensitivity"
QT_MOC_LITERAL(6, 81, 4), // "Axis"
QT_MOC_LITERAL(7, 86, 1), // "X"
QT_MOC_LITERAL(8, 88, 1), // "Y"
QT_MOC_LITERAL(9, 90, 6), // "Button"
QT_MOC_LITERAL(10, 97, 4), // "Left"
QT_MOC_LITERAL(11, 102, 6), // "Center"
QT_MOC_LITERAL(12, 109, 5) // "Right"

    },
    "Qt3DInput::QMouseController\0"
    "sensitivityChanged\0\0value\0setSensitivity\0"
    "sensitivity\0Axis\0X\0Y\0Button\0Left\0"
    "Center\0Right"
};
#undef QT_MOC_LITERAL

static const uint qt_meta_data_Qt3DInput__QMouseController[] = {

 // content:
       7,       // revision
       0,       // classname
       0,    0, // classinfo
       2,   14, // methods
       1,   30, // properties
       2,   34, // enums/sets
       0,    0, // constructors
       0,       // flags
       1,       // signalCount

 // signals: name, argc, parameters, tag, flags
       1,    1,   24,    2, 0x06 /* Public */,

 // slots: name, argc, parameters, tag, flags
       4,    1,   27,    2, 0x0a /* Public */,

 // signals: parameters
    QMetaType::Void, QMetaType::Float,    3,

 // slots: parameters
    QMetaType::Void, QMetaType::Float,    3,

 // properties: name, type, flags
       5, QMetaType::Float, 0x00495103,

 // properties: notify_signal_id
       0,

 // enums: name, flags, count, data
       6, 0x0,    2,   42,
       9, 0x0,    3,   46,

 // enum data: key, value
       7, uint(Qt3DInput::QMouseController::X),
       8, uint(Qt3DInput::QMouseController::Y),
      10, uint(Qt3DInput::QMouseController::Left),
      11, uint(Qt3DInput::QMouseController::Center),
      12, uint(Qt3DInput::QMouseController::Right),

       0        // eod
};

void Qt3DInput::QMouseController::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        QMouseController *_t = static_cast<QMouseController *>(_o);
        Q_UNUSED(_t)
        switch (_id) {
        case 0: _t->sensitivityChanged((*reinterpret_cast< float(*)>(_a[1]))); break;
        case 1: _t->setSensitivity((*reinterpret_cast< float(*)>(_a[1]))); break;
        default: ;
        }
    } else if (_c == QMetaObject::IndexOfMethod) {
        int *result = reinterpret_cast<int *>(_a[0]);
        void **func = reinterpret_cast<void **>(_a[1]);
        {
            typedef void (QMouseController::*_t)(float );
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&QMouseController::sensitivityChanged)) {
                *result = 0;
                return;
            }
        }
    }
#ifndef QT_NO_PROPERTIES
    else if (_c == QMetaObject::ReadProperty) {
        QMouseController *_t = static_cast<QMouseController *>(_o);
        Q_UNUSED(_t)
        void *_v = _a[0];
        switch (_id) {
        case 0: *reinterpret_cast< float*>(_v) = _t->sensitivity(); break;
        default: break;
        }
    } else if (_c == QMetaObject::WriteProperty) {
        QMouseController *_t = static_cast<QMouseController *>(_o);
        Q_UNUSED(_t)
        void *_v = _a[0];
        switch (_id) {
        case 0: _t->setSensitivity(*reinterpret_cast< float*>(_v)); break;
        default: break;
        }
    } else if (_c == QMetaObject::ResetProperty) {
    }
#endif // QT_NO_PROPERTIES
}

const QMetaObject Qt3DInput::QMouseController::staticMetaObject = {
    { &Qt3DInput::QAbstractPhysicalDevice::staticMetaObject, qt_meta_stringdata_Qt3DInput__QMouseController.data,
      qt_meta_data_Qt3DInput__QMouseController,  qt_static_metacall, Q_NULLPTR, Q_NULLPTR}
};


const QMetaObject *Qt3DInput::QMouseController::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->dynamicMetaObject() : &staticMetaObject;
}

void *Qt3DInput::QMouseController::qt_metacast(const char *_clname)
{
    if (!_clname) return Q_NULLPTR;
    if (!strcmp(_clname, qt_meta_stringdata_Qt3DInput__QMouseController.stringdata0))
        return static_cast<void*>(const_cast< QMouseController*>(this));
    return Qt3DInput::QAbstractPhysicalDevice::qt_metacast(_clname);
}

int Qt3DInput::QMouseController::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = Qt3DInput::QAbstractPhysicalDevice::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 2)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 2;
    } else if (_c == QMetaObject::RegisterMethodArgumentMetaType) {
        if (_id < 2)
            *reinterpret_cast<int*>(_a[0]) = -1;
        _id -= 2;
    }
#ifndef QT_NO_PROPERTIES
   else if (_c == QMetaObject::ReadProperty || _c == QMetaObject::WriteProperty
            || _c == QMetaObject::ResetProperty || _c == QMetaObject::RegisterPropertyMetaType) {
        qt_static_metacall(this, _c, _id, _a);
        _id -= 1;
    } else if (_c == QMetaObject::QueryPropertyDesignable) {
        _id -= 1;
    } else if (_c == QMetaObject::QueryPropertyScriptable) {
        _id -= 1;
    } else if (_c == QMetaObject::QueryPropertyStored) {
        _id -= 1;
    } else if (_c == QMetaObject::QueryPropertyEditable) {
        _id -= 1;
    } else if (_c == QMetaObject::QueryPropertyUser) {
        _id -= 1;
    }
#endif // QT_NO_PROPERTIES
    return _id;
}

// SIGNAL 0
void Qt3DInput::QMouseController::sensitivityChanged(float _t1)
{
    void *_a[] = { Q_NULLPTR, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 0, _a);
}
QT_END_MOC_NAMESPACE
