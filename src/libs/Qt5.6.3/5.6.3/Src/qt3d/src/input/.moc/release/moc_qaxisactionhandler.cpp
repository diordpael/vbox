/****************************************************************************
** Meta object code from reading C++ file 'qaxisactionhandler.h'
**
** Created by: The Qt Meta Object Compiler version 67 (Qt 5.6.3)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "../../frontend/qaxisactionhandler.h"
#include <QtCore/qbytearray.h>
#include <QtCore/qmetatype.h>
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'qaxisactionhandler.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 67
#error "This file was generated using the moc from 5.6.3. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
struct qt_meta_stringdata_Qt3DInput__QAxisActionHandler_t {
    QByteArrayData data[11];
    char stringdata0[171];
};
#define QT_MOC_LITERAL(idx, ofs, len) \
    Q_STATIC_BYTE_ARRAY_DATA_HEADER_INITIALIZER_WITH_OFFSET(len, \
    qptrdiff(offsetof(qt_meta_stringdata_Qt3DInput__QAxisActionHandler_t, stringdata0) + ofs \
        - idx * sizeof(QByteArrayData)) \
    )
static const qt_meta_stringdata_Qt3DInput__QAxisActionHandler_t qt_meta_stringdata_Qt3DInput__QAxisActionHandler = {
    {
QT_MOC_LITERAL(0, 0, 29), // "Qt3DInput::QAxisActionHandler"
QT_MOC_LITERAL(1, 30, 20), // "logicalDeviceChanged"
QT_MOC_LITERAL(2, 51, 0), // ""
QT_MOC_LITERAL(3, 52, 26), // "Qt3DInput::QLogicalDevice*"
QT_MOC_LITERAL(4, 79, 13), // "logicalDevice"
QT_MOC_LITERAL(5, 93, 13), // "actionStarted"
QT_MOC_LITERAL(6, 107, 4), // "name"
QT_MOC_LITERAL(7, 112, 14), // "actionFinished"
QT_MOC_LITERAL(8, 127, 16), // "axisValueChanged"
QT_MOC_LITERAL(9, 144, 9), // "axisValue"
QT_MOC_LITERAL(10, 154, 16) // "setLogicalDevice"

    },
    "Qt3DInput::QAxisActionHandler\0"
    "logicalDeviceChanged\0\0Qt3DInput::QLogicalDevice*\0"
    "logicalDevice\0actionStarted\0name\0"
    "actionFinished\0axisValueChanged\0"
    "axisValue\0setLogicalDevice"
};
#undef QT_MOC_LITERAL

static const uint qt_meta_data_Qt3DInput__QAxisActionHandler[] = {

 // content:
       7,       // revision
       0,       // classname
       0,    0, // classinfo
       5,   14, // methods
       1,   56, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       4,       // signalCount

 // signals: name, argc, parameters, tag, flags
       1,    1,   39,    2, 0x06 /* Public */,
       5,    1,   42,    2, 0x06 /* Public */,
       7,    1,   45,    2, 0x06 /* Public */,
       8,    2,   48,    2, 0x06 /* Public */,

 // slots: name, argc, parameters, tag, flags
      10,    1,   53,    2, 0x0a /* Public */,

 // signals: parameters
    QMetaType::Void, 0x80000000 | 3,    4,
    QMetaType::Void, QMetaType::QString,    6,
    QMetaType::Void, QMetaType::QString,    6,
    QMetaType::Void, QMetaType::QString, QMetaType::Float,    6,    9,

 // slots: parameters
    QMetaType::Void, 0x80000000 | 3,    4,

 // properties: name, type, flags
       4, 0x80000000 | 3, 0x0049510b,

 // properties: notify_signal_id
       0,

       0        // eod
};

void Qt3DInput::QAxisActionHandler::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        QAxisActionHandler *_t = static_cast<QAxisActionHandler *>(_o);
        Q_UNUSED(_t)
        switch (_id) {
        case 0: _t->logicalDeviceChanged((*reinterpret_cast< Qt3DInput::QLogicalDevice*(*)>(_a[1]))); break;
        case 1: _t->actionStarted((*reinterpret_cast< QString(*)>(_a[1]))); break;
        case 2: _t->actionFinished((*reinterpret_cast< QString(*)>(_a[1]))); break;
        case 3: _t->axisValueChanged((*reinterpret_cast< QString(*)>(_a[1])),(*reinterpret_cast< float(*)>(_a[2]))); break;
        case 4: _t->setLogicalDevice((*reinterpret_cast< Qt3DInput::QLogicalDevice*(*)>(_a[1]))); break;
        default: ;
        }
    } else if (_c == QMetaObject::IndexOfMethod) {
        int *result = reinterpret_cast<int *>(_a[0]);
        void **func = reinterpret_cast<void **>(_a[1]);
        {
            typedef void (QAxisActionHandler::*_t)(Qt3DInput::QLogicalDevice * );
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&QAxisActionHandler::logicalDeviceChanged)) {
                *result = 0;
                return;
            }
        }
        {
            typedef void (QAxisActionHandler::*_t)(QString );
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&QAxisActionHandler::actionStarted)) {
                *result = 1;
                return;
            }
        }
        {
            typedef void (QAxisActionHandler::*_t)(QString );
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&QAxisActionHandler::actionFinished)) {
                *result = 2;
                return;
            }
        }
        {
            typedef void (QAxisActionHandler::*_t)(QString , float );
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&QAxisActionHandler::axisValueChanged)) {
                *result = 3;
                return;
            }
        }
    }
#ifndef QT_NO_PROPERTIES
    else if (_c == QMetaObject::ReadProperty) {
        QAxisActionHandler *_t = static_cast<QAxisActionHandler *>(_o);
        Q_UNUSED(_t)
        void *_v = _a[0];
        switch (_id) {
        case 0: *reinterpret_cast< Qt3DInput::QLogicalDevice**>(_v) = _t->logicalDevice(); break;
        default: break;
        }
    } else if (_c == QMetaObject::WriteProperty) {
        QAxisActionHandler *_t = static_cast<QAxisActionHandler *>(_o);
        Q_UNUSED(_t)
        void *_v = _a[0];
        switch (_id) {
        case 0: _t->setLogicalDevice(*reinterpret_cast< Qt3DInput::QLogicalDevice**>(_v)); break;
        default: break;
        }
    } else if (_c == QMetaObject::ResetProperty) {
    }
#endif // QT_NO_PROPERTIES
}

const QMetaObject Qt3DInput::QAxisActionHandler::staticMetaObject = {
    { &Qt3DCore::QComponent::staticMetaObject, qt_meta_stringdata_Qt3DInput__QAxisActionHandler.data,
      qt_meta_data_Qt3DInput__QAxisActionHandler,  qt_static_metacall, Q_NULLPTR, Q_NULLPTR}
};


const QMetaObject *Qt3DInput::QAxisActionHandler::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->dynamicMetaObject() : &staticMetaObject;
}

void *Qt3DInput::QAxisActionHandler::qt_metacast(const char *_clname)
{
    if (!_clname) return Q_NULLPTR;
    if (!strcmp(_clname, qt_meta_stringdata_Qt3DInput__QAxisActionHandler.stringdata0))
        return static_cast<void*>(const_cast< QAxisActionHandler*>(this));
    return Qt3DCore::QComponent::qt_metacast(_clname);
}

int Qt3DInput::QAxisActionHandler::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = Qt3DCore::QComponent::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 5)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 5;
    } else if (_c == QMetaObject::RegisterMethodArgumentMetaType) {
        if (_id < 5)
            *reinterpret_cast<int*>(_a[0]) = -1;
        _id -= 5;
    }
#ifndef QT_NO_PROPERTIES
   else if (_c == QMetaObject::ReadProperty || _c == QMetaObject::WriteProperty
            || _c == QMetaObject::ResetProperty || _c == QMetaObject::RegisterPropertyMetaType) {
        qt_static_metacall(this, _c, _id, _a);
        _id -= 1;
    } else if (_c == QMetaObject::QueryPropertyDesignable) {
        _id -= 1;
    } else if (_c == QMetaObject::QueryPropertyScriptable) {
        _id -= 1;
    } else if (_c == QMetaObject::QueryPropertyStored) {
        _id -= 1;
    } else if (_c == QMetaObject::QueryPropertyEditable) {
        _id -= 1;
    } else if (_c == QMetaObject::QueryPropertyUser) {
        _id -= 1;
    }
#endif // QT_NO_PROPERTIES
    return _id;
}

// SIGNAL 0
void Qt3DInput::QAxisActionHandler::logicalDeviceChanged(Qt3DInput::QLogicalDevice * _t1)
{
    void *_a[] = { Q_NULLPTR, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 0, _a);
}

// SIGNAL 1
void Qt3DInput::QAxisActionHandler::actionStarted(QString _t1)
{
    void *_a[] = { Q_NULLPTR, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 1, _a);
}

// SIGNAL 2
void Qt3DInput::QAxisActionHandler::actionFinished(QString _t1)
{
    void *_a[] = { Q_NULLPTR, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 2, _a);
}

// SIGNAL 3
void Qt3DInput::QAxisActionHandler::axisValueChanged(QString _t1, float _t2)
{
    void *_a[] = { Q_NULLPTR, const_cast<void*>(reinterpret_cast<const void*>(&_t1)), const_cast<void*>(reinterpret_cast<const void*>(&_t2)) };
    QMetaObject::activate(this, &staticMetaObject, 3, _a);
}
QT_END_MOC_NAMESPACE
