/****************************************************************************
** Meta object code from reading C++ file 'qaxisinput.h'
**
** Created by: The Qt Meta Object Compiler version 67 (Qt 5.6.3)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "../../frontend/qaxisinput.h"
#include <QtCore/qbytearray.h>
#include <QtCore/qmetatype.h>
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'qaxisinput.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 67
#error "This file was generated using the moc from 5.6.3. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
struct qt_meta_stringdata_Qt3DInput__QAxisInput_t {
    QByteArrayData data[16];
    char stringdata0[211];
};
#define QT_MOC_LITERAL(idx, ofs, len) \
    Q_STATIC_BYTE_ARRAY_DATA_HEADER_INITIALIZER_WITH_OFFSET(len, \
    qptrdiff(offsetof(qt_meta_stringdata_Qt3DInput__QAxisInput_t, stringdata0) + ofs \
        - idx * sizeof(QByteArrayData)) \
    )
static const qt_meta_stringdata_Qt3DInput__QAxisInput_t qt_meta_stringdata_Qt3DInput__QAxisInput = {
    {
QT_MOC_LITERAL(0, 0, 21), // "Qt3DInput::QAxisInput"
QT_MOC_LITERAL(1, 22, 19), // "sourceDeviceChanged"
QT_MOC_LITERAL(2, 42, 0), // ""
QT_MOC_LITERAL(3, 43, 24), // "QAbstractPhysicalDevice*"
QT_MOC_LITERAL(4, 68, 12), // "sourceDevice"
QT_MOC_LITERAL(5, 81, 12), // "scaleChanged"
QT_MOC_LITERAL(6, 94, 5), // "scale"
QT_MOC_LITERAL(7, 100, 11), // "axisChanged"
QT_MOC_LITERAL(8, 112, 4), // "axis"
QT_MOC_LITERAL(9, 117, 11), // "keysChanged"
QT_MOC_LITERAL(10, 129, 4), // "keys"
QT_MOC_LITERAL(11, 134, 15), // "setSourceDevice"
QT_MOC_LITERAL(12, 150, 8), // "setScale"
QT_MOC_LITERAL(13, 159, 7), // "setAxis"
QT_MOC_LITERAL(14, 167, 7), // "setKeys"
QT_MOC_LITERAL(15, 175, 35) // "Qt3DInput::QAbstractPhysicalD..."

    },
    "Qt3DInput::QAxisInput\0sourceDeviceChanged\0"
    "\0QAbstractPhysicalDevice*\0sourceDevice\0"
    "scaleChanged\0scale\0axisChanged\0axis\0"
    "keysChanged\0keys\0setSourceDevice\0"
    "setScale\0setAxis\0setKeys\0"
    "Qt3DInput::QAbstractPhysicalDevice*"
};
#undef QT_MOC_LITERAL

static const uint qt_meta_data_Qt3DInput__QAxisInput[] = {

 // content:
       7,       // revision
       0,       // classname
       0,    0, // classinfo
       8,   14, // methods
       4,   78, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       4,       // signalCount

 // signals: name, argc, parameters, tag, flags
       1,    1,   54,    2, 0x06 /* Public */,
       5,    1,   57,    2, 0x06 /* Public */,
       7,    1,   60,    2, 0x06 /* Public */,
       9,    1,   63,    2, 0x06 /* Public */,

 // slots: name, argc, parameters, tag, flags
      11,    1,   66,    2, 0x0a /* Public */,
      12,    1,   69,    2, 0x0a /* Public */,
      13,    1,   72,    2, 0x0a /* Public */,
      14,    1,   75,    2, 0x0a /* Public */,

 // signals: parameters
    QMetaType::Void, 0x80000000 | 3,    4,
    QMetaType::Void, QMetaType::Float,    6,
    QMetaType::Void, QMetaType::Int,    8,
    QMetaType::Void, QMetaType::QVariantList,   10,

 // slots: parameters
    QMetaType::Void, 0x80000000 | 3,    4,
    QMetaType::Void, QMetaType::Float,    6,
    QMetaType::Void, QMetaType::Int,    8,
    QMetaType::Void, QMetaType::QVariantList,   10,

 // properties: name, type, flags
       4, 0x80000000 | 15, 0x0049510b,
       6, QMetaType::Float, 0x00495103,
       8, QMetaType::Int, 0x00495103,
      10, QMetaType::QVariantList, 0x00495103,

 // properties: notify_signal_id
       0,
       1,
       2,
       3,

       0        // eod
};

void Qt3DInput::QAxisInput::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        QAxisInput *_t = static_cast<QAxisInput *>(_o);
        Q_UNUSED(_t)
        switch (_id) {
        case 0: _t->sourceDeviceChanged((*reinterpret_cast< QAbstractPhysicalDevice*(*)>(_a[1]))); break;
        case 1: _t->scaleChanged((*reinterpret_cast< float(*)>(_a[1]))); break;
        case 2: _t->axisChanged((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 3: _t->keysChanged((*reinterpret_cast< const QVariantList(*)>(_a[1]))); break;
        case 4: _t->setSourceDevice((*reinterpret_cast< QAbstractPhysicalDevice*(*)>(_a[1]))); break;
        case 5: _t->setScale((*reinterpret_cast< float(*)>(_a[1]))); break;
        case 6: _t->setAxis((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 7: _t->setKeys((*reinterpret_cast< const QVariantList(*)>(_a[1]))); break;
        default: ;
        }
    } else if (_c == QMetaObject::IndexOfMethod) {
        int *result = reinterpret_cast<int *>(_a[0]);
        void **func = reinterpret_cast<void **>(_a[1]);
        {
            typedef void (QAxisInput::*_t)(QAbstractPhysicalDevice * );
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&QAxisInput::sourceDeviceChanged)) {
                *result = 0;
                return;
            }
        }
        {
            typedef void (QAxisInput::*_t)(float );
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&QAxisInput::scaleChanged)) {
                *result = 1;
                return;
            }
        }
        {
            typedef void (QAxisInput::*_t)(int );
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&QAxisInput::axisChanged)) {
                *result = 2;
                return;
            }
        }
        {
            typedef void (QAxisInput::*_t)(const QVariantList & );
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&QAxisInput::keysChanged)) {
                *result = 3;
                return;
            }
        }
    }
#ifndef QT_NO_PROPERTIES
    else if (_c == QMetaObject::ReadProperty) {
        QAxisInput *_t = static_cast<QAxisInput *>(_o);
        Q_UNUSED(_t)
        void *_v = _a[0];
        switch (_id) {
        case 0: *reinterpret_cast< Qt3DInput::QAbstractPhysicalDevice**>(_v) = _t->sourceDevice(); break;
        case 1: *reinterpret_cast< float*>(_v) = _t->scale(); break;
        case 2: *reinterpret_cast< int*>(_v) = _t->axis(); break;
        case 3: *reinterpret_cast< QVariantList*>(_v) = _t->keys(); break;
        default: break;
        }
    } else if (_c == QMetaObject::WriteProperty) {
        QAxisInput *_t = static_cast<QAxisInput *>(_o);
        Q_UNUSED(_t)
        void *_v = _a[0];
        switch (_id) {
        case 0: _t->setSourceDevice(*reinterpret_cast< Qt3DInput::QAbstractPhysicalDevice**>(_v)); break;
        case 1: _t->setScale(*reinterpret_cast< float*>(_v)); break;
        case 2: _t->setAxis(*reinterpret_cast< int*>(_v)); break;
        case 3: _t->setKeys(*reinterpret_cast< QVariantList*>(_v)); break;
        default: break;
        }
    } else if (_c == QMetaObject::ResetProperty) {
    }
#endif // QT_NO_PROPERTIES
}

const QMetaObject Qt3DInput::QAxisInput::staticMetaObject = {
    { &Qt3DCore::QNode::staticMetaObject, qt_meta_stringdata_Qt3DInput__QAxisInput.data,
      qt_meta_data_Qt3DInput__QAxisInput,  qt_static_metacall, Q_NULLPTR, Q_NULLPTR}
};


const QMetaObject *Qt3DInput::QAxisInput::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->dynamicMetaObject() : &staticMetaObject;
}

void *Qt3DInput::QAxisInput::qt_metacast(const char *_clname)
{
    if (!_clname) return Q_NULLPTR;
    if (!strcmp(_clname, qt_meta_stringdata_Qt3DInput__QAxisInput.stringdata0))
        return static_cast<void*>(const_cast< QAxisInput*>(this));
    return Qt3DCore::QNode::qt_metacast(_clname);
}

int Qt3DInput::QAxisInput::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = Qt3DCore::QNode::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 8)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 8;
    } else if (_c == QMetaObject::RegisterMethodArgumentMetaType) {
        if (_id < 8)
            *reinterpret_cast<int*>(_a[0]) = -1;
        _id -= 8;
    }
#ifndef QT_NO_PROPERTIES
   else if (_c == QMetaObject::ReadProperty || _c == QMetaObject::WriteProperty
            || _c == QMetaObject::ResetProperty || _c == QMetaObject::RegisterPropertyMetaType) {
        qt_static_metacall(this, _c, _id, _a);
        _id -= 4;
    } else if (_c == QMetaObject::QueryPropertyDesignable) {
        _id -= 4;
    } else if (_c == QMetaObject::QueryPropertyScriptable) {
        _id -= 4;
    } else if (_c == QMetaObject::QueryPropertyStored) {
        _id -= 4;
    } else if (_c == QMetaObject::QueryPropertyEditable) {
        _id -= 4;
    } else if (_c == QMetaObject::QueryPropertyUser) {
        _id -= 4;
    }
#endif // QT_NO_PROPERTIES
    return _id;
}

// SIGNAL 0
void Qt3DInput::QAxisInput::sourceDeviceChanged(QAbstractPhysicalDevice * _t1)
{
    void *_a[] = { Q_NULLPTR, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 0, _a);
}

// SIGNAL 1
void Qt3DInput::QAxisInput::scaleChanged(float _t1)
{
    void *_a[] = { Q_NULLPTR, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 1, _a);
}

// SIGNAL 2
void Qt3DInput::QAxisInput::axisChanged(int _t1)
{
    void *_a[] = { Q_NULLPTR, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 2, _a);
}

// SIGNAL 3
void Qt3DInput::QAxisInput::keysChanged(const QVariantList & _t1)
{
    void *_a[] = { Q_NULLPTR, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 3, _a);
}
QT_END_MOC_NAMESPACE
