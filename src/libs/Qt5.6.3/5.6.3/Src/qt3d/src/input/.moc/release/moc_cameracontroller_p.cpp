/****************************************************************************
** Meta object code from reading C++ file 'cameracontroller_p.h'
**
** Created by: The Qt Meta Object Compiler version 67 (Qt 5.6.3)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "../../backend/cameracontroller_p.h"
#include <QtCore/qbytearray.h>
#include <QtCore/qmetatype.h>
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'cameracontroller_p.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 67
#error "This file was generated using the moc from 5.6.3. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
struct qt_meta_stringdata_Qt3DInput__Input__CameraController_t {
    QByteArrayData data[23];
    char stringdata0[317];
};
#define QT_MOC_LITERAL(idx, ofs, len) \
    Q_STATIC_BYTE_ARRAY_DATA_HEADER_INITIALIZER_WITH_OFFSET(len, \
    qptrdiff(offsetof(qt_meta_stringdata_Qt3DInput__Input__CameraController_t, stringdata0) + ofs \
        - idx * sizeof(QByteArrayData)) \
    )
static const qt_meta_stringdata_Qt3DInput__Input__CameraController_t qt_meta_stringdata_Qt3DInput__Input__CameraController = {
    {
QT_MOC_LITERAL(0, 0, 34), // "Qt3DInput::Input::CameraContr..."
QT_MOC_LITERAL(1, 35, 18), // "linearSpeedChanged"
QT_MOC_LITERAL(2, 54, 0), // ""
QT_MOC_LITERAL(3, 55, 5), // "speed"
QT_MOC_LITERAL(4, 61, 16), // "orbitRateChanged"
QT_MOC_LITERAL(5, 78, 4), // "rate"
QT_MOC_LITERAL(6, 83, 15), // "lookRateChanged"
QT_MOC_LITERAL(7, 99, 25), // "multisampleEnabledChanged"
QT_MOC_LITERAL(8, 125, 7), // "enabled"
QT_MOC_LITERAL(9, 133, 18), // "controlModeChanged"
QT_MOC_LITERAL(10, 152, 11), // "ControlMode"
QT_MOC_LITERAL(11, 164, 11), // "controlMode"
QT_MOC_LITERAL(12, 176, 26), // "firstPersonUpVectorChanged"
QT_MOC_LITERAL(13, 203, 2), // "up"
QT_MOC_LITERAL(14, 206, 10), // "toggleMSAA"
QT_MOC_LITERAL(15, 217, 8), // "onUpdate"
QT_MOC_LITERAL(16, 226, 11), // "linearSpeed"
QT_MOC_LITERAL(17, 238, 9), // "orbitRate"
QT_MOC_LITERAL(18, 248, 8), // "lookRate"
QT_MOC_LITERAL(19, 257, 18), // "multisampleEnabled"
QT_MOC_LITERAL(20, 276, 19), // "firstPersonUpVector"
QT_MOC_LITERAL(21, 296, 8), // "FreeLook"
QT_MOC_LITERAL(22, 305, 11) // "FirstPerson"

    },
    "Qt3DInput::Input::CameraController\0"
    "linearSpeedChanged\0\0speed\0orbitRateChanged\0"
    "rate\0lookRateChanged\0multisampleEnabledChanged\0"
    "enabled\0controlModeChanged\0ControlMode\0"
    "controlMode\0firstPersonUpVectorChanged\0"
    "up\0toggleMSAA\0onUpdate\0linearSpeed\0"
    "orbitRate\0lookRate\0multisampleEnabled\0"
    "firstPersonUpVector\0FreeLook\0FirstPerson"
};
#undef QT_MOC_LITERAL

static const uint qt_meta_data_Qt3DInput__Input__CameraController[] = {

 // content:
       7,       // revision
       0,       // classname
       0,    0, // classinfo
       8,   14, // methods
       6,   74, // properties
       1,   98, // enums/sets
       0,    0, // constructors
       0,       // flags
       6,       // signalCount

 // signals: name, argc, parameters, tag, flags
       1,    1,   54,    2, 0x06 /* Public */,
       4,    1,   57,    2, 0x06 /* Public */,
       6,    1,   60,    2, 0x06 /* Public */,
       7,    1,   63,    2, 0x06 /* Public */,
       9,    1,   66,    2, 0x06 /* Public */,
      12,    1,   69,    2, 0x06 /* Public */,

 // slots: name, argc, parameters, tag, flags
      14,    0,   72,    2, 0x0a /* Public */,
      15,    0,   73,    2, 0x08 /* Private */,

 // signals: parameters
    QMetaType::Void, QMetaType::Float,    3,
    QMetaType::Void, QMetaType::Float,    5,
    QMetaType::Void, QMetaType::Float,    5,
    QMetaType::Void, QMetaType::Bool,    8,
    QMetaType::Void, 0x80000000 | 10,   11,
    QMetaType::Void, QMetaType::QVector3D,   13,

 // slots: parameters
    QMetaType::Void,
    QMetaType::Void,

 // properties: name, type, flags
      16, QMetaType::Float, 0x00495103,
      17, QMetaType::Float, 0x00495103,
      18, QMetaType::Float, 0x00495103,
      19, QMetaType::Bool, 0x00495001,
      11, 0x80000000 | 10, 0x0049510b,
      20, QMetaType::QVector3D, 0x00495103,

 // properties: notify_signal_id
       0,
       1,
       2,
       3,
       4,
       5,

 // enums: name, flags, count, data
      10, 0x0,    2,  102,

 // enum data: key, value
      21, uint(Qt3DInput::Input::CameraController::FreeLook),
      22, uint(Qt3DInput::Input::CameraController::FirstPerson),

       0        // eod
};

void Qt3DInput::Input::CameraController::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        CameraController *_t = static_cast<CameraController *>(_o);
        Q_UNUSED(_t)
        switch (_id) {
        case 0: _t->linearSpeedChanged((*reinterpret_cast< float(*)>(_a[1]))); break;
        case 1: _t->orbitRateChanged((*reinterpret_cast< float(*)>(_a[1]))); break;
        case 2: _t->lookRateChanged((*reinterpret_cast< float(*)>(_a[1]))); break;
        case 3: _t->multisampleEnabledChanged((*reinterpret_cast< bool(*)>(_a[1]))); break;
        case 4: _t->controlModeChanged((*reinterpret_cast< ControlMode(*)>(_a[1]))); break;
        case 5: _t->firstPersonUpVectorChanged((*reinterpret_cast< const QVector3D(*)>(_a[1]))); break;
        case 6: _t->toggleMSAA(); break;
        case 7: _t->onUpdate(); break;
        default: ;
        }
    } else if (_c == QMetaObject::IndexOfMethod) {
        int *result = reinterpret_cast<int *>(_a[0]);
        void **func = reinterpret_cast<void **>(_a[1]);
        {
            typedef void (CameraController::*_t)(float );
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&CameraController::linearSpeedChanged)) {
                *result = 0;
                return;
            }
        }
        {
            typedef void (CameraController::*_t)(float );
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&CameraController::orbitRateChanged)) {
                *result = 1;
                return;
            }
        }
        {
            typedef void (CameraController::*_t)(float );
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&CameraController::lookRateChanged)) {
                *result = 2;
                return;
            }
        }
        {
            typedef void (CameraController::*_t)(bool );
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&CameraController::multisampleEnabledChanged)) {
                *result = 3;
                return;
            }
        }
        {
            typedef void (CameraController::*_t)(ControlMode );
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&CameraController::controlModeChanged)) {
                *result = 4;
                return;
            }
        }
        {
            typedef void (CameraController::*_t)(const QVector3D & );
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&CameraController::firstPersonUpVectorChanged)) {
                *result = 5;
                return;
            }
        }
    }
#ifndef QT_NO_PROPERTIES
    else if (_c == QMetaObject::ReadProperty) {
        CameraController *_t = static_cast<CameraController *>(_o);
        Q_UNUSED(_t)
        void *_v = _a[0];
        switch (_id) {
        case 0: *reinterpret_cast< float*>(_v) = _t->linearSpeed(); break;
        case 1: *reinterpret_cast< float*>(_v) = _t->orbitRate(); break;
        case 2: *reinterpret_cast< float*>(_v) = _t->lookRate(); break;
        case 3: *reinterpret_cast< bool*>(_v) = _t->isMultisampleEnabled(); break;
        case 4: *reinterpret_cast< ControlMode*>(_v) = _t->controlMode(); break;
        case 5: *reinterpret_cast< QVector3D*>(_v) = _t->firstPersonUpVector(); break;
        default: break;
        }
    } else if (_c == QMetaObject::WriteProperty) {
        CameraController *_t = static_cast<CameraController *>(_o);
        Q_UNUSED(_t)
        void *_v = _a[0];
        switch (_id) {
        case 0: _t->setLinearSpeed(*reinterpret_cast< float*>(_v)); break;
        case 1: _t->setOrbitRate(*reinterpret_cast< float*>(_v)); break;
        case 2: _t->setLookRate(*reinterpret_cast< float*>(_v)); break;
        case 4: _t->setControlMode(*reinterpret_cast< ControlMode*>(_v)); break;
        case 5: _t->setFirstPersonUpVector(*reinterpret_cast< QVector3D*>(_v)); break;
        default: break;
        }
    } else if (_c == QMetaObject::ResetProperty) {
    }
#endif // QT_NO_PROPERTIES
}

const QMetaObject Qt3DInput::Input::CameraController::staticMetaObject = {
    { &QObject::staticMetaObject, qt_meta_stringdata_Qt3DInput__Input__CameraController.data,
      qt_meta_data_Qt3DInput__Input__CameraController,  qt_static_metacall, Q_NULLPTR, Q_NULLPTR}
};


const QMetaObject *Qt3DInput::Input::CameraController::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->dynamicMetaObject() : &staticMetaObject;
}

void *Qt3DInput::Input::CameraController::qt_metacast(const char *_clname)
{
    if (!_clname) return Q_NULLPTR;
    if (!strcmp(_clname, qt_meta_stringdata_Qt3DInput__Input__CameraController.stringdata0))
        return static_cast<void*>(const_cast< CameraController*>(this));
    return QObject::qt_metacast(_clname);
}

int Qt3DInput::Input::CameraController::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QObject::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 8)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 8;
    } else if (_c == QMetaObject::RegisterMethodArgumentMetaType) {
        if (_id < 8)
            *reinterpret_cast<int*>(_a[0]) = -1;
        _id -= 8;
    }
#ifndef QT_NO_PROPERTIES
   else if (_c == QMetaObject::ReadProperty || _c == QMetaObject::WriteProperty
            || _c == QMetaObject::ResetProperty || _c == QMetaObject::RegisterPropertyMetaType) {
        qt_static_metacall(this, _c, _id, _a);
        _id -= 6;
    } else if (_c == QMetaObject::QueryPropertyDesignable) {
        _id -= 6;
    } else if (_c == QMetaObject::QueryPropertyScriptable) {
        _id -= 6;
    } else if (_c == QMetaObject::QueryPropertyStored) {
        _id -= 6;
    } else if (_c == QMetaObject::QueryPropertyEditable) {
        _id -= 6;
    } else if (_c == QMetaObject::QueryPropertyUser) {
        _id -= 6;
    }
#endif // QT_NO_PROPERTIES
    return _id;
}

// SIGNAL 0
void Qt3DInput::Input::CameraController::linearSpeedChanged(float _t1)
{
    void *_a[] = { Q_NULLPTR, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 0, _a);
}

// SIGNAL 1
void Qt3DInput::Input::CameraController::orbitRateChanged(float _t1)
{
    void *_a[] = { Q_NULLPTR, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 1, _a);
}

// SIGNAL 2
void Qt3DInput::Input::CameraController::lookRateChanged(float _t1)
{
    void *_a[] = { Q_NULLPTR, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 2, _a);
}

// SIGNAL 3
void Qt3DInput::Input::CameraController::multisampleEnabledChanged(bool _t1)
{
    void *_a[] = { Q_NULLPTR, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 3, _a);
}

// SIGNAL 4
void Qt3DInput::Input::CameraController::controlModeChanged(ControlMode _t1)
{
    void *_a[] = { Q_NULLPTR, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 4, _a);
}

// SIGNAL 5
void Qt3DInput::Input::CameraController::firstPersonUpVectorChanged(const QVector3D & _t1)
{
    void *_a[] = { Q_NULLPTR, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 5, _a);
}
QT_END_MOC_NAMESPACE
