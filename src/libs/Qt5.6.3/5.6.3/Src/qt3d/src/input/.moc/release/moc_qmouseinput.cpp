/****************************************************************************
** Meta object code from reading C++ file 'qmouseinput.h'
**
** Created by: The Qt Meta Object Compiler version 67 (Qt 5.6.3)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "../../frontend/qmouseinput.h"
#include <QtCore/qbytearray.h>
#include <QtCore/qmetatype.h>
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'qmouseinput.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 67
#error "This file was generated using the moc from 5.6.3. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
struct qt_meta_stringdata_Qt3DInput__QMouseInput_t {
    QByteArrayData data[21];
    char stringdata0[292];
};
#define QT_MOC_LITERAL(idx, ofs, len) \
    Q_STATIC_BYTE_ARRAY_DATA_HEADER_INITIALIZER_WITH_OFFSET(len, \
    qptrdiff(offsetof(qt_meta_stringdata_Qt3DInput__QMouseInput_t, stringdata0) + ofs \
        - idx * sizeof(QByteArrayData)) \
    )
static const qt_meta_stringdata_Qt3DInput__QMouseInput_t qt_meta_stringdata_Qt3DInput__QMouseInput = {
    {
QT_MOC_LITERAL(0, 0, 22), // "Qt3DInput::QMouseInput"
QT_MOC_LITERAL(1, 23, 17), // "controllerChanged"
QT_MOC_LITERAL(2, 41, 0), // ""
QT_MOC_LITERAL(3, 42, 17), // "QMouseController*"
QT_MOC_LITERAL(4, 60, 10), // "controller"
QT_MOC_LITERAL(5, 71, 20), // "containsMouseChanged"
QT_MOC_LITERAL(6, 92, 13), // "containsMouse"
QT_MOC_LITERAL(7, 106, 7), // "clicked"
QT_MOC_LITERAL(8, 114, 23), // "Qt3DInput::QMouseEvent*"
QT_MOC_LITERAL(9, 138, 5), // "mouse"
QT_MOC_LITERAL(10, 144, 13), // "doubleClicked"
QT_MOC_LITERAL(11, 158, 7), // "entered"
QT_MOC_LITERAL(12, 166, 6), // "exited"
QT_MOC_LITERAL(13, 173, 7), // "pressed"
QT_MOC_LITERAL(14, 181, 8), // "released"
QT_MOC_LITERAL(15, 190, 12), // "pressAndHold"
QT_MOC_LITERAL(16, 203, 15), // "positionChanged"
QT_MOC_LITERAL(17, 219, 5), // "wheel"
QT_MOC_LITERAL(18, 225, 23), // "Qt3DInput::QWheelEvent*"
QT_MOC_LITERAL(19, 249, 13), // "setController"
QT_MOC_LITERAL(20, 263, 28) // "Qt3DInput::QMouseController*"

    },
    "Qt3DInput::QMouseInput\0controllerChanged\0"
    "\0QMouseController*\0controller\0"
    "containsMouseChanged\0containsMouse\0"
    "clicked\0Qt3DInput::QMouseEvent*\0mouse\0"
    "doubleClicked\0entered\0exited\0pressed\0"
    "released\0pressAndHold\0positionChanged\0"
    "wheel\0Qt3DInput::QWheelEvent*\0"
    "setController\0Qt3DInput::QMouseController*"
};
#undef QT_MOC_LITERAL

static const uint qt_meta_data_Qt3DInput__QMouseInput[] = {

 // content:
       7,       // revision
       0,       // classname
       0,    0, // classinfo
      12,   14, // methods
       2,  106, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
      11,       // signalCount

 // signals: name, argc, parameters, tag, flags
       1,    1,   74,    2, 0x06 /* Public */,
       5,    1,   77,    2, 0x06 /* Public */,
       7,    1,   80,    2, 0x06 /* Public */,
      10,    1,   83,    2, 0x06 /* Public */,
      11,    0,   86,    2, 0x06 /* Public */,
      12,    0,   87,    2, 0x06 /* Public */,
      13,    1,   88,    2, 0x06 /* Public */,
      14,    1,   91,    2, 0x06 /* Public */,
      15,    1,   94,    2, 0x06 /* Public */,
      16,    1,   97,    2, 0x06 /* Public */,
      17,    1,  100,    2, 0x06 /* Public */,

 // slots: name, argc, parameters, tag, flags
      19,    1,  103,    2, 0x0a /* Public */,

 // signals: parameters
    QMetaType::Void, 0x80000000 | 3,    4,
    QMetaType::Void, QMetaType::Bool,    6,
    QMetaType::Void, 0x80000000 | 8,    9,
    QMetaType::Void, 0x80000000 | 8,    9,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void, 0x80000000 | 8,    9,
    QMetaType::Void, 0x80000000 | 8,    9,
    QMetaType::Void, 0x80000000 | 8,    9,
    QMetaType::Void, 0x80000000 | 8,    9,
    QMetaType::Void, 0x80000000 | 18,   17,

 // slots: parameters
    QMetaType::Void, 0x80000000 | 3,    4,

 // properties: name, type, flags
       4, 0x80000000 | 20, 0x0049510b,
       6, QMetaType::Bool, 0x00495001,

 // properties: notify_signal_id
       0,
       1,

       0        // eod
};

void Qt3DInput::QMouseInput::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        QMouseInput *_t = static_cast<QMouseInput *>(_o);
        Q_UNUSED(_t)
        switch (_id) {
        case 0: _t->controllerChanged((*reinterpret_cast< QMouseController*(*)>(_a[1]))); break;
        case 1: _t->containsMouseChanged((*reinterpret_cast< bool(*)>(_a[1]))); break;
        case 2: _t->clicked((*reinterpret_cast< Qt3DInput::QMouseEvent*(*)>(_a[1]))); break;
        case 3: _t->doubleClicked((*reinterpret_cast< Qt3DInput::QMouseEvent*(*)>(_a[1]))); break;
        case 4: _t->entered(); break;
        case 5: _t->exited(); break;
        case 6: _t->pressed((*reinterpret_cast< Qt3DInput::QMouseEvent*(*)>(_a[1]))); break;
        case 7: _t->released((*reinterpret_cast< Qt3DInput::QMouseEvent*(*)>(_a[1]))); break;
        case 8: _t->pressAndHold((*reinterpret_cast< Qt3DInput::QMouseEvent*(*)>(_a[1]))); break;
        case 9: _t->positionChanged((*reinterpret_cast< Qt3DInput::QMouseEvent*(*)>(_a[1]))); break;
        case 10: _t->wheel((*reinterpret_cast< Qt3DInput::QWheelEvent*(*)>(_a[1]))); break;
        case 11: _t->setController((*reinterpret_cast< QMouseController*(*)>(_a[1]))); break;
        default: ;
        }
    } else if (_c == QMetaObject::RegisterMethodArgumentMetaType) {
        switch (_id) {
        default: *reinterpret_cast<int*>(_a[0]) = -1; break;
        case 2:
            switch (*reinterpret_cast<int*>(_a[1])) {
            default: *reinterpret_cast<int*>(_a[0]) = -1; break;
            case 0:
                *reinterpret_cast<int*>(_a[0]) = qRegisterMetaType< Qt3DInput::QMouseEvent* >(); break;
            }
            break;
        case 3:
            switch (*reinterpret_cast<int*>(_a[1])) {
            default: *reinterpret_cast<int*>(_a[0]) = -1; break;
            case 0:
                *reinterpret_cast<int*>(_a[0]) = qRegisterMetaType< Qt3DInput::QMouseEvent* >(); break;
            }
            break;
        case 6:
            switch (*reinterpret_cast<int*>(_a[1])) {
            default: *reinterpret_cast<int*>(_a[0]) = -1; break;
            case 0:
                *reinterpret_cast<int*>(_a[0]) = qRegisterMetaType< Qt3DInput::QMouseEvent* >(); break;
            }
            break;
        case 7:
            switch (*reinterpret_cast<int*>(_a[1])) {
            default: *reinterpret_cast<int*>(_a[0]) = -1; break;
            case 0:
                *reinterpret_cast<int*>(_a[0]) = qRegisterMetaType< Qt3DInput::QMouseEvent* >(); break;
            }
            break;
        case 8:
            switch (*reinterpret_cast<int*>(_a[1])) {
            default: *reinterpret_cast<int*>(_a[0]) = -1; break;
            case 0:
                *reinterpret_cast<int*>(_a[0]) = qRegisterMetaType< Qt3DInput::QMouseEvent* >(); break;
            }
            break;
        case 9:
            switch (*reinterpret_cast<int*>(_a[1])) {
            default: *reinterpret_cast<int*>(_a[0]) = -1; break;
            case 0:
                *reinterpret_cast<int*>(_a[0]) = qRegisterMetaType< Qt3DInput::QMouseEvent* >(); break;
            }
            break;
        case 10:
            switch (*reinterpret_cast<int*>(_a[1])) {
            default: *reinterpret_cast<int*>(_a[0]) = -1; break;
            case 0:
                *reinterpret_cast<int*>(_a[0]) = qRegisterMetaType< Qt3DInput::QWheelEvent* >(); break;
            }
            break;
        }
    } else if (_c == QMetaObject::IndexOfMethod) {
        int *result = reinterpret_cast<int *>(_a[0]);
        void **func = reinterpret_cast<void **>(_a[1]);
        {
            typedef void (QMouseInput::*_t)(QMouseController * );
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&QMouseInput::controllerChanged)) {
                *result = 0;
                return;
            }
        }
        {
            typedef void (QMouseInput::*_t)(bool );
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&QMouseInput::containsMouseChanged)) {
                *result = 1;
                return;
            }
        }
        {
            typedef void (QMouseInput::*_t)(Qt3DInput::QMouseEvent * );
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&QMouseInput::clicked)) {
                *result = 2;
                return;
            }
        }
        {
            typedef void (QMouseInput::*_t)(Qt3DInput::QMouseEvent * );
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&QMouseInput::doubleClicked)) {
                *result = 3;
                return;
            }
        }
        {
            typedef void (QMouseInput::*_t)();
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&QMouseInput::entered)) {
                *result = 4;
                return;
            }
        }
        {
            typedef void (QMouseInput::*_t)();
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&QMouseInput::exited)) {
                *result = 5;
                return;
            }
        }
        {
            typedef void (QMouseInput::*_t)(Qt3DInput::QMouseEvent * );
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&QMouseInput::pressed)) {
                *result = 6;
                return;
            }
        }
        {
            typedef void (QMouseInput::*_t)(Qt3DInput::QMouseEvent * );
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&QMouseInput::released)) {
                *result = 7;
                return;
            }
        }
        {
            typedef void (QMouseInput::*_t)(Qt3DInput::QMouseEvent * );
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&QMouseInput::pressAndHold)) {
                *result = 8;
                return;
            }
        }
        {
            typedef void (QMouseInput::*_t)(Qt3DInput::QMouseEvent * );
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&QMouseInput::positionChanged)) {
                *result = 9;
                return;
            }
        }
        {
            typedef void (QMouseInput::*_t)(Qt3DInput::QWheelEvent * );
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&QMouseInput::wheel)) {
                *result = 10;
                return;
            }
        }
    }
#ifndef QT_NO_PROPERTIES
    else if (_c == QMetaObject::ReadProperty) {
        QMouseInput *_t = static_cast<QMouseInput *>(_o);
        Q_UNUSED(_t)
        void *_v = _a[0];
        switch (_id) {
        case 0: *reinterpret_cast< Qt3DInput::QMouseController**>(_v) = _t->controller(); break;
        case 1: *reinterpret_cast< bool*>(_v) = _t->containsMouse(); break;
        default: break;
        }
    } else if (_c == QMetaObject::WriteProperty) {
        QMouseInput *_t = static_cast<QMouseInput *>(_o);
        Q_UNUSED(_t)
        void *_v = _a[0];
        switch (_id) {
        case 0: _t->setController(*reinterpret_cast< Qt3DInput::QMouseController**>(_v)); break;
        default: break;
        }
    } else if (_c == QMetaObject::ResetProperty) {
    }
#endif // QT_NO_PROPERTIES
}

const QMetaObject Qt3DInput::QMouseInput::staticMetaObject = {
    { &Qt3DCore::QComponent::staticMetaObject, qt_meta_stringdata_Qt3DInput__QMouseInput.data,
      qt_meta_data_Qt3DInput__QMouseInput,  qt_static_metacall, Q_NULLPTR, Q_NULLPTR}
};


const QMetaObject *Qt3DInput::QMouseInput::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->dynamicMetaObject() : &staticMetaObject;
}

void *Qt3DInput::QMouseInput::qt_metacast(const char *_clname)
{
    if (!_clname) return Q_NULLPTR;
    if (!strcmp(_clname, qt_meta_stringdata_Qt3DInput__QMouseInput.stringdata0))
        return static_cast<void*>(const_cast< QMouseInput*>(this));
    return Qt3DCore::QComponent::qt_metacast(_clname);
}

int Qt3DInput::QMouseInput::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = Qt3DCore::QComponent::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 12)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 12;
    } else if (_c == QMetaObject::RegisterMethodArgumentMetaType) {
        if (_id < 12)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 12;
    }
#ifndef QT_NO_PROPERTIES
   else if (_c == QMetaObject::ReadProperty || _c == QMetaObject::WriteProperty
            || _c == QMetaObject::ResetProperty || _c == QMetaObject::RegisterPropertyMetaType) {
        qt_static_metacall(this, _c, _id, _a);
        _id -= 2;
    } else if (_c == QMetaObject::QueryPropertyDesignable) {
        _id -= 2;
    } else if (_c == QMetaObject::QueryPropertyScriptable) {
        _id -= 2;
    } else if (_c == QMetaObject::QueryPropertyStored) {
        _id -= 2;
    } else if (_c == QMetaObject::QueryPropertyEditable) {
        _id -= 2;
    } else if (_c == QMetaObject::QueryPropertyUser) {
        _id -= 2;
    }
#endif // QT_NO_PROPERTIES
    return _id;
}

// SIGNAL 0
void Qt3DInput::QMouseInput::controllerChanged(QMouseController * _t1)
{
    void *_a[] = { Q_NULLPTR, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 0, _a);
}

// SIGNAL 1
void Qt3DInput::QMouseInput::containsMouseChanged(bool _t1)
{
    void *_a[] = { Q_NULLPTR, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 1, _a);
}

// SIGNAL 2
void Qt3DInput::QMouseInput::clicked(Qt3DInput::QMouseEvent * _t1)
{
    void *_a[] = { Q_NULLPTR, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 2, _a);
}

// SIGNAL 3
void Qt3DInput::QMouseInput::doubleClicked(Qt3DInput::QMouseEvent * _t1)
{
    void *_a[] = { Q_NULLPTR, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 3, _a);
}

// SIGNAL 4
void Qt3DInput::QMouseInput::entered()
{
    QMetaObject::activate(this, &staticMetaObject, 4, Q_NULLPTR);
}

// SIGNAL 5
void Qt3DInput::QMouseInput::exited()
{
    QMetaObject::activate(this, &staticMetaObject, 5, Q_NULLPTR);
}

// SIGNAL 6
void Qt3DInput::QMouseInput::pressed(Qt3DInput::QMouseEvent * _t1)
{
    void *_a[] = { Q_NULLPTR, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 6, _a);
}

// SIGNAL 7
void Qt3DInput::QMouseInput::released(Qt3DInput::QMouseEvent * _t1)
{
    void *_a[] = { Q_NULLPTR, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 7, _a);
}

// SIGNAL 8
void Qt3DInput::QMouseInput::pressAndHold(Qt3DInput::QMouseEvent * _t1)
{
    void *_a[] = { Q_NULLPTR, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 8, _a);
}

// SIGNAL 9
void Qt3DInput::QMouseInput::positionChanged(Qt3DInput::QMouseEvent * _t1)
{
    void *_a[] = { Q_NULLPTR, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 9, _a);
}

// SIGNAL 10
void Qt3DInput::QMouseInput::wheel(Qt3DInput::QWheelEvent * _t1)
{
    void *_a[] = { Q_NULLPTR, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 10, _a);
}
QT_END_MOC_NAMESPACE
