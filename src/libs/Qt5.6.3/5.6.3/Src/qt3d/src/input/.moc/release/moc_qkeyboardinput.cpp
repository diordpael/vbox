/****************************************************************************
** Meta object code from reading C++ file 'qkeyboardinput.h'
**
** Created by: The Qt Meta Object Compiler version 67 (Qt 5.6.3)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "../../frontend/qkeyboardinput.h"
#include <QtCore/qbytearray.h>
#include <QtCore/qmetatype.h>
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'qkeyboardinput.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 67
#error "This file was generated using the moc from 5.6.3. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
struct qt_meta_stringdata_Qt3DInput__QKeyboardInput_t {
    QByteArrayData data[52];
    char stringdata0[720];
};
#define QT_MOC_LITERAL(idx, ofs, len) \
    Q_STATIC_BYTE_ARRAY_DATA_HEADER_INITIALIZER_WITH_OFFSET(len, \
    qptrdiff(offsetof(qt_meta_stringdata_Qt3DInput__QKeyboardInput_t, stringdata0) + ofs \
        - idx * sizeof(QByteArrayData)) \
    )
static const qt_meta_stringdata_Qt3DInput__QKeyboardInput_t qt_meta_stringdata_Qt3DInput__QKeyboardInput = {
    {
QT_MOC_LITERAL(0, 0, 25), // "Qt3DInput::QKeyboardInput"
QT_MOC_LITERAL(1, 26, 17), // "controllerChanged"
QT_MOC_LITERAL(2, 44, 0), // ""
QT_MOC_LITERAL(3, 45, 20), // "QKeyboardController*"
QT_MOC_LITERAL(4, 66, 10), // "controller"
QT_MOC_LITERAL(5, 77, 12), // "focusChanged"
QT_MOC_LITERAL(6, 90, 5), // "focus"
QT_MOC_LITERAL(7, 96, 13), // "digit0Pressed"
QT_MOC_LITERAL(8, 110, 21), // "Qt3DInput::QKeyEvent*"
QT_MOC_LITERAL(9, 132, 5), // "event"
QT_MOC_LITERAL(10, 138, 13), // "digit1Pressed"
QT_MOC_LITERAL(11, 152, 13), // "digit2Pressed"
QT_MOC_LITERAL(12, 166, 13), // "digit3Pressed"
QT_MOC_LITERAL(13, 180, 13), // "digit4Pressed"
QT_MOC_LITERAL(14, 194, 13), // "digit5Pressed"
QT_MOC_LITERAL(15, 208, 13), // "digit6Pressed"
QT_MOC_LITERAL(16, 222, 13), // "digit7Pressed"
QT_MOC_LITERAL(17, 236, 13), // "digit8Pressed"
QT_MOC_LITERAL(18, 250, 13), // "digit9Pressed"
QT_MOC_LITERAL(19, 264, 11), // "leftPressed"
QT_MOC_LITERAL(20, 276, 12), // "rightPressed"
QT_MOC_LITERAL(21, 289, 9), // "upPressed"
QT_MOC_LITERAL(22, 299, 11), // "downPressed"
QT_MOC_LITERAL(23, 311, 10), // "tabPressed"
QT_MOC_LITERAL(24, 322, 14), // "backtabPressed"
QT_MOC_LITERAL(25, 337, 15), // "asteriskPressed"
QT_MOC_LITERAL(26, 353, 17), // "numberSignPressed"
QT_MOC_LITERAL(27, 371, 13), // "escapePressed"
QT_MOC_LITERAL(28, 385, 13), // "returnPressed"
QT_MOC_LITERAL(29, 399, 12), // "enterPressed"
QT_MOC_LITERAL(30, 412, 13), // "deletePressed"
QT_MOC_LITERAL(31, 426, 12), // "spacePressed"
QT_MOC_LITERAL(32, 439, 11), // "backPressed"
QT_MOC_LITERAL(33, 451, 13), // "cancelPressed"
QT_MOC_LITERAL(34, 465, 13), // "selectPressed"
QT_MOC_LITERAL(35, 479, 10), // "yesPressed"
QT_MOC_LITERAL(36, 490, 9), // "noPressed"
QT_MOC_LITERAL(37, 500, 15), // "context1Pressed"
QT_MOC_LITERAL(38, 516, 15), // "context2Pressed"
QT_MOC_LITERAL(39, 532, 15), // "context3Pressed"
QT_MOC_LITERAL(40, 548, 15), // "context4Pressed"
QT_MOC_LITERAL(41, 564, 11), // "callPressed"
QT_MOC_LITERAL(42, 576, 13), // "hangupPressed"
QT_MOC_LITERAL(43, 590, 11), // "flipPressed"
QT_MOC_LITERAL(44, 602, 11), // "menuPressed"
QT_MOC_LITERAL(45, 614, 15), // "volumeUpPressed"
QT_MOC_LITERAL(46, 630, 17), // "volumeDownPressed"
QT_MOC_LITERAL(47, 648, 7), // "pressed"
QT_MOC_LITERAL(48, 656, 8), // "released"
QT_MOC_LITERAL(49, 665, 13), // "setController"
QT_MOC_LITERAL(50, 679, 8), // "setFocus"
QT_MOC_LITERAL(51, 688, 31) // "Qt3DInput::QKeyboardController*"

    },
    "Qt3DInput::QKeyboardInput\0controllerChanged\0"
    "\0QKeyboardController*\0controller\0"
    "focusChanged\0focus\0digit0Pressed\0"
    "Qt3DInput::QKeyEvent*\0event\0digit1Pressed\0"
    "digit2Pressed\0digit3Pressed\0digit4Pressed\0"
    "digit5Pressed\0digit6Pressed\0digit7Pressed\0"
    "digit8Pressed\0digit9Pressed\0leftPressed\0"
    "rightPressed\0upPressed\0downPressed\0"
    "tabPressed\0backtabPressed\0asteriskPressed\0"
    "numberSignPressed\0escapePressed\0"
    "returnPressed\0enterPressed\0deletePressed\0"
    "spacePressed\0backPressed\0cancelPressed\0"
    "selectPressed\0yesPressed\0noPressed\0"
    "context1Pressed\0context2Pressed\0"
    "context3Pressed\0context4Pressed\0"
    "callPressed\0hangupPressed\0flipPressed\0"
    "menuPressed\0volumeUpPressed\0"
    "volumeDownPressed\0pressed\0released\0"
    "setController\0setFocus\0"
    "Qt3DInput::QKeyboardController*"
};
#undef QT_MOC_LITERAL

static const uint qt_meta_data_Qt3DInput__QKeyboardInput[] = {

 // content:
       7,       // revision
       0,       // classname
       0,    0, // classinfo
      44,   14, // methods
       2,  366, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
      42,       // signalCount

 // signals: name, argc, parameters, tag, flags
       1,    1,  234,    2, 0x06 /* Public */,
       5,    1,  237,    2, 0x06 /* Public */,
       7,    1,  240,    2, 0x06 /* Public */,
      10,    1,  243,    2, 0x06 /* Public */,
      11,    1,  246,    2, 0x06 /* Public */,
      12,    1,  249,    2, 0x06 /* Public */,
      13,    1,  252,    2, 0x06 /* Public */,
      14,    1,  255,    2, 0x06 /* Public */,
      15,    1,  258,    2, 0x06 /* Public */,
      16,    1,  261,    2, 0x06 /* Public */,
      17,    1,  264,    2, 0x06 /* Public */,
      18,    1,  267,    2, 0x06 /* Public */,
      19,    1,  270,    2, 0x06 /* Public */,
      20,    1,  273,    2, 0x06 /* Public */,
      21,    1,  276,    2, 0x06 /* Public */,
      22,    1,  279,    2, 0x06 /* Public */,
      23,    1,  282,    2, 0x06 /* Public */,
      24,    1,  285,    2, 0x06 /* Public */,
      25,    1,  288,    2, 0x06 /* Public */,
      26,    1,  291,    2, 0x06 /* Public */,
      27,    1,  294,    2, 0x06 /* Public */,
      28,    1,  297,    2, 0x06 /* Public */,
      29,    1,  300,    2, 0x06 /* Public */,
      30,    1,  303,    2, 0x06 /* Public */,
      31,    1,  306,    2, 0x06 /* Public */,
      32,    1,  309,    2, 0x06 /* Public */,
      33,    1,  312,    2, 0x06 /* Public */,
      34,    1,  315,    2, 0x06 /* Public */,
      35,    1,  318,    2, 0x06 /* Public */,
      36,    1,  321,    2, 0x06 /* Public */,
      37,    1,  324,    2, 0x06 /* Public */,
      38,    1,  327,    2, 0x06 /* Public */,
      39,    1,  330,    2, 0x06 /* Public */,
      40,    1,  333,    2, 0x06 /* Public */,
      41,    1,  336,    2, 0x06 /* Public */,
      42,    1,  339,    2, 0x06 /* Public */,
      43,    1,  342,    2, 0x06 /* Public */,
      44,    1,  345,    2, 0x06 /* Public */,
      45,    1,  348,    2, 0x06 /* Public */,
      46,    1,  351,    2, 0x06 /* Public */,
      47,    1,  354,    2, 0x06 /* Public */,
      48,    1,  357,    2, 0x06 /* Public */,

 // slots: name, argc, parameters, tag, flags
      49,    1,  360,    2, 0x0a /* Public */,
      50,    1,  363,    2, 0x0a /* Public */,

 // signals: parameters
    QMetaType::Void, 0x80000000 | 3,    4,
    QMetaType::Void, QMetaType::Bool,    6,
    QMetaType::Void, 0x80000000 | 8,    9,
    QMetaType::Void, 0x80000000 | 8,    9,
    QMetaType::Void, 0x80000000 | 8,    9,
    QMetaType::Void, 0x80000000 | 8,    9,
    QMetaType::Void, 0x80000000 | 8,    9,
    QMetaType::Void, 0x80000000 | 8,    9,
    QMetaType::Void, 0x80000000 | 8,    9,
    QMetaType::Void, 0x80000000 | 8,    9,
    QMetaType::Void, 0x80000000 | 8,    9,
    QMetaType::Void, 0x80000000 | 8,    9,
    QMetaType::Void, 0x80000000 | 8,    9,
    QMetaType::Void, 0x80000000 | 8,    9,
    QMetaType::Void, 0x80000000 | 8,    9,
    QMetaType::Void, 0x80000000 | 8,    9,
    QMetaType::Void, 0x80000000 | 8,    9,
    QMetaType::Void, 0x80000000 | 8,    9,
    QMetaType::Void, 0x80000000 | 8,    9,
    QMetaType::Void, 0x80000000 | 8,    9,
    QMetaType::Void, 0x80000000 | 8,    9,
    QMetaType::Void, 0x80000000 | 8,    9,
    QMetaType::Void, 0x80000000 | 8,    9,
    QMetaType::Void, 0x80000000 | 8,    9,
    QMetaType::Void, 0x80000000 | 8,    9,
    QMetaType::Void, 0x80000000 | 8,    9,
    QMetaType::Void, 0x80000000 | 8,    9,
    QMetaType::Void, 0x80000000 | 8,    9,
    QMetaType::Void, 0x80000000 | 8,    9,
    QMetaType::Void, 0x80000000 | 8,    9,
    QMetaType::Void, 0x80000000 | 8,    9,
    QMetaType::Void, 0x80000000 | 8,    9,
    QMetaType::Void, 0x80000000 | 8,    9,
    QMetaType::Void, 0x80000000 | 8,    9,
    QMetaType::Void, 0x80000000 | 8,    9,
    QMetaType::Void, 0x80000000 | 8,    9,
    QMetaType::Void, 0x80000000 | 8,    9,
    QMetaType::Void, 0x80000000 | 8,    9,
    QMetaType::Void, 0x80000000 | 8,    9,
    QMetaType::Void, 0x80000000 | 8,    9,
    QMetaType::Void, 0x80000000 | 8,    9,
    QMetaType::Void, 0x80000000 | 8,    9,

 // slots: parameters
    QMetaType::Void, 0x80000000 | 3,    4,
    QMetaType::Void, QMetaType::Bool,    6,

 // properties: name, type, flags
       4, 0x80000000 | 51, 0x0049510b,
       6, QMetaType::Bool, 0x00495103,

 // properties: notify_signal_id
       0,
       1,

       0        // eod
};

void Qt3DInput::QKeyboardInput::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        QKeyboardInput *_t = static_cast<QKeyboardInput *>(_o);
        Q_UNUSED(_t)
        switch (_id) {
        case 0: _t->controllerChanged((*reinterpret_cast< QKeyboardController*(*)>(_a[1]))); break;
        case 1: _t->focusChanged((*reinterpret_cast< bool(*)>(_a[1]))); break;
        case 2: _t->digit0Pressed((*reinterpret_cast< Qt3DInput::QKeyEvent*(*)>(_a[1]))); break;
        case 3: _t->digit1Pressed((*reinterpret_cast< Qt3DInput::QKeyEvent*(*)>(_a[1]))); break;
        case 4: _t->digit2Pressed((*reinterpret_cast< Qt3DInput::QKeyEvent*(*)>(_a[1]))); break;
        case 5: _t->digit3Pressed((*reinterpret_cast< Qt3DInput::QKeyEvent*(*)>(_a[1]))); break;
        case 6: _t->digit4Pressed((*reinterpret_cast< Qt3DInput::QKeyEvent*(*)>(_a[1]))); break;
        case 7: _t->digit5Pressed((*reinterpret_cast< Qt3DInput::QKeyEvent*(*)>(_a[1]))); break;
        case 8: _t->digit6Pressed((*reinterpret_cast< Qt3DInput::QKeyEvent*(*)>(_a[1]))); break;
        case 9: _t->digit7Pressed((*reinterpret_cast< Qt3DInput::QKeyEvent*(*)>(_a[1]))); break;
        case 10: _t->digit8Pressed((*reinterpret_cast< Qt3DInput::QKeyEvent*(*)>(_a[1]))); break;
        case 11: _t->digit9Pressed((*reinterpret_cast< Qt3DInput::QKeyEvent*(*)>(_a[1]))); break;
        case 12: _t->leftPressed((*reinterpret_cast< Qt3DInput::QKeyEvent*(*)>(_a[1]))); break;
        case 13: _t->rightPressed((*reinterpret_cast< Qt3DInput::QKeyEvent*(*)>(_a[1]))); break;
        case 14: _t->upPressed((*reinterpret_cast< Qt3DInput::QKeyEvent*(*)>(_a[1]))); break;
        case 15: _t->downPressed((*reinterpret_cast< Qt3DInput::QKeyEvent*(*)>(_a[1]))); break;
        case 16: _t->tabPressed((*reinterpret_cast< Qt3DInput::QKeyEvent*(*)>(_a[1]))); break;
        case 17: _t->backtabPressed((*reinterpret_cast< Qt3DInput::QKeyEvent*(*)>(_a[1]))); break;
        case 18: _t->asteriskPressed((*reinterpret_cast< Qt3DInput::QKeyEvent*(*)>(_a[1]))); break;
        case 19: _t->numberSignPressed((*reinterpret_cast< Qt3DInput::QKeyEvent*(*)>(_a[1]))); break;
        case 20: _t->escapePressed((*reinterpret_cast< Qt3DInput::QKeyEvent*(*)>(_a[1]))); break;
        case 21: _t->returnPressed((*reinterpret_cast< Qt3DInput::QKeyEvent*(*)>(_a[1]))); break;
        case 22: _t->enterPressed((*reinterpret_cast< Qt3DInput::QKeyEvent*(*)>(_a[1]))); break;
        case 23: _t->deletePressed((*reinterpret_cast< Qt3DInput::QKeyEvent*(*)>(_a[1]))); break;
        case 24: _t->spacePressed((*reinterpret_cast< Qt3DInput::QKeyEvent*(*)>(_a[1]))); break;
        case 25: _t->backPressed((*reinterpret_cast< Qt3DInput::QKeyEvent*(*)>(_a[1]))); break;
        case 26: _t->cancelPressed((*reinterpret_cast< Qt3DInput::QKeyEvent*(*)>(_a[1]))); break;
        case 27: _t->selectPressed((*reinterpret_cast< Qt3DInput::QKeyEvent*(*)>(_a[1]))); break;
        case 28: _t->yesPressed((*reinterpret_cast< Qt3DInput::QKeyEvent*(*)>(_a[1]))); break;
        case 29: _t->noPressed((*reinterpret_cast< Qt3DInput::QKeyEvent*(*)>(_a[1]))); break;
        case 30: _t->context1Pressed((*reinterpret_cast< Qt3DInput::QKeyEvent*(*)>(_a[1]))); break;
        case 31: _t->context2Pressed((*reinterpret_cast< Qt3DInput::QKeyEvent*(*)>(_a[1]))); break;
        case 32: _t->context3Pressed((*reinterpret_cast< Qt3DInput::QKeyEvent*(*)>(_a[1]))); break;
        case 33: _t->context4Pressed((*reinterpret_cast< Qt3DInput::QKeyEvent*(*)>(_a[1]))); break;
        case 34: _t->callPressed((*reinterpret_cast< Qt3DInput::QKeyEvent*(*)>(_a[1]))); break;
        case 35: _t->hangupPressed((*reinterpret_cast< Qt3DInput::QKeyEvent*(*)>(_a[1]))); break;
        case 36: _t->flipPressed((*reinterpret_cast< Qt3DInput::QKeyEvent*(*)>(_a[1]))); break;
        case 37: _t->menuPressed((*reinterpret_cast< Qt3DInput::QKeyEvent*(*)>(_a[1]))); break;
        case 38: _t->volumeUpPressed((*reinterpret_cast< Qt3DInput::QKeyEvent*(*)>(_a[1]))); break;
        case 39: _t->volumeDownPressed((*reinterpret_cast< Qt3DInput::QKeyEvent*(*)>(_a[1]))); break;
        case 40: _t->pressed((*reinterpret_cast< Qt3DInput::QKeyEvent*(*)>(_a[1]))); break;
        case 41: _t->released((*reinterpret_cast< Qt3DInput::QKeyEvent*(*)>(_a[1]))); break;
        case 42: _t->setController((*reinterpret_cast< QKeyboardController*(*)>(_a[1]))); break;
        case 43: _t->setFocus((*reinterpret_cast< bool(*)>(_a[1]))); break;
        default: ;
        }
    } else if (_c == QMetaObject::RegisterMethodArgumentMetaType) {
        switch (_id) {
        default: *reinterpret_cast<int*>(_a[0]) = -1; break;
        case 2:
            switch (*reinterpret_cast<int*>(_a[1])) {
            default: *reinterpret_cast<int*>(_a[0]) = -1; break;
            case 0:
                *reinterpret_cast<int*>(_a[0]) = qRegisterMetaType< Qt3DInput::QKeyEvent* >(); break;
            }
            break;
        case 3:
            switch (*reinterpret_cast<int*>(_a[1])) {
            default: *reinterpret_cast<int*>(_a[0]) = -1; break;
            case 0:
                *reinterpret_cast<int*>(_a[0]) = qRegisterMetaType< Qt3DInput::QKeyEvent* >(); break;
            }
            break;
        case 4:
            switch (*reinterpret_cast<int*>(_a[1])) {
            default: *reinterpret_cast<int*>(_a[0]) = -1; break;
            case 0:
                *reinterpret_cast<int*>(_a[0]) = qRegisterMetaType< Qt3DInput::QKeyEvent* >(); break;
            }
            break;
        case 5:
            switch (*reinterpret_cast<int*>(_a[1])) {
            default: *reinterpret_cast<int*>(_a[0]) = -1; break;
            case 0:
                *reinterpret_cast<int*>(_a[0]) = qRegisterMetaType< Qt3DInput::QKeyEvent* >(); break;
            }
            break;
        case 6:
            switch (*reinterpret_cast<int*>(_a[1])) {
            default: *reinterpret_cast<int*>(_a[0]) = -1; break;
            case 0:
                *reinterpret_cast<int*>(_a[0]) = qRegisterMetaType< Qt3DInput::QKeyEvent* >(); break;
            }
            break;
        case 7:
            switch (*reinterpret_cast<int*>(_a[1])) {
            default: *reinterpret_cast<int*>(_a[0]) = -1; break;
            case 0:
                *reinterpret_cast<int*>(_a[0]) = qRegisterMetaType< Qt3DInput::QKeyEvent* >(); break;
            }
            break;
        case 8:
            switch (*reinterpret_cast<int*>(_a[1])) {
            default: *reinterpret_cast<int*>(_a[0]) = -1; break;
            case 0:
                *reinterpret_cast<int*>(_a[0]) = qRegisterMetaType< Qt3DInput::QKeyEvent* >(); break;
            }
            break;
        case 9:
            switch (*reinterpret_cast<int*>(_a[1])) {
            default: *reinterpret_cast<int*>(_a[0]) = -1; break;
            case 0:
                *reinterpret_cast<int*>(_a[0]) = qRegisterMetaType< Qt3DInput::QKeyEvent* >(); break;
            }
            break;
        case 10:
            switch (*reinterpret_cast<int*>(_a[1])) {
            default: *reinterpret_cast<int*>(_a[0]) = -1; break;
            case 0:
                *reinterpret_cast<int*>(_a[0]) = qRegisterMetaType< Qt3DInput::QKeyEvent* >(); break;
            }
            break;
        case 11:
            switch (*reinterpret_cast<int*>(_a[1])) {
            default: *reinterpret_cast<int*>(_a[0]) = -1; break;
            case 0:
                *reinterpret_cast<int*>(_a[0]) = qRegisterMetaType< Qt3DInput::QKeyEvent* >(); break;
            }
            break;
        case 12:
            switch (*reinterpret_cast<int*>(_a[1])) {
            default: *reinterpret_cast<int*>(_a[0]) = -1; break;
            case 0:
                *reinterpret_cast<int*>(_a[0]) = qRegisterMetaType< Qt3DInput::QKeyEvent* >(); break;
            }
            break;
        case 13:
            switch (*reinterpret_cast<int*>(_a[1])) {
            default: *reinterpret_cast<int*>(_a[0]) = -1; break;
            case 0:
                *reinterpret_cast<int*>(_a[0]) = qRegisterMetaType< Qt3DInput::QKeyEvent* >(); break;
            }
            break;
        case 14:
            switch (*reinterpret_cast<int*>(_a[1])) {
            default: *reinterpret_cast<int*>(_a[0]) = -1; break;
            case 0:
                *reinterpret_cast<int*>(_a[0]) = qRegisterMetaType< Qt3DInput::QKeyEvent* >(); break;
            }
            break;
        case 15:
            switch (*reinterpret_cast<int*>(_a[1])) {
            default: *reinterpret_cast<int*>(_a[0]) = -1; break;
            case 0:
                *reinterpret_cast<int*>(_a[0]) = qRegisterMetaType< Qt3DInput::QKeyEvent* >(); break;
            }
            break;
        case 16:
            switch (*reinterpret_cast<int*>(_a[1])) {
            default: *reinterpret_cast<int*>(_a[0]) = -1; break;
            case 0:
                *reinterpret_cast<int*>(_a[0]) = qRegisterMetaType< Qt3DInput::QKeyEvent* >(); break;
            }
            break;
        case 17:
            switch (*reinterpret_cast<int*>(_a[1])) {
            default: *reinterpret_cast<int*>(_a[0]) = -1; break;
            case 0:
                *reinterpret_cast<int*>(_a[0]) = qRegisterMetaType< Qt3DInput::QKeyEvent* >(); break;
            }
            break;
        case 18:
            switch (*reinterpret_cast<int*>(_a[1])) {
            default: *reinterpret_cast<int*>(_a[0]) = -1; break;
            case 0:
                *reinterpret_cast<int*>(_a[0]) = qRegisterMetaType< Qt3DInput::QKeyEvent* >(); break;
            }
            break;
        case 19:
            switch (*reinterpret_cast<int*>(_a[1])) {
            default: *reinterpret_cast<int*>(_a[0]) = -1; break;
            case 0:
                *reinterpret_cast<int*>(_a[0]) = qRegisterMetaType< Qt3DInput::QKeyEvent* >(); break;
            }
            break;
        case 20:
            switch (*reinterpret_cast<int*>(_a[1])) {
            default: *reinterpret_cast<int*>(_a[0]) = -1; break;
            case 0:
                *reinterpret_cast<int*>(_a[0]) = qRegisterMetaType< Qt3DInput::QKeyEvent* >(); break;
            }
            break;
        case 21:
            switch (*reinterpret_cast<int*>(_a[1])) {
            default: *reinterpret_cast<int*>(_a[0]) = -1; break;
            case 0:
                *reinterpret_cast<int*>(_a[0]) = qRegisterMetaType< Qt3DInput::QKeyEvent* >(); break;
            }
            break;
        case 22:
            switch (*reinterpret_cast<int*>(_a[1])) {
            default: *reinterpret_cast<int*>(_a[0]) = -1; break;
            case 0:
                *reinterpret_cast<int*>(_a[0]) = qRegisterMetaType< Qt3DInput::QKeyEvent* >(); break;
            }
            break;
        case 23:
            switch (*reinterpret_cast<int*>(_a[1])) {
            default: *reinterpret_cast<int*>(_a[0]) = -1; break;
            case 0:
                *reinterpret_cast<int*>(_a[0]) = qRegisterMetaType< Qt3DInput::QKeyEvent* >(); break;
            }
            break;
        case 24:
            switch (*reinterpret_cast<int*>(_a[1])) {
            default: *reinterpret_cast<int*>(_a[0]) = -1; break;
            case 0:
                *reinterpret_cast<int*>(_a[0]) = qRegisterMetaType< Qt3DInput::QKeyEvent* >(); break;
            }
            break;
        case 25:
            switch (*reinterpret_cast<int*>(_a[1])) {
            default: *reinterpret_cast<int*>(_a[0]) = -1; break;
            case 0:
                *reinterpret_cast<int*>(_a[0]) = qRegisterMetaType< Qt3DInput::QKeyEvent* >(); break;
            }
            break;
        case 26:
            switch (*reinterpret_cast<int*>(_a[1])) {
            default: *reinterpret_cast<int*>(_a[0]) = -1; break;
            case 0:
                *reinterpret_cast<int*>(_a[0]) = qRegisterMetaType< Qt3DInput::QKeyEvent* >(); break;
            }
            break;
        case 27:
            switch (*reinterpret_cast<int*>(_a[1])) {
            default: *reinterpret_cast<int*>(_a[0]) = -1; break;
            case 0:
                *reinterpret_cast<int*>(_a[0]) = qRegisterMetaType< Qt3DInput::QKeyEvent* >(); break;
            }
            break;
        case 28:
            switch (*reinterpret_cast<int*>(_a[1])) {
            default: *reinterpret_cast<int*>(_a[0]) = -1; break;
            case 0:
                *reinterpret_cast<int*>(_a[0]) = qRegisterMetaType< Qt3DInput::QKeyEvent* >(); break;
            }
            break;
        case 29:
            switch (*reinterpret_cast<int*>(_a[1])) {
            default: *reinterpret_cast<int*>(_a[0]) = -1; break;
            case 0:
                *reinterpret_cast<int*>(_a[0]) = qRegisterMetaType< Qt3DInput::QKeyEvent* >(); break;
            }
            break;
        case 30:
            switch (*reinterpret_cast<int*>(_a[1])) {
            default: *reinterpret_cast<int*>(_a[0]) = -1; break;
            case 0:
                *reinterpret_cast<int*>(_a[0]) = qRegisterMetaType< Qt3DInput::QKeyEvent* >(); break;
            }
            break;
        case 31:
            switch (*reinterpret_cast<int*>(_a[1])) {
            default: *reinterpret_cast<int*>(_a[0]) = -1; break;
            case 0:
                *reinterpret_cast<int*>(_a[0]) = qRegisterMetaType< Qt3DInput::QKeyEvent* >(); break;
            }
            break;
        case 32:
            switch (*reinterpret_cast<int*>(_a[1])) {
            default: *reinterpret_cast<int*>(_a[0]) = -1; break;
            case 0:
                *reinterpret_cast<int*>(_a[0]) = qRegisterMetaType< Qt3DInput::QKeyEvent* >(); break;
            }
            break;
        case 33:
            switch (*reinterpret_cast<int*>(_a[1])) {
            default: *reinterpret_cast<int*>(_a[0]) = -1; break;
            case 0:
                *reinterpret_cast<int*>(_a[0]) = qRegisterMetaType< Qt3DInput::QKeyEvent* >(); break;
            }
            break;
        case 34:
            switch (*reinterpret_cast<int*>(_a[1])) {
            default: *reinterpret_cast<int*>(_a[0]) = -1; break;
            case 0:
                *reinterpret_cast<int*>(_a[0]) = qRegisterMetaType< Qt3DInput::QKeyEvent* >(); break;
            }
            break;
        case 35:
            switch (*reinterpret_cast<int*>(_a[1])) {
            default: *reinterpret_cast<int*>(_a[0]) = -1; break;
            case 0:
                *reinterpret_cast<int*>(_a[0]) = qRegisterMetaType< Qt3DInput::QKeyEvent* >(); break;
            }
            break;
        case 36:
            switch (*reinterpret_cast<int*>(_a[1])) {
            default: *reinterpret_cast<int*>(_a[0]) = -1; break;
            case 0:
                *reinterpret_cast<int*>(_a[0]) = qRegisterMetaType< Qt3DInput::QKeyEvent* >(); break;
            }
            break;
        case 37:
            switch (*reinterpret_cast<int*>(_a[1])) {
            default: *reinterpret_cast<int*>(_a[0]) = -1; break;
            case 0:
                *reinterpret_cast<int*>(_a[0]) = qRegisterMetaType< Qt3DInput::QKeyEvent* >(); break;
            }
            break;
        case 38:
            switch (*reinterpret_cast<int*>(_a[1])) {
            default: *reinterpret_cast<int*>(_a[0]) = -1; break;
            case 0:
                *reinterpret_cast<int*>(_a[0]) = qRegisterMetaType< Qt3DInput::QKeyEvent* >(); break;
            }
            break;
        case 39:
            switch (*reinterpret_cast<int*>(_a[1])) {
            default: *reinterpret_cast<int*>(_a[0]) = -1; break;
            case 0:
                *reinterpret_cast<int*>(_a[0]) = qRegisterMetaType< Qt3DInput::QKeyEvent* >(); break;
            }
            break;
        case 40:
            switch (*reinterpret_cast<int*>(_a[1])) {
            default: *reinterpret_cast<int*>(_a[0]) = -1; break;
            case 0:
                *reinterpret_cast<int*>(_a[0]) = qRegisterMetaType< Qt3DInput::QKeyEvent* >(); break;
            }
            break;
        case 41:
            switch (*reinterpret_cast<int*>(_a[1])) {
            default: *reinterpret_cast<int*>(_a[0]) = -1; break;
            case 0:
                *reinterpret_cast<int*>(_a[0]) = qRegisterMetaType< Qt3DInput::QKeyEvent* >(); break;
            }
            break;
        }
    } else if (_c == QMetaObject::IndexOfMethod) {
        int *result = reinterpret_cast<int *>(_a[0]);
        void **func = reinterpret_cast<void **>(_a[1]);
        {
            typedef void (QKeyboardInput::*_t)(QKeyboardController * );
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&QKeyboardInput::controllerChanged)) {
                *result = 0;
                return;
            }
        }
        {
            typedef void (QKeyboardInput::*_t)(bool );
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&QKeyboardInput::focusChanged)) {
                *result = 1;
                return;
            }
        }
        {
            typedef void (QKeyboardInput::*_t)(Qt3DInput::QKeyEvent * );
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&QKeyboardInput::digit0Pressed)) {
                *result = 2;
                return;
            }
        }
        {
            typedef void (QKeyboardInput::*_t)(Qt3DInput::QKeyEvent * );
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&QKeyboardInput::digit1Pressed)) {
                *result = 3;
                return;
            }
        }
        {
            typedef void (QKeyboardInput::*_t)(Qt3DInput::QKeyEvent * );
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&QKeyboardInput::digit2Pressed)) {
                *result = 4;
                return;
            }
        }
        {
            typedef void (QKeyboardInput::*_t)(Qt3DInput::QKeyEvent * );
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&QKeyboardInput::digit3Pressed)) {
                *result = 5;
                return;
            }
        }
        {
            typedef void (QKeyboardInput::*_t)(Qt3DInput::QKeyEvent * );
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&QKeyboardInput::digit4Pressed)) {
                *result = 6;
                return;
            }
        }
        {
            typedef void (QKeyboardInput::*_t)(Qt3DInput::QKeyEvent * );
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&QKeyboardInput::digit5Pressed)) {
                *result = 7;
                return;
            }
        }
        {
            typedef void (QKeyboardInput::*_t)(Qt3DInput::QKeyEvent * );
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&QKeyboardInput::digit6Pressed)) {
                *result = 8;
                return;
            }
        }
        {
            typedef void (QKeyboardInput::*_t)(Qt3DInput::QKeyEvent * );
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&QKeyboardInput::digit7Pressed)) {
                *result = 9;
                return;
            }
        }
        {
            typedef void (QKeyboardInput::*_t)(Qt3DInput::QKeyEvent * );
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&QKeyboardInput::digit8Pressed)) {
                *result = 10;
                return;
            }
        }
        {
            typedef void (QKeyboardInput::*_t)(Qt3DInput::QKeyEvent * );
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&QKeyboardInput::digit9Pressed)) {
                *result = 11;
                return;
            }
        }
        {
            typedef void (QKeyboardInput::*_t)(Qt3DInput::QKeyEvent * );
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&QKeyboardInput::leftPressed)) {
                *result = 12;
                return;
            }
        }
        {
            typedef void (QKeyboardInput::*_t)(Qt3DInput::QKeyEvent * );
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&QKeyboardInput::rightPressed)) {
                *result = 13;
                return;
            }
        }
        {
            typedef void (QKeyboardInput::*_t)(Qt3DInput::QKeyEvent * );
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&QKeyboardInput::upPressed)) {
                *result = 14;
                return;
            }
        }
        {
            typedef void (QKeyboardInput::*_t)(Qt3DInput::QKeyEvent * );
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&QKeyboardInput::downPressed)) {
                *result = 15;
                return;
            }
        }
        {
            typedef void (QKeyboardInput::*_t)(Qt3DInput::QKeyEvent * );
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&QKeyboardInput::tabPressed)) {
                *result = 16;
                return;
            }
        }
        {
            typedef void (QKeyboardInput::*_t)(Qt3DInput::QKeyEvent * );
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&QKeyboardInput::backtabPressed)) {
                *result = 17;
                return;
            }
        }
        {
            typedef void (QKeyboardInput::*_t)(Qt3DInput::QKeyEvent * );
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&QKeyboardInput::asteriskPressed)) {
                *result = 18;
                return;
            }
        }
        {
            typedef void (QKeyboardInput::*_t)(Qt3DInput::QKeyEvent * );
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&QKeyboardInput::numberSignPressed)) {
                *result = 19;
                return;
            }
        }
        {
            typedef void (QKeyboardInput::*_t)(Qt3DInput::QKeyEvent * );
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&QKeyboardInput::escapePressed)) {
                *result = 20;
                return;
            }
        }
        {
            typedef void (QKeyboardInput::*_t)(Qt3DInput::QKeyEvent * );
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&QKeyboardInput::returnPressed)) {
                *result = 21;
                return;
            }
        }
        {
            typedef void (QKeyboardInput::*_t)(Qt3DInput::QKeyEvent * );
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&QKeyboardInput::enterPressed)) {
                *result = 22;
                return;
            }
        }
        {
            typedef void (QKeyboardInput::*_t)(Qt3DInput::QKeyEvent * );
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&QKeyboardInput::deletePressed)) {
                *result = 23;
                return;
            }
        }
        {
            typedef void (QKeyboardInput::*_t)(Qt3DInput::QKeyEvent * );
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&QKeyboardInput::spacePressed)) {
                *result = 24;
                return;
            }
        }
        {
            typedef void (QKeyboardInput::*_t)(Qt3DInput::QKeyEvent * );
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&QKeyboardInput::backPressed)) {
                *result = 25;
                return;
            }
        }
        {
            typedef void (QKeyboardInput::*_t)(Qt3DInput::QKeyEvent * );
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&QKeyboardInput::cancelPressed)) {
                *result = 26;
                return;
            }
        }
        {
            typedef void (QKeyboardInput::*_t)(Qt3DInput::QKeyEvent * );
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&QKeyboardInput::selectPressed)) {
                *result = 27;
                return;
            }
        }
        {
            typedef void (QKeyboardInput::*_t)(Qt3DInput::QKeyEvent * );
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&QKeyboardInput::yesPressed)) {
                *result = 28;
                return;
            }
        }
        {
            typedef void (QKeyboardInput::*_t)(Qt3DInput::QKeyEvent * );
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&QKeyboardInput::noPressed)) {
                *result = 29;
                return;
            }
        }
        {
            typedef void (QKeyboardInput::*_t)(Qt3DInput::QKeyEvent * );
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&QKeyboardInput::context1Pressed)) {
                *result = 30;
                return;
            }
        }
        {
            typedef void (QKeyboardInput::*_t)(Qt3DInput::QKeyEvent * );
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&QKeyboardInput::context2Pressed)) {
                *result = 31;
                return;
            }
        }
        {
            typedef void (QKeyboardInput::*_t)(Qt3DInput::QKeyEvent * );
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&QKeyboardInput::context3Pressed)) {
                *result = 32;
                return;
            }
        }
        {
            typedef void (QKeyboardInput::*_t)(Qt3DInput::QKeyEvent * );
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&QKeyboardInput::context4Pressed)) {
                *result = 33;
                return;
            }
        }
        {
            typedef void (QKeyboardInput::*_t)(Qt3DInput::QKeyEvent * );
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&QKeyboardInput::callPressed)) {
                *result = 34;
                return;
            }
        }
        {
            typedef void (QKeyboardInput::*_t)(Qt3DInput::QKeyEvent * );
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&QKeyboardInput::hangupPressed)) {
                *result = 35;
                return;
            }
        }
        {
            typedef void (QKeyboardInput::*_t)(Qt3DInput::QKeyEvent * );
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&QKeyboardInput::flipPressed)) {
                *result = 36;
                return;
            }
        }
        {
            typedef void (QKeyboardInput::*_t)(Qt3DInput::QKeyEvent * );
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&QKeyboardInput::menuPressed)) {
                *result = 37;
                return;
            }
        }
        {
            typedef void (QKeyboardInput::*_t)(Qt3DInput::QKeyEvent * );
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&QKeyboardInput::volumeUpPressed)) {
                *result = 38;
                return;
            }
        }
        {
            typedef void (QKeyboardInput::*_t)(Qt3DInput::QKeyEvent * );
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&QKeyboardInput::volumeDownPressed)) {
                *result = 39;
                return;
            }
        }
        {
            typedef void (QKeyboardInput::*_t)(Qt3DInput::QKeyEvent * );
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&QKeyboardInput::pressed)) {
                *result = 40;
                return;
            }
        }
        {
            typedef void (QKeyboardInput::*_t)(Qt3DInput::QKeyEvent * );
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&QKeyboardInput::released)) {
                *result = 41;
                return;
            }
        }
    }
#ifndef QT_NO_PROPERTIES
    else if (_c == QMetaObject::ReadProperty) {
        QKeyboardInput *_t = static_cast<QKeyboardInput *>(_o);
        Q_UNUSED(_t)
        void *_v = _a[0];
        switch (_id) {
        case 0: *reinterpret_cast< Qt3DInput::QKeyboardController**>(_v) = _t->controller(); break;
        case 1: *reinterpret_cast< bool*>(_v) = _t->focus(); break;
        default: break;
        }
    } else if (_c == QMetaObject::WriteProperty) {
        QKeyboardInput *_t = static_cast<QKeyboardInput *>(_o);
        Q_UNUSED(_t)
        void *_v = _a[0];
        switch (_id) {
        case 0: _t->setController(*reinterpret_cast< Qt3DInput::QKeyboardController**>(_v)); break;
        case 1: _t->setFocus(*reinterpret_cast< bool*>(_v)); break;
        default: break;
        }
    } else if (_c == QMetaObject::ResetProperty) {
    }
#endif // QT_NO_PROPERTIES
}

const QMetaObject Qt3DInput::QKeyboardInput::staticMetaObject = {
    { &Qt3DCore::QComponent::staticMetaObject, qt_meta_stringdata_Qt3DInput__QKeyboardInput.data,
      qt_meta_data_Qt3DInput__QKeyboardInput,  qt_static_metacall, Q_NULLPTR, Q_NULLPTR}
};


const QMetaObject *Qt3DInput::QKeyboardInput::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->dynamicMetaObject() : &staticMetaObject;
}

void *Qt3DInput::QKeyboardInput::qt_metacast(const char *_clname)
{
    if (!_clname) return Q_NULLPTR;
    if (!strcmp(_clname, qt_meta_stringdata_Qt3DInput__QKeyboardInput.stringdata0))
        return static_cast<void*>(const_cast< QKeyboardInput*>(this));
    return Qt3DCore::QComponent::qt_metacast(_clname);
}

int Qt3DInput::QKeyboardInput::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = Qt3DCore::QComponent::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 44)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 44;
    } else if (_c == QMetaObject::RegisterMethodArgumentMetaType) {
        if (_id < 44)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 44;
    }
#ifndef QT_NO_PROPERTIES
   else if (_c == QMetaObject::ReadProperty || _c == QMetaObject::WriteProperty
            || _c == QMetaObject::ResetProperty || _c == QMetaObject::RegisterPropertyMetaType) {
        qt_static_metacall(this, _c, _id, _a);
        _id -= 2;
    } else if (_c == QMetaObject::QueryPropertyDesignable) {
        _id -= 2;
    } else if (_c == QMetaObject::QueryPropertyScriptable) {
        _id -= 2;
    } else if (_c == QMetaObject::QueryPropertyStored) {
        _id -= 2;
    } else if (_c == QMetaObject::QueryPropertyEditable) {
        _id -= 2;
    } else if (_c == QMetaObject::QueryPropertyUser) {
        _id -= 2;
    }
#endif // QT_NO_PROPERTIES
    return _id;
}

// SIGNAL 0
void Qt3DInput::QKeyboardInput::controllerChanged(QKeyboardController * _t1)
{
    void *_a[] = { Q_NULLPTR, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 0, _a);
}

// SIGNAL 1
void Qt3DInput::QKeyboardInput::focusChanged(bool _t1)
{
    void *_a[] = { Q_NULLPTR, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 1, _a);
}

// SIGNAL 2
void Qt3DInput::QKeyboardInput::digit0Pressed(Qt3DInput::QKeyEvent * _t1)
{
    void *_a[] = { Q_NULLPTR, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 2, _a);
}

// SIGNAL 3
void Qt3DInput::QKeyboardInput::digit1Pressed(Qt3DInput::QKeyEvent * _t1)
{
    void *_a[] = { Q_NULLPTR, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 3, _a);
}

// SIGNAL 4
void Qt3DInput::QKeyboardInput::digit2Pressed(Qt3DInput::QKeyEvent * _t1)
{
    void *_a[] = { Q_NULLPTR, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 4, _a);
}

// SIGNAL 5
void Qt3DInput::QKeyboardInput::digit3Pressed(Qt3DInput::QKeyEvent * _t1)
{
    void *_a[] = { Q_NULLPTR, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 5, _a);
}

// SIGNAL 6
void Qt3DInput::QKeyboardInput::digit4Pressed(Qt3DInput::QKeyEvent * _t1)
{
    void *_a[] = { Q_NULLPTR, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 6, _a);
}

// SIGNAL 7
void Qt3DInput::QKeyboardInput::digit5Pressed(Qt3DInput::QKeyEvent * _t1)
{
    void *_a[] = { Q_NULLPTR, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 7, _a);
}

// SIGNAL 8
void Qt3DInput::QKeyboardInput::digit6Pressed(Qt3DInput::QKeyEvent * _t1)
{
    void *_a[] = { Q_NULLPTR, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 8, _a);
}

// SIGNAL 9
void Qt3DInput::QKeyboardInput::digit7Pressed(Qt3DInput::QKeyEvent * _t1)
{
    void *_a[] = { Q_NULLPTR, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 9, _a);
}

// SIGNAL 10
void Qt3DInput::QKeyboardInput::digit8Pressed(Qt3DInput::QKeyEvent * _t1)
{
    void *_a[] = { Q_NULLPTR, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 10, _a);
}

// SIGNAL 11
void Qt3DInput::QKeyboardInput::digit9Pressed(Qt3DInput::QKeyEvent * _t1)
{
    void *_a[] = { Q_NULLPTR, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 11, _a);
}

// SIGNAL 12
void Qt3DInput::QKeyboardInput::leftPressed(Qt3DInput::QKeyEvent * _t1)
{
    void *_a[] = { Q_NULLPTR, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 12, _a);
}

// SIGNAL 13
void Qt3DInput::QKeyboardInput::rightPressed(Qt3DInput::QKeyEvent * _t1)
{
    void *_a[] = { Q_NULLPTR, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 13, _a);
}

// SIGNAL 14
void Qt3DInput::QKeyboardInput::upPressed(Qt3DInput::QKeyEvent * _t1)
{
    void *_a[] = { Q_NULLPTR, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 14, _a);
}

// SIGNAL 15
void Qt3DInput::QKeyboardInput::downPressed(Qt3DInput::QKeyEvent * _t1)
{
    void *_a[] = { Q_NULLPTR, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 15, _a);
}

// SIGNAL 16
void Qt3DInput::QKeyboardInput::tabPressed(Qt3DInput::QKeyEvent * _t1)
{
    void *_a[] = { Q_NULLPTR, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 16, _a);
}

// SIGNAL 17
void Qt3DInput::QKeyboardInput::backtabPressed(Qt3DInput::QKeyEvent * _t1)
{
    void *_a[] = { Q_NULLPTR, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 17, _a);
}

// SIGNAL 18
void Qt3DInput::QKeyboardInput::asteriskPressed(Qt3DInput::QKeyEvent * _t1)
{
    void *_a[] = { Q_NULLPTR, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 18, _a);
}

// SIGNAL 19
void Qt3DInput::QKeyboardInput::numberSignPressed(Qt3DInput::QKeyEvent * _t1)
{
    void *_a[] = { Q_NULLPTR, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 19, _a);
}

// SIGNAL 20
void Qt3DInput::QKeyboardInput::escapePressed(Qt3DInput::QKeyEvent * _t1)
{
    void *_a[] = { Q_NULLPTR, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 20, _a);
}

// SIGNAL 21
void Qt3DInput::QKeyboardInput::returnPressed(Qt3DInput::QKeyEvent * _t1)
{
    void *_a[] = { Q_NULLPTR, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 21, _a);
}

// SIGNAL 22
void Qt3DInput::QKeyboardInput::enterPressed(Qt3DInput::QKeyEvent * _t1)
{
    void *_a[] = { Q_NULLPTR, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 22, _a);
}

// SIGNAL 23
void Qt3DInput::QKeyboardInput::deletePressed(Qt3DInput::QKeyEvent * _t1)
{
    void *_a[] = { Q_NULLPTR, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 23, _a);
}

// SIGNAL 24
void Qt3DInput::QKeyboardInput::spacePressed(Qt3DInput::QKeyEvent * _t1)
{
    void *_a[] = { Q_NULLPTR, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 24, _a);
}

// SIGNAL 25
void Qt3DInput::QKeyboardInput::backPressed(Qt3DInput::QKeyEvent * _t1)
{
    void *_a[] = { Q_NULLPTR, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 25, _a);
}

// SIGNAL 26
void Qt3DInput::QKeyboardInput::cancelPressed(Qt3DInput::QKeyEvent * _t1)
{
    void *_a[] = { Q_NULLPTR, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 26, _a);
}

// SIGNAL 27
void Qt3DInput::QKeyboardInput::selectPressed(Qt3DInput::QKeyEvent * _t1)
{
    void *_a[] = { Q_NULLPTR, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 27, _a);
}

// SIGNAL 28
void Qt3DInput::QKeyboardInput::yesPressed(Qt3DInput::QKeyEvent * _t1)
{
    void *_a[] = { Q_NULLPTR, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 28, _a);
}

// SIGNAL 29
void Qt3DInput::QKeyboardInput::noPressed(Qt3DInput::QKeyEvent * _t1)
{
    void *_a[] = { Q_NULLPTR, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 29, _a);
}

// SIGNAL 30
void Qt3DInput::QKeyboardInput::context1Pressed(Qt3DInput::QKeyEvent * _t1)
{
    void *_a[] = { Q_NULLPTR, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 30, _a);
}

// SIGNAL 31
void Qt3DInput::QKeyboardInput::context2Pressed(Qt3DInput::QKeyEvent * _t1)
{
    void *_a[] = { Q_NULLPTR, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 31, _a);
}

// SIGNAL 32
void Qt3DInput::QKeyboardInput::context3Pressed(Qt3DInput::QKeyEvent * _t1)
{
    void *_a[] = { Q_NULLPTR, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 32, _a);
}

// SIGNAL 33
void Qt3DInput::QKeyboardInput::context4Pressed(Qt3DInput::QKeyEvent * _t1)
{
    void *_a[] = { Q_NULLPTR, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 33, _a);
}

// SIGNAL 34
void Qt3DInput::QKeyboardInput::callPressed(Qt3DInput::QKeyEvent * _t1)
{
    void *_a[] = { Q_NULLPTR, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 34, _a);
}

// SIGNAL 35
void Qt3DInput::QKeyboardInput::hangupPressed(Qt3DInput::QKeyEvent * _t1)
{
    void *_a[] = { Q_NULLPTR, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 35, _a);
}

// SIGNAL 36
void Qt3DInput::QKeyboardInput::flipPressed(Qt3DInput::QKeyEvent * _t1)
{
    void *_a[] = { Q_NULLPTR, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 36, _a);
}

// SIGNAL 37
void Qt3DInput::QKeyboardInput::menuPressed(Qt3DInput::QKeyEvent * _t1)
{
    void *_a[] = { Q_NULLPTR, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 37, _a);
}

// SIGNAL 38
void Qt3DInput::QKeyboardInput::volumeUpPressed(Qt3DInput::QKeyEvent * _t1)
{
    void *_a[] = { Q_NULLPTR, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 38, _a);
}

// SIGNAL 39
void Qt3DInput::QKeyboardInput::volumeDownPressed(Qt3DInput::QKeyEvent * _t1)
{
    void *_a[] = { Q_NULLPTR, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 39, _a);
}

// SIGNAL 40
void Qt3DInput::QKeyboardInput::pressed(Qt3DInput::QKeyEvent * _t1)
{
    void *_a[] = { Q_NULLPTR, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 40, _a);
}

// SIGNAL 41
void Qt3DInput::QKeyboardInput::released(Qt3DInput::QKeyEvent * _t1)
{
    void *_a[] = { Q_NULLPTR, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 41, _a);
}
QT_END_MOC_NAMESPACE
