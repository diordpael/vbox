/****************************************************************************
** Meta object code from reading C++ file 'qcameralens.h'
**
** Created by: The Qt Meta Object Compiler version 67 (Qt 5.6.3)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "../../core-components/qcameralens.h"
#include <QtCore/qbytearray.h>
#include <QtCore/qmetatype.h>
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'qcameralens.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 67
#error "This file was generated using the moc from 5.6.3. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
struct qt_meta_stringdata_Qt3DCore__QCameraLens_t {
    QByteArrayData data[36];
    char stringdata0[500];
};
#define QT_MOC_LITERAL(idx, ofs, len) \
    Q_STATIC_BYTE_ARRAY_DATA_HEADER_INITIALIZER_WITH_OFFSET(len, \
    qptrdiff(offsetof(qt_meta_stringdata_Qt3DCore__QCameraLens_t, stringdata0) + ofs \
        - idx * sizeof(QByteArrayData)) \
    )
static const qt_meta_stringdata_Qt3DCore__QCameraLens_t qt_meta_stringdata_Qt3DCore__QCameraLens = {
    {
QT_MOC_LITERAL(0, 0, 21), // "Qt3DCore::QCameraLens"
QT_MOC_LITERAL(1, 22, 21), // "projectionTypeChanged"
QT_MOC_LITERAL(2, 44, 0), // ""
QT_MOC_LITERAL(3, 45, 27), // "QCameraLens::ProjectionType"
QT_MOC_LITERAL(4, 73, 14), // "projectionType"
QT_MOC_LITERAL(5, 88, 16), // "nearPlaneChanged"
QT_MOC_LITERAL(6, 105, 9), // "nearPlane"
QT_MOC_LITERAL(7, 115, 15), // "farPlaneChanged"
QT_MOC_LITERAL(8, 131, 8), // "farPlane"
QT_MOC_LITERAL(9, 140, 18), // "fieldOfViewChanged"
QT_MOC_LITERAL(10, 159, 11), // "fieldOfView"
QT_MOC_LITERAL(11, 171, 18), // "aspectRatioChanged"
QT_MOC_LITERAL(12, 190, 11), // "aspectRatio"
QT_MOC_LITERAL(13, 202, 11), // "leftChanged"
QT_MOC_LITERAL(14, 214, 4), // "left"
QT_MOC_LITERAL(15, 219, 12), // "rightChanged"
QT_MOC_LITERAL(16, 232, 5), // "right"
QT_MOC_LITERAL(17, 238, 13), // "bottomChanged"
QT_MOC_LITERAL(18, 252, 6), // "bottom"
QT_MOC_LITERAL(19, 259, 10), // "topChanged"
QT_MOC_LITERAL(20, 270, 3), // "top"
QT_MOC_LITERAL(21, 274, 23), // "projectionMatrixChanged"
QT_MOC_LITERAL(22, 298, 16), // "projectionMatrix"
QT_MOC_LITERAL(23, 315, 17), // "setProjectionType"
QT_MOC_LITERAL(24, 333, 14), // "ProjectionType"
QT_MOC_LITERAL(25, 348, 12), // "setNearPlane"
QT_MOC_LITERAL(26, 361, 11), // "setFarPlane"
QT_MOC_LITERAL(27, 373, 14), // "setFieldOfView"
QT_MOC_LITERAL(28, 388, 14), // "setAspectRatio"
QT_MOC_LITERAL(29, 403, 7), // "setLeft"
QT_MOC_LITERAL(30, 411, 8), // "setRight"
QT_MOC_LITERAL(31, 420, 9), // "setBottom"
QT_MOC_LITERAL(32, 430, 6), // "setTop"
QT_MOC_LITERAL(33, 437, 22), // "OrthographicProjection"
QT_MOC_LITERAL(34, 460, 21), // "PerspectiveProjection"
QT_MOC_LITERAL(35, 482, 17) // "FrustumProjection"

    },
    "Qt3DCore::QCameraLens\0projectionTypeChanged\0"
    "\0QCameraLens::ProjectionType\0"
    "projectionType\0nearPlaneChanged\0"
    "nearPlane\0farPlaneChanged\0farPlane\0"
    "fieldOfViewChanged\0fieldOfView\0"
    "aspectRatioChanged\0aspectRatio\0"
    "leftChanged\0left\0rightChanged\0right\0"
    "bottomChanged\0bottom\0topChanged\0top\0"
    "projectionMatrixChanged\0projectionMatrix\0"
    "setProjectionType\0ProjectionType\0"
    "setNearPlane\0setFarPlane\0setFieldOfView\0"
    "setAspectRatio\0setLeft\0setRight\0"
    "setBottom\0setTop\0OrthographicProjection\0"
    "PerspectiveProjection\0FrustumProjection"
};
#undef QT_MOC_LITERAL

static const uint qt_meta_data_Qt3DCore__QCameraLens[] = {

 // content:
       7,       // revision
       0,       // classname
       0,    0, // classinfo
      19,   14, // methods
      10,  166, // properties
       1,  206, // enums/sets
       0,    0, // constructors
       0,       // flags
      10,       // signalCount

 // signals: name, argc, parameters, tag, flags
       1,    1,  109,    2, 0x06 /* Public */,
       5,    1,  112,    2, 0x06 /* Public */,
       7,    1,  115,    2, 0x06 /* Public */,
       9,    1,  118,    2, 0x06 /* Public */,
      11,    1,  121,    2, 0x06 /* Public */,
      13,    1,  124,    2, 0x06 /* Public */,
      15,    1,  127,    2, 0x06 /* Public */,
      17,    1,  130,    2, 0x06 /* Public */,
      19,    1,  133,    2, 0x06 /* Public */,
      21,    1,  136,    2, 0x06 /* Public */,

 // slots: name, argc, parameters, tag, flags
      23,    1,  139,    2, 0x0a /* Public */,
      25,    1,  142,    2, 0x0a /* Public */,
      26,    1,  145,    2, 0x0a /* Public */,
      27,    1,  148,    2, 0x0a /* Public */,
      28,    1,  151,    2, 0x0a /* Public */,
      29,    1,  154,    2, 0x0a /* Public */,
      30,    1,  157,    2, 0x0a /* Public */,
      31,    1,  160,    2, 0x0a /* Public */,
      32,    1,  163,    2, 0x0a /* Public */,

 // signals: parameters
    QMetaType::Void, 0x80000000 | 3,    4,
    QMetaType::Void, QMetaType::Float,    6,
    QMetaType::Void, QMetaType::Float,    8,
    QMetaType::Void, QMetaType::Float,   10,
    QMetaType::Void, QMetaType::Float,   12,
    QMetaType::Void, QMetaType::Float,   14,
    QMetaType::Void, QMetaType::Float,   16,
    QMetaType::Void, QMetaType::Float,   18,
    QMetaType::Void, QMetaType::Float,   20,
    QMetaType::Void, QMetaType::QMatrix4x4,   22,

 // slots: parameters
    QMetaType::Void, 0x80000000 | 24,    4,
    QMetaType::Void, QMetaType::Float,    6,
    QMetaType::Void, QMetaType::Float,    8,
    QMetaType::Void, QMetaType::Float,   10,
    QMetaType::Void, QMetaType::Float,   12,
    QMetaType::Void, QMetaType::Float,   14,
    QMetaType::Void, QMetaType::Float,   16,
    QMetaType::Void, QMetaType::Float,   18,
    QMetaType::Void, QMetaType::Float,   20,

 // properties: name, type, flags
       4, 0x80000000 | 24, 0x0049510b,
       6, QMetaType::Float, 0x00495103,
       8, QMetaType::Float, 0x00495103,
      10, QMetaType::Float, 0x00495103,
      12, QMetaType::Float, 0x00495103,
      14, QMetaType::Float, 0x00495103,
      16, QMetaType::Float, 0x00495103,
      18, QMetaType::Float, 0x00495103,
      20, QMetaType::Float, 0x00495103,
      22, QMetaType::QMatrix4x4, 0x00495001,

 // properties: notify_signal_id
       0,
       1,
       2,
       3,
       4,
       5,
       6,
       7,
       8,
       9,

 // enums: name, flags, count, data
      24, 0x0,    3,  210,

 // enum data: key, value
      33, uint(Qt3DCore::QCameraLens::OrthographicProjection),
      34, uint(Qt3DCore::QCameraLens::PerspectiveProjection),
      35, uint(Qt3DCore::QCameraLens::FrustumProjection),

       0        // eod
};

void Qt3DCore::QCameraLens::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        QCameraLens *_t = static_cast<QCameraLens *>(_o);
        Q_UNUSED(_t)
        switch (_id) {
        case 0: _t->projectionTypeChanged((*reinterpret_cast< QCameraLens::ProjectionType(*)>(_a[1]))); break;
        case 1: _t->nearPlaneChanged((*reinterpret_cast< float(*)>(_a[1]))); break;
        case 2: _t->farPlaneChanged((*reinterpret_cast< float(*)>(_a[1]))); break;
        case 3: _t->fieldOfViewChanged((*reinterpret_cast< float(*)>(_a[1]))); break;
        case 4: _t->aspectRatioChanged((*reinterpret_cast< float(*)>(_a[1]))); break;
        case 5: _t->leftChanged((*reinterpret_cast< float(*)>(_a[1]))); break;
        case 6: _t->rightChanged((*reinterpret_cast< float(*)>(_a[1]))); break;
        case 7: _t->bottomChanged((*reinterpret_cast< float(*)>(_a[1]))); break;
        case 8: _t->topChanged((*reinterpret_cast< float(*)>(_a[1]))); break;
        case 9: _t->projectionMatrixChanged((*reinterpret_cast< const QMatrix4x4(*)>(_a[1]))); break;
        case 10: _t->setProjectionType((*reinterpret_cast< ProjectionType(*)>(_a[1]))); break;
        case 11: _t->setNearPlane((*reinterpret_cast< float(*)>(_a[1]))); break;
        case 12: _t->setFarPlane((*reinterpret_cast< float(*)>(_a[1]))); break;
        case 13: _t->setFieldOfView((*reinterpret_cast< float(*)>(_a[1]))); break;
        case 14: _t->setAspectRatio((*reinterpret_cast< float(*)>(_a[1]))); break;
        case 15: _t->setLeft((*reinterpret_cast< float(*)>(_a[1]))); break;
        case 16: _t->setRight((*reinterpret_cast< float(*)>(_a[1]))); break;
        case 17: _t->setBottom((*reinterpret_cast< float(*)>(_a[1]))); break;
        case 18: _t->setTop((*reinterpret_cast< float(*)>(_a[1]))); break;
        default: ;
        }
    } else if (_c == QMetaObject::IndexOfMethod) {
        int *result = reinterpret_cast<int *>(_a[0]);
        void **func = reinterpret_cast<void **>(_a[1]);
        {
            typedef void (QCameraLens::*_t)(QCameraLens::ProjectionType );
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&QCameraLens::projectionTypeChanged)) {
                *result = 0;
                return;
            }
        }
        {
            typedef void (QCameraLens::*_t)(float );
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&QCameraLens::nearPlaneChanged)) {
                *result = 1;
                return;
            }
        }
        {
            typedef void (QCameraLens::*_t)(float );
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&QCameraLens::farPlaneChanged)) {
                *result = 2;
                return;
            }
        }
        {
            typedef void (QCameraLens::*_t)(float );
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&QCameraLens::fieldOfViewChanged)) {
                *result = 3;
                return;
            }
        }
        {
            typedef void (QCameraLens::*_t)(float );
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&QCameraLens::aspectRatioChanged)) {
                *result = 4;
                return;
            }
        }
        {
            typedef void (QCameraLens::*_t)(float );
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&QCameraLens::leftChanged)) {
                *result = 5;
                return;
            }
        }
        {
            typedef void (QCameraLens::*_t)(float );
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&QCameraLens::rightChanged)) {
                *result = 6;
                return;
            }
        }
        {
            typedef void (QCameraLens::*_t)(float );
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&QCameraLens::bottomChanged)) {
                *result = 7;
                return;
            }
        }
        {
            typedef void (QCameraLens::*_t)(float );
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&QCameraLens::topChanged)) {
                *result = 8;
                return;
            }
        }
        {
            typedef void (QCameraLens::*_t)(const QMatrix4x4 & );
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&QCameraLens::projectionMatrixChanged)) {
                *result = 9;
                return;
            }
        }
    }
#ifndef QT_NO_PROPERTIES
    else if (_c == QMetaObject::ReadProperty) {
        QCameraLens *_t = static_cast<QCameraLens *>(_o);
        Q_UNUSED(_t)
        void *_v = _a[0];
        switch (_id) {
        case 0: *reinterpret_cast< ProjectionType*>(_v) = _t->projectionType(); break;
        case 1: *reinterpret_cast< float*>(_v) = _t->nearPlane(); break;
        case 2: *reinterpret_cast< float*>(_v) = _t->farPlane(); break;
        case 3: *reinterpret_cast< float*>(_v) = _t->fieldOfView(); break;
        case 4: *reinterpret_cast< float*>(_v) = _t->aspectRatio(); break;
        case 5: *reinterpret_cast< float*>(_v) = _t->left(); break;
        case 6: *reinterpret_cast< float*>(_v) = _t->right(); break;
        case 7: *reinterpret_cast< float*>(_v) = _t->bottom(); break;
        case 8: *reinterpret_cast< float*>(_v) = _t->top(); break;
        case 9: *reinterpret_cast< QMatrix4x4*>(_v) = _t->projectionMatrix(); break;
        default: break;
        }
    } else if (_c == QMetaObject::WriteProperty) {
        QCameraLens *_t = static_cast<QCameraLens *>(_o);
        Q_UNUSED(_t)
        void *_v = _a[0];
        switch (_id) {
        case 0: _t->setProjectionType(*reinterpret_cast< ProjectionType*>(_v)); break;
        case 1: _t->setNearPlane(*reinterpret_cast< float*>(_v)); break;
        case 2: _t->setFarPlane(*reinterpret_cast< float*>(_v)); break;
        case 3: _t->setFieldOfView(*reinterpret_cast< float*>(_v)); break;
        case 4: _t->setAspectRatio(*reinterpret_cast< float*>(_v)); break;
        case 5: _t->setLeft(*reinterpret_cast< float*>(_v)); break;
        case 6: _t->setRight(*reinterpret_cast< float*>(_v)); break;
        case 7: _t->setBottom(*reinterpret_cast< float*>(_v)); break;
        case 8: _t->setTop(*reinterpret_cast< float*>(_v)); break;
        default: break;
        }
    } else if (_c == QMetaObject::ResetProperty) {
    }
#endif // QT_NO_PROPERTIES
}

const QMetaObject Qt3DCore::QCameraLens::staticMetaObject = {
    { &QComponent::staticMetaObject, qt_meta_stringdata_Qt3DCore__QCameraLens.data,
      qt_meta_data_Qt3DCore__QCameraLens,  qt_static_metacall, Q_NULLPTR, Q_NULLPTR}
};


const QMetaObject *Qt3DCore::QCameraLens::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->dynamicMetaObject() : &staticMetaObject;
}

void *Qt3DCore::QCameraLens::qt_metacast(const char *_clname)
{
    if (!_clname) return Q_NULLPTR;
    if (!strcmp(_clname, qt_meta_stringdata_Qt3DCore__QCameraLens.stringdata0))
        return static_cast<void*>(const_cast< QCameraLens*>(this));
    return QComponent::qt_metacast(_clname);
}

int Qt3DCore::QCameraLens::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QComponent::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 19)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 19;
    } else if (_c == QMetaObject::RegisterMethodArgumentMetaType) {
        if (_id < 19)
            *reinterpret_cast<int*>(_a[0]) = -1;
        _id -= 19;
    }
#ifndef QT_NO_PROPERTIES
   else if (_c == QMetaObject::ReadProperty || _c == QMetaObject::WriteProperty
            || _c == QMetaObject::ResetProperty || _c == QMetaObject::RegisterPropertyMetaType) {
        qt_static_metacall(this, _c, _id, _a);
        _id -= 10;
    } else if (_c == QMetaObject::QueryPropertyDesignable) {
        _id -= 10;
    } else if (_c == QMetaObject::QueryPropertyScriptable) {
        _id -= 10;
    } else if (_c == QMetaObject::QueryPropertyStored) {
        _id -= 10;
    } else if (_c == QMetaObject::QueryPropertyEditable) {
        _id -= 10;
    } else if (_c == QMetaObject::QueryPropertyUser) {
        _id -= 10;
    }
#endif // QT_NO_PROPERTIES
    return _id;
}

// SIGNAL 0
void Qt3DCore::QCameraLens::projectionTypeChanged(QCameraLens::ProjectionType _t1)
{
    void *_a[] = { Q_NULLPTR, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 0, _a);
}

// SIGNAL 1
void Qt3DCore::QCameraLens::nearPlaneChanged(float _t1)
{
    void *_a[] = { Q_NULLPTR, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 1, _a);
}

// SIGNAL 2
void Qt3DCore::QCameraLens::farPlaneChanged(float _t1)
{
    void *_a[] = { Q_NULLPTR, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 2, _a);
}

// SIGNAL 3
void Qt3DCore::QCameraLens::fieldOfViewChanged(float _t1)
{
    void *_a[] = { Q_NULLPTR, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 3, _a);
}

// SIGNAL 4
void Qt3DCore::QCameraLens::aspectRatioChanged(float _t1)
{
    void *_a[] = { Q_NULLPTR, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 4, _a);
}

// SIGNAL 5
void Qt3DCore::QCameraLens::leftChanged(float _t1)
{
    void *_a[] = { Q_NULLPTR, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 5, _a);
}

// SIGNAL 6
void Qt3DCore::QCameraLens::rightChanged(float _t1)
{
    void *_a[] = { Q_NULLPTR, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 6, _a);
}

// SIGNAL 7
void Qt3DCore::QCameraLens::bottomChanged(float _t1)
{
    void *_a[] = { Q_NULLPTR, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 7, _a);
}

// SIGNAL 8
void Qt3DCore::QCameraLens::topChanged(float _t1)
{
    void *_a[] = { Q_NULLPTR, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 8, _a);
}

// SIGNAL 9
void Qt3DCore::QCameraLens::projectionMatrixChanged(const QMatrix4x4 & _t1)
{
    void *_a[] = { Q_NULLPTR, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 9, _a);
}
QT_END_MOC_NAMESPACE
