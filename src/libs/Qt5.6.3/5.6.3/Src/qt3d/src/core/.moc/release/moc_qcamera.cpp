/****************************************************************************
** Meta object code from reading C++ file 'qcamera.h'
**
** Created by: The Qt Meta Object Compiler version 67 (Qt 5.6.3)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "../../core-components/qcamera.h"
#include <QtCore/qbytearray.h>
#include <QtCore/qmetatype.h>
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'qcamera.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 67
#error "This file was generated using the moc from 5.6.3. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
struct qt_meta_stringdata_Qt3DCore__QCamera_t {
    QByteArrayData data[66];
    char stringdata0[865];
};
#define QT_MOC_LITERAL(idx, ofs, len) \
    Q_STATIC_BYTE_ARRAY_DATA_HEADER_INITIALIZER_WITH_OFFSET(len, \
    qptrdiff(offsetof(qt_meta_stringdata_Qt3DCore__QCamera_t, stringdata0) + ofs \
        - idx * sizeof(QByteArrayData)) \
    )
static const qt_meta_stringdata_Qt3DCore__QCamera_t qt_meta_stringdata_Qt3DCore__QCamera = {
    {
QT_MOC_LITERAL(0, 0, 17), // "Qt3DCore::QCamera"
QT_MOC_LITERAL(1, 18, 21), // "projectionTypeChanged"
QT_MOC_LITERAL(2, 40, 0), // ""
QT_MOC_LITERAL(3, 41, 27), // "QCameraLens::ProjectionType"
QT_MOC_LITERAL(4, 69, 14), // "projectionType"
QT_MOC_LITERAL(5, 84, 16), // "nearPlaneChanged"
QT_MOC_LITERAL(6, 101, 9), // "nearPlane"
QT_MOC_LITERAL(7, 111, 15), // "farPlaneChanged"
QT_MOC_LITERAL(8, 127, 8), // "farPlane"
QT_MOC_LITERAL(9, 136, 18), // "fieldOfViewChanged"
QT_MOC_LITERAL(10, 155, 11), // "fieldOfView"
QT_MOC_LITERAL(11, 167, 18), // "aspectRatioChanged"
QT_MOC_LITERAL(12, 186, 11), // "aspectRatio"
QT_MOC_LITERAL(13, 198, 11), // "leftChanged"
QT_MOC_LITERAL(14, 210, 4), // "left"
QT_MOC_LITERAL(15, 215, 12), // "rightChanged"
QT_MOC_LITERAL(16, 228, 5), // "right"
QT_MOC_LITERAL(17, 234, 13), // "bottomChanged"
QT_MOC_LITERAL(18, 248, 6), // "bottom"
QT_MOC_LITERAL(19, 255, 10), // "topChanged"
QT_MOC_LITERAL(20, 266, 3), // "top"
QT_MOC_LITERAL(21, 270, 23), // "projectionMatrixChanged"
QT_MOC_LITERAL(22, 294, 16), // "projectionMatrix"
QT_MOC_LITERAL(23, 311, 15), // "positionChanged"
QT_MOC_LITERAL(24, 327, 8), // "position"
QT_MOC_LITERAL(25, 336, 15), // "upVectorChanged"
QT_MOC_LITERAL(26, 352, 8), // "upVector"
QT_MOC_LITERAL(27, 361, 17), // "viewCenterChanged"
QT_MOC_LITERAL(28, 379, 10), // "viewCenter"
QT_MOC_LITERAL(29, 390, 17), // "viewVectorChanged"
QT_MOC_LITERAL(30, 408, 10), // "viewVector"
QT_MOC_LITERAL(31, 419, 17), // "viewMatrixChanged"
QT_MOC_LITERAL(32, 437, 10), // "viewMatrix"
QT_MOC_LITERAL(33, 448, 17), // "setProjectionType"
QT_MOC_LITERAL(34, 466, 4), // "type"
QT_MOC_LITERAL(35, 471, 12), // "setNearPlane"
QT_MOC_LITERAL(36, 484, 11), // "setFarPlane"
QT_MOC_LITERAL(37, 496, 14), // "setFieldOfView"
QT_MOC_LITERAL(38, 511, 14), // "setAspectRatio"
QT_MOC_LITERAL(39, 526, 7), // "setLeft"
QT_MOC_LITERAL(40, 534, 8), // "setRight"
QT_MOC_LITERAL(41, 543, 9), // "setBottom"
QT_MOC_LITERAL(42, 553, 6), // "setTop"
QT_MOC_LITERAL(43, 560, 11), // "setPosition"
QT_MOC_LITERAL(44, 572, 11), // "setUpVector"
QT_MOC_LITERAL(45, 584, 13), // "setViewCenter"
QT_MOC_LITERAL(46, 598, 9), // "translate"
QT_MOC_LITERAL(47, 608, 6), // "vLocal"
QT_MOC_LITERAL(48, 615, 23), // "CameraTranslationOption"
QT_MOC_LITERAL(49, 639, 6), // "option"
QT_MOC_LITERAL(50, 646, 14), // "translateWorld"
QT_MOC_LITERAL(51, 661, 6), // "vWorld"
QT_MOC_LITERAL(52, 668, 4), // "tilt"
QT_MOC_LITERAL(53, 673, 5), // "angle"
QT_MOC_LITERAL(54, 679, 3), // "pan"
QT_MOC_LITERAL(55, 683, 4), // "axis"
QT_MOC_LITERAL(56, 688, 4), // "roll"
QT_MOC_LITERAL(57, 693, 19), // "tiltAboutViewCenter"
QT_MOC_LITERAL(58, 713, 18), // "panAboutViewCenter"
QT_MOC_LITERAL(59, 732, 19), // "rollAboutViewCenter"
QT_MOC_LITERAL(60, 752, 6), // "rotate"
QT_MOC_LITERAL(61, 759, 1), // "q"
QT_MOC_LITERAL(62, 761, 21), // "rotateAboutViewCenter"
QT_MOC_LITERAL(63, 783, 37), // "Qt3DCore::QCameraLens::Projec..."
QT_MOC_LITERAL(64, 821, 19), // "TranslateViewCenter"
QT_MOC_LITERAL(65, 841, 23) // "DontTranslateViewCenter"

    },
    "Qt3DCore::QCamera\0projectionTypeChanged\0"
    "\0QCameraLens::ProjectionType\0"
    "projectionType\0nearPlaneChanged\0"
    "nearPlane\0farPlaneChanged\0farPlane\0"
    "fieldOfViewChanged\0fieldOfView\0"
    "aspectRatioChanged\0aspectRatio\0"
    "leftChanged\0left\0rightChanged\0right\0"
    "bottomChanged\0bottom\0topChanged\0top\0"
    "projectionMatrixChanged\0projectionMatrix\0"
    "positionChanged\0position\0upVectorChanged\0"
    "upVector\0viewCenterChanged\0viewCenter\0"
    "viewVectorChanged\0viewVector\0"
    "viewMatrixChanged\0viewMatrix\0"
    "setProjectionType\0type\0setNearPlane\0"
    "setFarPlane\0setFieldOfView\0setAspectRatio\0"
    "setLeft\0setRight\0setBottom\0setTop\0"
    "setPosition\0setUpVector\0setViewCenter\0"
    "translate\0vLocal\0CameraTranslationOption\0"
    "option\0translateWorld\0vWorld\0tilt\0"
    "angle\0pan\0axis\0roll\0tiltAboutViewCenter\0"
    "panAboutViewCenter\0rollAboutViewCenter\0"
    "rotate\0q\0rotateAboutViewCenter\0"
    "Qt3DCore::QCameraLens::ProjectionType\0"
    "TranslateViewCenter\0DontTranslateViewCenter"
};
#undef QT_MOC_LITERAL

static const uint qt_meta_data_Qt3DCore__QCamera[] = {

 // content:
       7,       // revision
       0,       // classname
       0,    0, // classinfo
      41,   14, // methods
      15,  350, // properties
       1,  410, // enums/sets
       0,    0, // constructors
       0,       // flags
      15,       // signalCount

 // signals: name, argc, parameters, tag, flags
       1,    1,  219,    2, 0x06 /* Public */,
       5,    1,  222,    2, 0x06 /* Public */,
       7,    1,  225,    2, 0x06 /* Public */,
       9,    1,  228,    2, 0x06 /* Public */,
      11,    1,  231,    2, 0x06 /* Public */,
      13,    1,  234,    2, 0x06 /* Public */,
      15,    1,  237,    2, 0x06 /* Public */,
      17,    1,  240,    2, 0x06 /* Public */,
      19,    1,  243,    2, 0x06 /* Public */,
      21,    1,  246,    2, 0x06 /* Public */,
      23,    1,  249,    2, 0x06 /* Public */,
      25,    1,  252,    2, 0x06 /* Public */,
      27,    1,  255,    2, 0x06 /* Public */,
      29,    1,  258,    2, 0x06 /* Public */,
      31,    1,  261,    2, 0x06 /* Public */,

 // slots: name, argc, parameters, tag, flags
      33,    1,  264,    2, 0x0a /* Public */,
      35,    1,  267,    2, 0x0a /* Public */,
      36,    1,  270,    2, 0x0a /* Public */,
      37,    1,  273,    2, 0x0a /* Public */,
      38,    1,  276,    2, 0x0a /* Public */,
      39,    1,  279,    2, 0x0a /* Public */,
      40,    1,  282,    2, 0x0a /* Public */,
      41,    1,  285,    2, 0x0a /* Public */,
      42,    1,  288,    2, 0x0a /* Public */,
      43,    1,  291,    2, 0x0a /* Public */,
      44,    1,  294,    2, 0x0a /* Public */,
      45,    1,  297,    2, 0x0a /* Public */,

 // methods: name, argc, parameters, tag, flags
      46,    2,  300,    2, 0x02 /* Public */,
      46,    1,  305,    2, 0x22 /* Public | MethodCloned */,
      50,    2,  308,    2, 0x02 /* Public */,
      50,    1,  313,    2, 0x22 /* Public | MethodCloned */,
      52,    1,  316,    2, 0x02 /* Public */,
      54,    1,  319,    2, 0x02 /* Public */,
      54,    2,  322,    2, 0x02 /* Public */,
      56,    1,  327,    2, 0x02 /* Public */,
      57,    1,  330,    2, 0x02 /* Public */,
      58,    1,  333,    2, 0x02 /* Public */,
      58,    2,  336,    2, 0x02 /* Public */,
      59,    1,  341,    2, 0x02 /* Public */,
      60,    1,  344,    2, 0x02 /* Public */,
      62,    1,  347,    2, 0x02 /* Public */,

 // signals: parameters
    QMetaType::Void, 0x80000000 | 3,    4,
    QMetaType::Void, QMetaType::Float,    6,
    QMetaType::Void, QMetaType::Float,    8,
    QMetaType::Void, QMetaType::Float,   10,
    QMetaType::Void, QMetaType::Float,   12,
    QMetaType::Void, QMetaType::Float,   14,
    QMetaType::Void, QMetaType::Float,   16,
    QMetaType::Void, QMetaType::Float,   18,
    QMetaType::Void, QMetaType::Float,   20,
    QMetaType::Void, QMetaType::QMatrix4x4,   22,
    QMetaType::Void, QMetaType::QVector3D,   24,
    QMetaType::Void, QMetaType::QVector3D,   26,
    QMetaType::Void, QMetaType::QVector3D,   28,
    QMetaType::Void, QMetaType::QVector3D,   30,
    QMetaType::Void, QMetaType::QMatrix4x4,   32,

 // slots: parameters
    QMetaType::Void, 0x80000000 | 3,   34,
    QMetaType::Void, QMetaType::Float,    6,
    QMetaType::Void, QMetaType::Float,    8,
    QMetaType::Void, QMetaType::Float,   10,
    QMetaType::Void, QMetaType::Float,   12,
    QMetaType::Void, QMetaType::Float,   14,
    QMetaType::Void, QMetaType::Float,   16,
    QMetaType::Void, QMetaType::Float,   18,
    QMetaType::Void, QMetaType::Float,   20,
    QMetaType::Void, QMetaType::QVector3D,   24,
    QMetaType::Void, QMetaType::QVector3D,   26,
    QMetaType::Void, QMetaType::QVector3D,   28,

 // methods: parameters
    QMetaType::Void, QMetaType::QVector3D, 0x80000000 | 48,   47,   49,
    QMetaType::Void, QMetaType::QVector3D,   47,
    QMetaType::Void, QMetaType::QVector3D, 0x80000000 | 48,   51,   49,
    QMetaType::Void, QMetaType::QVector3D,   51,
    QMetaType::Void, QMetaType::Float,   53,
    QMetaType::Void, QMetaType::Float,   53,
    QMetaType::Void, QMetaType::Float, QMetaType::QVector3D,   53,   55,
    QMetaType::Void, QMetaType::Float,   53,
    QMetaType::Void, QMetaType::Float,   53,
    QMetaType::Void, QMetaType::Float,   53,
    QMetaType::Void, QMetaType::Float, QMetaType::QVector3D,   53,   55,
    QMetaType::Void, QMetaType::Float,   53,
    QMetaType::Void, QMetaType::QQuaternion,   61,
    QMetaType::Void, QMetaType::QQuaternion,   61,

 // properties: name, type, flags
       4, 0x80000000 | 63, 0x0049510b,
       6, QMetaType::Float, 0x00495103,
       8, QMetaType::Float, 0x00495103,
      10, QMetaType::Float, 0x00495103,
      12, QMetaType::Float, 0x00495103,
      14, QMetaType::Float, 0x00495103,
      16, QMetaType::Float, 0x00495103,
      18, QMetaType::Float, 0x00495103,
      20, QMetaType::Float, 0x00495103,
      22, QMetaType::QMatrix4x4, 0x00495001,
      24, QMetaType::QVector3D, 0x00495103,
      26, QMetaType::QVector3D, 0x00495103,
      28, QMetaType::QVector3D, 0x00495103,
      30, QMetaType::QVector3D, 0x00495001,
      32, QMetaType::QMatrix4x4, 0x00495001,

 // properties: notify_signal_id
       0,
       1,
       2,
       3,
       4,
       5,
       6,
       7,
       8,
       9,
      10,
      11,
      12,
      13,
      14,

 // enums: name, flags, count, data
      48, 0x0,    2,  414,

 // enum data: key, value
      64, uint(Qt3DCore::QCamera::TranslateViewCenter),
      65, uint(Qt3DCore::QCamera::DontTranslateViewCenter),

       0        // eod
};

void Qt3DCore::QCamera::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        QCamera *_t = static_cast<QCamera *>(_o);
        Q_UNUSED(_t)
        switch (_id) {
        case 0: _t->projectionTypeChanged((*reinterpret_cast< QCameraLens::ProjectionType(*)>(_a[1]))); break;
        case 1: _t->nearPlaneChanged((*reinterpret_cast< float(*)>(_a[1]))); break;
        case 2: _t->farPlaneChanged((*reinterpret_cast< float(*)>(_a[1]))); break;
        case 3: _t->fieldOfViewChanged((*reinterpret_cast< float(*)>(_a[1]))); break;
        case 4: _t->aspectRatioChanged((*reinterpret_cast< float(*)>(_a[1]))); break;
        case 5: _t->leftChanged((*reinterpret_cast< float(*)>(_a[1]))); break;
        case 6: _t->rightChanged((*reinterpret_cast< float(*)>(_a[1]))); break;
        case 7: _t->bottomChanged((*reinterpret_cast< float(*)>(_a[1]))); break;
        case 8: _t->topChanged((*reinterpret_cast< float(*)>(_a[1]))); break;
        case 9: _t->projectionMatrixChanged((*reinterpret_cast< const QMatrix4x4(*)>(_a[1]))); break;
        case 10: _t->positionChanged((*reinterpret_cast< const QVector3D(*)>(_a[1]))); break;
        case 11: _t->upVectorChanged((*reinterpret_cast< const QVector3D(*)>(_a[1]))); break;
        case 12: _t->viewCenterChanged((*reinterpret_cast< const QVector3D(*)>(_a[1]))); break;
        case 13: _t->viewVectorChanged((*reinterpret_cast< const QVector3D(*)>(_a[1]))); break;
        case 14: _t->viewMatrixChanged((*reinterpret_cast< const QMatrix4x4(*)>(_a[1]))); break;
        case 15: _t->setProjectionType((*reinterpret_cast< QCameraLens::ProjectionType(*)>(_a[1]))); break;
        case 16: _t->setNearPlane((*reinterpret_cast< float(*)>(_a[1]))); break;
        case 17: _t->setFarPlane((*reinterpret_cast< float(*)>(_a[1]))); break;
        case 18: _t->setFieldOfView((*reinterpret_cast< float(*)>(_a[1]))); break;
        case 19: _t->setAspectRatio((*reinterpret_cast< float(*)>(_a[1]))); break;
        case 20: _t->setLeft((*reinterpret_cast< float(*)>(_a[1]))); break;
        case 21: _t->setRight((*reinterpret_cast< float(*)>(_a[1]))); break;
        case 22: _t->setBottom((*reinterpret_cast< float(*)>(_a[1]))); break;
        case 23: _t->setTop((*reinterpret_cast< float(*)>(_a[1]))); break;
        case 24: _t->setPosition((*reinterpret_cast< const QVector3D(*)>(_a[1]))); break;
        case 25: _t->setUpVector((*reinterpret_cast< const QVector3D(*)>(_a[1]))); break;
        case 26: _t->setViewCenter((*reinterpret_cast< const QVector3D(*)>(_a[1]))); break;
        case 27: _t->translate((*reinterpret_cast< const QVector3D(*)>(_a[1])),(*reinterpret_cast< CameraTranslationOption(*)>(_a[2]))); break;
        case 28: _t->translate((*reinterpret_cast< const QVector3D(*)>(_a[1]))); break;
        case 29: _t->translateWorld((*reinterpret_cast< const QVector3D(*)>(_a[1])),(*reinterpret_cast< CameraTranslationOption(*)>(_a[2]))); break;
        case 30: _t->translateWorld((*reinterpret_cast< const QVector3D(*)>(_a[1]))); break;
        case 31: _t->tilt((*reinterpret_cast< float(*)>(_a[1]))); break;
        case 32: _t->pan((*reinterpret_cast< float(*)>(_a[1]))); break;
        case 33: _t->pan((*reinterpret_cast< float(*)>(_a[1])),(*reinterpret_cast< const QVector3D(*)>(_a[2]))); break;
        case 34: _t->roll((*reinterpret_cast< float(*)>(_a[1]))); break;
        case 35: _t->tiltAboutViewCenter((*reinterpret_cast< float(*)>(_a[1]))); break;
        case 36: _t->panAboutViewCenter((*reinterpret_cast< float(*)>(_a[1]))); break;
        case 37: _t->panAboutViewCenter((*reinterpret_cast< float(*)>(_a[1])),(*reinterpret_cast< const QVector3D(*)>(_a[2]))); break;
        case 38: _t->rollAboutViewCenter((*reinterpret_cast< float(*)>(_a[1]))); break;
        case 39: _t->rotate((*reinterpret_cast< const QQuaternion(*)>(_a[1]))); break;
        case 40: _t->rotateAboutViewCenter((*reinterpret_cast< const QQuaternion(*)>(_a[1]))); break;
        default: ;
        }
    } else if (_c == QMetaObject::IndexOfMethod) {
        int *result = reinterpret_cast<int *>(_a[0]);
        void **func = reinterpret_cast<void **>(_a[1]);
        {
            typedef void (QCamera::*_t)(QCameraLens::ProjectionType );
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&QCamera::projectionTypeChanged)) {
                *result = 0;
                return;
            }
        }
        {
            typedef void (QCamera::*_t)(float );
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&QCamera::nearPlaneChanged)) {
                *result = 1;
                return;
            }
        }
        {
            typedef void (QCamera::*_t)(float );
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&QCamera::farPlaneChanged)) {
                *result = 2;
                return;
            }
        }
        {
            typedef void (QCamera::*_t)(float );
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&QCamera::fieldOfViewChanged)) {
                *result = 3;
                return;
            }
        }
        {
            typedef void (QCamera::*_t)(float );
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&QCamera::aspectRatioChanged)) {
                *result = 4;
                return;
            }
        }
        {
            typedef void (QCamera::*_t)(float );
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&QCamera::leftChanged)) {
                *result = 5;
                return;
            }
        }
        {
            typedef void (QCamera::*_t)(float );
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&QCamera::rightChanged)) {
                *result = 6;
                return;
            }
        }
        {
            typedef void (QCamera::*_t)(float );
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&QCamera::bottomChanged)) {
                *result = 7;
                return;
            }
        }
        {
            typedef void (QCamera::*_t)(float );
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&QCamera::topChanged)) {
                *result = 8;
                return;
            }
        }
        {
            typedef void (QCamera::*_t)(const QMatrix4x4 & );
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&QCamera::projectionMatrixChanged)) {
                *result = 9;
                return;
            }
        }
        {
            typedef void (QCamera::*_t)(const QVector3D & );
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&QCamera::positionChanged)) {
                *result = 10;
                return;
            }
        }
        {
            typedef void (QCamera::*_t)(const QVector3D & );
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&QCamera::upVectorChanged)) {
                *result = 11;
                return;
            }
        }
        {
            typedef void (QCamera::*_t)(const QVector3D & );
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&QCamera::viewCenterChanged)) {
                *result = 12;
                return;
            }
        }
        {
            typedef void (QCamera::*_t)(const QVector3D & );
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&QCamera::viewVectorChanged)) {
                *result = 13;
                return;
            }
        }
        {
            typedef void (QCamera::*_t)(const QMatrix4x4 & );
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&QCamera::viewMatrixChanged)) {
                *result = 14;
                return;
            }
        }
    }
#ifndef QT_NO_PROPERTIES
    else if (_c == QMetaObject::ReadProperty) {
        QCamera *_t = static_cast<QCamera *>(_o);
        Q_UNUSED(_t)
        void *_v = _a[0];
        switch (_id) {
        case 0: *reinterpret_cast< Qt3DCore::QCameraLens::ProjectionType*>(_v) = _t->projectionType(); break;
        case 1: *reinterpret_cast< float*>(_v) = _t->nearPlane(); break;
        case 2: *reinterpret_cast< float*>(_v) = _t->farPlane(); break;
        case 3: *reinterpret_cast< float*>(_v) = _t->fieldOfView(); break;
        case 4: *reinterpret_cast< float*>(_v) = _t->aspectRatio(); break;
        case 5: *reinterpret_cast< float*>(_v) = _t->left(); break;
        case 6: *reinterpret_cast< float*>(_v) = _t->right(); break;
        case 7: *reinterpret_cast< float*>(_v) = _t->bottom(); break;
        case 8: *reinterpret_cast< float*>(_v) = _t->top(); break;
        case 9: *reinterpret_cast< QMatrix4x4*>(_v) = _t->projectionMatrix(); break;
        case 10: *reinterpret_cast< QVector3D*>(_v) = _t->position(); break;
        case 11: *reinterpret_cast< QVector3D*>(_v) = _t->upVector(); break;
        case 12: *reinterpret_cast< QVector3D*>(_v) = _t->viewCenter(); break;
        case 13: *reinterpret_cast< QVector3D*>(_v) = _t->viewVector(); break;
        case 14: *reinterpret_cast< QMatrix4x4*>(_v) = _t->viewMatrix(); break;
        default: break;
        }
    } else if (_c == QMetaObject::WriteProperty) {
        QCamera *_t = static_cast<QCamera *>(_o);
        Q_UNUSED(_t)
        void *_v = _a[0];
        switch (_id) {
        case 0: _t->setProjectionType(*reinterpret_cast< Qt3DCore::QCameraLens::ProjectionType*>(_v)); break;
        case 1: _t->setNearPlane(*reinterpret_cast< float*>(_v)); break;
        case 2: _t->setFarPlane(*reinterpret_cast< float*>(_v)); break;
        case 3: _t->setFieldOfView(*reinterpret_cast< float*>(_v)); break;
        case 4: _t->setAspectRatio(*reinterpret_cast< float*>(_v)); break;
        case 5: _t->setLeft(*reinterpret_cast< float*>(_v)); break;
        case 6: _t->setRight(*reinterpret_cast< float*>(_v)); break;
        case 7: _t->setBottom(*reinterpret_cast< float*>(_v)); break;
        case 8: _t->setTop(*reinterpret_cast< float*>(_v)); break;
        case 10: _t->setPosition(*reinterpret_cast< QVector3D*>(_v)); break;
        case 11: _t->setUpVector(*reinterpret_cast< QVector3D*>(_v)); break;
        case 12: _t->setViewCenter(*reinterpret_cast< QVector3D*>(_v)); break;
        default: break;
        }
    } else if (_c == QMetaObject::ResetProperty) {
    }
#endif // QT_NO_PROPERTIES
}

static const QMetaObject * const qt_meta_extradata_Qt3DCore__QCamera[] = {
        &Qt3DCore::QCameraLens::staticMetaObject,
    Q_NULLPTR
};

const QMetaObject Qt3DCore::QCamera::staticMetaObject = {
    { &QEntity::staticMetaObject, qt_meta_stringdata_Qt3DCore__QCamera.data,
      qt_meta_data_Qt3DCore__QCamera,  qt_static_metacall, qt_meta_extradata_Qt3DCore__QCamera, Q_NULLPTR}
};


const QMetaObject *Qt3DCore::QCamera::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->dynamicMetaObject() : &staticMetaObject;
}

void *Qt3DCore::QCamera::qt_metacast(const char *_clname)
{
    if (!_clname) return Q_NULLPTR;
    if (!strcmp(_clname, qt_meta_stringdata_Qt3DCore__QCamera.stringdata0))
        return static_cast<void*>(const_cast< QCamera*>(this));
    return QEntity::qt_metacast(_clname);
}

int Qt3DCore::QCamera::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QEntity::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 41)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 41;
    } else if (_c == QMetaObject::RegisterMethodArgumentMetaType) {
        if (_id < 41)
            *reinterpret_cast<int*>(_a[0]) = -1;
        _id -= 41;
    }
#ifndef QT_NO_PROPERTIES
   else if (_c == QMetaObject::ReadProperty || _c == QMetaObject::WriteProperty
            || _c == QMetaObject::ResetProperty || _c == QMetaObject::RegisterPropertyMetaType) {
        qt_static_metacall(this, _c, _id, _a);
        _id -= 15;
    } else if (_c == QMetaObject::QueryPropertyDesignable) {
        _id -= 15;
    } else if (_c == QMetaObject::QueryPropertyScriptable) {
        _id -= 15;
    } else if (_c == QMetaObject::QueryPropertyStored) {
        _id -= 15;
    } else if (_c == QMetaObject::QueryPropertyEditable) {
        _id -= 15;
    } else if (_c == QMetaObject::QueryPropertyUser) {
        _id -= 15;
    }
#endif // QT_NO_PROPERTIES
    return _id;
}

// SIGNAL 0
void Qt3DCore::QCamera::projectionTypeChanged(QCameraLens::ProjectionType _t1)
{
    void *_a[] = { Q_NULLPTR, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 0, _a);
}

// SIGNAL 1
void Qt3DCore::QCamera::nearPlaneChanged(float _t1)
{
    void *_a[] = { Q_NULLPTR, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 1, _a);
}

// SIGNAL 2
void Qt3DCore::QCamera::farPlaneChanged(float _t1)
{
    void *_a[] = { Q_NULLPTR, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 2, _a);
}

// SIGNAL 3
void Qt3DCore::QCamera::fieldOfViewChanged(float _t1)
{
    void *_a[] = { Q_NULLPTR, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 3, _a);
}

// SIGNAL 4
void Qt3DCore::QCamera::aspectRatioChanged(float _t1)
{
    void *_a[] = { Q_NULLPTR, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 4, _a);
}

// SIGNAL 5
void Qt3DCore::QCamera::leftChanged(float _t1)
{
    void *_a[] = { Q_NULLPTR, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 5, _a);
}

// SIGNAL 6
void Qt3DCore::QCamera::rightChanged(float _t1)
{
    void *_a[] = { Q_NULLPTR, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 6, _a);
}

// SIGNAL 7
void Qt3DCore::QCamera::bottomChanged(float _t1)
{
    void *_a[] = { Q_NULLPTR, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 7, _a);
}

// SIGNAL 8
void Qt3DCore::QCamera::topChanged(float _t1)
{
    void *_a[] = { Q_NULLPTR, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 8, _a);
}

// SIGNAL 9
void Qt3DCore::QCamera::projectionMatrixChanged(const QMatrix4x4 & _t1)
{
    void *_a[] = { Q_NULLPTR, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 9, _a);
}

// SIGNAL 10
void Qt3DCore::QCamera::positionChanged(const QVector3D & _t1)
{
    void *_a[] = { Q_NULLPTR, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 10, _a);
}

// SIGNAL 11
void Qt3DCore::QCamera::upVectorChanged(const QVector3D & _t1)
{
    void *_a[] = { Q_NULLPTR, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 11, _a);
}

// SIGNAL 12
void Qt3DCore::QCamera::viewCenterChanged(const QVector3D & _t1)
{
    void *_a[] = { Q_NULLPTR, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 12, _a);
}

// SIGNAL 13
void Qt3DCore::QCamera::viewVectorChanged(const QVector3D & _t1)
{
    void *_a[] = { Q_NULLPTR, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 13, _a);
}

// SIGNAL 14
void Qt3DCore::QCamera::viewMatrixChanged(const QMatrix4x4 & _t1)
{
    void *_a[] = { Q_NULLPTR, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 14, _a);
}
QT_END_MOC_NAMESPACE
